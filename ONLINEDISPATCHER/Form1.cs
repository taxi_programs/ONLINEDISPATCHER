﻿#undef TRACE

using System.Diagnostics;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using csmapcontrol;
using System.Net;
using System.IO;
using System.Data.SqlClient;
using System.Xml;
using System.Data.SQLite;



//using LumiSoft.SIP.UA.Resources;
/*
using LumiSoft.Net;
using LumiSoft.Net.SDP;
using LumiSoft.Net.SIP;
using LumiSoft.Net.SIP.Debug;
using LumiSoft.Net.SIP.Stack;
using LumiSoft.Net.SIP.Message;
 */ 
using LumiSoft.Net.Media;
using LumiSoft.Net.Media.Codec;
using LumiSoft.Net.Media.Codec.Audio;
/*using LumiSoft.Net.RTP;
using LumiSoft.Net.RTP.Debug;
using LumiSoft.Net.STUN.Client;
using LumiSoft.Net.UPnP.NAT;
 */ 
using System.Text;
using LumiSoft.Net;




/* 2. прокладка маршрута - автоматический запрос к серверу, разбор XML+++
 * 1. сохранение тайлов на диске - переработка компонента +++
 * 3. загрузка маркеров из базы данных. обновление маркеров. по таймеру картинка маркера. текст маркера
 * 4. получение координат объекта из базы - 5000 наименований. 80 тыс кооординат. возврат мнгновенный.
 * 
 * 
9	256		      100 000 000  	на линии в районе
104	772		    1 100 000 100  	на заказе
425	768		    1 100 000 000  	на линии но не в районе
1	1280	   10 100 000 000  	не на линии

 * 
 * 
 * 
 */



namespace OSM_CLIENT
{
    public partial class Form1 : Form
    {
        AudioOut audioout = new AudioOut(AudioOut.Devices[0], 8000, 16, 1);


        //String DataFile = @"c:\projects\OSM_CLIENT\database\osm_client.db3";
        String DataFile = @"database\osm_client.db3";
        
        private int scale = 32;

        private double last_click_lat = 0;
        private double last_click_lon = 0;
        //private double to_lat = 0;
        //private double to_lon = 0;


        csmapcontrol.MapPath calculated_path = new csmapcontrol.MapPath();

        private Boolean calcDone = true;

        private double path_distance = 0;


        SqlConnection m_conn=null;
        FirebirdSql.Data.Firebird.FbConnection m_fbConn = null;
        

        Marker from_marker = new Marker(), to_marker = new Marker();

        private Dictionary<string, car> mCars = new Dictionary<string,car>();



        SipCaler caller = new SipCaler();



        public class car
        {
            //779||7-ка  1991г.в. 779||57690738||39805210
            public car(String car_data)
            {
                String [] s_arr = car_data.Split('|');
                if (s_arr.Length == 4)
                {
                    id = s_arr[0];
                    name = s_arr[1];
                    Double.TryParse(s_arr[2], out lat);
                    Double.TryParse(s_arr[3], out lon);
                    lat = lat / 1000000;
                    lon = lon / 1000000;
                }
            }
            public double lat = 0;
            public double lon = 0;
            public String name="";
            public String id="";
            public String status = "";

            public Marker marker = new Marker();
        }


        private Dictionary<String, String> zones = new Dictionary<string,string>();

        public Form1()
        {
            InitializeComponent();
            caller.main_form = this;
        }

        private void scrscale_ValueChanged(object sender, EventArgs e)
        {
            

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //mapControl1.ViewChanged += MapViewChanged;
            //mapControl1.LocationClicked += MapLocationClicked;
            //mapControl1.TrackPointClicked += TrackPointClicked;

            mapControl1.CachePath = "./tiles";
            mapControl1.Latitude = 57.6315;
            mapControl1.Longitude=39.8684;
            Uri newUri = new Uri("http://192.168.100.10:8080/");
            WebProxy myProxy = new WebProxy();
            myProxy.Address = newUri;
            //mapControl1.webProxy = myProxy;

            

            //57.6477&tlon=39.8684
            //39.8671&tlat=57.6315
            scale = 26;
            mapControl1.LonScale = MapControl.DiscreteScales[scale];
            


            service_name.SelectedIndex = 0;

            // Add a Track for demo purposes...
            //Track t=new Track("../SwinstyReservoir.gpx");
            //mapControl1.Tracks.Add(t);

            mapControl1.HighlightEntities = true;

            
            mapControl1.MouseWheel += new MouseEventHandler(wheel);
            loadData();

        }
        delegate void SetStreetsCallback(string[] streets);


        private void SetStreets(string [] streets) {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.from_street.InvokeRequired) {
                SetStreetsCallback d = new SetStreetsCallback(SetStreets);
                this.Invoke(d, new object[] { streets });
            } else {
                this.from_street.Items.AddRange(streets);
                to_street.Items.AddRange(streets);
                over_street1.Items.AddRange(streets);
                over_street2.Items.AddRange(streets);
            }
        }



        private void threadLoadData() { 
            Console.WriteLine("hello!");

            SQLiteConnection con = new SQLiteConnection();
            
            con.ConnectionString = @"Data Source=" + DataFile + ";New=False;Version=3"; // в таком виде всё работает!
            try {
                con.Open();
            } catch (Exception ex) {
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



            System.Data.SQLite.SQLiteCommand cmd = con.CreateCommand();

            

            cmd.CommandText = "SELECT DISTINCT street FROM addresses";

            List<String> streets = new List<String>();

            SQLiteDataReader dr = cmd.ExecuteReader();
            while (dr.Read()) {
                streets.Add(dr.GetString(0));
                /*
                                from_street.Items.Add(dr.GetString(0));
                                to_street.Items.Add(dr.GetString(0));
                                over_street1.Items.Add(dr.GetString(0));
                                over_street2.Items.Add(dr.GetString(0));
                 */
            }
            dr.Close();

            cmd.CommandText = "SELECT DISTINCT name FROM addresses";

            dr = cmd.ExecuteReader();
            

            
            
            while (dr.Read()) {
                streets.Add(dr.GetString(0));
                
            }
            dr.Close();
            Console.WriteLine("hello 2!");

            this.SetStreets(streets.ToArray());
            //this.from_street.Items.AddRange(streets.ToArray());

            /*
             from_building_number.Items.Clear();
             from_street.Items.aAdd(dr.GetString(0));
                to_street.Items.Add(dr.GetString(0));
                over_street1.Items.Add(dr.GetString(0));
                over_street2.Items.Add(dr.GetString(0));
             * */
        }
        private void loadData()
        {
            System.Threading.Thread t = new System.Threading.Thread(new System.Threading.ThreadStart(threadLoadData));
            t.Start();

            
        }

        private void wheel(object sender, MouseEventArgs e)
        {
            scale -=  (int) (e.Delta / 120);

            if (scale < 0)
                scale = 0;
            if (scale > MapControl.DiscreteScales.Length - 1)
                scale = MapControl.DiscreteScales.Length - 1;

            mapControl1.LonScale = MapControl.DiscreteScales[scale];
            

        }

        private void mapControl1_ViewChanged(double lat, double lon, double scale)
        {
            
            if (System.Diagnostics.Process.GetCurrentProcess().WorkingSet64 > 50000000)
                MapControl.EmptyHalfTileCache();

            
            
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void mapControl1_LocationClicked(double lat, double lon)
        {
            last_click_lat = lat;
            last_click_lon = lon;
            if ((Control.ModifierKeys & Keys.Shift) != Keys.Shift)
                return;

            if (calcDone == false)
                return;
            calculated_path.addLastPoint(lat, lon, "");

            doCalculate();

            
            

        }

        private void doUpdateCars()
        {
            if (use_sql.Checked == false) {
                mapControl1.MarkersCars.Clear();
                return;
            }

            string connString = "Data Source=192.168.100.1\\TAXA;Database=TAXA;Uid=taxausr;Pwd=654321;";

            //string strSQL = "select * from auth_table where login ='" + log + "' AND pass ='" + pass + "'";

            if (m_conn == null)
            {
                m_conn = new SqlConnection(connString);
                m_conn.Open();
            }
            
            SqlCommand cmd = m_conn.CreateCommand();
            cmd.CommandText = "select cast(id as varchar(10))+cast('|' as varchar(10))+cast(descr as varchar(32))+cast(' ' as varchar(10))+cast(gosnum as varchar(10))+cast('|' as varchar(10))+cast(lat as varchar(10))+cast('|' as varchar(10))+cast(lon as varchar(10)), (status & 4) from cars where (status & 1024 = 0)";
            SqlDataReader dr =  cmd.ExecuteReader();
            while (dr.Read())
            {
                String s = dr.GetString(0);
                String on_order = dr.GetInt32(1).ToString();
                //System.Console.WriteLine(s);
                MapControl.MarkerCar c = new MapControl.MarkerCar(s, on_order);
                {
                    if(mapControl1.MarkersCars.ContainsKey(c.id))
                    {
                        mapControl1.MarkersCars.Remove(c.id);
                    }
                    mapControl1.MarkersCars.Add(c.id, c);
                }
            }

            dr.Close();
            mapControl1.Invalidate();

            /*            cmd.CommandText = "SELECT ID, DESCR  FROM ZONES ORDER BY ID";
            dr = cmd.ExecuteReader();
            while (dr.Read()){
                zones.Add(dr.GetString(0), dr.GetString(0));
            }


            zones
            */


            
            
        }
        
        

        private void doCalculate()
        {

            
            //csmapcontrol.MapPath.PathPoint from = new csmapcontrol.MapPath.PathPoint(from_lat, from_lon, "");
            //csmapcontrol.MapPath.PathPoint to = new csmapcontrol.MapPath.PathPoint(to_lat, to_lon, "");

            //path.addInterPoint(from_lat, from_lon, "");
            //path.addInterPoint(to_lat, to_lon, "");

            calculated_path.calculatePath(4000);
            mapControl1.Paths.Clear();
            mapControl1.Paths.Add(calculated_path);

            mapControl1.Invalidate();
            
            
            distance.Text = String.Format("{0:.00} км", calculated_path.path_distance);


            service_name_SelectedIndexChanged(null, null);

            
            calcDone = true;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            //http://wiki.openstreetmap.org/wiki/YOURS
            //http://www.yournavigation.org/api/1.0/gosmore.php?format=kml&flat=57.6929&flon=39.7779&tlat=57.5930&tlon=39.8653&v=motorcar&fast=1&layer=mapnik&instructions=1
            String c = "39.77779,57.69285 39.778449,57.692441 39.778303,57.692247 39.776215,57.691203 39.775907,57.691048 39.775683,57.690935 39.777974,57.689605 39.778278,57.689419 39.778529,57.689261 39.779056,57.68897 39.784246,57.686027 39.784726,57.685705 39.784562,57.685559 39.783526,57.684518 39.779385,57.680358 39.779303,57.680278 39.773797,57.674885 39.773765,57.674756 39.773001,57.67395 39.77217,57.673126 39.771777,57.672791 39.770774,57.671819 39.770532,57.671625 39.770289,57.671468 39.769653,57.671076 39.769392,57.670861 39.769211,57.670655 39.768948,57.67034 39.768521,57.669676 39.768285,57.669322 39.768011,57.668991 39.766994,57.667967 39.765207,57.666441 39.762254,57.664003 39.76129,57.663216 39.761082,57.663047 39.757926,57.66049 39.756622,57.659404 39.755885,57.658645 39.754847,57.657285 39.752181,57.652914 39.751022,57.65147 39.749007,57.649565 39.748738,57.64931 39.748482,57.649059 39.748284,57.648837 39.748117,57.648626 39.747948,57.648384 39.7477,57.647949 39.747575,57.647705 39.747517,57.647578 39.747442,57.64736 39.747413,57.647229 39.747397,57.647095 39.747371,57.646771 39.74736,57.646359 39.747613,57.641755 39.747621,57.64121 39.747613,57.640972 39.747592,57.640717 39.747554,57.640365 39.747445,57.639772 39.747391,57.639438 39.747302,57.639089 39.74717,57.638673 39.74656,57.636959 39.746314,57.636273 39.746199,57.635979 39.745934,57.635393 39.745881,57.6352 39.745861,57.635033 39.745869,57.63485 39.745913,57.634693 39.745967,57.63453 39.746047,57.634406 39.746162,57.634288 39.746327,57.634171 39.746557,57.634058 39.746786,57.633961 39.747639,57.633688 39.747987,57.633572 39.748286,57.633446 39.756072,57.629564 39.757638,57.628782 39.762681,57.626313 39.778328,57.617862 39.779144,57.617399 39.77999,57.616908 39.780629,57.61651 39.781122,57.616186 39.781411,57.615965 39.78214,57.615345 39.783277,57.614362 39.783323,57.614322 39.785574,57.612258 39.791164,57.607196 39.795714,57.603046 39.796134,57.602686 39.797017,57.601886 39.797399,57.601528 39.799413,57.599704 39.800026,57.599216 39.800468,57.598878 39.800977,57.598527 39.801383,57.598285 39.801765,57.5981 39.80218,57.597941 39.802641,57.597792 39.803135,57.597654 39.803801,57.597494 39.804929,57.597266 39.80712,57.596919 39.807277,57.596891 39.808186,57.596727 39.808311,57.596705 39.809666,57.596368 39.810718,57.595867 39.81149,57.59539 39.812573,57.594483 39.816291,57.591182 39.816513,57.590995 39.817855,57.589644 39.818562,57.588929 39.819424,57.588058 39.820603,57.586865 39.822114,57.585298 39.822822,57.584644 39.82398,57.583839 39.825588,57.583038 39.833652,57.57984 39.83503,57.579288 39.835735,57.579006 39.838442,57.577923 39.839669,57.577432 39.839923,57.577331 39.840025,57.57729 39.840178,57.577229 39.844045,57.575683 39.844371,57.575575 39.845418,57.576378 39.845683,57.576588 39.846396,57.577154 39.847986,57.578316 39.850554,57.580177 39.852036,57.581388 39.852082,57.581425 39.852422,57.5817 39.852987,57.582196 39.85407,57.583277 39.855106,57.584358 39.855744,57.585078 39.857382,57.58701 39.858517,57.588354 39.858606,57.58847 39.859132,57.589158 39.859601,57.58973 39.859857,57.590042 39.860435,57.590591 39.861251,57.591255 39.861434,57.591382 39.861618,57.59151 39.863246,57.59254 39.863795,57.592868 39.863956,57.592971 39.864919,57.593573 39.865547,57.593971 39.865593,57.593663 39.865448,57.593279 39.865632,57.593095";
            String [] c_arr = c.Split(' ');
            Track t = new Track();
            for (int i = 0; i < c_arr.Length; i++)
            {
                String[] c1 = c_arr[i].Split(',');
                c1[0]=c1[0].Replace('.', ',');
                c1[1] = c1[1].Replace('.', ',');
                TrackPoint tp = new TrackPoint();
                tp.Lon = Double.Parse(c1[0]);
                tp.Lat = Double.Parse(c1[1]);

                t.Points.Add(tp);
            }

            mapControl1.Tracks.Add(t);

        }

        private void diagnostic_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            doCalculate();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            doUpdateCars();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            doUpdateCars();
        }

        private void loadCSV()
        {
            

            SQLiteConnection con = new SQLiteConnection();
            con.ConnectionString = @"Data Source=" + DataFile + ";New=False;Version=3"; // в таком виде всё работает!
            try
            {
                con.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



            System.Data.SQLite.SQLiteCommand cmd = con.CreateCommand();
            SQLiteTransaction trans = con.BeginTransaction();
            int n = 0;
            StreamReader  sr = new StreamReader("c:/addresses.csv");
            
            String s = "";
            while (sr.Peek() >= 0) 
            {
                String [] adr = sr.ReadLine().Split(';');
                if (adr.Length == 6)
                {
                    double lat, lon;
                    double.TryParse(adr[3], out lat);
                    double.TryParse(adr[4], out lon);
                    cmd.CommandText = "insert into addresses(street, building_num, lat, lon, name) values(@street, @house_number, @lat, @lon, @name)";
                    cmd.Parameters.AddWithValue("@street", adr[0]);
                    cmd.Parameters.AddWithValue("@house_number", adr[1]);
                    cmd.Parameters.AddWithValue("@lat", lat);
                    cmd.Parameters.AddWithValue("@lon", lon);
                    cmd.Parameters.AddWithValue("@name", adr[2]);

                    cmd.ExecuteNonQuery();
                }
                n++;
                
            }

            trans.Commit();
            Console.WriteLine(n.ToString() + "nodes read");
            //log.Text += nodes.Count.ToString() + " узлов прочитано";
            Console.ReadLine();
        }

        private void btnLoadXML_Click(object sender, EventArgs e)
        {
            

            SQLiteConnection con = new SQLiteConnection();
            con.ConnectionString = @"Data Source=" + DataFile + ";New=False;Version=3"; // в таком виде всё работает!
            try
            {
                con.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



            System.Data.SQLite.SQLiteCommand cmd = con.CreateCommand();
            SQLiteTransaction trans =   con.BeginTransaction();
            int n = 0;
            XmlTextReader reader = new XmlTextReader("c:/ru-yar.xml");
            String last_id = "";   //для вставки в связки и таги - последний путь по которому прошелся парсер
            while (reader.Read())
            {
                n++;
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element: // Узел является элементом.
                        if (reader.Name == "node")
                        {
                            //Console.Write("<" + reader.Name);
                            //Console.WriteLine("> id = " + reader.GetAttribute("id"));
                            cmd.CommandText = "insert into primitives(id, name, uid, lat, lon) values('" +
                                reader.GetAttribute("id") + "','node', '" + reader.GetAttribute("uid") + "','"+
                            reader.GetAttribute("lat") + "','" +reader.GetAttribute("lon") +"')";
                            cmd.ExecuteNonQuery();
                            last_id = reader.GetAttribute("id");
                            //               
                            
                        }
                        if (reader.Name == "way")
                        {
                            cmd.CommandText = "insert into primitives(id, name, uid) values('" +
                                reader.GetAttribute("id") + "','way', '" + reader.GetAttribute("uid") + "')";
                            cmd.ExecuteNonQuery();
                            last_id = reader.GetAttribute("id");
                            //загрузить список нодов
                        }
                        if (reader.Name == "relation")
                        {
                            cmd.CommandText = "insert into primitives(id, name, uid) values('" +
                                reader.GetAttribute("id") + "','relation', '" + reader.GetAttribute("uid") + "')";
                            cmd.ExecuteNonQuery();
                            last_id = reader.GetAttribute("id");
                            //загрузить список нодов
                        }
                        if (reader.Name == "nd")
                        {
                            cmd.CommandText = "insert into nd(primitive_id, ref) values('" +
                                last_id+"','" + reader.GetAttribute("ref")+"')";
                            cmd.ExecuteNonQuery();
                        }
                        if (reader.Name == "tag")
                        {
                            cmd.CommandText = "insert into tags(primitive_id, name, value) values(@primitive_id, @name, @value)";
                            cmd.Parameters.AddWithValue("@primitive_id", last_id);
                            cmd.Parameters.AddWithValue("@name", reader.GetAttribute("k"));
                            cmd.Parameters.AddWithValue("@value", reader.GetAttribute("v"));

                            cmd.ExecuteNonQuery();
                        }


                        break;
                    case XmlNodeType.Text: // Вывести текст в каждом элементе.
                        //Console.WriteLine(reader.Value);
                        break;
                    case XmlNodeType.EndElement: // Вывести конец элемента.
                        //Console.Write("</" + reader.Name);
                        //Console.WriteLine(">");
                        break;
                }
                

            }

            trans.Commit();
            Console.WriteLine(n.ToString() + "nodes read");
            //log.Text += nodes.Count.ToString() + " узлов прочитано";
            Console.ReadLine();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            /*String [] c = coord_from.Text.Replace('.', ',').Split(' ');
            if (c.Length == 2)
            {
                Double.TryParse(c[1], out from_lat);
                Double.TryParse(c[0], out from_lon);
            }
           

            c = coord_to.Text.Replace('.', ',').Split(' ');
            if (c.Length == 2)
            {
                Double.TryParse(c[1], out to_lat);
                Double.TryParse(c[0], out to_lon);
            }
            */

            //if (calculated_path
            //calculated_path.InterPoints[

            doCalculate();

        }

        private void btnLoadCsv_Click(object sender, EventArgs e)
        {
            loadCSV();
        }

        private void path_distance_Click(object sender, EventArgs e)
        {

        }

        private void service_name_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (service_name.SelectedIndex == 0)
            {
                path_cost.Text = String.Format("{0:.} руб", calculated_path.path_distance * 8 + 80);
            }
            if (service_name.SelectedIndex == 1)
            {
                path_cost.Text = String.Format("{0:.} руб", calculated_path.path_distance * 7 + 70);
            }
            if (service_name.SelectedIndex == 2)
            {
                path_cost.Text = String.Format("{0:.} руб", calculated_path.path_distance * 12 + 110);
            }
            
        }

        private String doSearchAddress(String street, String building_number)
        {
            

            SQLiteConnection con = new SQLiteConnection();
            con.ConnectionString = @"Data Source=" + DataFile + ";New=False;Version=3"; // в таком виде всё работает!
            try
            {
                con.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            System.Data.SQLite.SQLiteCommand cmd = con.CreateCommand();
            String result = "";
            if (building_number == "")
            { //поиск места - без номера дома
                cmd.CommandText = "SELECT lon ||' '||lat from addresses where name=@name";
                cmd.Parameters.AddWithValue("@name", street);
                result = (String)cmd.ExecuteScalar();

            }
            else
            {   //поиск по улице и дому
                cmd.CommandText = "SELECT lon ||' '||lat from addresses where street=@street and building_num=@building_num";
                cmd.Parameters.AddWithValue("@street", street);
                cmd.Parameters.AddWithValue("@building_num", building_number);
                result = (String)cmd.ExecuteScalar();
            }
            
            return result;
            

        }

        private void button5_Click(object sender, EventArgs e)
        {
            coord_from.Text = doSearchAddress(from_street.Text, from_building_number.Text);
            
        }

        private void button8_Click(object sender, EventArgs e)
        {
            coord_to.Text = doSearchAddress(to_street.Text, to_building_number.Text);        
        }

        private void from_street_TextChanged(object sender, EventArgs e)
        {
            

            SQLiteConnection con = new SQLiteConnection();
            con.ConnectionString = @"Data Source=" + DataFile + ";New=False;Version=3"; // в таком виде всё работает!
            try
            {
                con.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            System.Data.SQLite.SQLiteCommand cmd = con.CreateCommand();

            from_building_number.Items.Clear();
            if (from_street.Text!="")
            {

                cmd.CommandText = "SELECT DISTINCT building_num FROM addresses WHERE street LIKE @street||'%'";
                cmd.Parameters.AddWithValue("@street",  from_street.Text);
                SQLiteDataReader dr =  cmd.ExecuteReader();
                while (dr.Read())
                {
                
                    from_building_number.Items.Add(dr.GetString(0));
                }
                dr.Close();



                coord_from.Text = doSearchAddress(from_street.Text, from_building_number.Text);
                btnSave.Enabled = true;
            }
            
        }

        private void from_street_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void to_street_TextChanged(object sender, EventArgs e)
        {
            

            SQLiteConnection con = new SQLiteConnection();
            con.ConnectionString = @"Data Source=" + DataFile + ";New=False;Version=3"; // в таком виде всё работает!
            try
            {
                con.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            System.Data.SQLite.SQLiteCommand cmd = con.CreateCommand();

            to_building_number.Items.Clear();
            if (to_street.Text != "")
            {

                cmd.CommandText = "SELECT DISTINCT building_num FROM addresses WHERE street LIKE @street||'%'";
                cmd.Parameters.AddWithValue("@street", to_street.Text);
                SQLiteDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {

                    to_building_number.Items.Add(dr.GetString(0));
                }
                dr.Close();

                coord_to.Text = doSearchAddress(to_street.Text, to_building_number.Text);
                btnSave.Enabled = true;
            }
        }

        private void from_building_number_TextChanged(object sender, EventArgs e)
        {
            coord_from.Text = doSearchAddress(from_street.Text, from_building_number.Text);
            btnSave.Enabled = true;
        }

        private void to_building_number_TextChanged(object sender, EventArgs e)
        {
            coord_to.Text = doSearchAddress(to_street.Text, to_building_number.Text);
            btnSave.Enabled = true;
        }

        

        private void pointer1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawLine(Pens.Green, 0, 0, 10, 10);
        }

        private void маршрутОтсюдаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            calculated_path.setPoint(0,last_click_lat , last_click_lon, "");
            coord_from.Text = String.Format("{0:.000000} {1:.000000}", last_click_lat, last_click_lon);
        }

        private void маршрутСюдаToolStripMenuItem_Click(object sender, EventArgs e) {
            
            calculated_path.setPoint(3,last_click_lat, last_click_lon, "");
            coord_to.Text = String.Format("{0:.000000} {1:.000000}", last_click_lat, last_click_lon);
        }

        private void промежуточнаяТочка1ToolStripMenuItem_Click(object sender, EventArgs e) {
            calculated_path.setPoint(1,last_click_lat, last_click_lon, "");
            coord_over_1.Text = String.Format("{0:.000000} {1:.000000}", last_click_lat, last_click_lon);
        }
        private void промежуточнаяТочка2ToolStripMenuItem_Click(object sender, EventArgs e) {
            calculated_path.setPoint(2, last_click_lat, last_click_lon, "");
            coord_over_2.Text = String.Format("{0:.000000} {1:.000000}", last_click_lat, last_click_lon);
        }

        private void over_street1_TextChanged(object sender, EventArgs e) {
            

            SQLiteConnection con = new SQLiteConnection();
            con.ConnectionString = @"Data Source=" + DataFile + ";New=False;Version=3"; // в таком виде всё работает!
            try {
                con.Open();
            } catch (Exception ex) {
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            System.Data.SQLite.SQLiteCommand cmd = con.CreateCommand();

            over_building_number1.Items.Clear();
            if (over_street1.Text != "") {

                cmd.CommandText = "SELECT DISTINCT building_num FROM addresses WHERE street LIKE @street||'%'";
                cmd.Parameters.AddWithValue("@street", over_street1.Text);
                SQLiteDataReader dr = cmd.ExecuteReader();
                while (dr.Read()) {

                    over_building_number1.Items.Add(dr.GetString(0));
                }
                dr.Close();



                coord_over_1.Text = doSearchAddress(over_street1.Text, over_building_number1.Text);
            }
        }
        private void over_street2_TextChanged(object sender, EventArgs e) {
            

            SQLiteConnection con = new SQLiteConnection();
            con.ConnectionString = @"Data Source=" + DataFile + ";New=False;Version=3"; // в таком виде всё работает!
            try {
                con.Open();
            } catch (Exception ex) {
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            System.Data.SQLite.SQLiteCommand cmd = con.CreateCommand();

            over_building_number2.Items.Clear();
            if (over_street2.Text != "") {

                cmd.CommandText = "SELECT DISTINCT building_num FROM addresses WHERE street LIKE @street||'%'";
                cmd.Parameters.AddWithValue("@street", over_street2.Text);
                SQLiteDataReader dr = cmd.ExecuteReader();
                while (dr.Read()) {

                    over_building_number2.Items.Add(dr.GetString(0));
                }
                dr.Close();



                coord_over_2.Text = doSearchAddress(over_street2.Text, over_building_number2.Text);
            }
        }

        private void over_street2_SelectedIndexChanged(object sender, EventArgs e) {
         
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e) {

        }

        private void очиститьМаршрутToolStripMenuItem_Click(object sender, EventArgs e) {
            calculated_path.InterPoints.Clear();
            calculated_path.Points.Clear();
            calculated_path.path_distance = 0;
            mapControl1.Invalidate();
            from_street.Text="";
            from_building_number.Text="";
            to_street.Text ="";
            to_building_number.Text = "";
            over_street1.Text = "";
            over_building_number1.Text = "";
            over_street2.Text = "";
            over_building_number2.Text = "";
            distance.Text = "";
            path_cost.Text = "";

        }
        
        private void coord_from_TextChanged(object sender, EventArgs e) {
            String[] arr = coord_from.Text.Replace('.', ',').Split(' ');
            if (arr.Length == 2) {
                double lat, lon;

                if ((Double.TryParse(arr[0], out lat)) && (
                        Double.TryParse(arr[1], out lon))) {
                    calculated_path.setPoint(0, lat, lon, "");
                }
            }
        }

        private void coord_to_TextChanged(object sender, EventArgs e) {
            String[] arr = coord_to.Text.Replace('.', ',').Split(' ');
            if (arr.Length == 2) {
                double lat, lon;

                if ((Double.TryParse(arr[0], out lat)) && (
                        Double.TryParse(arr[1], out lon))) {
                    calculated_path.setPoint(3, lat, lon, "");
                }
            }
        }

        private void coord_over_1_TextChanged(object sender, EventArgs e) {
            String[] arr = coord_over_1.Text.Replace('.', ',').Split(' ');
            if (arr.Length == 2) {
                double lat, lon;

                if ((Double.TryParse(arr[0], out lat)) && (
                        Double.TryParse(arr[1], out lon))) {
                    calculated_path.setPoint(1, lat, lon, "");
                }
            }
        }

        private void coord_over_2_TextChanged(object sender, EventArgs e) {
            String[] arr = coord_over_2.Text.Replace('.', ',').Split(' ');
            if (arr.Length == 2) {
                double lat, lon;

                if ((Double.TryParse(arr[0], out lat)) && (
                        Double.TryParse(arr[1], out lon))) {
                    calculated_path.setPoint(2, lat, lon, "");
                }
            }
        }

        private void over_building_number1_TextChanged(object sender, EventArgs e) {
            coord_over_1.Text = doSearchAddress(over_street1.Text, over_building_number1.Text); 
        }

        private void over_building_number2_TextChanged(object sender, EventArgs e) {
            coord_over_2.Text = doSearchAddress(over_street2.Text, over_building_number2.Text);
        }

        private void use_sql_CheckStateChanged(object sender, EventArgs e) {
            timer1.Enabled = use_sql.Checked;
            mapControl1.MarkersCars.Clear();
            mapControl1.Invalidate();
        }

        private void очиститьТочку1ToolStripMenuItem_Click(object sender, EventArgs e) {
            calculated_path.setPoint(1, 0,0,"");
            coord_over_1.Text = "";
        }

        private void очиститьПромежуточнуюТочку2ToolStripMenuItem_Click(object sender, EventArgs e) {
            calculated_path.setPoint(2, 0, 0, "");
            coord_over_2.Text = "";
        }

        private void btnSave_Click(object sender, EventArgs e) {
            //записать в базу
            //выключить кнопку пока не поменяются данные откуда куда
            btnSave.Enabled = false;


            string connString = "Data Source=192.168.100.57\\TAXA;Database=TAXA;Uid=taxausr;Pwd=654321;";

            //string strSQL = "select * from auth_table where login ='" + log + "' AND pass ='" + pass + "'";

            if (m_conn == null) {
                m_conn = new SqlConnection(connString);
                m_conn.Open();
            }

            SqlCommand cmd = m_conn.CreateCommand();
            cmd.CommandText = "insert into orders(phone, connectPhone, addrFrom, houseFrom, AddrFromName, "+
"AddrTo, zoneId, ServiceID, dtArrive, predvar, state, agreement, resultCode, "+
"request_attributes, "+
"clientID, OpId) "+
"values(@phone, @connectPhone, @addrFrom,@houseFrom,@AddrFromName, " +
"@addrto, @zoneID, 0, getdate(), 0, 0,0,@ServiceID, " +
" @request_attributes," +
"1, 129)";
            cmd.Parameters.AddWithValue("@phone", phone.Text);
            cmd.Parameters.AddWithValue("@connectPhone", connectPhone.Text);
            cmd.Parameters.AddWithValue("@addrFrom", from_street.Text);
            cmd.Parameters.AddWithValue("@houseFrom", from_building_number.Text);
            cmd.Parameters.AddWithValue("@AddrFromName", "");
            cmd.Parameters.AddWithValue("@addrto", to_street.Text);
            //cmd.Parameters.AddWithValue("@houseto", to_building_number.Text);
            cmd.Parameters.AddWithValue("@zoneID", 9);
            
            if (service_name.SelectedIndex==0)
                cmd.Parameters.AddWithValue("@ServiceID", 1);
            else if (service_name.SelectedIndex==1)
                cmd.Parameters.AddWithValue("@ServiceID", 3);
            else cmd.Parameters.AddWithValue("@ServiceID", 1);


            cmd.Parameters.AddWithValue("@request_attributes", "423535");


            cmd.ExecuteNonQuery();
            


        }

        private SqlConnection mssqlGetConnection()
        {
            if (m_conn == null)
            {
                string connString = "Data Source=192.168.100.57\\TAXA;Database=TAXA;Uid=taxausr;Pwd=654321;";

                m_conn = new SqlConnection(connString);
                m_conn.Open();
            }
            return m_conn;
        }

        private FirebirdSql.Data.Firebird.FbConnection fbGetConnection()
        {
            if (m_fbConn == null)
            {
                string connString = @"Server=localhost;User=SYSDBA;Password=masterkey;Database=c:\projects\OSM_CLIENT\database\TAXI.FDB";
                m_fbConn = new FirebirdSql.Data.Firebird.FbConnection(connString);
                System.Diagnostics.Trace.Listeners.Clear();
                



                m_fbConn.Open();
           
            }
            return m_fbConn;
        }

        private int fbInsert(String table, String[] fields, object[] values)
        {
            if (fields.Length != values.Length)
                return -1;
            String field_names = "";
            String value_names = "";

            for (int i = 0; i < fields.Length; i++)
            {
                if (i != 0)
                {
                    field_names += ", ";
                    value_names += ", ";
                }

                field_names += fields[i];
                value_names += "@" + fields[i];
            }

            String sql = "INSERT INTO " + table + "(" + field_names + ") values (" + value_names + ")";


            FirebirdSql.Data.Firebird.FbCommand cmd = fbGetConnection().CreateCommand();
            cmd.CommandText = sql;

            for (int i = 0; i < fields.Length; i++)
            {
                cmd.Parameters.Add("@" + fields[i], values[i]);
            }
            cmd.ExecuteNonQuery();

            return 0;
        }

        private int fbUpdate(String table, String[] fields, object[] values, String key_field, object key_value)
        {
            if (fields.Length != values.Length)
                return -1;
            String fields_values = "";
            

            for (int i = 0; i < fields.Length; i++)
            {
                if (i != 0)
                {
                    fields_values += ", ";
                    
                }

                fields_values += fields[i] + "=@" + fields[i];
                
            }

            String sql = "UPDATE " + table + " SET " + fields_values + " WHERE " + key_field + " = @" + key_field;


            FirebirdSql.Data.Firebird.FbCommand cmd = fbGetConnection().CreateCommand();
            cmd.CommandText = sql;

            for (int i = 0; i < fields.Length; i++)
            {
                cmd.Parameters.Add("@" + fields[i], values[i]);
            }

            cmd.Parameters.Add("@" + key_field, key_value);
            cmd.ExecuteNonQuery();

            return 0;
        }

        private System.Data.DataTable fbRead(String query)
        {
            FirebirdSql.Data.Firebird.FbCommand cmd = fbGetConnection().CreateCommand();
            
            cmd.CommandText = query;
            FirebirdSql.Data.Firebird.FbDataReader dataReader = cmd.ExecuteReader();
            System.Data.DataTable dt = new DataTable();
            dt.Load(dataReader);
            
            dataReader.Close();
            return dt;
        }




        private int mssqlInsert(String table, String[] fields, object[] values)
        {
            if (fields.Length != values.Length)
                return -1;
            String field_names = "";
            String value_names = "";

            for (int i = 0; i < fields.Length; i++)
            {
                if (i != 0)
                {
                    field_names += ", ";
                    value_names += ", ";
                }
                
                field_names += fields[i];
                value_names += "@"+fields[i];
            }

            String sql = "INSERT INTO " + table + "(" + field_names + ") values (" + value_names + ")";


            SqlCommand cmd = mssqlGetConnection().CreateCommand();
            cmd.CommandText = sql;
            
            for (int i = 0; i < fields.Length; i++)
            {
                cmd.Parameters.AddWithValue("@"+fields[i], values[i]);
            }
            cmd.ExecuteNonQuery();
            
            return 0;
        }


        private void from_building_number_KeyPress(object sender, KeyPressEventArgs e) {

        }

        private void button3_Click_1(object sender, EventArgs e)
        {

        }

        private void tabPage7_Click(object sender, EventArgs e)
        {

        }

        private void dateTimePicker4_ValueChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker3_ValueChanged(object sender, EventArgs e)
        {

        }

        private void groupBox5_Enter(object sender, EventArgs e)
        {

        }

        private void btnSave_Click_1(object sender, EventArgs e)
        {

        }

        private void button12_Click(object sender, EventArgs e)
        {
            LogCommand();
            String[] fields = { "msg" };
            
            object [] values = {"asdfasd gfasgf"};
            
            fbInsert("LOG", fields, values);
        }

        private void button7_Click(object sender, EventArgs e)
        {

    
            int ms_start = DateTime.Now.Second*1000+ DateTime.Now.Millisecond;

            DataTable dt =  fbRead("SELECT first  300 * FROM ORD");

            //dataSet1.Tables["ord"].Merge(dt);



            
            

            DataTable dt1 = new DataTable();
            dt1.Columns.Add("ORD_ID");
            dt1.Columns.Add("KLN_ADDRESS");
            dt1.Columns.Add("DRIVER_NUMBER");
            dt1.Columns.Add("CAR_NAME");
            dt1.Columns.Add("DRIVER_FIO");
            dt1.Columns.Add("ORD_DATE");
            
            dt1.Merge(dt, false, MissingSchemaAction.Ignore);
            dt1.Columns["KLN_ADDRESS"].Caption = "адрес";

            int first = 0;
            int first_id = 0;
            int selected = 0;
            int selected_id = 0;

            int delta = 0;

            if (dataGridView1.Rows.Count>0){

                first = dataGridView1.Rows.GetFirstRow(DataGridViewElementStates.Displayed);
                if (first >= 0)
                {
                    first_id = int.Parse(dataGridView1.Rows[first].Cells[0].Value.ToString());

                    selected = dataGridView1.Rows.GetFirstRow(DataGridViewElementStates.Selected);
                    selected_id = int.Parse(dataGridView1.Rows[selected].Cells[0].Value.ToString());

                    delta = selected - first;
                }

                

            }

            String sort_name = "";
            if (dataGridView1.SortedColumn != null)
                sort_name = dataGridView1.SortedColumn.Name;
            System.Windows.Forms.SortOrder so = dataGridView1.SortOrder;
            
            
            

            
            dataGridView1.DataSource = dt1;

            ListSortDirection sd = ListSortDirection.Ascending;
            if (so == System.Windows.Forms.SortOrder.Ascending)
            {
                dataGridView1.Sort(dataGridView1.Columns[sort_name], ListSortDirection.Ascending);
            }
            if (so == System.Windows.Forms.SortOrder.Descending)
            {
                dataGridView1.Sort(dataGridView1.Columns[sort_name], ListSortDirection.Descending);
            }

            
            
            

            if (selected_id > 0)
            {
                int i = 0;
                for (i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    if (int.Parse(dataGridView1.Rows[i].Cells[0].Value.ToString()) == selected_id)
                    {
                        break;
                    }
                }
                if (i < dataGridView1.Rows.Count)
                    selected = i;  //иначе - та же позиция что и до обновления.  

                    
            }
            dataGridView1.CurrentCell = dataGridView1.Rows[selected].Cells[0];

            if (selected - delta >= 0)
            {
                dataGridView1.FirstDisplayedScrollingRowIndex = selected - delta;
            }
            else
            {
                dataGridView1.FirstDisplayedScrollingRowIndex = 0;
            }









            dataGridView1.Columns["KLN_ADDRESS"].HeaderText = "аДрес";
            dataGridView1.Columns["KLN_ADDRESS"].Width = 300;
            


            System.Console.WriteLine("Время выборки " + (DateTime.Now.Second * 1000 + DateTime.Now.Millisecond - ms_start).ToString() + "мсек");
            ms_start = DateTime.Now.Second * 1000 + DateTime.Now.Millisecond;
            DataRow [] dr =  dt.Select();

            System.Console.WriteLine("Время получения массива строк " + (DateTime.Now.Second * 1000 + DateTime.Now.Millisecond - ms_start).ToString() + "мсек");
            ms_start = DateTime.Now.Second * 1000 + DateTime.Now.Millisecond;
            int n = 0;

            foreach( DataRow r in dr){
                object [] items = r.ItemArray;

                foreach (object o in items)
                {
                    n++;
                    //System.Console.WriteLine(o.ToString());
                }
            }

            foreach (DataRow r in dr)
            {
                object[] items = r.ItemArray;

                foreach (object o in items)
                {
                    n++;
                    //System.Console.WriteLine(o.ToString());
                }
            }

            System.Console.WriteLine(n.ToString()+". Время обработки " + (DateTime.Now.Second * 1000 + DateTime.Now.Millisecond - ms_start).ToString() + "мсек");

        }


        [Conditional("TRACE")]
        private void LogCommand()
        {
            System.Console.WriteLine("MY TRACE");
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
 button7_Click(null, null);
        }

        private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (!e.RowIndex.Equals(-1) && !e.ColumnIndex.Equals(-1) && e.Button.Equals(MouseButtons.Right))
            {
                dataGridView1.CurrentCell = dataGridView1[e.ColumnIndex, e.RowIndex];
                dataGridView1.CurrentRow.Selected = true;
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
           

            
        }

        private void button14_Click(object sender, EventArgs e)
        {
            //init
            // local ip
            //remote ip
            //local sip port
            // sip user id
            //sip user password

            caller.InitCaller("192.168.0.24", 7666);
            caller.registerCaller("1200", "", "192.168.0.1");
            
          
        }

      

        private void btnCallDial_Click(object sender, EventArgs e)
        {
            caller.doDial("1200");
        }

        private void callBtnDial_Click(object sender, EventArgs e)
        {
            caller.doDial(callPhone.Text);


        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            caller.SipClose();
        }

        private void callBtnHangup_Click(object sender, EventArgs e)
        {
            caller.doHangup();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            int bufferSize = 320; // (8000 / (1000 / 320)) * (16 / 8);

            _WaveIn m_pWaveIn = new _WaveIn(AudioIn.Devices[0], 8000, 16, 1, bufferSize);
            m_pWaveIn.AudioFrameReceived += new EventHandler<EventArgs<byte[]>>(m_pWaveIn_AudioFrameReceived);
            m_pWaveIn.Start();

            
            
            

            
            
        }


        private void m_pWaveIn_AudioFrameReceived(object sender, EventArgs<byte[]> e)
        {
            double gain = 1.0;
            if (numericUpDown1.Value != 0)
            {
                gain = 1 + (double)numericUpDown1.Value / 100.0;
            }

            
                        System.Drawing.Graphics g = this.pictureBox1.CreateGraphics();
                        System.Drawing.Point [] points = new System.Drawing.Point[e.Value.Length/2];

                        int offset = 0;
                        byte[] retVal = new byte[e.Value.Length / 2];
                        while (offset < e.Value.Length-2)
                        {
                            // Little-Endian - lower byte,higer byte.
                            short pcm = (short)(e.Value[offset + 1] << 8 | e.Value[offset]);

                            pcm = (short)(gain*pcm);


                            points[offset / 2].X = ((int)(offset / 2 * 1.0 * pictureBox1.Width / (e.Value.Length / 2)));

                            byte low = (byte)(pcm & 255);
                            byte hi = (byte)(pcm >> 8);


                            e.Value[offset] = low;
                            e.Value[offset+1] = hi;

                            points[offset / 2].Y = (pictureBox1.Height / 2 - (int)(pcm * pictureBox1.Height / 65535.0));
                            offset += 2;
                        }




                        g.FillRectangle(System.Drawing.Brushes.White, 0, 0, pictureBox1.Width, pictureBox1.Height);

                        g.DrawLines(System.Drawing.Pens.Blue, points);

                        audioout.Write(e.Value, 0, e.Value.Length);

                      //  for (int i = 0; i < points.Length/2; i+=2)
                        //{
            //                g.DrawLine(System.Drawing.Pens.Blue, points[i], points[i + 2]);
                        //}
            
            try
            {
               
            }
            catch (Exception x)
            {
                if (!this.IsDisposed)
                {
                    // Raise error event(We can't throw expection directly, we are on threadpool thread).
                    //OnError(x);
                }
            }
                        g.Dispose();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            caller.doShowSettings();
        }


        #region Properties implementation

        #endregion


    }
}

