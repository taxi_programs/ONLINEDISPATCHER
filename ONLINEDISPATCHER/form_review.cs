﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ONLINEDISPATCHER
{
    public partial class form_review : Form
    {
        public int m_iID = 0;
        private int selected_car_id = 0;
        private int selected_driver_id = 0;
        
        DataTable cars = null;

        private bool save = false;
        
        public form_review()
        {
            InitializeComponent();


            cars = DataBase.mssqlRead("SELECT CARS.ID, cast (CARS.ID as varchar(6))+' '+  CARS.COLOR +' '+  CARS.mark +' '+ cars.gosnum +' '+drivers.fio +' '+ drivers.phones CAR_DRIVER_DESC  FROM CARS, DRIVERS WHERE CARS.ID = DRIVERS.CARID order by CARS.ID");
            


            //    ServiceID.Items.Clear();
            carid.DataSource = cars;
            carid.DisplayMember = "CAR_DRIVER_DESC";
            carid.ValueMember = "ID";
            carid.DropDownStyle = ComboBoxStyle.DropDown;
            carid.AutoCompleteSource = AutoCompleteSource.ListItems;
            //  ZoneID.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            //comboCars.SelectedIndex = -1;
            carid.AutoCompleteMode = AutoCompleteMode.SuggestAppend;

            carid.SelectedValue = selected_car_id.ToString();

        }

        private void form_review_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (save != true) {

                if (chk_close_not_saved.Checked == false) {
                    e.Cancel = true;
                    return;
                }
            }
            
        }


        public void doLoad(int id) {

            m_iID = id;

            DataTable dt = DataBase.mssqlRead("SELECT ID, STATUS, CLN_FIO,  PHONE, MESSAGE, TM, CARID, CAR_DESCR, DRIVER_FIO, (SELECT OPERATORS.FIO FROM OPERATORS WHERE OPERATORS.ID = REVIEW.OPID) op_fio FROM REVIEW WHERE ID = " + m_iID.ToString());
            if (dt.Rows.Count>0){
                this.carid.SelectedValue = dt.Rows[0]["CARID"];
                this.car_descr.Text = dt.Rows[0]["CAR_DESCR"].ToString();
                this.driver_fio.Text = dt.Rows[0]["DRIVER_FIO"].ToString();
                this.cln_fio.Text = dt.Rows[0]["cln_fio"].ToString();
                this.phone.Text = dt.Rows[0]["phone"].ToString();
                this.message.Text = dt.Rows[0]["message"].ToString();
                this.op_fio.Text = dt.Rows[0]["op_fio"].ToString();
                this.tm.Text = dt.Rows[0]["tm"].ToString();
                this.line.Text = "";//dt.Rows[0]["message"].ToString();

                this.status.SelectedIndex = (int)dt.Rows[0]["status"];



            }

        }


        private void button1_Click(object sender, EventArgs e)
        {
            if (this.m_iID > 0) {
                    String[] fields = { "phone", "message",  "status", "carid",
                                      "driverid","driver_fio","car_descr","cln_fio"};



                    object[] values = {phone.Text, message.Text, status.SelectedIndex, selected_car_id,
                                      selected_driver_id, this.driver_fio.Text, this.car_descr.Text, this.cln_fio.Text};

                    //DataBase.fbInsert("LOG", fields, values);
                    DataBase.mssqlUpdate("REVIEW", fields, values, "ID", m_iID);

                }
                if (this.m_iID == 0) {   //новый заказ 
                    String[] fields = { "phone", "message", "opid", "status", "carid",
                                      "driverid","driver_fio","car_descr","cln_fio"};



                    object[] values = {phone.Text, message.Text, MAIN_FORM.OperatorID, status.SelectedIndex, selected_car_id,
                                      selected_driver_id, this.driver_fio.Text, this.car_descr.Text, this.cln_fio.Text};

                    //DataBase.fbInsert("LOG", fields, values);
                    DataBase.mssqlInsert("REVIEW", fields, values);

                
                  

                }

                save = true;
                Close();

            

        }

        
    
        private void carid_SelectedIndexChanged(object sender, EventArgs e)
        {
            selected_car_id = 0;
             if (carid.SelectedValue != null) {
                try {
                    //int s = (String)comboCars.SelectedValue;
                    //System.Console.WriteLine(s);
                    int icarid = (int)carid.SelectedValue;
                    selected_car_id = icarid;
                    DataTable carinfo = DataBase.mssqlRead("SELECT CARS.ID, cast (CARS.ID as varchar(6))+' '+  CARS.COLOR +' '+  CARS.mark +' '+ cars.gosnum CAR_DESC, drivers.fio +' '+ drivers.phones DRIVER_DESC, drivers.id driver_id, cars.serviceid car_service_id FROM CARS left join DRIVERS on DRIVERS.ID = CARS.DRIVERID WHERE CARS.ID = " + icarid.ToString());
                    
                    selected_driver_id = (int)carinfo.Rows[0]["driver_id"];
                    String car_descr = (String)carinfo.Rows[0]["CAR_DESC"];
                    String driver_descr = (String)carinfo.Rows[0]["DRIVER_DESC"];

                    this.car_descr.Text = car_descr;
                    this.driver_fio.Text = driver_descr;

                } catch (Exception ex) {

                }
            }
        }

        private void btnMakeOrder_Click(object sender, EventArgs e) {
            order_big o = new order_big();
            o.Text = "";
            o.doLoad(0, this.phone.Text, this.line.Text, "");
            o.Show();
        }
         
        }

  


}

