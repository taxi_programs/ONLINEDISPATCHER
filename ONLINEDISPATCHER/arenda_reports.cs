﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ONLINEDISPATCHER
{
    public partial class arenda_reports : Form
    {

        private int arenda_reg_sum = 0;
        private int kassa_arenda_drivers_sum;
        private int kassa_all_drivers_sum;
        //private int arenda_reg_sum = 0;
        
        public arenda_reports()
        {
            InitializeComponent();
        }

        private void arenda_reports_Load(object sender, EventArgs e)
        {
            dateTimePicker1.Value = DateTime.Now.Date;
            dateTimePicker2.Value = dateTimePicker1.Value;


            DataTable drivers_only = DataBase.mssqlRead("select -1 ID, '<Любой водитель>' DRIVER_DESC union SELECT drivers.id ID, cast(cars.id as varchar(10))+' '+cars.color+ ' ' +cars.mark + ' ' + cars.gosnum +' '+drivers.fio +' '+ drivers.phones DRIVER_DESC  FROM CARS, DRIVERS WHERE DRIVERS.ID = CARS.DRIVERID and cars.arenda_car_id>0 order by 1");






            // DataTable cars_only = DataBase.mssqlRead("SELECT CARS.ID, cast (CARS.ID as varchar(6))+' '+  CARS.COLOR +' '+  CARS.mark +' '+ cars.gosnum  CAR_DESC  FROM CARS order by CARS.ID");
            DataTable arenda_cars = DataBase.mssqlRead("select -1 ID, '<Любая машина>' CAR_DESC union SELECT ID, ARENDA_CARS.COLOR +' '+  ARENDA_CARS.mark +' '+ ARENDA_cars.gosnum  CAR_DESC  FROM ARENDA_CARS order by ID");
            //DataTable drivers_only = DataBase.mssqlRead("SELECT DRIVERS.ID, drivers.fio +' '+ drivers.phones DRIVER_DESC  FROM DRIVERS order by DRIVERS.FIO");


            cmbArendaCars.DisplayMember = "CAR_DESC";
            cmbArendaCars.ValueMember = "ID";
            cmbArendaCars.DropDownStyle = ComboBoxStyle.DropDown;
            cmbArendaCars.AutoCompleteSource = AutoCompleteSource.ListItems;

            cmbArendaCars.MatchingMethod = SergeUtils.StringMatchingMethod.UseRegexs;
            cmbArendaCars.Text = "";
            cmbArendaCars.DataSource = arenda_cars;



            cmbDrivers.DisplayMember = "DRIVER_DESC";
            cmbDrivers.ValueMember = "ID";
            cmbDrivers.DropDownStyle = ComboBoxStyle.DropDown;
            cmbDrivers.AutoCompleteSource = AutoCompleteSource.ListItems;

            cmbDrivers.MatchingMethod = SergeUtils.StringMatchingMethod.UseRegexs;
            cmbDrivers.Text = "";
            cmbDrivers.DataSource = drivers_only;

            cmbArendaCars.SelectedItem = null;
            cmbDrivers.SelectedItem = null;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataSet1.Tables["report_data"].Clear();
            /*
            dataSet1.Tables["report_params"].Clear();

            object[] values = { "Заголовок", dateTimePicker1.Value, dateTimePicker2.Value};
            dataSet1.Tables["report_params"].LoadDataRow(values, true);

            */
            List<SqlParameter> parameters = new List<SqlParameter>();

            
             
            parameters.Add(new SqlParameter("dt1", dateTimePicker1.Value.Date));
            parameters.Add(new SqlParameter("dt2", dateTimePicker2.Value.Date));
            parameters.Add(new SqlParameter("driver_id", cmbDrivers.SelectedValue==null?0:(int)cmbDrivers.SelectedValue));
            parameters.Add(new SqlParameter("arenda_car_id", cmbArendaCars.SelectedValue == null ? 0 : (int)cmbArendaCars.SelectedValue));
            parameters.Add(new SqlParameter("interval", "day"));


            

            DataTable dt = DataBase.mssqlRead("select * from arenda_show_report(@driver_id, @arenda_car_id, @dt1, @dt2, @interval) order by dt", parameters.ToArray());

            //foreach (DataRow row in dt.Select("", "dtEnd")) {

            arenda_reg_sum = 0;
            kassa_arenda_drivers_sum=0;
            kassa_all_drivers_sum=0;
            foreach (DataRow row in dt.Rows)
            {
                dataSet1.Tables["report_data"].ImportRow(row);
                
                if (row["driverFIO"].ToString().CompareTo("ИТОГО") != 0)
                {
                    arenda_reg_sum              += row["arenda_reg_sum"] == DBNull.Value ? 0 : (int)row["arenda_reg_sum"];
                }
                if (row["driverFIO"].ToString().CompareTo("ИТОГО") == 0)
                {
                    kassa_arenda_drivers_sum    += (row["kassa_arenda_drivers_sum"] == DBNull.Value) ? 0 : (int)(decimal)row["kassa_arenda_drivers_sum"];
                    kassa_all_drivers_sum       += (row["kassa_all_drivers_sum"] == DBNull.Value) ? 0 : (int)(decimal)row["kassa_all_drivers_sum"];
                }
            }


           // dataGridView1.Columns["payers"].Visible = false;


            
            label_info.Text="Сумма оплат "+arenda_reg_sum.ToString()+"\n"+
                "Наличка арендники " + kassa_arenda_drivers_sum+"\n"+
                "Наличка частники " + (kassa_all_drivers_sum - kassa_arenda_drivers_sum).ToString() + "\n" +
                "Наличка всего " + kassa_all_drivers_sum+"\n"+
                "БЕЗНАЛ " + (arenda_reg_sum - kassa_arenda_drivers_sum).ToString() + " \nв БНАЛ я запутался какая тут должна \nстоять цифра\n";  

            //dataGridView2.DataSource = dt;

            //dataSet1.Tables["report_data"].Load(dt.CreateDataReader());




            //report1.RegisterData(dataSet1.Tables["report_data"], "Table1");
            //report1.Show();

            //dataSet1.Tables["report_orders"].Load(orders_table.CreateDataReader());

            //report1.RegisterData(dt, "Table1");
            //report1.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //payed_cars
        }

        private void button2_Click(object sender, EventArgs e)
        {
             dataSet1.Tables["report_data2"].Clear();
            /*
            dataSet1.Tables["report_params"].Clear();

            object[] values = { "Заголовок", dateTimePicker1.Value, dateTimePicker2.Value};
            dataSet1.Tables["report_params"].LoadDataRow(values, true);

            */
            List<SqlParameter> parameters = new List<SqlParameter>();

            
             
            parameters.Add(new SqlParameter("dt1", dateTimePicker1.Value.Date));
            parameters.Add(new SqlParameter("dt2", dateTimePicker2.Value.Date));
            parameters.Add(new SqlParameter("driver_id", cmbDrivers.SelectedValue==null?0:(int)cmbDrivers.SelectedValue));
            parameters.Add(new SqlParameter("arenda_car_id", cmbArendaCars.SelectedValue == null ? 0 : (int)cmbArendaCars.SelectedValue));





            DataTable dt = DataBase.mssqlRead("select * from arenda_show_report_2(@driver_id, @arenda_car_id, @dt1, @dt2) order by car_gosnum, dt", parameters.ToArray());

            //foreach (DataRow row in dt.Select("", "dtEnd")) {

            arenda_reg_sum = 0;
            kassa_arenda_drivers_sum=0;
            kassa_all_drivers_sum=0;
            foreach (DataRow row in dt.Rows)
            {
                dataSet1.Tables["report_data2"].ImportRow(row);
            }



//            report1.RegisterData(dt, "report_data3");
//            report1.GetDataSource("report_data3").Enabled = true;

//            ((FastReport.DataBand)report1.Report.FindObject("Data1")).DataSource = report1.Report.GetDataSource("report_data3");
//            report1.Prepare();

            object[] values = { "Подробный отчет об оплатах за машины", dateTimePicker1.Value, dateTimePicker2.Value };
            dataSet1.Tables["report_params"].LoadDataRow(values, true);
            

            report1.Show();
            //.dataGridView1.dataGridView1.DataSource = dataSet1.Tables["report_data2"];
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            dataSet1.Tables["report_kassa_data"].Clear();
           
            List<SqlParameter> parameters = new List<SqlParameter>();



            parameters.Add(new SqlParameter("dt1", dateTimePicker1.Value.Date));
            parameters.Add(new SqlParameter("dt2", dateTimePicker2.Value.Date));
            parameters.Add(new SqlParameter("driver_id", cmbDrivers.SelectedValue == null ? 0 : (int)cmbDrivers.SelectedValue));

            





            DataTable dt = DataBase.mssqlRead("select * from arenda_report_money(@dt1, @dt2, @driver_id,0)", parameters.ToArray());

           
            foreach (DataRow row in dt.Rows)
            {
                dataSet1.Tables["report_kassa_data"].ImportRow(row);
            }

            object[] values = { "Подробный отчет об движении по кассе", dateTimePicker1.Value, dateTimePicker2.Value };
            dataSet1.Tables["report_params"].LoadDataRow(values, true);


            report2.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            dataSet1.Tables["report_kassa_data"].Clear();

            List<SqlParameter> parameters = new List<SqlParameter>();



            parameters.Add(new SqlParameter("dt1", dateTimePicker1.Value.Date));
            parameters.Add(new SqlParameter("dt2", dateTimePicker2.Value.Date));
            parameters.Add(new SqlParameter("driver_id", cmbDrivers.SelectedValue == null ? 0 : (int)cmbDrivers.SelectedValue));







            DataTable dt = DataBase.mssqlRead("select * from arenda_report_money(@dt1, @dt2, @driver_id,1)", parameters.ToArray());


            foreach (DataRow row in dt.Rows)
            {
                dataSet1.Tables["report_kassa_data"].ImportRow(row);
            }

            object[] values = { "Подробный отчет об движении по кассе", dateTimePicker1.Value, dateTimePicker2.Value };
            dataSet1.Tables["report_params"].LoadDataRow(values, true);


            report2.Show();
        }

    }
}
