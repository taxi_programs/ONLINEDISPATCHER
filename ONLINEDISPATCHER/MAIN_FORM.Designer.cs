﻿namespace ONLINEDISPATCHER
{
    partial class MAIN_FORM
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MAIN_FORM));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label_pwo_timer = new System.Windows.Forms.Label();
            this.sip_status_combo = new System.Windows.Forms.ComboBox();
            this.cmd_filter_orders = new System.Windows.Forms.ComboBox();
            this.leftTab = new System.Windows.Forms.TabControl();
            this.tabOrders = new System.Windows.Forms.TabPage();
            this.view_work_orders = new System.Windows.Forms.DataGridView();
            this.mnuViewWorkOrders = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miOrderEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.завершитьЗаказToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miEndOrder1 = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuEndOrder2 = new System.Windows.Forms.ToolStripMenuItem();
            this.miEndOrder3 = new System.Windows.Forms.ToolStripMenuItem();
            this.нетМашинToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.снятToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.miNewOrder = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.звлнкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDialCar = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuDialClient = new System.Windows.Forms.ToolStripMenuItem();
            this.администрированиеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.дублироватьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.закрытьИДублироватьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.положитьСуммуПоЗаказуВодителюToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem14 = new System.Windows.Forms.ToolStripSeparator();
            this.вЧерныйСписокToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem15 = new System.Windows.Forms.ToolStripSeparator();
            this.закрытьЧерныйСписокToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem16 = new System.Windows.Forms.ToolStripSeparator();
            this.перевестиВУспешноЗавершеныеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.завершитьСНазначениемВодителяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem19 = new System.Windows.Forms.ToolStripSeparator();
            this.перевестиВзаказВРаботеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ВернутьЗаказВРаботу = new System.Windows.Forms.ToolStripMenuItem();
            this.полноеРедактированиеЗаказаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripSeparator();
            this.yjdsqToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.редактироватьНоваяФормаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.поторопитеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.снятьпоторопитеToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.поставитьпоторопитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.снятьпоторопитеИОтправитьВодителюторопимToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.снятьпоторопитеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.таксометрToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.включитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выключитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.назначениеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.назначитьВодителяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.снятьВодителяСЗаказаToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.предложитьЗаказВодителюОкошкомToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.попиликатьУВодителяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel3 = new System.Windows.Forms.Panel();
            this.chkFreeOrders = new System.Windows.Forms.CheckBox();
            this.FFilterPos = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.fFilterAddrPhone = new System.Windows.Forms.TextBox();
            this.chkMyOrders = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.filterPanel = new System.Windows.Forms.Panel();
            this.button16 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.bnal_opl_tm = new System.Windows.Forms.DateTimePicker();
            this.button14 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.chkUnclosedBnal = new System.Windows.Forms.CheckBox();
            this.chkBnal = new System.Windows.Forms.CheckBox();
            this.Успешные = new System.Windows.Forms.CheckBox();
            this.btn_print = new System.Windows.Forms.Button();
            this.Отмененные = new System.Windows.Forms.CheckBox();
            this.filterAddress = new System.Windows.Forms.TextBox();
            this.filterPhone = new System.Windows.Forms.TextBox();
            this.filterOrg = new System.Windows.Forms.ComboBox();
            this.filterCarService = new System.Windows.Forms.ComboBox();
            this.filterOrderService = new System.Windows.Forms.ComboBox();
            this.filterCar = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnRefreshOrders = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btn1Month = new System.Windows.Forms.Button();
            this.btn7Day = new System.Windows.Forms.Button();
            this.btn1Day = new System.Windows.Forms.Button();
            this.filterTm2 = new System.Windows.Forms.DateTimePicker();
            this.filterTm1 = new System.Windows.Forms.DateTimePicker();
            this.filterDt2 = new System.Windows.Forms.DateTimePicker();
            this.filterDt1 = new System.Windows.Forms.DateTimePicker();
            this.tabMap = new System.Windows.Forms.TabPage();
            this.mapControl1 = new csmapcontrol.MapControl();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.маршрутОтсюдаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.маршрутСюдаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.промежуточнаяТочка1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.промежуточнаяТочка2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.очиститьМаршрутToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.очиститьТочку1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.очиститьПромежуточнуюТочку2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.редактироватьРайонToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьТочкуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel2 = new System.Windows.Forms.Panel();
            this.сщхранитьгород = new System.Windows.Forms.Button();
            this.загрузитьгород = new System.Windows.Forms.Button();
            this.btnSaveZones = new System.Windows.Forms.Button();
            this.btnLoadZones = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.chkMapShowOrders = new System.Windows.Forms.CheckedListBox();
            this.path_info = new System.Windows.Forms.TextBox();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.btnReview = new System.Windows.Forms.Button();
            this.btn_driver_card = new System.Windows.Forms.Button();
            this.btnLoadGisModuleData = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnPromisedPayment = new System.Windows.Forms.Button();
            this.btnDisableAlarm = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button12 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tabControl4 = new System.Windows.Forms.TabControl();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.btnArendaCars = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.btnPhonesList = new System.Windows.Forms.Button();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.btnAktSverki = new System.Windows.Forms.Button();
            this.btn_dic_gis_addresses = new System.Windows.Forms.Button();
            this.buttoт_dic_organizations = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tabControl5 = new System.Windows.Forms.TabControl();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.button6 = new System.Windows.Forms.Button();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker4 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.button5 = new System.Windows.Forms.Button();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.rightTabControl = new System.Windows.Forms.TabControl();
            this.tabCars = new System.Windows.Forms.TabPage();
            this.view_cars = new System.Windows.Forms.DataGridView();
            this.mnuCars = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.позвонитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сообщениеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
            this.снятьДозвонToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.снятьТревогуToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripSeparator();
            this.поставитьпослеОтменыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.снятьпToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripSeparator();
            this.карточкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem17 = new System.Windows.Forms.ToolStripSeparator();
            this.загрузитьМаршрутToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem18 = new System.Windows.Forms.ToolStripSeparator();
            this.поставитьПервымПослеОтменыToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.view_raions = new System.Windows.Forms.DataGridView();
            this.tabCalls = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnCallDial = new System.Windows.Forms.Button();
            this.btnCallHangup = new System.Windows.Forms.Button();
            this.btnCallHold = new System.Windows.Forms.Button();
            this.listBox5 = new System.Windows.Forms.ListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.panCommands = new System.Windows.Forms.Panel();
            this.txtStatus = new System.Windows.Forms.Label();
            this.button17 = new System.Windows.Forms.Button();
            this.SkippedCalls = new System.Windows.Forms.Label();
            this.callBtnHold = new System.Windows.Forms.Button();
            this.statusS = new System.Windows.Forms.StatusStrip();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.statusbar = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.callBtnHangup = new System.Windows.Forms.Button();
            this.btnRestartSip = new System.Windows.Forms.Button();
            this.callBtnDial = new System.Windows.Forms.Button();
            this.callPhone = new System.Windows.Forms.TextBox();
            this.btnSettings = new System.Windows.Forms.Button();
            this.time_label = new System.Windows.Forms.Label();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.post_order_timer = new System.Windows.Forms.Timer(this.components);
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker2 = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker3 = new System.ComponentModel.BackgroundWorker();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.dataSet1 = new System.Data.DataSet();
            this.report_orders = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dataColumn13 = new System.Data.DataColumn();
            this.ord_date = new System.Data.DataColumn();
            this.dataColumn14 = new System.Data.DataColumn();
            this.dataTable1 = new System.Data.DataTable();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dataColumn11 = new System.Data.DataColumn();
            this.dataColumn12 = new System.Data.DataColumn();
            this.report1 = new FastReport.Report();
            this.report2 = new FastReport.Report();
            this.report3 = new FastReport.Report();
            this.report4 = new FastReport.Report();
            this.report5 = new FastReport.Report();
            this.check_call_state = new System.Windows.Forms.Timer(this.components);
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.leftTab.SuspendLayout();
            this.tabOrders.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.view_work_orders)).BeginInit();
            this.mnuViewWorkOrders.SuspendLayout();
            this.panel3.SuspendLayout();
            this.filterPanel.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tabMap.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabControl4.SuspendLayout();
            this.tabPage9.SuspendLayout();
            this.tabPage10.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabControl5.SuspendLayout();
            this.tabPage11.SuspendLayout();
            this.rightTabControl.SuspendLayout();
            this.tabCars.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.view_cars)).BeginInit();
            this.mnuCars.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.view_raions)).BeginInit();
            this.tabCalls.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusS.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.report_orders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.report1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.report2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.report3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.report4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.report5)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.splitContainer1.Panel1.Controls.Add(this.label_pwo_timer);
            this.splitContainer1.Panel1.Controls.Add(this.sip_status_combo);
            this.splitContainer1.Panel1.Controls.Add(this.cmd_filter_orders);
            this.splitContainer1.Panel1.Controls.Add(this.leftTab);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.splitContainer1.Panel2.Controls.Add(this.rightTabControl);
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Size = new System.Drawing.Size(1122, 613);
            this.splitContainer1.SplitterDistance = 815;
            this.splitContainer1.TabIndex = 5;
            // 
            // label_pwo_timer
            // 
            this.label_pwo_timer.AutoSize = true;
            this.label_pwo_timer.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label_pwo_timer.Location = new System.Drawing.Point(577, 6);
            this.label_pwo_timer.Name = "label_pwo_timer";
            this.label_pwo_timer.Size = new System.Drawing.Size(0, 13);
            this.label_pwo_timer.TabIndex = 8;
            // 
            // sip_status_combo
            // 
            this.sip_status_combo.BackColor = System.Drawing.SystemColors.MenuBar;
            this.sip_status_combo.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.sip_status_combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sip_status_combo.ForeColor = System.Drawing.SystemColors.WindowText;
            this.sip_status_combo.FormattingEnabled = true;
            this.sip_status_combo.Items.AddRange(new object[] {
            "На смене",
            "Занят разговором",
            "Отошел"});
            this.sip_status_combo.Location = new System.Drawing.Point(455, 3);
            this.sip_status_combo.Name = "sip_status_combo";
            this.sip_status_combo.Size = new System.Drawing.Size(116, 21);
            this.sip_status_combo.TabIndex = 6;
            this.sip_status_combo.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.sip_status_combo_DrawItem);
            this.sip_status_combo.SelectedIndexChanged += new System.EventHandler(this.sip_status_combo_SelectedIndexChanged);
            // 
            // cmd_filter_orders
            // 
            this.cmd_filter_orders.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmd_filter_orders.FormattingEnabled = true;
            this.cmd_filter_orders.Items.AddRange(new object[] {
            "Заказы в работе",
            "Предварительные заказы",
            "Выполненые за 12 часов",
            "Архив"});
            this.cmd_filter_orders.Location = new System.Drawing.Point(201, 3);
            this.cmd_filter_orders.Name = "cmd_filter_orders";
            this.cmd_filter_orders.Size = new System.Drawing.Size(145, 21);
            this.cmd_filter_orders.TabIndex = 1;
            this.cmd_filter_orders.SelectedIndexChanged += new System.EventHandler(this.cmd_filter_orders_SelectedIndexChanged);
            // 
            // leftTab
            // 
            this.leftTab.Controls.Add(this.tabOrders);
            this.leftTab.Controls.Add(this.tabMap);
            this.leftTab.Controls.Add(this.tabPage8);
            this.leftTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leftTab.Location = new System.Drawing.Point(0, 0);
            this.leftTab.Margin = new System.Windows.Forms.Padding(0);
            this.leftTab.Name = "leftTab";
            this.leftTab.SelectedIndex = 0;
            this.leftTab.Size = new System.Drawing.Size(815, 613);
            this.leftTab.TabIndex = 7;
            // 
            // tabOrders
            // 
            this.tabOrders.Controls.Add(this.view_work_orders);
            this.tabOrders.Controls.Add(this.panel3);
            this.tabOrders.Controls.Add(this.filterPanel);
            this.tabOrders.Location = new System.Drawing.Point(4, 22);
            this.tabOrders.Margin = new System.Windows.Forms.Padding(0);
            this.tabOrders.Name = "tabOrders";
            this.tabOrders.Size = new System.Drawing.Size(807, 587);
            this.tabOrders.TabIndex = 1;
            this.tabOrders.Text = "Заказы";
            this.tabOrders.UseVisualStyleBackColor = true;
            // 
            // view_work_orders
            // 
            this.view_work_orders.AllowUserToAddRows = false;
            this.view_work_orders.AllowUserToDeleteRows = false;
            this.view_work_orders.AllowUserToResizeRows = false;
            this.view_work_orders.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
            this.view_work_orders.BackgroundColor = System.Drawing.SystemColors.Control;
            this.view_work_orders.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.view_work_orders.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.view_work_orders.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.view_work_orders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.view_work_orders.ContextMenuStrip = this.mnuViewWorkOrders;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.view_work_orders.DefaultCellStyle = dataGridViewCellStyle9;
            this.view_work_orders.Dock = System.Windows.Forms.DockStyle.Fill;
            this.view_work_orders.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.view_work_orders.GridColor = System.Drawing.SystemColors.HighlightText;
            this.view_work_orders.Location = new System.Drawing.Point(0, 187);
            this.view_work_orders.MultiSelect = false;
            this.view_work_orders.Name = "view_work_orders";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.view_work_orders.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.view_work_orders.RowHeadersVisible = false;
            this.view_work_orders.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.view_work_orders.RowTemplate.Height = 18;
            this.view_work_orders.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.view_work_orders.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.view_work_orders.Size = new System.Drawing.Size(807, 400);
            this.view_work_orders.TabIndex = 53;
            this.view_work_orders.MouseDown += new System.Windows.Forms.MouseEventHandler(this.view_work_orders_MouseDown);
            this.view_work_orders.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            this.view_work_orders.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseDown);
            this.view_work_orders.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.view_work_orders_CellFormatting);
            this.view_work_orders.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.view_work_orders_CellPainting);
            this.view_work_orders.ColumnWidthChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.view_work_orders_ColumnWidthChanged);
            this.view_work_orders.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // mnuViewWorkOrders
            // 
            this.mnuViewWorkOrders.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miOrderEdit,
            this.завершитьЗаказToolStripMenuItem,
            this.toolStripMenuItem1,
            this.miNewOrder,
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.звлнкиToolStripMenuItem,
            this.администрированиеToolStripMenuItem,
            this.toolStripMenuItem11,
            this.toolStripMenuItem12,
            this.toolStripMenuItem13,
            this.yjdsqToolStripMenuItem,
            this.редактироватьНоваяФормаToolStripMenuItem,
            this.поторопитеToolStripMenuItem,
            this.снятьпоторопитеToolStripMenuItem,
            this.таксометрToolStripMenuItem,
            this.назначениеToolStripMenuItem,
            this.попиликатьУВодителяToolStripMenuItem});
            this.mnuViewWorkOrders.Name = "mnuViewWorkOrders";
            this.mnuViewWorkOrders.Size = new System.Drawing.Size(231, 304);
            this.mnuViewWorkOrders.Opening += new System.ComponentModel.CancelEventHandler(this.mnuViewWorkOrders_Opening);
            // 
            // miOrderEdit
            // 
            this.miOrderEdit.Name = "miOrderEdit";
            this.miOrderEdit.Size = new System.Drawing.Size(230, 22);
            this.miOrderEdit.Text = "Редактировать";
            this.miOrderEdit.Click += new System.EventHandler(this.miOrderEdit_Click);
            // 
            // завершитьЗаказToolStripMenuItem
            // 
            this.завершитьЗаказToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miEndOrder1,
            this.mnuEndOrder2,
            this.miEndOrder3,
            this.нетМашинToolStripMenuItem,
            this.снятToolStripMenuItem});
            this.завершитьЗаказToolStripMenuItem.Name = "завершитьЗаказToolStripMenuItem";
            this.завершитьЗаказToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.завершитьЗаказToolStripMenuItem.Text = "завершить заказ";
            this.завершитьЗаказToolStripMenuItem.Click += new System.EventHandler(this.завершитьЗаказToolStripMenuItem_Click);
            // 
            // miEndOrder1
            // 
            this.miEndOrder1.Name = "miEndOrder1";
            this.miEndOrder1.Size = new System.Drawing.Size(203, 22);
            this.miEndOrder1.Text = "успешно";
            this.miEndOrder1.Click += new System.EventHandler(this.miEndOrder1_Click);
            // 
            // mnuEndOrder2
            // 
            this.mnuEndOrder2.Name = "mnuEndOrder2";
            this.mnuEndOrder2.Size = new System.Drawing.Size(203, 22);
            this.mnuEndOrder2.Text = "отказ клиента";
            this.mnuEndOrder2.Click += new System.EventHandler(this.mnuEndOrder2_Click);
            // 
            // miEndOrder3
            // 
            this.miEndOrder3.Name = "miEndOrder3";
            this.miEndOrder3.Size = new System.Drawing.Size(203, 22);
            this.miEndOrder3.Text = "водитель не повез";
            this.miEndOrder3.Click += new System.EventHandler(this.miEndOrder3_Click);
            // 
            // нетМашинToolStripMenuItem
            // 
            this.нетМашинToolStripMenuItem.Name = "нетМашинToolStripMenuItem";
            this.нетМашинToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.нетМашинToolStripMenuItem.Text = "Нет машин";
            this.нетМашинToolStripMenuItem.Visible = false;
            this.нетМашинToolStripMenuItem.Click += new System.EventHandler(this.нетМашинToolStripMenuItem_Click);
            // 
            // снятToolStripMenuItem
            // 
            this.снятToolStripMenuItem.Name = "снятToolStripMenuItem";
            this.снятToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.снятToolStripMenuItem.Text = "Снят (отказ пассажира)";
            this.снятToolStripMenuItem.Click += new System.EventHandler(this.снятToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(227, 6);
            // 
            // miNewOrder
            // 
            this.miNewOrder.Name = "miNewOrder";
            this.miNewOrder.ShortcutKeys = System.Windows.Forms.Keys.Insert;
            this.miNewOrder.Size = new System.Drawing.Size(230, 22);
            this.miNewOrder.Text = "Новый заказ";
            this.miNewOrder.Click += new System.EventHandler(this.miNewOrder_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(227, 6);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(227, 6);
            // 
            // звлнкиToolStripMenuItem
            // 
            this.звлнкиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuDialCar,
            this.toolStripMenuItem4,
            this.mnuDialClient});
            this.звлнкиToolStripMenuItem.Name = "звлнкиToolStripMenuItem";
            this.звлнкиToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.звлнкиToolStripMenuItem.Text = "Звонки";
            // 
            // mnuDialCar
            // 
            this.mnuDialCar.Name = "mnuDialCar";
            this.mnuDialCar.Size = new System.Drawing.Size(171, 22);
            this.mnuDialCar.Text = "набрать водителя";
            this.mnuDialCar.Click += new System.EventHandler(this.mnuDialCar_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(168, 6);
            // 
            // mnuDialClient
            // 
            this.mnuDialClient.Name = "mnuDialClient";
            this.mnuDialClient.Size = new System.Drawing.Size(171, 22);
            this.mnuDialClient.Text = "набрать клиента";
            this.mnuDialClient.Click += new System.EventHandler(this.mnuDialClient_Click);
            // 
            // администрированиеToolStripMenuItem
            // 
            this.администрированиеToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.дублироватьToolStripMenuItem,
            this.toolStripMenuItem6,
            this.закрытьИДублироватьToolStripMenuItem,
            this.toolStripMenuItem7,
            this.положитьСуммуПоЗаказуВодителюToolStripMenuItem,
            this.toolStripMenuItem14,
            this.вЧерныйСписокToolStripMenuItem,
            this.toolStripMenuItem15,
            this.закрытьЧерныйСписокToolStripMenuItem,
            this.toolStripMenuItem16,
            this.перевестиВУспешноЗавершеныеToolStripMenuItem,
            this.завершитьСНазначениемВодителяToolStripMenuItem,
            this.toolStripMenuItem19,
            this.перевестиВзаказВРаботеToolStripMenuItem,
            this.ВернутьЗаказВРаботу,
            this.полноеРедактированиеЗаказаToolStripMenuItem});
            this.администрированиеToolStripMenuItem.Name = "администрированиеToolStripMenuItem";
            this.администрированиеToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.администрированиеToolStripMenuItem.Text = "Администрирование";
            // 
            // дублироватьToolStripMenuItem
            // 
            this.дублироватьToolStripMenuItem.Name = "дублироватьToolStripMenuItem";
            this.дублироватьToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.дублироватьToolStripMenuItem.Text = "Дублировать";
            this.дублироватьToolStripMenuItem.Click += new System.EventHandler(this.дублироватьToolStripMenuItem_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(337, 22);
            this.toolStripMenuItem6.Text = "--";
            // 
            // закрытьИДублироватьToolStripMenuItem
            // 
            this.закрытьИДублироватьToolStripMenuItem.Name = "закрытьИДублироватьToolStripMenuItem";
            this.закрытьИДублироватьToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.закрытьИДублироватьToolStripMenuItem.Text = "Снять и дублировать";
            this.закрытьИДублироватьToolStripMenuItem.Visible = false;
            this.закрытьИДублироватьToolStripMenuItem.Click += new System.EventHandler(this.закрытьИДублироватьToolStripMenuItem_Click);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(337, 22);
            this.toolStripMenuItem7.Text = "--";
            // 
            // положитьСуммуПоЗаказуВодителюToolStripMenuItem
            // 
            this.положитьСуммуПоЗаказуВодителюToolStripMenuItem.Name = "положитьСуммуПоЗаказуВодителюToolStripMenuItem";
            this.положитьСуммуПоЗаказуВодителюToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.положитьСуммуПоЗаказуВодителюToolStripMenuItem.Text = "Положить сумму по заказу водителю";
            this.положитьСуммуПоЗаказуВодителюToolStripMenuItem.Visible = false;
            this.положитьСуммуПоЗаказуВодителюToolStripMenuItem.Click += new System.EventHandler(this.положитьСуммуПоЗаказуВодителюToolStripMenuItem_Click);
            // 
            // toolStripMenuItem14
            // 
            this.toolStripMenuItem14.Name = "toolStripMenuItem14";
            this.toolStripMenuItem14.Size = new System.Drawing.Size(334, 6);
            // 
            // вЧерныйСписокToolStripMenuItem
            // 
            this.вЧерныйСписокToolStripMenuItem.Name = "вЧерныйСписокToolStripMenuItem";
            this.вЧерныйСписокToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.вЧерныйСписокToolStripMenuItem.Text = "В черный список";
            this.вЧерныйСписокToolStripMenuItem.Click += new System.EventHandler(this.вЧерныйСписокToolStripMenuItem_Click);
            // 
            // toolStripMenuItem15
            // 
            this.toolStripMenuItem15.Name = "toolStripMenuItem15";
            this.toolStripMenuItem15.Size = new System.Drawing.Size(334, 6);
            // 
            // закрытьЧерныйСписокToolStripMenuItem
            // 
            this.закрытьЧерныйСписокToolStripMenuItem.Name = "закрытьЧерныйСписокToolStripMenuItem";
            this.закрытьЧерныйСписокToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.закрытьЧерныйСписокToolStripMenuItem.Text = "Закрыть черный список";
            this.закрытьЧерныйСписокToolStripMenuItem.Click += new System.EventHandler(this.закрытьЧерныйСписокToolStripMenuItem_Click);
            // 
            // toolStripMenuItem16
            // 
            this.toolStripMenuItem16.Name = "toolStripMenuItem16";
            this.toolStripMenuItem16.Size = new System.Drawing.Size(334, 6);
            // 
            // перевестиВУспешноЗавершеныеToolStripMenuItem
            // 
            this.перевестиВУспешноЗавершеныеToolStripMenuItem.Name = "перевестиВУспешноЗавершеныеToolStripMenuItem";
            this.перевестиВУспешноЗавершеныеToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.перевестиВУспешноЗавершеныеToolStripMenuItem.Text = "Перевести в успешно завершеные";
            this.перевестиВУспешноЗавершеныеToolStripMenuItem.Click += new System.EventHandler(this.перевестиВУспешноЗавершеныеToolStripMenuItem_Click);
            // 
            // завершитьСНазначениемВодителяToolStripMenuItem
            // 
            this.завершитьСНазначениемВодителяToolStripMenuItem.Name = "завершитьСНазначениемВодителяToolStripMenuItem";
            this.завершитьСНазначениемВодителяToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.завершитьСНазначениемВодителяToolStripMenuItem.Text = "Завершить с назначением водителя";
            this.завершитьСНазначениемВодителяToolStripMenuItem.Click += new System.EventHandler(this.завершитьСНазначениемВодителяToolStripMenuItem_Click);
            // 
            // toolStripMenuItem19
            // 
            this.toolStripMenuItem19.Name = "toolStripMenuItem19";
            this.toolStripMenuItem19.Size = new System.Drawing.Size(334, 6);
            // 
            // перевестиВзаказВРаботеToolStripMenuItem
            // 
            this.перевестиВзаказВРаботеToolStripMenuItem.Name = "перевестиВзаказВРаботеToolStripMenuItem";
            this.перевестиВзаказВРаботеToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.перевестиВзаказВРаботеToolStripMenuItem.Text = "Перевести предварительный  в \"заказ в работе\"";
            this.перевестиВзаказВРаботеToolStripMenuItem.ToolTipText = "Переводит заказ в работу (например предварительный)";
            this.перевестиВзаказВРаботеToolStripMenuItem.Click += new System.EventHandler(this.перевестиВзаказВРаботеToolStripMenuItem_Click);
            // 
            // ВернутьЗаказВРаботу
            // 
            this.ВернутьЗаказВРаботу.Name = "ВернутьЗаказВРаботу";
            this.ВернутьЗаказВРаботу.Size = new System.Drawing.Size(337, 22);
            this.ВернутьЗаказВРаботу.Text = "Вернуть заказ в работу";
            this.ВернутьЗаказВРаботу.Click += new System.EventHandler(this.ВернутьЗаказВРаботу_Click);
            // 
            // полноеРедактированиеЗаказаToolStripMenuItem
            // 
            this.полноеРедактированиеЗаказаToolStripMenuItem.Name = "полноеРедактированиеЗаказаToolStripMenuItem";
            this.полноеРедактированиеЗаказаToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.полноеРедактированиеЗаказаToolStripMenuItem.Text = "Полное редактирование заказа";
            this.полноеРедактированиеЗаказаToolStripMenuItem.Click += new System.EventHandler(this.полноеРедактированиеЗаказаToolStripMenuItem_Click);
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(227, 6);
            // 
            // toolStripMenuItem12
            // 
            this.toolStripMenuItem12.Name = "toolStripMenuItem12";
            this.toolStripMenuItem12.Size = new System.Drawing.Size(227, 6);
            // 
            // toolStripMenuItem13
            // 
            this.toolStripMenuItem13.Name = "toolStripMenuItem13";
            this.toolStripMenuItem13.Size = new System.Drawing.Size(227, 6);
            // 
            // yjdsqToolStripMenuItem
            // 
            this.yjdsqToolStripMenuItem.Name = "yjdsqToolStripMenuItem";
            this.yjdsqToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.yjdsqToolStripMenuItem.Text = "новый заказ новая форма";
            this.yjdsqToolStripMenuItem.Visible = false;
            this.yjdsqToolStripMenuItem.Click += new System.EventHandler(this.yjdsqToolStripMenuItem_Click);
            // 
            // редактироватьНоваяФормаToolStripMenuItem
            // 
            this.редактироватьНоваяФормаToolStripMenuItem.Name = "редактироватьНоваяФормаToolStripMenuItem";
            this.редактироватьНоваяФормаToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.редактироватьНоваяФормаToolStripMenuItem.Text = "Редактировать новая форма";
            this.редактироватьНоваяФормаToolStripMenuItem.Visible = false;
            this.редактироватьНоваяФормаToolStripMenuItem.Click += new System.EventHandler(this.редактироватьНоваяФормаToolStripMenuItem_Click);
            // 
            // поторопитеToolStripMenuItem
            // 
            this.поторопитеToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.снятьпоторопитеToolStripMenuItem1,
            this.поставитьпоторопитьToolStripMenuItem,
            this.снятьпоторопитеИОтправитьВодителюторопимToolStripMenuItem});
            this.поторопитеToolStripMenuItem.Name = "поторопитеToolStripMenuItem";
            this.поторопитеToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.поторопитеToolStripMenuItem.Text = "Поторопите";
            // 
            // снятьпоторопитеToolStripMenuItem1
            // 
            this.снятьпоторопитеToolStripMenuItem1.Name = "снятьпоторопитеToolStripMenuItem1";
            this.снятьпоторопитеToolStripMenuItem1.Size = new System.Drawing.Size(372, 22);
            this.снятьпоторопитеToolStripMenuItem1.Text = "Снять \"поторопите\"";
            this.снятьпоторопитеToolStripMenuItem1.Click += new System.EventHandler(this.снятьпоторопитеToolStripMenuItem1_Click);
            // 
            // поставитьпоторопитьToolStripMenuItem
            // 
            this.поставитьпоторопитьToolStripMenuItem.Name = "поставитьпоторопитьToolStripMenuItem";
            this.поставитьпоторопитьToolStripMenuItem.Size = new System.Drawing.Size(372, 22);
            this.поставитьпоторопитьToolStripMenuItem.Text = "Поставить \"поторопить\"";
            this.поставитьпоторопитьToolStripMenuItem.Click += new System.EventHandler(this.поставитьпоторопитьToolStripMenuItem_Click);
            // 
            // снятьпоторопитеИОтправитьВодителюторопимToolStripMenuItem
            // 
            this.снятьпоторопитеИОтправитьВодителюторопимToolStripMenuItem.Name = "снятьпоторопитеИОтправитьВодителюторопимToolStripMenuItem";
            this.снятьпоторопитеИОтправитьВодителюторопимToolStripMenuItem.Size = new System.Drawing.Size(372, 22);
            this.снятьпоторопитеИОтправитьВодителюторопимToolStripMenuItem.Text = "Снять \"поторопите\" и отправить водителю \"торопим\"";
            this.снятьпоторопитеИОтправитьВодителюторопимToolStripMenuItem.Click += new System.EventHandler(this.снятьпоторопитеИОтправитьВодителюторопимToolStripMenuItem_Click);
            // 
            // снятьпоторопитеToolStripMenuItem
            // 
            this.снятьпоторопитеToolStripMenuItem.Name = "снятьпоторопитеToolStripMenuItem";
            this.снятьпоторопитеToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.снятьпоторопитеToolStripMenuItem.Text = "Снять \"поторопите\"";
            this.снятьпоторопитеToolStripMenuItem.Visible = false;
            this.снятьпоторопитеToolStripMenuItem.Click += new System.EventHandler(this.снятьпоторопитеToolStripMenuItem_Click);
            // 
            // таксометрToolStripMenuItem
            // 
            this.таксометрToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.включитьToolStripMenuItem,
            this.выключитьToolStripMenuItem});
            this.таксометрToolStripMenuItem.Name = "таксометрToolStripMenuItem";
            this.таксометрToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.таксометрToolStripMenuItem.Text = "Таксометр";
            // 
            // включитьToolStripMenuItem
            // 
            this.включитьToolStripMenuItem.Name = "включитьToolStripMenuItem";
            this.включитьToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.включитьToolStripMenuItem.Text = "Включить";
            this.включитьToolStripMenuItem.Click += new System.EventHandler(this.включитьToolStripMenuItem_Click);
            // 
            // выключитьToolStripMenuItem
            // 
            this.выключитьToolStripMenuItem.Name = "выключитьToolStripMenuItem";
            this.выключитьToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.выключитьToolStripMenuItem.Text = "Выключить";
            this.выключитьToolStripMenuItem.Click += new System.EventHandler(this.выключитьToolStripMenuItem_Click);
            // 
            // назначениеToolStripMenuItem
            // 
            this.назначениеToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.назначитьВодителяToolStripMenuItem,
            this.снятьВодителяСЗаказаToolStripMenuItem1,
            this.предложитьЗаказВодителюОкошкомToolStripMenuItem});
            this.назначениеToolStripMenuItem.Name = "назначениеToolStripMenuItem";
            this.назначениеToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.назначениеToolStripMenuItem.Text = "Назначение";
            // 
            // назначитьВодителяToolStripMenuItem
            // 
            this.назначитьВодителяToolStripMenuItem.Name = "назначитьВодителяToolStripMenuItem";
            this.назначитьВодителяToolStripMenuItem.Size = new System.Drawing.Size(287, 22);
            this.назначитьВодителяToolStripMenuItem.Text = "Назначить водителя на заказ";
            this.назначитьВодителяToolStripMenuItem.Click += new System.EventHandler(this.назначитьВодителяToolStripMenuItem_Click);
            // 
            // снятьВодителяСЗаказаToolStripMenuItem1
            // 
            this.снятьВодителяСЗаказаToolStripMenuItem1.Name = "снятьВодителяСЗаказаToolStripMenuItem1";
            this.снятьВодителяСЗаказаToolStripMenuItem1.Size = new System.Drawing.Size(287, 22);
            this.снятьВодителяСЗаказаToolStripMenuItem1.Text = "Снять водителя с заказа";
            this.снятьВодителяСЗаказаToolStripMenuItem1.Click += new System.EventHandler(this.снятьВодителяСЗаказаToolStripMenuItem1_Click);
            // 
            // предложитьЗаказВодителюОкошкомToolStripMenuItem
            // 
            this.предложитьЗаказВодителюОкошкомToolStripMenuItem.Name = "предложитьЗаказВодителюОкошкомToolStripMenuItem";
            this.предложитьЗаказВодителюОкошкомToolStripMenuItem.Size = new System.Drawing.Size(287, 22);
            this.предложитьЗаказВодителюОкошкомToolStripMenuItem.Text = "Предложить заказ водителю окошком";
            this.предложитьЗаказВодителюОкошкомToolStripMenuItem.Click += new System.EventHandler(this.предложитьЗаказВодителюОкошкомToolStripMenuItem_Click);
            // 
            // попиликатьУВодителяToolStripMenuItem
            // 
            this.попиликатьУВодителяToolStripMenuItem.Name = "попиликатьУВодителяToolStripMenuItem";
            this.попиликатьУВодителяToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.попиликатьУВодителяToolStripMenuItem.Text = "Попиликать у водителя";
            this.попиликатьУВодителяToolStripMenuItem.Click += new System.EventHandler(this.попиликатьУВодителяToolStripMenuItem_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.chkFreeOrders);
            this.panel3.Controls.Add(this.FFilterPos);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.fFilterAddrPhone);
            this.panel3.Controls.Add(this.chkMyOrders);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 161);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(807, 26);
            this.panel3.TabIndex = 55;
            // 
            // chkFreeOrders
            // 
            this.chkFreeOrders.AutoSize = true;
            this.chkFreeOrders.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.chkFreeOrders.Location = new System.Drawing.Point(118, 3);
            this.chkFreeOrders.Name = "chkFreeOrders";
            this.chkFreeOrders.Size = new System.Drawing.Size(83, 17);
            this.chkFreeOrders.TabIndex = 4;
            this.chkFreeOrders.Text = "Свободные";
            this.chkFreeOrders.UseVisualStyleBackColor = true;
            // 
            // FFilterPos
            // 
            this.FFilterPos.Location = new System.Drawing.Point(531, 1);
            this.FFilterPos.Name = "FFilterPos";
            this.FFilterPos.Size = new System.Drawing.Size(100, 20);
            this.FFilterPos.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label8.Location = new System.Drawing.Point(503, 1);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(27, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Поз";
            // 
            // fFilterAddrPhone
            // 
            this.fFilterAddrPhone.Location = new System.Drawing.Point(367, 3);
            this.fFilterAddrPhone.Name = "fFilterAddrPhone";
            this.fFilterAddrPhone.Size = new System.Drawing.Size(100, 20);
            this.fFilterAddrPhone.TabIndex = 1;
            // 
            // chkMyOrders
            // 
            this.chkMyOrders.AutoSize = true;
            this.chkMyOrders.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.chkMyOrders.Location = new System.Drawing.Point(8, 3);
            this.chkMyOrders.Name = "chkMyOrders";
            this.chkMyOrders.Size = new System.Drawing.Size(88, 17);
            this.chkMyOrders.TabIndex = 0;
            this.chkMyOrders.Text = "Мои заказы";
            this.chkMyOrders.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label7.Location = new System.Drawing.Point(275, 4);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Адрес, телефон";
            // 
            // filterPanel
            // 
            this.filterPanel.Controls.Add(this.button16);
            this.filterPanel.Controls.Add(this.button15);
            this.filterPanel.Controls.Add(this.label9);
            this.filterPanel.Controls.Add(this.bnal_opl_tm);
            this.filterPanel.Controls.Add(this.button14);
            this.filterPanel.Controls.Add(this.button13);
            this.filterPanel.Controls.Add(this.chkUnclosedBnal);
            this.filterPanel.Controls.Add(this.chkBnal);
            this.filterPanel.Controls.Add(this.Успешные);
            this.filterPanel.Controls.Add(this.btn_print);
            this.filterPanel.Controls.Add(this.Отмененные);
            this.filterPanel.Controls.Add(this.filterAddress);
            this.filterPanel.Controls.Add(this.filterPhone);
            this.filterPanel.Controls.Add(this.filterOrg);
            this.filterPanel.Controls.Add(this.filterCarService);
            this.filterPanel.Controls.Add(this.filterOrderService);
            this.filterPanel.Controls.Add(this.filterCar);
            this.filterPanel.Controls.Add(this.label6);
            this.filterPanel.Controls.Add(this.label5);
            this.filterPanel.Controls.Add(this.label4);
            this.filterPanel.Controls.Add(this.label3);
            this.filterPanel.Controls.Add(this.label2);
            this.filterPanel.Controls.Add(this.label1);
            this.filterPanel.Controls.Add(this.btnRefreshOrders);
            this.filterPanel.Controls.Add(this.groupBox6);
            this.filterPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.filterPanel.Location = new System.Drawing.Point(0, 0);
            this.filterPanel.Name = "filterPanel";
            this.filterPanel.Size = new System.Drawing.Size(807, 161);
            this.filterPanel.TabIndex = 54;
            this.filterPanel.Visible = false;
            this.filterPanel.VisibleChanged += new System.EventHandler(this.filterPanel_VisibleChanged);
            this.filterPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.filterPanel_Paint);
            // 
            // button16
            // 
            this.button16.Font = new System.Drawing.Font("Microsoft Sans Serif", 5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button16.Location = new System.Drawing.Point(604, 56);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(75, 23);
            this.button16.TabIndex = 28;
            this.button16.Text = "Печать по сотрудникам";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(763, 101);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(75, 23);
            this.button15.TabIndex = 27;
            this.button15.Text = "Печать 4";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click_1);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(558, 117);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 13);
            this.label9.TabIndex = 26;
            this.label9.Text = "Оплаченые";
            // 
            // bnal_opl_tm
            // 
            this.bnal_opl_tm.Checked = false;
            this.bnal_opl_tm.Location = new System.Drawing.Point(557, 130);
            this.bnal_opl_tm.Name = "bnal_opl_tm";
            this.bnal_opl_tm.ShowCheckBox = true;
            this.bnal_opl_tm.Size = new System.Drawing.Size(200, 20);
            this.bnal_opl_tm.TabIndex = 25;
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(685, 37);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(75, 23);
            this.button14.TabIndex = 24;
            this.button14.Text = "Печать 3";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click_1);
            // 
            // button13
            // 
            this.button13.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button13.Location = new System.Drawing.Point(685, 8);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(75, 23);
            this.button13.TabIndex = 23;
            this.button13.Text = "Печать2";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click_2);
            // 
            // chkUnclosedBnal
            // 
            this.chkUnclosedBnal.AutoSize = true;
            this.chkUnclosedBnal.Location = new System.Drawing.Point(557, 33);
            this.chkUnclosedBnal.Name = "chkUnclosedBnal";
            this.chkUnclosedBnal.Size = new System.Drawing.Size(122, 17);
            this.chkUnclosedBnal.TabIndex = 22;
            this.chkUnclosedBnal.Text = "незакрытый БНАЛ";
            this.chkUnclosedBnal.UseVisualStyleBackColor = true;
            // 
            // chkBnal
            // 
            this.chkBnal.AutoSize = true;
            this.chkBnal.Location = new System.Drawing.Point(557, 12);
            this.chkBnal.Name = "chkBnal";
            this.chkBnal.Size = new System.Drawing.Size(56, 17);
            this.chkBnal.TabIndex = 21;
            this.chkBnal.Text = "БНАЛ";
            this.chkBnal.UseVisualStyleBackColor = true;
            // 
            // Успешные
            // 
            this.Успешные.AutoSize = true;
            this.Успешные.Checked = true;
            this.Успешные.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Успешные.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Успешные.Location = new System.Drawing.Point(147, 136);
            this.Успешные.Name = "Успешные";
            this.Успешные.Size = new System.Drawing.Size(80, 17);
            this.Успешные.TabIndex = 20;
            this.Успешные.Text = "Успешные";
            this.Успешные.UseVisualStyleBackColor = true;
            // 
            // btn_print
            // 
            this.btn_print.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btn_print.Location = new System.Drawing.Point(763, 128);
            this.btn_print.Name = "btn_print";
            this.btn_print.Size = new System.Drawing.Size(75, 23);
            this.btn_print.TabIndex = 19;
            this.btn_print.Text = "Печать";
            this.btn_print.UseVisualStyleBackColor = true;
            this.btn_print.Click += new System.EventHandler(this.button13_Click_1);
            // 
            // Отмененные
            // 
            this.Отмененные.AutoSize = true;
            this.Отмененные.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Отмененные.Location = new System.Drawing.Point(147, 117);
            this.Отмененные.Name = "Отмененные";
            this.Отмененные.Size = new System.Drawing.Size(91, 17);
            this.Отмененные.TabIndex = 18;
            this.Отмененные.Text = "Отмененные";
            this.Отмененные.UseVisualStyleBackColor = true;
            // 
            // filterAddress
            // 
            this.filterAddress.Location = new System.Drawing.Point(340, 56);
            this.filterAddress.Name = "filterAddress";
            this.filterAddress.Size = new System.Drawing.Size(190, 20);
            this.filterAddress.TabIndex = 17;
            // 
            // filterPhone
            // 
            this.filterPhone.Location = new System.Drawing.Point(340, 34);
            this.filterPhone.Name = "filterPhone";
            this.filterPhone.Size = new System.Drawing.Size(190, 20);
            this.filterPhone.TabIndex = 16;
            // 
            // filterOrg
            // 
            this.filterOrg.FormattingEnabled = true;
            this.filterOrg.Location = new System.Drawing.Point(340, 130);
            this.filterOrg.Name = "filterOrg";
            this.filterOrg.Size = new System.Drawing.Size(190, 21);
            this.filterOrg.TabIndex = 15;
            // 
            // filterCarService
            // 
            this.filterCarService.FormattingEnabled = true;
            this.filterCarService.Location = new System.Drawing.Point(340, 105);
            this.filterCarService.Name = "filterCarService";
            this.filterCarService.Size = new System.Drawing.Size(190, 21);
            this.filterCarService.TabIndex = 14;
            // 
            // filterOrderService
            // 
            this.filterOrderService.FormattingEnabled = true;
            this.filterOrderService.Location = new System.Drawing.Point(340, 81);
            this.filterOrderService.Name = "filterOrderService";
            this.filterOrderService.Size = new System.Drawing.Size(190, 21);
            this.filterOrderService.TabIndex = 13;
            // 
            // filterCar
            // 
            this.filterCar.FormattingEnabled = true;
            this.filterCar.Location = new System.Drawing.Point(340, 11);
            this.filterCar.Name = "filterCar";
            this.filterCar.Size = new System.Drawing.Size(190, 21);
            this.filterCar.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label6.Location = new System.Drawing.Point(253, 108);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Служба машины";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label5.Location = new System.Drawing.Point(253, 133);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Организация";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label4.Location = new System.Drawing.Point(253, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Служба заказа";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label3.Location = new System.Drawing.Point(253, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Адрес";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(253, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Телефон";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(253, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Водитель";
            // 
            // btnRefreshOrders
            // 
            this.btnRefreshOrders.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnRefreshOrders.Location = new System.Drawing.Point(8, 126);
            this.btnRefreshOrders.Name = "btnRefreshOrders";
            this.btnRefreshOrders.Size = new System.Drawing.Size(117, 23);
            this.btnRefreshOrders.TabIndex = 5;
            this.btnRefreshOrders.Text = "Обновить список";
            this.btnRefreshOrders.UseVisualStyleBackColor = true;
            this.btnRefreshOrders.Click += new System.EventHandler(this.btnRefreshOrders_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.button1);
            this.groupBox6.Controls.Add(this.btn1Month);
            this.groupBox6.Controls.Add(this.btn7Day);
            this.groupBox6.Controls.Add(this.btn1Day);
            this.groupBox6.Controls.Add(this.filterTm2);
            this.groupBox6.Controls.Add(this.filterTm1);
            this.groupBox6.Controls.Add(this.filterDt2);
            this.groupBox6.Controls.Add(this.filterDt1);
            this.groupBox6.Location = new System.Drawing.Point(8, 8);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(237, 103);
            this.groupBox6.TabIndex = 4;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Период";
            // 
            // button1
            // 
            this.button1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button1.Location = new System.Drawing.Point(6, 69);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(26, 23);
            this.button1.TabIndex = 11;
            this.button1.Text = "2ч";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // btn1Month
            // 
            this.btn1Month.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btn1Month.Location = new System.Drawing.Point(178, 69);
            this.btn1Month.Name = "btn1Month";
            this.btn1Month.Size = new System.Drawing.Size(52, 23);
            this.btn1Month.TabIndex = 10;
            this.btn1Month.Text = "Месяц";
            this.btn1Month.UseVisualStyleBackColor = true;
            this.btn1Month.Click += new System.EventHandler(this.btn1Month_Click);
            // 
            // btn7Day
            // 
            this.btn7Day.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btn7Day.Location = new System.Drawing.Point(120, 69);
            this.btn7Day.Name = "btn7Day";
            this.btn7Day.Size = new System.Drawing.Size(52, 23);
            this.btn7Day.TabIndex = 9;
            this.btn7Day.Text = "7";
            this.btn7Day.UseVisualStyleBackColor = true;
            this.btn7Day.Click += new System.EventHandler(this.btn7Day_Click);
            // 
            // btn1Day
            // 
            this.btn1Day.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btn1Day.Location = new System.Drawing.Point(65, 69);
            this.btn1Day.Name = "btn1Day";
            this.btn1Day.Size = new System.Drawing.Size(52, 23);
            this.btn1Day.TabIndex = 8;
            this.btn1Day.Text = "1";
            this.btn1Day.UseVisualStyleBackColor = true;
            this.btn1Day.Click += new System.EventHandler(this.btn1Day_Click);
            // 
            // filterTm2
            // 
            this.filterTm2.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.filterTm2.Location = new System.Drawing.Point(157, 43);
            this.filterTm2.Name = "filterTm2";
            this.filterTm2.ShowUpDown = true;
            this.filterTm2.Size = new System.Drawing.Size(73, 20);
            this.filterTm2.TabIndex = 7;
            // 
            // filterTm1
            // 
            this.filterTm1.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.filterTm1.Location = new System.Drawing.Point(157, 17);
            this.filterTm1.Name = "filterTm1";
            this.filterTm1.ShowUpDown = true;
            this.filterTm1.Size = new System.Drawing.Size(73, 20);
            this.filterTm1.TabIndex = 6;
            // 
            // filterDt2
            // 
            this.filterDt2.Location = new System.Drawing.Point(7, 43);
            this.filterDt2.Name = "filterDt2";
            this.filterDt2.Size = new System.Drawing.Size(128, 20);
            this.filterDt2.TabIndex = 5;
            // 
            // filterDt1
            // 
            this.filterDt1.Location = new System.Drawing.Point(7, 17);
            this.filterDt1.Name = "filterDt1";
            this.filterDt1.Size = new System.Drawing.Size(128, 20);
            this.filterDt1.TabIndex = 4;
            // 
            // tabMap
            // 
            this.tabMap.Controls.Add(this.mapControl1);
            this.tabMap.Controls.Add(this.panel2);
            this.tabMap.Location = new System.Drawing.Point(4, 22);
            this.tabMap.Margin = new System.Windows.Forms.Padding(0);
            this.tabMap.Name = "tabMap";
            this.tabMap.Size = new System.Drawing.Size(807, 587);
            this.tabMap.TabIndex = 0;
            this.tabMap.Text = "Карта";
            this.tabMap.UseVisualStyleBackColor = true;
            // 
            // mapControl1
            // 
            this.mapControl1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.mapControl1.CachePath = "";
            this.mapControl1.ContextMenuStrip = this.contextMenuStrip1;
            this.mapControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mapControl1.HighlightEntities = false;
            this.mapControl1.Latitude = 53.9753;
            this.mapControl1.Location = new System.Drawing.Point(0, 100);
            this.mapControl1.Longitude = -1.7017;
            this.mapControl1.LonScale = 0.00390625;
            this.mapControl1.Name = "mapControl1";
            this.mapControl1.Size = new System.Drawing.Size(807, 487);
            this.mapControl1.TabIndex = 2;
            this.mapControl1.TabStop = false;
            this.mapControl1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.mapControl1_MouseUp);
            this.mapControl1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.mapControl1_MouseDown);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.маршрутОтсюдаToolStripMenuItem,
            this.маршрутСюдаToolStripMenuItem,
            this.промежуточнаяТочка1ToolStripMenuItem,
            this.промежуточнаяТочка2ToolStripMenuItem,
            this.очиститьМаршрутToolStripMenuItem,
            this.очиститьТочку1ToolStripMenuItem,
            this.очиститьПромежуточнуюТочку2ToolStripMenuItem,
            this.редактироватьРайонToolStripMenuItem,
            this.удалитьТочкуToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(266, 202);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // маршрутОтсюдаToolStripMenuItem
            // 
            this.маршрутОтсюдаToolStripMenuItem.Name = "маршрутОтсюдаToolStripMenuItem";
            this.маршрутОтсюдаToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.маршрутОтсюдаToolStripMenuItem.Text = "Маршрут отсюда";
            this.маршрутОтсюдаToolStripMenuItem.Click += new System.EventHandler(this.маршрутОтсюдаToolStripMenuItem_Click);
            // 
            // маршрутСюдаToolStripMenuItem
            // 
            this.маршрутСюдаToolStripMenuItem.Name = "маршрутСюдаToolStripMenuItem";
            this.маршрутСюдаToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.маршрутСюдаToolStripMenuItem.Text = "Маршрут сюда";
            // 
            // промежуточнаяТочка1ToolStripMenuItem
            // 
            this.промежуточнаяТочка1ToolStripMenuItem.Name = "промежуточнаяТочка1ToolStripMenuItem";
            this.промежуточнаяТочка1ToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.промежуточнаяТочка1ToolStripMenuItem.Text = "Промежуточная точка 1";
            // 
            // промежуточнаяТочка2ToolStripMenuItem
            // 
            this.промежуточнаяТочка2ToolStripMenuItem.Name = "промежуточнаяТочка2ToolStripMenuItem";
            this.промежуточнаяТочка2ToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.промежуточнаяТочка2ToolStripMenuItem.Text = "Промежуточная точка 2";
            // 
            // очиститьМаршрутToolStripMenuItem
            // 
            this.очиститьМаршрутToolStripMenuItem.Name = "очиститьМаршрутToolStripMenuItem";
            this.очиститьМаршрутToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.очиститьМаршрутToolStripMenuItem.Text = "Очистить маршрут";
            // 
            // очиститьТочку1ToolStripMenuItem
            // 
            this.очиститьТочку1ToolStripMenuItem.Name = "очиститьТочку1ToolStripMenuItem";
            this.очиститьТочку1ToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.очиститьТочку1ToolStripMenuItem.Text = "Очистить промежуточную точку 1";
            // 
            // очиститьПромежуточнуюТочку2ToolStripMenuItem
            // 
            this.очиститьПромежуточнуюТочку2ToolStripMenuItem.Name = "очиститьПромежуточнуюТочку2ToolStripMenuItem";
            this.очиститьПромежуточнуюТочку2ToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.очиститьПромежуточнуюТочку2ToolStripMenuItem.Text = "Очистить промежуточную точку 2";
            // 
            // редактироватьРайонToolStripMenuItem
            // 
            this.редактироватьРайонToolStripMenuItem.Name = "редактироватьРайонToolStripMenuItem";
            this.редактироватьРайонToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.редактироватьРайонToolStripMenuItem.Text = "Редактировать район";
            this.редактироватьРайонToolStripMenuItem.Visible = false;
            this.редактироватьРайонToolStripMenuItem.Click += new System.EventHandler(this.редактироватьРайонToolStripMenuItem_Click);
            // 
            // удалитьТочкуToolStripMenuItem
            // 
            this.удалитьТочкуToolStripMenuItem.Name = "удалитьТочкуToolStripMenuItem";
            this.удалитьТочкуToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.удалитьТочкуToolStripMenuItem.Text = "Удалить точку";
            this.удалитьТочкуToolStripMenuItem.Click += new System.EventHandler(this.удалитьТочкуToolStripMenuItem_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.сщхранитьгород);
            this.panel2.Controls.Add(this.загрузитьгород);
            this.panel2.Controls.Add(this.btnSaveZones);
            this.panel2.Controls.Add(this.btnLoadZones);
            this.panel2.Controls.Add(this.button11);
            this.panel2.Controls.Add(this.button3);
            this.panel2.Controls.Add(this.chkMapShowOrders);
            this.panel2.Controls.Add(this.path_info);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(807, 100);
            this.panel2.TabIndex = 4;
            // 
            // сщхранитьгород
            // 
            this.сщхранитьгород.Location = new System.Drawing.Point(158, 72);
            this.сщхранитьгород.Name = "сщхранитьгород";
            this.сщхранитьгород.Size = new System.Drawing.Size(126, 23);
            this.сщхранитьгород.TabIndex = 11;
            this.сщхранитьгород.Text = "Сохранить город";
            this.сщхранитьгород.UseVisualStyleBackColor = true;
            this.сщхранитьгород.Click += new System.EventHandler(this.сщхранитьгород_Click);
            // 
            // загрузитьгород
            // 
            this.загрузитьгород.Location = new System.Drawing.Point(7, 69);
            this.загрузитьгород.Name = "загрузитьгород";
            this.загрузитьгород.Size = new System.Drawing.Size(125, 23);
            this.загрузитьгород.TabIndex = 10;
            this.загрузитьгород.Text = "Загрузить город";
            this.загрузитьгород.UseVisualStyleBackColor = true;
            this.загрузитьгород.Click += new System.EventHandler(this.загрузитьгород_Click);
            // 
            // btnSaveZones
            // 
            this.btnSaveZones.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnSaveZones.Location = new System.Drawing.Point(86, 47);
            this.btnSaveZones.Margin = new System.Windows.Forms.Padding(2);
            this.btnSaveZones.Name = "btnSaveZones";
            this.btnSaveZones.Size = new System.Drawing.Size(80, 19);
            this.btnSaveZones.TabIndex = 8;
            this.btnSaveZones.Text = "сохр зоны";
            this.btnSaveZones.UseVisualStyleBackColor = true;
            this.btnSaveZones.Click += new System.EventHandler(this.btnSaveZones_Click);
            // 
            // btnLoadZones
            // 
            this.btnLoadZones.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnLoadZones.Location = new System.Drawing.Point(2, 45);
            this.btnLoadZones.Margin = new System.Windows.Forms.Padding(2);
            this.btnLoadZones.Name = "btnLoadZones";
            this.btnLoadZones.Size = new System.Drawing.Size(80, 19);
            this.btnLoadZones.TabIndex = 7;
            this.btnLoadZones.Text = "загр зоны";
            this.btnLoadZones.UseVisualStyleBackColor = true;
            this.btnLoadZones.Click += new System.EventHandler(this.btnLoadZones_Click);
            // 
            // button11
            // 
            this.button11.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button11.Location = new System.Drawing.Point(359, 19);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(169, 23);
            this.button11.TabIndex = 9;
            this.button11.Text = "загрузить точки 1 региона";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button3
            // 
            this.button3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button3.Location = new System.Drawing.Point(568, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 6;
            this.button3.Text = "Адреса";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click_3);
            // 
            // chkMapShowOrders
            // 
            this.chkMapShowOrders.FormattingEnabled = true;
            this.chkMapShowOrders.Items.AddRange(new object[] {
            "Свободные заказы",
            "Окончания текущих заказов"});
            this.chkMapShowOrders.Location = new System.Drawing.Point(8, 8);
            this.chkMapShowOrders.Name = "chkMapShowOrders";
            this.chkMapShowOrders.Size = new System.Drawing.Size(241, 34);
            this.chkMapShowOrders.TabIndex = 5;
            this.chkMapShowOrders.SelectedIndexChanged += new System.EventHandler(this.chkMapShowOrders_SelectedIndexChanged);
            // 
            // path_info
            // 
            this.path_info.Location = new System.Drawing.Point(375, 40);
            this.path_info.Multiline = true;
            this.path_info.Name = "path_info";
            this.path_info.Size = new System.Drawing.Size(268, 41);
            this.path_info.TabIndex = 3;
            this.path_info.Text = "Здесь будет информация по маршруту\r\nиз заказа или если прямое построение по карте" +
                "";
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.btnReview);
            this.tabPage8.Controls.Add(this.btn_driver_card);
            this.tabPage8.Controls.Add(this.btnLoadGisModuleData);
            this.tabPage8.Controls.Add(this.button4);
            this.tabPage8.Controls.Add(this.button2);
            this.tabPage8.Controls.Add(this.btnPromisedPayment);
            this.tabPage8.Controls.Add(this.btnDisableAlarm);
            this.tabPage8.Controls.Add(this.groupBox5);
            this.tabPage8.Controls.Add(this.groupBox4);
            this.tabPage8.Controls.Add(this.groupBox3);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Margin = new System.Windows.Forms.Padding(0);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(807, 587);
            this.tabPage8.TabIndex = 2;
            this.tabPage8.Text = "Управление";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // btnReview
            // 
            this.btnReview.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnReview.Location = new System.Drawing.Point(8, 373);
            this.btnReview.Name = "btnReview";
            this.btnReview.Size = new System.Drawing.Size(228, 23);
            this.btnReview.TabIndex = 10;
            this.btnReview.Text = "Отзывы";
            this.btnReview.UseVisualStyleBackColor = true;
            this.btnReview.Click += new System.EventHandler(this.btnReview_Click);
            // 
            // btn_driver_card
            // 
            this.btn_driver_card.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.btn_driver_card.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btn_driver_card.Location = new System.Drawing.Point(8, 291);
            this.btn_driver_card.Name = "btn_driver_card";
            this.btn_driver_card.Size = new System.Drawing.Size(228, 23);
            this.btn_driver_card.TabIndex = 9;
            this.btn_driver_card.Text = "Карточка водителя (F5)";
            this.btn_driver_card.UseVisualStyleBackColor = true;
            this.btn_driver_card.Visible = false;
            this.btn_driver_card.Click += new System.EventHandler(this.btn_driver_card_Click);
            // 
            // btnLoadGisModuleData
            // 
            this.btnLoadGisModuleData.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnLoadGisModuleData.Location = new System.Drawing.Point(8, 419);
            this.btnLoadGisModuleData.Name = "btnLoadGisModuleData";
            this.btnLoadGisModuleData.Size = new System.Drawing.Size(228, 35);
            this.btnLoadGisModuleData.TabIndex = 8;
            this.btnLoadGisModuleData.Text = "подргузить адреса из базы (безопасно)";
            this.btnLoadGisModuleData.UseVisualStyleBackColor = true;
            this.btnLoadGisModuleData.Click += new System.EventHandler(this.btnLoadGisModuleData_Click);
            // 
            // button4
            // 
            this.button4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button4.Location = new System.Drawing.Point(480, 419);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(173, 35);
            this.button4.TabIndex = 7;
            this.button4.Text = "загрузить адреса 2GIS";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Visible = false;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // button2
            // 
            this.button2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button2.Location = new System.Drawing.Point(8, 329);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(228, 23);
            this.button2.TabIndex = 6;
            this.button2.Text = "Внесение суммы/платежки";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_2);
            // 
            // btnPromisedPayment
            // 
            this.btnPromisedPayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnPromisedPayment.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnPromisedPayment.Location = new System.Drawing.Point(451, 373);
            this.btnPromisedPayment.Name = "btnPromisedPayment";
            this.btnPromisedPayment.Size = new System.Drawing.Size(228, 23);
            this.btnPromisedPayment.TabIndex = 5;
            this.btnPromisedPayment.Text = "Обещанный платеж";
            this.btnPromisedPayment.UseVisualStyleBackColor = true;
            this.btnPromisedPayment.Visible = false;
            this.btnPromisedPayment.Click += new System.EventHandler(this.btnPromisedPayment_Click);
            // 
            // btnDisableAlarm
            // 
            this.btnDisableAlarm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnDisableAlarm.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnDisableAlarm.Location = new System.Drawing.Point(451, 344);
            this.btnDisableAlarm.Name = "btnDisableAlarm";
            this.btnDisableAlarm.Size = new System.Drawing.Size(228, 23);
            this.btnDisableAlarm.TabIndex = 4;
            this.btnDisableAlarm.Text = "Снять тревогу";
            this.btnDisableAlarm.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnDisableAlarm.UseVisualStyleBackColor = true;
            this.btnDisableAlarm.Visible = false;
            this.btnDisableAlarm.Click += new System.EventHandler(this.btnDisableAlarm_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.button12);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox5.Location = new System.Drawing.Point(0, 213);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(807, 56);
            this.groupBox5.TabIndex = 3;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Утилиты";
            this.groupBox5.Visible = false;
            // 
            // button12
            // 
            this.button12.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button12.Location = new System.Drawing.Point(8, 19);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(75, 23);
            this.button12.TabIndex = 6;
            this.button12.Text = "SMS рассылка";
            this.button12.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tabControl4);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Location = new System.Drawing.Point(0, 120);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(807, 93);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Справочники";
            // 
            // tabControl4
            // 
            this.tabControl4.Controls.Add(this.tabPage9);
            this.tabControl4.Controls.Add(this.tabPage10);
            this.tabControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl4.Location = new System.Drawing.Point(3, 16);
            this.tabControl4.Name = "tabControl4";
            this.tabControl4.SelectedIndex = 0;
            this.tabControl4.Size = new System.Drawing.Size(801, 74);
            this.tabControl4.TabIndex = 1;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.btnArendaCars);
            this.tabPage9.Controls.Add(this.button8);
            this.tabPage9.Controls.Add(this.button7);
            this.tabPage9.Controls.Add(this.btnPhonesList);
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(793, 48);
            this.tabPage9.TabIndex = 0;
            this.tabPage9.Text = "Основные";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // btnArendaCars
            // 
            this.btnArendaCars.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnArendaCars.Location = new System.Drawing.Point(491, 6);
            this.btnArendaCars.Name = "btnArendaCars";
            this.btnArendaCars.Size = new System.Drawing.Size(155, 23);
            this.btnArendaCars.TabIndex = 3;
            this.btnArendaCars.Text = "АРЕНДНЫЕ МАШИНЫ";
            this.btnArendaCars.UseVisualStyleBackColor = true;
            this.btnArendaCars.Click += new System.EventHandler(this.btnArendaCars_Click);
            // 
            // button8
            // 
            this.button8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button8.Location = new System.Drawing.Point(89, 6);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 1;
            this.button8.Text = "Водители";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button7.Location = new System.Drawing.Point(8, 6);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 0;
            this.button7.Text = "Машины";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // btnPhonesList
            // 
            this.btnPhonesList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnPhonesList.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnPhonesList.Location = new System.Drawing.Point(203, 6);
            this.btnPhonesList.Name = "btnPhonesList";
            this.btnPhonesList.Size = new System.Drawing.Size(222, 23);
            this.btnPhonesList.TabIndex = 2;
            this.btnPhonesList.Text = "Телефоны (черный список)";
            this.btnPhonesList.UseVisualStyleBackColor = true;
            this.btnPhonesList.Click += new System.EventHandler(this.btnPhonesList_Click);
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.btnAktSverki);
            this.tabPage10.Controls.Add(this.btn_dic_gis_addresses);
            this.tabPage10.Controls.Add(this.buttoт_dic_organizations);
            this.tabPage10.Controls.Add(this.button10);
            this.tabPage10.Controls.Add(this.button9);
            this.tabPage10.Location = new System.Drawing.Point(4, 22);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(793, 48);
            this.tabPage10.TabIndex = 1;
            this.tabPage10.Text = "Административные";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // btnAktSverki
            // 
            this.btnAktSverki.Location = new System.Drawing.Point(552, 6);
            this.btnAktSverki.Name = "btnAktSverki";
            this.btnAktSverki.Size = new System.Drawing.Size(120, 23);
            this.btnAktSverki.TabIndex = 8;
            this.btnAktSverki.Text = "Акт сверки";
            this.btnAktSverki.UseVisualStyleBackColor = true;
            this.btnAktSverki.Click += new System.EventHandler(this.btnAktSverki_Click);
            // 
            // btn_dic_gis_addresses
            // 
            this.btn_dic_gis_addresses.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btn_dic_gis_addresses.Location = new System.Drawing.Point(190, 6);
            this.btn_dic_gis_addresses.Name = "btn_dic_gis_addresses";
            this.btn_dic_gis_addresses.Size = new System.Drawing.Size(93, 23);
            this.btn_dic_gis_addresses.TabIndex = 7;
            this.btn_dic_gis_addresses.Text = "Адреса";
            this.btn_dic_gis_addresses.UseVisualStyleBackColor = true;
            this.btn_dic_gis_addresses.Click += new System.EventHandler(this.btn_dic_gis_addresses_Click);
            // 
            // buttoт_dic_organizations
            // 
            this.buttoт_dic_organizations.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.buttoт_dic_organizations.Location = new System.Drawing.Point(444, 6);
            this.buttoт_dic_organizations.Name = "buttoт_dic_organizations";
            this.buttoт_dic_organizations.Size = new System.Drawing.Size(93, 23);
            this.buttoт_dic_organizations.TabIndex = 6;
            this.buttoт_dic_organizations.Text = "Организации";
            this.buttoт_dic_organizations.UseVisualStyleBackColor = true;
            this.buttoт_dic_organizations.Click += new System.EventHandler(this.buttoт_dic_organizations_Click);
            // 
            // button10
            // 
            this.button10.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button10.Location = new System.Drawing.Point(89, 6);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 23);
            this.button10.TabIndex = 5;
            this.button10.Text = "Тарифы";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button9
            // 
            this.button9.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button9.Location = new System.Drawing.Point(8, 6);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 4;
            this.button9.Text = "Операторы";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tabControl5);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(807, 120);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Отчеты";
            // 
            // tabControl5
            // 
            this.tabControl5.Controls.Add(this.tabPage11);
            this.tabControl5.Controls.Add(this.tabPage12);
            this.tabControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl5.Location = new System.Drawing.Point(3, 16);
            this.tabControl5.Name = "tabControl5";
            this.tabControl5.SelectedIndex = 0;
            this.tabControl5.Size = new System.Drawing.Size(801, 101);
            this.tabControl5.TabIndex = 0;
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this.button6);
            this.tabPage11.Controls.Add(this.dateTimePicker3);
            this.tabPage11.Controls.Add(this.dateTimePicker4);
            this.tabPage11.Controls.Add(this.dateTimePicker2);
            this.tabPage11.Controls.Add(this.dateTimePicker1);
            this.tabPage11.Controls.Add(this.button5);
            this.tabPage11.Location = new System.Drawing.Point(4, 22);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage11.Size = new System.Drawing.Size(793, 75);
            this.tabPage11.TabIndex = 0;
            this.tabPage11.Text = "tabPage11";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button6.Location = new System.Drawing.Point(155, 44);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(152, 23);
            this.button6.TabIndex = 5;
            this.button6.Text = "Графики";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePicker3.Location = new System.Drawing.Point(416, 7);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.Size = new System.Drawing.Size(71, 20);
            this.dateTimePicker3.TabIndex = 4;
            // 
            // dateTimePicker4
            // 
            this.dateTimePicker4.Location = new System.Drawing.Point(280, 7);
            this.dateTimePicker4.Name = "dateTimePicker4";
            this.dateTimePicker4.Size = new System.Drawing.Size(130, 20);
            this.dateTimePicker4.TabIndex = 3;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePicker2.Location = new System.Drawing.Point(144, 7);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(71, 20);
            this.dateTimePicker2.TabIndex = 2;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(8, 7);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(130, 20);
            this.dateTimePicker1.TabIndex = 1;
            // 
            // button5
            // 
            this.button5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button5.Location = new System.Drawing.Point(6, 44);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(132, 23);
            this.button5.TabIndex = 0;
            this.button5.Text = "Список заказов";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // tabPage12
            // 
            this.tabPage12.Location = new System.Drawing.Point(4, 22);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage12.Size = new System.Drawing.Size(793, 75);
            this.tabPage12.TabIndex = 1;
            this.tabPage12.Text = "tabPage12";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // rightTabControl
            // 
            this.rightTabControl.Controls.Add(this.tabCars);
            this.rightTabControl.Controls.Add(this.tabCalls);
            this.rightTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rightTabControl.Location = new System.Drawing.Point(0, 143);
            this.rightTabControl.Name = "rightTabControl";
            this.rightTabControl.SelectedIndex = 0;
            this.rightTabControl.Size = new System.Drawing.Size(303, 470);
            this.rightTabControl.TabIndex = 7;
            // 
            // tabCars
            // 
            this.tabCars.Controls.Add(this.view_cars);
            this.tabCars.Controls.Add(this.view_raions);
            this.tabCars.Location = new System.Drawing.Point(4, 22);
            this.tabCars.Name = "tabCars";
            this.tabCars.Padding = new System.Windows.Forms.Padding(3);
            this.tabCars.Size = new System.Drawing.Size(295, 444);
            this.tabCars.TabIndex = 0;
            this.tabCars.Text = "Машины";
            this.tabCars.UseVisualStyleBackColor = true;
            // 
            // view_cars
            // 
            this.view_cars.AllowUserToAddRows = false;
            this.view_cars.AllowUserToDeleteRows = false;
            this.view_cars.AllowUserToResizeRows = false;
            this.view_cars.BackgroundColor = System.Drawing.SystemColors.Control;
            this.view_cars.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.view_cars.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.view_cars.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.view_cars.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.view_cars.ContextMenuStrip = this.mnuCars;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.view_cars.DefaultCellStyle = dataGridViewCellStyle2;
            this.view_cars.Dock = System.Windows.Forms.DockStyle.Fill;
            this.view_cars.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.view_cars.GridColor = System.Drawing.SystemColors.HighlightText;
            this.view_cars.Location = new System.Drawing.Point(3, 3);
            this.view_cars.Name = "view_cars";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.view_cars.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.view_cars.RowHeadersVisible = false;
            this.view_cars.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.view_cars.RowTemplate.Height = 18;
            this.view_cars.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.view_cars.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.view_cars.Size = new System.Drawing.Size(289, 12);
            this.view_cars.TabIndex = 53;
            this.view_cars.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.view_cars_CellMouseDown);
            this.view_cars.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.view_cars_CellFormatting);
            // 
            // mnuCars
            // 
            this.mnuCars.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.позвонитьToolStripMenuItem,
            this.сообщениеToolStripMenuItem,
            this.toolStripMenuItem9,
            this.toolStripMenuItem5,
            this.снятьДозвонToolStripMenuItem1,
            this.снятьТревогуToolStripMenuItem1,
            this.toolStripMenuItem10,
            this.поставитьпослеОтменыToolStripMenuItem,
            this.снятьпToolStripMenuItem,
            this.toolStripMenuItem8,
            this.карточкаToolStripMenuItem,
            this.toolStripMenuItem17,
            this.загрузитьМаршрутToolStripMenuItem,
            this.toolStripMenuItem18,
            this.поставитьПервымПослеОтменыToolStripMenuItem1});
            this.mnuCars.Name = "mnuCars";
            this.mnuCars.Size = new System.Drawing.Size(261, 238);
            this.mnuCars.Opening += new System.ComponentModel.CancelEventHandler(this.mnuCars_Opening);
            // 
            // позвонитьToolStripMenuItem
            // 
            this.позвонитьToolStripMenuItem.Name = "позвонитьToolStripMenuItem";
            this.позвонитьToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.позвонитьToolStripMenuItem.Text = "Позвонить";
            this.позвонитьToolStripMenuItem.Click += new System.EventHandler(this.позвонитьToolStripMenuItem_Click);
            // 
            // сообщениеToolStripMenuItem
            // 
            this.сообщениеToolStripMenuItem.Name = "сообщениеToolStripMenuItem";
            this.сообщениеToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.сообщениеToolStripMenuItem.Text = "Сообщение";
            this.сообщениеToolStripMenuItem.Click += new System.EventHandler(this.сообщениеToolStripMenuItem_Click);
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(257, 6);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(257, 6);
            // 
            // снятьДозвонToolStripMenuItem1
            // 
            this.снятьДозвонToolStripMenuItem1.Name = "снятьДозвонToolStripMenuItem1";
            this.снятьДозвонToolStripMenuItem1.Size = new System.Drawing.Size(260, 22);
            this.снятьДозвонToolStripMenuItem1.Text = "Снять дозвон";
            this.снятьДозвонToolStripMenuItem1.Click += new System.EventHandler(this.снятьДозвонToolStripMenuItem1_Click);
            // 
            // снятьТревогуToolStripMenuItem1
            // 
            this.снятьТревогуToolStripMenuItem1.Name = "снятьТревогуToolStripMenuItem1";
            this.снятьТревогуToolStripMenuItem1.Size = new System.Drawing.Size(260, 22);
            this.снятьТревогуToolStripMenuItem1.Text = "Снять тревогу";
            this.снятьТревогуToolStripMenuItem1.Visible = false;
            this.снятьТревогуToolStripMenuItem1.Click += new System.EventHandler(this.снятьТревогуToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(257, 6);
            // 
            // поставитьпослеОтменыToolStripMenuItem
            // 
            this.поставитьпослеОтменыToolStripMenuItem.Name = "поставитьпослеОтменыToolStripMenuItem";
            this.поставитьпослеОтменыToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.поставитьпослеОтменыToolStripMenuItem.Text = "Поставить \"после отмены\"";
            this.поставитьпослеОтменыToolStripMenuItem.Click += new System.EventHandler(this.поставитьпослеОтменыToolStripMenuItem_Click);
            // 
            // снятьпToolStripMenuItem
            // 
            this.снятьпToolStripMenuItem.Name = "снятьпToolStripMenuItem";
            this.снятьпToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.снятьпToolStripMenuItem.Text = "Снять \"после отмены\"";
            this.снятьпToolStripMenuItem.Visible = false;
            this.снятьпToolStripMenuItem.Click += new System.EventHandler(this.снятьпToolStripMenuItem_Click);
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(257, 6);
            // 
            // карточкаToolStripMenuItem
            // 
            this.карточкаToolStripMenuItem.Name = "карточкаToolStripMenuItem";
            this.карточкаToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.карточкаToolStripMenuItem.Text = "Карточка";
            this.карточкаToolStripMenuItem.Click += new System.EventHandler(this.карточкаToolStripMenuItem_Click);
            // 
            // toolStripMenuItem17
            // 
            this.toolStripMenuItem17.Name = "toolStripMenuItem17";
            this.toolStripMenuItem17.Size = new System.Drawing.Size(257, 6);
            // 
            // загрузитьМаршрутToolStripMenuItem
            // 
            this.загрузитьМаршрутToolStripMenuItem.Name = "загрузитьМаршрутToolStripMenuItem";
            this.загрузитьМаршрутToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.загрузитьМаршрутToolStripMenuItem.Text = "Загрузить маршрут";
            this.загрузитьМаршрутToolStripMenuItem.Click += new System.EventHandler(this.загрузитьМаршрутToolStripMenuItem_Click);
            // 
            // toolStripMenuItem18
            // 
            this.toolStripMenuItem18.Name = "toolStripMenuItem18";
            this.toolStripMenuItem18.Size = new System.Drawing.Size(257, 6);
            // 
            // поставитьПервымПослеОтменыToolStripMenuItem1
            // 
            this.поставитьПервымПослеОтменыToolStripMenuItem1.Name = "поставитьПервымПослеОтменыToolStripMenuItem1";
            this.поставитьПервымПослеОтменыToolStripMenuItem1.Size = new System.Drawing.Size(260, 22);
            this.поставитьПервымПослеОтменыToolStripMenuItem1.Text = "Поставить первым после отмены";
            this.поставитьПервымПослеОтменыToolStripMenuItem1.Click += new System.EventHandler(this.поставитьПервымПослеОтменыToolStripMenuItem1_Click);
            // 
            // view_raions
            // 
            this.view_raions.AllowUserToAddRows = false;
            this.view_raions.AllowUserToDeleteRows = false;
            this.view_raions.AllowUserToResizeColumns = false;
            this.view_raions.AllowUserToResizeRows = false;
            this.view_raions.BackgroundColor = System.Drawing.SystemColors.Control;
            this.view_raions.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.view_raions.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.view_raions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.view_raions.ColumnHeadersVisible = false;
            this.view_raions.ContextMenuStrip = this.mnuCars;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.view_raions.DefaultCellStyle = dataGridViewCellStyle4;
            this.view_raions.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.view_raions.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.view_raions.Location = new System.Drawing.Point(3, 15);
            this.view_raions.Name = "view_raions";
            this.view_raions.RowHeadersVisible = false;
            this.view_raions.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.view_raions.RowTemplate.Height = 18;
            this.view_raions.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.view_raions.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.view_raions.Size = new System.Drawing.Size(289, 426);
            this.view_raions.TabIndex = 1;
            this.view_raions.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.view_raions_RowPrePaint);
            this.view_raions.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.view_raions_CellMouseDown);
            this.view_raions.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.view_raions_RowPostPaint);
            this.view_raions.CellParsing += new System.Windows.Forms.DataGridViewCellParsingEventHandler(this.view_raions_CellParsing);
            this.view_raions.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.view_raions_CellFormatting);
            this.view_raions.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.view_raions_CellPainting);
            // 
            // tabCalls
            // 
            this.tabCalls.Controls.Add(this.groupBox2);
            this.tabCalls.Location = new System.Drawing.Point(4, 22);
            this.tabCalls.Name = "tabCalls";
            this.tabCalls.Padding = new System.Windows.Forms.Padding(3);
            this.tabCalls.Size = new System.Drawing.Size(295, 444);
            this.tabCalls.TabIndex = 1;
            this.tabCalls.Text = "Звонки";
            this.tabCalls.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnCallDial);
            this.groupBox2.Controls.Add(this.btnCallHangup);
            this.groupBox2.Controls.Add(this.btnCallHold);
            this.groupBox2.Controls.Add(this.listBox5);
            this.groupBox2.Location = new System.Drawing.Point(3, 102);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(321, 498);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "История звонков";
            // 
            // btnCallDial
            // 
            this.btnCallDial.Enabled = false;
            this.btnCallDial.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnCallDial.Location = new System.Drawing.Point(186, 81);
            this.btnCallDial.Margin = new System.Windows.Forms.Padding(2);
            this.btnCallDial.Name = "btnCallDial";
            this.btnCallDial.Size = new System.Drawing.Size(56, 19);
            this.btnCallDial.TabIndex = 5;
            this.btnCallDial.Text = "btnCallDial";
            this.btnCallDial.UseVisualStyleBackColor = true;
            this.btnCallDial.Click += new System.EventHandler(this.btnCallDial_Click);
            // 
            // btnCallHangup
            // 
            this.btnCallHangup.Enabled = false;
            this.btnCallHangup.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnCallHangup.Location = new System.Drawing.Point(186, 44);
            this.btnCallHangup.Margin = new System.Windows.Forms.Padding(2);
            this.btnCallHangup.Name = "btnCallHangup";
            this.btnCallHangup.Size = new System.Drawing.Size(56, 19);
            this.btnCallHangup.TabIndex = 4;
            this.btnCallHangup.Text = "Завершить";
            this.btnCallHangup.UseVisualStyleBackColor = true;
            // 
            // btnCallHold
            // 
            this.btnCallHold.Enabled = false;
            this.btnCallHold.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnCallHold.Location = new System.Drawing.Point(186, 13);
            this.btnCallHold.Margin = new System.Windows.Forms.Padding(2);
            this.btnCallHold.Name = "btnCallHold";
            this.btnCallHold.Size = new System.Drawing.Size(56, 19);
            this.btnCallHold.TabIndex = 3;
            this.btnCallHold.Text = "Hold";
            this.btnCallHold.UseVisualStyleBackColor = true;
            // 
            // listBox5
            // 
            this.listBox5.FormattingEnabled = true;
            this.listBox5.Location = new System.Drawing.Point(10, 37);
            this.listBox5.Margin = new System.Windows.Forms.Padding(2);
            this.listBox5.Name = "listBox5";
            this.listBox5.Size = new System.Drawing.Size(118, 69);
            this.listBox5.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.panCommands);
            this.panel1.Controls.Add(this.txtStatus);
            this.panel1.Controls.Add(this.button17);
            this.panel1.Controls.Add(this.SkippedCalls);
            this.panel1.Controls.Add(this.callBtnHold);
            this.panel1.Controls.Add(this.statusS);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.time_label);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(303, 143);
            this.panel1.TabIndex = 8;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(173, 159);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "label10";
            // 
            // panCommands
            // 
            this.panCommands.Location = new System.Drawing.Point(56, 240);
            this.panCommands.Name = "panCommands";
            this.panCommands.Size = new System.Drawing.Size(194, 20);
            this.panCommands.TabIndex = 8;
            // 
            // txtStatus
            // 
            this.txtStatus.AutoSize = true;
            this.txtStatus.Location = new System.Drawing.Point(10, 176);
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Size = new System.Drawing.Size(41, 13);
            this.txtStatus.TabIndex = 7;
            this.txtStatus.Text = "label10";
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(7, 145);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(75, 23);
            this.button17.TabIndex = 6;
            this.button17.Text = "button17";
            this.button17.UseVisualStyleBackColor = true;
            // 
            // SkippedCalls
            // 
            this.SkippedCalls.AutoSize = true;
            this.SkippedCalls.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold);
            this.SkippedCalls.ForeColor = System.Drawing.Color.Red;
            this.SkippedCalls.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.SkippedCalls.Location = new System.Drawing.Point(155, 10);
            this.SkippedCalls.Name = "SkippedCalls";
            this.SkippedCalls.Size = new System.Drawing.Size(30, 31);
            this.SkippedCalls.TabIndex = 5;
            this.SkippedCalls.Text = "0";
            // 
            // callBtnHold
            // 
            this.callBtnHold.Enabled = false;
            this.callBtnHold.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.callBtnHold.Location = new System.Drawing.Point(160, 123);
            this.callBtnHold.Name = "callBtnHold";
            this.callBtnHold.Size = new System.Drawing.Size(75, 20);
            this.callBtnHold.TabIndex = 4;
            this.callBtnHold.Text = "Перевести";
            this.callBtnHold.UseVisualStyleBackColor = true;
            this.callBtnHold.Visible = false;
            // 
            // statusS
            // 
            this.statusS.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton1,
            this.statusbar});
            this.statusS.Location = new System.Drawing.Point(0, 121);
            this.statusS.Name = "statusS";
            this.statusS.Size = new System.Drawing.Size(303, 22);
            this.statusS.TabIndex = 2;
            this.statusS.Text = "statusStrip1";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(29, 20);
            this.toolStripDropDownButton1.Text = "toolStripDropDownButton1";
            // 
            // statusbar
            // 
            this.statusbar.BackColor = System.Drawing.Color.Red;
            this.statusbar.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.statusbar.Name = "statusbar";
            this.statusbar.Size = new System.Drawing.Size(0, 17);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.callBtnHangup);
            this.groupBox1.Controls.Add(this.btnRestartSip);
            this.groupBox1.Controls.Add(this.callBtnDial);
            this.groupBox1.Controls.Add(this.callPhone);
            this.groupBox1.Controls.Add(this.btnSettings);
            this.groupBox1.Location = new System.Drawing.Point(7, 43);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(244, 79);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Телефон";
            // 
            // callBtnHangup
            // 
            this.callBtnHangup.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.callBtnHangup.Location = new System.Drawing.Point(87, 51);
            this.callBtnHangup.Name = "callBtnHangup";
            this.callBtnHangup.Size = new System.Drawing.Size(75, 20);
            this.callBtnHangup.TabIndex = 3;
            this.callBtnHangup.Text = "Завершить";
            this.callBtnHangup.UseVisualStyleBackColor = true;
            this.callBtnHangup.Click += new System.EventHandler(this.callBtnHangup_Click);
            // 
            // btnRestartSip
            // 
            this.btnRestartSip.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F);
            this.btnRestartSip.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnRestartSip.Location = new System.Drawing.Point(188, 19);
            this.btnRestartSip.Name = "btnRestartSip";
            this.btnRestartSip.Size = new System.Drawing.Size(52, 26);
            this.btnRestartSip.TabIndex = 3;
            this.btnRestartSip.Text = "Сброс \r\nзвонилки";
            this.btnRestartSip.UseVisualStyleBackColor = true;
            this.btnRestartSip.Click += new System.EventHandler(this.btnRestartSip_Click);
            // 
            // callBtnDial
            // 
            this.callBtnDial.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.callBtnDial.Location = new System.Drawing.Point(6, 51);
            this.callBtnDial.Name = "callBtnDial";
            this.callBtnDial.Size = new System.Drawing.Size(75, 20);
            this.callBtnDial.TabIndex = 1;
            this.callBtnDial.Text = "Набрать";
            this.callBtnDial.UseVisualStyleBackColor = true;
            this.callBtnDial.Click += new System.EventHandler(this.callBtnDial_Click);
            // 
            // callPhone
            // 
            this.callPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.callPhone.Location = new System.Drawing.Point(6, 19);
            this.callPhone.Name = "callPhone";
            this.callPhone.Size = new System.Drawing.Size(165, 26);
            this.callPhone.TabIndex = 0;
            // 
            // btnSettings
            // 
            this.btnSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F);
            this.btnSettings.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnSettings.Location = new System.Drawing.Point(189, 58);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(51, 18);
            this.btnSettings.TabIndex = 1;
            this.btnSettings.Text = "Настройки";
            this.btnSettings.UseVisualStyleBackColor = true;
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // time_label
            // 
            this.time_label.AutoSize = true;
            this.time_label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.time_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F);
            this.time_label.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.time_label.Location = new System.Drawing.Point(3, 3);
            this.time_label.Name = "time_label";
            this.time_label.Size = new System.Drawing.Size(145, 38);
            this.time_label.TabIndex = 0;
            this.time_label.Text = "                ";
            // 
            // timer2
            // 
            this.timer2.Interval = 1000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // post_order_timer
            // 
            this.post_order_timer.Interval = 3000;
            this.post_order_timer.Tick += new System.EventHandler(this.post_order_timer_Tick);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // backgroundWorker2
            // 
            this.backgroundWorker2.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker2_DoWork);
            this.backgroundWorker2.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker2_RunWorkerCompleted);
            // 
            // backgroundWorker3
            // 
            this.backgroundWorker3.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker3_DoWork);
            this.backgroundWorker3.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker3_RunWorkerCompleted);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "pin-green.png");
            this.imageList1.Images.SetKeyName(1, "pin-blue.png");
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "NewDataSet";
            this.dataSet1.Tables.AddRange(new System.Data.DataTable[] {
            this.report_orders,
            this.dataTable1});
            // 
            // report_orders
            // 
            this.report_orders.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn9,
            this.dataColumn13,
            this.ord_date,
            this.dataColumn14});
            this.report_orders.TableName = "report_orders";
            // 
            // dataColumn1
            // 
            this.dataColumn1.ColumnName = "id";
            this.dataColumn1.DataType = typeof(int);
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "dtEnd";
            this.dataColumn2.DataType = typeof(System.Data.SqlTypes.SqlDateTime);
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "ADDRFROM";
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "phone";
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "addrTo";
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "clientname";
            // 
            // dataColumn7
            // 
            this.dataColumn7.ColumnName = "sum";
            this.dataColumn7.DataType = typeof(short);
            // 
            // dataColumn8
            // 
            this.dataColumn8.ColumnName = "carid";
            this.dataColumn8.DataType = typeof(short);
            // 
            // dataColumn9
            // 
            this.dataColumn9.ColumnName = "SHORT_ADDRFROM";
            // 
            // dataColumn13
            // 
            this.dataColumn13.Caption = "Отдел";
            this.dataColumn13.ColumnName = "ORGANIZATION_DETAIL_NAME";
            // 
            // ord_date
            // 
            this.ord_date.ColumnName = "ord_date";
            this.ord_date.DataType = typeof(System.Data.SqlTypes.SqlDateTime);
            // 
            // dataColumn14
            // 
            this.dataColumn14.ColumnName = "interpoints";
            this.dataColumn14.DefaultValue = "";
            // 
            // dataTable1
            // 
            this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn10,
            this.dataColumn11,
            this.dataColumn12});
            this.dataTable1.TableName = "report_params";
            // 
            // dataColumn10
            // 
            this.dataColumn10.ColumnName = "organization_name";
            // 
            // dataColumn11
            // 
            this.dataColumn11.ColumnName = "dtFrom";
            // 
            // dataColumn12
            // 
            this.dataColumn12.ColumnName = "dtTo";
            // 
            // report1
            // 
            this.report1.ReportResourceString = resources.GetString("report1.ReportResourceString");
            this.report1.RegisterData(this.dataSet1, "dataSet1");
            // 
            // report2
            // 
            this.report2.ReportResourceString = resources.GetString("report2.ReportResourceString");
            this.report2.RegisterData(this.dataSet1, "dataSet1");
            // 
            // report3
            // 
            this.report3.ReportResourceString = resources.GetString("report3.ReportResourceString");
            this.report3.RegisterData(this.dataSet1, "dataSet1");
            // 
            // report4
            // 
            this.report4.ReportResourceString = resources.GetString("report4.ReportResourceString");
            this.report4.RegisterData(this.dataSet1, "dataSet1");
            // 
            // report5
            // 
            this.report5.ReportResourceString = resources.GetString("report5.ReportResourceString");
            this.report5.RegisterData(this.dataSet1, "dataSet1");
            // 
            // check_call_state
            // 
            this.check_call_state.Enabled = true;
            this.check_call_state.Interval = 1000;
            this.check_call_state.Tick += new System.EventHandler(this.check_call_state_Tick);
            // 
            // MAIN_FORM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1122, 613);
            this.Controls.Add(this.splitContainer1);
            this.KeyPreview = true;
            this.Name = "MAIN_FORM";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Диспетчер 2.0. Прием заявок такси";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MAIN_FORM_KeyDown);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.leftTab.ResumeLayout(false);
            this.tabOrders.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.view_work_orders)).EndInit();
            this.mnuViewWorkOrders.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.filterPanel.ResumeLayout(false);
            this.filterPanel.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.tabMap.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabPage8.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.tabControl4.ResumeLayout(false);
            this.tabPage9.ResumeLayout(false);
            this.tabPage10.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.tabControl5.ResumeLayout(false);
            this.tabPage11.ResumeLayout(false);
            this.rightTabControl.ResumeLayout(false);
            this.tabCars.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.view_cars)).EndInit();
            this.mnuCars.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.view_raions)).EndInit();
            this.tabCalls.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusS.ResumeLayout(false);
            this.statusS.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.report_orders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.report1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.report2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.report3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.report4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.report5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem маршрутОтсюдаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem маршрутСюдаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem промежуточнаяТочка1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem промежуточнаяТочка2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem очиститьМаршрутToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem очиститьТочку1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem очиститьПромежуточнуюТочку2ToolStripMenuItem;
        private System.Windows.Forms.TabControl leftTab;
        private System.Windows.Forms.TabPage tabOrders;
        private System.Windows.Forms.TabPage tabMap;
        private System.Windows.Forms.TabControl rightTabControl;
        private System.Windows.Forms.TabPage tabCars;
        private System.Windows.Forms.TabPage tabCalls;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button callBtnHold;
        private System.Windows.Forms.Button callBtnHangup;
        private System.Windows.Forms.Button callBtnDial;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TabControl tabControl4;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TabControl tabControl5;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.Windows.Forms.DateTimePicker dateTimePicker4;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.ListBox listBox5;
        public System.Windows.Forms.Button btnCallHold;
        public System.Windows.Forms.Button btnCallDial;
        public System.Windows.Forms.Button btnCallHangup;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.DataGridView view_raions;
        private System.Windows.Forms.DataGridView view_cars;
        private System.Windows.Forms.ContextMenuStrip mnuViewWorkOrders;
        private System.Windows.Forms.ToolStripMenuItem miOrderEdit;
        private System.Windows.Forms.ToolStripMenuItem завершитьЗаказToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miEndOrder1;
        private System.Windows.Forms.ToolStripMenuItem mnuEndOrder2;
        private System.Windows.Forms.ToolStripMenuItem miEndOrder3;
        private System.Windows.Forms.TextBox path_info;
        public System.Windows.Forms.ComboBox sip_status_combo;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem miNewOrder;
        private System.Windows.Forms.Timer post_order_timer;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem звлнкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnuDialCar;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem mnuDialClient;
        private System.Windows.Forms.ContextMenuStrip mnuCars;
        private System.Windows.Forms.ToolStripMenuItem позвонитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem администрированиеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem дублироватьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem закрытьИДублироватьToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.ComponentModel.BackgroundWorker backgroundWorker2;
        private System.ComponentModel.BackgroundWorker backgroundWorker3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label time_label;
        private System.Windows.Forms.ComboBox cmd_filter_orders;
        private System.Windows.Forms.DataGridView view_work_orders;
        private System.Windows.Forms.Button btnSettings;
        private System.Windows.Forms.StatusStrip statusS;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        public System.Windows.Forms.ToolStripStatusLabel statusbar;
        private System.Windows.Forms.ToolStripMenuItem нетМашинToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem снятToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сообщениеToolStripMenuItem;
        private System.Windows.Forms.Button btnDisableAlarm;
        private System.Windows.Forms.Button btnPromisedPayment;
        private System.Windows.Forms.Panel filterPanel;
        private System.Windows.Forms.Button btnRefreshOrders;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btn1Month;
        private System.Windows.Forms.Button btn7Day;
        private System.Windows.Forms.Button btn1Day;
        private System.Windows.Forms.DateTimePicker filterTm2;
        private System.Windows.Forms.DateTimePicker filterTm1;
        private System.Windows.Forms.DateTimePicker filterDt2;
        private System.Windows.Forms.DateTimePicker filterDt1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox filterAddress;
        private System.Windows.Forms.TextBox filterPhone;
        private System.Windows.Forms.ComboBox filterOrg;
        private System.Windows.Forms.ComboBox filterCarService;
        private System.Windows.Forms.ComboBox filterOrderService;
        private System.Windows.Forms.ComboBox filterCar;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem положитьСуммуПоЗаказуВодителюToolStripMenuItem;
        private System.Windows.Forms.CheckBox chkMyOrders;
        private System.Windows.Forms.Label label_pwo_timer;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripMenuItem снятьДозвонToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem снятьТревогуToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem поставитьпослеОтменыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem снятьпToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem9;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem10;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem8;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckedListBox chkMapShowOrders;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox FFilterPos;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox fFilterAddrPhone;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox chkFreeOrders;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button btnRestartSip;
        public System.Windows.Forms.TextBox callPhone;
        private System.Windows.Forms.Button btnPhonesList;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem14;
        private System.Windows.Forms.ToolStripMenuItem вЧерныйСписокToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem11;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem12;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem13;
        private System.Windows.Forms.CheckBox Отмененные;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnSaveZones;
        private System.Windows.Forms.Button btnLoadZones;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.ToolStripMenuItem yjdsqToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem редактироватьНоваяФормаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem редактироватьРайонToolStripMenuItem;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ToolStripMenuItem удалитьТочкуToolStripMenuItem;
        private System.Windows.Forms.Button btnLoadGisModuleData;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem15;
        private System.Windows.Forms.ToolStripMenuItem закрытьЧерныйСписокToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem16;
        private System.Windows.Forms.ToolStripMenuItem перевестиВУспешноЗавершеныеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem карточкаToolStripMenuItem;
        private System.Windows.Forms.Button btn_driver_card;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem17;
        private System.Windows.Forms.ToolStripMenuItem загрузитьМаршрутToolStripMenuItem;
        public csmapcontrol.MapControl mapControl1;
        private System.Windows.Forms.Label SkippedCalls;
        private System.Windows.Forms.ToolStripMenuItem завершитьСНазначениемВодителяToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem18;
        private System.Windows.Forms.ToolStripMenuItem поставитьПервымПослеОтменыToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem снятьпоторопитеToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem19;
        private System.Windows.Forms.ToolStripMenuItem перевестиВзаказВРаботеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem поторопитеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem снятьпоторопитеToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem поставитьпоторопитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem снятьпоторопитеИОтправитьВодителюторопимToolStripMenuItem;
        private System.Windows.Forms.Button buttoт_dic_organizations;
        private System.Data.DataSet dataSet1;
        private System.Data.DataTable report_orders;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Windows.Forms.Button btn_print;
        private System.Data.DataColumn dataColumn6;
        private System.Data.DataColumn dataColumn7;
        private System.Data.DataColumn dataColumn8;
        private System.Data.DataColumn dataColumn9;
        private System.Data.DataTable dataTable1;
        private System.Data.DataColumn dataColumn10;
        private System.Data.DataColumn dataColumn11;
        private System.Data.DataColumn dataColumn12;
        private FastReport.Report report1;
        private System.Windows.Forms.CheckBox Успешные;
        private System.Windows.Forms.Button загрузитьгород;
        private System.Windows.Forms.Button сщхранитьгород;
        private System.Windows.Forms.CheckBox chkBnal;
        private System.Windows.Forms.CheckBox chkUnclosedBnal;
        private System.Windows.Forms.Button button13;
        private FastReport.Report report2;
        private System.Data.DataColumn dataColumn13;
        private System.Windows.Forms.Button button14;
        private FastReport.Report report3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker bnal_opl_tm;
        private System.Windows.Forms.Button btnReview;
        private System.Windows.Forms.Button btn_dic_gis_addresses;
        private System.Windows.Forms.Button button15;
        private FastReport.Report report4;
        private System.Data.DataColumn ord_date;
        private System.Windows.Forms.Button btnAktSverki;
        private System.Windows.Forms.ToolStripMenuItem ВернутьЗаказВРаботу;
        private System.Windows.Forms.Button button16;
        private FastReport.Report report5;
        private System.Data.DataColumn dataColumn14;
        private System.Windows.Forms.ToolStripMenuItem таксометрToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem включитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выключитьToolStripMenuItem;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Panel panCommands;
        private System.Windows.Forms.Label txtStatus;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Timer check_call_state;
        private System.Windows.Forms.ToolStripMenuItem назначениеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem назначитьВодителяToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem снятьВодителяСЗаказаToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem попиликатьУВодителяToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem предложитьЗаказВодителюОкошкомToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem полноеРедактированиеЗаказаToolStripMenuItem;
        private System.Windows.Forms.Button btnArendaCars;
    }
}

