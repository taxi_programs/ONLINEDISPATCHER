﻿namespace ONLINEDISPATCHER
{
    partial class order_big
    {
        /// <summary>
        ///Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(order_big));
            this.ServiceID = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.ClientID = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.OrgID = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.CarID = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.Descr = new System.Windows.Forms.TextBox();
            this.order_prim_menu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnSave = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnEndCall = new System.Windows.Forms.Button();
            this.chkPredvar = new System.Windows.Forms.CheckBox();
            this.dtPredvar = new System.Windows.Forms.DateTimePicker();
            this.tmPredvar = new System.Windows.Forms.DateTimePicker();
            this.dt_order_info = new System.Windows.Forms.Label();
            this.FromPhone = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.ORGDETAIL_ID = new System.Windows.Forms.ComboBox();
            this.chk_EndOrder = new System.Windows.Forms.CheckBox();
            this.comboEndState = new System.Windows.Forms.ComboBox();
            this.btnEndOrder = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.summ = new System.Windows.Forms.TextBox();
            this.btnSumtoDriver = new System.Windows.Forms.Button();
            this.CarIDView = new System.Windows.Forms.TextBox();
            this.btnCarDial = new System.Windows.Forms.Button();
            this.btnCarMessage = new System.Windows.Forms.Button();
            this.TalonNumber = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.расчетМаршрута = new System.Windows.Forms.Button();
            this.group_beznal = new System.Windows.Forms.GroupBox();
            this.org_subdetail_prim = new System.Windows.Forms.Label();
            this.org_detail_prim = new System.Windows.Forms.Label();
            this.ORGSUBDETAIL_ID = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.month_limit = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.bnal_opl_tm = new System.Windows.Forms.DateTimePicker();
            this.org_prim = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.DistanceMoney = new System.Windows.Forms.Label();
            this.predvShowDelta = new System.Windows.Forms.ComboBox();
            this.group_calculator = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.Inerpoint4_name = new System.Windows.Forms.ComboBox();
            this.Inerpoint3_name = new System.Windows.Forms.ComboBox();
            this.Inerpoint4_address = new System.Windows.Forms.ComboBox();
            this.Inerpoint3_address = new System.Windows.Forms.ComboBox();
            this.Inerpoint3_house = new System.Windows.Forms.TextBox();
            this.Inerpoint4_house = new System.Windows.Forms.TextBox();
            this.Inerpoint2_name = new System.Windows.Forms.ComboBox();
            this.Inerpoint1_name = new System.Windows.Forms.ComboBox();
            this.Inerpoint2_address = new System.Windows.Forms.ComboBox();
            this.Inerpoint1_address = new System.Windows.Forms.ComboBox();
            this.Inerpoint1_house = new System.Windows.Forms.TextBox();
            this.Inerpoint2_house = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.wait_time = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.distanceMgor = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.DistanceCity = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.black_list_car_id = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.black_list_money = new System.Windows.Forms.TextBox();
            this.calc_check_black_list = new System.Windows.Forms.CheckBox();
            this.calc_check_4 = new System.Windows.Forms.CheckBox();
            this.calc_check_3 = new System.Windows.Forms.CheckBox();
            this.calc_check_2 = new System.Windows.Forms.CheckBox();
            this.calc_check_1 = new System.Windows.Forms.CheckBox();
            this.label27 = new System.Windows.Forms.Label();
            this.AddrTo = new System.Windows.Forms.ComboBox();
            this.calculator_text = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.HouseTo2 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.group_header = new System.Windows.Forms.GroupBox();
            this.order_10percent = new System.Windows.Forms.CheckBox();
            this.btnMakeAbonent = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.phone_info = new System.Windows.Forms.Label();
            this.chk_close_not_saved = new System.Windows.Forms.CheckBox();
            this.btn_fix_to = new System.Windows.Forms.Button();
            this.btn_fix_from = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.ZoneID = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Phone = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ConnectPhone = new System.Windows.Forms.TextBox();
            this.AddrFrom = new System.Windows.Forms.TextBox();
            this.HouseFrom = new System.Windows.Forms.TextBox();
            this.FlatFrom = new System.Windows.Forms.TextBox();
            this.AddrFromName = new System.Windows.Forms.ComboBox();
            this.btnPhone2Dial = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.btnPhone1Dial = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.AddrToName = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.RaionToID = new System.Windows.Forms.ComboBox();
            this.btnPhoneInfo = new System.Windows.Forms.Button();
            this.lbPhoneInfo = new System.Windows.Forms.ListBox();
            this.AddrToForDriver = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnMakeReview = new System.Windows.Forms.Button();
            this.chk_beznal = new System.Windows.Forms.CheckBox();
            this.chk_calculator = new System.Windows.Forms.CheckBox();
            this.button4 = new System.Windows.Forms.Button();
            this.group_order_history_info = new System.Windows.Forms.GroupBox();
            this.label_taxometr_info = new System.Windows.Forms.Label();
            this.group_footer = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chk_podacha = new System.Windows.Forms.CheckBox();
            this.btnOkDialToPassenger = new System.Windows.Forms.Button();
            this.order_price_Descr = new System.Windows.Forms.TextBox();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.btnДанные = new System.Windows.Forms.Button();
            this.group_beznal.SuspendLayout();
            this.group_calculator.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.group_header.SuspendLayout();
            this.group_order_history_info.SuspendLayout();
            this.group_footer.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ServiceID
            // 
            this.ServiceID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ServiceID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ServiceID.FormattingEnabled = true;
            this.ServiceID.Location = new System.Drawing.Point(400, 184);
            this.ServiceID.Name = "ServiceID";
            this.ServiceID.Size = new System.Drawing.Size(117, 28);
            this.ServiceID.TabIndex = 2;
            this.ServiceID.TabStop = false;
            this.ServiceID.SelectedIndexChanged += new System.EventHandler(this.ServiceID_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(344, 193);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 18);
            this.label7.TabIndex = 12;
            this.label7.Text = "Служба";
            // 
            // ClientID
            // 
            this.ClientID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ClientID.Location = new System.Drawing.Point(375, 47);
            this.ClientID.Name = "ClientID";
            this.ClientID.Size = new System.Drawing.Size(195, 30);
            this.ClientID.TabIndex = 10;
            this.ClientID.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(371, 34);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 17);
            this.label11.TabIndex = 22;
            this.label11.Text = "Пассажир";
            // 
            // OrgID
            // 
            this.OrgID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.OrgID.FormattingEnabled = true;
            this.OrgID.Location = new System.Drawing.Point(8, 26);
            this.OrgID.Name = "OrgID";
            this.OrgID.Size = new System.Drawing.Size(261, 33);
            this.OrgID.TabIndex = 0;
            this.OrgID.TabStop = false;
            this.OrgID.SelectedIndexChanged += new System.EventHandler(this.OrgID_SelectedIndexChanged);
            this.OrgID.TextChanged += new System.EventHandler(this.OrgID_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(11, 12);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(95, 17);
            this.label12.TabIndex = 20;
            this.label12.Text = "Организация";
            // 
            // CarID
            // 
            this.CarID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CarID.FormattingEnabled = true;
            this.CarID.Location = new System.Drawing.Point(6, 117);
            this.CarID.Name = "CarID";
            this.CarID.Size = new System.Drawing.Size(407, 28);
            this.CarID.TabIndex = 14;
            this.CarID.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(5, 99);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(95, 18);
            this.label13.TabIndex = 24;
            this.label13.Text = "Автомобиль";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.ForeColor = System.Drawing.Color.Blue;
            this.label16.Location = new System.Drawing.Point(5, 10);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(233, 17);
            this.label16.TabIndex = 30;
            this.label16.Text = "Примечание для водителя и цена";
            // 
            // Descr
            // 
            this.Descr.ContextMenuStrip = this.order_prim_menu;
            this.Descr.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Descr.Location = new System.Drawing.Point(5, 31);
            this.Descr.MaxLength = 250;
            this.Descr.Multiline = true;
            this.Descr.Name = "Descr";
            this.Descr.Size = new System.Drawing.Size(604, 43);
            this.Descr.TabIndex = 0;
            // 
            // order_prim_menu
            // 
            this.order_prim_menu.Name = "order_prim_menu";
            this.order_prim_menu.Size = new System.Drawing.Size(61, 4);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.Control;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.ImageKey = "document-save.png";
            this.btnSave.ImageList = this.imageList1;
            this.btnSave.Location = new System.Drawing.Point(7, 13);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(262, 32);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "На распределение/Сохранить (F2)";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Add.ico");
            this.imageList1.Images.SetKeyName(1, "Delete.ico");
            this.imageList1.Images.SetKeyName(2, "Earth.ico");
            this.imageList1.Images.SetKeyName(3, "Info.ico");
            this.imageList1.Images.SetKeyName(4, "29.ico");
            this.imageList1.Images.SetKeyName(5, "39.ico");
            this.imageList1.Images.SetKeyName(6, "call-start.png");
            this.imageList1.Images.SetKeyName(7, "documentinfo.png");
            this.imageList1.Images.SetKeyName(8, "mail-forward.png");
            this.imageList1.Images.SetKeyName(9, "call-stop.png");
            this.imageList1.Images.SetKeyName(10, "document-save.png");
            this.imageList1.Images.SetKeyName(11, "phone_blue.png");
            // 
            // btnEndCall
            // 
            this.btnEndCall.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnEndCall.ForeColor = System.Drawing.Color.Red;
            this.btnEndCall.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEndCall.ImageKey = "call-stop.png";
            this.btnEndCall.ImageList = this.imageList1;
            this.btnEndCall.Location = new System.Drawing.Point(348, 13);
            this.btnEndCall.Name = "btnEndCall";
            this.btnEndCall.Size = new System.Drawing.Size(249, 32);
            this.btnEndCall.TabIndex = 11;
            this.btnEndCall.TabStop = false;
            this.btnEndCall.Text = "Завершить разговор (F4)";
            this.btnEndCall.UseVisualStyleBackColor = true;
            this.btnEndCall.Visible = false;
            this.btnEndCall.Click += new System.EventHandler(this.btnEndCall_Click);
            // 
            // chkPredvar
            // 
            this.chkPredvar.AutoSize = true;
            this.chkPredvar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chkPredvar.Location = new System.Drawing.Point(269, 11);
            this.chkPredvar.Name = "chkPredvar";
            this.chkPredvar.Size = new System.Drawing.Size(76, 21);
            this.chkPredvar.TabIndex = 15;
            this.chkPredvar.TabStop = false;
            this.chkPredvar.Text = "предв.";
            this.chkPredvar.UseVisualStyleBackColor = true;
            this.chkPredvar.CheckStateChanged += new System.EventHandler(this.chkPredvar_CheckStateChanged);
            this.chkPredvar.CheckedChanged += new System.EventHandler(this.chkPredvar_CheckedChanged);
            // 
            // dtPredvar
            // 
            this.dtPredvar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dtPredvar.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtPredvar.Location = new System.Drawing.Point(322, 6);
            this.dtPredvar.Name = "dtPredvar";
            this.dtPredvar.Size = new System.Drawing.Size(111, 30);
            this.dtPredvar.TabIndex = 18;
            this.dtPredvar.TabStop = false;
            this.dtPredvar.Visible = false;
            // 
            // tmPredvar
            // 
            this.tmPredvar.CustomFormat = "HH:mm";
            this.tmPredvar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tmPredvar.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.tmPredvar.Location = new System.Drawing.Point(439, 6);
            this.tmPredvar.Name = "tmPredvar";
            this.tmPredvar.ShowUpDown = true;
            this.tmPredvar.Size = new System.Drawing.Size(78, 30);
            this.tmPredvar.TabIndex = 19;
            this.tmPredvar.TabStop = false;
            this.tmPredvar.Visible = false;
            // 
            // dt_order_info
            // 
            this.dt_order_info.Location = new System.Drawing.Point(7, 13);
            this.dt_order_info.Name = "dt_order_info";
            this.dt_order_info.Size = new System.Drawing.Size(234, 76);
            this.dt_order_info.TabIndex = 0;
            this.dt_order_info.Text = "Поступил:\r\nТолько что  asdfas ldfuasb duibas uigbsaukbg skadf asi babs fasbd kjba" +
                "skjb f";
            // 
            // FromPhone
            // 
            this.FromPhone.AutoSize = true;
            this.FromPhone.BackColor = System.Drawing.SystemColors.Control;
            this.FromPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FromPhone.Location = new System.Drawing.Point(518, 187);
            this.FromPhone.Name = "FromPhone";
            this.FromPhone.Size = new System.Drawing.Size(63, 20);
            this.FromPhone.TabIndex = 41;
            this.FromPhone.Text = "423535";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label23.Location = new System.Drawing.Point(14, 52);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(121, 17);
            this.label23.TabIndex = 42;
            this.label23.Text = "Сотрудник/отдел";
            // 
            // ORGDETAIL_ID
            // 
            this.ORGDETAIL_ID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.ORGDETAIL_ID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.ORGDETAIL_ID.DropDownHeight = 350;
            this.ORGDETAIL_ID.DropDownWidth = 450;
            this.ORGDETAIL_ID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ORGDETAIL_ID.FormattingEnabled = true;
            this.ORGDETAIL_ID.IntegralHeight = false;
            this.ORGDETAIL_ID.Location = new System.Drawing.Point(6, 64);
            this.ORGDETAIL_ID.MaxDropDownItems = 15;
            this.ORGDETAIL_ID.Name = "ORGDETAIL_ID";
            this.ORGDETAIL_ID.Size = new System.Drawing.Size(263, 33);
            this.ORGDETAIL_ID.Sorted = true;
            this.ORGDETAIL_ID.TabIndex = 1;
            this.ORGDETAIL_ID.TabStop = false;
            this.ORGDETAIL_ID.SelectedIndexChanged += new System.EventHandler(this.ORGDETAIL_ID_SelectedIndexChanged);
            // 
            // chk_EndOrder
            // 
            this.chk_EndOrder.AutoSize = true;
            this.chk_EndOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chk_EndOrder.Location = new System.Drawing.Point(478, 74);
            this.chk_EndOrder.Name = "chk_EndOrder";
            this.chk_EndOrder.Size = new System.Drawing.Size(147, 22);
            this.chk_EndOrder.TabIndex = 1;
            this.chk_EndOrder.TabStop = false;
            this.chk_EndOrder.Text = "Завершить заказ";
            this.chk_EndOrder.UseVisualStyleBackColor = true;
            this.chk_EndOrder.CheckedChanged += new System.EventHandler(this.chk_EndOrder_CheckedChanged);
            // 
            // comboEndState
            // 
            this.comboEndState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboEndState.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboEndState.FormattingEnabled = true;
            this.comboEndState.Items.AddRange(new object[] {
            "Успешно",
            "Снят (отказ пассажира)",
            "Снят (отказ водителя)"});
            this.comboEndState.Location = new System.Drawing.Point(478, 44);
            this.comboEndState.Name = "comboEndState";
            this.comboEndState.Size = new System.Drawing.Size(130, 28);
            this.comboEndState.TabIndex = 44;
            this.comboEndState.TabStop = false;
            this.comboEndState.Visible = false;
            // 
            // btnEndOrder
            // 
            this.btnEndOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnEndOrder.Location = new System.Drawing.Point(478, 14);
            this.btnEndOrder.Name = "btnEndOrder";
            this.btnEndOrder.Size = new System.Drawing.Size(129, 24);
            this.btnEndOrder.TabIndex = 45;
            this.btnEndOrder.TabStop = false;
            this.btnEndOrder.Text = "Завершить заказ";
            this.btnEndOrder.UseVisualStyleBackColor = true;
            this.btnEndOrder.Visible = false;
            this.btnEndOrder.Click += new System.EventHandler(this.btnEndOrder_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(448, 36);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(75, 25);
            this.label17.TabIndex = 46;
            this.label17.Text = "сумма";
            // 
            // summ
            // 
            this.summ.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.summ.Location = new System.Drawing.Point(504, 33);
            this.summ.Name = "summ";
            this.summ.Size = new System.Drawing.Size(72, 30);
            this.summ.TabIndex = 47;
            this.summ.TabStop = false;
            // 
            // btnSumtoDriver
            // 
            this.btnSumtoDriver.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSumtoDriver.ImageKey = "mail-forward.png";
            this.btnSumtoDriver.ImageList = this.imageList1;
            this.btnSumtoDriver.Location = new System.Drawing.Point(582, 35);
            this.btnSumtoDriver.Name = "btnSumtoDriver";
            this.btnSumtoDriver.Size = new System.Drawing.Size(23, 23);
            this.btnSumtoDriver.TabIndex = 48;
            this.btnSumtoDriver.TabStop = false;
            this.btnSumtoDriver.UseVisualStyleBackColor = true;
            this.btnSumtoDriver.Visible = false;
            this.btnSumtoDriver.Click += new System.EventHandler(this.btnSumtoDriver_Click);
            // 
            // CarIDView
            // 
            this.CarIDView.BackColor = System.Drawing.Color.LightBlue;
            this.CarIDView.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CarIDView.Location = new System.Drawing.Point(5, 118);
            this.CarIDView.Name = "CarIDView";
            this.CarIDView.ReadOnly = true;
            this.CarIDView.Size = new System.Drawing.Size(411, 26);
            this.CarIDView.TabIndex = 49;
            // 
            // btnCarDial
            // 
            this.btnCarDial.ImageKey = "29.ico";
            this.btnCarDial.ImageList = this.imageList1;
            this.btnCarDial.Location = new System.Drawing.Point(429, 116);
            this.btnCarDial.Name = "btnCarDial";
            this.btnCarDial.Size = new System.Drawing.Size(26, 26);
            this.btnCarDial.TabIndex = 50;
            this.btnCarDial.TabStop = false;
            this.btnCarDial.UseVisualStyleBackColor = true;
            this.btnCarDial.Click += new System.EventHandler(this.btnCarDial_Click);
            // 
            // btnCarMessage
            // 
            this.btnCarMessage.ImageKey = "39.ico";
            this.btnCarMessage.ImageList = this.imageList1;
            this.btnCarMessage.Location = new System.Drawing.Point(461, 117);
            this.btnCarMessage.Name = "btnCarMessage";
            this.btnCarMessage.Size = new System.Drawing.Size(26, 26);
            this.btnCarMessage.TabIndex = 51;
            this.btnCarMessage.TabStop = false;
            this.btnCarMessage.UseVisualStyleBackColor = true;
            this.btnCarMessage.Click += new System.EventHandler(this.btnCarMessage_Click);
            // 
            // TalonNumber
            // 
            this.TalonNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TalonNumber.Location = new System.Drawing.Point(504, 4);
            this.TalonNumber.MaxLength = 10;
            this.TalonNumber.Name = "TalonNumber";
            this.TalonNumber.Size = new System.Drawing.Size(101, 30);
            this.TalonNumber.TabIndex = 55;
            this.TalonNumber.TabStop = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label18.Location = new System.Drawing.Point(444, 10);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(68, 25);
            this.label18.TabIndex = 54;
            this.label18.Text = "Талон";
            // 
            // расчетМаршрута
            // 
            this.расчетМаршрута.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.расчетМаршрута.Location = new System.Drawing.Point(7, 180);
            this.расчетМаршрута.Name = "расчетМаршрута";
            this.расчетМаршрута.Size = new System.Drawing.Size(158, 24);
            this.расчетМаршрута.TabIndex = 0;
            this.расчетМаршрута.TabStop = false;
            this.расчетМаршрута.Tag = "";
            this.расчетМаршрута.Text = "РАСЧЕТ (F5)";
            this.расчетМаршрута.UseVisualStyleBackColor = true;
            this.расчетМаршрута.Click += new System.EventHandler(this.расчетМаршрута_Click);
            // 
            // group_beznal
            // 
            this.group_beznal.AutoSize = true;
            this.group_beznal.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.group_beznal.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.group_beznal.Controls.Add(this.org_subdetail_prim);
            this.group_beznal.Controls.Add(this.org_detail_prim);
            this.group_beznal.Controls.Add(this.ORGSUBDETAIL_ID);
            this.group_beznal.Controls.Add(this.label26);
            this.group_beznal.Controls.Add(this.month_limit);
            this.group_beznal.Controls.Add(this.label25);
            this.group_beznal.Controls.Add(this.bnal_opl_tm);
            this.group_beznal.Controls.Add(this.org_prim);
            this.group_beznal.Controls.Add(this.ORGDETAIL_ID);
            this.group_beznal.Controls.Add(this.OrgID);
            this.group_beznal.Controls.Add(this.label23);
            this.group_beznal.Controls.Add(this.TalonNumber);
            this.group_beznal.Controls.Add(this.label18);
            this.group_beznal.Controls.Add(this.summ);
            this.group_beznal.Controls.Add(this.label17);
            this.group_beznal.Controls.Add(this.btnSumtoDriver);
            this.group_beznal.Controls.Add(this.label12);
            this.group_beznal.Dock = System.Windows.Forms.DockStyle.Top;
            this.group_beznal.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.group_beznal.Location = new System.Drawing.Point(0, 439);
            this.group_beznal.Name = "group_beznal";
            this.group_beznal.Size = new System.Drawing.Size(600, 157);
            this.group_beznal.TabIndex = 0;
            this.group_beznal.TabStop = false;
            this.group_beznal.Text = "БЕЗНАЛ";
            this.group_beznal.Visible = false;
            this.group_beznal.VisibleChanged += new System.EventHandler(this.group_beznal_VisibleChanged);
            // 
            // org_subdetail_prim
            // 
            this.org_subdetail_prim.AutoEllipsis = true;
            this.org_subdetail_prim.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.org_subdetail_prim.Location = new System.Drawing.Point(274, 106);
            this.org_subdetail_prim.MaximumSize = new System.Drawing.Size(150, 50);
            this.org_subdetail_prim.MinimumSize = new System.Drawing.Size(150, 20);
            this.org_subdetail_prim.Name = "org_subdetail_prim";
            this.org_subdetail_prim.Size = new System.Drawing.Size(150, 20);
            this.org_subdetail_prim.TabIndex = 64;
            this.org_subdetail_prim.Text = "       ";
            // 
            // org_detail_prim
            // 
            this.org_detail_prim.AutoEllipsis = true;
            this.org_detail_prim.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.org_detail_prim.Location = new System.Drawing.Point(275, 75);
            this.org_detail_prim.MaximumSize = new System.Drawing.Size(150, 50);
            this.org_detail_prim.MinimumSize = new System.Drawing.Size(150, 20);
            this.org_detail_prim.Name = "org_detail_prim";
            this.org_detail_prim.Size = new System.Drawing.Size(150, 20);
            this.org_detail_prim.TabIndex = 63;
            this.org_detail_prim.Text = "       ";
            // 
            // ORGSUBDETAIL_ID
            // 
            this.ORGSUBDETAIL_ID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.ORGSUBDETAIL_ID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.ORGSUBDETAIL_ID.DropDownHeight = 350;
            this.ORGSUBDETAIL_ID.DropDownWidth = 450;
            this.ORGSUBDETAIL_ID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ORGSUBDETAIL_ID.FormattingEnabled = true;
            this.ORGSUBDETAIL_ID.IntegralHeight = false;
            this.ORGSUBDETAIL_ID.Location = new System.Drawing.Point(6, 104);
            this.ORGSUBDETAIL_ID.MaxDropDownItems = 15;
            this.ORGSUBDETAIL_ID.Name = "ORGSUBDETAIL_ID";
            this.ORGSUBDETAIL_ID.Size = new System.Drawing.Size(263, 33);
            this.ORGSUBDETAIL_ID.Sorted = true;
            this.ORGSUBDETAIL_ID.TabIndex = 61;
            this.ORGSUBDETAIL_ID.TabStop = false;
            this.ORGSUBDETAIL_ID.SelectedIndexChanged += new System.EventHandler(this.ORGSUBDETAIL_ID_SelectedIndexChanged);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label26.Location = new System.Drawing.Point(14, 92);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(78, 17);
            this.label26.TabIndex = 62;
            this.label26.Text = "Сотрудник";
            // 
            // month_limit
            // 
            this.month_limit.AutoSize = true;
            this.month_limit.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.month_limit.Location = new System.Drawing.Point(278, 64);
            this.month_limit.Name = "month_limit";
            this.month_limit.Size = new System.Drawing.Size(25, 15);
            this.month_limit.TabIndex = 60;
            this.month_limit.Text = "      ";
            this.month_limit.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label25.Location = new System.Drawing.Point(452, 75);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(64, 13);
            this.label25.TabIndex = 59;
            this.label25.Text = "Оплачено";
            // 
            // bnal_opl_tm
            // 
            this.bnal_opl_tm.Checked = false;
            this.bnal_opl_tm.CustomFormat = "dd.MM.yyyy HH:mm";
            this.bnal_opl_tm.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bnal_opl_tm.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.bnal_opl_tm.Location = new System.Drawing.Point(502, 110);
            this.bnal_opl_tm.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.bnal_opl_tm.Name = "bnal_opl_tm";
            this.bnal_opl_tm.ShowCheckBox = true;
            this.bnal_opl_tm.Size = new System.Drawing.Size(110, 19);
            this.bnal_opl_tm.TabIndex = 58;
            this.bnal_opl_tm.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.bnal_opl_tm.ValueChanged += new System.EventHandler(this.bnal_opl_tm_ValueChanged);
            this.bnal_opl_tm.MouseDown += new System.Windows.Forms.MouseEventHandler(this.bnal_opl_tm_MouseDown);
            // 
            // org_prim
            // 
            this.org_prim.AutoEllipsis = true;
            this.org_prim.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.org_prim.Location = new System.Drawing.Point(274, 26);
            this.org_prim.MaximumSize = new System.Drawing.Size(150, 50);
            this.org_prim.MinimumSize = new System.Drawing.Size(150, 20);
            this.org_prim.Name = "org_prim";
            this.org_prim.Size = new System.Drawing.Size(150, 20);
            this.org_prim.TabIndex = 56;
            this.org_prim.Text = "Примечание по организации ";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // DistanceMoney
            // 
            this.DistanceMoney.AutoSize = true;
            this.DistanceMoney.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DistanceMoney.Location = new System.Drawing.Point(580, 234);
            this.DistanceMoney.Name = "DistanceMoney";
            this.DistanceMoney.Size = new System.Drawing.Size(0, 20);
            this.DistanceMoney.TabIndex = 65;
            // 
            // predvShowDelta
            // 
            this.predvShowDelta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.predvShowDelta.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.predvShowDelta.FormattingEnabled = true;
            this.predvShowDelta.Items.AddRange(new object[] {
            "20",
            "30",
            "40"});
            this.predvShowDelta.Location = new System.Drawing.Point(523, 4);
            this.predvShowDelta.Name = "predvShowDelta";
            this.predvShowDelta.Size = new System.Drawing.Size(41, 33);
            this.predvShowDelta.TabIndex = 68;
            this.predvShowDelta.Visible = false;
            // 
            // group_calculator
            // 
            this.group_calculator.AutoSize = true;
            this.group_calculator.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.group_calculator.BackColor = System.Drawing.Color.LightBlue;
            this.group_calculator.Controls.Add(this.groupBox4);
            this.group_calculator.Controls.Add(this.label24);
            this.group_calculator.Controls.Add(this.wait_time);
            this.group_calculator.Controls.Add(this.label21);
            this.group_calculator.Controls.Add(this.distanceMgor);
            this.group_calculator.Controls.Add(this.label20);
            this.group_calculator.Controls.Add(this.label19);
            this.group_calculator.Controls.Add(this.DistanceCity);
            this.group_calculator.Controls.Add(this.groupBox2);
            this.group_calculator.Controls.Add(this.label27);
            this.group_calculator.Controls.Add(this.AddrTo);
            this.group_calculator.Dock = System.Windows.Forms.DockStyle.Top;
            this.group_calculator.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.group_calculator.Location = new System.Drawing.Point(0, 596);
            this.group_calculator.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.group_calculator.Name = "group_calculator";
            this.group_calculator.Size = new System.Drawing.Size(600, 276);
            this.group_calculator.TabIndex = 71;
            this.group_calculator.TabStop = false;
            this.group_calculator.Text = "Расчет стоимости";
            this.group_calculator.Visible = false;
            this.group_calculator.VisibleChanged += new System.EventHandler(this.group_calculator_VisibleChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.Inerpoint4_name);
            this.groupBox4.Controls.Add(this.Inerpoint3_name);
            this.groupBox4.Controls.Add(this.Inerpoint4_address);
            this.groupBox4.Controls.Add(this.Inerpoint3_address);
            this.groupBox4.Controls.Add(this.Inerpoint3_house);
            this.groupBox4.Controls.Add(this.Inerpoint4_house);
            this.groupBox4.Controls.Add(this.Inerpoint2_name);
            this.groupBox4.Controls.Add(this.Inerpoint1_name);
            this.groupBox4.Controls.Add(this.Inerpoint2_address);
            this.groupBox4.Controls.Add(this.Inerpoint1_address);
            this.groupBox4.Controls.Add(this.Inerpoint1_house);
            this.groupBox4.Controls.Add(this.Inerpoint2_house);
            this.groupBox4.Location = new System.Drawing.Point(151, 94);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(424, 163);
            this.groupBox4.TabIndex = 94;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Промежуточные точки поездки";
            // 
            // Inerpoint4_name
            // 
            this.Inerpoint4_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Inerpoint4_name.FormattingEnabled = true;
            this.Inerpoint4_name.Location = new System.Drawing.Point(284, 123);
            this.Inerpoint4_name.Name = "Inerpoint4_name";
            this.Inerpoint4_name.Size = new System.Drawing.Size(127, 24);
            this.Inerpoint4_name.TabIndex = 100;
            this.Inerpoint4_name.TabStop = false;
            this.Inerpoint4_name.Visible = false;
            // 
            // Inerpoint3_name
            // 
            this.Inerpoint3_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Inerpoint3_name.FormattingEnabled = true;
            this.Inerpoint3_name.Location = new System.Drawing.Point(284, 91);
            this.Inerpoint3_name.Name = "Inerpoint3_name";
            this.Inerpoint3_name.Size = new System.Drawing.Size(127, 24);
            this.Inerpoint3_name.TabIndex = 99;
            this.Inerpoint3_name.TabStop = false;
            this.Inerpoint3_name.Visible = false;
            // 
            // Inerpoint4_address
            // 
            this.Inerpoint4_address.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Inerpoint4_address.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Inerpoint4_address.FormattingEnabled = true;
            this.Inerpoint4_address.Location = new System.Drawing.Point(15, 123);
            this.Inerpoint4_address.Name = "Inerpoint4_address";
            this.Inerpoint4_address.Size = new System.Drawing.Size(192, 24);
            this.Inerpoint4_address.TabIndex = 97;
            this.Inerpoint4_address.Leave += new System.EventHandler(this.Inerpoint4_address_Leave);
            this.Inerpoint4_address.Enter += new System.EventHandler(this.Inerpoint4_address_Enter);
            this.Inerpoint4_address.TextChanged += new System.EventHandler(this.Inerpoint4_address_TextChanged);
            // 
            // Inerpoint3_address
            // 
            this.Inerpoint3_address.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Inerpoint3_address.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Inerpoint3_address.FormattingEnabled = true;
            this.Inerpoint3_address.Location = new System.Drawing.Point(15, 89);
            this.Inerpoint3_address.Name = "Inerpoint3_address";
            this.Inerpoint3_address.Size = new System.Drawing.Size(192, 24);
            this.Inerpoint3_address.TabIndex = 95;
            this.Inerpoint3_address.SelectedIndexChanged += new System.EventHandler(this.Inerpoint3_address_SelectedIndexChanged);
            this.Inerpoint3_address.Leave += new System.EventHandler(this.Inerpoint3_address_Leave);
            this.Inerpoint3_address.Enter += new System.EventHandler(this.Inerpoint3_address_Enter);
            this.Inerpoint3_address.TextChanged += new System.EventHandler(this.Inerpoint3_address_TextChanged);
            // 
            // Inerpoint3_house
            // 
            this.Inerpoint3_house.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.Inerpoint3_house.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.Inerpoint3_house.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Inerpoint3_house.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Inerpoint3_house.Location = new System.Drawing.Point(213, 91);
            this.Inerpoint3_house.Name = "Inerpoint3_house";
            this.Inerpoint3_house.Size = new System.Drawing.Size(65, 23);
            this.Inerpoint3_house.TabIndex = 96;
            this.Inerpoint3_house.TextChanged += new System.EventHandler(this.Inerpoint3_house_TextChanged);
            this.Inerpoint3_house.Leave += new System.EventHandler(this.Inerpoint3_house_Leave);
            this.Inerpoint3_house.Enter += new System.EventHandler(this.Inerpoint3_house_Enter);
            // 
            // Inerpoint4_house
            // 
            this.Inerpoint4_house.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.Inerpoint4_house.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.Inerpoint4_house.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Inerpoint4_house.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Inerpoint4_house.Location = new System.Drawing.Point(213, 125);
            this.Inerpoint4_house.Name = "Inerpoint4_house";
            this.Inerpoint4_house.Size = new System.Drawing.Size(65, 23);
            this.Inerpoint4_house.TabIndex = 98;
            this.Inerpoint4_house.TextChanged += new System.EventHandler(this.Inerpoint4_house_TextChanged);
            this.Inerpoint4_house.Leave += new System.EventHandler(this.Inerpoint4_house_Leave);
            this.Inerpoint4_house.Enter += new System.EventHandler(this.Inerpoint4_house_Enter);
            // 
            // Inerpoint2_name
            // 
            this.Inerpoint2_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Inerpoint2_name.FormattingEnabled = true;
            this.Inerpoint2_name.Location = new System.Drawing.Point(284, 57);
            this.Inerpoint2_name.Name = "Inerpoint2_name";
            this.Inerpoint2_name.Size = new System.Drawing.Size(127, 24);
            this.Inerpoint2_name.TabIndex = 94;
            this.Inerpoint2_name.TabStop = false;
            this.Inerpoint2_name.SelectedIndexChanged += new System.EventHandler(this.Inerpoint2_name_SelectedIndexChanged);
            this.Inerpoint2_name.SelectedValueChanged += new System.EventHandler(this.Inerpoint2_name_SelectedValueChanged);
            this.Inerpoint2_name.Click += new System.EventHandler(this.AddrFromName_Click);
            // 
            // Inerpoint1_name
            // 
            this.Inerpoint1_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Inerpoint1_name.FormattingEnabled = true;
            this.Inerpoint1_name.Location = new System.Drawing.Point(284, 25);
            this.Inerpoint1_name.Name = "Inerpoint1_name";
            this.Inerpoint1_name.Size = new System.Drawing.Size(127, 24);
            this.Inerpoint1_name.TabIndex = 93;
            this.Inerpoint1_name.TabStop = false;
            this.Inerpoint1_name.SelectedIndexChanged += new System.EventHandler(this.Inerpoint1_name_SelectedIndexChanged);
            this.Inerpoint1_name.SelectedValueChanged += new System.EventHandler(this.Inerpoint1_name_SelectedValueChanged);
            this.Inerpoint1_name.Click += new System.EventHandler(this.AddrFromName_Click);
            // 
            // Inerpoint2_address
            // 
            this.Inerpoint2_address.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Inerpoint2_address.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Inerpoint2_address.FormattingEnabled = true;
            this.Inerpoint2_address.Location = new System.Drawing.Point(15, 57);
            this.Inerpoint2_address.Name = "Inerpoint2_address";
            this.Inerpoint2_address.Size = new System.Drawing.Size(192, 24);
            this.Inerpoint2_address.TabIndex = 91;
            this.Inerpoint2_address.Leave += new System.EventHandler(this.Inerpoint2_address_Leave);
            this.Inerpoint2_address.Enter += new System.EventHandler(this.Inerpoint2_address_Enter);
            this.Inerpoint2_address.TextChanged += new System.EventHandler(this.Inerpoint2_address_TextChanged);
            // 
            // Inerpoint1_address
            // 
            this.Inerpoint1_address.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Inerpoint1_address.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Inerpoint1_address.FormattingEnabled = true;
            this.Inerpoint1_address.Location = new System.Drawing.Point(15, 23);
            this.Inerpoint1_address.Name = "Inerpoint1_address";
            this.Inerpoint1_address.Size = new System.Drawing.Size(192, 24);
            this.Inerpoint1_address.TabIndex = 89;
            this.Inerpoint1_address.Leave += new System.EventHandler(this.Inerpoint1_address_Leave);
            this.Inerpoint1_address.Enter += new System.EventHandler(this.Inerpoint1_address_Enter);
            this.Inerpoint1_address.TextChanged += new System.EventHandler(this.Inerpoint1_address_TextChanged);
            // 
            // Inerpoint1_house
            // 
            this.Inerpoint1_house.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.Inerpoint1_house.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.Inerpoint1_house.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Inerpoint1_house.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Inerpoint1_house.Location = new System.Drawing.Point(213, 25);
            this.Inerpoint1_house.Name = "Inerpoint1_house";
            this.Inerpoint1_house.Size = new System.Drawing.Size(65, 23);
            this.Inerpoint1_house.TabIndex = 90;
            this.Inerpoint1_house.TextChanged += new System.EventHandler(this.Inerpoint1_house_TextChanged);
            this.Inerpoint1_house.Leave += new System.EventHandler(this.Inerpoint1_house_Leave);
            this.Inerpoint1_house.Enter += new System.EventHandler(this.Inerpoint1_house_Enter);
            // 
            // Inerpoint2_house
            // 
            this.Inerpoint2_house.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.Inerpoint2_house.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.Inerpoint2_house.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Inerpoint2_house.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Inerpoint2_house.Location = new System.Drawing.Point(213, 59);
            this.Inerpoint2_house.Name = "Inerpoint2_house";
            this.Inerpoint2_house.Size = new System.Drawing.Size(65, 23);
            this.Inerpoint2_house.TabIndex = 92;
            this.Inerpoint2_house.TextChanged += new System.EventHandler(this.Inerpoint2_house_TextChanged);
            this.Inerpoint2_house.Leave += new System.EventHandler(this.Inerpoint2_house_Leave);
            this.Inerpoint2_house.Enter += new System.EventHandler(this.Inerpoint2_house_Enter);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(14, 166);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(80, 17);
            this.label24.TabIndex = 97;
            this.label24.Text = "Ожидание:";
            // 
            // wait_time
            // 
            this.wait_time.Location = new System.Drawing.Point(91, 159);
            this.wait_time.Name = "wait_time";
            this.wait_time.Size = new System.Drawing.Size(49, 23);
            this.wait_time.TabIndex = 96;
            this.wait_time.Text = "0";
            this.wait_time.TextChanged += new System.EventHandler(this.wait_time_TextChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(14, 139);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(77, 17);
            this.label21.TabIndex = 95;
            this.label21.Text = "Межгород:";
            // 
            // distanceMgor
            // 
            this.distanceMgor.Location = new System.Drawing.Point(91, 132);
            this.distanceMgor.Name = "distanceMgor";
            this.distanceMgor.Size = new System.Drawing.Size(49, 23);
            this.distanceMgor.TabIndex = 94;
            this.distanceMgor.Text = "0";
            this.distanceMgor.TextChanged += new System.EventHandler(this.distanceMgor_TextChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(216, 16);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(0, 17);
            this.label20.TabIndex = 93;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(14, 109);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(78, 17);
            this.label19.TabIndex = 86;
            this.label19.Text = "По городу:";
            // 
            // DistanceCity
            // 
            this.DistanceCity.Location = new System.Drawing.Point(91, 106);
            this.DistanceCity.Name = "DistanceCity";
            this.DistanceCity.Size = new System.Drawing.Size(49, 23);
            this.DistanceCity.TabIndex = 85;
            this.DistanceCity.Text = "0";
            this.DistanceCity.TextChanged += new System.EventHandler(this.DistanceCity_TextChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.black_list_car_id);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.black_list_money);
            this.groupBox2.Controls.Add(this.calc_check_black_list);
            this.groupBox2.Controls.Add(this.calc_check_4);
            this.groupBox2.Controls.Add(this.calc_check_3);
            this.groupBox2.Controls.Add(this.calc_check_2);
            this.groupBox2.Controls.Add(this.calc_check_1);
            this.groupBox2.Location = new System.Drawing.Point(10, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(565, 81);
            this.groupBox2.TabIndex = 78;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Дополнительные  услуги";
            // 
            // black_list_car_id
            // 
            this.black_list_car_id.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.black_list_car_id.Location = new System.Drawing.Point(474, 51);
            this.black_list_car_id.Name = "black_list_car_id";
            this.black_list_car_id.Size = new System.Drawing.Size(72, 30);
            this.black_list_car_id.TabIndex = 51;
            this.black_list_car_id.TabStop = false;
            this.black_list_car_id.Visible = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label22.Location = new System.Drawing.Point(437, 51);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(35, 17);
            this.label22.TabIndex = 50;
            this.label22.Text = "Для";
            this.label22.Visible = false;
            // 
            // black_list_money
            // 
            this.black_list_money.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.black_list_money.Location = new System.Drawing.Point(473, 19);
            this.black_list_money.Name = "black_list_money";
            this.black_list_money.Size = new System.Drawing.Size(72, 30);
            this.black_list_money.TabIndex = 49;
            this.black_list_money.TabStop = false;
            this.black_list_money.Visible = false;
            // 
            // calc_check_black_list
            // 
            this.calc_check_black_list.AutoSize = true;
            this.calc_check_black_list.Location = new System.Drawing.Point(365, 23);
            this.calc_check_black_list.Name = "calc_check_black_list";
            this.calc_check_black_list.Size = new System.Drawing.Size(110, 21);
            this.calc_check_black_list.TabIndex = 4;
            this.calc_check_black_list.Text = "Черн список";
            this.calc_check_black_list.UseVisualStyleBackColor = true;
            this.calc_check_black_list.CheckedChanged += new System.EventHandler(this.calc_check_black_list_CheckedChanged);
            // 
            // calc_check_4
            // 
            this.calc_check_4.AutoSize = true;
            this.calc_check_4.Location = new System.Drawing.Point(188, 46);
            this.calc_check_4.Name = "calc_check_4";
            this.calc_check_4.Size = new System.Drawing.Size(197, 21);
            this.calc_check_4.TabIndex = 3;
            this.calc_check_4.Text = "Плохая дорога/грунт(20р)";
            this.calc_check_4.UseVisualStyleBackColor = true;
            this.calc_check_4.CheckedChanged += new System.EventHandler(this.calc_check_4_CheckedChanged);
            // 
            // calc_check_3
            // 
            this.calc_check_3.AutoSize = true;
            this.calc_check_3.Location = new System.Drawing.Point(188, 23);
            this.calc_check_3.Name = "calc_check_3";
            this.calc_check_3.Size = new System.Drawing.Size(132, 21);
            this.calc_check_3.TabIndex = 2;
            this.calc_check_3.Text = "Животное (20р)";
            this.calc_check_3.UseVisualStyleBackColor = true;
            this.calc_check_3.CheckedChanged += new System.EventHandler(this.calc_check_3_CheckedChanged);
            // 
            // calc_check_2
            // 
            this.calc_check_2.AutoSize = true;
            this.calc_check_2.Location = new System.Drawing.Point(11, 46);
            this.calc_check_2.Name = "calc_check_2";
            this.calc_check_2.Size = new System.Drawing.Size(104, 21);
            this.calc_check_2.TabIndex = 1;
            this.calc_check_2.Text = "Багаж (30р)";
            this.calc_check_2.UseVisualStyleBackColor = true;
            this.calc_check_2.CheckedChanged += new System.EventHandler(this.calc_check_2_CheckedChanged);
            // 
            // calc_check_1
            // 
            this.calc_check_1.AutoSize = true;
            this.calc_check_1.Location = new System.Drawing.Point(11, 23);
            this.calc_check_1.Name = "calc_check_1";
            this.calc_check_1.Size = new System.Drawing.Size(193, 21);
            this.calc_check_1.TabIndex = 0;
            this.calc_check_1.Text = "Отдаленный район (20р)";
            this.calc_check_1.UseVisualStyleBackColor = true;
            this.calc_check_1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label27.Location = new System.Drawing.Point(636, 19);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(55, 25);
            this.label27.TabIndex = 75;
            this.label27.Text = "Куда";
            this.label27.Visible = false;
            // 
            // AddrTo
            // 
            this.AddrTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddrTo.FormattingEnabled = true;
            this.AddrTo.Location = new System.Drawing.Point(640, 39);
            this.AddrTo.Name = "AddrTo";
            this.AddrTo.Size = new System.Drawing.Size(161, 33);
            this.AddrTo.TabIndex = 0;
            this.AddrTo.TabStop = false;
            this.AddrTo.Visible = false;
            this.AddrTo.TextChanged += new System.EventHandler(this.AddrTo_TextChanged);
            this.AddrTo.Click += new System.EventHandler(this.AddrTo_Click);
            // 
            // calculator_text
            // 
            this.calculator_text.AutoEllipsis = true;
            this.calculator_text.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.calculator_text.Location = new System.Drawing.Point(6, 219);
            this.calculator_text.Name = "calculator_text";
            this.calculator_text.Size = new System.Drawing.Size(591, 24);
            this.calculator_text.TabIndex = 84;
            this.calculator_text.Text = "                                               ";
            // 
            // button2
            // 
            this.button2.ImageKey = "Earth.ico";
            this.button2.ImageList = this.imageList1;
            this.button2.Location = new System.Drawing.Point(275, 152);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(26, 26);
            this.button2.TabIndex = 70;
            this.button2.TabStop = false;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // HouseTo2
            // 
            this.HouseTo2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.HouseTo2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.HouseTo2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HouseTo2.Location = new System.Drawing.Point(205, 152);
            this.HouseTo2.MaxLength = 10;
            this.HouseTo2.Name = "HouseTo2";
            this.HouseTo2.Size = new System.Drawing.Size(65, 26);
            this.HouseTo2.TabIndex = 5;
            this.HouseTo2.TextChanged += new System.EventHandler(this.HouseTo2_TextChanged);
            this.HouseTo2.Leave += new System.EventHandler(this.HouseTo2_Leave);
            this.HouseTo2.Enter += new System.EventHandler(this.HouseTo2_Enter);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(222, 127);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 17);
            this.label9.TabIndex = 65;
            this.label9.Text = "Дом";
            // 
            // group_header
            // 
            this.group_header.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.group_header.BackColor = System.Drawing.SystemColors.Control;
            this.group_header.Controls.Add(this.order_10percent);
            this.group_header.Controls.Add(this.btnMakeAbonent);
            this.group_header.Controls.Add(this.button1);
            this.group_header.Controls.Add(this.phone_info);
            this.group_header.Controls.Add(this.chk_close_not_saved);
            this.group_header.Controls.Add(this.btn_fix_to);
            this.group_header.Controls.Add(this.btn_fix_from);
            this.group_header.Controls.Add(this.calculator_text);
            this.group_header.Controls.Add(this.DistanceMoney);
            this.group_header.Controls.Add(this.расчетМаршрута);
            this.group_header.Controls.Add(this.button2);
            this.group_header.Controls.Add(this.HouseTo2);
            this.group_header.Controls.Add(this.label9);
            this.group_header.Controls.Add(this.ClientID);
            this.group_header.Controls.Add(this.button3);
            this.group_header.Controls.Add(this.ZoneID);
            this.group_header.Controls.Add(this.label1);
            this.group_header.Controls.Add(this.predvShowDelta);
            this.group_header.Controls.Add(this.Phone);
            this.group_header.Controls.Add(this.label2);
            this.group_header.Controls.Add(this.ConnectPhone);
            this.group_header.Controls.Add(this.AddrFrom);
            this.group_header.Controls.Add(this.HouseFrom);
            this.group_header.Controls.Add(this.FlatFrom);
            this.group_header.Controls.Add(this.AddrFromName);
            this.group_header.Controls.Add(this.btnPhone2Dial);
            this.group_header.Controls.Add(this.label10);
            this.group_header.Controls.Add(this.btnPhone1Dial);
            this.group_header.Controls.Add(this.label8);
            this.group_header.Controls.Add(this.AddrToName);
            this.group_header.Controls.Add(this.label15);
            this.group_header.Controls.Add(this.tmPredvar);
            this.group_header.Controls.Add(this.RaionToID);
            this.group_header.Controls.Add(this.ServiceID);
            this.group_header.Controls.Add(this.label7);
            this.group_header.Controls.Add(this.dtPredvar);
            this.group_header.Controls.Add(this.btnPhoneInfo);
            this.group_header.Controls.Add(this.chkPredvar);
            this.group_header.Controls.Add(this.lbPhoneInfo);
            this.group_header.Controls.Add(this.AddrToForDriver);
            this.group_header.Controls.Add(this.label5);
            this.group_header.Controls.Add(this.label4);
            this.group_header.Controls.Add(this.label3);
            this.group_header.Controls.Add(this.label14);
            this.group_header.Controls.Add(this.label11);
            this.group_header.Controls.Add(this.FromPhone);
            this.group_header.Controls.Add(this.label6);
            this.group_header.Dock = System.Windows.Forms.DockStyle.Top;
            this.group_header.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.group_header.Location = new System.Drawing.Point(0, 0);
            this.group_header.Name = "group_header";
            this.group_header.Size = new System.Drawing.Size(600, 253);
            this.group_header.TabIndex = 72;
            this.group_header.TabStop = false;
            this.group_header.Text = "Основная информация по заказу";
            this.group_header.Enter += new System.EventHandler(this.group_header_Enter);
            // 
            // order_10percent
            // 
            this.order_10percent.AutoSize = true;
            this.order_10percent.Location = new System.Drawing.Point(187, 191);
            this.order_10percent.Name = "order_10percent";
            this.order_10percent.Size = new System.Drawing.Size(166, 21);
            this.order_10percent.TabIndex = 91;
            this.order_10percent.Text = "10% (МЕЖГОРОД)";
            this.order_10percent.UseVisualStyleBackColor = true;
            this.order_10percent.CheckedChanged += new System.EventHandler(this.order_10percent_CheckedChanged);
            // 
            // btnMakeAbonent
            // 
            this.btnMakeAbonent.Location = new System.Drawing.Point(693, 117);
            this.btnMakeAbonent.Name = "btnMakeAbonent";
            this.btnMakeAbonent.Size = new System.Drawing.Size(128, 23);
            this.btnMakeAbonent.TabIndex = 90;
            this.btnMakeAbonent.Text = "Создать абонента";
            this.btnMakeAbonent.UseVisualStyleBackColor = true;
            this.btnMakeAbonent.Visible = false;
            this.btnMakeAbonent.Click += new System.EventHandler(this.btnMakeAbonent_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(671, 56);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 89;
            this.button1.Text = "doLoadPhoneInfo";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // phone_info
            // 
            this.phone_info.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.phone_info.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.phone_info.Location = new System.Drawing.Point(7, 11);
            this.phone_info.Name = "phone_info";
            this.phone_info.Size = new System.Drawing.Size(266, 14);
            this.phone_info.TabIndex = 43;
            this.phone_info.Text = "йцукцук,";
            // 
            // chk_close_not_saved
            // 
            this.chk_close_not_saved.AutoSize = true;
            this.chk_close_not_saved.BackColor = System.Drawing.Color.Yellow;
            this.chk_close_not_saved.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chk_close_not_saved.Location = new System.Drawing.Point(575, 12);
            this.chk_close_not_saved.Name = "chk_close_not_saved";
            this.chk_close_not_saved.Size = new System.Drawing.Size(37, 21);
            this.chk_close_not_saved.TabIndex = 76;
            this.chk_close_not_saved.Text = "Х";
            this.chk_close_not_saved.UseVisualStyleBackColor = false;
            // 
            // btn_fix_to
            // 
            this.btn_fix_to.Location = new System.Drawing.Point(102, 131);
            this.btn_fix_to.Name = "btn_fix_to";
            this.btn_fix_to.Size = new System.Drawing.Size(17, 13);
            this.btn_fix_to.TabIndex = 88;
            this.btn_fix_to.Text = "btn_fix_to";
            this.btn_fix_to.UseVisualStyleBackColor = true;
            this.btn_fix_to.Click += new System.EventHandler(this.btn_fix_to_Click);
            // 
            // btn_fix_from
            // 
            this.btn_fix_from.Location = new System.Drawing.Point(72, 80);
            this.btn_fix_from.Name = "btn_fix_from";
            this.btn_fix_from.Size = new System.Drawing.Size(17, 13);
            this.btn_fix_from.TabIndex = 87;
            this.btn_fix_from.Text = "fix_from";
            this.btn_fix_from.UseVisualStyleBackColor = true;
            this.btn_fix_from.Click += new System.EventHandler(this.btn_fix_from_Click);
            // 
            // button3
            // 
            this.button3.ImageKey = "Earth.ico";
            this.button3.ImageList = this.imageList1;
            this.button3.Location = new System.Drawing.Point(275, 99);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(26, 26);
            this.button3.TabIndex = 71;
            this.button3.TabStop = false;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // ZoneID
            // 
            this.ZoneID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ZoneID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ZoneID.Location = new System.Drawing.Point(307, 99);
            this.ZoneID.Name = "ZoneID";
            this.ZoneID.Size = new System.Drawing.Size(130, 28);
            this.ZoneID.TabIndex = 3;
            this.ZoneID.SelectedIndexChanged += new System.EventHandler(this.ZoneID_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(1, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Телефон";
            // 
            // Phone
            // 
            this.Phone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Phone.Location = new System.Drawing.Point(5, 47);
            this.Phone.Name = "Phone";
            this.Phone.Size = new System.Drawing.Size(121, 30);
            this.Phone.TabIndex = 0;
            this.Phone.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(185, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(137, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Телефон для связи";
            // 
            // ConnectPhone
            // 
            this.ConnectPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ConnectPhone.Location = new System.Drawing.Point(187, 47);
            this.ConnectPhone.Name = "ConnectPhone";
            this.ConnectPhone.Size = new System.Drawing.Size(121, 30);
            this.ConnectPhone.TabIndex = 1;
            this.ConnectPhone.TabStop = false;
            this.ConnectPhone.TextChanged += new System.EventHandler(this.ConnectPhone_TextChanged);
            // 
            // AddrFrom
            // 
            this.AddrFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddrFrom.Location = new System.Drawing.Point(5, 99);
            this.AddrFrom.Name = "AddrFrom";
            this.AddrFrom.Size = new System.Drawing.Size(149, 26);
            this.AddrFrom.TabIndex = 0;
            this.AddrFrom.TextChanged += new System.EventHandler(this.AddrFrom_TextChanged);
            this.AddrFrom.Click += new System.EventHandler(this.AddrFrom_Click);
            this.AddrFrom.Leave += new System.EventHandler(this.AddrFrom_Leave);
            this.AddrFrom.Enter += new System.EventHandler(this.AddrFrom_Enter);
            // 
            // HouseFrom
            // 
            this.HouseFrom.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.HouseFrom.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.HouseFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HouseFrom.Location = new System.Drawing.Point(160, 99);
            this.HouseFrom.MaxLength = 10;
            this.HouseFrom.Name = "HouseFrom";
            this.HouseFrom.Size = new System.Drawing.Size(62, 26);
            this.HouseFrom.TabIndex = 1;
            this.HouseFrom.TextChanged += new System.EventHandler(this.HouseFrom_TextChanged);
            this.HouseFrom.Click += new System.EventHandler(this.HouseFrom_Click);
            this.HouseFrom.Leave += new System.EventHandler(this.HouseFrom_Leave);
            this.HouseFrom.Enter += new System.EventHandler(this.HouseFrom_Enter);
            // 
            // FlatFrom
            // 
            this.FlatFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FlatFrom.Location = new System.Drawing.Point(225, 99);
            this.FlatFrom.MaxLength = 10;
            this.FlatFrom.Name = "FlatFrom";
            this.FlatFrom.Size = new System.Drawing.Size(45, 26);
            this.FlatFrom.TabIndex = 2;
            // 
            // AddrFromName
            // 
            this.AddrFromName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddrFromName.Location = new System.Drawing.Point(443, 99);
            this.AddrFromName.Name = "AddrFromName";
            this.AddrFromName.Size = new System.Drawing.Size(126, 28);
            this.AddrFromName.TabIndex = 4;
            this.AddrFromName.TabStop = false;
            this.AddrFromName.SelectedIndexChanged += new System.EventHandler(this.AddrFromName_SelectedIndexChanged);
            this.AddrFromName.SelectedValueChanged += new System.EventHandler(this.AddrFromName_SelectedValueChanged);
            this.AddrFromName.Click += new System.EventHandler(this.AddrFromName_Click);
            // 
            // btnPhone2Dial
            // 
            this.btnPhone2Dial.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnPhone2Dial.ImageKey = "29.ico";
            this.btnPhone2Dial.ImageList = this.imageList1;
            this.btnPhone2Dial.Location = new System.Drawing.Point(314, 48);
            this.btnPhone2Dial.Name = "btnPhone2Dial";
            this.btnPhone2Dial.Size = new System.Drawing.Size(26, 26);
            this.btnPhone2Dial.TabIndex = 53;
            this.btnPhone2Dial.TabStop = false;
            this.btnPhone2Dial.UseVisualStyleBackColor = true;
            this.btnPhone2Dial.Click += new System.EventHandler(this.btnPhone2Dial_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.ForeColor = System.Drawing.Color.Blue;
            this.label10.Location = new System.Drawing.Point(1, 127);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(95, 17);
            this.label10.TabIndex = 14;
            this.label10.Text = "Куда (Улица)";
            // 
            // btnPhone1Dial
            // 
            this.btnPhone1Dial.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnPhone1Dial.ImageKey = "29.ico";
            this.btnPhone1Dial.ImageList = this.imageList1;
            this.btnPhone1Dial.Location = new System.Drawing.Point(157, 48);
            this.btnPhone1Dial.Margin = new System.Windows.Forms.Padding(0);
            this.btnPhone1Dial.Name = "btnPhone1Dial";
            this.btnPhone1Dial.Size = new System.Drawing.Size(26, 26);
            this.btnPhone1Dial.TabIndex = 52;
            this.btnPhone1Dial.TabStop = false;
            this.btnPhone1Dial.UseVisualStyleBackColor = true;
            this.btnPhone1Dial.Click += new System.EventHandler(this.btnPhone1Dial_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(442, 127);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(106, 17);
            this.label8.TabIndex = 18;
            this.label8.Text = "Название куда";
            // 
            // AddrToName
            // 
            this.AddrToName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddrToName.FormattingEnabled = true;
            this.AddrToName.Location = new System.Drawing.Point(443, 150);
            this.AddrToName.Name = "AddrToName";
            this.AddrToName.Size = new System.Drawing.Size(127, 28);
            this.AddrToName.TabIndex = 8;
            this.AddrToName.TabStop = false;
            this.AddrToName.SelectedIndexChanged += new System.EventHandler(this.AddrToName_SelectedIndexChanged);
            this.AddrToName.SelectedValueChanged += new System.EventHandler(this.AddrToName_SelectedValueChanged);
            this.AddrToName.Click += new System.EventHandler(this.AddrFromName_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.ForeColor = System.Drawing.Color.Blue;
            this.label15.Location = new System.Drawing.Point(304, 127);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(83, 17);
            this.label15.TabIndex = 28;
            this.label15.Text = "Район куда";
            // 
            // RaionToID
            // 
            this.RaionToID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.RaionToID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RaionToID.FormattingEnabled = true;
            this.RaionToID.Location = new System.Drawing.Point(307, 150);
            this.RaionToID.Name = "RaionToID";
            this.RaionToID.Size = new System.Drawing.Size(130, 28);
            this.RaionToID.TabIndex = 5;
            this.RaionToID.TabStop = false;
            // 
            // btnPhoneInfo
            // 
            this.btnPhoneInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnPhoneInfo.ImageKey = "documentinfo.png";
            this.btnPhoneInfo.ImageList = this.imageList1;
            this.btnPhoneInfo.Location = new System.Drawing.Point(128, 48);
            this.btnPhoneInfo.Name = "btnPhoneInfo";
            this.btnPhoneInfo.Size = new System.Drawing.Size(26, 26);
            this.btnPhoneInfo.TabIndex = 6;
            this.btnPhoneInfo.TabStop = false;
            this.btnPhoneInfo.UseVisualStyleBackColor = true;
            this.btnPhoneInfo.Click += new System.EventHandler(this.btnPhoneInfo_Click);
            // 
            // lbPhoneInfo
            // 
            this.lbPhoneInfo.BackColor = System.Drawing.Color.LemonChiffon;
            this.lbPhoneInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbPhoneInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbPhoneInfo.FormattingEnabled = true;
            this.lbPhoneInfo.ItemHeight = 25;
            this.lbPhoneInfo.Location = new System.Drawing.Point(-1, 77);
            this.lbPhoneInfo.Name = "lbPhoneInfo";
            this.lbPhoneInfo.Size = new System.Drawing.Size(414, 2);
            this.lbPhoneInfo.TabIndex = 40;
            this.lbPhoneInfo.Visible = false;
            this.lbPhoneInfo.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lbPhoneInfo_MouseDoubleClick);
            this.lbPhoneInfo.SelectedIndexChanged += new System.EventHandler(this.lbPhoneInfo_SelectedIndexChanged);
            this.lbPhoneInfo.Leave += new System.EventHandler(this.lbPhoneInfo_Leave);
            // 
            // AddrToForDriver
            // 
            this.AddrToForDriver.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddrToForDriver.FormattingEnabled = true;
            this.AddrToForDriver.Location = new System.Drawing.Point(7, 150);
            this.AddrToForDriver.Name = "AddrToForDriver";
            this.AddrToForDriver.Size = new System.Drawing.Size(192, 28);
            this.AddrToForDriver.TabIndex = 4;
            this.AddrToForDriver.SelectedIndexChanged += new System.EventHandler(this.AddrToForDriver_SelectedIndexChanged);
            this.AddrToForDriver.Leave += new System.EventHandler(this.AddrToForDriver_Leave);
            this.AddrToForDriver.TextChanged += new System.EventHandler(this.AddrToForDriver_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(225, 83);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(24, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "Кв";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(169, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Дом";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(1, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Откуда";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.ForeColor = System.Drawing.Color.Blue;
            this.label14.Location = new System.Drawing.Point(304, 83);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(98, 17);
            this.label14.TabIndex = 26;
            this.label14.Text = "Район откуда";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(435, 83);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(121, 17);
            this.label6.TabIndex = 10;
            this.label6.Text = "Название откуда";
            // 
            // btnMakeReview
            // 
            this.btnMakeReview.Location = new System.Drawing.Point(468, 6);
            this.btnMakeReview.Name = "btnMakeReview";
            this.btnMakeReview.Size = new System.Drawing.Size(141, 23);
            this.btnMakeReview.TabIndex = 91;
            this.btnMakeReview.Text = "Создать отзыв";
            this.btnMakeReview.UseVisualStyleBackColor = true;
            this.btnMakeReview.Click += new System.EventHandler(this.btnMakeReview_Click);
            // 
            // chk_beznal
            // 
            this.chk_beznal.AutoSize = true;
            this.chk_beznal.Location = new System.Drawing.Point(205, 147);
            this.chk_beznal.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.chk_beznal.Name = "chk_beznal";
            this.chk_beznal.Size = new System.Drawing.Size(83, 21);
            this.chk_beznal.TabIndex = 75;
            this.chk_beznal.TabStop = false;
            this.chk_beznal.Text = "БЕЗНАЛ";
            this.chk_beznal.UseVisualStyleBackColor = true;
            this.chk_beznal.CheckedChanged += new System.EventHandler(this.chk_beznal_CheckedChanged);
            // 
            // chk_calculator
            // 
            this.chk_calculator.AutoSize = true;
            this.chk_calculator.Location = new System.Drawing.Point(6, 147);
            this.chk_calculator.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.chk_calculator.Name = "chk_calculator";
            this.chk_calculator.Size = new System.Drawing.Size(205, 21);
            this.chk_calculator.TabIndex = 74;
            this.chk_calculator.TabStop = false;
            this.chk_calculator.Text = "РАСЧЕТ СТОИМОСТИ (F9)";
            this.chk_calculator.UseVisualStyleBackColor = true;
            this.chk_calculator.CheckedChanged += new System.EventHandler(this.chk_calculator_CheckedChanged);
            // 
            // button4
            // 
            this.button4.ImageKey = "Earth.ico";
            this.button4.ImageList = this.imageList1;
            this.button4.Location = new System.Drawing.Point(506, 118);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(26, 26);
            this.button4.TabIndex = 72;
            this.button4.TabStop = false;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // group_order_history_info
            // 
            this.group_order_history_info.AutoSize = true;
            this.group_order_history_info.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.group_order_history_info.BackColor = System.Drawing.SystemColors.Control;
            this.group_order_history_info.Controls.Add(this.btnДанные);
            this.group_order_history_info.Controls.Add(this.label_taxometr_info);
            this.group_order_history_info.Controls.Add(this.dt_order_info);
            this.group_order_history_info.Controls.Add(this.chk_EndOrder);
            this.group_order_history_info.Controls.Add(this.comboEndState);
            this.group_order_history_info.Controls.Add(this.btnEndOrder);
            this.group_order_history_info.Dock = System.Windows.Forms.DockStyle.Top;
            this.group_order_history_info.Location = new System.Drawing.Point(0, 872);
            this.group_order_history_info.Name = "group_order_history_info";
            this.group_order_history_info.Size = new System.Drawing.Size(600, 137);
            this.group_order_history_info.TabIndex = 74;
            this.group_order_history_info.TabStop = false;
            this.group_order_history_info.Text = "Дополнительно";
            this.group_order_history_info.Visible = false;
            this.group_order_history_info.VisibleChanged += new System.EventHandler(this.group_order_history_info_VisibleChanged);
            // 
            // label_taxometr_info
            // 
            this.label_taxometr_info.Location = new System.Drawing.Point(241, 13);
            this.label_taxometr_info.Name = "label_taxometr_info";
            this.label_taxometr_info.Size = new System.Drawing.Size(231, 80);
            this.label_taxometr_info.TabIndex = 46;
            this.label_taxometr_info.Text = "Подача: 3км город 12 мгор, 12 мин гор, ";
            // 
            // group_footer
            // 
            this.group_footer.Controls.Add(this.btnSave);
            this.group_footer.Controls.Add(this.btnEndCall);
            this.group_footer.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.group_footer.Location = new System.Drawing.Point(0, 584);
            this.group_footer.Name = "group_footer";
            this.group_footer.Size = new System.Drawing.Size(620, 45);
            this.group_footer.TabIndex = 12;
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Controls.Add(this.group_order_history_info);
            this.panel2.Controls.Add(this.group_calculator);
            this.panel2.Controls.Add(this.group_beznal);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Controls.Add(this.group_header);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(620, 584);
            this.panel2.TabIndex = 75;
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSize = true;
            this.groupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox1.Controls.Add(this.chk_podacha);
            this.groupBox1.Controls.Add(this.btnMakeReview);
            this.groupBox1.Controls.Add(this.btnOkDialToPassenger);
            this.groupBox1.Controls.Add(this.order_price_Descr);
            this.groupBox1.Controls.Add(this.CarID);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.Descr);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.chk_beznal);
            this.groupBox1.Controls.Add(this.btnCarDial);
            this.groupBox1.Controls.Add(this.btnCarMessage);
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Controls.Add(this.chk_calculator);
            this.groupBox1.Controls.Add(this.CarIDView);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 253);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(600, 186);
            this.groupBox1.TabIndex = 85;
            this.groupBox1.TabStop = false;
            // 
            // chk_podacha
            // 
            this.chk_podacha.AutoSize = true;
            this.chk_podacha.Location = new System.Drawing.Point(322, 147);
            this.chk_podacha.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.chk_podacha.Name = "chk_podacha";
            this.chk_podacha.Size = new System.Drawing.Size(110, 21);
            this.chk_podacha.TabIndex = 92;
            this.chk_podacha.TabStop = false;
            this.chk_podacha.Text = "С ПОДАЧЕЙ";
            this.chk_podacha.UseVisualStyleBackColor = true;
            // 
            // btnOkDialToPassenger
            // 
            this.btnOkDialToPassenger.ImageKey = "phone_blue.png";
            this.btnOkDialToPassenger.ImageList = this.imageList1;
            this.btnOkDialToPassenger.Location = new System.Drawing.Point(554, 118);
            this.btnOkDialToPassenger.Name = "btnOkDialToPassenger";
            this.btnOkDialToPassenger.Size = new System.Drawing.Size(26, 26);
            this.btnOkDialToPassenger.TabIndex = 77;
            this.btnOkDialToPassenger.TabStop = false;
            this.btnOkDialToPassenger.UseVisualStyleBackColor = true;
            this.btnOkDialToPassenger.Click += new System.EventHandler(this.btnOkDialToPassenger_Click);
            // 
            // order_price_Descr
            // 
            this.order_price_Descr.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.order_price_Descr.Location = new System.Drawing.Point(4, 78);
            this.order_price_Descr.Name = "order_price_Descr";
            this.order_price_Descr.Size = new System.Drawing.Size(604, 26);
            this.order_price_Descr.TabIndex = 76;
            // 
            // btnДанные
            // 
            this.btnДанные.Location = new System.Drawing.Point(244, 93);
            this.btnДанные.Name = "btnДанные";
            this.btnДанные.Size = new System.Drawing.Size(151, 23);
            this.btnДанные.TabIndex = 47;
            this.btnДанные.Text = "данные";
            this.btnДанные.UseVisualStyleBackColor = true;
            this.btnДанные.Click += new System.EventHandler(this.btnДанные_Click);
            // 
            // order_big
            // 
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(620, 629);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.group_footer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "order_big";
            this.Text = "Новый заказ";
            this.TopMost = true;
            this.Deactivate += new System.EventHandler(this.order_big_Deactivate);
            this.Load += new System.EventHandler(this.order_Load);
            this.Activated += new System.EventHandler(this.order_big_Activated);
            this.VisibleChanged += new System.EventHandler(this.order_big_VisibleChanged);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.order_big_FormClosed);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.order_MouseDown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.order_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.order_big_KeyDown);
            this.group_beznal.ResumeLayout(false);
            this.group_beznal.PerformLayout();
            this.group_calculator.ResumeLayout(false);
            this.group_calculator.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.group_header.ResumeLayout(false);
            this.group_header.PerformLayout();
            this.group_order_history_info.ResumeLayout(false);
            this.group_order_history_info.PerformLayout();
            this.group_footer.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox ServiceID;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox ClientID;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox OrgID;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox CarID;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox Descr;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnEndCall;
        private System.Windows.Forms.CheckBox chkPredvar;
        private System.Windows.Forms.DateTimePicker dtPredvar;
        private System.Windows.Forms.DateTimePicker tmPredvar;
        private System.Windows.Forms.Label dt_order_info;
        private System.Windows.Forms.Label FromPhone;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.CheckBox chk_EndOrder;
        private System.Windows.Forms.ComboBox comboEndState;
        private System.Windows.Forms.Button btnEndOrder;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox summ;
        private System.Windows.Forms.Button btnSumtoDriver;
        private System.Windows.Forms.TextBox CarIDView;
        private System.Windows.Forms.Button btnCarDial;
        private System.Windows.Forms.Button btnCarMessage;
        private System.Windows.Forms.TextBox TalonNumber;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button расчетМаршрута;
        private System.Windows.Forms.GroupBox group_beznal;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label DistanceMoney;
        private System.Windows.Forms.ComboBox predvShowDelta;
        private System.Windows.Forms.GroupBox group_calculator;
        private System.Windows.Forms.ComboBox AddrTo;
        private System.Windows.Forms.TextBox HouseTo2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox group_header;
        private System.Windows.Forms.ComboBox ZoneID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox ConnectPhone;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox AddrFrom;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox HouseFrom;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox FlatFrom;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox AddrFromName;
        private System.Windows.Forms.Button btnPhone2Dial;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnPhone1Dial;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox AddrToName;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox RaionToID;
        private System.Windows.Forms.Button btnPhoneInfo;
        private System.Windows.Forms.ListBox lbPhoneInfo;
        private System.Windows.Forms.ComboBox AddrToForDriver;
        private System.Windows.Forms.Label phone_info;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.GroupBox group_order_history_info;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.CheckBox chk_beznal;
        private System.Windows.Forms.CheckBox chk_calculator;
        private System.Windows.Forms.Label org_prim;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label calculator_text;
        private System.Windows.Forms.Panel group_footer;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox DistanceCity;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox calc_check_4;
        private System.Windows.Forms.CheckBox calc_check_3;
        private System.Windows.Forms.CheckBox calc_check_2;
        private System.Windows.Forms.CheckBox calc_check_1;
        private System.Windows.Forms.Button btn_fix_to;
        private System.Windows.Forms.Button btn_fix_from;
        private System.Windows.Forms.ContextMenuStrip order_prim_menu;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox wait_time;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox distanceMgor;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox Inerpoint2_name;
        private System.Windows.Forms.ComboBox Inerpoint1_name;
        private System.Windows.Forms.ComboBox Inerpoint2_address;
        private System.Windows.Forms.ComboBox Inerpoint1_address;
        private System.Windows.Forms.TextBox Inerpoint1_house;
        private System.Windows.Forms.TextBox Inerpoint2_house;
        private System.Windows.Forms.CheckBox chk_close_not_saved;
        private System.Windows.Forms.TextBox order_price_Descr;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox black_list_money;
        private System.Windows.Forms.CheckBox calc_check_black_list;
        private System.Windows.Forms.TextBox black_list_car_id;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnMakeAbonent;
        public System.Windows.Forms.TextBox Phone;
        private System.Windows.Forms.ComboBox Inerpoint4_name;
        private System.Windows.Forms.ComboBox Inerpoint3_name;
        private System.Windows.Forms.ComboBox Inerpoint4_address;
        private System.Windows.Forms.ComboBox Inerpoint3_address;
        private System.Windows.Forms.TextBox Inerpoint3_house;
        private System.Windows.Forms.TextBox Inerpoint4_house;
        private System.Windows.Forms.Button btnOkDialToPassenger;
        private System.Windows.Forms.DateTimePicker bnal_opl_tm;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button btnMakeReview;
        private System.Windows.Forms.Label month_limit;
        private System.Windows.Forms.CheckBox order_10percent;
        private System.Windows.Forms.Label label_taxometr_info;
        private System.Windows.Forms.CheckBox chk_podacha;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Label org_subdetail_prim;
        private System.Windows.Forms.Label org_detail_prim;
        private System.Windows.Forms.ComboBox ORGSUBDETAIL_ID;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox ORGDETAIL_ID;
        private System.Windows.Forms.Button btnДанные;
    }
}
