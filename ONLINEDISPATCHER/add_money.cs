﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ONLINEDISPATCHER {
    public partial class add_money : Form {
        public add_money() {
            InitializeComponent();
        }

        public int selected_car_id = 0;
        public int selected_car_service_id = 0;

        public int selected_driver_id = 0;
        public int selected_org_id = 0;


        DataTable cars = null;
        DataTable organizations = null;

        private void add_money_Load(object sender, EventArgs e) {
            cars = DataBase.mssqlRead("SELECT CARS.ID, cast (CARS.ID as varchar(6))+' '+  CARS.COLOR +' '+  CARS.mark +' '+ cars.gosnum +' '+drivers.fio +' '+ drivers.phones CAR_DRIVER_DESC  FROM CARS, DRIVERS WHERE CARS.ID = DRIVERS.CARID order by CARS.ID");
            organizations = DataBase.mssqlRead("SELECT * FROM ORGANIZATIONS WHERE AGREEMENTSTATUS=1 order by name");


            //    ServiceID.Items.Clear();
            comboCars.DataSource = cars;
            comboCars.DisplayMember = "CAR_DRIVER_DESC";
            comboCars.ValueMember = "ID";
            comboCars.DropDownStyle = ComboBoxStyle.DropDown;
            comboCars.AutoCompleteSource = AutoCompleteSource.ListItems;
            //  ZoneID.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            //comboCars.SelectedIndex = -1;
            comboCars.AutoCompleteMode = AutoCompleteMode.SuggestAppend;

            comboCars.SelectedValue = selected_car_id.ToString();




            this.OrgID.DataSource = organizations;
            OrgID.DisplayMember = "Name";
            OrgID.ValueMember = "ID";

            OrgID.AutoCompleteSource = AutoCompleteSource.ListItems;
            OrgID.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            OrgID.SelectedIndex = -1;

            if (selected_org_id > 0)
            {
                radioButton2.Checked = true;
                OrgID.SelectedValue = selected_org_id.ToString();
            }



        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e) {
            comboCars.Enabled = radioButton1.Checked;
            OrgID.Enabled = !comboCars.Enabled;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e) {
            comboCars.Enabled = radioButton1.Checked;
            OrgID.Enabled = !comboCars.Enabled;
        }

        private void button1_Click(object sender, EventArgs e) {

            if (!MAIN_FORM.OperatorIsAdmin())
            {
                MessageBox.Show("У вас нет доступа на данную операцию");
                return;

            }


            int summ = 0;
            int type = 0;
            int.TryParse(textbox_summ.Text, out summ);

            

            if (radioButton1.Checked) {
                if (summ < 0) {
                    type = 2;
                    summ = -summ;
                }

                String[] fields = { "driverid", "ServiceID", "OpID", "dtArrive", "sum", "type", "promised_payment", "Descr" };
                object[] values = { selected_driver_id, selected_car_service_id, MAIN_FORM.OperatorID, payment_date.Value, summ, type, 0, "касса: " + descr.Text };

                DataBase.mssqlInsert("BANK", fields, values);

                Close();
            }

            if (radioButton2.Checked) {

                if (summ < 0) {
                    type = 2;
                    summ = -summ;
                }

                String[] fields = { "clientID", "OpID", "dtArrive", "sum", "type", "Descr" };
                object[] values = { (int)OrgID.SelectedValue, MAIN_FORM.OperatorID, payment_date.Value.Date, summ, type, "БЕЗНАЛ : " + descr.Text };

                DataBase.mssqlInsert("BANK", fields, values);

                Close();
            }
        }

        private void comboCars_SelectedIndexChanged(object sender, EventArgs e) {
            if (comboCars.SelectedValue != null) {
                try {
                    //int s = (String)comboCars.SelectedValue;
                    //System.Console.WriteLine(s);
                    int carid = (int)comboCars.SelectedValue;
                    selected_car_id = carid;
                    DataTable carinfo = DataBase.mssqlRead("SELECT CARS.ID, cast (CARS.ID as varchar(6))+' '+  CARS.COLOR +' '+  CARS.mark +' '+ cars.gosnum CAR_DESC, drivers.fio +' '+ drivers.phones DRIVER_DESC, drivers.id driver_id, cars.serviceid car_service_id FROM CARS left join DRIVERS on DRIVERS.ID = CARS.DRIVERID WHERE CARS.ID = " + carid.ToString());
                    selected_car_service_id = (int)carinfo.Rows[0]["car_service_id"];
                    selected_driver_id = (int)carinfo.Rows[0]["driver_id"];

                } catch (Exception ex) {

                }
            }
        }

        private void OrgID_SelectedIndexChanged(object sender, EventArgs e) {
            if (OrgID.SelectedValue != null) {
                try {
                    //int s = (String)comboCars.SelectedValue;
                    //System.Console.WriteLine(s);
                    selected_org_id = (int)OrgID.SelectedValue;

                } catch (Exception ex) {

                }
            }
        }

        private void add_money_Shown(object sender, EventArgs e)
        {
         //   if (radioButton2.Enabled=false)

            textbox_summ.Focus();

        }
    }
}
