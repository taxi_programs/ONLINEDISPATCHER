﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LumiSoft.Net.SIP.Stack;
using LumiSoft.Net.SDP;
using csmapcontrol;
using System.Diagnostics;


namespace ONLINEDISPATCHER
{
    public partial class order_big : Form
    {
        int ms_start = 0;


        private int m_iID = 0;
        private int m_iServiceID = 27;

        private string m_sCallID = "";

        private static DataTable services = null;
        private static DataTable zones = null;
        private static DataTable zonesTo = null;

        private static DataTable organizations = null;
        private static DataTable organization_detail = null;
        private static DataTable organization_subdetail = null;

        //private static DataTable adresses_names = DataBase.mssqlRead("select distinct name from gis_adresses order by name");
        private static DataTable adresses_names = gisModule.get_adresses_names();// DataBase.mssqlRead("select distinct name from adresses "+
//"union "+
//"select distinct name from gis_adresses order by name");


        public MapPath ORDER_PATH = new MapPath();

        //public gisModule.geoPoint geo_location_from = new gisModule.geoPoint();
        //public gisModule.geoPoint geo_location_to = new gisModule.geoPoint();

        //private double path_distance = 0;


        private double city_distance = 0;
        private double mgor_distance = 0;

        private int calculator_add_money = 0;
        private int total_money = 0;
        private String calc_check="";
        private string sms_text = "";
        private string abonent_skid_percent = "";


        DataTable cars = DataBase.mssqlRead("SELECT ID, cast(ID as varchar(5)) + ' '+ color + ' ' + Mark + ' ' + GosNum as car_name from CARS order by cars.id");//where cars.status&1024 <> 1024 order by cars.ID");


        DataTable phone_data = new DataTable();


        private static Point lastLocation = new Point(100, 100);
        //SipCaller m_pSipCaller = null;


        private bool m_bLoading = false;

        public void doOpenAllfields()
        {
            CarID.SelectedItem = null;
            CarID.Text = "";
            this.CarIDView.Text = "";
            this.ZoneID.Enabled = true;
            this.RaionToID.Enabled = true;
            CarID.Visible = true;

            this.ZoneID.Enabled = true;
            this.AddrFrom.Enabled = true;
            this.HouseFrom.Enabled = true;
            this.AddrToForDriver.Enabled = true;
            this.AddrTo.Enabled = true;
            this.HouseTo2.Enabled = true;
            this.AddrFromName.Enabled = true;
            this.AddrToName.Enabled = true;
            this.FlatFrom.Enabled = true;
            this.ConnectPhone.Enabled = true;
            this.Phone.Enabled = true;
        }


        public void setAsNewOrder()
        {
            m_iID = 0;
            doOpenAllfields();
            
          //  this.DistanceCity.Text = ""; убрано. т.к. при дублировании теряется результат расчета
                

        }


        private void addFocusColorChangeHandler() {


            foreach (Control c0 in this.Controls) {
                //if (c.TabStop == true) 
                {
                    foreach (Control c in c0.Controls)
                    {
                        c.Enter += new System.EventHandler(this.ComponentEnter);

                        //System.Console.WriteLine("Add handler to " + c.Name);
                        c.Leave += new System.EventHandler(this.ComponentLeave);

                        foreach (Control c2 in c.Controls)
                        {
                            c2.Enter += new System.EventHandler(this.ComponentEnter);

                            //System.Console.WriteLine("Add handler to " + c.Name);
                            c2.Leave += new System.EventHandler(this.ComponentLeave);
                        }
                    }
                }
            }
                
        
            
        }

        public order_big()
        {
            InitializeComponent();

            addFocusColorChangeHandler();

//            m_pSipCaller = caller;
            this.btnEndCall.Visible = true;

            if (order_big.lastLocation != null)
            {
                this.StartPosition = FormStartPosition.Manual;
                this.Location = order_big.lastLocation;
            }


            this.Height = this.group_header.Height + this.group_footer.Height+groupBox1.Height+35;

        }


        public void doLoad( int order_id, String phone, String from_phone, String call_id )
        {
            Stopwatch sWatch = new Stopwatch();
            sWatch.Start();


            //загрузка постоянных справочников заказа

            if (!MAIN_FORM.OperatorIsAdmin())
            {
                bnal_opl_tm.Enabled = false;
            }


            bnal_opl_tm.Value = bnal_opl_tm.MinDate;
            bnal_opl_tm.Text = "01.01.2000";

            m_bLoading = true;

            {
                if (services == null)
                    services = DataBase.mssqlRead("SELECT * FROM SERVICES where ID>0");
                if (zones == null)
                    zones = DataBase.mssqlRead("select * from zones order by (case when ord< 1000 then descr else 'я'+descr end)");
                if (zonesTo == null)
                    zonesTo = DataBase.mssqlRead("select * from zones order by (case when ord< 1000 then descr else 'я'+descr end)");

                
                if (organizations == null)
                    organizations = DataBase.mssqlRead("SELECT * FROM ORGANIZATIONS WHERE AGREEMENTSTATUS=1 order by name");
                if (organization_detail == null)
                    organization_detail = DataBase.mssqlRead("SELECT id, ORGANIZATIONID, name, moneylimit, name +' код:'+password+case when moneylimit=0 then '' else ' лимит:'+cast(moneylimit as varchar(10))+'р.' end + case when timelimit='' then '' else ' время: '+timelimit end descr, ORGANIZATION_DETAIL.descr as descr2, month_money_limit FROM ORGANIZATION_DETAIL where is_active=1 order by name");
                if (organization_subdetail == null)
                    organization_subdetail = DataBase.mssqlRead("SELECT id, ORGANIZATION_DETAIL_ID, name, descr FROM ORGANIZATION_SUBDETAIL where is_active=1 order by name");
                
                    //organization_detail = DataBase.mssqlRead("SELECT id, ORGANIZATIONID, name, moneylimit, name +' код:'+password+' лимит:'+cast(moneylimit as varchar(10))+'р.'+' время: '+timelimit descr, month_money_limit FROM ORGANIZATION_DETAIL where is_active=1 order by name");

                
      
            }


            Console.WriteLine("LoadOrder 0_0: " + sWatch.ElapsedMilliseconds.ToString());

            if (order_prim_menu.Items.Count == 0)
            {
                DataTable dt0 = DataBase.mssqlRead("SELECT VALUE1 FROM SETTINGS WHERE SETT_NAME='ORDER_PRIM_STRINGS' ORDER BY VALUE1");
                foreach (DataRow row in dt0.Rows)
                {
                    order_prim_menu.Items.Add(row["VALUE1"].ToString()).Click += new EventHandler(order_prim_menu_click);

                }

            }


            Console.WriteLine("LoadOrder 0: " + sWatch.ElapsedMilliseconds.ToString());


            dtPredvar.Value = DateTime.Now;
            tmPredvar.Value = DateTime.Now;
            tmPredvar.Value.AddSeconds(-tmPredvar.Value.Second);

            predvShowDelta.SelectedIndex = 0;

            m_sCallID = call_id;

            AddrFromName.Tag = "BLOCK_CHANGE_TEXT_EVENT";
            AddrFrom.Tag = "BLOCK_CHANGE_TEXT_EVENT";

            FromPhone.Text = from_phone;

            if (from_phone.CompareTo( "420070")==0){
                FromPhone.Text = "ВЕТЕРАН";
                FromPhone.BackColor = Color.Red;
            }



//            btn_fix_from.Visible = (MAIN_FORM.OperatorID == 129) || (MAIN_FORM.OperatorID == 123);
//            btn_fix_to.Visible = (MAIN_FORM.OperatorID == 129) || (MAIN_FORM.OperatorID == 123);


            //загрузить стпавочники
            //загрузить данные по заказу

            //справочники: службы, стоянки.
            //при открытии комбобоксов - загружать содержимое: места, организации, автомобили

            if (cars.Rows.Count > 0)
            {
                CarID.DataSource = cars;
                CarID.DisplayMember = "car_name";
                CarID.ValueMember = "ID";
                CarID.SelectedItem = null;
                //CarID.SelectedValue = carid;
            }



            this.OrgID.DataSource = organizations;
            OrgID.DisplayMember = "Name";
            OrgID.ValueMember = "ID";

            OrgID.AutoCompleteSource = AutoCompleteSource.ListItems;
            OrgID.AutoCompleteMode = AutoCompleteMode.Suggest;
            OrgID.SelectedIndex = -1;




            //    ServiceID.Items.Clear();
            ServiceID.DataSource = services;
            ServiceID.DisplayMember = "Name";
            ServiceID.ValueMember = "ID";

            ServiceID.SelectedValue = 27;

            DataRow[] services_for_line = services.Select("PhoneList='" + FromPhone.Text + "'");
            if (services_for_line.Length > 0)
                ServiceID.SelectedValue = services_for_line[0]["ID"];


            if ( ServiceID.SelectedValue.ToString().Equals("25")){
                chk_beznal.Checked = true;
            }

            ServiceID.DropDownHeight = ServiceID.ItemHeight * ((DataTable)ServiceID.DataSource).Rows.Count;


            ZoneID.DataSource = zones;
            ZoneID.DisplayMember = "Descr";
            ZoneID.ValueMember = "ID";

            ZoneID.AutoCompleteSource = AutoCompleteSource.ListItems;
            ZoneID.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            ZoneID.SelectedIndex = -1;

            ZoneID.DropDownHeight = ZoneID.ItemHeight * ((DataTable)ZoneID.DataSource).Rows.Count;


            RaionToID.DataSource = zonesTo;
            RaionToID.DisplayMember = "Descr";
            RaionToID.ValueMember = "ID";

            RaionToID.AutoCompleteSource = AutoCompleteSource.ListItems;
            RaionToID.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            RaionToID.SelectedIndex = -1;

            RaionToID.DropDownHeight = RaionToID.ItemHeight * ((DataTable)RaionToID.DataSource).Rows.Count;

            Console.WriteLine("LoadOrder 0_1: " + sWatch.ElapsedMilliseconds.ToString());

            doFillAddrFromName();
            Console.WriteLine("LoadOrder 0_0_1: " + sWatch.ElapsedMilliseconds.ToString());
            doFillAddrFrom();


            Console.WriteLine("LoadOrder 1: " + sWatch.ElapsedMilliseconds.ToString());

            /*
            AddrToForDriver.AutoCompleteMode = AutoCompleteMode.None;
            AddrToForDriver.AutoCompleteCustomSource.Clear();
            foreach (DataRow row in zones.Rows)
            {
                AddrToForDriver.Items.Add(row["Descr"].ToString());

            }
            AddrToForDriver.AutoCompleteSource = AutoCompleteSource.ListItems;
            AddrToForDriver.AutoCompleteMode = AutoCompleteMode.SuggestAppend;

            AddrToForDriver.DropDownHeight = AddrToForDriver.ItemHeight * AddrToForDriver.Items.Count;
            */

            if (order_id == 0)
            {
                //AddrFrom.Tag = "";
             //   doFindAddrFromName();
                //AddrFrom.Tag = "BLOCK_CHANGE_TEXT_EVENT";

                
            }


            if (order_id > 0)
            {

                this.group_order_history_info.Visible = true;

                DataTable ord_data = DataBase.mssqlRead("SELECT * FROM ORDERS WHERE ID=" + order_id.ToString());
                m_iID = ((int)ord_data.Rows[0]["ID"]);


                String time2meet = ((int)ord_data.Rows[0]["time2meet"]).ToString();

                if (time2meet == "1")
                    time2meet = " (5 мин)";
                if (time2meet == "2")
                    time2meet = " (10 мин)";
                if (time2meet == "3")
                    time2meet = " (15 мин)";
                if (time2meet == "4")
                    time2meet = " (3 мин)";
                if (time2meet == "5")
                    time2meet = " (6 мин)";
                if (time2meet == "6")
                    time2meet = " (9 мин)";


                if (ord_data.Rows[0]["predvShowDelta"] != DBNull.Value) {
                    int predvShowDeltaValue = ((int)ord_data.Rows[0]["predvShowDelta"]);
                    if (predvShowDeltaValue == 20)
                        predvShowDelta.SelectedIndex = 0;
                    if (predvShowDeltaValue == 30)
                        predvShowDelta.SelectedIndex = 1;
                    if (predvShowDeltaValue == 40)
                        predvShowDelta.SelectedIndex = 2;
                }

                

                this.dt_order_info.Text = "Поступил:    "+ord_data.Rows[0]["dtCreate"].ToString();
                dt_order_info.Text += "\r\n" + "Раздача:         " + ord_data.Rows[0]["dtArrive"].ToString();
                dt_order_info.Text += "\r\n"+"Взят водителем:         " + ord_data.Rows[0]["dtBegin"].ToString();
                dt_order_info.Text += "\r\n" + "Выполнен: " + ord_data.Rows[0]["dtEnd"].ToString();


                this.label_taxometr_info.Text = "Цена по таксометру: " + (ord_data.Rows[0]["tax_money"]).ToString() + " руб";
                if (ord_data.Rows[0]["tax_podacha_km"] != DBNull.Value)
                {
                    this.label_taxometr_info.Text += "\r\nПодача: " + ((double)ord_data.Rows[0]["tax_podacha_km"]).ToString("0.00") + " км, " + ((double)ord_data.Rows[0]["tax_podacha_time"]).ToString("0.0") + " мин";
                    this.label_taxometr_info.Text += "\r\n" + "Город: " + ((double)ord_data.Rows[0]["tax_city_km"]).ToString("0.00") + " км, " + ((double)ord_data.Rows[0]["tax_city_wait_time"]).ToString("0.0") + " мин платного"; ;
                    this.label_taxometr_info.Text += "\r\n" + "Загород: " + ((double)ord_data.Rows[0]["tax_mgor_km"]).ToString("0.00") + " км, " + ((double)ord_data.Rows[0]["tax_mgor_wait_time"]).ToString("0.0") + " мин платного"; ;

                    int uslugi = (int)ord_data.Rows[0]["tax_uslugi"];
                    String sUslugi = "";
                    if ((uslugi & 1) == 1) sUslugi += "ОТД РАЙОН ";
                    if ((uslugi & 2) == 2) sUslugi += "БАГАЖ ";
                    if ((uslugi & 4) == 4) sUslugi += "ЖИВОТНОЕ ";
                    if ((uslugi & 8) == 8) sUslugi += "ГРУНТ ";
                    if (sUslugi != "")
                        this.label_taxometr_info.Text += "\r\n" + "Услуги: " + sUslugi;
                }





                Console.WriteLine("LoadOrder 2: " + sWatch.ElapsedMilliseconds.ToString());


                this.FromPhone.Text = ((String)ord_data.Rows[0]["FromPhone"]);

                this.Phone.Text = ((String)ord_data.Rows[0]["Phone"]);
                this.ConnectPhone.Text = ((String)ord_data.Rows[0]["ConnectPhone"]);
                this.AddrFrom.Text = ((String)ord_data.Rows[0]["AddrFrom"]);

                this.HouseFrom.Text = ((String)ord_data.Rows[0]["HouseFrom"]);
                this.FlatFrom.Text = ((String)ord_data.Rows[0]["FlatFrom"]);
                this.AddrFromName.Text = ((String)ord_data.Rows[0]["AddrFromName"]);
                this.AddrToForDriver.Text = ((String)ord_data.Rows[0]["AddrTo"]);

                if (ord_data.Rows[0]["HouseTo"] != DBNull.Value)
                    this.HouseTo2.Text = ((String)ord_data.Rows[0]["HouseTo"]);
                this.AddrToName.Text = ((String)ord_data.Rows[0]["AddrToName"]);
                


                if (ord_data.Rows[0]["AddrTo2"] != DBNull.Value)
                    this.AddrTo.Text = ((String)ord_data.Rows[0]["AddrTo2"]);
                if (ord_data.Rows[0]["DistanceCity"] != DBNull.Value) 
                    DistanceCity.Text = ((double)ord_data.Rows[0]["DistanceCity"]).ToString("0.00");
                if (ord_data.Rows[0]["DistanceMGor"] != DBNull.Value)
                    distanceMgor.Text = ((double)ord_data.Rows[0]["DistanceMGor"]).ToString("0.00");
                
                

                if (ord_data.Rows[0]["wait_time"] != DBNull.Value)
                    wait_time.Text = ((double)ord_data.Rows[0]["wait_time"]).ToString("0.00");

                if (ord_data.Rows[0]["calc_check"] != DBNull.Value) {
                    calc_check = ord_data.Rows[0]["calc_check"].ToString();

                    for (int i = 0; i < calc_check.Length; i++) {
                        if (i == 0)
                            calc_check_1.Checked = (calc_check[i]=='1');
                        if (i == 1)
                            calc_check_2.Checked = (calc_check[i] == '1');
                        if (i == 2)
                            calc_check_3.Checked = (calc_check[i] == '1');
                        if (i == 3)
                            calc_check_4.Checked = (calc_check[i] == '1');
                        if (i == 4)
                            calc_check_black_list.Checked = (calc_check[i] == '1');
                    }
                }


                if (ord_data.Rows[0]["black_list_money"] != DBNull.Value)
                    black_list_money.Text = (ord_data.Rows[0]["black_list_money"]).ToString();
                if (ord_data.Rows[0]["black_list_car_id"] != DBNull.Value)
                    black_list_car_id.Text = (ord_data.Rows[0]["black_list_car_id"]).ToString();





                Console.WriteLine("LoadOrder 3: " + sWatch.ElapsedMilliseconds.ToString());



                //выделить то что после палки - это цена
                //Очистить примечание от цены
                this.Descr.Text = ((String)ord_data.Rows[0]["Descr"]);
                int pos = this.Descr.Text.IndexOf('|');
                if (pos >= 0)
                {
                    this.order_price_Descr.Text = this.Descr.Text.Substring(pos+1);
                    this.Descr.Text = this.Descr.Text.Substring(0, pos);
                }
                calculator_text.Text = "";

                this.ClientID.Text = ((String)ord_data.Rows[0]["ClientName"]);
                this.summ.Text = ord_data.Rows[0]["BnalMoney"].ToString();
                this.TalonNumber.Text = (ord_data.Rows[0]["TalonNumber"]).ToString();

                this.ClientID.Tag = null;
                if (ord_data.Rows[0]["ClientId"] != DBNull.Value)
                    this.ClientID.Tag = ((int)ord_data.Rows[0]["ClientId"]);


                if (ord_data.Rows[0]["is_beznal"] != DBNull.Value)
                    this.chk_beznal.Checked = Convert.ToBoolean(ord_data.Rows[0]["is_beznal"]);
                if (ord_data.Rows[0]["is_calculated"] != DBNull.Value)
                    this.chk_calculator.Checked = Convert.ToBoolean(ord_data.Rows[0]["is_calculated"]);
                if (ord_data.Rows[0]["order_10percent"] != DBNull.Value)
                    this.order_10percent.Checked = Convert.ToBoolean(ord_data.Rows[0]["order_10percent"]);

                if (ord_data.Rows[0]["chk_podacha"] != DBNull.Value)
                    this.chk_podacha.Checked = Convert.ToBoolean(ord_data.Rows[0]["chk_podacha"]);
                



                

                //bnal_opl_tm.
                if (ord_data.Rows[0]["bnal_opl_tm"] != DBNull.Value) {
                    
                    bnal_opl_tm.Value = (DateTime) ord_data.Rows[0]["bnal_opl_tm"];
                    if (this.bnal_opl_tm.Value != bnal_opl_tm.MinDate)
                        bnal_opl_tm.Checked = true;
                }
                if (ord_data.Rows[0]["predvar"] != DBNull.Value)
                    this.chkPredvar.Checked = Convert.ToBoolean(ord_data.Rows[0]["predvar"]);

                if (ord_data.Rows[0]["dtPredvar"] != DBNull.Value)
                {
                    this.dtPredvar.Value = ((DateTime)ord_data.Rows[0]["dtPredvar"]);
                    this.tmPredvar.Value = ((DateTime)ord_data.Rows[0]["dtPredvar"]);
                }

                if (ord_data.Rows[0]["ServiceID"].ToString() != "")
                {
                    ServiceID.SelectedValue = ord_data.Rows[0]["ServiceID"].ToString();
                    m_iServiceID = (int)ord_data.Rows[0]["ServiceID"];
                }

                

                if (ord_data.Rows[0]["ZoneID"].ToString() != "")
                {
                    ZoneID.SelectedValue = ord_data.Rows[0]["ZoneID"].ToString();
                }

                if (ord_data.Rows[0]["Raionto"].ToString() != "")
                {
                    RaionToID.SelectedValue = ord_data.Rows[0]["Raionto"].ToString();
                }

                if (ord_data.Rows[0]["OrgID"].ToString() != "")
                {
                    OrgID.SelectedValue = ord_data.Rows[0]["OrgID"].ToString();
                }

                if (ord_data.Rows[0]["OrganizationDetailID"].ToString() != "")
                {
                    ORGDETAIL_ID.SelectedValue = ord_data.Rows[0]["OrganizationDetailID"].ToString();
                }
                if (ord_data.Rows[0]["OrganizationSubDetailID"].ToString() != "")
                {
                    ORGSUBDETAIL_ID.SelectedValue = ord_data.Rows[0]["OrganizationSubDetailID"].ToString();
                }


                
                int from_lat=0, from_lon=0, from_moved=0;
                int to_lat = 0, to_lon = 0, to_moved = 0;

                if (ord_data.Rows[0]["lat"] != DBNull.Value)
                    from_lat = (int)ord_data.Rows[0]["lat"];
                if (ord_data.Rows[0]["lon"] != DBNull.Value)
                    from_lon = (int)ord_data.Rows[0]["lon"];
                if (ord_data.Rows[0]["from_geopoint_moved"] != DBNull.Value)
                    from_moved = (int)ord_data.Rows[0]["from_geopoint_moved"];

                Console.WriteLine("LoadOrder 5: " + sWatch.ElapsedMilliseconds.ToString());

                ORDER_PATH.setFirstPoint(from_lat / 1000000.0, from_lon / 1000000.0, "");
                ORDER_PATH.getFirstPoint().MarkerMoved = from_moved;
/*                this.geo_location_from = new gisModule.geoPoint();
                geo_location_from.lat = from_lat / 1000000.0;
                geo_location_from.lon = from_lon / 1000000.0;
                geo_location_from.isMoved = from_moved;
*/
                

                if (ord_data.Rows[0]["to_lat"] != DBNull.Value)
                    to_lat = (int)ord_data.Rows[0]["to_lat"];
                
                if (ord_data.Rows[0]["to_lon"] != DBNull.Value)
                    to_lon = (int)ord_data.Rows[0]["to_lon"];
                
                if (ord_data.Rows[0]["to_geopoint_moved"] != DBNull.Value)
                    to_moved = (int)ord_data.Rows[0]["to_geopoint_moved"];


                ORDER_PATH.setLastPoint(to_lat / 1000000.0, to_lon / 1000000.0, "");
                ORDER_PATH.getLastPoint().MarkerMoved = to_moved;
                /*this.geo_location_to = new gisModule.geoPoint();
                geo_location_to.lat = to_lat / 1000000.0;
                geo_location_to.lon = to_lon / 1000000.0;
                geo_location_to.isMoved = to_moved;
                */


                Console.WriteLine("LoadOrder 6: " + sWatch.ElapsedMilliseconds.ToString());

                ////Загрузка промежуточных точек маршрута - разбор строки

                //.ORDER_PATH.calc_path_points += "ADDRESS=;HOUSE=;NAME=;LAT=" + lat + ";LON=" + lon + ";MOVED=" + moved + ";|";
                String calc_path_points = ord_data.Rows[0]["calc_path_points"].ToString();

                int n = 1;
                if (calc_path_points != "")
                {
                    String[] arr1 = calc_path_points.Split('|');
                    foreach (String point_data in arr1)
                    {
                        String[] data_array = point_data.Split(';');
                        System.Collections.SortedList list_params = new System.Collections.SortedList();

                        foreach (String keyvalue in data_array)
                        {
                            String[] param_value = keyvalue.Split('=');
                            if (param_value.Length == 2)
                            {
                                //DistanceCity.Text = DistanceCity.Text.Replace('.', ',');
                                list_params.Add(param_value[0].Trim(), param_value[1].Trim().Replace('.', ','));
                            }
                        }

                        int lat = 0, lon = 0, moved = 0;
                        String address = "", house = "";
                        int.TryParse((String)list_params["LAT"], out lat);
                        int.TryParse((String)list_params["LON"], out lon);
                        int.TryParse((String)list_params["MOVED"], out moved);

                        address = (String)list_params["ADDRESS"];
                        house = (String)list_params["HOUSE"];

                        switch (n)
                        {
                            case 1: this.Inerpoint1_address.Text = address; this.Inerpoint1_house.Text = house; ORDER_PATH.setPoint(n, lat / 1000000.0, lon / 1000000.0, ""); ORDER_PATH.InterMarkers[n].MarkerMoved = moved; break;
                            case 2: this.Inerpoint2_address.Text = address; this.Inerpoint2_house.Text = house; ORDER_PATH.setPoint(n, lat / 1000000.0, lon / 1000000.0, ""); ORDER_PATH.InterMarkers[n].MarkerMoved = moved; break;
                            case 3: this.Inerpoint3_address.Text = address; this.Inerpoint3_house.Text = house; ORDER_PATH.setPoint(n, lat / 1000000.0, lon / 1000000.0, ""); ORDER_PATH.InterMarkers[n].MarkerMoved = moved; break;
                            case 4: this.Inerpoint4_address.Text = address; this.Inerpoint4_house.Text = house; ORDER_PATH.setPoint(n, lat / 1000000.0, lon / 1000000.0, ""); ORDER_PATH.InterMarkers[n].MarkerMoved = moved; break;

                            default: break;
                        }

/*                        if (n < 3)
                        {
                            ORDER_PATH.setPoint(n, lat / 1000000.0, lon / 1000000.0, "");
                            ORDER_PATH.InterMarkers[n].MarkerMoved = moved;
                        }
 */ 
                        n++;


                    }
                }



                Console.WriteLine("LoadOrder 7: " + sWatch.ElapsedMilliseconds.ToString());






                

                int carid = int.Parse("0" + ord_data.Rows[0]["CarID"].ToString());
                if (carid > 0)
                {
                    //DataTable dt = DataBase.mssqlRead("SELECT ID, cast(ID as varchar(5)) + ' '+ color + ' ' + Mark + ' ' + GosNum as car_name from CARS where ID=" + carid);
                    //if (dt.Rows.Count > 0)
                    {
                        //                        CarID.DataSource = dt;
                        //                        CarID.DisplayMember = "car_name";
                        //CarID.ValueMember = "ID";
                        CarID.SelectedValue = carid;
                        ServiceID.Enabled = false;
                        CarID.BackColor = Color.Red;
                        CarID.ForeColor = Color.Red;
                        this.ZoneID.Enabled = false;
                        this.RaionToID.Enabled = false;
                    }


                }


                if ((MAIN_FORM.OperatorID != 105) && ((int)ord_data.Rows[0]["opid"] !=1) && (m_iServiceID != 3))
                {
                    this.ZoneID.Enabled = false;
                    this.RaionToID.Enabled = false;
                    this.AddrFrom.Enabled = false;
                    this.HouseFrom.Enabled = false;
                    this.AddrToForDriver.Enabled = false;
                    //this.AddrTo.Enabled = false;
                    //this.HouseTo2.Enabled = false;
                    this.AddrFromName.Enabled = false;
                    this.AddrToName.Enabled = false;
                    this.FlatFrom.Enabled = false;
                    this.HouseTo2.Enabled = false;
                }
                //this.ConnectPhone.Enabled = false;
                

                //после загрузки заказа - закрыть поля от редактирования
                //машина, телефон, 
                CarID.Visible = false;
                CarIDView.Text = CarID.Text + time2meet;
                Phone.ReadOnly = true;


                DataTable last_call_data = DataBase.mssqlRead("select status,  last_call, getdate() as now_dt, tries, call_id  from call_out where nivr=2 and call_out.order_id=" + order_id.ToString());
                if (last_call_data.Rows.Count > 0)
                {
                    try
                    {
                        String delta = ((DateTime)last_call_data.Rows[0]["now_dt"]).Subtract((DateTime)last_call_data.Rows[0]["last_call"]).TotalMinutes.ToString("#.0");
                        int tries = (int)last_call_data.Rows[0]["tries"];
                        String last_call_id = last_call_data.Rows[0]["call_id"].ToString();

                        dt_order_info.Text += "\r\n" + "Дозвон:      ";
                        if (tries > 2)
                        {
                            dt_order_info.Text += "ДОЗВОН оператором";
                        }
                        else
                        {
                            if (last_call_data.Rows[0]["status"].ToString() == "f")
                                dt_order_info.Text += "НЕДОЗВОН";
                        }

                        dt_order_info.Text += " " + last_call_data.Rows[0]["last_call"] + " :  " + delta + " минут назад";
                    }
                    catch (Exception ex) { };
                }

               

            }
            Console.WriteLine("LoadOrder 8: " + sWatch.ElapsedMilliseconds.ToString());

            if (m_iID == 0)
            if (phone != "")
            {
                this.Phone.Text = phone;
                this.ServiceID.Text = from_phone;
                getPhoneInfo();
                doFindAddrFromName();

                //this.ServiceID

                this.AddrFrom.SelectAll();

                this.AddrFrom.Focus();
                


            }

            Console.WriteLine("LoadOrder 9: " + sWatch.ElapsedMilliseconds.ToString());

            //загрузить инфо по заказу. если новый - то заполнить поля.
            //Если телефон из черного списка - включить галку "ЧС" в расчете стоимости.
            doLoadPhoneInfo();

            doCalculatePath();
            
            doUpdateCalculatorText();

            Console.WriteLine("LoadOrder 10: " + sWatch.ElapsedMilliseconds.ToString());
//
            AddrFromName.Tag = 0;
            AddrFrom.Tag = 0;

            m_bLoading = false;
            AddrFrom.Focus();
            this.ActiveControl = AddrFrom;


            
            Console.WriteLine("LoadOrder: " + sWatch.ElapsedMilliseconds.ToString());

            sWatch.Stop();

            //AddrFrom.

           // System.Console.WriteLine("Открытие заказа" + (DateTime.Now.Second * 1000 + DateTime.Now.Millisecond - ms_start).ToString() + "мсек");

        }



        void doLoadPhoneInfo()
        {
            {
                String[] phoneInfo = caller.call_info.getPhoneInfo(this.Phone.Text);

                phone_info.Text = phoneInfo[2] + " заказов за 30 дней. " + phoneInfo[3];
                if ((phoneInfo[4] != "") && (phoneInfo[4] != null)) //телефон есть в списке
                {
                    String[] phone_addr_data = phoneInfo[4].Split('|');

                    /// cast(state as varchar(10))+'|'+FIO+'|'+address+'|'+houseNum+'|'+FlatNum+'|'+Descr
                    if (phone_addr_data.Length == 9)
                    {
                        String state = phone_addr_data[0];
                        String FIO = phone_addr_data[1];
                        String address = phone_addr_data[2];
                        String houseFrom = phone_addr_data[3];
                        String flatFrom = phone_addr_data[4];
                        String descr = phone_addr_data[5];
                        abonent_skid_percent = phone_addr_data[6];
                        String money_summ = phone_addr_data[7];
                        String car_id = phone_addr_data[8];


                        if (state == "1")
                        {
                            this.Phone.BackColor = Color.Black;
                            this.Phone.ForeColor = Color.White;
                        }
                        if (state == "2")
                        {
                            this.Phone.BackColor = Color.Gray;
                        }
                        if (state == "3")
                        {
                            this.Phone.BackColor = Color.Yellow;

                            //Здесь можно сохранить поле VIP - тогда будет выделяться жирным и показываться только 1 заказ

                        }
                        if (this.m_iID == 0)
                        {

                            this.HouseFrom.Text = houseFrom;
                            this.FlatFrom.Text = flatFrom;
                            this.AddrFrom.Text = address;
                            this.Descr.Text = descr;
                            if (state == "1")
                            {
                                this.chk_calculator.Checked = true;
                                this.calc_check_black_list.Checked = true;
                                money_summ = money_summ.Replace("-", "");
                                this.black_list_money.Text = money_summ;
                                this.black_list_car_id.Text = car_id;
                                doUpdateCalculatorText();

                            }
                        }

                    }
                }
                else
                {
                    //телефон - не в списках... если заказов много - можно сделать абонента
                    int cnt = 0;
                    int.TryParse(phoneInfo[2], out cnt);
                    if (cnt > 3)
                    {
                        btnMakeAbonent.Visible = true;
                    }

                }
                





                //инфо по телефону - из Phones : адрес имя клиента id клиента
                //несколько записей - выпадает меню с выбором



            }
        }

        void order_prim_menu_click(object sender, EventArgs e) {

        }

        private void getPhoneInfo()
        {
            /* поиск по номеру телефона - адрес, клиент, ч-б-серый список
             */
            phone_data = DataBase.mssqlRead("SELECT * FROM PHONES WHERE phone='" + this.Phone.Text+"'");
            if (phone_data.Rows.Count > 0)
            {
                this.AddrFrom.Text = phone_data.Rows[0]["Address"].ToString();
                this.HouseFrom.Text = phone_data.Rows[0]["HouseNum"].ToString();
                this.FlatFrom.Text = phone_data.Rows[0]["FlatNum"].ToString();
                this.ClientID.Text = phone_data.Rows[0]["FIO"].ToString();
                this.ClientID.Tag = int.Parse(phone_data.Rows[0]["ID"].ToString());
                this.Descr.Text = phone_data.Rows[0]["Descr"].ToString();
                String cln_fio = phone_data.Rows[0]["FIO"].ToString();


                if (phone_data.Rows.Count > 1)//несколько записей - заполнить адрес из первого и показать меню выбора
                {

                    lbPhoneInfo.Items.Clear();
                    
                    foreach (DataRow dr in phone_data.Rows){

                        String line = dr["FIO"].ToString() + " " +
                            dr["Address"].ToString() + " " +
                            dr["HouseNum"].ToString() + " " +
                            dr["FlatNum"].ToString() + " " +
                            dr["Descr"].ToString();
                        lbPhoneInfo.Items.Add(line);
                    }
                    lbPhoneInfo.Height = lbPhoneInfo.ItemHeight * (lbPhoneInfo.Items.Count+1);
                    lbPhoneInfo.Visible = true;

                }

            }
        }

        public int doSave()
        {
            //1. проверить валидность введенных данных
            //2. записать в базу
            //3. разговор прервать
            //4. окно закрыть.

            int result = 0;

            try {

                if (ServiceID.SelectedValue.ToString().Equals("25")) {
                    if ((OrgID.SelectedValue == null) || OrgID.SelectedValue.ToString().Equals("")) {
                        MessageBox.Show("Не указана организация по безналу");
                        return 0;
                    }
                }

                if (this.Phone.Text.Length==0)
                {
                    MessageBox.Show("Не указан телефон");
                    return 0;
                }
                if (this.Phone.Text=="anonymous")
                {
                    MessageBox.Show("Не указан телефон");
                    return 0;
                }

                if (this.ServiceID.SelectedIndex == -1) {

                    MessageBox.Show("Не указана  служба заказа");
                    return 0;
                }
                if (this.ZoneID.SelectedIndex == -1) {

                    MessageBox.Show("Не указана стоянка 'Откуда'");
                    return 0;
                }

                if (this.AddrToForDriver.Text.Length == 0) {

                    MessageBox.Show("Не указано 'Куда'");
                    return 0;
                }

/*
                if (this.m_iID > 0)
                    if (this.m_iServiceID != (int)this.ServiceID.SelectedValue) {
                        if (MessageBox.Show("Изменена служба. отменить старый заказ и создать новый?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1) == DialogResult.Yes) {//1 - узнать как закрыть старый заказ
                            //2 - id = -1;
                            //insert COMMANDS(ComID, OpID, dtArrive, Value1, Value2, State) 
                            //--	values(4,129,getdate(),653266,0,0)
      

                            String[] fields1 = { "DescrForClient" };
                            object[] values1 = { "снят дублированием со сменой зоны" };

                            DataBase.mssqlUpdate("ORDERS", fields1, values1, "ID", m_iID);


                            String[] fields = { "ComID", "OpID", "dtArrive", "Value1", "Value2", "State" };
                            object[] values = { 4, MAIN_FORM.OperatorID, DateTime.Now, m_iID, 3, 0 };

                            DataBase.mssqlInsert("COMMANDS", fields, values);


                            setAsNewOrder();
                        }



                    }
*/



                int state = 0;
                if (chkPredvar.Checked)
                    state = 5;
                int agreement = 0;
                if (OrgID.SelectedValue != null)
                    agreement = 1;


                DataRow[] services_for_id = services.Select("ID='" + ServiceID.SelectedValue + "'");
                String request_attributes = "";

                if (services_for_id.Length > 0)
                    request_attributes = (String)services_for_id[0]["PhoneList"];


                //координаты ОТКУДА
                int from_lat=0, from_lon=0,from_moved = 0;
                //Координаты КУДА
                int to_lat = 0, to_lon = 0, to_moved = 0;
               // if (geo_location_to != null)
                {
                    to_lat = (int)(ORDER_PATH.getLastPoint().lat * 1000000);
                    to_lon = (int)(ORDER_PATH.getLastPoint().lon * 1000000);
                    to_moved = ORDER_PATH.getLastPoint().MarkerMoved;

                }

                //if (ORDER_PATH.getFirstPoint() != null)
                {
                    from_lat = (int)(ORDER_PATH.getFirstPoint().lat * 1000000);
                    from_lon = (int)(ORDER_PATH.getFirstPoint().lon * 1000000);
                    from_moved = ORDER_PATH.getFirstPoint().MarkerMoved;

                }


                String calc_path_points = "";   //ADDRESS=адрес; HOUSE=дом; NAME=название; LAT=lat;LON=lon;MOVED=moved; |
                if (ORDER_PATH.InterMarkers.Count > 2)
                for (int i=1; i < ORDER_PATH.InterMarkers.Count - 1; i++){

                    csmapcontrol.Marker inter_point = ORDER_PATH.InterMarkers[i];
                    //if (inter_point.lat > 0)
                    {
                        int lat = (int) (inter_point.lat * 1000000);
                        int lon = (int) (inter_point.lon * 1000000);
                        int moved = inter_point.MarkerMoved;
                        String address = "";
                        String house = "";
                        switch (i)
                        {
                            case 0: address = AddrFrom.Text; house = HouseFrom.Text; break;
                            case 1: address = this.Inerpoint1_address.Text; house = this.Inerpoint1_house.Text; break;
                            case 2: address = this.Inerpoint2_address.Text; house = this.Inerpoint2_house.Text; break;
                            case 3: address = this.Inerpoint3_address.Text; house = this.Inerpoint3_house.Text; break;
                            case 4: address = this.Inerpoint4_address.Text; house = this.Inerpoint4_house.Text; break;
                            case 5: address = this.AddrToForDriver.Text; house = this.HouseTo2.Text; break;
                        }
                        calc_path_points += "ADDRESS=" + address + ";HOUSE=" + house + ";NAME=;LAT=" + lat + ";LON=" + lon + ";MOVED=" + moved + ";|";
                    }


                }



                double distanceCity = 0;
                DistanceCity.Text = DistanceCity.Text.Replace('.', ',');
                double.TryParse(DistanceCity.Text, out distanceCity);

                double _wait_time = 0;
                wait_time.Text = wait_time.Text.Replace('.', ',');
                double.TryParse(wait_time.Text, out _wait_time);



                //Окрудлить километры 
             //   distanceCity = (double)(int)(distanceCity + 0.5);


                calc_check = "";
                if (calc_check_1.Checked)
                    calc_check += '1';
                else
                    calc_check += '0';
                if (calc_check_2.Checked)
                    calc_check += '1';
                else
                    calc_check += '0';
                if (calc_check_3.Checked)
                    calc_check += '1';
                else
                    calc_check += '0';
                if (calc_check_4.Checked)
                    calc_check += '1';
                else
                    calc_check += '0';
                if (calc_check_black_list.Checked)
                    calc_check += '1';
                else
                    calc_check += '0';
                


                String Цена = "";
/*                if (total_money > 0) {
                    try {
                        Цена = "Цена " + total_money.ToString() + "руб. из расчета " + ((int)(distanceCity + 0.4999)).ToString() + " км, ";
                    } catch (Exception e) {
                    }
                }
 */
                DateTime bnalOplTm = bnal_opl_tm.MinDate;
                if (bnal_opl_tm.Checked){
                    bnalOplTm = bnal_opl_tm.Value; 
                }


                if (this.m_iID > 0) {
                    String[] fields = { "Phone", "ConnectPhone", "AddrFrom", "HouseFrom", "FlatFrom",
                                      "AddrFromName","AddrTo","HouseTo","AddrToName","Descr",
                                  "ServiceID","ZoneID",
                                  "ClientName", "clientId", "sum", 
                                  "BnalMoney","TalonNumber", "skidka", "DescrForClient", "predvar", "dtPredvar",
                                  "request_attributes", "raionto", "OrgID", "OrganizationDetailID", "Agreement", "AddrTo2", "DistanceCity", "DistanceMoney",
                                  "predvShowDelta", "is_beznal", "is_calculated", "lat", "lon", "from_geopoint_moved", "to_lat", "to_lon", "to_geopoint_moved", "calc_check",
                                  "calc_path_points", "wait_time", "black_list_money", "black_list_car_id", "order_money", "sms_text", "bnal_opl_tm", "order_10percent", "chk_podacha", "OrganizationSubDetailID"};



                    object[] values = {Phone.Text, ConnectPhone.Text, AddrFrom.Text, HouseFrom.Text, FlatFrom.Text,
                                      AddrFromName.Text,AddrToForDriver.Text,HouseTo2.Text,AddrToName.Text,Цена + Descr.Text +"|"+ order_price_Descr.Text,
                                  ServiceID.SelectedValue,ZoneID.SelectedValue,
                                  ClientID.Text, ClientID.Tag, summ.Text,
                                  summ.Text, TalonNumber.Text, 0, "", Convert.ToInt32(chkPredvar.Checked), dtPredvar.Value.Date.Add(tmPredvar.Value.TimeOfDay),
                                  request_attributes, RaionToID.SelectedValue, OrgID.SelectedValue, ORGDETAIL_ID.SelectedValue, agreement, AddrTo.Text, distanceCity, DistanceMoney.Tag,
                                  int.Parse(predvShowDelta.Text), Convert.ToInt32(chk_beznal.Checked), Convert.ToInt32(chk_calculator.Checked), from_lat, from_lon, from_moved, to_lat, to_lon, to_moved, calc_check,
                                  calc_path_points, _wait_time, black_list_money.Text, black_list_car_id.Text, total_money, sms_text, bnalOplTm, Convert.ToInt32(this.order_10percent.Checked), Convert.ToInt32(chk_podacha.Checked), ORGSUBDETAIL_ID.SelectedValue};

                    //DataBase.fbInsert("LOG", fields, values);
                    result = DataBase.mssqlUpdate("ORDERS", fields, values, "ID", m_iID);

                }
                if (this.m_iID == 0) {   //новый заказ 

                    int carid = 0;
                    int driverid = 0;

                    if (CarID.SelectedValue != null) {
                        carid = (int)CarID.SelectedValue;
                        driverid = DataBase.mssqlReadInt("SELECT ID FROM DRIVERS WHERE carid = "+carid.ToString());
                    }

                    

                    String[] fields = { "Phone", "ConnectPhone", "AddrFrom", "HouseFrom", "FlatFrom",
                                      "AddrFromName","AddrTo","HouseTo","AddrToName","Descr",
                                  "ServiceID","CarID","DriverID", "ZoneID", 
                                  "ClientName", "clientId", "sum", "opId",  
                                  "BnalMoney","TalonNumber", "skidka", "DescrForClient", "predvar", "dtPredvar",
                                  "state", "agreement", "resultCode", 
                                  "fromPhone", "request_attributes", "raionto", "OrgID", "OrganizationDetailID", "AddrTo2", "DistanceCity", "DistanceMoney",
                                  "predvShowDelta", "is_beznal", "is_calculated", "lat", "lon", "from_geopoint_moved", "to_lat", "to_lon", "to_geopoint_moved", "calc_check",
                                  "calc_path_points", "wait_time", "black_list_money", "black_list_car_id", "order_money", "sms_text", "bnal_opl_tm", "order_10percent", "chk_podacha", "OrganizationSubDetailID"};



                    object[] values = {Phone.Text, ConnectPhone.Text, AddrFrom.Text, HouseFrom.Text, FlatFrom.Text,
                                      AddrFromName.Text,AddrToForDriver.Text,HouseTo2.Text,AddrToName.Text,Цена + Descr.Text+"|"+order_price_Descr.Text,
                                  ServiceID.SelectedValue,carid, driverid, ZoneID.SelectedValue,
                                  ClientID.Text, ClientID.Tag, summ.Text, MAIN_FORM.OperatorID, 
                                  summ.Text, TalonNumber.Text, 0, "", Convert.ToInt32(chkPredvar.Checked), dtPredvar.Value.Date.Add(tmPredvar.Value.TimeOfDay),
                                  state, agreement, 0,
                                  FromPhone.Text, request_attributes, RaionToID.SelectedValue, OrgID.SelectedValue, ORGDETAIL_ID.SelectedValue, AddrTo.Text, distanceCity, DistanceMoney.Tag,
                                  int.Parse(predvShowDelta.Text),Convert.ToInt32(chk_beznal.Checked), Convert.ToInt32(chk_calculator.Checked), from_lat, from_lon, from_moved, to_lat, to_lon, to_moved, calc_check,
                                  calc_path_points,_wait_time, black_list_money.Text, black_list_car_id.Text, total_money, sms_text, bnalOplTm, Convert.ToInt32(this.order_10percent.Checked), Convert.ToInt32(chk_podacha.Checked), ORGSUBDETAIL_ID.SelectedValue};

                    //DataBase.fbInsert("LOG", fields, values);
                    result = DataBase.mssqlInsert("ORDERS", fields, values);


                    /*
                    16-07-2013 - отправка SMS - в момент назначения машины.
                    String phone = "";
                    phone = Phone.Text;
                    if (ConnectPhone.Text != "")
                        phone = ConnectPhone.Text;
                    if (sms_text != "")
                    if (phone.Length == 11) {


                        String[] fields1 = { "createTime", "tel_to", "text", "tries", "status", "proj_id" };
                        object[] values1 = { DateTime.Now, phone, sms_text, 0, "N", ServiceID.SelectedValue };

                        DataBase.mssqlInsert("sms_out", fields1, values1);
                    }
                     * 
                     */
                    //SMS
                    
//                    insert into sms_out(createTime, tel_to, text, last_try, tries, status)
//values(getdate(), '$phone', '$sms_text',  0, 'N');

                }

            } catch (Exception e) {
                MessageBox.Show(e.Message);
                return 0;
            }
            return result;
            /*искать клиента по телефону
             * искать адрес по телефону
             * 
             */
            
        }

        private void order_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "tAXADataSet.SERVICES". При необходимости она может быть перемещена или удалена.

            

        }

        private void order_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (chk_close_not_saved.Checked == false) {
                e.Cancel = true;
                return;
            }

            order_big.lastLocation = this.Location;

            /*
            if (this.m_pSipCaller != null)
            {
                MessageBox.Show("Закрыть заказ при активном звонке"
            }
            
            order.lastLocation = this.Location;
            if (this.m_pSipCaller != null) {
                m_pSipCaller.doHangup();
                m_pSipCaller.doStartPostCallTimer();
            }
             **/
            //to do сделать задержку на прием нового звонка 5 секунды
            
            //убрал автосброс звонка при закрытии формы заказа....

           // BeginInvoke(new MethodInvoker(delegate()
            //{
                MAIN_FORM.main_form_object.doStartPostCallTimer();
            //}));
                //m_pSipCaller.doStartPostCallTimer();


        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //при сохранении
            //Если изменилась служба  - старый заказ закрыть, этот сохранить как новый.
            //при назначенной машине - службу нельзя менять.

            //!!Проверить сумму закрытия безнала!


            int money = Int32.Parse("0" + this.summ.Text);
            int limit = 0;
            if (money > 0)
            if (ORGDETAIL_ID.SelectedValue != null)
                        if (ORGDETAIL_ID.SelectedValue.ToString() != "System.Data.DataRowView") {
                            DataRow[] select = organization_detail.Select(" ID=" + ORGDETAIL_ID.SelectedValue.ToString());
                            if (select.Length > 0) {
                                limit = (int)select[0]["moneylimit"];
                            }
                        }
            if ((limit > 0) && (money > limit))   //превышение лимита сотрудника, запрет сохранения !!!!
            {
                MessageBox.Show("Превышение лимита сотрудника");
                return;
            }



                
        
            if (this.order_price_Descr.Text=="")
                doCalculatePath();



            


            if (doSave() > 0) {
                chk_close_not_saved.Checked = true;
                this.Close();
            }

        }

        private void AddrFromName_Click(object sender, EventArgs e)
        {

            doFillAddrFromName();
            //теперь надо найти такое название в соответствии с адресом

//            doFindAddrFrom();

        }

        private void doFillAddrFromName()
        {
            if (AddrFromName.DataSource == null)
            {
                //DataTable adresses_names = DataBase.mssqlRead("select distinct name from adresses");
                AddrFromName.DataSource = adresses_names;
                AddrFromName.DisplayMember = "name";
                AddrFromName.ValueMember = "name";

                AddrFromName.AutoCompleteSource = AutoCompleteSource.ListItems;
                AddrFromName.AutoCompleteMode = AutoCompleteMode.SuggestAppend;

                //DataTable dt = DataBase.mssqlRead("select distinct name from adresses  order by name"); ;



                AddrToName.DataSource = adresses_names.Copy();
                AddrToName.DisplayMember = "name";
                AddrToName.ValueMember = "name";

                AddrToName.AutoCompleteSource = AutoCompleteSource.ListItems;
                AddrToName.AutoCompleteMode = AutoCompleteMode.SuggestAppend;

                
                Inerpoint1_name.DataSource = adresses_names.Copy();
                Inerpoint1_name.DisplayMember = "name";
                Inerpoint1_name.ValueMember = "name";

                Inerpoint1_name.AutoCompleteSource = AutoCompleteSource.ListItems;
                Inerpoint1_name.AutoCompleteMode = AutoCompleteMode.SuggestAppend;

                Inerpoint2_name.DataSource = adresses_names.Copy();
                Inerpoint2_name.DisplayMember = "name";
                Inerpoint2_name.ValueMember = "name";

                Inerpoint2_name.AutoCompleteSource = AutoCompleteSource.ListItems;
                Inerpoint2_name.AutoCompleteMode = AutoCompleteMode.SuggestAppend;

                
            }


        }

        

        private void AddrFrom_Click(object sender, EventArgs e)
        {
            
            doFillAddrFrom();
        }
        private void AddrFrom_Enter(object sender, EventArgs e)
        {
            
            doFillAddrFrom();
        }

        private void ComponentEnter(object sender, EventArgs e)
        {
            System.Console.WriteLine("COMPONENT_ENTER: " + sender.ToString());
            if (sender.GetType().Name == "TextBox")
            {
                ((TextBox)sender).BackColor = Color.LightSkyBlue;
            }

            if (sender.GetType().Name == "ComboBox") {
                ((ComboBox)sender).BackColor = Color.LightSkyBlue;
            }

            
            if (sender.GetType().Name == "Button") {
                ((Button)sender).BackColor = Color.LightSkyBlue;
            }

        }

        private void ComponentLeave(object sender, EventArgs e) {
            if (sender.GetType().Name == "TextBox") {
                ((TextBox)sender).BackColor = Color.White;
            }
            if (sender.GetType().Name == "ComboBox") {
                ((ComboBox)sender).BackColor = Color.White;
            }


            if (sender.GetType().Name == "Button") {
                ((Button)sender).BackColor = btnEndOrder.BackColor;
            }
        }

        private void doFillAddrFrom()
        {
            if (AddrFrom.AutoCompleteCustomSource.Count == 0)
            {
//                DataTable adresses_address = DataBase.mssqlRead("select distinct address from adresses WHERE state=0");
               // DataTable adresses_address = DataBase.mssqlRead("select distinct street from gis_adresses");

                AddrFrom.AutoCompleteMode = AutoCompleteMode.None;
                AddrFrom.AutoCompleteCustomSource.Clear();
                AddrFrom.AutoCompleteCustomSource.AddRange(gisModule.getStreets(""));


/*                
                foreach (DataRow row in adresses_address.Rows)
                {
                    AddrFrom.AutoCompleteCustomSource.Add(row[0].ToString());
                   
                }
*/
                AddrFrom.AutoCompleteSource = AutoCompleteSource.CustomSource;
                AddrFrom.AutoCompleteMode = AutoCompleteMode.SuggestAppend;


                AddrToForDriver.AutoCompleteMode = AutoCompleteMode.None;
                AddrToForDriver.AutoCompleteCustomSource.Clear();
                AddrToForDriver.AutoCompleteCustomSource.AddRange(gisModule.getStreets(""));

                AddrToForDriver.AutoCompleteSource = AutoCompleteSource.CustomSource;
                AddrToForDriver.AutoCompleteMode = AutoCompleteMode.SuggestAppend;




                Inerpoint1_address.AutoCompleteMode = AutoCompleteMode.None;
                Inerpoint1_address.AutoCompleteCustomSource.Clear();
                Inerpoint1_address.AutoCompleteCustomSource.AddRange(gisModule.getStreets(""));

                Inerpoint1_address.AutoCompleteSource = AutoCompleteSource.CustomSource;
                Inerpoint1_address.AutoCompleteMode = AutoCompleteMode.SuggestAppend;

                Inerpoint2_address.AutoCompleteMode = AutoCompleteMode.None;
                Inerpoint2_address.AutoCompleteCustomSource.Clear();
                Inerpoint2_address.AutoCompleteCustomSource.AddRange(gisModule.getStreets(""));

                Inerpoint2_address.AutoCompleteSource = AutoCompleteSource.CustomSource;
                Inerpoint2_address.AutoCompleteMode = AutoCompleteMode.SuggestAppend;

                Inerpoint3_address.AutoCompleteMode = AutoCompleteMode.None;
                Inerpoint3_address.AutoCompleteCustomSource.Clear();
                Inerpoint3_address.AutoCompleteCustomSource.AddRange(gisModule.getStreets(""));

                Inerpoint3_address.AutoCompleteSource = AutoCompleteSource.CustomSource;
                Inerpoint3_address.AutoCompleteMode = AutoCompleteMode.SuggestAppend;

                Inerpoint4_address.AutoCompleteMode = AutoCompleteMode.None;
                Inerpoint4_address.AutoCompleteCustomSource.Clear();
                Inerpoint4_address.AutoCompleteCustomSource.AddRange(gisModule.getStreets(""));

                Inerpoint4_address.AutoCompleteSource = AutoCompleteSource.CustomSource;
                Inerpoint4_address.AutoCompleteMode = AutoCompleteMode.SuggestAppend;

               


            }
            
        }

        private void HouseFrom_Click(object sender, EventArgs e)
        {
           // doFillHouseFrom();
        }
        private void HouseFrom_Enter(object sender, EventArgs e)
        {
            doFillHouseFrom();
        }

        private void doFillHouseFrom()
        {
            //return;
            if (AddrFrom.Text != "")
            {
                if (AddrFrom.Text != HouseFrom.Tag)
                {
//                    DataTable adresses_house = DataBase.mssqlRead("select distinct HouseNum from adresses where state=1 and Address='" + AddrFrom.Text.Replace("'", "") + "'");
                    //DataTable adresses_house = DataBase.mssqlRead("select distinct number from gis_adresses where street='" + AddrFrom.Text.Replace("'", "") + "'");


                    
                    //HouseFrom.AutoCompleteMode = AutoCompleteMode.None;
                    //HouseFrom.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    HouseFrom.AutoCompleteCustomSource.Clear();
                    HouseFrom.AutoCompleteCustomSource.AddRange(gisModule.getStreetNumbers("", AddrFrom.Text.Replace("'", "")));
/*
                    foreach (DataRow row in adresses_house.Rows)
                    {
                        HouseFrom.AutoCompleteCustomSource.Add(row[0].ToString());
                    }
*/
  //                  HouseFrom.AutoCompleteSource = AutoCompleteSource.CustomSource;
                    //HouseFrom.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    HouseFrom.Tag = AddrFrom.Text;
                }
            }
        }

        private void doFillHouseTo() {
            //return;
            if (AddrToForDriver.Text != "") {
                if (AddrToForDriver.Text != HouseTo2.Tag) {
                    HouseTo2.AutoCompleteCustomSource.Clear();
                    HouseTo2.AutoCompleteCustomSource.AddRange(gisModule.getStreetNumbers("", AddrToForDriver.Text.Replace("'", "")));
                    
                    HouseTo2.Tag = AddrToForDriver.Text;
                }
            }
        }

        private void AddrFrom_TextChanged(object sender, EventArgs e)
        {
            //doFindAddrFromName();
            if (m_bLoading == false) {
                ZoneID.SelectedIndex = -1;
                ZoneID.SelectedItem = null;

                ORDER_PATH.getFirstPoint().lat = 0;
                ORDER_PATH.getFirstPoint().lon = 0;
//                csmapcontrol.MapControl.ORDER_CALCULATED_PATH.Clear();
                csmapcontrol.MapControl.ORDER_CALCULATED_PATH.need_calculate = true;
                this.order_price_Descr.Text = "";
                this.calculator_text.Text = "";
                DistanceCity.Text = "";
                distanceMgor.Text = "";
                calc_check_1.Checked = false;
            }
        }
        
        private void HouseFrom_TextChanged(object sender, EventArgs e)
        {
            if (m_bLoading == false) {
                //ZoneID.SelectedValue = null;
                ZoneID.SelectedIndex = -1;
                ZoneID.SelectedItem = null;
//                geo_location_from = null;
                ORDER_PATH.getFirstPoint().lat = 0;
                ORDER_PATH.getFirstPoint().lon = 0;
//                csmapcontrol.MapControl.ORDER_CALCULATED_PATH.Clear();
                csmapcontrol.MapControl.ORDER_CALCULATED_PATH.need_calculate = true;
                this.order_price_Descr.Text = "";
                this.calculator_text.Text = "";
                DistanceCity.Text = "";
                distanceMgor.Text = "";
                calc_check_1.Checked = false;
                //ZoneID.SelectedValue = 0;
            }
            //doFindAddrFromName();
        }



        public void UpdateZonesByOnGeoPointChanges()
        {
            bool can_calc_distance = false;

            if (ZoneID.Enabled)
            {
                calc_check_1.Checked = false;
                gisModule.geoPoint p = new gisModule.geoPoint(ORDER_PATH.getFirstPoint().lat, ORDER_PATH.getFirstPoint().lon);
                    
                int zone_id = gisModule.getPointRaion(p);
               /* if (zone_id == 0)
                {
                    ZoneID.SelectedIndex = -1;
                    ZoneID.SelectedItem = null;
                    ZoneID.SelectedIndex = -1;
                }
                else
                */ 
                {
                    if (zone_id < 0)
                    {
                        calc_check_1.Checked = true;
                        zone_id = -zone_id;
                    }
                    if (ZoneID.SelectedValue != (object)zone_id)
                        ZoneID.SelectedValue = zone_id;
                }
            }


            if (RaionToID.Enabled)
            {

                gisModule.geoPoint p = new gisModule.geoPoint(ORDER_PATH.getLastPoint().lat, ORDER_PATH.getLastPoint().lon);
                int zone_id = gisModule.getPointRaion(p);

                if (zone_id < 0)
                {
                    calc_check_1.Checked = true;
                    zone_id = -zone_id;
                }

                if (RaionToID.SelectedValue != (object)zone_id)
                {
                    RaionToID.SelectedValue = zone_id;
                    //AddrToForDriver.Text = RaionToID.Text;
                }

            }


            if ((ORDER_PATH.getFirstPoint().lat != 0) && (ORDER_PATH.getLastPoint().lat != 0)){
                расчетМаршрута.PerformClick();
            }



        }


        //ищет, какое наименование соответствует введенному адресу.
        //
        private void doFindAddrFromName()
        {
            if (ZoneID.SelectedItem == null)
            {
                if (AddrFrom.Text != "")
                {

                    gisModule.geoPoint location_from = gisModule.getLocationByText("", AddrFrom.Text.Replace("'", ""), HouseFrom.Text.Replace("'", ""), "", false);

                    //!!!!
                    //Если маркер двигался - использовать его как основу,

                    if (ORDER_PATH.getFirstPoint().lat != 0)
                        if (ORDER_PATH.getFirstPoint().MarkerMoved == 1)
                            location_from = new gisModule.geoPoint(ORDER_PATH.getFirstPoint().lat, ORDER_PATH.getFirstPoint().lon);


                    

                    if (location_from.lat != 0){
                        ORDER_PATH.setFirstPoint(location_from.lat, location_from.lon, "");
                        //geo_location_from = location_from;

                        //double lat = (double)adresses_names.Rows[0]["lat"];
                        //double lon = (double)adresses_names.Rows[0]["lon"];

                       



                        AddrFromName.Tag = "BLOCK_CHANGE_TEXT_EVENT";
                       // if (AddrFromName.Text != adresses_names.Rows[0][0].ToString())
                       //     AddrFromName.Text = adresses_names.Rows[0][0].ToString();

                        UpdateZonesByOnGeoPointChanges();
                        
                        AddrFromName.Tag = 0;
                    } else {

                        doUpdateCalculatorText();
                        if (ZoneID.Enabled){
                            ZoneID.SelectedIndex = -1;
                            ZoneID.SelectedItem = null;
                            ZoneID.SelectedIndex = -1;
                        }
                    }
                }
            }
        }

        private void doFindAddrTo() {

            {
                if (AddrToForDriver.Text != "") {
                    gisModule.geoPoint location_to = gisModule.getLocationByText("", AddrToForDriver.Text.Replace("'", ""), HouseTo2.Text.Replace("'", ""), "", false);


                    if (ORDER_PATH.getLastPoint().MarkerMoved  == 1)
                            location_to = new gisModule.geoPoint(ORDER_PATH.getLastPoint().lat, ORDER_PATH.getLastPoint().lon);


                    

                    if (location_to.lat != 0){
                        ORDER_PATH.setLastPoint(location_to.lat, location_to.lon, "");
                        AddrToName.Tag = "BLOCK_CHANGE_TEXT_EVENT";

                        UpdateZonesByOnGeoPointChanges();
                            
                        AddrToName.Tag = 0;
                        
                    }
                    else
                        doUpdateCalculatorText();
                }
            }
        }




        private void doFindAddrFrom()
        {
            if (AddrFromName.Tag != "BLOCK_CHANGE_TEXT_EVENT")
            {
                if (AddrFromName.Text != "")
                {

                    DataTable dd = DataBase.mssqlRead("select address, housenum, zoneID from adresses " +
                     " where Name='" + AddrFromName.Text.Replace("'", "") + "'" +
                     "union select street, number, zoneID from gis_adresses  where Name='" + AddrFromName.Text.Replace("'", "") + "'");                      
                     
                     ;

                    if (dd.Rows.Count > 0)
                    {
                        AddrFrom.Tag = "BLOCK_CHANGE_TEXT_EVENT";
                        AddrFrom.Text = dd.Rows[0][0].ToString();
                        HouseFrom.Text = dd.Rows[0][1].ToString();

                        //ZoneID.SelectedValue = dd.Rows[0][2].ToString();

                        AddrFrom.Tag = 0;
                       
                    }
                }
            }
        }

        private void AddrFromName_SelectedValueChanged(object sender, EventArgs e)
        {
            if (m_bLoading == false) {
                doFindAddrFrom();
                doFindAddrFromName();
            }
        }

        private void chkPredvar_CheckStateChanged(object sender, EventArgs e)
        {
            dtPredvar.Visible = chkPredvar.Checked;
            tmPredvar.Visible = chkPredvar.Checked;

            predvShowDelta.Visible = chkPredvar.Checked;

        }

        private void btnPhoneInfo_Click(object sender, EventArgs e)
        {
            //getPhoneInfo();
            this.doLoadPhoneInfo();
        }

        private void lbPhoneInfo_Leave(object sender, EventArgs e)
        {
            lbPhoneInfo.Visible = false;
        }

        private void order_MouseDown(object sender, MouseEventArgs e)
        {
            if (lbPhoneInfo.Visible)
                lbPhoneInfo.Visible = false;
        }

        private void lbPhoneInfo_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //выбран адрес - прописать его в поля заказа
            if (lbPhoneInfo.SelectedIndex <phone_data.Rows.Count )
            {
                this.AddrFrom.Text = phone_data.Rows[lbPhoneInfo.SelectedIndex]["Address"].ToString();
                this.HouseFrom.Text = phone_data.Rows[lbPhoneInfo.SelectedIndex]["HouseNum"].ToString();
                this.FlatFrom.Text = phone_data.Rows[lbPhoneInfo.SelectedIndex]["FlatNum"].ToString();
                this.ClientID.Text = phone_data.Rows[lbPhoneInfo.SelectedIndex]["FIO"].ToString();
                this.ClientID.Tag = int.Parse(phone_data.Rows[lbPhoneInfo.SelectedIndex]["ID"].ToString());
                this.Descr.Text = phone_data.Rows[lbPhoneInfo.SelectedIndex]["Descr"].ToString();
                

            }
            
            lbPhoneInfo.Visible = false;
        }

        private void btnEndCall_Click(object sender, EventArgs e)
        {
            //if (m_pSipCaller != null)
            {
                if (MAIN_FORM.caller == null)
                    return;
                MAIN_FORM.caller.doHangup();
              //  m_pSipCaller.doHangup();
                btnEndCall.Visible = false;


                //to do сделать задержку на прием нового звонка пока открыто окно

                //m_pSipCaller.doBlockIncomingCalls();
            }
        }

        private void OrgID_SelectedIndexChanged(object sender, EventArgs e) {
            //изменилась организация - подгрузить людей этой организации.
            //вывести их списком с паролем и временем ограничения на заказ - оператор сем решит - заводить заказ или нет.

            //if (m_bLoading == false)

            this.org_prim.Text = "";
            try {
                if (OrgID.SelectedValue == null) {

                    ServiceID.SelectedValue = 27;
                    ORGDETAIL_ID.DataSource = null;
                } else {
                    if (OrgID.SelectedValue.ToString() == "System.Data.DataRowView")
                        return;


                    DataRow[] org = ((DataTable)OrgID.DataSource).Select("ID=" + OrgID.SelectedValue.ToString());

                    if (org.Length > 0) {
                       this.org_prim.Text = org[0]["descr"].ToString();
                    }

                    DataTable dt = new DataTable();
                    dt.Columns.Add("Descr");
                    dt.Columns.Add("ID");
                    dt.Columns.Add("descr2");

                    //здесь исключение - надо убрать
                    //System.Console.WriteLine("ORDID.SelectedValue = " + OrgID.SelectedValue.ToString());
                    DataRow[] select = organization_detail.Select(" ORGANIZATIONID=" + OrgID.SelectedValue.ToString());
                    foreach (DataRow row in select) {
                        DataRow newrow = dt.NewRow();
                        newrow["Descr"] = row["Descr"];
                        newrow["descr2"] = row["descr2"];
                        newrow["ID"] = row["ID"];
                        dt.Rows.Add(newrow);

                    }

                    if (m_bLoading == false) {
                        this.Descr.Text = "БЕЗНАЛ. " + OrgID.Text;
                        if (((int)OrgID.SelectedValue == 31)) {
                            ServiceID.SelectedValue = 27;
                        } else {

                         //   if (((int)OrgID.SelectedValue == 50) || ((int)OrgID.SelectedValue == 53) || ((int)OrgID.SelectedValue == 53)) {
                                ServiceID.SelectedValue = 25;
                                //this.Descr.Text += "Посадка 100р, 10р/км.";
/*                                if (((int)OrgID.SelectedValue == 50) || ((int)OrgID.SelectedValue == 53))
                                    if (this.chkPredvar.Checked)
                                        this.Descr.Text += " +50 руб за предварительный.";
 */ 
                           // }
                        }
                        

                    }

                    //m_bLoading = true;
                    ORGDETAIL_ID.Tag = "BLOCK_";
                    ORGDETAIL_ID.DataSource = dt;
                    ORGDETAIL_ID.DisplayMember = "Descr";
                    ORGDETAIL_ID.ValueMember = "ID";

                    ORGDETAIL_ID.AutoCompleteSource = AutoCompleteSource.ListItems;
                    ORGDETAIL_ID.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    //ORGDETAIL_ID.SelectedValue = null;
                    ORGDETAIL_ID.SelectedIndex = -1;

                    //m_bLoading = false;
                    ORGDETAIL_ID.Tag = "";

                    btnSumtoDriver.Visible = true;

                    doUpdateCalculatorText();

                }

            } catch (Exception eee) {
                btnSumtoDriver.Visible = false;
            }

        }


        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void chk_EndOrder_CheckedChanged(object sender, EventArgs e)
        {
            btnEndOrder.Visible = chk_EndOrder.Checked;
            comboEndState.Visible = chk_EndOrder.Checked;
            comboEndState.SelectedIndex = 0;
        }

        private void btnEndOrder_Click(object sender, EventArgs e)
        {
            int end_state = comboEndState.SelectedIndex+1;
            //if (end_state > 0)
//                end_state = 3;

            String[] fields = { "ComID", "OpID", "dtArrive", "Value1", "Value2", "State" };
            object[] values = { 4, MAIN_FORM.OperatorID, DateTime.Now, m_iID, end_state, 0 };

            DataBase.mssqlInsert("COMMANDS", fields, values);
        }

        private void OrgID_TextChanged(object sender, EventArgs e)
        {
            btnSumtoDriver.Visible = (OrgID.SelectedValue != null);
        }

        private void btnSumtoDriver_Click(object sender, EventArgs e)
        {

            int orderid = this.m_iID;
            int carid = (int) this.CarID.SelectedValue;
            int sum = 0;
            int.TryParse(summ.Text, out sum);
            if (orderid <= 0)
                return;
            if (carid <= 0)
                return;
            if (sum <= 0)
                return;
            
            DataTable ORDER_DRIVER_INFO = DataBase.mssqlRead("SELECT CARS.SERVICEID, CARS.DRIVERID from ORDERS, CARS WHERE CARS.ID=ORDERS.CARID and ORDERS.ID=" + orderid.ToString());
            if (ORDER_DRIVER_INFO.Rows.Count <= 0)
                return;

            int car_service_id = (int)ORDER_DRIVER_INFO.Rows[0]["SERVICEID"];
            int driver_id = (int)ORDER_DRIVER_INFO.Rows[0]["DRIVERID"];

            if (MessageBox.Show("положить на баланс водителю " + carid.ToString() + " сумму заказа " + sum.ToString() + " ?", "Пополнение баланса", MessageBoxButtons.YesNo) != DialogResult.Yes)
                return;




            DataTable bank_info = DataBase.mssqlRead("SELECT dtArrive from bank where DriverID=" + driver_id.ToString() + " and type=0 and descr='<ПО ЗАКАЗУ>' and orderid=" + orderid.ToString());
            if (bank_info.Rows.Count > 0)
            {
                MessageBox.Show("Ошибка. У водителя уже внесен платеж платеж по этому заказу" + bank_info.Rows[0]["dtArrive"].ToString(), "Пополнение баланса", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            /*02-12-2013 - вносится на 10% меньше + 20 рублей.*/


            String[] fields = { "driverid", "ServiceID", "OpID", "dtArrive", "sum", "type", "Descr", "orderid" };
            object[] values = { driver_id, car_service_id, MAIN_FORM.OperatorID, DateTime.Now, sum, 0, "<ПО ЗАКАЗУ>", orderid };

            DataBase.mssqlInsert("BANK", fields, values);

            MessageBox.Show("Внесен  платеж по заказу", "Пополнение баланса", MessageBoxButtons.OK, MessageBoxIcon.None);
            
            doSave();
        }

        private void ServiceID_SelectedIndexChanged(object sender, EventArgs e)
        {
            расчетМаршрута.PerformClick();
        }

        private void AddrFrom_Leave(object sender, EventArgs e)
        {
            doFindAddrFromName();
        }

        private void HouseFrom_Leave(object sender, EventArgs e)
        {
            doFindAddrFromName();
        }

        private void btnCarMessage_Click(object sender, EventArgs e)
        {
            if (CarID.SelectedValue != null)
            {
                send_mess_box smb = new send_mess_box();
                smb.CAR_IDS.Text = ' '+CarID.SelectedValue.ToString()+' ';
                smb.Show(this);
            }
        }

        private void btnCarDial_Click(object sender, EventArgs e)
        {
            if (CarID.SelectedValue != null)
            {
             
                    int carid = 0;

                    if (int.TryParse(CarID.SelectedValue.ToString(), out carid))
                    {
                        //this.m_pSipCaller = 
                            MAIN_FORM.main_form_object.doDialCar(carid);
                         //MAIN_FORM.main_form_object.doDialPhone(this.Phone.Text);
                        this.btnEndCall.Visible = true;
                    }
             
            }

        }

        private void btnPhone1Dial_Click(object sender, EventArgs e)
        {
            //this.m_pSipCaller = 
                MAIN_FORM.main_form_object.doDialPhone(this.Phone.Text);
            this.btnEndCall.Visible = true;
        }

        private void btnPhone2Dial_Click(object sender, EventArgs e)
        {
           //this.m_pSipCaller = 
            MAIN_FORM.main_form_object.doDialPhone(this.ConnectPhone.Text);

           this.btnEndCall.Visible = true;
            
        }

        private void ORGDETAIL_ID_TextChanged(object sender, EventArgs e)
        {
            

        }

        private void chkPredvar_CheckedChanged(object sender, EventArgs e)
        {
            if (m_bLoading == false)
            {
                if (OrgID.SelectedItem != null)
                {
                    ORGDETAIL_ID_TextChanged(null, null);
                }
            }
        }

        private void doUpdateCalculatorText()
        {
            try {

                //IGOR 06-03-2014 - для того чтобы всегда проверялись отдаленые районы
               // UpdateZonesByOnGeoPointChanges();

                //Очистить примечание от цены
                int pos = this.Descr.Text.IndexOf('|');
                if (pos >= 0)
                    this.Descr.Text = this.Descr.Text.Substring(0, pos);

                расчетМаршрута.Text = "";

                int iOrgID = 0;
                int iServiceID = (int)ServiceID.SelectedValue;

                if (OrgID.SelectedValue != null)
                {
                    String org_id = OrgID.SelectedValue.ToString();

                    int.TryParse(org_id, out iOrgID);
                }



                //taxi_service_tarif Тариф = taxi_service_tarif.getTarif((int)ServiceID.SelectedValue);
                DateTime dt = dtPredvar.Value.Date.Add(tmPredvar.Value.TimeOfDay);

                taxi_service_tarif Тариф = taxi_service_tarif.getDatabaseTarif(iServiceID, iOrgID, 0, dt);
/*
                if (chk_beznal.Checked) {
                    if (OrgID.SelectedValue != null) {
                        String org_id = OrgID.SelectedValue.ToString();
                        
                        int.TryParse(org_id, out iOrgID);
                        Тариф = taxi_service_tarif.getTarifForOrganization(iOrgID);
                    }


                }
 */ 

                sms_text = "";
                double distanceCity = 0;
                double distanceMgor0 = 0;
                total_money = 0;
                if (DistanceCity.Text == "")
                    DistanceCity.Text = "0";
                DistanceCity.Text = DistanceCity.Text.Replace('.', ',');
                double.TryParse(DistanceCity.Text, out distanceCity);

                if (distanceMgor.Text == "")
                    distanceMgor.Text = "0";
                distanceMgor.Text = distanceMgor.Text.Replace('.', ',');
                double.TryParse(distanceMgor.Text, out distanceMgor0);

                double _wait_time = 0;
                wait_time.Text = wait_time.Text.Replace('.', ',');
                double.TryParse(wait_time.Text, out _wait_time);



                //расчет доп услуг

                String uslugi_text = "";

                int add_money = 0;
                if (calc_check_1.Checked) {
                    int price = Тариф.ПолучитьСтоимостьУслуги("Отдаленный район");
                    if (price != 0) {
                        add_money += price;
                        uslugi_text += " Отдаленный район";
                    }
                }
                if (calc_check_2.Checked) {
                    int price = Тариф.ПолучитьСтоимостьУслуги("Багаж");
                    if (price != 0) {
                        add_money += price;
                        uslugi_text += " Багаж";
                    }
                }
                if (calc_check_3.Checked) {
                    int price = Тариф.ПолучитьСтоимостьУслуги("Животное");
                    if (price != 0) {
                        add_money += price;
                        uslugi_text += " Животное";
                    }
                }
                if (calc_check_4.Checked) {
                    int price = Тариф.ПолучитьСтоимостьУслуги("Грунтовая дорога");
                    if (price != 0) {
                        add_money += price;
                        uslugi_text += " Грунтовая дорога";
                    }
                }


                if (this.calc_check_black_list.Checked) {

                    int price = 0;
                    double d_price = 0;
                    double.TryParse(black_list_money.Text, out d_price);
                    price = (int)d_price;
                    if (price != 0) {
                        add_money += price;
                        uslugi_text += " ДОЛГ для " + black_list_car_id.Text + " " + price.ToString() + " руб.";
                    }
                }

                if (chkPredvar.Checked) {
                    int price = Тариф.ПолучитьСтоимостьУслуги("Предварительный безнал");
                    if (price != 0) {
                        add_money += price;
                        uslugi_text += " Предварительный безнал";
                    }
                }




                if (_wait_time > 0) {
                    uslugi_text += " Ожидание " + wait_time.Text + " мин";
                    add_money += (int)(0.5 + Тариф.Ожидание * _wait_time);
                }

                double skid = 0;
                if (abonent_skid_percent != "") {
                    double.TryParse(abonent_skid_percent, out skid);
                    if (skid > 0) {
                        uslugi_text += " СКИДКА " + skid.ToString() + " %";
                    }
                }



                //Округлить километры 
                distanceCity = (double)(int)(distanceCity + 0.5);

                String inter_points = "";
                if (this.Inerpoint1_address.Text != "") {
                    inter_points += " через " + Inerpoint1_address.Text + " " + this.Inerpoint1_house.Text;
                }

                if (this.Inerpoint2_address.Text != "") {
                    inter_points += " через " + Inerpoint2_address.Text + " " + this.Inerpoint2_house.Text;
                }
                if (this.Inerpoint3_address.Text != "") {
                    inter_points += " через " + Inerpoint3_address.Text + " " + this.Inerpoint3_house.Text;
                }
                if (this.Inerpoint4_address.Text != "") {
                    inter_points += " через " + Inerpoint4_address.Text + " " + this.Inerpoint4_house.Text;
                }


                DateTime order_time = DateTime.Now;

                if (chkPredvar.Checked)
                    order_time = dtPredvar.Value.Date.Add(tmPredvar.Value.TimeOfDay);


                //Convert.ToInt32(chkPredvar.Checked), dtPredvar.Value.Date.Add(tmPredvar.Value.TimeOfDay)

                //если расстояние не рассчитано - вывести цену посадки
                if (distanceCity+distanceMgor0 == 0) {
                    this.Descr.Text = this.Descr.Text;
                    this.order_price_Descr.Text = "Посадка " + Тариф.Посадка.ToString();

                    if (order_time > DateTime.Parse("2013-12-31 18:00"))
                        if (order_time < DateTime.Parse("2014-01-02 8:00")) {
                            this.order_price_Descr.Text = "Посадка " + (Тариф.Посадка*2).ToString();
                        }


                    if (order_10percent.Checked) {
                        this.order_price_Descr.Text += " МЕЖГОРОД оплата 10%";
                    }

                    this.order_price_Descr.Text += inter_points;

                    if (uslugi_text != "")
                        this.order_price_Descr.Text += " дополнительно " + uslugi_text;
                    return;
                }


                расчетМаршрута.Text = DistanceCity.Text + " км";
                if (distanceMgor.Text != "") 
                if (double.Parse(distanceMgor.Text)>0)
                {
                    расчетМаршрута.Text += " + "+distanceMgor.Text + " мгор";
                }

                //Считать по тарифу
                total_money = Тариф.СосчитатьСтоимостьПути(distanceCity, distanceMgor0);




                total_money += add_money;

                //тут указать предварительное время если предварительный
                if (order_time > DateTime.Parse("2013-12-31 18:00"))
                    if (order_time < DateTime.Parse("2014-01-02 8:00")) {
                        total_money *= 2;
                    }


                if (skid > 0) {
                    total_money = (int)(total_money * ((100 - skid) / 100.0));
                }


                total_money = Тариф.ОкруглитьДо10Руб(total_money);
                String summ_text = "";

               /* if (this.Inerpoint1_address.Text != "") {
                    summ_text += " через " + Inerpoint1_address.Text + " " + this.Inerpoint1_house.Text;
                }

                if (this.Inerpoint2_address.Text != "") {
                    summ_text += " через " + Inerpoint2_address.Text + " " + this.Inerpoint2_house.Text;
                }
                if (this.Inerpoint3_address.Text != "") {
                    summ_text += " через " + Inerpoint3_address.Text + " " + this.Inerpoint3_house.Text;
                }
                if (this.Inerpoint4_address.Text != "") {
                    summ_text += " через " + Inerpoint4_address.Text + " " + this.Inerpoint4_house.Text;
                }
                */

                if (order_10percent.Checked) {
                    summ_text += " МЕЖГОРОД оплата 10% ";
                }
                summ_text += inter_points + " Цена " + total_money.ToString() + "р";//, " + расчетМаршрута.Text;
                if (uslugi_text != "")
                    summ_text += ", включая: " + uslugi_text;

                //summ_text = summ_text;

                
                //total_money = (int)(5.0 * ((int)((0.49999 + total_money / 5.0))));

                //расчетМаршрута.Tag = ((Double)total_money );

                //DistanceMoney.Text = ((Double)DistanceMoney.Tag).ToString("0.") + " руб";

                int pos1 = this.Descr.Text.IndexOf('|');
                if (pos1 >= 0)
                    this.Descr.Text = this.Descr.Text.Substring(0, pos1);

                this.Descr.Text = this.Descr.Text;
                this.order_price_Descr.Text = summ_text;

                calculator_text.Text = "Цена поездки " + total_money.ToString() + " руб за " + (distanceCity+distanceMgor0).ToString() + " км без изменения маршрута";


                if (uslugi_text != "")
                    calculator_text.Text += "\r\nС учетом услуг: " + uslugi_text;

                //if ((int)ServiceID.SelectedValue != 27)
                //calculator_text.Text += "\r\nВодитель меняет цену только при изменении маршрута, ожидании или разнице в пути более 2 км";
                //calculator_text.Text += "\r\nСлужба качества: 420010";

                //sms_text = "Цена поездки " + total_money.ToString() + " р. за " + distanceCity.ToString() + "км"; //26
                sms_text = calculator_text.Text;// += ". Водитель может изменить цену по факту";

                //System.Console.WriteLine(sms_text);
            } catch (Exception e) {
                
                MessageBox.Show(e.StackTrace + " " + e.Message);
            }




        }

        public void doCalculatePath()
        {
//            if (this.m_iID > 0)
//                return;
            try {

                this.расчетМаршрута.Text = "расчет...";
                /*
                if (this.AddrFrom.Text == "")
                {
                    MessageBox.Show("Не указан адрес откуда");
                    return;
                }
                if (this.AddrTo.Text == "")
                {
                    MessageBox.Show("Не указан адрес куда");
                    return;
                }
                */

                String str1 = this.AddrFrom.Text, str2 = this.AddrTo.Text;
                gisModule.geoPoint location_from = new gisModule.geoPoint(ORDER_PATH.getFirstPoint().lat, ORDER_PATH.getFirstPoint().lon);
                if (ORDER_PATH.getFirstPoint().lat == 0) {
                    location_from = gisModule.getLocationByText("", AddrFrom.Text.Replace("'", ""), HouseFrom.Text.Replace("'", ""), "", false);
                    ORDER_PATH.setFirstPoint(location_from.lat, location_from.lon, "");
                    //location_from = geo_location_from;
                }

                /*
                if ((this.geo_location_from.lat == 0) || (this.geo_location_from == null))
                {
                    location_from = gisModule.getLocationByText("", AddrFrom.Text.Replace("'", ""), HouseFrom.Text.Replace("'", ""), "", false);
                    geo_location_from = location_from;
                }
                */

                //if (geo_location_to == null)
                // location_to  = new 
                //gisModule.getLocationByText("", AddrToForDriver.Text.Replace("'", ""), HouseTo2.Text.Replace("'", ""), "", false);

                gisModule.geoPoint location_to = new gisModule.geoPoint(ORDER_PATH.getLastPoint().lat, ORDER_PATH.getLastPoint().lon);

                if ((this.ORDER_PATH.getLastPoint().lat == 0)) {
                    location_to = gisModule.getLocationByText("", AddrToForDriver.Text.Replace("'", ""), HouseTo2.Text.Replace("'", ""), "", false);
                    ORDER_PATH.setLastPoint(location_to.lat, location_to.lon, "");
                }



                // String coord1 = MAIN_FORM.doYandexSearchAddress(str1, this.HouseFrom.Text);
                //  String coord2 = MAIN_FORM.doYandexSearchAddress(str2, this.HouseTo2.Text);

                //  coord1 = coord1.Replace('.', ',');
                //  coord2 = coord2.Replace('.', ',');

                //РАССЧЕТ МАРШРУТА
                /* 1. Статически задать точки MapControlPath
                 * 2. статически рассчитаь маршрут
                 * 3. 
                 */

                /* тариф: 
                 * price0: посадка 90 - 120
                 * price1: цена километра в городе 10
                 * price2:  цена километра за городом   15
                 * price3:  цена минуты ожидания    4
                 * 
                 * price0 + price1*dist1 + price2 * dist2 + price3 * delay_seconds/60 
                 * 
                 * 90 + 10*12.48 + 15*0 + 4 * 147/60 = 214.8 + 9.8 = 224.6
                 * 
                 * 221 > 220
                 * 223 > 225
                 * 227 > 225
                 * 228 > 230
                 * 
                 * 
                 *  фиксированная стоимость поездки (посадка есть, остальное по нулям).
                 * 
                 * 
                 */


                if (this.Inerpoint1_address.Text != "") {
                    if (ORDER_PATH.InterMarkers[1].lat == 0) {

                    }

                }

                if (this.Inerpoint2_address.Text != "") {
                    if (ORDER_PATH.InterMarkers[2].lat == 0) {

                    }
                }

                if (this.Inerpoint3_address.Text != "") {
                    if (ORDER_PATH.InterMarkers[3].lat == 0) {

                    }
                }

                if (this.Inerpoint4_address.Text != "") {
                    if (ORDER_PATH.InterMarkers[4].lat == 0) {

                    }
                }




                if (location_from.lat != 0) {
                    if (location_to.lat != 0) {
                        csmapcontrol.MapPath calculated_path = csmapcontrol.MapControl.ORDER_CALCULATED_PATH;
                        /*Double lat1, lon1, lat2, lon2;
                        lon1 = Double.Parse(coord1.Split(' ')[0]);
                        lat1 = Double.Parse(coord1.Split(' ')[1]);

                        lon2 = Double.Parse(coord2.Split(' ')[0]);
                        lat2 = Double.Parse(coord2.Split(' ')[1]);
    */


                        for (int i = 0; i < ORDER_PATH.InterMarkers.Count; i++) {
                            csmapcontrol.MapControl.ORDER_CALCULATED_PATH.setPoint(i, ORDER_PATH.InterMarkers[i].lat, ORDER_PATH.InterMarkers[i].lon, "");
                        }

                        // calculated_path.setFirstPoint(ORDER_PATH.getLastPoint().lat, ORDER_PATH.getLastPoint().lon, AddrFrom.Text + " " + HouseFrom.Text);
                        // calculated_path.setLastPoint(ORDER_PATH.getFirstPoint().lat, ORDER_PATH.getFirstPoint().lon, AddrToForDriver.Text + " " + HouseTo2.Text);
                        city_distance = 0;
                        mgor_distance = 0;
                        calculated_path.calculatePath(200);

                        

                        csmapcontrol.MapPath.PathPoint prevPoint = null;
                        for (int i=0;i< calculated_path.Points.Count; i++){
                            csmapcontrol.MapPath.PathPoint p = calculated_path.Points[i];
                            if (prevPoint == null) {
                                prevPoint = p;
                                continue;
                            }

                            gisModule.geoPoint gp = new gisModule.geoPoint();
                            gp.lat =  p.Lat;
                            gp.lon =  p.Lon;
                            if (gisModule.PointInZonePolygon(gp,  gisModule.ZonePolygon.CITY_zone_polygon)){
                                city_distance += gisModule.Distance(prevPoint.Lat, prevPoint.Lon, p.Lat, p.Lon);
                            }
                            else
                            {
                                mgor_distance += gisModule.Distance(prevPoint.Lat, prevPoint.Lon, p.Lat, p.Lon);
                            }


                            
                            prevPoint = p;
                        }


                        //тут же можно посчитать сколько по городу и по межгороду
                        /*
                         *             city_distance = 0;
            mgor_distance = 0;

                         */

                        // MAIN_FORM.main_form_object.doShowPath(calculated_path);

                        //расчетМаршрута.Tag = ((Double) calculated_path.path_distance);

                        //this.calculator_add_money = dop_money_elements.CheckedItems.Count * 20;

                        //path_distance = calculated_path.path_distance;

                        //if (city_distance> 0.1)
                            //city_distance += 0.4;
                        //DistanceCity.Text = ((Double)calculated_path.path_distance).ToString(".0");
                        DistanceCity.Text = ((Double)city_distance).ToString(".0");
                        distanceMgor.Text = ((Double)mgor_distance).ToString(".0");
                        




                        //02-06-2014 doUpdateCalculatorText();




                    }
                }
                //02-06-2014
                doUpdateCalculatorText();
                if ((location_from.lat == 0)) {
                    расчетМаршрута.Text = "Ошибка..";
                    расчетМаршрута.Tag = (Double)0;
                    DistanceMoney.Tag = ((Double)0.0);
                    DistanceMoney.Text = "Ошибка";
                    doUpdateCalculatorText();
                    calculator_text.Text = "Не определились координаты точки посадки. \r\nДля расчета необходимо указать точку посадки на карте";
                }

                if ((location_to.lat == 0)) {
                    расчетМаршрута.Text = "Ошибка..";
                    расчетМаршрута.Tag = (Double)0;
                    DistanceMoney.Tag = ((Double)0.0);
                    DistanceMoney.Text = "Ошибка";
                    doUpdateCalculatorText();
                    calculator_text.Text = "Не определились координаты точки окончания поездки. \r\nУкажите точку окончания поездки на карте";
                }

                if (this.Inerpoint1_address.Text != "") {
                    if (ORDER_PATH.InterMarkers[1].lat == 0) {
                        DistanceCity.Text = "";
                        distanceMgor.Text = "";
                        расчетМаршрута.Text = "Ошибка..";
                        расчетМаршрута.Tag = (Double)0;
                        DistanceMoney.Tag = ((Double)0.0);
                        DistanceMoney.Text = "Ошибка";
                        doUpdateCalculatorText();
                        calculator_text.Text = "Не определились координаты " + Inerpoint1_address.Text + " " + Inerpoint1_house.Text + ". \r\nУкажите точку на карте";
                    }

                }

                if (this.Inerpoint2_address.Text != "") {
                    if (ORDER_PATH.InterMarkers[2].lat == 0) {
                        DistanceCity.Text = "";
                        distanceMgor.Text = "";
                        расчетМаршрута.Text = "Ошибка..";
                        расчетМаршрута.Tag = (Double)0;
                        DistanceMoney.Tag = ((Double)0.0);
                        DistanceMoney.Text = "Ошибка";
                        doUpdateCalculatorText();
                        calculator_text.Text = "Не определились координаты " + Inerpoint2_address.Text + " " + Inerpoint2_house.Text + ". \r\nУкажите точку на карте";
                    }
                }

                if (this.Inerpoint3_address.Text != "") {
                    if (ORDER_PATH.InterMarkers[3].lat == 0) {
                        DistanceCity.Text = "";
                        distanceMgor.Text = "";
                        расчетМаршрута.Text = "Ошибка..";
                        расчетМаршрута.Tag = (Double)0;
                        DistanceMoney.Tag = ((Double)0.0);
                        DistanceMoney.Text = "Ошибка";
                        doUpdateCalculatorText();
                        calculator_text.Text = "Не определились координаты " + Inerpoint3_address.Text + " " + Inerpoint3_house.Text + ". \r\nУкажите точку на карте";
                    }
                }

                if (this.Inerpoint4_address.Text != "") {
                    if (ORDER_PATH.InterMarkers[4].lat == 0) {
                        DistanceCity.Text = "";
                        distanceMgor.Text = "";
                        расчетМаршрута.Text = "Ошибка..";
                        расчетМаршрута.Tag = (Double)0;
                        DistanceMoney.Tag = ((Double)0.0);
                        DistanceMoney.Text = "Ошибка";
                        doUpdateCalculatorText();
                        calculator_text.Text = "Не определились координаты " + Inerpoint4_address.Text + " " + Inerpoint4_house.Text + ". \r\nУкажите точку на карте";
                    }
                }

            } catch (Exception e) {
                MessageBox.Show(e.StackTrace +" "+ e.Message);
            }
            
            
        }

        private void расчетМаршрута_Click(object sender, EventArgs e) {

            doCalculatePath();


        }

        private void timer1_Tick(object sender, EventArgs e) {
            timer1.Enabled = false;
            if ((Phone.Text.Length < 6) || (Phone.Text == "anonymous")) 
            {
                Phone.BackColor = Color.Red;
            }

        }

        private void lbPhoneInfo_SelectedIndexChanged(object sender, EventArgs e) {

        }

        private void AddrTo_Leave(object sender, EventArgs e) {
            doFindAddrTo();
        }

        private void HouseTo_Leave(object sender, EventArgs e) {
            doFindAddrTo();
        }

        private void label24_Click(object sender, EventArgs e)
        {

        }

        private void HouseTo_TextChanged(object sender, EventArgs e)
        {

        }

        private void AddrToForDriver_Leave(object sender, EventArgs e)
        {
            doFindAddrTo();
        }


        private void AddrToForDriver_TextChanged(object sender, EventArgs e)
        {
            if (m_bLoading == false)
            {
                RaionToID.SelectedValue = 0;
            }
            ORDER_PATH.setLastPoint(0, 0, "");
            ORDER_PATH.getLastPoint().MarkerMoved = 0;
            //csmapcontrol.MapControl.ORDER_CALCULATED_PATH.Clear();
            csmapcontrol.MapControl.ORDER_CALCULATED_PATH.need_calculate = true;


            this.order_price_Descr.Text = "";
            this.calculator_text.Text = "";
            DistanceCity.Text = "";
            distanceMgor.Text = "";
            calc_check_1.Checked = false;

        }


        private void ZoneID_SelectedIndexChanged(object sender, EventArgs e) {
            ///попробую сделать чтобы подставлялся мигающий автомобиль из района
            ///1. Если район выбран - запрос машин из района
            ///2. если район пустой - убрать машину
            ///
/*
            object value = ((ComboBox)sender).SelectedValue;
            if (value != null)
                System.Console.WriteLine("ZoneID.selectedValue = " + value.ToString());
            else {
                System.Console.WriteLine("ZoneID.selectedValue = null");
                CarID.SelectedItem = null;
                //CarID.SelectedIndex = -1;
                //ZoneID.SelectedItem = null;
            }


            if (value != null) {
                if (MAIN_FORM.main_form_object.first_cars.ContainsValue(value.ToString())) {
                    foreach (object key in MAIN_FORM.main_form_object.first_cars.Keys) {

                        if (MAIN_FORM.main_form_object.first_cars[key].ToString().CompareTo(value.ToString()) == 0) {
                            this.CarID.SelectedValue = key;
                        }
                    }

                } else {
                    CarID.SelectedItem = null;
                }
            }
*/

        }

        private void chk_beznal_CheckedChanged(object sender, EventArgs e)
        {
            group_beznal.Visible = chk_beznal.Checked;
            

        }

        private void chk_calculator_CheckedChanged(object sender, EventArgs e)
        {
            group_calculator.Visible = chk_calculator.Checked;
        }

        private void group_calculator_VisibleChanged(object sender, EventArgs e)
        {
            int h = this.group_header.Height + this.group_footer.Height +groupBox1.Height + 35;
            if (group_calculator.Visible)
                h += group_calculator.Height + 5;
            if (group_beznal.Visible)
                h += group_beznal.Height + 5;
            if (this.group_order_history_info.Visible)
                h += group_order_history_info.Height + 5;
            this.Height = h;
        }

        private void group_beznal_VisibleChanged(object sender, EventArgs e)
        {
            int h = this.group_header.Height + this.group_footer.Height + groupBox1.Height + 35;
            if (group_calculator.Visible)
                h += group_calculator.Height + 5;
            if (group_beznal.Visible)
                h += group_beznal.Height + 5;
            if (this.group_order_history_info.Visible)
                h += group_order_history_info.Height + 5;
            this.Height = h;
        }

        private void group_order_history_info_VisibleChanged(object sender, EventArgs e)
        {
            int h = this.group_header.Height + this.group_footer.Height + groupBox1.Height + 35;
            if (group_calculator.Visible)
                h += group_calculator.Height + 5;
            if (group_beznal.Visible)
                h += group_beznal.Height + 5;
            if (this.group_order_history_info.Visible)
                h += group_order_history_info.Height + 5;
            this.Height = h;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            gisModule.showAddressIn2GIS(AddrFrom.Text, HouseFrom.Text);
        }

        private void AddrTo_Click(object sender, EventArgs e)
        {
            doFillAddrFrom();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            gisModule.showAddressIn2GIS(AddrToForDriver.Text, HouseTo2.Text);
        }

        private void dop_money_elements_ItemCheck(object sender, ItemCheckEventArgs e)
        {
           
            
        }

        private void dop_money_elements_Click(object sender, EventArgs e)
        {
         
        }

        private void dop_money_elements_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            String id = CarID.SelectedValue.ToString();
            int _id = 0;
            int.TryParse(id, out _id);
            if (_id > 0){
                DataTable dt = DataBase.mssqlRead("SELECT ID, lat, lon from cars where id = " + _id.ToString());
                if (dt.Rows.Count > 0)
                {
                    int ilat = (int)dt.Rows[0]["lat"];
                    int ilon = (int)dt.Rows[0]["lon"];
                    double lat = (double)ilat / 1000000.0;
                    double lon = (double)ilon / 1000000.0;


                gisModule.showPointIn2GIS(new gisModule.geoPoint(lat, lon), CarID.Text);
                }
            }

            
        }

        private void order_big_Activated(object sender, EventArgs e)
        {
            MAIN_FORM.active_order = this;
        }

        private void order_big_Deactivate(object sender, EventArgs e)
        {
            
        }

        private void order_big_VisibleChanged(object sender, EventArgs e)
        {

        }

        private void order_big_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (MAIN_FORM.active_order == this)
            {
                MAIN_FORM.active_order = null;
            }
        }

        private void AddrTo_TextChanged(object sender, EventArgs e) {
            //geo_location_to = null;
            ORDER_PATH.setLastPoint(0, 0, "");
            ORDER_PATH.getLastPoint().MarkerMoved = 0;
//            csmapcontrol.MapControl.ORDER_CALCULATED_PATH.Clear();
            csmapcontrol.MapControl.ORDER_CALCULATED_PATH.need_calculate = true;
            this.order_price_Descr.Text = "";
            this.calculator_text.Text = "";
            DistanceCity.Text = "";
            distanceMgor.Text = "";
            calc_check_1.Checked = false;
        }

        private void HouseTo2_TextChanged(object sender, EventArgs e) {
            //geo_location_to = null;
            ORDER_PATH.setLastPoint(0, 0, "");
            ORDER_PATH.getLastPoint().MarkerMoved = 0;
            //csmapcontrol.MapControl.ORDER_CALCULATED_PATH.Clear();
            csmapcontrol.MapControl.ORDER_CALCULATED_PATH.need_calculate = true;
            this.order_price_Descr.Text = "";
            this.calculator_text.Text = "";
            DistanceCity.Text = "";
            distanceMgor.Text = "";
            calc_check_1.Checked = false;
        }

        private void HouseTo2_Leave(object sender, EventArgs e) {
            doFindAddrTo();
        }

        private void HouseTo2_Enter(object sender, EventArgs e) {
            doFillHouseTo();
        }

        private void group_header_Enter(object sender, EventArgs e) {

        }

        private void DistanceCity_TextChanged(object sender, EventArgs e) {
            return;
            doUpdateCalculatorText();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e) {
            doUpdateCalculatorText();
        }

        private void calc_check_2_CheckedChanged(object sender, EventArgs e) {
            doUpdateCalculatorText();
        }

        private void calc_check_3_CheckedChanged(object sender, EventArgs e) {
            doUpdateCalculatorText();
        }

        private void calc_check_4_CheckedChanged(object sender, EventArgs e) {
            doUpdateCalculatorText();
        }

        private void btn_fix_from_Click(object sender, EventArgs e)
        {
            //записать в базу адресов адрес "откуда", используя координату с карты
            //1. проверить такой текст, если есть - просто обновить координату
            //2. если нету - вставить запись

//            if ((MAIN_FORM.OperatorID != 129) &&  (MAIN_FORM.OperatorID != 123))
//                return;
            

            if (this.ORDER_PATH.getFirstPoint().lat == 0)
                return;

            int addr_id = DataBase.mssqlReadInt("select id from gis_adresses where street='"+AddrFrom.Text+"' and number='"+HouseFrom.Text+"'");
            if (addr_id > 0){
                
                String[] fields1 = { "lat", "lon", "state" };
                object[] values1 = { ORDER_PATH.getFirstPoint().lat, ORDER_PATH.getFirstPoint().lon, 2 };

                DataBase.mssqlUpdate("GIS_ADRESSES", fields1, values1, "ID", addr_id);
            }
            else{
                String[] fields = {"city", "name", "street", "number", "lat", "lon", "state" };
                object[] values = { "Ярославль", this.AddrFromName.Text, AddrFrom.Text, HouseFrom.Text, ORDER_PATH.getFirstPoint().lat, ORDER_PATH.getFirstPoint().lon, 1 };
                DataBase.mssqlInsert("GIS_ADRESSES", fields, values);
            }


        }

        private void btn_fix_to_Click(object sender, EventArgs e)
        {
            //записать в базу адресов адрес "откуда", используя координату с карты
            //1. проверить такой текст, если есть - просто обновить координату
            //2. если нету - вставить запись

//            if ((MAIN_FORM.OperatorID != 129) && (MAIN_FORM.OperatorID != 123))
//                return;

            //if (geo_location_to == null)
                //return;

            if (this.ORDER_PATH.getLastPoint().lat == 0)
                return;

            int addr_id = DataBase.mssqlReadInt("select id from gis_adresses where street='" + this.AddrToForDriver.Text + "' and number='" + HouseTo2.Text + "'");
            if (addr_id > 0)
            {

                String[] fields1 = { "lat", "lon", "state" };
                object[] values1 = { ORDER_PATH.getLastPoint().lat, ORDER_PATH.getLastPoint().lon, 2 };

                DataBase.mssqlUpdate("GIS_ADRESSES", fields1, values1, "ID", addr_id);
            }
            else
            {
                String[] fields = { "city", "name", "street", "number", "lat", "lon", "state" };
                object[] values = { "Ярославль", this.AddrToName.Text, AddrToForDriver.Text, HouseTo2.Text, ORDER_PATH.getLastPoint().lat, ORDER_PATH.getLastPoint().lon, 1 };
                DataBase.mssqlInsert("GIS_ADRESSES", fields, values);
            }

        }

        private void order_big_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                btnEndCall.PerformClick();
                btnSave.PerformClick();// имитируем нажатие button1
            }

            if (e.KeyCode == Keys.F4)
            {
                btnEndCall.PerformClick();
            }

            if (e.KeyCode == Keys.F5)
            {
                //btnEndCall.PerformClick();
                //btnSave.PerformClick();// имитируем нажатие button1
                расчетМаршрута.PerformClick();
            }
        }


  

  

        private void AddrToName_SelectedValueChanged(object sender, EventArgs e)
        {
            if (m_bLoading == false)
            {
//                doFindAddrToName();
                doFindAddrToName();
                doFindAddrTo();
                
            }

        }

        private void doFindAddrToName()
        {
          
            if (AddrToName.Tag != "BLOCK_CHANGE_TEXT_EVENT")
            {
                if (AddrToName.Text != "")
                {

                    DataTable dd = DataBase.mssqlRead("select address, housenum, zoneID from adresses " +
                     " where Name='" + AddrToName.Text.Replace("'", "") + "'" +
                     "union select street, number, zoneID from gis_adresses  where Name='" + AddrToName.Text.Replace("'", "") + "'");                      


                    

                    if (dd.Rows.Count > 0)
                    {
                        AddrToForDriver.Tag = "BLOCK_CHANGE_TEXT_EVENT";
                        AddrToForDriver.Text = dd.Rows[0][0].ToString();
                        HouseTo2.Text = dd.Rows[0][1].ToString();

                        RaionToID.SelectedValue = dd.Rows[0][2].ToString();

                        AddrToName.Tag = 0;

                    }
                }
            }
        }

        private void Inerpoint1_address_TextChanged(object sender, EventArgs e)
        {
            
            ORDER_PATH.setPoint(1, 0, 0, "");
            ORDER_PATH.getInterPoint(1).MarkerMoved = 0;
            //csmapcontrol.MapControl.ORDER_CALCULATED_PATH.Clear();
            csmapcontrol.MapControl.ORDER_CALCULATED_PATH.need_calculate = true;
            this.order_price_Descr.Text = "";
            this.calculator_text.Text = "";
            DistanceCity.Text = "";
            distanceMgor.Text = "";
//            calc_check_1.Checked = false;
        }

        private void doFindInterPoint1GeoLocation()
        {
                if (this.Inerpoint1_address.Text != "")
                {
                    gisModule.geoPoint location_interpoint1 = gisModule.getLocationByText("", Inerpoint1_address.Text.Replace("'", ""), Inerpoint1_house.Text.Replace("'", ""), "", false);
                    if (ORDER_PATH.getInterPoint(1).MarkerMoved == 1)
                        location_interpoint1 = new gisModule.geoPoint(ORDER_PATH.getInterPoint(1).lat, ORDER_PATH.getInterPoint(1).lon);
                    //if (location_interpoint1.lat != 0)
                    {
                        ORDER_PATH.setPoint(1, location_interpoint1.lat, location_interpoint1.lon, "");
                    }
                    doCalculatePath();
                }
            
        }

        private void doFindInterPoint2GeoLocation()
        {
            if (this.Inerpoint2_address.Text != "")
            {
                gisModule.geoPoint location_interpoint2 = gisModule.getLocationByText("", Inerpoint2_address.Text.Replace("'", ""), Inerpoint2_house.Text.Replace("'", ""), "", false);
                if (ORDER_PATH.getInterPoint(2).MarkerMoved == 1)
                    location_interpoint2 = new gisModule.geoPoint(ORDER_PATH.getInterPoint(2).lat, ORDER_PATH.getInterPoint(2).lon);
               // if (location_interpoint2.lat != 0)
                {
                    ORDER_PATH.setPoint(2, location_interpoint2.lat, location_interpoint2.lon, "");
                }
                doCalculatePath();
            }

        }

        private void doFindInterPoint3GeoLocation() {
            if (this.Inerpoint3_address.Text != "") {
                gisModule.geoPoint location_interpoint3 = gisModule.getLocationByText("", Inerpoint3_address.Text.Replace("'", ""), Inerpoint3_house.Text.Replace("'", ""), "", false);
                if (ORDER_PATH.getInterPoint(3).MarkerMoved == 1)
                    location_interpoint3 = new gisModule.geoPoint(ORDER_PATH.getInterPoint(3).lat, ORDER_PATH.getInterPoint(3).lon);
                // if (location_interpoint2.lat != 0)
                {
                    ORDER_PATH.setPoint(3, location_interpoint3.lat, location_interpoint3.lon, "");
                }
                doCalculatePath();
            }

        }

        private void doFindInterPoint4GeoLocation() {
            if (this.Inerpoint4_address.Text != "") {
                gisModule.geoPoint location_interpoint4 = gisModule.getLocationByText("", Inerpoint4_address.Text.Replace("'", ""), Inerpoint4_house.Text.Replace("'", ""), "", false);
                if (ORDER_PATH.getInterPoint(4).MarkerMoved == 1)
                    location_interpoint4 = new gisModule.geoPoint(ORDER_PATH.getInterPoint(4).lat, ORDER_PATH.getInterPoint(4).lon);
                // if (location_interpoint2.lat != 0)
                {
                    ORDER_PATH.setPoint(4, location_interpoint4.lat, location_interpoint4.lon, "");
                }
                doCalculatePath();
            }

        }

        private void Inerpoint1_address_Leave(object sender, EventArgs e)
        {
            doFindInterPoint1GeoLocation();
        }

        private void Inerpoint1_house_TextChanged(object sender, EventArgs e)
        {
            ORDER_PATH.setPoint(1, 0, 0, "");
            ORDER_PATH.getInterPoint(1).MarkerMoved = 0;
            //csmapcontrol.MapControl.ORDER_CALCULATED_PATH.Clear();
            csmapcontrol.MapControl.ORDER_CALCULATED_PATH.need_calculate = true;
            this.order_price_Descr.Text = "";
            this.calculator_text.Text = "";
            DistanceCity.Text = "";
            distanceMgor.Text = "";
            //calc_check_1.Checked = false;
        }

        private void Inerpoint1_house_Leave(object sender, EventArgs e)
        {
            doFindInterPoint1GeoLocation();
        }

        private void Inerpoint1_name_SelectedValueChanged(object sender, EventArgs e)
        {
            if (Inerpoint1_name.Text != "")
            {

                DataTable dd = DataBase.mssqlRead("select address, housenum, zoneID from adresses " +
                    " where Name='" + Inerpoint1_name.Text.Replace("'", "") + "'" +
                    "union select street, number, zoneID from gis_adresses  where Name='" + Inerpoint1_name.Text.Replace("'", "") + "'");  


              //  DataTable dd = DataBase.mssqlRead("select address, housenum, zoneID from adresses " +
               //  " where Name='" + Inerpoint1_name.Text.Replace("'", "") + "'");

                if (dd.Rows.Count > 0)
                {
                    
                    Inerpoint1_address.Text = dd.Rows[0][0].ToString();
                    Inerpoint1_house.Text = dd.Rows[0][1].ToString();

                    doFindInterPoint1GeoLocation();

                    

                }
            }
        }

        private void Inerpoint2_name_SelectedValueChanged(object sender, EventArgs e)
        {
            if (Inerpoint2_name.Text != "")
            {

                DataTable dd = DataBase.mssqlRead("select address, housenum, zoneID from adresses " +
                                    " where Name='" + Inerpoint2_name.Text.Replace("'", "") + "'" +
                                    "union select street, number, zoneID from gis_adresses  where Name='" + Inerpoint2_name.Text.Replace("'", "") + "'");  

                if (dd.Rows.Count > 0)
                {

                    Inerpoint2_address.Text = dd.Rows[0][0].ToString();
                    Inerpoint2_house.Text = dd.Rows[0][1].ToString();

                    doFindInterPoint2GeoLocation();



                }
            }
        }

        private void Inerpoint1_house_Enter(object sender, EventArgs e)
        {
            if (Inerpoint1_address.Text != "")
            {
                if (Inerpoint1_address.Text != Inerpoint1_house.Tag)
                {
                    Inerpoint1_house.AutoCompleteCustomSource.Clear();
                    Inerpoint1_house.AutoCompleteCustomSource.AddRange(gisModule.getStreetNumbers("", Inerpoint1_address.Text.Replace("'", "")));

                    Inerpoint1_house.Tag = Inerpoint1_address.Text;
                }
            }
        }

        private void Inerpoint2_address_TextChanged(object sender, EventArgs e)
        {
            ORDER_PATH.setPoint(2, 0, 0, "");
            ORDER_PATH.getInterPoint(2).MarkerMoved = 0;
            //csmapcontrol.MapControl.ORDER_CALCULATED_PATH.Clear();
            csmapcontrol.MapControl.ORDER_CALCULATED_PATH.need_calculate = true;
            this.order_price_Descr.Text = "";
            this.calculator_text.Text = "";
            DistanceCity.Text = "";
            distanceMgor.Text = "";
            //calc_check_1.Checked = false;
        }

        private void Inerpoint2_address_Leave(object sender, EventArgs e)
        {
            doFindInterPoint2GeoLocation();
        }

        private void Inerpoint2_house_TextChanged(object sender, EventArgs e)
        {
            ORDER_PATH.setPoint(2, 0, 0, "");
            ORDER_PATH.getInterPoint(2).MarkerMoved = 0;
            //csmapcontrol.MapControl.ORDER_CALCULATED_PATH.Clear();
            csmapcontrol.MapControl.ORDER_CALCULATED_PATH.need_calculate = true;
            this.order_price_Descr.Text = "";
            this.calculator_text.Text = "";
            DistanceCity.Text = "";
            distanceMgor.Text = "";
            //calc_check_1.Checked = false;
        }

        private void Inerpoint2_house_Enter(object sender, EventArgs e)
        {
            if (Inerpoint2_address.Text != "")
            {
                if (Inerpoint2_address.Text != Inerpoint2_house.Tag)
                {
                    Inerpoint2_house.AutoCompleteCustomSource.Clear();
                    Inerpoint2_house.AutoCompleteCustomSource.AddRange(gisModule.getStreetNumbers("", Inerpoint2_address.Text.Replace("'", "")));

                    Inerpoint2_house.Tag = Inerpoint2_address.Text;
                }
            }
        }

        private void Inerpoint2_house_Leave(object sender, EventArgs e)
        {
            doFindInterPoint2GeoLocation();
        }

        private void Inerpoint1_address_Enter(object sender, EventArgs e)
        {
            doFillAddrFrom();
        }

        private void Inerpoint2_address_Enter(object sender, EventArgs e)
        {
            doFillAddrFrom();
        }

        private void wait_time_TextChanged(object sender, EventArgs e)
        {
            return;
            this.doUpdateCalculatorText();
        }

        private void calc_check_black_list_CheckedChanged(object sender, EventArgs e)
        {
            this.black_list_car_id.Visible = calc_check_black_list.Checked;
            this.black_list_money.Visible = calc_check_black_list.Checked;
            label22.Visible = calc_check_black_list.Checked;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            doLoadPhoneInfo();
        }

        private void btnMakeAbonent_Click(object sender, EventArgs e)
        {
            
            


            DictionaryElement elem = new DictionaryElement();
            //elem.doMakeInterface("PHONES");
            elem.doLoad("PHONES", 0);
            elem.doSetValue("STATE", 0);
            elem.doSetValue("FIO", "постоянник");
            elem.doSetValue("PHONE", Phone.Text);
            elem.doSetValue("PHONE2", ConnectPhone.Text);
            elem.doSetValue("ADDRESS", this.AddrFrom.Text);
            elem.doSetValue("HOUSENUM", this.HouseFrom.Text);
            elem.doSetValue("FLATNUM", this.FlatFrom.Text);


            //.FlatFrom.elem.doSetValue("DESCR", "ПО ЗАКАЗУ " + id.ToString() + " " + date + " " + address);


            elem.ShowDialog(this);
                
        }

        private void Inerpoint3_address_TextChanged(object sender, EventArgs e) {
            ORDER_PATH.setPoint(3, 0, 0, "");
            ORDER_PATH.getInterPoint(3).MarkerMoved = 0;
            //csmapcontrol.MapControl.ORDER_CALCULATED_PATH.Clear();
            csmapcontrol.MapControl.ORDER_CALCULATED_PATH.need_calculate = true;
            this.order_price_Descr.Text = "";
            this.calculator_text.Text = "";
            DistanceCity.Text = "";
            distanceMgor.Text = "";
            //calc_check_1.Checked = false;
        }

        private void Inerpoint3_address_SelectedIndexChanged(object sender, EventArgs e) {

        }

        private void Inerpoint4_address_TextChanged(object sender, EventArgs e) {
            ORDER_PATH.setPoint(4, 0, 0, "");
            ORDER_PATH.getInterPoint(4).MarkerMoved = 0;
            //csmapcontrol.MapControl.ORDER_CALCULATED_PATH.Clear();
            csmapcontrol.MapControl.ORDER_CALCULATED_PATH.need_calculate = true;
            this.order_price_Descr.Text = "";
            this.calculator_text.Text = "";
            DistanceCity.Text = "";
            distanceMgor.Text = "";
            //calc_check_1.Checked = false;
        }

        private void Inerpoint3_address_Enter(object sender, EventArgs e) {
            doFillAddrFrom();
        }

        private void Inerpoint4_address_Enter(object sender, EventArgs e) {
            doFillAddrFrom();
        }

        private void Inerpoint3_address_Leave(object sender, EventArgs e) {
            doFindInterPoint3GeoLocation();
        }

        private void Inerpoint4_address_Leave(object sender, EventArgs e) {
            doFindInterPoint4GeoLocation();
        }

        private void Inerpoint3_house_TextChanged(object sender, EventArgs e) {
            ORDER_PATH.setPoint(3, 0, 0, "");
            ORDER_PATH.getInterPoint(3).MarkerMoved = 0;
            //csmapcontrol.MapControl.ORDER_CALCULATED_PATH.Clear();
            csmapcontrol.MapControl.ORDER_CALCULATED_PATH.need_calculate = true;
            this.order_price_Descr.Text = "";
            this.calculator_text.Text = "";
            DistanceCity.Text = "";
            distanceMgor.Text = "";
            //calc_check_1.Checked = false;
        }

        private void Inerpoint4_house_TextChanged(object sender, EventArgs e) {
            ORDER_PATH.setPoint(4, 0, 0, "");
            ORDER_PATH.getInterPoint(4).MarkerMoved = 0;
            //csmapcontrol.MapControl.ORDER_CALCULATED_PATH.Clear();
            csmapcontrol.MapControl.ORDER_CALCULATED_PATH.need_calculate = true;
            this.order_price_Descr.Text = "";
            this.calculator_text.Text = "";
            DistanceCity.Text = "";
            distanceMgor.Text = "";
            //calc_check_1.Checked = false;
        }

        private void Inerpoint3_house_Enter(object sender, EventArgs e) {
            if (Inerpoint3_address.Text != "") {
                if (Inerpoint3_address.Text != Inerpoint3_house.Tag) {
                    Inerpoint3_house.AutoCompleteCustomSource.Clear();
                    Inerpoint3_house.AutoCompleteCustomSource.AddRange(gisModule.getStreetNumbers("", Inerpoint3_address.Text.Replace("'", "")));

                    Inerpoint3_house.Tag = Inerpoint3_address.Text;
                }
            }
        }

        private void Inerpoint4_house_Enter(object sender, EventArgs e) {
            if (Inerpoint4_address.Text != "") {
                if (Inerpoint4_address.Text != Inerpoint4_house.Tag) {
                    Inerpoint4_house.AutoCompleteCustomSource.Clear();
                    Inerpoint4_house.AutoCompleteCustomSource.AddRange(gisModule.getStreetNumbers("", Inerpoint4_address.Text.Replace("'", "")));

                    Inerpoint4_house.Tag = Inerpoint4_address.Text;
                }
            }
        }

        private void Inerpoint3_house_Leave(object sender, EventArgs e) {
            doFindInterPoint3GeoLocation();
        }

        private void Inerpoint4_house_Leave(object sender, EventArgs e) {
            doFindInterPoint4GeoLocation();
        }

        private void AddrToName_SelectedIndexChanged(object sender, EventArgs e) {
            doCalculatePath();
            doUpdateCalculatorText();
        }

        private void AddrFromName_SelectedIndexChanged(object sender, EventArgs e) {
            doCalculatePath();
            doUpdateCalculatorText();
        }

        private void Inerpoint2_name_SelectedIndexChanged(object sender, EventArgs e) {

        }

        private void Inerpoint1_name_SelectedIndexChanged(object sender, EventArgs e) {

        }

        private void ConnectPhone_TextChanged(object sender, EventArgs e) {

        }

        private void btnOkDialToPassenger_Click(object sender, EventArgs e) {
            //1246973

            String[] fields = { "status", "tries"};
            object[] values = { "d", 3};

            DataBase.mssqlUpdate("CALL_OUT", fields, values, "order_id", m_iID);


        }


        private void bnal_opl_tm_MouseDown(object sender, MouseEventArgs e) {
            if (bnal_opl_tm.Value == bnal_opl_tm.MinDate) {
                bnal_opl_tm.Value = DateTime.Now;
                TalonNumber.Text += "/ОПЛ";
            }
        }

        private void bnal_opl_tm_ValueChanged(object sender, EventArgs e) {
            TalonNumber.Text += "/ОПЛ";
        }

        private void btnMakeReview_Click(object sender, EventArgs e)
        {
            form_review form = new form_review();
            form.line.Text = this.FromPhone.Text;
            form.phone.Text = this.Phone.Text;
            form.Show(this);
        }

        private void ORGDETAIL_ID_SelectedIndexChanged(object sender, EventArgs e) {
            month_limit.Text = "";
            month_limit.Tag = 0;

            if (m_bLoading == false)
                if (ORGDETAIL_ID.Tag != "BLOCK_") {
                    if ((ORGDETAIL_ID.SelectedIndex == 0) && (ORGDETAIL_ID.Text == ""))
                        return;


                    this.Descr.Text = "БЕЗНАЛ. " + OrgID.Text;
                   





                    this.org_detail_prim.Text = "";
            
                    DataRow[] ORGDETAIL1_ID = ((DataTable)ORGDETAIL_ID.DataSource).Select("ID='" + ORGDETAIL_ID.SelectedValue.ToString()+"'");

                    if (ORGDETAIL1_ID.Length > 0)
                    {
                        this.org_detail_prim.Text = ORGDETAIL1_ID[0]["descr2"].ToString();
                    }













                    



                    ///ВОТ ТУТ КОСЯК ГРИШИНОЙ С НЕУКАЗАНЫМИ ЛИМИТАМИ
                    if (ORGDETAIL_ID.SelectedValue != null)
                        if (ORGDETAIL_ID.SelectedValue.ToString() != "System.Data.DataRowView") {
                            DataRow[] select = organization_detail.Select(" ID='" + ORGDETAIL_ID.SelectedValue.ToString() + "'");

                            if (select.Length > 0) {
                                this.Descr.Text += ", " + select[0]["Name"];
                                if ((int)select[0]["moneylimit"] > 0)
                                    if ((int)select[0]["moneylimit"] < 5000) this.Descr.Text += " лимит " + select[0]["moneylimit"] + "р.";

                                if (select[0]["month_money_limit"] != DBNull.Value) {
                                    if (((int)select[0]["month_money_limit"]) > 0) {


                                        DataTable dt = DataBase.mssqlRead("select cast(sum(bnalmoney) as varchar(10)) from orders where orders.orgid = " + OrgID.SelectedValue.ToString() + " and organizationdetailid=" + ORGDETAIL_ID.SelectedValue.ToString() + " and dtEnd >  DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0) and state=3 and resultcode=1");
                                        String sum = "0";
                                        if (dt.Rows[0][0] != DBNull.Value) {

                                            sum = dt.Rows[0][0].ToString();
                                        }


                                        if ((int)select[0]["month_money_limit"] < int.Parse(sum))
                                            month_limit.Tag = 1;


                                        //this.Descr.Text += ", лимит в месяц " + select[0]["month_money_limit"];
                                        //this.org_prim.Text = "лимит в месяц " + select[0]["month_money_limit"] + ", "+org_prim.Text;



                                        month_limit.Text = "Лимит в месяц " + sum + " / " + select[0]["month_money_limit"];


                                    }



                                }

                                //this.ClientID.Text = select[0]["Name"].ToString();
                            }

                        }
                }

            DataTable dt1 = new DataTable();
            dt1.Columns.Add("Name");
            dt1.Columns.Add("Descr");
            dt1.Columns.Add("ID");

            //здесь исключение - надо убрать
            //System.Console.WriteLine("ORDID.SelectedValue = " + OrgID.SelectedValue.ToString());

             if (ORGDETAIL_ID.SelectedValue != null)
                 if (ORGDETAIL_ID.SelectedValue.ToString() != "System.Data.DataRowView")
                 {

                     DataRow[] select1 = organization_subdetail.Select(" ORGANIZATION_DETAIL_ID='" + ORGDETAIL_ID.SelectedValue.ToString() + "'");
                     foreach (DataRow row in select1)
                     {
                         DataRow newrow = dt1.NewRow();
                         newrow["Name"] = row["Name"];
                         newrow["Descr"] = row["Descr"];
                         newrow["ID"] = row["ID"];
                         dt1.Rows.Add(newrow);

                     }

                     ORGSUBDETAIL_ID.Tag = "BLOCK_";
                     ORGSUBDETAIL_ID.DataSource = dt1;
                     ORGSUBDETAIL_ID.DisplayMember = "Name";
                     ORGSUBDETAIL_ID.ValueMember = "ID";

                     ORGSUBDETAIL_ID.AutoCompleteSource = AutoCompleteSource.ListItems;
                     ORGSUBDETAIL_ID.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                     //ORGDETAIL_ID.SelectedValue = null;
                     ORGSUBDETAIL_ID.SelectedIndex = -1;

                     //m_bLoading = false;
                     ORGSUBDETAIL_ID.Tag = "";


                 }

            doUpdateCalculatorText();


            if (month_limit.Tag != null)
                if ((int)month_limit.Tag > 0) {
                    MessageBox.Show(this, "ПРЕВЫШЕНИЕ МЕСЯЧНОГО ЛИМИТА, ЗАКАЗ ОФОРМЛЯТЬ НЕ НАДО");
                }
        }

        private void order_10percent_CheckedChanged(object sender, EventArgs e) {
            doUpdateCalculatorText();
        }

        private void AddrToForDriver_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void distanceMgor_TextChanged(object sender, EventArgs e)
        {
            return;
            doUpdateCalculatorText();
        }

        private void ORGDETAIL_ID_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            //изменилась организация - подгрузить людей этой организации.
            //вывести их списком с паролем и временем ограничения на заказ - оператор сем решит - заводить заказ или нет.

            //if (m_bLoading == false)

            this.org_detail_prim.Text = "";
            try
            {
                if (ORGDETAIL_ID.SelectedValue == null)
                {
                    ORGSUBDETAIL_ID.DataSource = null;
                }
                else
                {
                    if (ORGDETAIL_ID.SelectedValue.ToString() == "System.Data.DataRowView")
                        return;


                    DataRow[] ORGDETAIL1_ID = ((DataTable)ORGDETAIL_ID.DataSource).Select("ID='" + ORGDETAIL_ID.SelectedValue.ToString() + "'");

                    if (ORGDETAIL1_ID.Length > 0)
                    {
                        this.org_detail_prim.Text = ORGDETAIL1_ID[0]["descr2"].ToString();
                    }
                    
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Name");
                    dt.Columns.Add("Descr");
                    dt.Columns.Add("ID");
                    
                    //здесь исключение - надо убрать
                    //System.Console.WriteLine("ORDID.SelectedValue = " + OrgID.SelectedValue.ToString());
                    DataRow[] select = organization_subdetail.Select(" ORGANIZATION_DETAIL_ID='" + ORGDETAIL_ID.SelectedValue.ToString() + "'");
                    foreach (DataRow row in select)
                    {
                        DataRow newrow = dt.NewRow();
                        newrow["Name"] = row["Name"];
                        newrow["Descr"] = row["Descr"];
                        newrow["ID"] = row["ID"];
                        dt.Rows.Add(newrow);

                    }
                    
                    ORGSUBDETAIL_ID.Tag = "BLOCK_";
                    ORGSUBDETAIL_ID.DataSource = dt;
                    ORGSUBDETAIL_ID.DisplayMember = "Name";
                    ORGSUBDETAIL_ID.ValueMember = "ID";

                    ORGSUBDETAIL_ID.AutoCompleteSource = AutoCompleteSource.ListItems;
                    ORGSUBDETAIL_ID.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    //ORGDETAIL_ID.SelectedValue = null;
                    ORGSUBDETAIL_ID.SelectedIndex = -1;

                    //m_bLoading = false;
                    ORGSUBDETAIL_ID.Tag = "";

                    btnSumtoDriver.Visible = true;

                    doUpdateCalculatorText();

                }

            }
            catch (Exception eee)
            {
                //btnSumtoDriver.Visible = false;
            }
        }

        private void ORGSUBDETAIL_ID_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.org_subdetail_prim.Text = "";
            if (ORGSUBDETAIL_ID.SelectedValue == null)
                     return;
            if (ORGSUBDETAIL_ID.SelectedValue.ToString() == "System.Data.DataRowView")
                         return;

            DataRow[] ORGSUB_DETAIL_ID = ((DataTable)ORGSUBDETAIL_ID.DataSource).Select("ID='" + ORGSUBDETAIL_ID.SelectedValue.ToString() + "'");

            if (ORGSUB_DETAIL_ID.Length > 0)
                 this.org_subdetail_prim.Text = ORGSUB_DETAIL_ID[0]["descr"].ToString();

             
        }

        private void btnДанные_Click(object sender, EventArgs e)
        {
            

            String sql = "select  tm, carid, status from orders_distrib_log where orderid=" + m_iID + " order by tm desc";
            DataTable readTbl = DataBase.mssqlRead(sql);
            String result = "";

            for (int i = 0; i < readTbl.Rows.Count; i++)
            {
                result += readTbl.Rows[i][0].ToString() + '\t' + readTbl.Rows[i][1].ToString() + '\t' + readTbl.Rows[i][2].ToString() + "\r\n";

            }

            text_show_form form = new text_show_form();
            form.textBox1.Text = result;
            form.Show();
        }



        



    }
}
