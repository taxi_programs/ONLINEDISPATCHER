﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ONLINEDISPATCHER
{
    public partial class dictionary : Form
    {

        

        private  List<SqlParameter> sqlParameters = new List<SqlParameter>();

        private string Sql = "";

        private string dop_sql_where = "";
        private List<QueryVisualParam> Fields= new List<QueryVisualParam>();

        private string dic_type = "";

        public int summ_value_index = -1;




        public class QueryVisualParam : TableLayoutPanel
        {
            public Label label;
            public Control visual_control;
//            private TextBox text_box;
//            private TextBox combo_box;
            public SqlParameter param = null;
            public QueryVisualParamType mQueryVisualParamType; 

            public enum QueryVisualParamType
            {
                Text,
                BigText,
                Numeric,
                Calendar,
                Combobox,
                Checkbox,
                DBDictionary,
                Decimal,
                Image, 
                DBTextDictionary
            }

            public void SetValue(object newValue)
            {
                if (newValue == DBNull.Value)
                    return;
                switch (mQueryVisualParamType)
                {
                    case QueryVisualParamType.Text:
                    case QueryVisualParamType.BigText:

                        ((TextBox)(visual_control)).Text = (String)newValue;
                        break;
                    case QueryVisualParamType.Image:
                        try {
                            byte[] data = (byte[])newValue;
                            ((PictureBox)(visual_control)).Image = Image.FromStream(new System.IO.MemoryStream(data));
                            ((PictureBox)(visual_control)).BackgroundImage = null;
                        } catch (Exception e) {
                        }
                        
                        //pictureBox1.Image = Image.FromStream(new MemoryStream(data));
                        break;

                    case QueryVisualParamType.Numeric:
                        ((NumericTextbox)(visual_control)).Text = newValue.ToString();
                        break;
                    case QueryVisualParamType.Decimal:
                        ((NumericTextbox)(visual_control)).Text = newValue.ToString();
                        break;                    
                        
                    case QueryVisualParamType.Combobox:
                        ((ComboBox)(visual_control)).SelectedIndex = (int)newValue;
                        break;

                    case QueryVisualParamType.Checkbox:
                        ((CheckBox)(visual_control)).Checked = ((int)newValue==1);
                        break;
                    case QueryVisualParamType.Calendar:
                        ((DateTimePicker)(visual_control)).Value = (DateTime)newValue;
                        break;
                    case QueryVisualParamType.DBDictionary:


                        ((ComboBox)(visual_control)).SelectedValue = newValue;
                        break;
                    case QueryVisualParamType.DBTextDictionary:
                        ((ComboBox)(visual_control)).Text = (String)newValue;
                        break;

                }
            }

            public object getValue()
            {
                object value = new object();

                switch (mQueryVisualParamType)
                {
                    case QueryVisualParamType.Text:
                    case QueryVisualParamType.BigText:

                        value = ((TextBox)(visual_control)).Text;
                        break;
                    case QueryVisualParamType.Image:

                        value = new byte[0];
                        if (((PictureBox)(visual_control)).Image != null) {
                            System.IO.MemoryStream ms = new System.IO.MemoryStream();
                            ((PictureBox)(visual_control)).Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                            //pox..Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                            byte[] data = ms.GetBuffer();


                            value = data;
                        } 
                        //else {
                            //Image im = new Image().;

//                        }
                        break;
                    case QueryVisualParamType.Numeric:

                        value = ((NumericTextbox)(visual_control)).IntValue;
                        break;
                    case QueryVisualParamType.Decimal:

                        value = ((NumericTextbox)(visual_control)).DecimalValue;
                        break;


                    case QueryVisualParamType.Combobox:
                        value = ((ComboBox)(visual_control)).SelectedIndex;
                        break;
                    case QueryVisualParamType.Checkbox:
                        value = ((CheckBox)(visual_control)).Checked;
                        break;
                    case QueryVisualParamType.Calendar:
                        value = ((DateTimePicker)(visual_control)).Value;
                        break;
                    case QueryVisualParamType.DBDictionary:
                        value = ((ComboBox)(visual_control)).SelectedValue;
                        break;
                    case QueryVisualParamType.DBTextDictionary:
                        value = ((ComboBox)(visual_control)).Text;
                        break;


                }

                return value;
            }

            public QueryVisualParam(string label_text, string field_name, QueryVisualParamType type, Object list_items)
                : base()
            {
                this.RowCount = 1;
                this.ColumnCount = 2;
                this.GrowStyle = TableLayoutPanelGrowStyle.AddRows;
                this.Anchor = AnchorStyles.Right;
                    //(System.Windows.Forms.AnchorStyles)( AnchorStyles.Right | AnchorStyles.Left);
                //this.Dock = DockStyle.Fill;
                //this.BackColor = Color.Aqua;
                mQueryVisualParamType = type;

                AutoSize = true;

                label = new Label();
                label.Text = label_text;
                //label.Width = 100;
                label.AutoSize = true;
                label.Anchor = AnchorStyles.Right;
                label.TextAlign = ContentAlignment.MiddleRight;

                Controls.Add(label);

                

                

                

                if (type == QueryVisualParamType.Text)
                {
                    
                    TextBox tbox = new TextBox();
                    //tbox.AutoSize = true;
                    tbox.Anchor = AnchorStyles.Left;
                    tbox.TextChanged += new EventHandler(text_box_TextChanged);
                    visual_control = tbox;

                    param = new SqlParameter(field_name, SqlDbType.Text);
                    param.Value = tbox.Text;
                }
                if (type == QueryVisualParamType.BigText)
                {
                    TextBox tbox = new TextBox();
                    tbox.Multiline = true;
                    tbox.Height = tbox.Height * 4;
                    tbox.Width = tbox.Width * 2;
                    //tbox.AutoSize = true;
                    tbox.Anchor = AnchorStyles.Left;
                    tbox.TextChanged += new EventHandler(text_box_TextChanged);
                    visual_control = tbox;

                    param = new SqlParameter(field_name, SqlDbType.Text);
                    param.Value = tbox.Text;
                }

                if (type == QueryVisualParamType.Image)
                {
                    PictureBox pbox = new PictureBox();
                    pbox.BorderStyle = BorderStyle.FixedSingle;
                    visual_control = pbox;
                    pbox.SizeMode = PictureBoxSizeMode.Zoom;

                    pbox.Height = 150;
                    pbox.Width = 150;
                    //pbox.setMaximumSize.Width = 100;
                    //pbox.MaximumSize.Height = 100;
                    //tbox.AutoSize = true;
                    //tbox.Anchor = AnchorStyles.Left;
                    //событие вызывается при смене БГ картинки - но данные лежать в обычном Image (он масштабируется при отображении)
                    pbox.BackgroundImageChanged += new EventHandler(pbox_BackgroundImageChanged); 
                    

                    param = new SqlParameter(field_name, SqlDbType.Variant);

                    System.IO.MemoryStream ms = new System.IO.MemoryStream();
                    param.Value = null;

                    if (pbox.Image != null)
                    {
                    pbox.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    //pox..Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    byte[] data = ms.GetBuffer();


                    param.Value = data;
                    }

                    pbox.ContextMenu = new ContextMenu();
                    pbox.ContextMenu.MenuItems.Add("Задать изображение из файла").Click += new EventHandler(pbox_ContextMenu1_Click);
                    pbox.ContextMenu.MenuItems.Add("Задать изображение из буфера обмена").Click += new EventHandler(pbox_ContextMenu1_Click);
                    pbox.ContextMenu.MenuItems.Add("Копировать в буфер обмена").Click += new EventHandler(pbox_ContextMenu1_Click);
                    pbox.ContextMenu.MenuItems.Add("Очистить").Click += new EventHandler(pbox_ContextMenu1_Click);
                        


                    //param.Value = tbox.Text;
                }

               

                if (type == QueryVisualParamType.Numeric)
                {

                    NumericTextbox tbox = new NumericTextbox();
                    //tbox.AutoSize = true;
                    tbox.Anchor = AnchorStyles.Left;
                    tbox.TextChanged += new EventHandler(text_box_TextChanged);

                    
                    visual_control = tbox;

                    param = new SqlParameter(field_name, SqlDbType.BigInt);
                    param.Value = tbox.IntValue;

                    
                }

                if (type == QueryVisualParamType.Decimal)
                {

                    NumericTextbox tbox = new NumericTextbox();
                    //tbox.AutoSize = true;
                    tbox.Anchor = AnchorStyles.Left;
                    tbox.TextChanged += new EventHandler(text_box_TextChanged);


                    visual_control = tbox;

                    param = new SqlParameter(field_name, SqlDbType.Float);
                    param.Value = tbox.DecimalValue;

                    
                }
                if (type == QueryVisualParamType.Calendar)
                {

                    DateTimePicker picker = new DateTimePicker();
                    //cbox.DropDownStyle = ComboBoxStyle.DropDownList;

                    //List<object> list_elem = (List<object>)list_items;
                    //cbox.Items.AddRange(list_elem.ToArray());


                    // cbox.AutoSize = true;
                    //cbox.Anchor = AnchorStyles.Left;

                    picker.ValueChanged += new EventHandler(picker_ValueChanged);

                    visual_control = picker;
                    
                    param = new SqlParameter(field_name, SqlDbType.DateTime);
                }

                if (type == QueryVisualParamType.Combobox)
                {
                    ComboBox cbox = new ComboBox();
                    cbox.DropDownStyle = ComboBoxStyle.DropDownList;
                    
                    List<object> list_elem = (List<object> ) list_items;
                    cbox.Items.AddRange(list_elem.ToArray());
                    

                   // cbox.AutoSize = true;
                    cbox.Anchor = AnchorStyles.Left;

                    cbox.SelectedIndexChanged += new EventHandler(cbox_SelectedIndexChanged);

                    visual_control = cbox;
                    
                    
                    param = new SqlParameter(field_name, SqlDbType.BigInt);
                    param.Value = 0;

                }


                if (type == QueryVisualParamType.Checkbox)
                {
                    CheckBox checkbox = new CheckBox();
                    


                    // cbox.AutoSize = true;
                    checkbox.Anchor = AnchorStyles.Left;

                    checkbox.CheckedChanged += new EventHandler(checkbox_CheckedChanged);

                    visual_control = checkbox;


                    param = new SqlParameter(field_name, SqlDbType.BigInt);
                    param.Value = 0;

                }

                if (type == QueryVisualParamType.DBDictionary) {

                    //var t1 = new dictionary.QueryVisualParam("Служба", "SERVICE_ID", dictionary.QueryVisualParam.QueryVisualParamType.DBDictionary, null,  services);


                    SergeUtils.EasyCompletionComboBox cbox = new SergeUtils.EasyCompletionComboBox();//
                    DataTable datatable = (DataTable)list_items;
                    //cbox.DataBindings.DefaultDataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
                    cbox.DataSource = datatable;

                    cbox.DisplayMember = "Name";
                    cbox.ValueMember = "ID";
                    cbox.DropDownStyle = ComboBoxStyle.DropDown;
                    cbox.AutoCompleteSource = AutoCompleteSource.ListItems;

                    cbox.MatchingMethod = SergeUtils.StringMatchingMethod.UseRegexs;
                    cbox.Text = "";


                    //ComboBox cbox = new ComboBox();
                   // cbox.DropDownStyle = ComboBoxStyle.DropDownList;

                 
                 
                    if (((DataTable)cbox.DataSource).Rows.Count > 0)
                        cbox.DropDownHeight = cbox.ItemHeight * ((DataTable)cbox.DataSource).Rows.Count;
                    // cbox.AutoSize = true;
                    cbox.Anchor = AnchorStyles.Left;

                    cbox.SelectedIndexChanged += new EventHandler(cboxDBDic_SelectedIndexChanged);

                    cbox.SelectedItem = 50;// null;
                    cbox.SelectedValue = 50;

                    visual_control = cbox;

                    cbox.SelectedValue = "50";
                    cbox.SelectedItem = "50";// null;


                    param = new SqlParameter(field_name, SqlDbType.BigInt);
                    param.Value = 0;
                }

                if (type == QueryVisualParamType.DBTextDictionary)
                {

                    //var t1 = new dictionary.QueryVisualParam("Служба", "SERVICE_ID", dictionary.QueryVisualParam.QueryVisualParamType.DBDictionary, null,  services);


                    ComboBox cbox = new ComboBox();
                    cbox.DropDownStyle = ComboBoxStyle.DropDown;

                    DataTable datatable = (DataTable)list_items;
                    cbox.DataBindings.DefaultDataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
                    cbox.DataSource = datatable;

                    //Category.DataTextField = "CatName";
                    //Category.DataValueField = "CatName";

                    cbox.DisplayMember = "Name";
                    cbox.DropDownStyle = ComboBoxStyle.DropDown;
                    cbox.DropDownHeight = cbox.ItemHeight * ((DataTable)cbox.DataSource).Rows.Count;
                    cbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    cbox.AutoCompleteSource = AutoCompleteSource.ListItems;




                    // cbox.AutoSize = true;
                    //cbox.Anchor = AnchorStyles.Left;
                    cbox.TextChanged += new EventHandler(cbox_TextChanged);
//                    cbox.SelectedIndexChanged += new EventHandler(cboxDBDic_SelectedIndexChanged);

                    //cbox.SelectedItem = null;

                    visual_control = cbox;


                    param = new SqlParameter(field_name, SqlDbType.Text);
                    param.Value = "";
                }



                visual_control.Width = 200;
                Controls.Add(visual_control);
                
            }

            void checkbox_CheckedChanged(object sender, EventArgs e)
            {
                param.Value = ((CheckBox)sender).Checked;
            }

            void cbox_TextChanged(object sender, EventArgs e)
            {
                param.Value = ((SergeUtils.EasyCompletionComboBox)sender).Text;
            }

            void picker_ValueChanged(object sender, EventArgs e)
            {
                 param.Value = ((DateTimePicker)sender).Value;
            }

            void pbox_BackgroundImageChanged(object sender, EventArgs e)
            {
                param.Value = null;

                //((PictureBox)sender).Image = ((PictureBox)sender).BackgroundImage;

                if (((PictureBox)sender).Image != null)
                {
                    System.IO.MemoryStream ms = new System.IO.MemoryStream();
                    ((PictureBox)sender).Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    //pox..Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    byte[] data = ms.GetBuffer();


                    param.Value = data;
                }
            }

            void cbox_SelectedIndexChanged(object sender, EventArgs e)
            {
                param.Value = ((ComboBox)sender).SelectedIndex;
            }

            void cboxDBDic_SelectedIndexChanged(object sender, EventArgs e) {
                param.Value = ((SergeUtils.EasyCompletionComboBox)sender).SelectedValue;
            }

            void text_box_TextChanged(object sender, EventArgs e)
            {

                param.Value = ((TextBox)sender).Text;
            }

            void pbox_ContextMenu1_Click(object sender, EventArgs e)
            {
                System.Console.WriteLine(((MenuItem)sender).Text);
                if (((MenuItem)sender).Text.CompareTo("Задать изображение из файла")== 0){
                    OpenFileDialog dialog = new OpenFileDialog();
                    if (dialog.ShowDialog() == DialogResult.OK)
                    {

                        Image photo = Image.FromFile(dialog.FileName);
                        ContextMenu menu = (ContextMenu)((MenuItem)sender).Parent;
                        PictureBox pbox = (PictureBox)menu.SourceControl;
                        pbox.Image = photo;
                        pbox.BackgroundImage = null;

/*                        System.IO.MemoryStream ms = new System.IO.MemoryStream();
                        photo.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                        //pox..Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                        byte[] data = ms.GetBuffer();


                        param.Value = data;
 */ 

                        //pictureBox1.Image = photo;
                    }
                }


                if (((MenuItem)sender).Text.CompareTo("Задать изображение из буфера обмена") == 0) {

                    ContextMenu menu = (ContextMenu)((MenuItem)sender).Parent;
                    System.Drawing.Image returnImage = null;
                    if (Clipboard.ContainsImage()) {
                        returnImage = Clipboard.GetImage();

                        PictureBox pbox = (PictureBox)menu.SourceControl;
                        pbox.Image = returnImage;
                        pbox.BackgroundImage = null;
                        
                    }
                    
                }

                if (((MenuItem)sender).Text.CompareTo("Копировать в буфер обмена") == 0) {

                    ContextMenu menu = (ContextMenu)((MenuItem)sender).Parent;
                    

                    PictureBox pbox = (PictureBox)menu.SourceControl;
                    System.Drawing.Image returnImage = pbox.Image;
                    Clipboard.SetImage(returnImage);
                    
                }


                


                if (((MenuItem)sender).Text.CompareTo("Очистить") == 0) {
                        
                        ContextMenu menu = (ContextMenu)((MenuItem)sender).Parent;
                        PictureBox pbox = (PictureBox)menu.SourceControl;
                        pbox.Image = null;
                        pbox.BackgroundImage = null;

                       
                    
                }


            }
        }





        public dictionary()
        {
            InitializeComponent();

            if (this.ParentForm != null)
                this.Location = this.ParentForm.Location;// Cursor.Position;
            else
                this.Location = Cursor.Position;
        }


        public void makeInterface(string type)
        {
            dic_type = type;
            if (type == "PHONES")
            {
                //фильтр
                //sql
                //колонки таблицы

                var panel_base = new TableLayoutPanel();
                //panel.FlowDirection = FlowDirection.TopDown;
                panel_base.Dock = DockStyle.Top;
                panel_base.RowCount = 1;
                panel_base.ColumnCount = 1;
                panel_base.AutoSize = true;
                //panel_base.BackColor = Color.Red;
                



                var panel = new TableLayoutPanel();
                //panel.FlowDirection = FlowDirection.TopDown;
                //panel.Dock = DockStyle.Top;
                //panel.RowCount = 2;
                panel.ColumnCount = 2;
                //panel.BackColor = Color.Azure;
                panel.AutoSize = true;
                panel_base.Controls.Add(panel);


                //panel1.Top = 40;

                var phone = new QueryVisualParam("Телефон", "phone", QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(phone);
                Fields.Add(phone);
                sqlParameters.Add(phone.param);


                String [] state_list= {"Белый", "ЧЕРНЫЙ", "Серый", "VIP"};
                List<object> ll = new List<object>();
                ll.AddRange(state_list);
                var state = new QueryVisualParam("Состояние", "state", QueryVisualParam.QueryVisualParamType.Combobox, ll);
                panel.Controls.Add(state);
                Fields.Add(state);
                sqlParameters.Add(state.param);

                this.Controls.Add(panel_base);

                Sql = "SELECT $TOP_REPLACE$ ID, ADDRESS, HOUSENUM, FLATNUM, FIO,  PHONE, PHONE2, PHONE3, DESCR, skid_percent, money_summ, car_id FROM PHONES WHERE DELETED = 0 " +
                " and phone+case when phone2 is null then '' else phone2 end+case when phone3 is null then '' else phone3 end like '%'+@phone+'%'"+
                " and state=@state"
                
                ;
                this.Text = "АБОНЕНТЫ / ЧЕРНЫЕ СПИСКИ";

            }

            if (type == "CARS") {
                //фильтр
                //sql
                //колонки таблицы

                var panel_base = new TableLayoutPanel();
                //panel.FlowDirection = FlowDirection.TopDown;
                panel_base.Dock = DockStyle.Top;
                panel_base.RowCount = 1;
                panel_base.ColumnCount = 1;
                panel_base.AutoSize = true;
                //panel_base.BackColor = Color.Red;




                var panel = new TableLayoutPanel();
                //panel.FlowDirection = FlowDirection.TopDown;
                //panel.Dock = DockStyle.Top;
                //panel.RowCount = 2;
                panel.ColumnCount = 2;
                //panel.BackColor = Color.Azure;
                panel.AutoSize = true;
                panel_base.Controls.Add(panel);


                //panel1.Top = 40;

                var phone = new QueryVisualParam("гос номер", "gos_num", QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(phone);
                Fields.Add(phone);
                sqlParameters.Add(phone.param);


               

                this.Controls.Add(panel_base);

                Sql = "SELECT $TOP_REPLACE$ ID, (select arenda_cars.mark + ' '+arenda_cars.gosnum+' '+arenda_cars.color from arendA_cars where arenda_cars.id = cars.arenda_car_id) арендная_машина,  mark, gosnum, color, Descr, (select services.[Name] from services  where services.id = cars.ServiceID) service_name, " +
" (select top 1 FIO+' '+drivers.Phones+' '+drivers.Descr+', баланс '+cast( case when (select summ from balance where balance.doc_id=drivers.id and balance.doc_typ=\'DRIVER\') is null then '0' else (select summ from balance where balance.doc_id=drivers.id and balance.doc_typ=\'DRIVER\') end as varchar(10)) from drivers where drivers.CarID = cars.id and drivers.[State]=0 ) driver_descr " +
 " FROM cars ";

                ;
                this.Text = "Список машин";

            }



            if (type == "ARENDA_CARS")
            {
                //фильтр
                //sql
                //колонки таблицы

                var panel_base = new TableLayoutPanel();
                //panel.FlowDirection = FlowDirection.TopDown;
                panel_base.Dock = DockStyle.Top;
                panel_base.RowCount = 1;
                panel_base.ColumnCount = 1;
                panel_base.AutoSize = true;
                //panel_base.BackColor = Color.Red;




                var panel = new TableLayoutPanel();
                //panel.FlowDirection = FlowDirection.TopDown;
                //panel.Dock = DockStyle.Top;
                //panel.RowCount = 2;
                panel.ColumnCount = 2;
                //panel.BackColor = Color.Azure;
                panel.AutoSize = true;
                panel_base.Controls.Add(panel);


                //panel1.Top = 40;

                var phone = new QueryVisualParam("гос номер", "gosnum", QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(phone);
                Fields.Add(phone);
                sqlParameters.Add(phone.param);




                this.Controls.Add(panel_base);

                Sql = "SELECT $TOP_REPLACE$ ID,  (select cast(cars.id as varchar(10))+' ' from cars where cars.arenda_car_id = arenda_cars.id order by cars.id for xml path('')) привязка,  mark, gosnum, color, Descr, probeg, dt_create, dt_police_end, is_worked, sts, pts  FROM arenda_cars ";

                ;
                this.Text = "Список арендных машин";

            }

            if (type == "DRIVERS") {
                //фильтр
                //sql
                //колонки таблицы

                var panel_base = new TableLayoutPanel();
                //panel.FlowDirection = FlowDirection.TopDown;
                panel_base.Dock = DockStyle.Top;
                panel_base.RowCount = 1;
                panel_base.ColumnCount = 1;
                panel_base.AutoSize = true;
                //panel_base.BackColor = Color.Red;




                var panel = new TableLayoutPanel();
                //panel.FlowDirection = FlowDirection.TopDown;
                //panel.Dock = DockStyle.Top;
                //panel.RowCount = 2;
                panel.ColumnCount = 2;
                //panel.BackColor = Color.Azure;
                panel.AutoSize = true;
                panel_base.Controls.Add(panel);


                //panel1.Top = 40;

                var fio = new QueryVisualParam(" ФИО", "fio", QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(fio);
                Fields.Add(fio);
                sqlParameters.Add(fio.param);



                Label where_label = new Label();
                where_label.Text = "доп фильтрация";
                TextBox where = new TextBox();
                where.TextChanged += new EventHandler(where_TextChanged);
                where.Width = 300;
                panel.Controls.Add(new Label());
                panel.Controls.Add(where_label);
                panel.Controls.Add(where);

                this.Controls.Add(panel_base);



                Button  btnPrintDrivers = new Button();
                btnPrintDrivers.Text = "Бэйджи";
                btnPrintDrivers.Click += new EventHandler(btnPrintDrivers_Click);
                btnPrintDrivers.Left = 400;
                btnPrintDrivers.Top = 5;

                Button btnPrintDrivers2 = new Button();
                btnPrintDrivers2.Text = "Список";
                btnPrintDrivers2.Click += new EventHandler(btnPrintDrivers2_Click);
                btnPrintDrivers2.Left = 500;
                btnPrintDrivers2.Top = 5;

                Button btnPrintDrivers3 = new Button();
                btnPrintDrivers3.Text = "Фотки";
                btnPrintDrivers3.Click += new EventHandler(btnPrintDrivers3_Click);
                btnPrintDrivers3.Left = 600;
                btnPrintDrivers3.Top = 5;

                panel1.Controls.Add(btnPrintDrivers);
                panel1.Controls.Add(btnPrintDrivers2);
                panel1.Controls.Add(btnPrintDrivers3);








                Sql = "SELECT $TOP_REPLACE$ ID,(select ID from cars  where cars.id = drivers.carid) carid,  FIO, PHONES, PHONE2, PHONE3, DESCR, (select summ from balance where balance.doc_id=drivers.id and balance.doc_typ=\'DRIVER\') BALANCE, (select Color + ' '+mark+' '+gosnum+' '+descr from cars  where cars.id = drivers.carid) carname, dtEmployed, (select max(dtEnd) from orders where orders.CarID = drivers.CarID and orders.dtEnd > getdate() - 90) last_order  FROM drivers where drivers.[State]=0 $dop_sql_where$";

                ;
                this.Text = "Список водителей";

            }

            if (type == "ABONENTS")
            {
                //фильтр
                //sql
                //колонки таблицы

                var panel_base = new TableLayoutPanel();
                //panel.FlowDirection = FlowDirection.TopDown;
                panel_base.Dock = DockStyle.Top;
                panel_base.RowCount = 1;
                panel_base.ColumnCount = 1;
                panel_base.AutoSize = true;
                //panel_base.BackColor = Color.Red;




                var panel = new TableLayoutPanel();
                //panel.FlowDirection = FlowDirection.TopDown;
                //panel.Dock = DockStyle.Top;
                //panel.RowCount = 2;
                panel.ColumnCount = 2;
                //panel.BackColor = Color.Azure;
                panel.AutoSize = true;
                panel_base.Controls.Add(panel);


                //panel1.Top = 40;

                var fio = new QueryVisualParam(" ФИО", "fio", QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(fio);
                Fields.Add(fio);
                sqlParameters.Add(fio.param);


                this.Controls.Add(panel_base);

                Sql = "SELECT $TOP_REPLACE$ ID,(select ID from cars  where cars.id = drivers.carid) carid,  FIO, PHONES, DESCR, BALANCE, (select Color + ' '+mark+' '+gosnum+' '+descr from cars  where cars.id = drivers.carid) carname FROM drivers where drivers.[State]=0 ";

                ;
                this.Text = "Список водителей";

            }

            if (type == "ORGANIZATIONS") {
                //фильтр
                //sql
                //колонки таблицы

                var panel_base = new TableLayoutPanel();
                //panel.FlowDirection = FlowDirection.TopDown;
                panel_base.Dock = DockStyle.Top;
                panel_base.RowCount = 1;
                panel_base.ColumnCount = 1;
                panel_base.AutoSize = true;
                //panel_base.BackColor = Color.Red;




                var panel = new TableLayoutPanel();
                //panel.FlowDirection = FlowDirection.TopDown;
                //panel.Dock = DockStyle.Top;
                //panel.RowCount = 2;
                panel.ColumnCount = 2;
                //panel.BackColor = Color.Azure;
                panel.AutoSize = true;
                panel_base.Controls.Add(panel);



                //panel1.Top = 40;

                var AgreementStatus = new QueryVisualParam("В работе", "AgreementStatus", QueryVisualParam.QueryVisualParamType.Checkbox, new List<object>());
                AgreementStatus.SetValue(1);
                panel.Controls.Add(AgreementStatus);
                Fields.Add(AgreementStatus);
                sqlParameters.Add(AgreementStatus.param);






                this.Controls.Add(panel_base);

                Sql = "SELECT  TOP 1000 ID, name, (select balance.summ from balance where balance.doc_typ=\'ORGANIZATION\' and balance.DOC_ID=ORGANIZATIONS.ID) BALANCE, DESCR, AgreementNum, PhoneList, AgreementStatus, address,"+
                "(select sum(orders.BnalMoney) from orders where orders.dtEnd > getdate()-30 and orders.OrgID = organizations.id and orders.ResultCode=1) as \'Сумма за 30 дней\', "+
                "replace((select CAST(doc_summ as varchar(10))+', '+cast(doc_date as varchar(20))+'|' from balance_reg where balance_reg.TO_DOC_TYP=\'ORGANIZATION\' and balancE_reg.TO_DOC_ID = ORGANIZATIONS.ID and BALANCE_REG.DOC_DATE > getdate() - 30 order by doc_date desc for xml path(\'\')), \'|\', \'; \') as \'Последние платежки\' "+
                "FROM ORGANIZATIONS "+
                "where  AgreementStatus=@AgreementStatus";

                
                this.btnOrganizationDetails.Visible = true;
                this.btnOrganizationPayments.Visible = true;
                this.btnAkts.Visible = true;
                this.Text = "Список организаций";

            }


            if (type == "ORGANIZATION_DETAIL") {
                //фильтр
                //sql
                //колонки таблицы

                var panel_base = new TableLayoutPanel();
                //panel.FlowDirection = FlowDirection.TopDown;
                panel_base.Dock = DockStyle.Top;
                panel_base.RowCount = 1;
                panel_base.ColumnCount = 1;
                panel_base.AutoSize = true;
                //panel_base.BackColor = Color.Red;






                var panel = new TableLayoutPanel();
                //panel.FlowDirection = FlowDirection.TopDown;
                //panel.Dock = DockStyle.Top;
                //panel.RowCount = 2;
                panel.ColumnCount = 2;
                //panel.BackColor = Color.Azure;
                panel.AutoSize = true;
                




                var is_active = new QueryVisualParam("В работе", "is_active", QueryVisualParam.QueryVisualParamType.Checkbox, new List<object>());
                is_active.SetValue(1);
                panel.Controls.Add(is_active);
                Fields.Add(is_active);
                sqlParameters.Add(is_active.param);
                
                panel_base.Controls.Add(panel);

                this.Controls.Add(panel_base);

                Sql = "SELECT  TOP 1000 ID, name, password,moneylimit, timelimit, month_money_limit, is_active FROM ORGANIZATION_DETAIL where ORGANIZATIONID=" + this.Tag +
                    "  and is_active = @is_active";


                this.btn_oranization_subdetail.Visible = true;

                
                this.Text = "Список сотрудников(отделов) организации";

            }

            if (type == "ORGANIZATION_SUBDETAIL")
            {
                //фильтр
                //sql
                //колонки таблицы

                var panel_base = new TableLayoutPanel();
                //panel.FlowDirection = FlowDirection.TopDown;
                panel_base.Dock = DockStyle.Top;
                panel_base.RowCount = 1;
                panel_base.ColumnCount = 1;
                panel_base.AutoSize = true;
                //panel_base.BackColor = Color.Red;






                var panel = new TableLayoutPanel();
                //panel.FlowDirection = FlowDirection.TopDown;
                //panel.Dock = DockStyle.Top;
                //panel.RowCount = 2;
                panel.ColumnCount = 2;
                //panel.BackColor = Color.Azure;
                panel.AutoSize = true;





                var is_active = new QueryVisualParam("В работе", "is_active", QueryVisualParam.QueryVisualParamType.Checkbox, new List<object>());
                is_active.SetValue(1);
                panel.Controls.Add(is_active);
                Fields.Add(is_active);
                sqlParameters.Add(is_active.param);

                panel_base.Controls.Add(panel);

                this.Controls.Add(panel_base);

                Sql = "SELECT  TOP 1000 ID, name, descr, is_active FROM ORGANIZATION_SUBDETAIL where ORGANIZATION_DETAIL_ID=" + this.Tag +
                    "  and is_active = @is_active";




                this.Text = "Список сотрудников отделов организации";

            }

            


            if (type == "ORGANIZATION_PAYMENTS") {
                //фильтр
                //sql
                //колонки таблицы

                var panel_base = new TableLayoutPanel();
                //panel.FlowDirection = FlowDirection.TopDown;
                panel_base.Dock = DockStyle.Top;
                panel_base.RowCount = 1;
                panel_base.ColumnCount = 1;
                panel_base.AutoSize = true;
                //panel_base.BackColor = Color.Red;




                var panel = new TableLayoutPanel();
                //panel.FlowDirection = FlowDirection.TopDown;
                //panel.Dock = DockStyle.Top;
                //panel.RowCount = 2;
                panel.ColumnCount = 2;
                //panel.BackColor = Color.Azure;
                panel.AutoSize = true;
                panel_base.Controls.Add(panel);

                /////
                DataTable organizations = DataBase.mssqlRead("SELECT ID, name from ORGANIZATIONS where AGREEMENTSTATUS=1");
                DataRow dr = organizations.NewRow();
                dr[0] = "-1";
                dr[1] = "<Все>";
                organizations.Rows.InsertAt( dr, 0);
                //organizations.Rows.InsertAt(new ("-1", "<Все>");

                this.Controls.Add(panel_base);

                var dt_from = new QueryVisualParam("С", "dt_from", QueryVisualParam.QueryVisualParamType.Calendar, new List<object>());
                panel.Controls.Add(dt_from);
                Fields.Add(dt_from);
                sqlParameters.Add(dt_from.param);
                var dt_to = new QueryVisualParam("по", "dt_to", QueryVisualParam.QueryVisualParamType.Calendar, new List<object>());
                panel.Controls.Add(dt_to);
                Fields.Add(dt_to);
                sqlParameters.Add(dt_to.param);

                var org = new QueryVisualParam("Организация", "from_doc_id", QueryVisualParam.QueryVisualParamType.DBDictionary, organizations);
                panel.Controls.Add(org);
                Fields.Add(org);
                sqlParameters.Add(org.param);

                panel_base.Controls.Add(panel);


                org.SetValue(this.Tag.ToString());
                dt_from.SetValue(DateTime.Parse("01/01/2014"));
                dt_to.SetValue( DateTime.Today.AddDays(1.0));
                ////


                Sql = "SELECT BALANCE_REG_ID ID, reg_desc, doc_date, doc_summ, (select name from organizations where organizations.id = from_doc_id) ORG_NAME from balance_reg where ((from_doc_id = @from_doc_id) or (@from_doc_id=-1)) and doc_typ='BANK_PP' and doc_date between @dt_from and @dt_to order by doc_date";


                this.btnDelete.Visible = true;

                this.btnEdit.Visible = false;

                this.Text = "Список платежек организации";

            }




            if (type == "DRIVER_BALANCE_HISTORY")
            {
                //фильтр
                //sql
                //колонки таблицы

                var panel_base = new TableLayoutPanel();
                //panel.FlowDirection = FlowDirection.TopDown;
                panel_base.Dock = DockStyle.Top;
                panel_base.RowCount = 1;
                panel_base.ColumnCount = 1;
                panel_base.AutoSize = true;
                //panel_base.BackColor = Color.Red;




                var panel = new TableLayoutPanel();
                //panel.FlowDirection = FlowDirection.TopDown;
                //panel.Dock = DockStyle.Top;
                //panel.RowCount = 2;
                panel.ColumnCount = 2;
                //panel.BackColor = Color.Azure;
                panel.AutoSize = true;
                panel_base.Controls.Add(panel);



                Sql = "select tm, mess from history where dop2="+this.Tag.ToString()+" and mess like 'ИЗМЕНЕНИЕ БАЛАНСА:%' and doc='DRIVER' order by tm desc";

                this.btnEdit.Visible = false;
                this.btnAkts.Visible = false;
                this.btnOrganizationDetails.Visible = false;



                this.Text = "История изменения баланса водителя";

            }

            if (type == "DRIVER_PAYMENTS")
            {
                //фильтр
                //sql
                //колонки таблицы

                var panel_base = new TableLayoutPanel();
                //panel.FlowDirection = FlowDirection.TopDown;
                panel_base.Dock = DockStyle.Top;
                panel_base.RowCount = 1;
                panel_base.ColumnCount = 1;
                panel_base.AutoSize = true;
                //panel_base.BackColor = Color.Red;




                var panel = new TableLayoutPanel();
                //panel.FlowDirection = FlowDirection.TopDown;
                //panel.Dock = DockStyle.Top;
                //panel.RowCount = 2;
                panel.ColumnCount = 2;
                //panel.BackColor = Color.Azure;
                panel.AutoSize = true;
                panel_base.Controls.Add(panel);

                /////
                DataTable drivers = DataBase.mssqlRead("SELECT ID, cast(carid as varchar(10)) + ' '+ FIO name from DRIVERS order by carid");
                DataRow dr = drivers.NewRow();
                dr[0] = "-1";
                dr[1] = "<Все>";
                drivers.Rows.InsertAt(dr, 0);
                //organizations.Rows.InsertAt(new ("-1", "<Все>");

                this.Controls.Add(panel_base);

                var dt_from = new QueryVisualParam("С", "dt_from", QueryVisualParam.QueryVisualParamType.Calendar, new List<object>());
                panel.Controls.Add(dt_from);
                Fields.Add(dt_from);
                sqlParameters.Add(dt_from.param);
                var dt_to = new QueryVisualParam("по", "dt_to", QueryVisualParam.QueryVisualParamType.Calendar, new List<object>());
                panel.Controls.Add(dt_to);
                Fields.Add(dt_to);
                sqlParameters.Add(dt_to.param);

                var drv = new QueryVisualParam("Водитель", "driver_id", QueryVisualParam.QueryVisualParamType.DBDictionary, drivers);
                panel.Controls.Add(drv);
                Fields.Add(drv);
                sqlParameters.Add(drv.param);

                panel_base.Controls.Add(panel);


                drv.SetValue(this.Tag.ToString());
                dt_from.SetValue(DateTime.Today.AddMonths(-1));
                dt_to.SetValue(DateTime.Today.AddDays(1.0));
                ////


                Sql = "SELECT BALANCE_REG_ID ID, reg_desc, doc_date, -doc_summ from balance_reg where  "+
"((from_doc_id = @driver_id)  and (from_doc_typ='DRIVER')) and doc_date between @dt_from and @dt_to " +
"union "+
"SELECT BALANCE_REG_ID ID, reg_desc, doc_date, doc_summ from balance_reg where  "+
"((to_doc_id = @driver_id)  and (to_doc_typ='DRIVER'))  and doc_date between @dt_from and @dt_to " +
"order by 3 desc";


                this.btnEdit.Visible = false;
                
                this.btnAkts.Visible = false;
                this.btnOrganizationDetails.Visible = false;

                this.Text = "Список движений денег водителя";

            }

            /*
             * SELECT BALANCE_REG_ID ID, reg_desc, doc_date, -doc_summ from balance_reg where 
((from_doc_id = 1149)  and (from_doc_typ='DRIVER')) 
union
SELECT BALANCE_REG_ID ID, reg_desc, doc_date, doc_summ from balance_reg where 
((to_doc_id = 1149)  and (to_doc_typ='DRIVER'))  

order by 3
             */


            if (type == "BNAL_AKT") {
                //фильтр
                //sql
                //колонки таблицы

                var panel_base = new TableLayoutPanel();
                //panel.FlowDirection = FlowDirection.TopDown;
                panel_base.Dock = DockStyle.Top;
                panel_base.RowCount = 1;
                panel_base.ColumnCount = 1;
                panel_base.AutoSize = true;
                //panel_base.BackColor = Color.Red;




                var panel = new TableLayoutPanel();
                //panel.FlowDirection = FlowDirection.TopDown;
                //panel.Dock = DockStyle.Top;
                //panel.RowCount = 2;
                panel.ColumnCount = 2;
                //panel.BackColor = Color.Azure;
                panel.AutoSize = true;
                panel_base.Controls.Add(panel);

                /////
                DataTable organizations = DataBase.mssqlRead("SELECT ID, name from ORGANIZATIONS where AGREEMENTSTATUS=1");
                DataRow dr = organizations.NewRow();
                dr[0] = "-1";
                dr[1] = "<Все>";
                organizations.Rows.InsertAt(dr, 0);
                //organizations.Rows.InsertAt(new ("-1", "<Все>");

                this.Controls.Add(panel_base);

                var dt_from = new QueryVisualParam("С", "dt_from", QueryVisualParam.QueryVisualParamType.Calendar, new List<object>());
                panel.Controls.Add(dt_from);
                Fields.Add(dt_from);
                sqlParameters.Add(dt_from.param);
                var dt_to = new QueryVisualParam("по", "dt_to", QueryVisualParam.QueryVisualParamType.Calendar, new List<object>());
                panel.Controls.Add(dt_to);
                Fields.Add(dt_to);
                sqlParameters.Add(dt_to.param);

                var org = new QueryVisualParam("Организация", "org_id", QueryVisualParam.QueryVisualParamType.DBDictionary, organizations);
                panel.Controls.Add(org);
                Fields.Add(org);
                sqlParameters.Add(org.param);

                panel_base.Controls.Add(panel);


                org.SetValue(this.Tag.ToString());
                dt_from.SetValue(DateTime.Parse("01/01/2014"));
                dt_to.SetValue(DateTime.Today.AddDays(1.0));
                ////




                Sql = "SELECT ID, AKT_NUM, AKT_DATE, AKT_TEXT, AKT_SUMM, (SELECT name from ORGANIZATIONS where ORGANIZATIONS.id = ORGANIZATION_ID), ORGANIZATION_ID FROM BNAL_AKT where ((ORGANIZATION_ID = @org_id) or (@org_id = -1)) and akt_date between @dt_from and @dt_to order by akt_date desc";



                this.btnDelete.Visible = true;
                this.Text = "Список актов организации";

            }




            if (type == "REVIEW") {
                //фильтр
                //sql
                //колонки таблицы

                var panel_base = new TableLayoutPanel();
                //panel.FlowDirection = FlowDirection.TopDown;
                panel_base.Dock = DockStyle.Top;
                panel_base.RowCount = 1;
                panel_base.ColumnCount = 1;
                panel_base.AutoSize = true;



                var panel = new TableLayoutPanel();
                panel.ColumnCount = 2;

                panel.AutoSize = true;
                panel_base.Controls.Add(panel);

                

                Sql = "SELECT  TOP 1000 ID, CLN_FIO,  PHONE, MESSAGE, TM, CARID, CAR_DESCR, DRIVER_FIO, (SELECT OPERATORS.FIO FROM OPERATORS WHERE OPERATORS.ID = REVIEW.OPID) FROM REVIEW ORDER BY TM DESC";




                this.Text = "Отзывы";

            }

            if (type == "GIS_ADRESSES")
            {
                //фильтр
                //sql
                //колонки таблицы

                var panel_base = new TableLayoutPanel();
                //panel.FlowDirection = FlowDirection.TopDown;
                panel_base.Dock = DockStyle.Top;
                panel_base.RowCount = 1;
                panel_base.ColumnCount = 1;
                panel_base.AutoSize = true;



                var panel = new TableLayoutPanel();
                panel.ColumnCount = 2;

                panel.AutoSize = true;
                panel_base.Controls.Add(panel);



                DataTable names = DataBase.mssqlRead("SELECT distinct NAME from GIS_ADRESSES");
                DataTable names2 = DataBase.mssqlRead("SELECT distinct NAME from GIS_ADRESSES");
                DataTable streets = DataBase.mssqlRead("SELECT distinct STREET as NAME from GIS_ADRESSES");




                //                var t1 = new dictionary.QueryVisualParam("Позывной", "CARID", dictionary.QueryVisualParam.QueryVisualParamType.Numeric, new List<object>());
                //                panel.Controls.Add(t1); Fields.Add("ID", t1); sqlParameters.Add(t1.param);
                //                t1.Enabled = false;

                //    t1 = new dictionary.QueryVisualParam("Организация", "ORGANIZATIONID", dictionary.QueryVisualParam.QueryVisualParamType.DBDictionary, organizations);
                //    panel.Controls.Add(t1); Fields.Add("ORGANIZATIONID", t1); sqlParameters.Add(t1.param);

                //var t1 = new dictionary.QueryVisualParam("Наименование", "NAME", dictionary.QueryVisualParam.QueryVisualParamType.DBTextDictionary, names);
                //panel.Controls.Add(t1); Fields.Add("NAME", t1); sqlParameters.Add(t1.param);

                var street = new QueryVisualParam("Улица", "STREET", QueryVisualParam.QueryVisualParamType.DBTextDictionary, streets);
                panel.Controls.Add(street);
                Fields.Add(street);
                sqlParameters.Add(street.param);

/*

                

                var street = new QueryVisualParam("Улица", "STREET", QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(street);
                Fields.Add(street);
                sqlParameters.Add(street.param);
*/
                var name = new QueryVisualParam("Название", "NAME", QueryVisualParam.QueryVisualParamType.DBTextDictionary, names);
                panel.Controls.Add(name);
                Fields.Add(name);
                sqlParameters.Add(name.param);

                this.Controls.Add(panel_base);

                




                //like '%'+@phone+'%'"+
                Sql = "SELECT  ID, CITY,NAME, STREET, NUMBER FROM GIS_ADRESSES where ((@STREET='') or (STREET LIKE '%'+@STREET+'%')) "
                + "and  ((@NAME='') or (NAME LIKE '%'+@NAME+'%'))"
                +"ORDER BY STREET, NUMBER";

                //'%'+@phone+'%'"

                this.btnDelete.Visible = true;
                this.Text = "База адресов";

            }



            doLoad();


        }


        void btnPrintDrivers2_Click(object sender, EventArgs e) {

            String query = "SELECT fio, carid, null as photo, PHONES  FROM DRIVERS where state=0 $dop_sql_where$ order by carid";

            if (query.Contains("dop_sql_where"))
                query = query.Replace("$dop_sql_where$", dop_sql_where);


            DataTable dt = DataBase.mssqlRead(query);

            if (dt.Rows.Count <= 0)
                return;


            dataSet1.Tables["driver_card"].Clear();
            


            //object[] values = { (String)dt.Rows[0]["fio"], (int)dt.Rows[0]["carid"], (byte [])dt.Rows[0]["photo"] };
            for (int i = 0; i < dt.Rows.Count; i++) {
                String fio = (String)dt.Rows[i]["fio"];
                object[] values = { fio, (int)dt.Rows[i]["carid"], (object)dt.Rows[i]["photo"], (String)dt.Rows[i]["PHONES"] };
                dataSet1.Tables["driver_card"].LoadDataRow(values, true);
            }
            
            report2.Show();
        }


        void btnPrintDrivers3_Click(object sender, EventArgs e) {

            String query = "SELECT fio, carid, photo, PHONES, phone2, phone3, (select Color + ' '+mark+' '+gosnum+' '+descr from cars  where cars.id = drivers.carid) carname FROM DRIVERS where state=0 $dop_sql_where$ order by carid";

            if (query.Contains("dop_sql_where"))
                query = query.Replace("$dop_sql_where$", dop_sql_where);


            DataTable dt = DataBase.mssqlRead(query);

            if (dt.Rows.Count <= 0)
                return;


            dataSet1.Tables["driver_card"].Clear();



            //object[] values = { (String)dt.Rows[0]["fio"], (int)dt.Rows[0]["carid"], (byte [])dt.Rows[0]["photo"] };
            for (int i = 0; i < dt.Rows.Count; i++) {
                String fio = (String)dt.Rows[i]["fio"];
                object[] values = { fio, (int)dt.Rows[i]["carid"], (object)dt.Rows[i]["photo"], (String)dt.Rows[i]["PHONES"], (String)dt.Rows[i]["carname"], (String)dt.Rows[i]["phone2"], (String)dt.Rows[i]["phone3"] };
                dataSet1.Tables["driver_card"].LoadDataRow(values, true);
            }

            report3.Show();
        }

        void btnPrintDrivers_Click(object sender, EventArgs e) {

            String query = "SELECT fio, carid, photo, PHONES  FROM DRIVERS where state=0 $dop_sql_where$ order by carid";

            if (query.Contains("dop_sql_where"))
                query = query.Replace("$dop_sql_where$", dop_sql_where);


            DataTable dt = DataBase.mssqlRead(query);

            if (dt.Rows.Count <= 0)
                return;


            dataSet1.Tables["driver_card"].Clear();
            


            //object[] values = { (String)dt.Rows[0]["fio"], (int)dt.Rows[0]["carid"], (byte [])dt.Rows[0]["photo"] };
            for (int i = 0; i < dt.Rows.Count; i++) {
                String fio = (String)dt.Rows[i]["fio"];
                fio = fio.Replace(' ', '\n');
                object[] values = { fio, (int)dt.Rows[i]["carid"], (object)dt.Rows[i]["photo"], (String)dt.Rows[i]["PHONES"] };
                dataSet1.Tables["driver_card"].LoadDataRow(values, true);
            }
            
            report1.Show();
        }

        void where_TextChanged(object sender, EventArgs e) {
            dop_sql_where = ((TextBox)sender).Text;
        }

        public void doLoad(){
            int id = 0;
            int first_row = 0;
            int selected_row = 0;
            if (dataGridView1.SelectedRows.Count > 0)
            {
                first_row = dataGridView1.FirstDisplayedScrollingRowIndex;
                selected_row = dataGridView1.SelectedRows[0].Index;

                id = int.Parse((dataGridView1.Rows[selected_row].Cells["ID"].Value.ToString()));
            }


            String query = Sql.Replace("$TOP_REPLACE$", " TOP "+topNumber.Text);
            if (query.Contains("dop_sql_where"))
                query = query.Replace("$dop_sql_where$", dop_sql_where);
            

            DataTable dt = DataBase.mssqlRead(query, sqlParameters.ToArray());
            this.dataGridView1.DataSource = dt;

            dataGridView1.ClearSelection();

            int summ = 0;

            if (summ_value_index != -1)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    String s = dt.Rows[i][summ_value_index].ToString();
                    //s = s.Replace(',', '.');

                    int n = 0;
                    double d = 0;
                    double.TryParse(s, out d);
                    n = (int) d;

                    summ += n ;
                }
            }

            try
            {
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    if (int.Parse((dataGridView1.Rows[i].Cells["ID"].Value.ToString())) == id)
                    {

                        dataGridView1.Rows[i].Selected = true;

                        int idx = i - (selected_row - first_row);
                        if (idx >= 0)
                            dataGridView1.FirstDisplayedScrollingRowIndex = idx;
                        dataGridView1.Update();
                        break;
                    }


                }
            }
            catch (Exception e) { }
            

            toolStripStatusLabel1.Text = dt.Rows.Count.ToString();

            if (summ_value_index!=-1)
                toolStripStatusLabel1.Text += ", Сумма "+summ.ToString();

            //вывести сумму 
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            int isNumber = 0;
            e.Handled = !int.TryParse(e.KeyChar.ToString(), out isNumber);
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            doLoad();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dic_type == "ORGANIZATION_PAYMENTS")
                return;


            if (dic_type == "REVIEW") {

                int id = int.Parse((dataGridView1.Rows[dataGridView1.SelectedRows[0].Index].Cells["ID"].Value.ToString()));
                form_review form = new form_review();
                form.doLoad(id);
                form.Show();
                return;
            }

            int ms_start = DateTime.Now.Second * 1000 + DateTime.Now.Millisecond;
            if (dataGridView1.SelectedRows.Count != 1)
                return;

            if (dataGridView1.SelectedRows[0].Index >= 0)
            {

                int id = int.Parse((dataGridView1.Rows[dataGridView1.SelectedRows[0].Index].Cells["ID"].Value.ToString()));
                DictionaryElement de = new DictionaryElement();
                //de.doMakeInterface(dic_type);
                //de.Location = Cursor.Position;
                de.Show();
                de.Tag = this.Tag;
                de.doLoad(dic_type, id);
                

                
                
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            btnEdit_Click(null, null);
        }

        private void btnNewElement_Click(object sender, EventArgs e) {

            if (dic_type == "ORGANIZATION_PAYMENTS")
            {

                add_money form = new add_money();




                form.selected_org_id = (int)sqlParameters[2].Value;// (int)this.Tag;
                form.Show();
                return;
            }



            if (dic_type == "REVIEW") {
                form_review form = new form_review();
                form.Show();
                return;
            }
            DictionaryElement de = new DictionaryElement();
            //de.doMakeInterface(dic_type);

            de.Location = Cursor.Position;

            de.Show();
            de.Tag = this.Tag;
            de.doLoad(dic_type, 0);


            
        }

        private void button1_Click(object sender, EventArgs e) {

            if (dataGridView1.SelectedRows.Count != 1)
                return;

            if (dataGridView1.SelectedRows[0].Index >= 0) {

                int id = int.Parse((dataGridView1.Rows[dataGridView1.SelectedRows[0].Index].Cells["ID"].Value.ToString()));

                dictionary form = new dictionary();
                form.Tag = id;
                form.makeInterface("ORGANIZATION_DETAIL");
                form.ShowDialog(this);

            }


            
        }

        private void btnOrganizationPayments_Click(object sender, EventArgs e) {

            if (dataGridView1.SelectedRows.Count != 1)
                return;

            if (dataGridView1.SelectedRows[0].Index >= 0) {

                int id = int.Parse((dataGridView1.Rows[dataGridView1.SelectedRows[0].Index].Cells["ID"].Value.ToString()));

                dictionary form = new dictionary();
                form.Tag = id;
                form.summ_value_index = 3;
                form.Show();
                form.makeInterface("ORGANIZATION_PAYMENTS");
                
                //form.ShowDialog(this);


            }

        }

        private void btnAkts_Click(object sender, EventArgs e) {

            if (dataGridView1.SelectedRows.Count != 1)
                return;

            if (dataGridView1.SelectedRows[0].Index >= 0) {

                int id = int.Parse((dataGridView1.Rows[dataGridView1.SelectedRows[0].Index].Cells["ID"].Value.ToString()));

                dictionary form = new dictionary();
                form.Show();
                form.Tag = id;
                form.summ_value_index = 4;
                form.makeInterface("BNAL_AKT");
                

            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows[0].Index >= 0)
            {

                int id = int.Parse((dataGridView1.Rows[dataGridView1.SelectedRows[0].Index].Cells["ID"].Value.ToString()));
                
                if (dic_type.CompareTo("ORGANIZATION_PAYMENTS")==0){
                    if (MessageBox.Show("Удалить элемент?", "Запрос на удаление", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        DataBase.mssqlExecuteSQL("delete from bank where id = (select doc_id from balance_reg where balance_reg_id=" + id+")");
                        doLoad();
                    }
                }


                if (dic_type.CompareTo("BNAL_AKT") == 0)
                {
                    if (MessageBox.Show("Удалить элемент?", "Запрос на удаление", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        DataBase.mssqlExecuteSQL("delete from BNAL_AKT where id = " + id + "");
                        doLoad();
                    }
                }

                if (dic_type.CompareTo("GIS_ADRESSES") == 0)
                {
                    if (MessageBox.Show("Удалить элемент?", "Запрос на удаление", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        DataBase.mssqlExecuteSQL("delete from gis_adresses where id = " + id + "");
                        doLoad();
                    }
                }





            }
        }

        private void btn_oranization_subdetail_Click(object sender, EventArgs e)
        {

            if (dataGridView1.SelectedRows.Count != 1)
                return;

            if (dataGridView1.SelectedRows[0].Index >= 0)
            {

                int id = int.Parse((dataGridView1.Rows[dataGridView1.SelectedRows[0].Index].Cells["ID"].Value.ToString()));

                dictionary form = new dictionary();
                form.Tag = id;
                form.makeInterface("ORGANIZATION_SUBDETAIL");
                form.ShowDialog(this);

            }

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

    }
}
