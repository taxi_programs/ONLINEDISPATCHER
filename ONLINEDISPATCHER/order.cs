﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LumiSoft.Net.SIP.Stack;
using LumiSoft.Net.SDP;
using csmapcontrol;


namespace ONLINEDISPATCHER
{
    public partial class order : Form
    {
        int ms_start = 0;


        private int m_iID = 0;
        private int m_iServiceID = 0;

        private string m_sCallID = "";

        private static DataTable services = null;
        private static DataTable zones = null;
        private static DataTable zonesTo = null;

        private static DataTable organizations = null;
        private static DataTable organization_detail = null;

        private static DataTable adresses_names = DataBase.mssqlRead("select distinct name from adresses order by name");

        


        DataTable cars = DataBase.mssqlRead("SELECT ID, cast(ID as varchar(5)) + ' '+ color + ' ' + Mark + ' ' + GosNum as car_name from CARS order by cars.id");//where cars.status&1024 <> 1024 order by cars.ID");


        DataTable phone_data = new DataTable();


        private static Point lastLocation = new Point(100, 100);
        caller m_pSipCaller = null;


        private bool m_bLoading = false;


        public void setAsNewOrder()
        {
            m_iID = 0;
            
            CarID.SelectedItem = null;
            CarID.Text = "";
            this.CarIDView.Text = "";
            this.ZoneID.Enabled = true;
            CarID.Visible = true;

            this.ZoneID.Enabled = true;
            this.AddrFrom.Enabled = true;
            this.HouseFrom.Enabled = true;
            this.AddrToForDriver.Enabled = true;
            this.AddrTo.Enabled = true;
            this.HouseTo2.Enabled = true;
            this.AddrFromName.Enabled = true;
            this.AddrToName.Enabled = true;
            this.FlatFrom.Enabled = true;
            this.ConnectPhone.Enabled = true;
            this.Phone.Enabled = true;
            
                

        }


        private void addFocusColorChangeHandler() {


            foreach (Control c in this.Controls) {
                //if (c.TabStop == true) 
                {
                    
                    c.Enter += new System.EventHandler(this.ComponentEnter);
                    c.Leave += new System.EventHandler(this.ComponentLeave);
                }
            }
                
        
            
        }

        public order(SipCaller caller)
        {
            InitializeComponent();

            addFocusColorChangeHandler();

            m_pSipCaller = caller;
            this.btnEndCall.Visible = true;

            if (order.lastLocation != null)
            {
                this.StartPosition = FormStartPosition.Manual;
                this.Location = order.lastLocation;
            }
        }

        public order()
        {
            ms_start = DateTime.Now.Second * 1000 + DateTime.Now.Millisecond;

            InitializeComponent();
            addFocusColorChangeHandler();
            if (order.lastLocation != null){
                this.StartPosition = FormStartPosition.Manual;
                this.Location = order.lastLocation;
            }
        }

        public void doLoad( int order_id, String phone, String from_phone, String call_id )
        {
            //загрузка постоянных справочников заказа

            m_bLoading = true;

            {
                if (services == null)
                    services = DataBase.mssqlRead("SELECT * FROM SERVICES where ID>0");
                if (zones == null)
                    zones = DataBase.mssqlRead("SELECT * FROM ZONES order by descr");
                if (zonesTo == null)
                    zonesTo = DataBase.mssqlRead("SELECT * FROM ZONES order by descr");

                if (order_prim_menu.Items.Count==0)
                {
                    DataTable dt0 = DataBase.mssqlRead("SELECT VALUE1 FROM SETTINGS WHERE SETT_NAME='ORDER_PRIM_STRINGS' ORDER BY VALUE1");
                    foreach (DataRow row in dt0.Rows)
                    {
                        order_prim_menu.Items.Add(row["VALUE1"].ToString()).Click += new EventHandler(order_prim_menu_click);

                    }

                }


                
                if (organizations == null)
                    organizations = DataBase.mssqlRead("SELECT * FROM ORGANIZATIONS WHERE AGREEMENTSTATUS=1 order by name");
                if (organization_detail == null)
                    organization_detail = DataBase.mssqlRead("SELECT id, ORGANIZATIONID, name, moneylimit, name +' код:'+password+' лимит:'+cast(moneylimit as varchar(10))+'р.'+' время: '+timelimit descr FROM ORGANIZATION_DETAIL order by name");
      
            }





            dtPredvar.Value = DateTime.Now;
            tmPredvar.Value = DateTime.Now;
            tmPredvar.Value.AddSeconds(-tmPredvar.Value.Second);

            predvShowDelta.SelectedIndex = 0;

            m_sCallID = call_id;

            AddrFromName.Tag = "BLOCK_CHANGE_TEXT_EVENT";
            AddrFrom.Tag = "BLOCK_CHANGE_TEXT_EVENT";

            FromPhone.Text = from_phone;


            //загрузить стпавочники
            //загрузить данные по заказу

            //справочники: службы, стоянки.
            //при открытии комбобоксов - загружать содержимое: места, организации, автомобили

            if (cars.Rows.Count > 0)
            {
                CarID.DataSource = cars;
                CarID.DisplayMember = "car_name";
                CarID.ValueMember = "ID";
                CarID.SelectedItem = null;
                //CarID.SelectedValue = carid;
            }



            this.OrgID.DataSource = organizations;
            OrgID.DisplayMember = "Name";
            OrgID.ValueMember = "ID";

            OrgID.AutoCompleteSource = AutoCompleteSource.ListItems;
            OrgID.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            OrgID.SelectedIndex = -1;




            //    ServiceID.Items.Clear();
            ServiceID.DataSource = services;
            ServiceID.DisplayMember = "Name";
            ServiceID.ValueMember = "ID";

            DataRow[] services_for_line = services.Select("PhoneList='" + FromPhone.Text + "'");
            if (services_for_line.Length > 0)
                ServiceID.SelectedValue = services_for_line[0]["ID"];

            ServiceID.DropDownHeight = ServiceID.ItemHeight * ((DataTable)ServiceID.DataSource).Rows.Count;


            ZoneID.DataSource = zones;
            ZoneID.DisplayMember = "Descr";
            ZoneID.ValueMember = "ID";

            ZoneID.AutoCompleteSource = AutoCompleteSource.ListItems;
            ZoneID.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            ZoneID.SelectedIndex = -1;

            ZoneID.DropDownHeight = ZoneID.ItemHeight * ((DataTable)ZoneID.DataSource).Rows.Count;


            RaionToID.DataSource = zonesTo;
            RaionToID.DisplayMember = "Descr";
            RaionToID.ValueMember = "ID";

            RaionToID.AutoCompleteSource = AutoCompleteSource.ListItems;
            RaionToID.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            RaionToID.SelectedIndex = -1;

            RaionToID.DropDownHeight = RaionToID.ItemHeight * ((DataTable)RaionToID.DataSource).Rows.Count;



            doFillAddrFromName();


            AddrToForDriver.AutoCompleteMode = AutoCompleteMode.None;
            AddrToForDriver.AutoCompleteCustomSource.Clear();
            foreach (DataRow row in zones.Rows)
            {
                AddrToForDriver.Items.Add(row["Descr"].ToString());

            }
            AddrToForDriver.AutoCompleteSource = AutoCompleteSource.ListItems;
            AddrToForDriver.AutoCompleteMode = AutoCompleteMode.SuggestAppend;

            AddrToForDriver.DropDownHeight = AddrToForDriver.ItemHeight * AddrToForDriver.Items.Count;


            if (order_id == 0)
            {
                //AddrFrom.Tag = "";
             //   doFindAddrFromName();
                //AddrFrom.Tag = "BLOCK_CHANGE_TEXT_EVENT";

                
            }


            if (order_id > 0)
            {
                DataTable ord_data = DataBase.mssqlRead("SELECT * FROM ORDERS WHERE ID=" + order_id.ToString());
                m_iID = ((int)ord_data.Rows[0]["ID"]);


                String time2meet = ((int)ord_data.Rows[0]["time2meet"]).ToString();

                if (time2meet == "1")
                    time2meet = " (5 мин)";
                if (time2meet == "2")
                    time2meet = " (10 мин)";
                if (time2meet == "3")
                    time2meet = " (15 мин)";


                if (ord_data.Rows[0]["predvShowDelta"] != DBNull.Value) {
                    int predvShowDeltaValue = ((int)ord_data.Rows[0]["predvShowDelta"]);
                    if (predvShowDeltaValue == 20)
                        predvShowDelta.SelectedIndex = 0;
                    if (predvShowDeltaValue == 30)
                        predvShowDelta.SelectedIndex = 1;
                    if (predvShowDeltaValue == 40)
                        predvShowDelta.SelectedIndex = 2;
                }


                this.dt_order_info.Text = "Поступил:    "+ord_data.Rows[0]["dtArrive"].ToString();
                dt_order_info.Text += "\r\n"+"Начат:         " + ord_data.Rows[0]["dtBegin"].ToString();
                dt_order_info.Text += "\r\n" + "Выполнен: " + ord_data.Rows[0]["dtEnd"].ToString();


                this.FromPhone.Text = ((String)ord_data.Rows[0]["FromPhone"]);

                this.Phone.Text = ((String)ord_data.Rows[0]["Phone"]);
                this.ConnectPhone.Text = ((String)ord_data.Rows[0]["ConnectPhone"]);
                this.AddrFrom.Text = ((String)ord_data.Rows[0]["AddrFrom"]);

                this.HouseFrom.Text = ((String)ord_data.Rows[0]["HouseFrom"]);
                this.FlatFrom.Text = ((String)ord_data.Rows[0]["FlatFrom"]);
                this.AddrFromName.Text = ((String)ord_data.Rows[0]["AddrFromName"]);
                this.AddrToForDriver.Text = ((String)ord_data.Rows[0]["AddrTo"]);
                if (ord_data.Rows[0]["AddrTo2"] != DBNull.Value)
                    this.AddrTo.Text = ((String)ord_data.Rows[0]["AddrTo2"]);
                if (ord_data.Rows[0]["DistanceCity"] != DBNull.Value) {
                    this.расчетМаршрута.Tag = ((double)ord_data.Rows[0]["DistanceCity"]);
                    this.расчетМаршрута.Text = ((double)this.расчетМаршрута.Tag).ToString(".00") + " км";


                    DistanceMoney.Tag = (((Double)расчетМаршрута.Tag) * 10 + 80);
                    DistanceMoney.Text = ((Double)DistanceMoney.Tag).ToString("0.") + " руб";
                }


                if (ord_data.Rows[0]["AddrToHouse2"] != DBNull.Value)
                    this.HouseTo2.Text = ((String)ord_data.Rows[0]["AddrToHouse2"]);
                this.AddrToName.Text = ((String)ord_data.Rows[0]["AddrToName"]);
                this.Descr.Text = ((String)ord_data.Rows[0]["Descr"]);
                this.ClientID.Text = ((String)ord_data.Rows[0]["ClientName"]);
                this.summ.Text = ord_data.Rows[0]["BnalMoney"].ToString();
                this.TalonNumber.Text = (ord_data.Rows[0]["TalonNumber"]).ToString();

                this.ClientID.Tag = null;
                if (ord_data.Rows[0]["ClientId"] != DBNull.Value)
                    this.ClientID.Tag = ((int)ord_data.Rows[0]["ClientId"]);

                if (ord_data.Rows[0]["predvar"] != DBNull.Value)
                    this.chkPredvar.Checked = Convert.ToBoolean(ord_data.Rows[0]["predvar"]);

                if (ord_data.Rows[0]["dtPredvar"] != DBNull.Value)
                {
                    this.dtPredvar.Value = ((DateTime)ord_data.Rows[0]["dtPredvar"]);
                    this.tmPredvar.Value = ((DateTime)ord_data.Rows[0]["dtPredvar"]);
                }

                if (ord_data.Rows[0]["ServiceID"].ToString() != "")
                {
                    ServiceID.SelectedValue = ord_data.Rows[0]["ServiceID"].ToString();
                    m_iServiceID = (int)ord_data.Rows[0]["ServiceID"];
                }

                if (ord_data.Rows[0]["ZoneID"].ToString() != "")
                {
                    ZoneID.SelectedValue = ord_data.Rows[0]["ZoneID"].ToString();
                }

                if (ord_data.Rows[0]["Raionto"].ToString() != "")
                {
                    RaionToID.SelectedValue = ord_data.Rows[0]["Raionto"].ToString();
                }

                if (ord_data.Rows[0]["OrgID"].ToString() != "")
                {
                    OrgID.SelectedValue = ord_data.Rows[0]["OrgID"].ToString();
                }

                if (ord_data.Rows[0]["OrganizationDetailID"].ToString() != "")
                {
                    ORGDETAIL_ID.SelectedValue = ord_data.Rows[0]["OrganizationDetailID"].ToString();
                }

                int carid = int.Parse("0" + ord_data.Rows[0]["CarID"].ToString());
                if (carid > 0)
                {
                    //DataTable dt = DataBase.mssqlRead("SELECT ID, cast(ID as varchar(5)) + ' '+ color + ' ' + Mark + ' ' + GosNum as car_name from CARS where ID=" + carid);
                    //if (dt.Rows.Count > 0)
                    {
                        //                        CarID.DataSource = dt;
                        //                        CarID.DisplayMember = "car_name";
                        //CarID.ValueMember = "ID";
                        CarID.SelectedValue = carid;
                        ServiceID.Enabled = false;
                        CarID.BackColor = Color.Red;
                        CarID.ForeColor = Color.Red;
                        this.ZoneID.Enabled = false;
                        this.RaionToID.Enabled = false;
                    }


                }
                this.ZoneID.Enabled = false;
                this.RaionToID.Enabled = false;
                this.AddrFrom.Enabled = false;
                this.HouseFrom.Enabled = false;
                this.AddrToForDriver.Enabled = false;
                this.AddrTo.Enabled = false;
                this.HouseTo2.Enabled = false;
                this.AddrFromName.Enabled = false;
                this.AddrToName.Enabled = false;
                this.FlatFrom.Enabled = false;
                //this.ConnectPhone.Enabled = false;
                

                //после загрузки заказа - закрыть поля от редактирования
                //машина, телефон, 
                CarID.Visible = false;
                CarIDView.Text = CarID.Text + time2meet;
                Phone.ReadOnly = true;


                DataTable last_call_data = DataBase.mssqlRead("select status,  last_call, getdate() as now_dt from call_out where call_out.order_id=" + order_id.ToString());
                if (last_call_data.Rows.Count > 0)
                {

                    String delta = ((DateTime)last_call_data.Rows[0]["now_dt"]).Subtract((DateTime)last_call_data.Rows[0]["last_call"]).TotalMinutes.ToString("#.0");

                    dt_order_info.Text += "\r\n" + "Дозвон:      ";
                    if (last_call_data.Rows[0]["status"].ToString() == "f")
                        dt_order_info.Text += "НЕДОЗВОН";
                    dt_order_info.Text += " " + last_call_data.Rows[0]["last_call"] + " :  " + delta + " минут назад"; 
                }


            }

            if (m_iID == 0)
            if (phone != "")
            {
                this.Phone.Text = phone;
                this.ServiceID.Text = from_phone;
                getPhoneInfo();
                doFindAddrFromName();

                this.AddrFrom.SelectAll();

                this.AddrFrom.Focus();
                


            }

            //загрузить инфо по заказу. если новый - то заполнить поля.
            {
                String[] phoneInfo = caller.getPhoneInfo(this.Phone.Text);

                phone_info.Text = phoneInfo[2] + " заказов за 30 дней. " + phoneInfo[3];
                if ((phoneInfo[4] != "") && (phoneInfo[4] != null)) //телефон есть в списке
                {
                    String[] phone_addr_data = phoneInfo[4].Split('|');

                    /// cast(state as varchar(10))+'|'+FIO+'|'+address+'|'+houseNum+'|'+FlatNum+'|'+Descr
                    if (phone_addr_data.Length == 6)
                    {
                        String state = phone_addr_data[0];
                        String FIO = phone_addr_data[1];
                        String address = phone_addr_data[2];
                        String houseFrom = phone_addr_data[3];
                        String flatFrom = phone_addr_data[4];
                        String descr = phone_addr_data[5];

                        
                        if (state == "1")
                        {
                            this.Phone.BackColor = Color.Black;
                            this.Phone.ForeColor = Color.White;
                        }
                        if (state == "2")
                        {
                            this.Phone.BackColor = Color.Gray;
                        }
                        if (state == "3")
                        {
                            this.Phone.BackColor = Color.Yellow;

                            //Здесь можно сохранить поле VIP - тогда будет выделяться жирным и показываться только 1 заказ

                        }
                        if (this.m_iID == 0)
                        {

                            this.HouseFrom.Text = houseFrom;
                            this.FlatFrom.Text = flatFrom;
                            this.AddrFrom.Text = address;
                            this.Descr.Text = descr;
                        }

                    }
                }

                

            

                //инфо по телефону - из Phones : адрес имя клиента id клиента
                //несколько записей - выпадает меню с выбором



            }

            AddrFromName.Tag = 0;
            AddrFrom.Tag = 0;

            m_bLoading = false;


            System.Console.WriteLine("Открытие заказа" + (DateTime.Now.Second * 1000 + DateTime.Now.Millisecond - ms_start).ToString() + "мсек");

        }

        void order_prim_menu_click(object sender, EventArgs e)
        {
            this.Descr.Text = sender.ToString()+ " " + Descr.Text;
            //throw new NotImplementedException();
        }

        private void getPhoneInfo()
        {
            /* поиск по номеру телефона - адрес, клиент, ч-б-серый список
             */
            phone_data = DataBase.mssqlRead("SELECT * FROM PHONES WHERE phone='" + this.Phone.Text+"'");
            if (phone_data.Rows.Count > 0)
            {
                this.AddrFrom.Text = phone_data.Rows[0]["Address"].ToString();
                this.HouseFrom.Text = phone_data.Rows[0]["HouseNum"].ToString();
                this.FlatFrom.Text = phone_data.Rows[0]["FlatNum"].ToString();
                this.ClientID.Text = phone_data.Rows[0]["FIO"].ToString();
                this.ClientID.Tag = int.Parse(phone_data.Rows[0]["ID"].ToString());
                this.Descr.Text = phone_data.Rows[0]["Descr"].ToString();
                String cln_fio = phone_data.Rows[0]["FIO"].ToString();


                if (phone_data.Rows.Count > 1)//несколько записей - заполнить адрес из первого и показать меню выбора
                {

                    lbPhoneInfo.Items.Clear();
                    
                    foreach (DataRow dr in phone_data.Rows){

                        String line = dr["FIO"].ToString() + " " +
                            dr["Address"].ToString() + " " +
                            dr["HouseNum"].ToString() + " " +
                            dr["FlatNum"].ToString() + " " +
                            dr["Descr"].ToString();
                        lbPhoneInfo.Items.Add(line);
                    }
                    lbPhoneInfo.Height = lbPhoneInfo.ItemHeight * (lbPhoneInfo.Items.Count+1);
                    lbPhoneInfo.Visible = true;

                }

            }
        }

        public int doSave()
        {
            //1. проверить валидность введенных данных
            //2. записать в базу
            //3. разговор прервать
            //4. окно закрыть.

            try {
                if (this.Phone.Text.Length==0)
                {
                    MessageBox.Show("Не указан телефон");
                    return 0;
                }
                if (this.Phone.Text=="anonymous")
                {
                    MessageBox.Show("Не указан телефон");
                    return 0;
                }

                if (this.ServiceID.SelectedIndex == -1) {

                    MessageBox.Show("Не указана  служба заказа");
                    return 0;
                }
                if (this.ZoneID.SelectedIndex == -1) {

                    MessageBox.Show("Не указана стоянка 'Откуда'");
                    return 0;
                }

                if (this.AddrToForDriver.Text.Length == 0) {

                    MessageBox.Show("Не указано 'Куда'");
                    return 0;
                }


                if (this.m_iID > 0)
                    if (this.m_iServiceID != (int)this.ServiceID.SelectedValue) {
                        if (MessageBox.Show("Изменена служба. отменить старый заказ и создать новый?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1) == DialogResult.Yes) {//1 - узнать как закрыть старый заказ
                            //2 - id = -1;
                            //insert COMMANDS(ComID, OpID, dtArrive, Value1, Value2, State) 
                            //--	values(4,129,getdate(),653266,0,0)
                            /*
                                                String[] fields = { "state","resultcode", "dtEnd"}; 
                                                object [] values = { "3","1", DateTime.Now};
                    
                                                DataBase.mssqlUpdate("ORDERS", fields, values, "ID", m_iID); 
                             * 
                             * String[] fields1 = { "DescrForClient"};
                                            object[] values1 = { "снят дублированием"};

                                            DataBase.mssqlUpdate("ORDERS", fields1, values1, "ID", id);
                             */

                            String[] fields1 = { "DescrForClient" };
                            object[] values1 = { "снят дублированием со сменой зоны" };

                            DataBase.mssqlUpdate("ORDERS", fields1, values1, "ID", m_iID);


                            String[] fields = { "ComID", "OpID", "dtArrive", "Value1", "Value2", "State" };
                            object[] values = { 4, MAIN_FORM.OperatorID, DateTime.Now, m_iID, 3, 0 };

                            DataBase.mssqlInsert("COMMANDS", fields, values);


                            setAsNewOrder();
                        }



                    }




                int state = 0;
                if (chkPredvar.Checked)
                    state = 5;
                int agreement = 0;
                if (OrgID.SelectedValue != null)
                    agreement = 1;


                DataRow[] services_for_id = services.Select("ID='" + ServiceID.SelectedValue + "'");
                String request_attributes = "";

                if (services_for_id.Length > 0)
                    request_attributes = (String)services_for_id[0]["PhoneList"];

                if (this.m_iID > 0) {
                    String[] fields = { "Phone", "ConnectPhone", "AddrFrom", "HouseFrom", "FlatFrom",
                                      "AddrFromName","AddrTo","AddrToHouse2","AddrToName","Descr",
                                  "ServiceID","ZoneID",
                                  "ClientName", "clientId",  
                                  "BnalMoney","TalonNumber", "skidka", "DescrForClient", "predvar", "dtPredvar",
                                  "request_attributes", "raionto", "OrgID", "OrganizationDetailID", "Agreement", "AddrTo2", "DistanceCity", "DistanceMoney",
                                  "predvShowDelta"};



                    object[] values = {Phone.Text, ConnectPhone.Text, AddrFrom.Text, HouseFrom.Text, FlatFrom.Text,
                                      AddrFromName.Text,AddrToForDriver.Text,HouseTo2.Text,AddrToName.Text,Descr.Text,
                                  ServiceID.SelectedValue,ZoneID.SelectedValue,
                                  ClientID.Text, ClientID.Tag, 
                                  summ.Text, TalonNumber.Text, 0, "", Convert.ToInt32(chkPredvar.Checked), dtPredvar.Value.Date.Add(tmPredvar.Value.TimeOfDay),
                                  request_attributes, RaionToID.SelectedValue, OrgID.SelectedValue, ORGDETAIL_ID.SelectedValue, agreement, AddrTo.Text, расчетМаршрута.Tag, DistanceMoney.Tag,
                                  int.Parse(predvShowDelta.Text)};

                    //DataBase.fbInsert("LOG", fields, values);
                    DataBase.mssqlUpdate("ORDERS", fields, values, "ID", m_iID);

                }
                if (this.m_iID == 0) {   //новый заказ 
                    String[] fields = { "Phone", "ConnectPhone", "AddrFrom", "HouseFrom", "FlatFrom",
                                      "AddrFromName","AddrTo","AddrToHouse2","AddrToName","Descr",
                                  "ServiceID","CarID","ZoneID",
                                  "ClientName", "clientId", "opId", "dtArrive", 
                                  "BnalMoney","TalonNumber", "skidka", "DescrForClient", "predvar", "dtPredvar",
                                  "state", "agreement", "resultCode", 
                                  "fromPhone", "request_attributes", "raionto", "OrgID", "OrganizationDetailID", "AddrTo2", "DistanceCity", "DistanceMoney",
                                  "predvShowDelta"};



                    object[] values = {Phone.Text, ConnectPhone.Text, AddrFrom.Text, HouseFrom.Text, FlatFrom.Text,
                                      AddrFromName.Text,AddrToForDriver.Text,HouseTo2.Text,AddrToName.Text,Descr.Text,
                                  ServiceID.SelectedValue,CarID.SelectedValue, ZoneID.SelectedValue,
                                  ClientID.Text, ClientID.Tag, MAIN_FORM.OperatorID, DateTime.Now, 
                                  summ.Text, TalonNumber.Text, 0, "", Convert.ToInt32(chkPredvar.Checked), dtPredvar.Value.Date.Add(tmPredvar.Value.TimeOfDay),
                                  state, agreement, 0,
                                  FromPhone.Text, request_attributes, RaionToID.SelectedValue, OrgID.SelectedValue, ORGDETAIL_ID.SelectedValue, AddrTo.Text, расчетМаршрута.Tag, DistanceMoney.Tag,
                                  int.Parse(predvShowDelta.Text)};

                    //DataBase.fbInsert("LOG", fields, values);
                    DataBase.mssqlInsert("ORDERS", fields, values);
                }

            } catch (Exception e) {
                MessageBox.Show(e.Message);
                return 0;
            }
            return 1;
            /*искать клиента по телефону
             * искать адрес по телефону
             * 
             */
            
        }

        private void order_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "tAXADataSet.SERVICES". При необходимости она может быть перемещена или удалена.

            

        }

        private void order_FormClosing(object sender, FormClosingEventArgs e)
        {

            order.lastLocation = this.Location;
            /*
            if (this.m_pSipCaller != null)
            {
                MessageBox.Show("Закрыть заказ при активном звонке"
            }
            
            order.lastLocation = this.Location;
            if (this.m_pSipCaller != null) {
                m_pSipCaller.doHangup();
                m_pSipCaller.doStartPostCallTimer();
            }
             **/
            //to do сделать задержку на прием нового звонка 5 секунды
            
            //убрал автосброс звонка при закрытии формы заказа....
            if (this.m_pSipCaller != null)
                    m_pSipCaller.doStartPostCallTimer();


        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //при сохранении
            //Если изменилась служба  - старый заказ закрыть, этот сохранить как новый.
            //при назначенной машине - службу нельзя менять.

            if (doSave() > 0)
                this.Close();

        }

        private void AddrFromName_Click(object sender, EventArgs e)
        {

            doFillAddrFromName();
            //теперь надо найти такое название в соответствии с адресом

//            doFindAddrFrom();

        }

        private void doFillAddrFromName()
        {
            if (AddrFromName.DataSource == null)
            {
                //DataTable adresses_names = DataBase.mssqlRead("select distinct name from adresses");
                AddrFromName.DataSource = adresses_names;
                AddrFromName.DisplayMember = "name";
                AddrFromName.ValueMember = "name";

                AddrFromName.AutoCompleteSource = AutoCompleteSource.ListItems;
                AddrFromName.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            }


        }

        

        private void AddrFrom_Click(object sender, EventArgs e)
        {
            
            doFillAddrFrom();
        }
        private void AddrFrom_Enter(object sender, EventArgs e)
        {
            
            doFillAddrFrom();
        }

        private void ComponentEnter(object sender, EventArgs e)
        {
            if (sender.GetType().Name == "TextBox")
            {
                ((TextBox)sender).BackColor = Color.LightSkyBlue;
            }

            if (sender.GetType().Name == "ComboBox") {
                ((ComboBox)sender).BackColor = Color.LightSkyBlue;
            }

            
            if (sender.GetType().Name == "Button") {
                ((Button)sender).BackColor = Color.LightSkyBlue;
            }

        }

        private void ComponentLeave(object sender, EventArgs e) {
            if (sender.GetType().Name == "TextBox") {
                ((TextBox)sender).BackColor = Color.White;
            }
            if (sender.GetType().Name == "ComboBox") {
                ((ComboBox)sender).BackColor = Color.White;
            }


            if (sender.GetType().Name == "Button") {
                ((Button)sender).BackColor = btnEndOrder.BackColor;
            }
        }

        private void doFillAddrFrom()
        {
            if (AddrFrom.AutoCompleteCustomSource.Count == 0)
            {
                DataTable adresses_address = DataBase.mssqlRead("select distinct address from adresses WHERE state=0");


                AddrFrom.AutoCompleteMode = AutoCompleteMode.None;
                AddrFrom.AutoCompleteCustomSource.Clear();




                foreach (DataRow row in adresses_address.Rows)
                {
                    AddrFrom.AutoCompleteCustomSource.Add(row[0].ToString());

                }

                AddrFrom.AutoCompleteSource = AutoCompleteSource.CustomSource;
                AddrFrom.AutoCompleteMode = AutoCompleteMode.SuggestAppend;


                AddrTo.AutoCompleteMode = AutoCompleteMode.None;
                AddrTo.AutoCompleteCustomSource.Clear();



                foreach (DataRow row in adresses_address.Rows)
                {
                    AddrTo.AutoCompleteCustomSource.Add(row[0].ToString());

                }

                AddrTo.AutoCompleteSource = AutoCompleteSource.CustomSource;
                AddrTo.AutoCompleteMode = AutoCompleteMode.SuggestAppend;




            }

        }


        private void HouseFrom_Click(object sender, EventArgs e)
        {
            doFillHouseFrom();
        }
        private void HouseFrom_Enter(object sender, EventArgs e)
        {
            doFillHouseFrom();
        }

        private void doFillHouseFrom()
        {
            return;
            if (AddrFrom.Text != "")
            {
                if (AddrFrom.Text != HouseFrom.Tag)
                {
                    DataTable adresses_house = DataBase.mssqlRead("select distinct HouseNum from adresses where state=1 and Address='" + AddrFrom.Text.Replace("'", "") + "'");




                    HouseFrom.AutoCompleteMode = AutoCompleteMode.None;
                    HouseFrom.AutoCompleteCustomSource.Clear();


                    foreach (DataRow row in adresses_house.Rows)
                    {
                        HouseFrom.AutoCompleteCustomSource.Add(row[0].ToString());
                    }

                    HouseFrom.AutoCompleteSource = AutoCompleteSource.CustomSource;
                    HouseFrom.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    HouseFrom.Tag = AddrFrom.Text;
                }
            }
        }


        private void AddrFrom_TextChanged(object sender, EventArgs e)
        {
            //doFindAddrFromName();
            if (m_bLoading == false) {
                ZoneID.SelectedIndex = -1;
                ZoneID.SelectedItem = null;
            }
        }
        
        private void HouseFrom_TextChanged(object sender, EventArgs e)
        {
            if (m_bLoading == false) {
                ZoneID.SelectedValue = 0;
                ZoneID.SelectedItem = null;
                //ZoneID.SelectedValue = 0;
            }
            //doFindAddrFromName();
        }

        



        //ищет, какое наименование соответствует введенному адресу.
        //
        private void doFindAddrFromName()
        {


            //РЅР°РґРѕ РёСЃРєР°С‚СЊ Р°РґСЂРµСЃ РЅРµ С‚РѕР»СЊРєРѕ РµСЃР»Рё СЃРјРµРЅРёР»СЃСЏ С‚РµРєСЃС‚ РІ РїРѕР»СЏС… Р°РґСЂРµСЃР°
            //if (AddrFrom.Tag != "BLOCK_CHANGE_TEXT_EVENT")
            if (ZoneID.SelectedItem == null)
            {
                if (AddrFrom.Text != "")
                {


                    DataTable adresses_names = DataBase.mssqlRead("select name, zoneID from adresses " +
                      " where Address='" + AddrFrom.Text.Replace("'", "") + "'" +
                      " and housenum='" + HouseFrom.Text.Replace("'", "") + "'");

                    if (adresses_names.Rows.Count > 0)
                    {














                        AddrFromName.Tag = "BLOCK_CHANGE_TEXT_EVENT";
                        if (AddrFromName.Text != adresses_names.Rows[0][0].ToString())
                            AddrFromName.Text = adresses_names.Rows[0][0].ToString();
                        if (ZoneID.Enabled)
                        {


                            //if (adresses_names.Rows[0][1]
                            if (adresses_names.Rows[0][1] == DBNull.Value)
                            {
                                ZoneID.SelectedIndex = -1;
                                ZoneID.SelectedItem = null;
                                ZoneID.SelectedIndex = -1;
                            }
                            else
                                if (ZoneID.SelectedValue != adresses_names.Rows[0][1])
                                    ZoneID.SelectedValue = adresses_names.Rows[0][1];










                        }
                        AddrFromName.Tag = 0;
                    }
                    else
                    {
                        if (ZoneID.Enabled)
                        {
                            ZoneID.SelectedIndex = -1;
                            ZoneID.SelectedItem = null;
                            ZoneID.SelectedIndex = -1;
                        }
                    }
                }
            }
        }


        private void doFindAddrTo()
        {
            //if (AddrFrom.Tag != "BLOCK_CHANGE_TEXT_EVENT")
            {
                if (AddrToForDriver.Text != "")
                {
                    DataTable adresses_names = DataBase.mssqlRead("select name, adresses.zoneID, zones.descr from adresses left join zones on adresses.zoneid = zones.id " +
                      " where Address='" + AddrToForDriver.Text.Replace("'", "") + "'" + "");


                    //        " and housenum='" + HouseTo2.Text.Replace("'", "") + "'");

                    if (adresses_names.Rows.Count > 0)
                    {






                        AddrToName.Tag = "BLOCK_CHANGE_TEXT_EVENT";
                        if (AddrToName.Text != adresses_names.Rows[0][0].ToString())
                            AddrToName.Text = adresses_names.Rows[0][0].ToString();
                        //                        if (AddrToForDriver.Enabled)
                        //                            AddrToForDriver.Text = adresses_names.Rows[0][2].ToString();
                        if (RaionToID.Enabled)
                        {
                            if (RaionToID.SelectedValue != adresses_names.Rows[0][1])
                                RaionToID.SelectedValue = adresses_names.Rows[0][1];
                        }

                        AddrToName.Tag = 0;

                    }
                }
            }
        }



        private void doFindAddrFrom()
        {
            if (AddrFromName.Tag != "BLOCK_CHANGE_TEXT_EVENT")
            {
                if (AddrFromName.Text != "")
                {

                    DataTable dd = DataBase.mssqlRead("select address, housenum, zoneID from adresses " +
                     " where Name='" + AddrFromName.Text.Replace("'", "") + "'");

                    if (dd.Rows.Count > 0)
                    {
                        AddrFrom.Tag = "BLOCK_CHANGE_TEXT_EVENT";
                        AddrFrom.Text = dd.Rows[0][0].ToString();
                        HouseFrom.Text = dd.Rows[0][1].ToString();

                        ZoneID.SelectedValue = dd.Rows[0][2].ToString();

                        AddrFrom.Tag = 0;
                       
                    }
                }
            }
        }

        private void AddrFromName_SelectedValueChanged(object sender, EventArgs e)
        {
            if (m_bLoading == false) {
                doFindAddrFrom();
            }
        }

        private void chkPredvar_CheckStateChanged(object sender, EventArgs e)
        {
            dtPredvar.Visible = chkPredvar.Checked;
            tmPredvar.Visible = chkPredvar.Checked;

            predvShowDelta.Visible = chkPredvar.Checked;

        }

        private void btnPhoneInfo_Click(object sender, EventArgs e)
        {
            getPhoneInfo();
        }

        private void lbPhoneInfo_Leave(object sender, EventArgs e)
        {
            lbPhoneInfo.Visible = false;
        }

        private void order_MouseDown(object sender, MouseEventArgs e)
        {
            if (lbPhoneInfo.Visible)
                lbPhoneInfo.Visible = false;
        }

        private void lbPhoneInfo_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //выбран адрес - прописать его в поля заказа
            if (lbPhoneInfo.SelectedIndex <phone_data.Rows.Count )
            {
                this.AddrFrom.Text = phone_data.Rows[lbPhoneInfo.SelectedIndex]["Address"].ToString();
                this.HouseFrom.Text = phone_data.Rows[lbPhoneInfo.SelectedIndex]["HouseNum"].ToString();
                this.FlatFrom.Text = phone_data.Rows[lbPhoneInfo.SelectedIndex]["FlatNum"].ToString();
                this.ClientID.Text = phone_data.Rows[lbPhoneInfo.SelectedIndex]["FIO"].ToString();
                this.ClientID.Tag = int.Parse(phone_data.Rows[lbPhoneInfo.SelectedIndex]["ID"].ToString());
                this.Descr.Text = phone_data.Rows[lbPhoneInfo.SelectedIndex]["Descr"].ToString();
                

            }
            
            lbPhoneInfo.Visible = false;
        }

        private void btnEndCall_Click(object sender, EventArgs e)
        {
            if (m_pSipCaller != null)
            {
                m_pSipCaller.doHangup();
                btnEndCall.Visible = false;


                //to do сделать задержку на прием нового звонка пока открыто окно

                m_pSipCaller.doBlockIncomingCalls();
            }
        }

        private void OrgID_SelectedIndexChanged(object sender, EventArgs e) {
            //изменилась организация - подгрузить людей этой организации.
            //вывести их списком с паролем и временем ограничения на заказ - оператор сем решит - заводить заказ или нет.

            //if (m_bLoading == false)

            try {
                if (OrgID.SelectedValue == null) {
                    ORGDETAIL_ID.DataSource = null;
                } else {
                    if (OrgID.SelectedValue.ToString() == "System.Data.DataRowView")
                        return;

                    DataTable dt = new DataTable();
                    dt.Columns.Add("Descr");
                    dt.Columns.Add("ID");

                    //здесь исключение - надо убрать
                    //System.Console.WriteLine("ORDID.SelectedValue = " + OrgID.SelectedValue.ToString());
                    DataRow[] select = organization_detail.Select(" ORGANIZATIONID=" + OrgID.SelectedValue.ToString());
                    foreach (DataRow row in select) {
                        DataRow newrow = dt.NewRow();
                        newrow["Descr"] = row["Descr"];
                        newrow["ID"] = row["ID"];
                        dt.Rows.Add(newrow);

                    }

                    if (m_bLoading == false) {
                        this.Descr.Text = "БЕЗНАЛ. " + OrgID.Text + "Посадка 120р, 10р/км.";
                        if (this.chkPredvar.Checked)
                            this.Descr.Text += " +50 руб за предварительный.";

                    }

                    //m_bLoading = true;
                    ORGDETAIL_ID.Tag = "BLOCK_";
                    ORGDETAIL_ID.DataSource = dt;
                    ORGDETAIL_ID.DisplayMember = "Descr";
                    ORGDETAIL_ID.ValueMember = "ID";

                    ORGDETAIL_ID.AutoCompleteSource = AutoCompleteSource.ListItems;
                    ORGDETAIL_ID.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    //ORGDETAIL_ID.SelectedValue = null;
                    ORGDETAIL_ID.SelectedIndex = -1;

                    //m_bLoading = false;
                    ORGDETAIL_ID.Tag = "";

                    btnSumtoDriver.Visible = true;

                }

            } catch (Exception eee) {
                btnSumtoDriver.Visible = false;
            }

        }


        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void chk_EndOrder_CheckedChanged(object sender, EventArgs e)
        {
            btnEndOrder.Visible = chk_EndOrder.Checked;
            comboEndState.Visible = chk_EndOrder.Checked;
            comboEndState.SelectedIndex = 0;
        }

        private void btnEndOrder_Click(object sender, EventArgs e)
        {
            int end_state = comboEndState.SelectedIndex;
            if (end_state > 0)
                end_state = 3;

            String[] fields = { "ComID", "OpID", "dtArrive", "Value1", "Value2", "State" };
            object[] values = { 4, MAIN_FORM.OperatorID, DateTime.Now, m_iID, end_state, 0 };

            DataBase.mssqlInsert("COMMANDS", fields, values);
        }

        private void OrgID_TextChanged(object sender, EventArgs e)
        {
            btnSumtoDriver.Visible = (OrgID.SelectedValue != null);
        }

        private void btnSumtoDriver_Click(object sender, EventArgs e)
        {

            int orderid = this.m_iID;
            int carid = (int) this.CarID.SelectedValue;
            int sum = 0;
            int.TryParse(summ.Text, out sum);
            if (orderid <= 0)
                return;
            if (carid <= 0)
                return;
            if (sum <= 0)
                return;
            
            DataTable ORDER_DRIVER_INFO = DataBase.mssqlRead("SELECT CARS.SERVICEID, CARS.DRIVERID from ORDERS, CARS WHERE CARS.ID=ORDERS.CARID and ORDERS.ID=" + orderid.ToString());
            if (ORDER_DRIVER_INFO.Rows.Count <= 0)
                return;

            int car_service_id = (int)ORDER_DRIVER_INFO.Rows[0]["SERVICEID"];
            int driver_id = (int)ORDER_DRIVER_INFO.Rows[0]["DRIVERID"];

            if (MessageBox.Show("положить на баланс водителю " + carid.ToString() + " сумму заказа " + sum.ToString() + " ?", "Пополнение баланса", MessageBoxButtons.YesNo) != DialogResult.Yes)
                return;




            DataTable bank_info = DataBase.mssqlRead("SELECT dtArrive from bank where DriverID=" + driver_id.ToString() + " and orderid=" + orderid.ToString());
            if (bank_info.Rows.Count > 0)
            {
                MessageBox.Show("Ошибка. У водителя уже внесен платеж платеж по этому заказу" + bank_info.Rows[0]["dtArrive"].ToString(), "Пополнение баланса", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }



            String[] fields = { "driverid", "ServiceID", "OpID", "dtArrive", "sum", "type", "Descr", "orderid" };
            object[] values = { driver_id, car_service_id, MAIN_FORM.OperatorID, DateTime.Now, sum, 0, "<ПО ЗАКАЗУ>", orderid };

            DataBase.mssqlInsert("BANK", fields, values);

            MessageBox.Show("Внесен  платеж по заказу", "Пополнение баланса", MessageBoxButtons.OK, MessageBoxIcon.None);
            
            doSave();
        }

        private void ServiceID_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void AddrFrom_Leave(object sender, EventArgs e)
        {
            doFindAddrFromName();
        }

        private void HouseFrom_Leave(object sender, EventArgs e)
        {
            doFindAddrFromName();
        }

        private void btnCarMessage_Click(object sender, EventArgs e)
        {
            if (CarID.SelectedValue != null)
            {
                send_mess_box smb = new send_mess_box();
                smb.CAR_IDS.Text = ' '+CarID.SelectedValue.ToString()+' ';
                smb.Show(this);
            }
        }

        private void btnCarDial_Click(object sender, EventArgs e)
        {
            if (CarID.SelectedValue != null)
            {
             
                    int carid = 0;

                    if (int.TryParse(CarID.SelectedValue.ToString(), out carid))
                    {
                        this.m_pSipCaller = MAIN_FORM.main_form_object.doDialCar(carid);
                         //MAIN_FORM.main_form_object.doDialPhone(this.Phone.Text);
                        this.btnEndCall.Visible = true;
                    }
             
            }

        }

        private void btnPhone1Dial_Click(object sender, EventArgs e)
        {
            this.m_pSipCaller = MAIN_FORM.main_form_object.doDialPhone(this.Phone.Text);
            this.btnEndCall.Visible = true;
        }

        private void btnPhone2Dial_Click(object sender, EventArgs e)
        {
           this.m_pSipCaller = MAIN_FORM.main_form_object.doDialPhone(this.ConnectPhone.Text);

           this.btnEndCall.Visible = true;
            
        }

        private void ORGDETAIL_ID_TextChanged(object sender, EventArgs e)
        {
            if (m_bLoading == false)
            if (ORGDETAIL_ID.Tag != "BLOCK_")

            {
                if ((ORGDETAIL_ID.SelectedIndex == 0) && (ORGDETAIL_ID.Text == ""))
                    return;

                
                this.Descr.Text = "БЕЗНАЛ. " + OrgID.Text + "Посадка 120р, 10р/км.";
                if (this.chkPredvar.Checked)
                    this.Descr.Text += " +50 руб за предварительный.";


                if (ORGDETAIL_ID.SelectedValue != null)
                    if (ORGDETAIL_ID.SelectedValue.ToString() !=  "System.Data.DataRowView")
                {
                    DataRow[] select = organization_detail.Select(" ID=" + ORGDETAIL_ID.SelectedValue.ToString());

                    if (select.Length > 0)
                    {
                        this.Descr.Text += ", " + select[0]["Name"] + " лимит " + select[0]["moneylimit"] + "р.";
                        this.ClientID.Text = select[0]["Name"].ToString();
                    }

                }
            }

        }

        private void chkPredvar_CheckedChanged(object sender, EventArgs e)
        {
            if (m_bLoading == false)
            {
                if (OrgID.SelectedItem != null)
                {
                    ORGDETAIL_ID_TextChanged(null, null);
                }
            }
        }

        private void расчетМаршрута_Click(object sender, EventArgs e) {

            this.расчетМаршрута.Text = "расчет...";

            if (this.AddrFrom.Text == "")
            {
                MessageBox.Show("Не указан адрес откуда");
                return;
            }
            if (this.AddrTo.Text == "")
            {
                MessageBox.Show("Не указан адрес куда");
                return;
            }


            String str1 = this.AddrFrom.Text, str2=this.AddrTo.Text;

            if (chkYaroslavl1.Checked)
                str1 = "ярославль+" + str1;
            if (chkYaroslavl2.Checked)
                str2 = "ярославль+" + str2;

            gisModule.geoPoint location_from = gisModule.getLocationByText("", AddrFrom.Text.Replace("'", ""), HouseFrom.Text.Replace("'", ""), "", false);
            gisModule.geoPoint location_to = gisModule.getLocationByText("", AddrTo.Text.Replace("'", ""), HouseTo2.Text.Replace("'", ""), "", false);

           // String coord1 = MAIN_FORM.doYandexSearchAddress(str1, this.HouseFrom.Text);
          //  String coord2 = MAIN_FORM.doYandexSearchAddress(str2, this.HouseTo2.Text);

          //  coord1 = coord1.Replace('.', ',');
          //  coord2 = coord2.Replace('.', ',');
            if (location_from.lat!=0)
                if (location_to.lat!=0)
                {
                    csmapcontrol.MapPath calculated_path = csmapcontrol.MapControl.ORDER_CALCULATED_PATH;
                    /*Double lat1, lon1, lat2, lon2;
                    lon1 = Double.Parse(coord1.Split(' ')[0]);
                    lat1 = Double.Parse(coord1.Split(' ')[1]);

                    lon2 = Double.Parse(coord2.Split(' ')[0]);
                    lat2 = Double.Parse(coord2.Split(' ')[1]);
*/
                    

                    calculated_path.setFirstPoint(location_from.lat,location_from.lon,AddrFrom.Text + " " + HouseFrom.Text);
                    calculated_path.setLastPoint(location_to.lat, location_to.lon, AddrTo.Text + " " + HouseTo2.Text);
                    calculated_path.calculatePath(4000);

                    //csmapcontrol.MapControl.

                    //MAIN_FORM.main_form_object.doShowPath(calculated_path);

                    расчетМаршрута.Text = calculated_path.path_distance.ToString(".00") + " км";
                    расчетМаршрута.Tag = calculated_path.path_distance;


                    DistanceMoney.Tag = (((Double)расчетМаршрута.Tag) * 10 + 80);
                    DistanceMoney.Text = ((Double)DistanceMoney.Tag).ToString("0.") + " руб";

                }
            


        }

        private void timer1_Tick(object sender, EventArgs e) {
            timer1.Enabled = false;
            if ((Phone.Text.Length < 6) || (Phone.Text == "anonymous")) 
            {
                Phone.BackColor = Color.Red;
            }

        }

        private void lbPhoneInfo_SelectedIndexChanged(object sender, EventArgs e) {

        }

        private void AddrTo_Leave(object sender, EventArgs e) {
            doFindAddrTo();
        }

        private void HouseTo_Leave(object sender, EventArgs e) {
            doFindAddrTo();
        }

        private void label24_Click(object sender, EventArgs e)
        {

        }

        private void HouseTo_TextChanged(object sender, EventArgs e)
        {

        }

        private void AddrToForDriver_Leave(object sender, EventArgs e)
        {
            doFindAddrTo();
        }


        private void AddrToForDriver_TextChanged(object sender, EventArgs e)
        {
            if (m_bLoading == false)
            {
                RaionToID.SelectedValue = 0;
            }
        }

        private void AddrToForDriver_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (AddrToForDriver.SelectedIndex != -1)
            {
              //  doFindAddrTo();
            }
        }

        private void ZoneID_SelectedIndexChanged(object sender, EventArgs e) {
            ///попробую сделать чтобы подставлялся мигающий автомобиль из района
            ///1. Если район выбран - запрос машин из района
            ///2. если район пустой - убрать машину
            ///
/*
            object value = ((ComboBox)sender).SelectedValue;
            if (value != null)
                System.Console.WriteLine("ZoneID.selectedValue = " + value.ToString());
            else {
                System.Console.WriteLine("ZoneID.selectedValue = null");
                CarID.SelectedItem = null;
                //CarID.SelectedIndex = -1;
                //ZoneID.SelectedItem = null;
            }


            if (value != null) {
                if (MAIN_FORM.main_form_object.first_cars.ContainsValue(value.ToString())) {
                    foreach (object key in MAIN_FORM.main_form_object.first_cars.Keys) {

                        if (MAIN_FORM.main_form_object.first_cars[key].ToString().CompareTo(value.ToString()) == 0) {
                            this.CarID.SelectedValue = key;
                        }
                    }

                } else {
                    CarID.SelectedItem = null;
                }
            }
*/

        }

        private void order_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                btnEndCall.PerformClick();
                btnSave.PerformClick();// имитируем нажатие button1
            }
        }

        private void order_prim_menu_Opening(object sender, CancelEventArgs e)
        {

        }




    }
}
