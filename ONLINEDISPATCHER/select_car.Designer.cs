﻿namespace ONLINEDISPATCHER
{
    partial class select_car
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboCars = new System.Windows.Forms.ComboBox();
            this.btnSelect = new System.Windows.Forms.Button();
            this.btnDial = new System.Windows.Forms.Button();
            this.car_info = new System.Windows.Forms.TextBox();
            this.btnPromisedPayment = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // comboCars
            // 
            this.comboCars.DropDownHeight = 120;
            this.comboCars.FormattingEnabled = true;
            this.comboCars.IntegralHeight = false;
            this.comboCars.Location = new System.Drawing.Point(9, 10);
            this.comboCars.Margin = new System.Windows.Forms.Padding(2);
            this.comboCars.Name = "comboCars";
            this.comboCars.Size = new System.Drawing.Size(357, 21);
            this.comboCars.TabIndex = 0;
            this.comboCars.SelectedIndexChanged += new System.EventHandler(this.comboCars_SelectedIndexChanged);
            this.comboCars.DropDownClosed += new System.EventHandler(this.comboCars_DropDownClosed);
            // 
            // btnSelect
            // 
            this.btnSelect.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSelect.Location = new System.Drawing.Point(13, 53);
            this.btnSelect.Margin = new System.Windows.Forms.Padding(2);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(353, 19);
            this.btnSelect.TabIndex = 1;
            this.btnSelect.Text = "Выбрать";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnDial
            // 
            this.btnDial.Location = new System.Drawing.Point(13, 92);
            this.btnDial.Margin = new System.Windows.Forms.Padding(2);
            this.btnDial.Name = "btnDial";
            this.btnDial.Size = new System.Drawing.Size(353, 19);
            this.btnDial.TabIndex = 2;
            this.btnDial.Text = "Позвонить";
            this.btnDial.UseVisualStyleBackColor = true;
            this.btnDial.Click += new System.EventHandler(this.btnDial_Click);
            // 
            // car_info
            // 
            this.car_info.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.car_info.Location = new System.Drawing.Point(13, 146);
            this.car_info.Margin = new System.Windows.Forms.Padding(2);
            this.car_info.Multiline = true;
            this.car_info.Name = "car_info";
            this.car_info.ReadOnly = true;
            this.car_info.Size = new System.Drawing.Size(353, 86);
            this.car_info.TabIndex = 3;
            // 
            // btnPromisedPayment
            // 
            this.btnPromisedPayment.Location = new System.Drawing.Point(13, 236);
            this.btnPromisedPayment.Margin = new System.Windows.Forms.Padding(2);
            this.btnPromisedPayment.Name = "btnPromisedPayment";
            this.btnPromisedPayment.Size = new System.Drawing.Size(127, 19);
            this.btnPromisedPayment.TabIndex = 4;
            this.btnPromisedPayment.Text = "Обещанный платеж";
            this.btnPromisedPayment.UseVisualStyleBackColor = true;
            this.btnPromisedPayment.Click += new System.EventHandler(this.btnPromisedPayment_Click);
            // 
            // select_car
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(376, 85);
            this.Controls.Add(this.btnPromisedPayment);
            this.Controls.Add(this.car_info);
            this.Controls.Add(this.btnDial);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.comboCars);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "select_car";
            this.Text = "Выбор машины";
            this.Load += new System.EventHandler(this.select_car_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox car_info;
        private System.Windows.Forms.Button btnPromisedPayment;
        public System.Windows.Forms.Button btnSelect;
        public System.Windows.Forms.Button btnDial;
        public System.Windows.Forms.ComboBox comboCars;
    }
}