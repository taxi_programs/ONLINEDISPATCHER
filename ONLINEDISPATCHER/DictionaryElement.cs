﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace ONLINEDISPATCHER
{
    public partial class DictionaryElement : Form
    {

        private List<SqlParameter> sqlParameters = new List<SqlParameter>();

        private string Sql = "";
        private Dictionary<String, dictionary.QueryVisualParam> Fields = new Dictionary<string, dictionary.QueryVisualParam>();

        private int m_iID = 0;

        private String table_name = "BAD_TABLE_NAME";

        public Form parent = null;


        //загружаемые справочники..








        public DictionaryElement()
        {  
            InitializeComponent();
           
            if (this.ParentForm != null)
                this.Location = this.ParentForm.Location;// Cursor.Position;
            else
                this.Location = Cursor.Position;
           
        }


        private void doMakeInterface(String type)
        {
            //создать панель, не ней элементы
            //внизу кнопка сохранить

            table_name = type;


            if (table_name == "ARENDA_REG")
            {

                var panel = new TableLayoutPanel();

                //panel.BackColor = Color.Azure;
                panel.AutoSize = true;


                panel.GrowStyle = TableLayoutPanelGrowStyle.AddRows;
                //panel.RowCount = 10;
                //panel.ColumnCount = 2;




                DataTable ARENDA_SUM_TYPE = DataBase.mssqlRead("SELECT * FROM ARENDA_SUM_TYPE order by ID");


                var t1 = new dictionary.QueryVisualParam("Тип суммы", "ARENDA_SUM_TYPE_ID", dictionary.QueryVisualParam.QueryVisualParamType.DBDictionary, ARENDA_SUM_TYPE);
                panel.Controls.Add(t1); Fields.Add("ARENDA_SUM_TYPE_ID", t1);  sqlParameters.Add(t1.param);
                t1.Enabled = false;

                t1 = new dictionary.QueryVisualParam("Сумма", "SUM", dictionary.QueryVisualParam.QueryVisualParamType.Decimal, new List<object>());
                panel.Controls.Add(t1); Fields.Add("SUM", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("Комментарий", "COMMENT", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("COMMENT", t1); sqlParameters.Add(t1.param);




                this.Controls.Add(panel);

                Sql = "SELECT ARENDA_SUM_TYPE_ID, SUM, COMMENT FROM ARENDA_REG WHERE ID=@ID";


                ;
                this.Text = "Редактирование ОПЛАТЫ";
                this.Width = panel.Width + 24;
                this.Height = panel.Height + 70;

                button1.Visible = false;
            }




            if (table_name == "PHONES")
            {

                var panel = new TableLayoutPanel();

                //panel.BackColor = Color.Azure;
                panel.AutoSize = true;


                panel.GrowStyle = TableLayoutPanelGrowStyle.AddRows;
                //panel.RowCount = 10;
                //panel.ColumnCount = 2;




                String[] state_list = { "Белый", "ЧЕРНЫЙ", "Серый", "VIP" };
                List<object> ll = new List<object>();
                ll.AddRange(state_list);
                var t1 = new dictionary.QueryVisualParam("Состояние", "STATE", dictionary.QueryVisualParam.QueryVisualParamType.Combobox, ll);
                panel.Controls.Add(t1);
                Fields.Add("STATE", t1);
                sqlParameters.Add(t1.param);


                t1 = new dictionary.QueryVisualParam("ФИО", "FIO", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("FIO", t1); sqlParameters.Add(t1.param);


                t1 = new dictionary.QueryVisualParam("Телефон", "PHONE", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("PHONE", t1); sqlParameters.Add(t1.param);
                t1 = new dictionary.QueryVisualParam("Телефон 2", "PHONE2", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("PHONE2", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("Телефон 3", "PHONE3", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("PHONE3", t1); sqlParameters.Add(t1.param);




                t1 = new dictionary.QueryVisualParam("Адрес", "ADDRESS", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("ADDRESS", t1); sqlParameters.Add(t1.param);
                t1 = new dictionary.QueryVisualParam("Дом", "HOUSENUM", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("HOUSENUM", t1); sqlParameters.Add(t1.param);
                t1 = new dictionary.QueryVisualParam("Квартира", "FLATNUM", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("FLATNUM", t1); sqlParameters.Add(t1.param);



                t1 = new dictionary.QueryVisualParam("Описание", "DESCR", dictionary.QueryVisualParam.QueryVisualParamType.BigText, new List<object>());
                panel.Controls.Add(t1); Fields.Add("DESCR", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("Скидка, %", "SKID_PERCENT", dictionary.QueryVisualParam.QueryVisualParamType.Decimal, new List<object>());
                panel.Controls.Add(t1); Fields.Add("SKID_PERCENT", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("Сумма, руб", "MONEY_SUMM", dictionary.QueryVisualParam.QueryVisualParamType.Decimal, new List<object>());
                panel.Controls.Add(t1); Fields.Add("MONEY_SUMM", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("Позывной , кому должны", "CAR_ID", dictionary.QueryVisualParam.QueryVisualParamType.Numeric, new List<object>());
                panel.Controls.Add(t1); Fields.Add("CAR_ID", t1); sqlParameters.Add(t1.param);




                this.Controls.Add(panel);

                Sql = "SELECT ADDRESS, HOUSENUM, FLATNUM, FIO,  PHONE, PHONE2, PHONE3, DESCR, STATE, SKID_PERCENT, MONEY_SUMM, CAR_ID FROM PHONES WHERE ID=@ID";


                ;
                this.Text = "Редактирование АБОНЕНТА / ЧЕРНОГО СПИСКА";
                this.Width = panel.Width + 24;
                this.Height = panel.Height + 70;
            }


            if (table_name == "CARS") {

                var panel = new TableLayoutPanel();

                //panel.BackColor = Color.Azure;
                panel.AutoSize = true;


                panel.GrowStyle = TableLayoutPanelGrowStyle.AddRows;
                //panel.RowCount = 10;
                //panel.ColumnCount = 2;


                DataTable services = DataBase.mssqlRead("SELECT * FROM SERVICES where ID>0");

                DataTable arenda_cars = DataBase.mssqlRead("select 0 as ID, '.НЕТ.' as NAME union SELECT ID, arenda_cars.mark + ' '+arenda_cars.gosnum+' '+arenda_cars.color as NAME FROM ARENDA_CARS where IS_WORKED=1 order by NAME");

                



                var t1 = new dictionary.QueryVisualParam("Позывной", "ID", dictionary.QueryVisualParam.QueryVisualParamType.Numeric, new List<object>());
                panel.Controls.Add(t1); Fields.Add("ID", t1); sqlParameters.Add(t1.param);

                if (m_iID > 0)
                    t1.Enabled = false;

                //String[] state_list = { "Белый", "ЧЕРНЫЙ", "Серый", "VIP" };
                //List<object> ll = new List<object>();
                //ll.AddRange(state_list);

                t1 = new dictionary.QueryVisualParam("Пароль", "PASSWORD", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("PASSWORD", t1); sqlParameters.Add(t1.param);

                if (!((MAIN_FORM.OperatorID == 129) ||
                    (MAIN_FORM.OperatorID == 123) ||
                    (MAIN_FORM.OperatorID == 120) ||
                    (MAIN_FORM.OperatorID == 147)))
                    t1.Visible = false;



                t1 = new dictionary.QueryVisualParam("Служба", "SERVICEID", dictionary.QueryVisualParam.QueryVisualParamType.DBDictionary, services);
                panel.Controls.Add(t1); Fields.Add("SERVICEID", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("Возит БЕЗНАЛ", "ENABLE_BEZNAL", dictionary.QueryVisualParam.QueryVisualParamType.Checkbox, new List<object>());
                panel.Controls.Add(t1); Fields.Add("ENABLE_BEZNAL", t1); sqlParameters.Add(t1.param);


//                t1 = new dictionary.QueryVisualParam("ID", "FIO", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
//                panel.Controls.Add(t1); Fields.Add("FIO", t1); sqlParameters.Add(t1.param);


                t1 = new dictionary.QueryVisualParam("Арендная машина", "ARENDA_CAR_ID", dictionary.QueryVisualParam.QueryVisualParamType.DBDictionary, arenda_cars);
                panel.Controls.Add(t1); Fields.Add("ARENDA_CAR_ID", t1); sqlParameters.Add(t1.param);



                t1 = new dictionary.QueryVisualParam("Марка", "MARK", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("MARK", t1); sqlParameters.Add(t1.param);
                t1 = new dictionary.QueryVisualParam("Гос номер", "GOSNUM", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("GOSNUM", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("Цвет", "COLOR", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("COLOR", t1); sqlParameters.Add(t1.param);


                t1 = new dictionary.QueryVisualParam("Описание", "DESCR", dictionary.QueryVisualParam.QueryVisualParamType.BigText, new List<object>());
                panel.Controls.Add(t1); Fields.Add("DESCR", t1); sqlParameters.Add(t1.param);



                /////ещще параметры контакт-центра - цвет и марка.

                String[] state_list = { "Белый", "Черный", "Красный", "Синий", "Желтый", "Зеленый", "Оранжевый", "Фиолетовый", "Серебристый", "Бежевый", "Золотистый", "Коричневый", "Серый", "Вишневый", "Серо-синий", "Темно-зеленый", "Темно-синий", "Фиолетовый", "Серо-зеленый", "Голубой" };
                List<object> ll = new List<object>();
                ll.AddRange(state_list);
                t1 = new dictionary.QueryVisualParam("КЦ Цвет ", "CCCOLOR", dictionary.QueryVisualParam.QueryVisualParamType.Combobox, ll);
                panel.Controls.Add(t1); Fields.Add("CCCOLOR", t1); sqlParameters.Add(t1.param);


                //String [] state_list2 = { "Иномарка", "ВАЗ", "Волга"};
                String[] state_list2 =  { "Иномарка", "Лада", "Волга", "Калина", "4-ка", "5-ка", "6-ка", "7-ка", "8-ка", "9-ка", "10-ка", "11-я", "12-я", "Приора", "14-я", "15-я", "Ауди", "БМВ", "Вольво", "Джилли", "Киа", "Ланос", "Лексус", "Лифан", "Мазда", "Матиз", "Мерседес", "Митсубиси", "Нексия", "Нива", "Нива шивроле", "Ниссан", "Ода", "Опель", "Пежо", "Рено", "Сааб", "Саманд", "Сенс", "Ситроен", "Соболь", "Субару", "Сузуки", "Тойота", "Фабула", "Фиат", "Фольцваген", "Форд", "Хёндай", "Хонда", "Черри", "Шевроле", "Шкода", "Ларгус", "Ховер", "Шанс", "Логан" };
                ll = new List<object>();
                ll.AddRange(state_list2);
/*                for (int i = 1; i < 51; i++) {
                    String s = "Тип " + i.ToString();
                    ll.Add(s);
                }
 */ 
                t1 = new dictionary.QueryVisualParam("КЦ Марка машины ", "CCTYPE", dictionary.QueryVisualParam.QueryVisualParamType.Combobox, ll);
                panel.Controls.Add(t1); Fields.Add("CCTYPE", t1); sqlParameters.Add(t1.param);




                this.Controls.Add(panel);

                Sql = "SELECT ID, PASSWORD,  SERVICEID, ENABLE_BEZNAL, CCCOLOR, CCTYPE, MARK, GOSNUM, COLOR, DESCR, ARENDA_CAR_ID FROM CARS WHERE ID=@ID";


                ;
                this.Text = "Редактирование машины";
                this.Width = panel.Width + 24;
                this.Height = panel.Height + 80;
            }



            if (table_name == "ARENDA_CARS")
            {

                var panel = new TableLayoutPanel();

                //panel.BackColor = Color.Azure;
                panel.AutoSize = true;


                panel.GrowStyle = TableLayoutPanelGrowStyle.AddRows;
                //panel.RowCount = 10;
                //panel.ColumnCount = 2;


                DataTable services = DataBase.mssqlRead("SELECT * FROM SERVICES where ID>0");

                

                var t1 = new dictionary.QueryVisualParam("В работе", "IS_WORKED", dictionary.QueryVisualParam.QueryVisualParamType.Checkbox, new List<object>());
                panel.Controls.Add(t1); Fields.Add("IS_WORKED", t1); sqlParameters.Add(t1.param);


                //                t1 = new dictionary.QueryVisualParam("ID", "FIO", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                //                panel.Controls.Add(t1); Fields.Add("FIO", t1); sqlParameters.Add(t1.param);





                t1 = new dictionary.QueryVisualParam("Марка", "MARK", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("MARK", t1); sqlParameters.Add(t1.param);
                t1 = new dictionary.QueryVisualParam("Гос номер", "GOSNUM", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("GOSNUM", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("Цвет", "COLOR", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("COLOR", t1); sqlParameters.Add(t1.param);


                t1 = new dictionary.QueryVisualParam("Описание", "DESCR", dictionary.QueryVisualParam.QueryVisualParamType.BigText, new List<object>());
                panel.Controls.Add(t1); Fields.Add("DESCR", t1); sqlParameters.Add(t1.param);


                t1 = new dictionary.QueryVisualParam("ПТС", "PTS", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("PTS", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("СТС", "STS", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("STS", t1); sqlParameters.Add(t1.param);


                t1 = new dictionary.QueryVisualParam("Дата выпуска машины", "DT_CREATE", dictionary.QueryVisualParam.QueryVisualParamType.Calendar, new List<object>());
                panel.Controls.Add(t1); Fields.Add("DT_CREATE", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("Дата окончания полиса", "COLOR", dictionary.QueryVisualParam.QueryVisualParamType.Calendar, new List<object>());
                panel.Controls.Add(t1); Fields.Add("DT_POLICE_END", t1); sqlParameters.Add(t1.param);


                t1 = new dictionary.QueryVisualParam("Пробег", "PROBEG", dictionary.QueryVisualParam.QueryVisualParamType.Decimal, new List<object>());
                panel.Controls.Add(t1); Fields.Add("PROBEG", t1); sqlParameters.Add(t1.param);



                /////ещще параметры контакт-центра - цвет и марка.

                String[] state_list = { "Белый", "Черный", "Красный", "Синий", "Желтый", "Зеленый", "Оранжевый", "Фиолетовый", "Серебристый", "Бежевый", "Золотистый", "Коричневый", "Серый", "Вишневый", "Серо-синий", "Темно-зеленый", "Темно-синий", "Фиолетовый", "Серо-зеленый", "Голубой" };
                List<object> ll = new List<object>();
                ll.AddRange(state_list);
                t1 = new dictionary.QueryVisualParam("КЦ Цвет ", "CCCOLOR", dictionary.QueryVisualParam.QueryVisualParamType.Combobox, ll);
                panel.Controls.Add(t1); Fields.Add("CCCOLOR", t1); sqlParameters.Add(t1.param);


                //String [] state_list2 = { "Иномарка", "ВАЗ", "Волга"};
                String[] state_list2 = { "Иномарка", "Лада", "Волга", "Калина", "4-ка", "5-ка", "6-ка", "7-ка", "8-ка", "9-ка", "10-ка", "11-я", "12-я", "Приора", "14-я", "15-я", "Ауди", "БМВ", "Вольво", "Джилли", "Киа", "Ланос", "Лексус", "Лифан", "Мазда", "Матиз", "Мерседес", "Митсубиси", "Нексия", "Нива", "Нива шивроле", "Ниссан", "Ода", "Опель", "Пежо", "Рено", "Сааб", "Саманд", "Сенс", "Ситроен", "Соболь", "Субару", "Сузуки", "Тойота", "Фабула", "Фиат", "Фольцваген", "Форд", "Хёндай", "Хонда", "Черри", "Шевроле", "Шкода", "Ларгус", "Ховер", "Шанс", "Логан" };
                ll = new List<object>();
                ll.AddRange(state_list2);
                /*                for (int i = 1; i < 51; i++) {
                                    String s = "Тип " + i.ToString();
                                    ll.Add(s);
                                }
                 */
                t1 = new dictionary.QueryVisualParam("КЦ Марка машины ", "CCTYPE", dictionary.QueryVisualParam.QueryVisualParamType.Combobox, ll);
                panel.Controls.Add(t1); Fields.Add("CCTYPE", t1); sqlParameters.Add(t1.param);




                this.Controls.Add(panel);

                Sql = "SELECT   ID,  DESCR,  MARK,  GOSNUM,  PTS,  STS,  DT_CREATE,  COLOR, IS_WORKED,  CCTYPE,  CCCOLOR,  DT_POLICE_END,  PROBEG FROM   dbo.arenda_cars  WHERE ID=@ID";


                ;
                this.Text = "Редактирование арендной машины";
                this.Width = panel.Width + 24;
                this.Height = panel.Height + 80;
            }



            if (table_name == "DRIVERS") {

                var panel = new TableLayoutPanel();

                //panel.BackColor = Color.Azure;
                panel.AutoSize = true;


                panel.GrowStyle = TableLayoutPanelGrowStyle.AddRows;
                //panel.RowCount = 10;
                //panel.ColumnCount = 2;


                DataTable cars = DataBase.mssqlRead("SELECT ID, cast (id as varchar(10))+' '+ Color +' '+ Mark +' '+ gosnum Name FROM cars where ID>0");




//                var t1 = new dictionary.QueryVisualParam("Позывной", "CARID", dictionary.QueryVisualParam.QueryVisualParamType.Numeric, new List<object>());
//                panel.Controls.Add(t1); Fields.Add("ID", t1); sqlParameters.Add(t1.param);
//                t1.Enabled = false;

                var t1 = new dictionary.QueryVisualParam("Позывной-автомобиль", "CARID", dictionary.QueryVisualParam.QueryVisualParamType.DBDictionary, cars);
                panel.Controls.Add(t1); Fields.Add("CARID", t1); sqlParameters.Add(t1.param);

                
                ///t1 = new dictionary.QueryVisualParam("Служба", "SERVICEID", dictionary.QueryVisualParam.QueryVisualParamType.DBDictionary, services);
                //panel.Controls.Add(t1); Fields.Add("SERVICEID", t1); sqlParameters.Add(t1.param);


                //                t1 = new dictionary.QueryVisualParam("ID", "FIO", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                //                panel.Controls.Add(t1); Fields.Add("FIO", t1); sqlParameters.Add(t1.param);


                t1 = new dictionary.QueryVisualParam("Дата приема", "DTEMPLOYED", dictionary.QueryVisualParam.QueryVisualParamType.Calendar, new List<object>());
                panel.Controls.Add(t1); Fields.Add("DTEMPLOYED", t1); sqlParameters.Add(t1.param);
                
                t1 = new dictionary.QueryVisualParam("ФИО", "FIO", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("FIO", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("Телефоны", "PHONES", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("PHONES", t1); sqlParameters.Add(t1.param);
                t1 = new dictionary.QueryVisualParam("Телефон2", "PHONE2", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("PHONE2", t1); sqlParameters.Add(t1.param);
                t1 = new dictionary.QueryVisualParam("Телефон3", "PHONE3", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("PHONE3", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("Адрес", "ADDRESS", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("ADDRESS", t1); sqlParameters.Add(t1.param);
                t1 = new dictionary.QueryVisualParam("Примечание", "DESCR", dictionary.QueryVisualParam.QueryVisualParamType.BigText, new List<object>());
                panel.Controls.Add(t1); Fields.Add("DESCR", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("Фото", "PHOTO", dictionary.QueryVisualParam.QueryVisualParamType.Image, null);
                panel.Controls.Add(t1); Fields.Add("PHOTO", t1); sqlParameters.Add(t1.param);



                this.Controls.Add(panel);

                Sql = "SELECT ID, CARID, FIO, PHONES, PHONE2, PHONE3, ADDRESS, DESCR, PHOTO, DTEMPLOYED FROM DRIVERS WHERE ID=@ID";


                ;
                this.Text = "Редактирование водителя";
                this.Width = panel.Width + 24;
                this.Height = panel.Height + 80;
            }


            if (table_name == "ORGANIZATIONS") {

                var panel = new TableLayoutPanel();

                //panel.BackColor = Color.Azure;
                panel.AutoSize = true;


                panel.GrowStyle = TableLayoutPanelGrowStyle.AddRows;

                var t1 = new dictionary.QueryVisualParam("Наименование", "NAME", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("NAME", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("Примечание", "DESCR", dictionary.QueryVisualParam.QueryVisualParamType.BigText, new List<object>());
                panel.Controls.Add(t1); Fields.Add("DESCR", t1); sqlParameters.Add(t1.param);
                t1 = new dictionary.QueryVisualParam("Договор", "AgreementNum", dictionary.QueryVisualParam.QueryVisualParamType.BigText, new List<object>());
                panel.Controls.Add(t1); Fields.Add("AgreementNum", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("Адрес доставки документов", "Address", dictionary.QueryVisualParam.QueryVisualParamType.BigText, new List<object>());
                panel.Controls.Add(t1); Fields.Add("Address", t1); sqlParameters.Add(t1.param);

                

                t1 = new dictionary.QueryVisualParam("Контакты", "PhoneList", dictionary.QueryVisualParam.QueryVisualParamType.BigText, new List<object>());
                panel.Controls.Add(t1); Fields.Add("PhoneList", t1); sqlParameters.Add(t1.param);
                t1 = new dictionary.QueryVisualParam("Логин", "username", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("username", t1); sqlParameters.Add(t1.param);
                t1 = new dictionary.QueryVisualParam("Пароль", "userpassword", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("userpassword", t1); sqlParameters.Add(t1.param);
                t1 = new dictionary.QueryVisualParam("В работе", "AgreementStatus", dictionary.QueryVisualParam.QueryVisualParamType.Checkbox, new List<object>());
                panel.Controls.Add(t1); Fields.Add("AgreementStatus", t1); sqlParameters.Add(t1.param);



                this.Controls.Add(panel);

                Sql = "SELECT ID, NAME, DESCR, AgreementNum, PhoneList, username, userpassword, AgreementStatus FROM ORGANIZATIONS WHERE ID=@ID";


                ;
                this.Text = "Редактирование организации";
                this.Width = panel.Width + 24;
                this.Height = panel.Height + 80;
            }

            if (table_name == "ORGANIZATION_DETAIL") {

                var panel = new TableLayoutPanel();

                //panel.BackColor = Color.Azure;
                panel.AutoSize = true;


                panel.GrowStyle = TableLayoutPanelGrowStyle.AddRows;

                var t1 = new dictionary.QueryVisualParam("ФИО сотрудника", "NAME", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("NAME", t1); sqlParameters.Add(t1.param);
                t1 = new dictionary.QueryVisualParam("Примечание", "DESCR", dictionary.QueryVisualParam.QueryVisualParamType.BigText, new List<object>());
                panel.Controls.Add(t1); Fields.Add("DESCR", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("Пароль", "PASSWORD", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("PASSWORD", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("Лимит денег", "MONEYLIMIT", dictionary.QueryVisualParam.QueryVisualParamType.Numeric, new List<object>());
                panel.Controls.Add(t1); Fields.Add("MONEYLIMIT", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("Лимит по времени", "TIMELIMIT", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("TIMELIMIT", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("Лимит в месяц", "MONTH_MONEY_LIMIT", dictionary.QueryVisualParam.QueryVisualParamType.Numeric, new List<object>());
                panel.Controls.Add(t1); Fields.Add("MONTH_MONEY_LIMIT", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("В работе", "is_active", dictionary.QueryVisualParam.QueryVisualParamType.Checkbox, new List<object>());
                panel.Controls.Add(t1); Fields.Add("is_active", t1); sqlParameters.Add(t1.param);

                


                DataTable organizations = DataBase.mssqlRead("SELECT ID, name from ORGANIZATIONS where AGREEMENTSTATUS=1");


                t1 = new dictionary.QueryVisualParam("Организация", "ORGANIZATIONID", dictionary.QueryVisualParam.QueryVisualParamType.DBDictionary, organizations);
                panel.Controls.Add(t1); Fields.Add("ORGANIZATIONID", t1); sqlParameters.Add(t1.param);



                this.Controls.Add(panel);

                Sql = "SELECT ID, NAME, DESCR, PASSWORD, MONEYLIMIT, TIMELIMIT, ORGANIZATIONID, MONTH_MONEY_LIMIT, is_active FROM ORGANIZATION_DETAIL WHERE ID=@ID";


                ;



                this.Text = "Редактирование сотрудника организации";
                this.Width = panel.Width + 24;
                this.Height = panel.Height + 80;
            }

            if (table_name == "ORGANIZATION_SUBDETAIL")
            {

                var panel = new TableLayoutPanel();

                //panel.BackColor = Color.Azure;
                panel.AutoSize = true;


                panel.GrowStyle = TableLayoutPanelGrowStyle.AddRows;

                var t1 = new dictionary.QueryVisualParam("Сотрудник", "NAME", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("NAME", t1); sqlParameters.Add(t1.param);
                t1 = new dictionary.QueryVisualParam("Примечание", "DESCR", dictionary.QueryVisualParam.QueryVisualParamType.BigText, new List<object>());
                panel.Controls.Add(t1); Fields.Add("DESCR", t1); sqlParameters.Add(t1.param);
                

                t1 = new dictionary.QueryVisualParam("В работе", "is_active", dictionary.QueryVisualParam.QueryVisualParamType.Checkbox, new List<object>());
                panel.Controls.Add(t1); Fields.Add("is_active", t1); sqlParameters.Add(t1.param);





                DataTable organization_detail = DataBase.mssqlRead("SELECT ID, name from ORGANIZATION_DETAIL od2  where ORGANIZATIONID = (select organizationid from ORGANIZATION_DETAIL od where od.id="+this.Tag+")");


                t1 = new dictionary.QueryVisualParam("Отдел", "ORGANIZATION_DETAIL_ID", dictionary.QueryVisualParam.QueryVisualParamType.DBDictionary, organization_detail);
                panel.Controls.Add(t1); Fields.Add("ORGANIZATION_DETAIL_ID", t1); sqlParameters.Add(t1.param);



                this.Controls.Add(panel);

                Sql = "SELECT ID, NAME, DESCR, ORGANIZATION_DETAIL_ID, is_active FROM ORGANIZATION_SUBDETAIL WHERE ID=@ID";


                ;



                this.Text = "Редактирование сотрудника отдела организации";
                this.Width = panel.Width + 24;
                this.Height = panel.Height + 80;
            }


            if (table_name == "BNAL_AKT") {

                var panel = new TableLayoutPanel();

                //panel.BackColor = Color.Azure;
                panel.AutoSize = true;


                panel.GrowStyle = TableLayoutPanelGrowStyle.AddRows;

                var t1 = new dictionary.QueryVisualParam("Номер акта", "AKT_NUM", dictionary.QueryVisualParam.QueryVisualParamType.Numeric, new List<object>());
                panel.Controls.Add(t1); Fields.Add("AKT_NUM", t1); sqlParameters.Add(t1.param);
                t1 = new dictionary.QueryVisualParam("Дата акта", "AKT_DATE", dictionary.QueryVisualParam.QueryVisualParamType.Calendar, new List<object>());
                panel.Controls.Add(t1); Fields.Add("AKT_DATE", t1); sqlParameters.Add(t1.param);
                t1 = new dictionary.QueryVisualParam("Текст акта", "AKT_TEXT", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("AKT_TEXT", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("Сумма", "AKT_SUMM", dictionary.QueryVisualParam.QueryVisualParamType.Decimal, new List<object>());
                panel.Controls.Add(t1); Fields.Add("AKT_SUMM", t1); sqlParameters.Add(t1.param);
                
                DataTable organizations = DataBase.mssqlRead("SELECT ID, name from ORGANIZATIONS where AGREEMENTSTATUS=1");




                //                var t1 = new dictionary.QueryVisualParam("Позывной", "CARID", dictionary.QueryVisualParam.QueryVisualParamType.Numeric, new List<object>());
                //                panel.Controls.Add(t1); Fields.Add("ID", t1); sqlParameters.Add(t1.param);
                //                t1.Enabled = false;

                t1 = new dictionary.QueryVisualParam("Клиент", "ORGANIZATION_ID", dictionary.QueryVisualParam.QueryVisualParamType.DBDictionary, organizations);
                panel.Controls.Add(t1); Fields.Add("ORGANIZATION_ID", t1); sqlParameters.Add(t1.param);



                this.Controls.Add(panel);

                Sql = "SELECT ID, AKT_NUM, AKT_DATE, AKT_TEXT, AKT_SUMM, ORGANIZATION_ID FROM BNAL_AKT WHERE ID=@ID";



                this.Text = "Редактирование акта выполненых работ организации";
                this.Width = panel.Width + 24;
                this.Height = panel.Height + 80;
            }


            if (table_name == "GIS_ADRESSES")
            {

                var panel = new TableLayoutPanel();

                //panel.BackColor = Color.Azure;
                panel.AutoSize = true;


                panel.GrowStyle = TableLayoutPanelGrowStyle.AddRows;


                DataTable names = DataBase.mssqlRead("SELECT distinct NAME from GIS_ADRESSES");




                //                var t1 = new dictionary.QueryVisualParam("Позывной", "CARID", dictionary.QueryVisualParam.QueryVisualParamType.Numeric, new List<object>());
                //                panel.Controls.Add(t1); Fields.Add("ID", t1); sqlParameters.Add(t1.param);
                //                t1.Enabled = false;

            //    t1 = new dictionary.QueryVisualParam("Организация", "ORGANIZATIONID", dictionary.QueryVisualParam.QueryVisualParamType.DBDictionary, organizations);
            //    panel.Controls.Add(t1); Fields.Add("ORGANIZATIONID", t1); sqlParameters.Add(t1.param);

                var t1 = new dictionary.QueryVisualParam("Наименование", "NAME", dictionary.QueryVisualParam.QueryVisualParamType.DBTextDictionary, names);
                panel.Controls.Add(t1); Fields.Add("NAME", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("Город", "CITY", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("CITY", t1); sqlParameters.Add(t1.param);

                t1.SetValue("Ярославль");

                t1 = new dictionary.QueryVisualParam("Улица", "STREET", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("STREET", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("Дом", "NUMBER", dictionary.QueryVisualParam.QueryVisualParamType.Text, new List<object>());
                panel.Controls.Add(t1); Fields.Add("NUMBER", t1); sqlParameters.Add(t1.param);



                this.Controls.Add(panel);

                Sql = "SELECT ID, CITY, NAME, STREET, NUMBER FROM GIS_ADRESSES WHERE ID=@ID";


                ;



                this.Text = "Редактирование адреса";
                this.Width = panel.Width + 24;
                this.Height = panel.Height + 80;
            }




            if (table_name == "TARIF")
            {

                var panel = new TableLayoutPanel();

                //panel.BackColor = Color.Azure;
                panel.AutoSize = true;


                panel.GrowStyle = TableLayoutPanelGrowStyle.AddRows;



                var t1 = new dictionary.QueryVisualParam("Посадка НАЛ", "money_posadka", dictionary.QueryVisualParam.QueryVisualParamType.Numeric, new List<object>());
                panel.Controls.Add(t1); Fields.Add("money_posadka", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("Посадка БНАЛ", "money_posadka_bnal", dictionary.QueryVisualParam.QueryVisualParamType.Numeric, new List<object>());
                panel.Controls.Add(t1); Fields.Add("money_posadka_bnal", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("КМ Город НАЛ", "money_km", dictionary.QueryVisualParam.QueryVisualParamType.Numeric, new List<object>());
                panel.Controls.Add(t1); Fields.Add("money_km", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("КМ Город БНАЛ", "money_km_bnal", dictionary.QueryVisualParam.QueryVisualParamType.Numeric, new List<object>());
                panel.Controls.Add(t1); Fields.Add("money_km_bnal", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("КМ Загород НАЛ", "money_km_zagorod", dictionary.QueryVisualParam.QueryVisualParamType.Numeric, new List<object>());
                panel.Controls.Add(t1); Fields.Add("money_km_zagorod", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("КМ Загород БНАЛ", "money_km_zagorod_bnal", dictionary.QueryVisualParam.QueryVisualParamType.Numeric, new List<object>());
                panel.Controls.Add(t1); Fields.Add("money_km_zagorod_bnal", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("минуты НАЛ", "money_minute", dictionary.QueryVisualParam.QueryVisualParamType.Numeric, new List<object>());
                panel.Controls.Add(t1); Fields.Add("money_minute", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("минуты БНАЛ", "money_minute_bnal", dictionary.QueryVisualParam.QueryVisualParamType.Numeric, new List<object>());
                panel.Controls.Add(t1); Fields.Add("money_minute_bnal", t1); sqlParameters.Add(t1.param);

                t1 = new dictionary.QueryVisualParam("причина изменения для историии", "desc", dictionary.QueryVisualParam.QueryVisualParamType.BigText, new List<object>());
                panel.Controls.Add(t1); Fields.Add("tarif_change_desc", t1); sqlParameters.Add(t1.param);


                this.Controls.Add(panel);

                Sql = "SELECT money_posadka, money_posadka_bnal, money_km, money_km_bnal, money_km_zagorod, money_km_zagorod_bnal, money_minute, money_minute_bnal, tarif_change_desc FROM TARIF where ID = @ID";


                ;



                this.Text = "Редактирование текущго тарифа";
                this.Width = panel.Width + 24;
                this.Height = panel.Height + 80;
            }










            

        }


        public void doSetValue(string fieldName, object value)
        {
            if (Fields.ContainsKey(fieldName))
            {
                ((dictionary.QueryVisualParam)Fields[fieldName]).SetValue(value);
            }
        }

        public void doLoad(String dic_type, int _id)
        {
            m_iID = _id;
            doMakeInterface(dic_type);
    

    
//            if (_id > 0) 
            {

                Sql = Sql.Replace("@ID", _id.ToString());
                DataTable dt = DataBase.mssqlRead(Sql);
                if (dt.Rows.Count > 0) {
                    DataRow row = dt.Rows[0];
                    foreach (DataColumn column in dt.Columns) {
                        String name = column.ColumnName;
                        if (Fields.ContainsKey(name)) {
                            dictionary.QueryVisualParam visual_param = (dictionary.QueryVisualParam)Fields[name];
                            visual_param.SetValue(row[name]);
                        }
                    }

                }

                m_iID = _id;


            }


            if (table_name.CompareTo("ORGANIZATION_DETAIL")==0) {
                dictionary.QueryVisualParam visual_param = (dictionary.QueryVisualParam)Fields["ORGANIZATIONID"];

                if (this.Tag != null) {

                    visual_param.SetValue(this.Tag);
                }
            }

            if (table_name.CompareTo("BNAL_AKT") == 0) {
                dictionary.QueryVisualParam visual_param = (dictionary.QueryVisualParam)Fields["ORGANIZATION_ID"];

                if (this.Tag != null) {
                    visual_param.SetValue(this.Tag);
                }
            }
            


            



        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //обновить-вставить в таблицу.
            List<String> fields = new List<string>();
            List<object> values = new List<object>();

            
            
//            fields.AddRange(Fields.Keys);

            //сохранение бинарных объектов - позже
            foreach (String key in Fields.Keys) {
                if (Fields[key].Enabled) {
                    {

                        fields.Add(key);
                        //if (Fields[key].mQueryVisualParamType == dictionary.QueryVisualParam.QueryVisualParamType.Image)
//                            values.Add((byte[])Fields[key].getValue());
//                        else
                            values.Add(Fields[key].getValue());
                    }
                }
            }


            int result = 0;
            if (m_iID == 0) //insert
            {
                if (table_name.CompareTo("DRIVERS") == 0) {
                    String max_id = DataBase.mssqlReadString("SELECT cast(MAX(ID)+1 as varchar(10)) from DRIVERS");
                    int n = 0;
                    int.TryParse(max_id, out n);
                    fields.Add("ID");
                    values.Add(n);
                        
                }


                result = DataBase.mssqlInsert(table_name, fields.ToArray(), values.ToArray());
            }
            if (m_iID > 0) //update
            {

                if (table_name.CompareTo("TARIF") == 0)
                {
                    
                    
                    fields.Add("OPERATOR");
                    values.Add(MAIN_FORM.OperatorFIO);

                }

                if (table_name.CompareTo("ARENDA_REG") == 0)
                {
                    //автоматическое редактирование баланса в кошельке водителя
                    //особый случай - если вносится отрицательная сумма - ее с кошелька не списывать.
                    //т.е. баланс менять на сумму изменения но только в том случае если  новая сумма больше равно ноля


                    

                }

                result = DataBase.mssqlUpdate(table_name, fields.ToArray(), values.ToArray(), "ID", m_iID);
            }

            if (result == 1)
            if (table_name.CompareTo("ARENDA_REG") == 0)
            {
                if (this.parent is ArendaCar)
                {
                    ((ArendaCar)parent).btnReload_Click(null, null);
                }



            }


            if (result == 1)
                this.Close();


        }

        private void button1_Click(object sender, EventArgs e) {
            DataTable dt = DataBase.mssqlRead("SELECT fio, carid, photo  FROM DRIVERS where ID='" + m_iID.ToString() + "' and state=0");
            if (dt.Rows.Count <= 0)
                return;


            dataSet1.Tables["driver_card"].Clear();
            String fio = (String)dt.Rows[0]["fio"];
            fio = fio.Replace(' ', '\n');


            //object[] values = { (String)dt.Rows[0]["fio"], (int)dt.Rows[0]["carid"], (byte [])dt.Rows[0]["photo"] };
            object[] values = { fio, (int)dt.Rows[0]["carid"], dt.Rows[0]["photo"] };
            dataSet1.Tables["driver_card"].LoadDataRow(values, true);


            report1.Show();
        }




    }
}
