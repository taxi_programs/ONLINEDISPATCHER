﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ONLINEDISPATCHER
{
    public partial class send_mess_box : Form
    {
        public send_mess_box()
        {
            InitializeComponent();
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (checkedListBox1.GetItemChecked(0))
            {
                CAR_IDS.Text += " 1 299 ";
            }
            if (checkedListBox1.GetItemChecked(1))
            {
                CAR_IDS.Text += " 299 1 ";
            }   
        }

        private void checkedListBox1_ItemCheck(object sender, ItemCheckEventArgs e)
        {


        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (CAR_IDS.Text != "")
            {
                String[] car_ids = CAR_IDS.Text.Split(' ');
                ///удалить дубликаты!!!


                foreach (String car_id in car_ids)
                {
                    String[] fields = { "ComID", "OpID", "dtArrive", "Value1", "message", "State" };
                    object[] values = { 5, MAIN_FORM.OperatorID, DateTime.Now, car_id, MESS.Text, 0 };

                    DataBase.mssqlInsert("COMMANDS", fields, values);
                }
            }

            //MessageBox.Show("Сообщение отправлено");
            this.Close();
        }

        private void contextMenuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            MESS.Text = e.ClickedItem.Text;
        }
    }
}
