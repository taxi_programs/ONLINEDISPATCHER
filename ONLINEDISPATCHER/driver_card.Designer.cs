﻿namespace ONLINEDISPATCHER
{
    partial class driver_form
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(driver_form));
            this.btnEndCall = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btn_balance = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.text_car_name = new System.Windows.Forms.TextBox();
            this.text_driver_fio = new System.Windows.Forms.TextBox();
            this.text_comment = new System.Windows.Forms.TextBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btn_phones = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_open_driver = new System.Windows.Forms.Button();
            this.btn_phone3 = new System.Windows.Forms.Button();
            this.btn_phone2 = new System.Windows.Forms.Button();
            this.btn_open_car = new System.Windows.Forms.Button();
            this.btn_money = new System.Windows.Forms.Button();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.btn_new_order = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Бэйдж = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.block_descr = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.dateTimePicker4 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.panel2 = new System.Windows.Forms.Panel();
            this.easyCompletionComboBox1 = new SergeUtils.EasyCompletionComboBox();
            this.label_beznal = new System.Windows.Forms.Label();
            this.dataSet1 = new System.Data.DataSet();
            this.driver_card = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.report1 = new FastReport.Report();
            this.btnFromQueue = new System.Windows.Forms.Button();
            this.btnBlock30 = new System.Windows.Forms.Button();
            this.block60 = new System.Windows.Forms.Button();
            this.blockCancel = new System.Windows.Forms.Button();
            this.block15 = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.driver_card)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.report1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnEndCall
            // 
            this.btnEndCall.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnEndCall.ForeColor = System.Drawing.Color.Red;
            this.btnEndCall.Location = new System.Drawing.Point(291, 88);
            this.btnEndCall.Name = "btnEndCall";
            this.btnEndCall.Size = new System.Drawing.Size(116, 52);
            this.btnEndCall.TabIndex = 2;
            this.btnEndCall.TabStop = false;
            this.btnEndCall.Text = "Завершить разговор";
            this.btnEndCall.UseVisualStyleBackColor = true;
            this.btnEndCall.Visible = false;
            this.btnEndCall.Click += new System.EventHandler(this.btnEndCall_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(15, 90);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(114, 23);
            this.button1.TabIndex = 19;
            this.button1.TabStop = false;
            this.button1.Text = "Последние заказы";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_balance
            // 
            this.btn_balance.Location = new System.Drawing.Point(95, 120);
            this.btn_balance.Margin = new System.Windows.Forms.Padding(0);
            this.btn_balance.Name = "btn_balance";
            this.btn_balance.Size = new System.Drawing.Size(181, 23);
            this.btn_balance.TabIndex = 23;
            this.btn_balance.TabStop = false;
            this.btn_balance.Text = "-60 р. Обещанный платеж";
            this.btn_balance.UseVisualStyleBackColor = true;
            this.btn_balance.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(135, 90);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(100, 23);
            this.button3.TabIndex = 24;
            this.button3.TabStop = false;
            this.button3.Text = "Текущий заказ";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "Машина";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 26;
            this.label3.Text = "Водитель";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 27;
            this.label4.Text = "Телефон";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 72);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 28;
            this.label5.Text = "Телефон2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 96);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 29;
            this.label6.Text = "Телефон3";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 144);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 24);
            this.label7.TabIndex = 30;
            this.label7.Text = "Комментарий";
            // 
            // text_car_name
            // 
            this.text_car_name.Location = new System.Drawing.Point(98, 3);
            this.text_car_name.Name = "text_car_name";
            this.text_car_name.ReadOnly = true;
            this.text_car_name.Size = new System.Drawing.Size(274, 20);
            this.text_car_name.TabIndex = 32;
            this.text_car_name.TabStop = false;
            // 
            // text_driver_fio
            // 
            this.text_driver_fio.Location = new System.Drawing.Point(98, 27);
            this.text_driver_fio.Name = "text_driver_fio";
            this.text_driver_fio.ReadOnly = true;
            this.text_driver_fio.Size = new System.Drawing.Size(274, 20);
            this.text_driver_fio.TabIndex = 33;
            this.text_driver_fio.TabStop = false;
            // 
            // text_comment
            // 
            this.text_comment.Location = new System.Drawing.Point(98, 147);
            this.text_comment.Name = "text_comment";
            this.text_comment.ReadOnly = true;
            this.text_comment.Size = new System.Drawing.Size(274, 20);
            this.text_comment.TabIndex = 38;
            this.text_comment.TabStop = false;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Add.ico");
            this.imageList1.Images.SetKeyName(1, "Delete.ico");
            this.imageList1.Images.SetKeyName(2, "Earth.ico");
            this.imageList1.Images.SetKeyName(3, "Info.ico");
            this.imageList1.Images.SetKeyName(4, "29.ico");
            this.imageList1.Images.SetKeyName(5, "39.ico");
            this.imageList1.Images.SetKeyName(6, "call-start.png");
            this.imageList1.Images.SetKeyName(7, "documentinfo.png");
            this.imageList1.Images.SetKeyName(8, "mail-forward.png");
            this.imageList1.Images.SetKeyName(9, "call-stop.png");
            this.imageList1.Images.SetKeyName(10, "document-save.png");
            // 
            // btn_phones
            // 
            this.btn_phones.Location = new System.Drawing.Point(95, 48);
            this.btn_phones.Margin = new System.Windows.Forms.Padding(0);
            this.btn_phones.Name = "btn_phones";
            this.btn_phones.Size = new System.Drawing.Size(181, 23);
            this.btn_phones.TabIndex = 39;
            this.btn_phones.TabStop = false;
            this.btn_phones.UseVisualStyleBackColor = true;
            this.btn_phones.Click += new System.EventHandler(this.btn_phones_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 280F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.Controls.Add(this.btn_open_driver, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.btn_phone3, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.text_comment, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.btn_balance, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.btn_phone2, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btn_phones, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.text_driver_fio, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.text_car_name, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.btn_open_car, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.btn_money, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.linkLabel1, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.linkLabel2, 1, 5);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 146);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(410, 168);
            this.tableLayoutPanel1.TabIndex = 40;
            // 
            // btn_open_driver
            // 
            this.btn_open_driver.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_open_driver.Location = new System.Drawing.Point(378, 24);
            this.btn_open_driver.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.btn_open_driver.Name = "btn_open_driver";
            this.btn_open_driver.Size = new System.Drawing.Size(29, 21);
            this.btn_open_driver.TabIndex = 44;
            this.btn_open_driver.Text = "...";
            this.btn_open_driver.UseVisualStyleBackColor = true;
            this.btn_open_driver.Click += new System.EventHandler(this.btn_open_driver_Click);
            // 
            // btn_phone3
            // 
            this.btn_phone3.Location = new System.Drawing.Point(95, 96);
            this.btn_phone3.Margin = new System.Windows.Forms.Padding(0);
            this.btn_phone3.Name = "btn_phone3";
            this.btn_phone3.Size = new System.Drawing.Size(181, 23);
            this.btn_phone3.TabIndex = 42;
            this.btn_phone3.TabStop = false;
            this.btn_phone3.UseVisualStyleBackColor = true;
            this.btn_phone3.Click += new System.EventHandler(this.btn_phone3_Click);
            // 
            // btn_phone2
            // 
            this.btn_phone2.Location = new System.Drawing.Point(95, 72);
            this.btn_phone2.Margin = new System.Windows.Forms.Padding(0);
            this.btn_phone2.Name = "btn_phone2";
            this.btn_phone2.Size = new System.Drawing.Size(181, 23);
            this.btn_phone2.TabIndex = 41;
            this.btn_phone2.TabStop = false;
            this.btn_phone2.UseVisualStyleBackColor = true;
            this.btn_phone2.Click += new System.EventHandler(this.btn_phone2_Click);
            // 
            // btn_open_car
            // 
            this.btn_open_car.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_open_car.Location = new System.Drawing.Point(378, 0);
            this.btn_open_car.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.btn_open_car.Name = "btn_open_car";
            this.btn_open_car.Size = new System.Drawing.Size(29, 21);
            this.btn_open_car.TabIndex = 43;
            this.btn_open_car.Text = "...";
            this.btn_open_car.UseVisualStyleBackColor = true;
            this.btn_open_car.Click += new System.EventHandler(this.btn_open_car_Click);
            // 
            // btn_money
            // 
            this.btn_money.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_money.Location = new System.Drawing.Point(378, 120);
            this.btn_money.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.btn_money.Name = "btn_money";
            this.btn_money.Size = new System.Drawing.Size(29, 21);
            this.btn_money.TabIndex = 45;
            this.btn_money.Text = "...";
            this.btn_money.UseVisualStyleBackColor = true;
            this.btn_money.Click += new System.EventHandler(this.btn_money_Click);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(3, 120);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(44, 13);
            this.linkLabel1.TabIndex = 46;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Баланс";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Location = new System.Drawing.Point(73, 120);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(16, 13);
            this.linkLabel2.TabIndex = 47;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "...";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // btn_new_order
            // 
            this.btn_new_order.Location = new System.Drawing.Point(15, 117);
            this.btn_new_order.Name = "btn_new_order";
            this.btn_new_order.Size = new System.Drawing.Size(114, 23);
            this.btn_new_order.TabIndex = 41;
            this.btn_new_order.TabStop = false;
            this.btn_new_order.Text = "Новый заказ";
            this.btn_new_order.UseVisualStyleBackColor = true;
            this.btn_new_order.Click += new System.EventHandler(this.btn_new_order_Click);
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox1.Location = new System.Drawing.Point(19, 4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(52, 29);
            this.textBox1.TabIndex = 0;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(6, 19);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(138, 20);
            this.dateTimePicker1.TabIndex = 42;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(458, 225);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(72, 17);
            this.checkBox1.TabIndex = 43;
            this.checkBox1.Text = "На карте";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.Controls.Add(this.Бэйдж);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 324);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(426, 150);
            this.panel1.TabIndex = 44;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // Бэйдж
            // 
            this.Бэйдж.Location = new System.Drawing.Point(345, 89);
            this.Бэйдж.Name = "Бэйдж";
            this.Бэйдж.Size = new System.Drawing.Size(75, 23);
            this.Бэйдж.TabIndex = 45;
            this.Бэйдж.Text = "Бэйдж";
            this.Бэйдж.UseVisualStyleBackColor = true;
            this.Бэйдж.Click += new System.EventHandler(this.Бэйдж_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(345, 14);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(72, 46);
            this.button4.TabIndex = 44;
            this.button4.Text = "Показать на карте";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button9);
            this.groupBox1.Controls.Add(this.button8);
            this.groupBox1.Controls.Add(this.button6);
            this.groupBox1.Controls.Add(this.block_descr);
            this.groupBox1.Controls.Add(this.button7);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.dateTimePicker4);
            this.groupBox1.Controls.Add(this.dateTimePicker3);
            this.groupBox1.Controls.Add(this.dateTimePicker2);
            this.groupBox1.Controls.Add(this.dateTimePicker1);
            this.groupBox1.Location = new System.Drawing.Point(6, 14);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(313, 136);
            this.groupBox1.TabIndex = 43;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Маршрут";
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(222, 69);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 38);
            this.button9.TabIndex = 51;
            this.button9.Text = "Показать данные";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(161, 73);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(46, 23);
            this.button8.TabIndex = 50;
            this.button8.Text = "вчера";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(58, 73);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(46, 23);
            this.button6.TabIndex = 48;
            this.button6.Text = "4ч";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // block_descr
            // 
            this.block_descr.AutoSize = true;
            this.block_descr.Location = new System.Drawing.Point(75, 111);
            this.block_descr.Name = "block_descr";
            this.block_descr.Size = new System.Drawing.Size(49, 13);
            this.block_descr.TabIndex = 50;
            this.block_descr.Text = "              ";
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(110, 74);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(46, 23);
            this.button7.TabIndex = 49;
            this.button7.Text = "24ч";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(6, 73);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(46, 23);
            this.button5.TabIndex = 47;
            this.button5.Text = "2ч";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(222, 18);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 45);
            this.button2.TabIndex = 0;
            this.button2.Text = "Показать маршрут";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // dateTimePicker4
            // 
            this.dateTimePicker4.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePicker4.Location = new System.Drawing.Point(150, 45);
            this.dateTimePicker4.Name = "dateTimePicker4";
            this.dateTimePicker4.ShowUpDown = true;
            this.dateTimePicker4.Size = new System.Drawing.Size(67, 20);
            this.dateTimePicker4.TabIndex = 45;
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePicker3.Location = new System.Drawing.Point(150, 19);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.ShowUpDown = true;
            this.dateTimePicker3.Size = new System.Drawing.Size(67, 20);
            this.dateTimePicker3.TabIndex = 44;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(6, 45);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(138, 20);
            this.dateTimePicker2.TabIndex = 43;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.easyCompletionComboBox1);
            this.panel2.Controls.Add(this.label_beznal);
            this.panel2.Controls.Add(this.tableLayoutPanel1);
            this.panel2.Controls.Add(this.checkBox1);
            this.panel2.Controls.Add(this.textBox1);
            this.panel2.Controls.Add(this.btn_new_order);
            this.panel2.Controls.Add(this.btnEndCall);
            this.panel2.Controls.Add(this.button3);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(426, 324);
            this.panel2.TabIndex = 45;
            // 
            // easyCompletionComboBox1
            // 
            this.easyCompletionComboBox1.Location = new System.Drawing.Point(19, 49);
            this.easyCompletionComboBox1.Name = "easyCompletionComboBox1";
            this.easyCompletionComboBox1.Size = new System.Drawing.Size(392, 21);
            this.easyCompletionComboBox1.TabIndex = 0;
            this.easyCompletionComboBox1.SelectedIndexChanged += new System.EventHandler(this.easyCompletionComboBox1_SelectedIndexChanged);
            this.easyCompletionComboBox1.SelectedValueChanged += new System.EventHandler(this.easyCompletionComboBox1_SelectedValueChanged);
            // 
            // label_beznal
            // 
            this.label_beznal.AutoSize = true;
            this.label_beznal.BackColor = System.Drawing.SystemColors.Window;
            this.label_beznal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_beznal.ForeColor = System.Drawing.Color.Red;
            this.label_beznal.Location = new System.Drawing.Point(287, 9);
            this.label_beznal.Name = "label_beznal";
            this.label_beznal.Size = new System.Drawing.Size(124, 13);
            this.label_beznal.TabIndex = 44;
            this.label_beznal.Text = "НЕ ВОЗИТ БЕЗНАЛ";
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "NewDataSet";
            this.dataSet1.Tables.AddRange(new System.Data.DataTable[] {
            this.driver_card});
            // 
            // driver_card
            // 
            this.driver_card.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3});
            this.driver_card.TableName = "driver_card";
            // 
            // dataColumn1
            // 
            this.dataColumn1.ColumnName = "fio";
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "carid";
            this.dataColumn2.DataType = typeof(int);
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "photo";
            this.dataColumn3.DataType = typeof(byte[]);
            // 
            // report1
            // 
            this.report1.ReportResourceString = resources.GetString("report1.ReportResourceString");
            this.report1.RegisterData(this.dataSet1, "dataSet1");
            // 
            // btnFromQueue
            // 
            this.btnFromQueue.Location = new System.Drawing.Point(9, 480);
            this.btnFromQueue.Name = "btnFromQueue";
            this.btnFromQueue.Size = new System.Drawing.Size(99, 23);
            this.btnFromQueue.TabIndex = 46;
            this.btnFromQueue.Text = "Снять с очереди";
            this.btnFromQueue.UseVisualStyleBackColor = true;
            this.btnFromQueue.Click += new System.EventHandler(this.btnFromQueue_Click);
            // 
            // btnBlock30
            // 
            this.btnBlock30.Location = new System.Drawing.Point(200, 480);
            this.btnBlock30.Name = "btnBlock30";
            this.btnBlock30.Size = new System.Drawing.Size(57, 23);
            this.btnBlock30.TabIndex = 47;
            this.btnBlock30.Text = "Блок 30 мин";
            this.btnBlock30.UseVisualStyleBackColor = true;
            this.btnBlock30.Click += new System.EventHandler(this.btnBlock30_Click);
            // 
            // block60
            // 
            this.block60.Location = new System.Drawing.Point(263, 480);
            this.block60.Name = "block60";
            this.block60.Size = new System.Drawing.Size(67, 23);
            this.block60.TabIndex = 48;
            this.block60.Text = "Блок 60 мин";
            this.block60.UseVisualStyleBackColor = true;
            this.block60.Click += new System.EventHandler(this.block60_Click);
            // 
            // blockCancel
            // 
            this.blockCancel.Location = new System.Drawing.Point(345, 480);
            this.blockCancel.Name = "blockCancel";
            this.blockCancel.Size = new System.Drawing.Size(75, 23);
            this.blockCancel.TabIndex = 49;
            this.blockCancel.Text = "Снять блок";
            this.blockCancel.UseVisualStyleBackColor = true;
            this.blockCancel.Click += new System.EventHandler(this.blockCancel_Click);
            // 
            // block15
            // 
            this.block15.Location = new System.Drawing.Point(129, 480);
            this.block15.Name = "block15";
            this.block15.Size = new System.Drawing.Size(65, 23);
            this.block15.TabIndex = 51;
            this.block15.Text = "Блок 15 мин";
            this.block15.UseVisualStyleBackColor = true;
            this.block15.Click += new System.EventHandler(this.block15_Click);
            // 
            // driver_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(426, 509);
            this.Controls.Add(this.block15);
            this.Controls.Add(this.blockCancel);
            this.Controls.Add(this.block60);
            this.Controls.Add(this.btnBlock30);
            this.Controls.Add(this.btnFromQueue);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Name = "driver_form";
            this.Text = "Карточка водителя";
            this.Load += new System.EventHandler(this.driver_form_Load);
            this.Shown += new System.EventHandler(this.driver_form_Shown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.driver_form_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.driver_form_KeyDown);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.driver_card)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.report1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnEndCall;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btn_balance;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox text_car_name;
        private System.Windows.Forms.TextBox text_driver_fio;
        private System.Windows.Forms.TextBox text_comment;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button btn_phones;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btn_phone3;
        private System.Windows.Forms.Button btn_phone2;
        private System.Windows.Forms.Button btn_new_order;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DateTimePicker dateTimePicker4;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button btn_open_driver;
        private System.Windows.Forms.Button btn_open_car;
        private System.Windows.Forms.Button btn_money;
        private System.Windows.Forms.Button Бэйдж;
        private System.Data.DataSet dataSet1;
        private System.Data.DataTable driver_card;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private FastReport.Report report1;
        private System.Windows.Forms.Button btnFromQueue;
        private System.Windows.Forms.Button btnBlock30;
        private System.Windows.Forms.Button block60;
        private System.Windows.Forms.Button blockCancel;
        private System.Windows.Forms.Label block_descr;
        private System.Windows.Forms.Button block15;
        private System.Windows.Forms.Label label_beznal;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private SergeUtils.EasyCompletionComboBox easyCompletionComboBox1;
        public System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button9;
        
    }
}