﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Net;
using System.IO;
using System.Xml;
using Newtonsoft.Json;


namespace csmapcontrol {
    public class MapPath {
        public class PathPoint {
            public double Lat;
            public double Lon;
            public double Elevation;
            //public DateTime Time;
            public String Name = "";
            public PathPoint(double lat, double lon, String name){
                Lat = lat;
                Lon = lon;
                Name = name;
            }
            public PathPoint() {
                
            }


        }


        public List<PathPoint> Points = new List<PathPoint>();  //точки маршрута - для отображения
        public List<Marker> InterMarkers = new List<Marker>(); //задаваемые точки - через которые должен проходить маршрут
        public string PathName;
        private bool calcDone = true;

        public bool need_calculate = false;

        public double path_distance = 0;

        public  static String OSRM_SERVER = "192.168.100.102:5000";
        public static bool USE_NEW_OSRM_PROTOCOL = false;
        private static int osrm_timeout = 3000;


        /// <summary>
        /// Create a new empty Path.
        /// </summary>
        public MapPath() {
            InterMarkers.Add(new Marker(0, 0));//, name));
            InterMarkers.Add(new Marker(0, 0));//, name));
            InterMarkers.Add(new Marker(0, 0));//, name));
            InterMarkers.Add(new Marker(0, 0));//, name));
            InterMarkers.Add(new Marker(0, 0));//, name));
            InterMarkers.Add(new Marker(0, 0));//, name));
        }
        public void Clear()
        {
            Points.Clear();
            InterMarkers.Clear();
        }


        public Marker getFirstPoint()
        {
            if (InterMarkers.Count>0)
                return InterMarkers[0];

            return new Marker(0, 0);
        }

        public Marker getLastPoint()
        {
            if (InterMarkers.Count > 1)
                return InterMarkers[InterMarkers.Count-1];

            return new Marker(0, 0);
        }

        public Marker getInterPoint(int index)
        {
            if (InterMarkers.Count > index)
                return InterMarkers[index];

            return new Marker(0, 0);
        }
        public int getInterMarkersCount()
        {
            return InterMarkers.Count;
        }

        public void RemoveInterPoint(Marker m)
        {
            for (int i = 1; i < InterMarkers.Count - 1; i++)
            {
                if (InterMarkers[i] == m)
                {
                    InterMarkers.RemoveAt(i);
                    need_calculate = true;
                    break;
                }
            }
        }

        public void setFirstPoint(double lat, double lon, String name)
        {
            if (InterMarkers.Count == 0)
            {
                InterMarkers.Add(new Marker(lat, lon));//, name));
            } else {
                InterMarkers[0].lat = lat;
                InterMarkers[0].lon = lon;
                //InterPoints[0].Name = name;
            }
        }

        public void setPoint(int index, double lat, double lon, String name) {
            while (InterMarkers.Count < index + 1)
            {
                InterMarkers.Add(new Marker());
             }

            InterMarkers[index].lat = lat;
            InterMarkers[index].lon = lon;
             //InterPoints[index].Name = name;
        }

        public void setLastPoint(double lat, double lon, String name) {
            if (InterMarkers.Count == 0)
            {
                while (InterMarkers.Count < 2)
                {
                    InterMarkers.Add(new Marker(lat, lon));
                    
                }
            } else {

                if (InterMarkers.Count == 1)
                {
                    InterMarkers.Add(new Marker(lat, lon));
                } else {
                    InterMarkers[InterMarkers.Count - 1].lat = lat;
                    InterMarkers[InterMarkers.Count - 1].lon = lon;
                    //InterMarkers[InterMarkers.Count - 1].Name = name;
                }
            }
        }
        /*просто добавляет точку к концу*/
        public void addLastPoint(double lat, double lon, String name) {
            InterMarkers.Add(new Marker(lat, lon));
        }
        /*Добавляет промежуточные точки но не трогает первую и последнюю
         */
       /* public Marker addInterPoint(double lat, double lon, String name) {
            
  
            while (InterMarkers.Count < 2)
            {
                InterMarkers.Add(new Marker(lat, lon));
            }
            //вставить предпоследнюю точку.
            Marker result = new Marker(lat, lon);
            InterMarkers.Insert(InterMarkers.Count - 1, result);
            return result;

        }
*/


        public void calculatePath(int timeout) {
            int result = 0;


            this.Points.Clear();

            //calcDone = false;
            //входные параметры маршрута - указать откуда куда
            //запрос к серверу - сохранение координат и дистанции
            String req_coord = "";// String.Format("loc={0:.0000}|{1:.0000}&loc={2:.0000}|{3:.0000}", from.lat, from.lon, to.lat, to.lon);


            //NEW GET http://{server}/nearest/v1/{profile}/{coordinates}.json?number={number}
            //coord = http://router.project-osrm.org/route/v1/driving/13.388860,52.517037;13.397634,52.529407;13.428555,52.523219?overview=false'
            //coords = long,lat;long,lat
            String req_coord_new = "";// String.Format("loc={0:.0000}|{1:.0000}&loc={2:.0000}|{3:.0000}", from.lat, from.lon, to.lat, to.lon);

            //Prepare request string....


            for (int i = 0; i < InterMarkers.Count; i++)
            {
                Marker from, to;
                lock (InterMarkers)
                {
                    if ((InterMarkers[i].lat == 0) || (InterMarkers[i].lon == 0))
                        continue;
                    String loc_str = String.Format("loc={0:.00000}|{1:.00000}", InterMarkers[i].lat, InterMarkers[i].lon);
                    String loc_str_new = String.Format("{0:.00000}|{1:.00000}", InterMarkers[i].lon, InterMarkers[i].lat);

                    if (req_coord != "")
                    {
                        req_coord += "&";
                        req_coord_new += ";";
                    }
                    req_coord += loc_str;
                    req_coord_new += loc_str_new;
                }
            }


            if (USE_NEW_OSRM_PROTOCOL)
                result = RequestPathNew(req_coord_new);
            else
                result = RequestPath(req_coord);


            

            int n = 0;
            int tryCount = 0;
            
            Marker MovedMarker = null;

            while (result == 0)
            {
                
                n = tryCount / 4;
                if (tryCount >= 16)
                        break;
               
                if (result == 0)
                {

                    switch (tryCount - n * 4)
                    {
                        case 0: if (MovedMarker != null) { MovedMarker.lat += 0.0002; MovedMarker.lon -= 0.0000; MovedMarker = null; }; if (InterMarkers[n].lat != 0) { InterMarkers[n].lat += 0.0001; InterMarkers[n].lon += 0.0001; } break;
                        case 1: if (InterMarkers[n].lon != 0) InterMarkers[n].lon -= 0.0002; break;
                        case 2: if (InterMarkers[n].lat != 0) InterMarkers[n].lat -= 0.0002; break;
                        case 3: if (InterMarkers[n].lon != 0) InterMarkers[n].lon += 0.0002; break;
                    }
                    if (InterMarkers[n].lat != 0)
                        MovedMarker = InterMarkers[n];
                    tryCount++;
                    



                    if (InterMarkers[n].lat == 0)
                        continue;

                    req_coord = "";
                    req_coord_new = "";

                    //посчтиаем растояние
                    //


                    for (int i = 0; i < InterMarkers.Count; i++)
                    {
                        Marker from, to;
                        lock (InterMarkers)
                        {
                            if ((InterMarkers[i].lat == 0) || (InterMarkers[i].lon == 0))
                                continue;
                            String loc_str = String.Format("loc={0:.00000}|{1:.00000}", InterMarkers[i].lat, InterMarkers[i].lon);
                            String loc_str_new = String.Format("{0:.00000},{1:.00000}", InterMarkers[i].lon, InterMarkers[i].lat);

                            if (req_coord != "")
                            {
                                req_coord += "&";
                                req_coord_new += ";";
                            }
                            req_coord += loc_str;
                            req_coord_new += loc_str_new;
                        }
                    }

                    if (USE_NEW_OSRM_PROTOCOL)
                        result = RequestPathNew(req_coord_new);
                    else
                        result = RequestPath(req_coord);

                    System.Console.Error.WriteLine("Try: " + tryCount + ". Distance " + result + " Request: " + req_coord);

                }


               
            }



            System.Console.Error.WriteLine("ITOG: Try: "+tryCount+". Distance " + result + " Request: " + req_coord);
            

            if (result > 0)
                result += 400;
            
            path_distance = result / 1000.0;


            need_calculate = false;

            //calcDone = true;


            
        }

        private static string GetHtmlPageText(string url, int timeout)
        {
            string result = String.Empty;





            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            if (MapControl.webProxy != null)
                request.Proxy = MapControl.webProxy;
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.52 Safari/537.17";
            request.Timeout = timeout;
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            result = reader.ReadToEnd();
                        }
                    }
                }
            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.Timeout)
                    switch_osrm_server();
                System.Console.WriteLine("Ошибка получения маршрута. " + e.Message);
            }

            return result;
        }

        private static void switch_osrm_server()
        {
            return;
            System.Console.WriteLine("switch_osrm_server");
            if (OSRM_SERVER == "router.project-osrm.org")
                OSRM_SERVER = "192.168.100.102:5000";
            else
                OSRM_SERVER = "router.project-osrm.org";


            if (OSRM_SERVER == "router.project-osrm.org")
                osrm_timeout = 3000;
            else
                osrm_timeout = 100;
            
        }



        private int RequestPath(String request)
        {
            int result = 0;

           


            int timeout = osrm_timeout;



            String req_coord = "";// String.Format("flat={0:.0000}&flon={1:.0000}&tlat={2:.0000}&tlon={3:.0000}", from.lat, from.lon, to.lat, to.lon);
            //req_coord = req_coord.Replace(',', '.');
            //string url = "http://www.yournavigation.org/api/1.0/gosmore.php.old?format=kml&" +
            //               req_coord +// "flat=57.6929&flon=39.7779&tlat=57.5930&tlon=39.8653"+

            //http://router.project-osrm.org/viaroute?loc=57.6937,39.7793&loc=57.7245727976243,39.829569578277&output=gpx

            //req_coord = String.Format("loc={0:.0000}|{1:.0000}&loc={2:.0000}|{3:.0000}", from.lat, from.lon, to.lat, to.lon);
            req_coord = request;
            req_coord = req_coord.Replace(',', '.');
            req_coord = req_coord.Replace('|', ',');


            //OSRM_SERVER = "192.168.10.200:5000";
            //OSRM_SERVER = "192.168.100.102:5000";


            string url = "http://" + OSRM_SERVER + "/viaroute?output=json&" + req_coord;

            //"&v=motorcar&fast=1&layer=mapnik&instructions=1";
            //{"version": 0.3,"status":0,"status_message": "Found route between points","route_geometry": "}gc_JsmxqFxAzAjCnCf@ZjD|I`@dA^|@Rp@Tg@pEgJd@aA`@u@`@{@N[j@iAhQs^JSl@mAN]P[f@gAzCmG`FiKZo@JUjFsKFMbG}LdNeYNYrAsC`BeDdAyBVg@FOLULWjCuFJUtAsCXk@zEuJtEmJbAcDl@kDb@iE^aHfCoc@f@gFl@gD`AaCj@aAfDoFbBmCJOv@y@vAsAvAy@~Ak@l@Mt@Mp@KjBSZBLBLHJJHPFPDVBV?XA\\EXGTKXmCtFW\\QLMDM?MCKMKOISGYCWCc@E{AEcCMwEY}EkAySSeDYkFQ_FI}DAuFPyPBkFE{FW{Fe@eEeAoF_BuGeJ{Yen@_`Ce@_Bi@iB_@eAa@cAs@yAs@oAq@gAsGcJ_A{Am@iAi@iAe@oAi@uAe@}Aa@cB]cB]mBYwBSaBSsBKmBIkBGkBCgB?aBBmBDmBFuBLwBPyBVwBXmBd@kCfEwShA_Fl@qCX}APiALeAF_AB}@@mAAiAGmEIqHMuGEy@Ii@Mi@M[Ug@_Mx@kA@U@qFFW?Y@oDDg@Ai@@gBGm@~BCJwEvQw@~CyBfJ}@dDwAnF_GnUuZnlAuGtWcNzg@Qj@Q`@OZMTQPOLSHOBS@_@?UAW@SBOBMFYLWPOPMRMZsQ~k@sM~`@KXa@`Ae@jAUh@s@pAa@r@]h@Wf@Qj@Oj@UdAW~AmEd[qAbJwC|SQtAI|@OhAYvAWjAa@rAk@dBg@nAa@t@c@r@UZSPQJsBfAkAp@c@XcDbCs@p@e@\\eC|BsBlBiE~DmKzJeD|CmBhBC@SR_EzDiAfAq@v@[{@{@eCOe@yBmGmBqFiAcDP]Xo@~@gBmAoD","route_instructions": [],"route_summary":{"total_distance":17347,"total_time":1077,"start_point":"улица Панина","end_point":""},"alternative_geometries": [],"alternative_instructions":[],"alternative_summaries":[],"route_name":["",""],"alternative_names":[["",""]],"via_points":[[57.69359,39.77962 ],[57.72467,39.82939 ]],"hint_data": {"checksum":1693994188, "locations": ["-M8xAEkoAAAuAAAANAAAAPLMssarNd4_jwhYAOqyPAB", "bA9kAAAAAACrAAAApAAAACmjJwm9beA_sxRYAFvGPAA"]},"transactionId": "OSRM Routing Engine JSON Descriptor (v0.3)"}

            // System.Console.WriteLine("URL=" + url);
            //return "0.0";

            //
            //            <?xml version="1.0" encoding="UTF-8"?><gpx creator="OSRM Routing Engine" version="1.1" xmlns="http://www.topografix.com/GPX/1/1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 gpx.xsd"><rte><rtept lat="57.68720" lon="39.76817"></rtept><rtept lat="57.68811" lon="39.76999"></rtept><rtept lat="57.68830" lon="39.77037"></rtept><rtept lat="57.68868" lon="39.77114"></rtept><rtept lat="57.68938" lon="39.77254"></rtept><rtept lat="57.68952" lon="39.77282"></rtept><rtept lat="57.69063" lon="39.77505"></rtept><rtept lat="57.69095" lon="39.77564"></rtept><rtept lat="57.69105" lon="39.77589"></rtept><rtept lat="57.69121" lon="39.77620"></rtept><rtept lat="57.69138" lon="39.77655"></rtept><rtept lat="57.69224" lon="39.77830"></rtept><rtept lat="57.69244" lon="39.77844"></rtept><rtept lat="57.69314" lon="39.77916"></rtept><rtept lat="57.69400" lon="39.78003"></rtept><rtept lat="57.69478" lon="39.78091"></rtept><rtept lat="57.69537" lon="39.77986"></rtept><rtept lat="57.69559" lon="39.78030"></rtept><rtept lat="57.69544" lon="39.78057"></rtept><rtept lat="57.69544" lon="39.78057"></rtept></rte></gpx>            
            //
            String response = MapPath.GetHtmlPageText(url, timeout);
            if (response == "")
            {
                calcDone = true;

                System.Console.WriteLine("Пустой ответ - ошибка расчета");
                return 0;
            }
            //"total_distance":17347,
            int idx_a = response.IndexOf("total_distance\":");
            if (idx_a > 0)
            {
                idx_a += "total_distance\":".Length;
                int idx_b = response.IndexOf(",", idx_a);
                if (idx_b > idx_a)
                {
                    String distance = response.Substring(idx_a, idx_b - idx_a);
                    int i_dist = 0;
                    int.TryParse(distance, out i_dist);

                    if (i_dist == 0)
                    {
                        return 0;
                    }

                    result = i_dist;


                }

            }




            url = "http://" + OSRM_SERVER + "/viaroute?output=gpx&" + req_coord;

            response = MapPath.GetHtmlPageText(url, timeout);
            if (response == "")
            {
                calcDone = true;

                //System.Console.WriteLine("Пустой ответ - ошибка расчета");
                //return 0;
            }

            //response = response.Replace("\\\"", "\"");

            //    System.Console.WriteLine(response);

            /* OSRM
             * 
             */


            // List<PathPoint> Points = new List<PathPoint>();


            XmlDocument x = new XmlDocument();
            x.LoadXml(response);
            if (x.DocumentElement.Name != "gpx")
                throw new ApplicationException("Document element should be 'gpx'");
            string ns = "http://www.topografix.com/GPX/1/0";
            string ver = x.DocumentElement.GetAttribute("version");
            if (ver != null && ver == "1.1")
                ns = "http://www.topografix.com/GPX/1/1";
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(x.NameTable);
            nsmgr.AddNamespace("gpx", ns);
            //Name = x.GetElementsByTagName("name")[0].InnerText;
            XmlNode base_node = x.SelectNodes("rte", nsmgr)[0];
            //foreach (XmlNode point in x.SelectNodes("rtept", nsmgr))
            foreach (XmlNode point in x.GetElementsByTagName("rtept"))
            {
                PathPoint p = new PathPoint();
                p.Lat = double.Parse(point.Attributes["lat"].Value.Replace('.', ','));
                p.Lon = double.Parse(point.Attributes["lon"].Value.Replace('.', ','));
                //                p.Elevation = double.Parse(point.SelectSingleNode("gpx:ele", nsmgr).InnerText);
                //                p.Time = DateTime.Parse(point.SelectSingleNode("gpx:time", nsmgr).InnerText).ToUniversalTime();
                Points.Add(p);
            }

            return result;

            /*


            int idx_a = response.IndexOf("<rtept");
            if (idx_a > 0)
            {
                response = response.Substring(idx_a);

                response = response.Replace("</rte></gpx>", "");
                response = response.Replace("</rtept>", "");
                response = response.Replace("<rtept ", "");


                String[] latlon_array = response.Split(">");


                

                


            }

*/


            return result;

            /* END OSRM
             */




            //String response = reader.ReadToEnd();
            //System.Console.WriteLine(response);

            int b = response.IndexOf("<coordinates>");
            int e = response.IndexOf("</coordinates>");
            String c = response.Substring(b + "<coordinates>".Length, e - b - "<coordinates>".Length);


            String[] c_arr = c.Split('\n');
            //Track t = new Track();
            for (int i = 0; i < c_arr.Length; i++)
            {
                String[] c1 = c_arr[i].Split(',');
                if (c1.Length == 2)
                {
                    c1[0] = c1[0].Replace('.', ',');
                    c1[1] = c1[1].Replace('.', ',');
                    PathPoint pp = new PathPoint();
                    Double.TryParse(c1[0], out pp.Lon);
                    Double.TryParse(c1[1], out pp.Lat);

                    this.Points.Add(pp);

                }
            }


            b = response.IndexOf("<distance>");
            e = response.IndexOf("</distance>");
            String distance1 = response.Substring(b + "<distance>".Length, e - b - "<distance>".Length);
            distance1 = distance1.Replace('.', ',');
            // result = distance1;

            return result;
        }


        private int RequestPathNew(String request)
        {
            int result = 0;




            int timeout = osrm_timeout;



            String req_coord = "";// String.Format("flat={0:.0000}&flon={1:.0000}&tlat={2:.0000}&tlon={3:.0000}", from.lat, from.lon, to.lat, to.lon);
            //req_coord = req_coord.Replace(',', '.');
            //string url = "http://www.yournavigation.org/api/1.0/gosmore.php.old?format=kml&" +
            //               req_coord +// "flat=57.6929&flon=39.7779&tlat=57.5930&tlon=39.8653"+

            //http://router.project-osrm.org/viaroute?loc=57.6937,39.7793&loc=57.7245727976243,39.829569578277&output=gpx

            //req_coord = String.Format("loc={0:.0000}|{1:.0000}&loc={2:.0000}|{3:.0000}", from.lat, from.lon, to.lat, to.lon);
            req_coord = request;
            req_coord = req_coord.Replace(',', '.');
            req_coord = req_coord.Replace('|', ',');


            //OSRM_SERVER = "192.168.10.200:5000";
            //OSRM_SERVER = "192.168.100.102:5000";


            string url = "http://" + OSRM_SERVER + "/route/v1/car/" + req_coord + ".json?geometries=geojson";

            //"&v=motorcar&fast=1&layer=mapnik&instructions=1";
            //{"version": 0.3,"status":0,"status_message": "Found route between points","route_geometry": "}gc_JsmxqFxAzAjCnCf@ZjD|I`@dA^|@Rp@Tg@pEgJd@aA`@u@`@{@N[j@iAhQs^JSl@mAN]P[f@gAzCmG`FiKZo@JUjFsKFMbG}LdNeYNYrAsC`BeDdAyBVg@FOLULWjCuFJUtAsCXk@zEuJtEmJbAcDl@kDb@iE^aHfCoc@f@gFl@gD`AaCj@aAfDoFbBmCJOv@y@vAsAvAy@~Ak@l@Mt@Mp@KjBSZBLBLHJJHPFPDVBV?XA\\EXGTKXmCtFW\\QLMDM?MCKMKOISGYCWCc@E{AEcCMwEY}EkAySSeDYkFQ_FI}DAuFPyPBkFE{FW{Fe@eEeAoF_BuGeJ{Yen@_`Ce@_Bi@iB_@eAa@cAs@yAs@oAq@gAsGcJ_A{Am@iAi@iAe@oAi@uAe@}Aa@cB]cB]mBYwBSaBSsBKmBIkBGkBCgB?aBBmBDmBFuBLwBPyBVwBXmBd@kCfEwShA_Fl@qCX}APiALeAF_AB}@@mAAiAGmEIqHMuGEy@Ii@Mi@M[Ug@_Mx@kA@U@qFFW?Y@oDDg@Ai@@gBGm@~BCJwEvQw@~CyBfJ}@dDwAnF_GnUuZnlAuGtWcNzg@Qj@Q`@OZMTQPOLSHOBS@_@?UAW@SBOBMFYLWPOPMRMZsQ~k@sM~`@KXa@`Ae@jAUh@s@pAa@r@]h@Wf@Qj@Oj@UdAW~AmEd[qAbJwC|SQtAI|@OhAYvAWjAa@rAk@dBg@nAa@t@c@r@UZSPQJsBfAkAp@c@XcDbCs@p@e@\\eC|BsBlBiE~DmKzJeD|CmBhBC@SR_EzDiAfAq@v@[{@{@eCOe@yBmGmBqFiAcDP]Xo@~@gBmAoD","route_instructions": [],"route_summary":{"total_distance":17347,"total_time":1077,"start_point":"улица Панина","end_point":""},"alternative_geometries": [],"alternative_instructions":[],"alternative_summaries":[],"route_name":["",""],"alternative_names":[["",""]],"via_points":[[57.69359,39.77962 ],[57.72467,39.82939 ]],"hint_data": {"checksum":1693994188, "locations": ["-M8xAEkoAAAuAAAANAAAAPLMssarNd4_jwhYAOqyPAB", "bA9kAAAAAACrAAAApAAAACmjJwm9beA_sxRYAFvGPAA"]},"transactionId": "OSRM Routing Engine JSON Descriptor (v0.3)"}

            // System.Console.WriteLine("URL=" + url);
            //return "0.0";

            //
            //            <?xml version="1.0" encoding="UTF-8"?><gpx creator="OSRM Routing Engine" version="1.1" xmlns="http://www.topografix.com/GPX/1/1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 gpx.xsd"><rte><rtept lat="57.68720" lon="39.76817"></rtept><rtept lat="57.68811" lon="39.76999"></rtept><rtept lat="57.68830" lon="39.77037"></rtept><rtept lat="57.68868" lon="39.77114"></rtept><rtept lat="57.68938" lon="39.77254"></rtept><rtept lat="57.68952" lon="39.77282"></rtept><rtept lat="57.69063" lon="39.77505"></rtept><rtept lat="57.69095" lon="39.77564"></rtept><rtept lat="57.69105" lon="39.77589"></rtept><rtept lat="57.69121" lon="39.77620"></rtept><rtept lat="57.69138" lon="39.77655"></rtept><rtept lat="57.69224" lon="39.77830"></rtept><rtept lat="57.69244" lon="39.77844"></rtept><rtept lat="57.69314" lon="39.77916"></rtept><rtept lat="57.69400" lon="39.78003"></rtept><rtept lat="57.69478" lon="39.78091"></rtept><rtept lat="57.69537" lon="39.77986"></rtept><rtept lat="57.69559" lon="39.78030"></rtept><rtept lat="57.69544" lon="39.78057"></rtept><rtept lat="57.69544" lon="39.78057"></rtept></rte></gpx>            
            //
            String response = MapPath.GetHtmlPageText(url, timeout);
            if (response == "")
            {
                calcDone = true;

                System.Console.WriteLine("Пустой ответ - ошибка расчета");
                return 0;
            }



            Newtonsoft.Json.Linq.JObject jresponse = (Newtonsoft.Json.Linq.JObject)JsonConvert.DeserializeObject(response);
            








            //"total_distance":17347,
           
            String distance = (String) jresponse["routes"][0]["distance"];
            distance = distance.Replace(".", ",");
            double f_dist = 0;
            double.TryParse(distance, out f_dist);
            int i_dist = (int)f_dist;
            

            if (i_dist == 0)
            {
                return 0;
            }

            result = i_dist;

            Newtonsoft.Json.Linq.JArray jcoords = (Newtonsoft.Json.Linq.JArray)jresponse["routes"][0]["geometry"]["coordinates"];

            foreach (Newtonsoft.Json.Linq.JArray point in jcoords)
            {
                PathPoint p = new PathPoint();

                double lat = 0;
                double lon = 0;
                double.TryParse(point[0].ToString(), out lon);
                double.TryParse(point[1].ToString(), out lat);

                p.Lat = lat;
                p.Lon = lon;
                //                p.Elevation = double.Parse(point.SelectSingleNode("gpx:ele", nsmgr).InnerText);
                //                p.Time = DateTime.Parse(point.SelectSingleNode("gpx:time", nsmgr).InnerText).ToUniversalTime();
                Points.Add(p);
            }

            return result;

            /*


            int idx_a = response.IndexOf("<rtept");
            if (idx_a > 0)
            {
                response = response.Substring(idx_a);

                response = response.Replace("</rte></gpx>", "");
                response = response.Replace("</rtept>", "");
                response = response.Replace("<rtept ", "");


                String[] latlon_array = response.Split(">");


                

                


            }

*/


            return result;

            /* END OSRM
             */




            //String response = reader.ReadToEnd();
            //System.Console.WriteLine(response);

            int b = response.IndexOf("<coordinates>");
            int e = response.IndexOf("</coordinates>");
            String c = response.Substring(b + "<coordinates>".Length, e - b - "<coordinates>".Length);


            String[] c_arr = c.Split('\n');
            //Track t = new Track();
            for (int i = 0; i < c_arr.Length; i++)
            {
                String[] c1 = c_arr[i].Split(',');
                if (c1.Length == 2)
                {
                    c1[0] = c1[0].Replace('.', ',');
                    c1[1] = c1[1].Replace('.', ',');
                    PathPoint pp = new PathPoint();
                    Double.TryParse(c1[0], out pp.Lon);
                    Double.TryParse(c1[1], out pp.Lat);

                    this.Points.Add(pp);

                }
            }


            b = response.IndexOf("<distance>");
            e = response.IndexOf("</distance>");
            String distance1 = response.Substring(b + "<distance>".Length, e - b - "<distance>".Length);
            distance1 = distance1.Replace('.', ',');
            // result = distance1;

            return result;
        }

        private int calculatePathInterval(Marker from, Marker to, int timeout, String request)
        {
            int result = 0;

            if ((from.lat == to.lat) && (from.lon == to.lon))
            return 0;


            timeout = osrm_timeout;


            
            String req_coord = String.Format("flat={0:.0000}&flon={1:.0000}&tlat={2:.0000}&tlon={3:.0000}", from.lat, from.lon, to.lat, to.lon);
            req_coord = req_coord.Replace(',', '.');
            //string url = "http://www.yournavigation.org/api/1.0/gosmore.php.old?format=kml&" +
//               req_coord +// "flat=57.6929&flon=39.7779&tlat=57.5930&tlon=39.8653"+

//http://router.project-osrm.org/viaroute?loc=57.6937,39.7793&loc=57.7245727976243,39.829569578277&output=gpx
            
            req_coord = String.Format("loc={0:.0000}|{1:.0000}&loc={2:.0000}|{3:.0000}", from.lat, from.lon, to.lat, to.lon);
            req_coord = request;
            req_coord = req_coord.Replace(',', '.');
            req_coord = req_coord.Replace('|', ',');

            
            //OSRM_SERVER = "192.168.10.200:5000";
            //OSRM_SERVER = "192.168.100.102:5000";


            string url = "http://"+OSRM_SERVER+"/viaroute?output=json&" + req_coord;

                //"&v=motorcar&fast=1&layer=mapnik&instructions=1";
            //{"version": 0.3,"status":0,"status_message": "Found route between points","route_geometry": "}gc_JsmxqFxAzAjCnCf@ZjD|I`@dA^|@Rp@Tg@pEgJd@aA`@u@`@{@N[j@iAhQs^JSl@mAN]P[f@gAzCmG`FiKZo@JUjFsKFMbG}LdNeYNYrAsC`BeDdAyBVg@FOLULWjCuFJUtAsCXk@zEuJtEmJbAcDl@kDb@iE^aHfCoc@f@gFl@gD`AaCj@aAfDoFbBmCJOv@y@vAsAvAy@~Ak@l@Mt@Mp@KjBSZBLBLHJJHPFPDVBV?XA\\EXGTKXmCtFW\\QLMDM?MCKMKOISGYCWCc@E{AEcCMwEY}EkAySSeDYkFQ_FI}DAuFPyPBkFE{FW{Fe@eEeAoF_BuGeJ{Yen@_`Ce@_Bi@iB_@eAa@cAs@yAs@oAq@gAsGcJ_A{Am@iAi@iAe@oAi@uAe@}Aa@cB]cB]mBYwBSaBSsBKmBIkBGkBCgB?aBBmBDmBFuBLwBPyBVwBXmBd@kCfEwShA_Fl@qCX}APiALeAF_AB}@@mAAiAGmEIqHMuGEy@Ii@Mi@M[Ug@_Mx@kA@U@qFFW?Y@oDDg@Ai@@gBGm@~BCJwEvQw@~CyBfJ}@dDwAnF_GnUuZnlAuGtWcNzg@Qj@Q`@OZMTQPOLSHOBS@_@?UAW@SBOBMFYLWPOPMRMZsQ~k@sM~`@KXa@`Ae@jAUh@s@pAa@r@]h@Wf@Qj@Oj@UdAW~AmEd[qAbJwC|SQtAI|@OhAYvAWjAa@rAk@dBg@nAa@t@c@r@UZSPQJsBfAkAp@c@XcDbCs@p@e@\\eC|BsBlBiE~DmKzJeD|CmBhBC@SR_EzDiAfAq@v@[{@{@eCOe@yBmGmBqFiAcDP]Xo@~@gBmAoD","route_instructions": [],"route_summary":{"total_distance":17347,"total_time":1077,"start_point":"улица Панина","end_point":""},"alternative_geometries": [],"alternative_instructions":[],"alternative_summaries":[],"route_name":["",""],"alternative_names":[["",""]],"via_points":[[57.69359,39.77962 ],[57.72467,39.82939 ]],"hint_data": {"checksum":1693994188, "locations": ["-M8xAEkoAAAuAAAANAAAAPLMssarNd4_jwhYAOqyPAB", "bA9kAAAAAACrAAAApAAAACmjJwm9beA_sxRYAFvGPAA"]},"transactionId": "OSRM Routing Engine JSON Descriptor (v0.3)"}

           // System.Console.WriteLine("URL=" + url);
            //return "0.0";

//
//            <?xml version="1.0" encoding="UTF-8"?><gpx creator="OSRM Routing Engine" version="1.1" xmlns="http://www.topografix.com/GPX/1/1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 gpx.xsd"><rte><rtept lat="57.68720" lon="39.76817"></rtept><rtept lat="57.68811" lon="39.76999"></rtept><rtept lat="57.68830" lon="39.77037"></rtept><rtept lat="57.68868" lon="39.77114"></rtept><rtept lat="57.68938" lon="39.77254"></rtept><rtept lat="57.68952" lon="39.77282"></rtept><rtept lat="57.69063" lon="39.77505"></rtept><rtept lat="57.69095" lon="39.77564"></rtept><rtept lat="57.69105" lon="39.77589"></rtept><rtept lat="57.69121" lon="39.77620"></rtept><rtept lat="57.69138" lon="39.77655"></rtept><rtept lat="57.69224" lon="39.77830"></rtept><rtept lat="57.69244" lon="39.77844"></rtept><rtept lat="57.69314" lon="39.77916"></rtept><rtept lat="57.69400" lon="39.78003"></rtept><rtept lat="57.69478" lon="39.78091"></rtept><rtept lat="57.69537" lon="39.77986"></rtept><rtept lat="57.69559" lon="39.78030"></rtept><rtept lat="57.69544" lon="39.78057"></rtept><rtept lat="57.69544" lon="39.78057"></rtept></rte></gpx>            
//
            String response = MapPath.GetHtmlPageText(url, timeout);
            if (response == "") {
                calcDone = true;

                System.Console.WriteLine("Пустой ответ - ошибка расчета");
                return 0;
            }
            //"total_distance":17347,
            int idx_a = response.IndexOf("total_distance\":");
            if (idx_a > 0)
            {
                idx_a += "total_distance\":".Length;
                int idx_b = response.IndexOf(",", idx_a);
                if (idx_b > idx_a)
                {
                    String distance = response.Substring(idx_a, idx_b - idx_a);
                    int i_dist = 0;
                    int.TryParse(distance, out i_dist);

                    if (i_dist == 0)
                    {
                        return 0;
                    }

                    result = i_dist;
                    

                }

            }




            url = "http://" + OSRM_SERVER + "/viaroute?output=gpx&" + req_coord;

            response = MapPath.GetHtmlPageText(url, timeout);
            if (response == "")
            {
                calcDone = true;

                //System.Console.WriteLine("Пустой ответ - ошибка расчета");
                //return 0;
            }

            //response = response.Replace("\\\"", "\"");

        //    System.Console.WriteLine(response);

            /* OSRM
             * 
             */
            

           // List<PathPoint> Points = new List<PathPoint>();


            XmlDocument x = new XmlDocument();
            x.LoadXml(response);
            if (x.DocumentElement.Name != "gpx")
                throw new ApplicationException("Document element should be 'gpx'");
            string ns = "http://www.topografix.com/GPX/1/0";
            string ver = x.DocumentElement.GetAttribute("version");
            if (ver != null && ver == "1.1")
                ns = "http://www.topografix.com/GPX/1/1";
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(x.NameTable);
            nsmgr.AddNamespace("gpx", ns);
            //Name = x.GetElementsByTagName("name")[0].InnerText;
            XmlNode base_node = x.SelectNodes("rte", nsmgr)[0];
            //foreach (XmlNode point in x.SelectNodes("rtept", nsmgr))
            foreach (XmlNode point in x.GetElementsByTagName("rtept"))
            
            {
                PathPoint p = new PathPoint();
                p.Lat = double.Parse(point.Attributes["lat"].Value.Replace('.', ','));
                p.Lon = double.Parse(point.Attributes["lon"].Value.Replace('.', ','));
//                p.Elevation = double.Parse(point.SelectSingleNode("gpx:ele", nsmgr).InnerText);
//                p.Time = DateTime.Parse(point.SelectSingleNode("gpx:time", nsmgr).InnerText).ToUniversalTime();
                Points.Add(p);
            }

            return result;

            /*


            int idx_a = response.IndexOf("<rtept");
            if (idx_a > 0)
            {
                response = response.Substring(idx_a);

                response = response.Replace("</rte></gpx>", "");
                response = response.Replace("</rtept>", "");
                response = response.Replace("<rtept ", "");


                String[] latlon_array = response.Split(">");


                

                


            }

*/


            return result;

            /* END OSRM
             */




            //String response = reader.ReadToEnd();
            //System.Console.WriteLine(response);

            int b = response.IndexOf("<coordinates>");
            int e = response.IndexOf("</coordinates>");
            String c = response.Substring(b + "<coordinates>".Length, e - b - "<coordinates>".Length);


            String[] c_arr = c.Split('\n');
            //Track t = new Track();
            for (int i = 0; i < c_arr.Length; i++) {
                String[] c1 = c_arr[i].Split(',');
                if (c1.Length == 2) {
                    c1[0] = c1[0].Replace('.', ',');
                    c1[1] = c1[1].Replace('.', ',');
                    PathPoint pp = new PathPoint();
                    Double.TryParse(c1[0], out pp.Lon);
                    Double.TryParse(c1[1], out pp.Lat);

                    this.Points.Add(pp);

                }
            }


            b = response.IndexOf("<distance>");
            e = response.IndexOf("</distance>");
            String distance1 = response.Substring(b + "<distance>".Length, e - b - "<distance>".Length);
            distance1 = distance1.Replace('.', ',');
           // result = distance1;

            return result;
        }

        
    }
}
