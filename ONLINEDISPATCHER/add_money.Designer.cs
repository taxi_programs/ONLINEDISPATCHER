﻿namespace ONLINEDISPATCHER {
    partial class add_money {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent() {
            this.comboCars = new System.Windows.Forms.ComboBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.OrgID = new System.Windows.Forms.ComboBox();
            this.textbox_summ = new System.Windows.Forms.TextBox();
            this.descr = new System.Windows.Forms.TextBox();
            this.payment_date = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // comboCars
            // 
            this.comboCars.DropDownHeight = 120;
            this.comboCars.FormattingEnabled = true;
            this.comboCars.IntegralHeight = false;
            this.comboCars.Location = new System.Drawing.Point(135, 13);
            this.comboCars.Margin = new System.Windows.Forms.Padding(2);
            this.comboCars.Name = "comboCars";
            this.comboCars.Size = new System.Drawing.Size(325, 21);
            this.comboCars.TabIndex = 1;
            this.comboCars.SelectedIndexChanged += new System.EventHandler(this.comboCars_SelectedIndexChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(13, 14);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(73, 17);
            this.radioButton1.TabIndex = 2;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Водитель";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(13, 65);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(92, 17);
            this.radioButton2.TabIndex = 3;
            this.radioButton2.Text = "Организация";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 105);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Сумма";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 132);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Примечание";
            // 
            // OrgID
            // 
            this.OrgID.Enabled = false;
            this.OrgID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.OrgID.FormattingEnabled = true;
            this.OrgID.Location = new System.Drawing.Point(135, 64);
            this.OrgID.Name = "OrgID";
            this.OrgID.Size = new System.Drawing.Size(325, 21);
            this.OrgID.TabIndex = 6;
            this.OrgID.TabStop = false;
            this.OrgID.SelectedIndexChanged += new System.EventHandler(this.OrgID_SelectedIndexChanged);
            // 
            // textbox_summ
            // 
            this.textbox_summ.Location = new System.Drawing.Point(135, 102);
            this.textbox_summ.Name = "textbox_summ";
            this.textbox_summ.Size = new System.Drawing.Size(94, 20);
            this.textbox_summ.TabIndex = 7;
            // 
            // descr
            // 
            this.descr.Location = new System.Drawing.Point(135, 129);
            this.descr.Name = "descr";
            this.descr.Size = new System.Drawing.Size(325, 20);
            this.descr.TabIndex = 8;
            // 
            // payment_date
            // 
            this.payment_date.Location = new System.Drawing.Point(135, 159);
            this.payment_date.Name = "payment_date";
            this.payment_date.Size = new System.Drawing.Size(200, 20);
            this.payment_date.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 159);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Примечание";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(385, 208);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 11;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // add_money
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(488, 242);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.payment_date);
            this.Controls.Add(this.descr);
            this.Controls.Add(this.textbox_summ);
            this.Controls.Add(this.OrgID);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.comboCars);
            this.Name = "add_money";
            this.Text = "Внесение суммы водителю/организации";
            this.Load += new System.EventHandler(this.add_money_Load);
            this.Shown += new System.EventHandler(this.add_money_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ComboBox comboCars;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox OrgID;
        private System.Windows.Forms.TextBox descr;
        private System.Windows.Forms.DateTimePicker payment_date;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.RadioButton radioButton1;
        public System.Windows.Forms.RadioButton radioButton2;
        public System.Windows.Forms.TextBox textbox_summ;
    }
}