﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ONLINEDISPATCHER {
    public partial class ListGpsAddress : Form {

        private static DataTable zones = null;


        public ListGpsAddress() {
            InitializeComponent();

            if (zones == null)
                zones = DataBase.mssqlRead("SELECT * FROM ZONES order by descr");


            zone.DataSource = zones;
            zone.DisplayMember = "Descr";
            zone.ValueMember = "ID";

            zone.AutoCompleteSource = AutoCompleteSource.ListItems;
            zone.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            zone.SelectedIndex = -1;

            zone.DropDownHeight = zone.ItemHeight * ((DataTable)zone.DataSource).Rows.Count;

        }

        private void button1_Click(object sender, EventArgs e) {
            RefreshList();
        }

        private void RefreshList() {
            if (view_addresses.DataSource == null) {
                DataTable dt1 = new DataTable();
                dt1.Columns.Add("ID");
                dt1.Columns.Add("city");
                dt1.Columns.Add("name");
                dt1.Columns.Add("street");
                dt1.Columns.Add("number");
                dt1.Columns.Add("zoneid");
                dt1.Columns.Add("zonename");
                dt1.Columns.Add("state");
                dt1.Columns.Add("lat");
                dt1.Columns.Add("lon");

                //                dt1.Columns["ord_date"].DataType = System.Type.GetType("System.DateTime");
                view_addresses.DataSource = dt1;

                view_addresses.Columns["name"].HeaderText = "Название";
                view_addresses.Columns["name"].Width = 160;
            }

            String sql = "select gis_adresses.*, (select zones.descr from zones where zones.id = gis_adresses.zoneid) zonename from gis_adresses ";
            sql += " where case when name is null then '' else name end + ' '+city+' '+street+' '+number like @addr ";
            if (zone.SelectedValue != null)
                sql += " and zoneid = " + zone.SelectedValue.ToString();

            if (show_utw.SelectedIndex == 0)
                sql += " and state=0";
            if (show_utw.SelectedIndex == 1)
                sql += " and state=1";


//            sql += " and lat < 57.49 and lon < 39.74 ";


            sql += " order by case when name is null then '' else name end + ' '+city+' '+street+' '+number";


            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("addr", "%" + filterAddress.Text + "%"));

            DataTable dt = DataBase.mssqlRead(sql, parameters.ToArray());

            view_addresses.DataSource = dt;
/*
            DataTable dt_view =  (DataTable)view_addresses.DataSource;
            DataColumnCollection columns = dt_view.Columns;
            for (int i = dt_view.Rows.Count - 1; i >= 0; i--) {
                DataRow view_row = dt_view.Rows[i];
                int id = int.Parse(view_row["ID"].ToString());
                DataRow[] select_result = dt.Select("ID = " + id.ToString());
                if (select_result.Length == 0)  //строки уже нет
                {
                    view_row.Delete();
                }

                if (select_result.Length == 1)  //такая строка есть - обновить данные
                {


                    bool need_changes = false;
                    foreach (DataColumn col in dt_view.Columns) {
                        if (dt.Columns.Contains(col.ColumnName)) {
                            if (view_row[col].ToString().CompareTo(select_result[0][col.ColumnName].ToString()) != 0)
                                need_changes = true;
                        }

                    }
                    if (need_changes)
                        foreach (DataColumn col in dt_view.Columns) {
                            if (dt.Columns.Contains(col.ColumnName)) {
                                // view_row.BeginEdit();
                                view_row[col] = select_result[0][col.ColumnName];
                                //  view_row.EndEdit();
                            }
                        }


                    select_result[0].Delete();
                    //select_result[0].AcceptChanges();
                }
            }
            dt.AcceptChanges();

            foreach (DataRow new_row in dt.Rows) {


                DataRow r = dt_view.NewRow();
                //r.BeginEdit();
                foreach (DataColumn col in dt_view.Columns) {
                    if (dt.Columns.Contains(col.ColumnName)) {
                        r[col] = new_row[col.ColumnName];
                    }
                }
                //r.EndEdit();
                dt_view.Rows.InsertAt(r, 0);

            }
*/                


             
        }

        private void view_addresses_CellDoubleClick(object sender, DataGridViewCellEventArgs e) {
            if (view_addresses.SelectedRows.Count != 1)
                return;

            if (view_addresses.SelectedRows[0].Index >= 0) {

                int id = int.Parse((view_addresses.Rows[view_addresses.SelectedRows[0].Index].Cells["ID"].Value.ToString()));
                EditGpsAddress edit = new EditGpsAddress();
                edit.doLoad(id);

                
                edit.Show();
            }
        }

        private void view_addresses_CellContentClick(object sender, DataGridViewCellEventArgs e) {

        }

        private void button2_Click(object sender, EventArgs e) {
            MAIN_FORM.main_form_object.doClearAdressMarkers();
        }

        private void view_addresses_Click(object sender, EventArgs e) {
            if (view_addresses.SelectedRows[0].Index >= 0) {

                int id = int.Parse((view_addresses.Rows[view_addresses.SelectedRows[0].Index].Cells["ID"].Value.ToString()));

                int lat = (int)(100000*(double.Parse("0"+view_addresses.Rows[view_addresses.SelectedRows[0].Index].Cells["lat"].Value.ToString())));
                int lon = (int)(100000 * (double.Parse("0" + view_addresses.Rows[view_addresses.SelectedRows[0].Index].Cells["lon"].Value.ToString())));

                String descr = (String)view_addresses.Rows[view_addresses.SelectedRows[0].Index].Cells["address"].Value;
                descr += " " + (String)view_addresses.Rows[view_addresses.SelectedRows[0].Index].Cells["housenum"].Value;


                MAIN_FORM.main_form_object.doShowAddressMarker(lat, lon, descr);
            }
        }

        private void button3_Click(object sender, EventArgs e) {
                    foreach (DataRow row in ((DataTable)(view_addresses.DataSource)).Rows)
                    {

                        String slat = "0";
                        if (row["LAT"] != DBNull.Value)
                            slat = (String)row["LAT"];
                        String slon = "0";
                        if (row["LON"] != DBNull.Value)
                            slon = (String)row["LON"];
                        
                        String ADDRID_string = (String)row["ID"];
                        int ADDRid = int.Parse(ADDRID_string );
                        String descr = (String)row["ADDRESS"]+" "+(String)row["HOUSENUM"];
                        
                        
                        bool orderCoord = true;

                        String latlon = "0.0 0.0";
                        if (slat == "0")
                        {

                            latlon = MAIN_FORM.doYandexSearchAddress("ЯРОСЛАВЛЬ+" + descr, "");
                            orderCoord = false;
                        }
                        else 
                            latlon = ((double.Parse(slon)).ToString()+ " "+ ((double.Parse(slat)).ToString()));
                        
                        latlon = latlon.Replace('.', ',');
                        //System.Console.WriteLine(latlon + " " + orderText);

                        if (latlon != "0.0 0.0")
                        {
                            
                            
                            Double lat1, lon1;
                            lon1 = Double.Parse(latlon.Split(' ')[0]);
                            lat1 = Double.Parse(latlon.Split(' ')[1]);

                            if (!orderCoord)
                            {
                                //записать координаты в заказ
                                int int_lat =(int) (lat1 *100000);
                                int int_lon = (int)(lon1 * 100000);

                                String[] fields = { "LAT", "LON"};
                                object[] values = { lat1, lon1 };

                                DataBase.mssqlUpdate("ADRESSES", fields, values, "ID", ADDRid);
                                    
                            }

                           MAIN_FORM.main_form_object.doShowAddressMarker(((int)(lat1 * 100000)), (int)(lon1*100000), descr);
                        }
                    }
        }

        private void button4_Click(object sender, EventArgs e) {

            MAIN_FORM.main_form_object.doClearAdressMarkers();
            
            foreach (DataRow row in ((DataTable)(view_addresses.DataSource)).Rows) {

                String slat = "0";
                if (row["LAT"] != DBNull.Value)
                    slat = (String)row["LAT"].ToString();
                String slon = "0";
                if (row["LON"] != DBNull.Value)
                    slon = (String)row["LON"].ToString();

                String ADDRID_string = (String)row["ID"].ToString();
                int ADDRid = int.Parse(ADDRID_string);
                String descr = (String)row["STREET"] + " " + (String)row["NUMBER"];

                double lat1 = double.Parse(slat);
                double lon1 = double.Parse(slon);

                MAIN_FORM.main_form_object.doShowAddressMarker(((int)(lat1 * 100000)), (int)(lon1 * 100000), descr);
            }
        }
    }
}
