﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace ONLINEDISPATCHER
{
    class taxi_service_tarif
    {
        public class Услуга
        {
            private String m_sName = "";
            private int m_iPrice = 0;
            public Услуга(String name, int price)
            {
                m_sName = name;
                m_iPrice = price;
            }

            public String Name
            {
                get
                {
                    return m_sName;
                }
            }
            public int Price
            {
                get
                {
                    return m_iPrice;
                }
            }
        }
        //Имя службы
        //номер службы
        //тариф службы

        //получение 
        private int m_iService_id = 0;

        private double m_dПосадка = 70;
        private double m_dЗаКмВГороде = 10;
        private double m_dЗаКмЗаГородом = 15;
        private double m_dОжиданиеЗаМинуту = 4;
        public  List<Услуга> услуги = new List<Услуга>();
        /* Тариф:
         * Основной. - для обычных пассажиров. Зависит от выбранной службы.
         * но для безнала - персональный для организации: если выбрана организация - берется тариф именно для нее.
         * 
         */

        public double Посадка
        {
            get
            {
                return m_dПосадка;
            }
        }

        public double Ожидание
        {
            get
            {
                return m_dОжиданиеЗаМинуту;
            }
        }

        public taxi_service_tarif(Int32 service_id, double Посадка, double ЗаКмВГороде, double ЗаКмЗаГородом, double ОжиданиеЗаМинуту)
        {
            

            m_iService_id = service_id;
            m_dПосадка = Посадка;
            m_dЗаКмВГороде = ЗаКмВГороде;
            m_dЗаКмЗаГородом = ЗаКмЗаГородом;
            m_dОжиданиеЗаМинуту = ОжиданиеЗаМинуту;

            услуги.Add(new Услуга("Отдаленный район", 20));
            услуги.Add(new Услуга("Багаж", 30));
            услуги.Add(new Услуга("Животное", 20));
            услуги.Add(new Услуга("Грунтовая дорога", 20));


        }


        


        private static System.Collections.Hashtable Тарифы = new System.Collections.Hashtable();


        private static void addTarif(Int32 service_id, double Посадка, double ЗаКмВГороде, double ЗаКмЗаГородом, double ОжиданиеЗаМинуту)
        {
            taxi_service_tarif tarif = new taxi_service_tarif(service_id, Посадка, ЗаКмВГороде, ЗаКмЗаГородом, ОжиданиеЗаМинуту);

            Тарифы.Add(service_id, tarif);
        }


        public static void doLoadTarifs(){
            if (Тарифы.Count > 0)
                return;

            DataTable dt =  DataBase.mssqlRead("SELECT * FROM GPSTARIFFS");
            foreach(DataRow row in dt.Rows){
                int service_id = (int)row["serviceid"];
                String tarif_string = (String)row["tariffparameters"];

                System.Collections.SortedList list_params = new System.Collections.SortedList();
                String[] tParams = tarif_string.Split(';');
                foreach (String param in tParams)
                {
                    String [] param_value  = param.Split('=');
                    if (param_value.Length == 2)
                    {
                        //DistanceCity.Text = DistanceCity.Text.Replace('.', ',');
                        list_params.Add(param_value[0].Trim(), param_value[1].Trim().Replace('.', ','));
                    }
                }


                double посадка = 70;
                double кмВГороде= 10;
                double кмЗаГородом= 15;
                double минутаОжидания= 4;

                double.TryParse((string)list_params["sumPosadkaN1"], out посадка);
                double.TryParse((string)list_params["sumKmY1"], out кмВГороде);

                кмВГороде = (int)кмВГороде;
                //double.TryParse((string)list_params["sumPosadkaN1"], out посадка);
                //double.TryParse((string)list_params["sumPosadkaN1"], out посадка);

                //taxi_service_tarif tarif = new taxi_service_tarif(service_id, Посадка, ЗаКмВГороде, ЗаКмЗаГородом, ОжиданиеЗаМинуту);
                taxi_service_tarif.addTarif(service_id, посадка, кмВГороде, кмЗаГородом, минутаОжидания);

                




                
                
            }




        }


        public static taxi_service_tarif getTarif(Int32 service_id)
        {
            if (Тарифы.ContainsKey(service_id))
                return (taxi_service_tarif)Тарифы[service_id];
            else
                return new taxi_service_tarif(0, 70, 10, 15, 4);

        }


        public static taxi_service_tarif getDatabaseTarif(Int32 service_id, Int32 organization_id, Int32 client_id, DateTime date)
        {


            Console.WriteLine("getDatabaseTarif");
            taxi_service_tarif tarif = new taxi_service_tarif(0,70, 10, 15, 4);

            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("service_id", service_id));
            parameters.Add(new SqlParameter("organization_id", organization_id));
            parameters.Add(new SqlParameter("client_id", client_id));
            parameters.Add(new SqlParameter("date", date));

            DataTable t = DataBase.mssqlRead(" SELECT * FROM GET_TARIF ( @service_id, @organization_id, @client_id, @date)", parameters.ToArray());

            if (t.Rows.Count > 0)
            {
                tarif.m_dПосадка = (double)t.Rows[0]["POSADKA_MONEY"];
                tarif.m_dЗаКмВГороде = (double)t.Rows[0]["CITY_KM_MONEY"];
                tarif.m_dЗаКмЗаГородом = (double)t.Rows[0]["MGOR_KM_MONEY"];
                tarif.m_dОжиданиеЗаМинуту = (double)t.Rows[0]["CITY_MINUTE_MONEY"];
                
            }


            return tarif;

        }


        public static taxi_service_tarif getTarifForOrganization(Int32 organization_id)
        {
            taxi_service_tarif tarif = new taxi_service_tarif(0, 80, 10, 15, 4);

            if (DateTime.Now > DateTime.Parse("2014-04-01 00:00")) {
                tarif = new taxi_service_tarif(0, 80, 10, 15, 4);
            } 
            
            
            if (organization_id == 72)
                tarif = new taxi_service_tarif(0, 80, 10, 15, 4);
            
            if (organization_id == 31)
                tarif = new taxi_service_tarif(0, 70, 10, 15, 4);
            
            /*
             tarif = new taxi_service_tarif(0, 100, 10, 15, 4);
            switch (organization_id){
                case 50: case 53:
                    tarif = new taxi_service_tarif(0, 100, 10, 15, 4);
                    //tarif.услуги.Add(new Услуга("Предварительный безнал", 50));
                    break;

                case 36:
                    tarif = new taxi_service_tarif(0, 99, 10, 15, 4);
                    break;
                case 31: 
                    tarif = new taxi_service_tarif(0, 70, 10, 15, 4);
                    //tarif.услуги.Add(new Услуга("Предварительный безнал", 50));
                    break;
//                case 54: case 55: case 56: case 57:
//                    tarif = new taxi_service_tarif(0, 100, 10, 15, 5);
//                    break;
                default:
                    break;

            }
            */
            return tarif;

        }


        public int СосчитатьСтоимостьПути(double поГороду, double поМежгороду)
        {
            double result = m_dПосадка + m_dЗаКмВГороде * поГороду + m_dЗаКмЗаГородом * поМежгороду;
            return (int)(result+0.5);
            //Не округлять
        }

        public int ПолучитьСтоимостьУслуги(string наименованиеУслуги){
            int result = 0;
            foreach(Услуга у in услуги){
                if (у.Name.CompareTo(наименованиеУслуги) == 0)
                {
                    result = у.Price;
                }
            }
            return result;
        }



        public int ОкруглитьДо10Руб(int inValue)
        {
            int result = 0;
            result = (int)((double)inValue+5) / 10;
            //result = (int)result;
            result = (int)(result * 10.0);
            return result;
        }





    }
}
