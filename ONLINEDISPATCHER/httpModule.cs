﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;

namespace ONLINEDISPATCHER
{
    class httpModule
    {

        public static WebProxy webProxy = null;
            
        public static string GetHtmlPageText(string url, int timeout)
        {
            string result = String.Empty;





            WebRequest request = WebRequest.Create(url);
            request.Proxy = webProxy;
            //request.Headers.Add("User-agent", "Opera/11.00 beta");
            request.Timeout = timeout;
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            result = reader.ReadToEnd();
                        }
                    }
                }
            }
            catch (WebException e)
            {
                System.Console.WriteLine("Ошибка получения маршрута. " + e.Message);
            }

            return result;
        }        
    }
}
