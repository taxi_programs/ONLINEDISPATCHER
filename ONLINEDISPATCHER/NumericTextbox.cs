﻿using System;
using System.Windows.Forms;

namespace ONLINEDISPATCHER {
    public class NumericTextbox : TextBox {
        bool allowSpace = false;

        // Restricts the entry of characters to digits (including hex), the negative sign,
        // the decimal point, and editing keystrokes (backspace).
        protected override void OnKeyPress(KeyPressEventArgs e) {
            base.OnKeyPress(e);

            System.Globalization.NumberFormatInfo numberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
            string decimalSeparator = numberFormatInfo.NumberDecimalSeparator;
            string groupSeparator = numberFormatInfo.NumberGroupSeparator;
            string negativeSign = numberFormatInfo.NegativeSign;

            string keyInput = e.KeyChar.ToString();
            if (keyInput.Equals("."))
            {
                keyInput = ",";
                e.KeyChar = ',';
            }
            if (Char.IsDigit(e.KeyChar)) {
                // Digits are OK
            } else if (keyInput.Equals(decimalSeparator) || keyInput.Equals(negativeSign)) {
                // Decimal separator is OK
            } else if (e.KeyChar == '\b') {
                // Backspace key is OK
            }
                //    else if ((ModifierKeys & (Keys.Control | Keys.Alt)) != 0)
                //    {
                //     // Let the edit control handle control and alt key combinations
                //    }
              else if (this.allowSpace && e.KeyChar == ' ') {

            } else {
                // Swallow this invalid key and beep
                e.Handled = true;
                //    MessageBeep();
            }
        }

        public int IntValue {
            get {
                int result = 0;
                if (this.Text != "")
                    Int32.TryParse(this.Text, out result);

                return result;
            }
        }

        public decimal DecimalValue {
            get {
                decimal result = 0;
                if (this.Text != "")
                    Decimal.TryParse(this.Text, out result);
                return result;
            }
        }

        public bool AllowSpace {
            set {
                this.allowSpace = value;
            }

            get {
                return this.allowSpace;
            }
        }
    }
}
