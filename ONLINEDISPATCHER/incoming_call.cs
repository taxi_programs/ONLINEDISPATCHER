﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;

using LumiSoft.Net;
using LumiSoft.Net.SDP;
using LumiSoft.Net.SIP;
using LumiSoft.Net.SIP.Stack;

using ONLINEDISPATCHER.resources;

namespace ONLINEDISPATCHER
{
    public partial class incoming_call : Form
    {
        private SIP_ServerTransaction m_pTransaction = null;

        private Boolean isAnswered = false;

//        private SIP_Dialog_Invite m_pDialog;
        private SDP_Message m_pSdpOffer;

        //private SipCaller m_pSipCaller;

        private DateTime form_tm = DateTime.Now;
        private String[] fromValues = new String[2];
        

        private DataTable dt = new DataTable();


        private SipCaller.incoming_call_info currentCallInfo = null;


        public incoming_call()
        {

            InitializeComponent();


                dt.Columns.Add("CallID");    
                dt.Columns.Add("info");
                dt.Columns.Add("from");
                dt.Columns.Add("phone");
                dt.Columns.Add("line");
                dt.Columns.Add("arr_tm");
                dt.Columns.Add("descr");
                dt.Columns.Add("delta_tm");

                
                
                dt.Columns["arr_tm"].DataType = System.Type.GetType("System.DateTime");


                
                dt.Columns["info"].DataType = System.Type.GetType("System.Object");
                this.DataGridView1.DataSource = dt;
                this.DataGridView1.Columns["CallID"].Visible = false;
                this.DataGridView1.Columns["info"].Visible = false;
                this.DataGridView1.Columns["from"].Visible = false;
                this.DataGridView1.Columns["arr_tm"].Visible = false;
                DataGridViewButtonColumn btnColumn = new DataGridViewButtonColumn();
                btnColumn.Name = "btn_answer";
                btnColumn.HeaderText = "Ответить";
                btnColumn.Text = "ответить";
                btnColumn.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn btnColumn2 = new DataGridViewButtonColumn();
                
                btnColumn2.Name = "btn_cancel";
                btnColumn2.HeaderText = "Отклонить";
                btnColumn2.Text = "Отклонить";
                btnColumn2.UseColumnTextForButtonValue = true;
                //btnColumn.DefaultCellStyle.BackColor = Button.DefaultBackColor;
                
            
                


                this.DataGridView1.Columns.Add(btnColumn);
                this.DataGridView1.Columns.Add(btnColumn2);

                
                this.DataGridView1.Columns["phone"].Width = 110;
                this.DataGridView1.Columns["line"].Width = 70;
                this.DataGridView1.Columns["arr_tm"].Width = 60;
                this.DataGridView1.Columns["arr_tm"].DefaultCellStyle.Format = "HH:mm:ss";   
                this.DataGridView1.Columns["delta_tm"].Width = 40;
                this.DataGridView1.Columns["descr"].Width = 350;

                this.DataGridView1.Columns["btn_answer"].Width = 100;
                this.DataGridView1.Columns["btn_cancel"].Width = 100;
                this.DataGridView1.Columns["CallID"].Width = 0;



                

                
            //    this.DataGridView1.Columns["info"].Visible = false;


        }

        //вернет в 0 элементе - код заказа если есть заказ
        //в 1 элементе - текстовка
        //2 элемент - колво заказов за 30 дней
        //3 элемент - абонент-клиент
        //4 - если телефон в списке - заполнить адрес и примечание   "0|ФИО|улица|Дом|кв|прим|skid_percent|money|carid"
        //5 - код машины



        public static  String [] getPhoneInfo(String phone)
        {

            bool isDriver = false;
            bool isExistOrder = false;

            String [] result = new String[6];
            result[0] = "0";
            result[1] = "новый заказ";
            result[5] = "";

            if (phone == "")
                return result;
            //узнать, не от водителя ли звонок.
            DataTable result_driver_info = DataBase.mssqlRead("SELECT * FROM DRIVERS WHERE PHONES='" + phone + "'");
            if (result_driver_info.Rows.Count > 0)
            {
                int carid = (int)result_driver_info.Rows[0]["CARID"];
                result[5] = carid.ToString();
                result[1] = "'Водитель. " + carid.ToString() + " " + (String)result_driver_info.Rows[0]["FIO"];
                isDriver = true;

                //проверить, нет ли этого водителя на заказах водителя работе....
                //если есть - открыть последний заказ

                String driver_order_info_sql = "  select  " +

" orders.id, phone,  addrFrom + houseFrom +' '+flatFrom ADDRFROM, addrTo, " +
" cast(cars.id as varchar(10)) + ' ' +cars.color +' '+cars.mark + ' ' + cars.gosnum car_name,  " +
" cars.state car_state, " +
" cars.status car_status, " +
" (select top 1 status+cast(tries as varchar(10)) from call_out where order_id=orders.id) dial_status " +
" from orders " +
" left join cars on cars.id=orders.carid " +
" where datediff(hour, dtArrive, getdate())<12 " +
" and orders.state <>3" +

" and orders.carid = '" + carid.ToString() + "'" +
" order by dtArrive desc ";
                DataTable result_driver_order_info = DataBase.mssqlRead(driver_order_info_sql);
                int orderid_driver = 0;

                if (result_driver_order_info.Rows.Count > 0)
                {
                    String addr_from = (String)result_driver_order_info.Rows[0]["ADDRFROM"];
                    String addr_to = (String)result_driver_order_info.Rows[0]["ADDRTO"];
                    String car_name = (String)result_driver_order_info.Rows[0]["CAR_NAME"];

                    result[1] = "ВОДИТЕЛЬ "+car_name+" ПО ЗАКАЗУ: \r\n" + addr_from + " -> " + addr_to  ;
                    orderid_driver = (int)result_driver_order_info.Rows[0]["ID"];
                    result[0] = orderid_driver.ToString();


                }



            }

            //узнать, не по заказу ли звонок (есть заказ в предварительных-рабочих).
            String phone_order_info_sql = "  select  " +
" cast (floor(datediff(ss, dtArrive, getdate())/60) as varchar(10))+':'+right( '00'+cast(datediff(ss, dtArrive, getdate()) -60*floor(datediff(ss, dtArrive, getdate())/60) as varchar(10)), 2) dtArrive, " +
" cast (floor(datediff(ss, dtBegin, getdate())/60) as varchar(10))+':'+right( '00'+cast(datediff(ss, dtBegin, getdate()) -60*floor(datediff(ss, dtArrive, getdate())/60) as varchar(10)), 2) dtBegin, " +
" " +
" orders.id, phone, orders.carid," +
" (select '['+substring(descr, 1, 4)+']  ' from zones where id = orders.zoneID)+ " +
" " +
" addrFrom + houseFrom +' '+flatFrom ADDRFROM, addrTo, orders.state order_state, " +
" cast(cars.id as varchar(10)) + ' ' +cars.color +' '+cars.mark + ' ' + cars.gosnum car_name,  " +
" cars.state car_state, " +
" cars.status car_status, " +
" (select top 1 status+cast(tries as varchar(10)) from call_out where order_id=orders.id) dial_status " +
" from orders " +
" left join cars on cars.id=orders.carid " +
" where datediff(hour, dtArrive, getdate())<12 " +
" and orders.state <>3" +

" and ((orders.phone = '" + phone + "')" +
" or (orders.connectphone = '" + phone + "'))" +
" order by dtArrive desc ";
            DataTable result_order_info = DataBase.mssqlRead(phone_order_info_sql);
            int orderid = 0;

            if (result_order_info.Rows.Count > 0)
            {
                String addr_from = (String)result_order_info.Rows[0]["ADDRFROM"];
                String addr_to = (String)result_order_info.Rows[0]["ADDRTO"];
                String car_name = "";
                if (result_order_info.Rows[0]["CAR_NAME"] != DBNull.Value)
                    car_name = (String)result_order_info.Rows[0]["CAR_NAME"];

                result[1] = "По заказу. " + addr_from + " " + addr_to + "\r\n" + car_name;
                orderid = (int)result_order_info.Rows[0]["ID"];
                result[0] = orderid.ToString();

                isExistOrder = true;
            }

            DataTable result2_order_info = DataBase.mssqlRead("SELECT COUNT(*) FROM ORDERS where PHONE='"+phone+"' and dtArrive > getdate()-30" );
            result[2] = result2_order_info.Rows[0][0].ToString();

            DataTable result3_phone_info = DataBase.mssqlRead("SELECT cast(state as varchar(10))+'|'+FIO+'|'+address+'|'+houseNum+'|'+FlatNum+'|'+replace(Descr, '|', ''), FIO, state, skid_percent, money_summ, car_id  FROM PHONES where PHONE='" + phone + "' and deleted = 0");
            if (result3_phone_info.Rows.Count > 0)
            {
                result[4] = result3_phone_info.Rows[0][0].ToString();
                result[3] = result3_phone_info.Rows[0][1].ToString();
                if (result3_phone_info.Rows[0][2].ToString() == "1")
                    result[3] = "ЧС :" + result[3];
                if (result3_phone_info.Rows[0][2].ToString() == "2")
                    result[3] = "CС :" + result[3];
                if (result3_phone_info.Rows[0][2].ToString() == "3")
                    result[3] = "VIP :" + result[3];
                //23-02-2013 - добавляю процент, сумму и машину из черного списка
                String s_skid_percent = result3_phone_info.Rows[0][3].ToString();
                String s_money_summ = result3_phone_info.Rows[0][4].ToString();
                String s_car_id = result3_phone_info.Rows[0][5].ToString();

                result[4] += "|" + s_skid_percent + "|" + s_money_summ + "|" + s_car_id;

            }





            return result;

        }
        
        public incoming_call(SIP_ServerTransaction invite, SIP_Dialog_Invite dialog, SipCaller caller, SDP_Message sdpOffer)
        {

        }

        
/*
        private void CallAnswer(bool open_order )
        {
            //1. определить выбранный звонок.
            //попытаться на него ответить
            //если ответился - закрыть форму.
           
            if (DataGridView1.SelectedRows.Count == 1)
            {
                String CallID = (String)DataGridView1.SelectedRows[0].Cells["CallID"].Value;

                foreach (SipCaller.incoming_call_info info in SipCaller.getCallsListCopy())
                {
                    if (info.CallID == CallID)
                    {
                        if (open_order == true)
                              MAIN_FORM.main_form_object.BeginInvoke(new MethodInvoker(delegate() {
                                MAIN_FORM.main_form_object.onIncomingCallAction("answer_new_order", info);
            }));
//                            MAIN_FORM.main_form_object.onIncomingCallAction("answer_new_order", info);
                        else
                            MAIN_FORM.main_form_object.BeginInvoke(new MethodInvoker(delegate() {
                                MAIN_FORM.main_form_object.onIncomingCallAction("answer", info);
                            }));
                            //MAIN_FORM.main_form_object.onIncomingCallAction("answer", info);
                    }
                }
            }
 
        }
   
   */
        


        #region Event handlign

        #region method m_pAccpet_Click

        /// <summary>
        /// Is called when accpet button has clicked.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event data.</param>
        private void m_pAccpet_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Yes;
        }

        #endregion

        #region method m_pReject_Click

        /// <summary>
        /// Is called when reject button has clicked.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event data.</param>
        private void m_pReject_Click(object sender, EventArgs e)
        {
            m_pTransaction.SendResponse(m_pTransaction.Stack.CreateResponse(SIP_ResponseCodes.x600_Busy_Everywhere, m_pTransaction.Request));

            this.DialogResult = DialogResult.No;
        }

        #endregion


        #region method m_pTransaction_Canceled

        /// <summary>
        /// Is called when transcation has canceled.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event data.</param>
        private void m_pTransaction_Canceled(object sender, EventArgs e)
        {
            // We need invoke here, we are running on thread pool thread.
            /*this.BeginInvoke(new MethodInvoker(delegate()
            {
                this.DialogResult = DialogResult.No;
            }));
             */ 
        }

        #endregion

        private void button3_Click(object sender, EventArgs e)
        {
//            CallAnswer(true);         
        }

        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
  //            CallAnswer(false);         
        }

        private void button2_Click(object sender, EventArgs e)
        {
//            MAIN_FORM.main_form_object.BeginInvoke(new MethodInvoker(delegate() {
//                MAIN_FORM.main_form_object.onIncomingCallAction("hangup", currentCallInfo);
//            }));
//            MAIN_FORM.main_form_object.onIncomingCallAction("hangup", currentCallInfo);
        }

        private void incoming_call_FormClosing(object sender, FormClosingEventArgs e) {
//            m_pSipCaller.m_pPlayer.Stop();
            // to do здесь удалется инфа о входящих звонках, но они не отбиваются!!!
            SipCaller.clearCallsList000(currentCallInfo);
            dt.Rows.Clear();
            e.Cancel = true;
            this.Visible = false;
        }

        public void UpdateCallList()
        {
            DataTable dt = (DataTable)this.DataGridView1.DataSource;

            int cnt = 0;

            foreach (SipCaller.incoming_call_info info in SipCaller.getCallsListCopy())
            {
                bool info_found = false;
                foreach (DataRow row in dt.Rows)
                {
                    if (row["CallID"] == info.CallID)
                    {
                        info_found = true;
                        cnt++;
                        break;

                    }
                }
                if (!info_found)
                {
                    DataRow empty_row = null;
                    foreach (DataRow row1 in dt.Rows)
                    {
                        //int delta = (int)((TimeSpan)(DateTime.Now - (DateTime)row1["arr_tm"])).TotalSeconds;

                        if (row1["arr_tm"] == DBNull.Value)
                        {
                            empty_row = row1;
                            break;
                        }
                    }


                    DataRow row = empty_row;
                    if (empty_row == null)
                        row = dt.NewRow();


                    row["info"] = info;
                    row["CallID"] = info.CallID;
                    row["from"] = info.from;
                    row["phone"] = info.phone;
                    row["line"] = info.line;
                    row["arr_tm"] = info.arriveTm;

                    row["descr"] = info.call_desc;

                    if (empty_row == null)
                        dt.Rows.Add(row);

                    System.Console.WriteLine("NEW CALL: "+info.CallID);

                    cnt++;

                }


            }


            foreach (DataRow row in dt.Rows)
            {

                if (row["arr_tm"] != DBNull.Value)
                {
                    //проверить, есть ли такой звонок в массиве. если нету - очистить строку
                    SipCaller.incoming_call_info info = null;
                    if (row["info"].GetType() != System.Type.GetType("System.String"))
                        info = (SipCaller.incoming_call_info)row["info"];

                    if (info != null)
                    if ( !SipCaller.getCallsListCopy().Contains(info))
                    {

                        row["CallID"] = null;
                        row["info"] = null;
                        row["from"] = null;
                        //                    row["arr_tm"] = null;
                        row["line"] = null;
                        row["descr"] = null;
                        row["arr_tm"] = DBNull.Value;
                        row["phone"] = null;
                        row["delta_tm"] = null;
                        cnt--;
                    }


                    if (row["arr_tm"] != DBNull.Value)
                    {
                        int delta = (int)((TimeSpan)(DateTime.Now - (DateTime)row["arr_tm"])).TotalSeconds;

                        row["delta_tm"] = delta.ToString();

                        ////15-12-2014 - т.к. стратегия очереди изменена на ringall - сбразывать звонок не надо.
                        if (delta > 10)
                        {
/*
                            String CallID = (String)row["CallID"];
                            foreach (SipCaller.incoming_call_info info1 in SipCaller.getCallsListCopy())
                            {
                                if (info1.CallID == CallID)
                                {
                                    try
                                    {
                                        info1.caller.onIncomingCallCancel(info);
                                    }
                                    catch (Exception e)
                                    {
                                        System.Console.WriteLine("Exception:  UpdateCallList() " + e.Message);
                                    }
                                }
                            }

                            row["CallID"] = null;
                            row["info"] = null;
                            row["from"] = null;
                            //                    row["arr_tm"] = null;
                            row["line"] = null;
                            row["descr"] = null;
                            row["arr_tm"] = DBNull.Value;
                            row["phone"] = null;
                            row["delta_tm"] = null;
                            cnt--;
*/
                        } 
                        else
                            row["delta_tm"] = delta.ToString();
                    }
                  
                }

            }

            this.DataGridView1.Refresh();

            this.Text = "Входящие звонки ( " + cnt + " )";
        }

        private void timer1_Tick(object sender, EventArgs e) {
            //status_label1.Text = ((int)(DateTime.Now - form_tm).Duration().TotalSeconds).ToString();
            UpdateCallList();

            
        }

        private void incoming_call_Load(object sender, EventArgs e) {
            
            
        }

        private void DataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == (DataGridView1.Columns["btn_answer"].Index))
            {
                if (DataGridView1.Rows[e.RowIndex].Cells["CallID"].Value != DBNull.Value)
                {
                    String CallID = (String)DataGridView1.Rows[e.RowIndex].Cells["CallID"].Value;
                    

                    foreach (SipCaller.incoming_call_info info in SipCaller.getCallsListCopy()) {

                        

                        if (info.CallID == CallID)
                        {

                            currentCallInfo = info;

                            DataGridView1.Rows[e.RowIndex].Cells["CallID"].Value = null;
                            DataGridView1.Rows[e.RowIndex].Cells["info"].Value = null;
                            DataGridView1.Rows[e.RowIndex].Cells["from"].Value = null;
                            //                    row["arr_tm"] = null;
                            DataGridView1.Rows[e.RowIndex].Cells["line"].Value = null;
                            DataGridView1.Rows[e.RowIndex].Cells["descr"].Value = null;
                            DataGridView1.Rows[e.RowIndex].Cells["arr_tm"].Value = DBNull.Value;
                            DataGridView1.Rows[e.RowIndex].Cells["phone"].Value = null;
                            DataGridView1.Rows[e.RowIndex].Cells["delta_tm"].Value = null;

                            MAIN_FORM.main_form_object.sip_status_combo.SelectedIndex = 1;

                            MAIN_FORM.main_form_object.BeginInvoke(new MethodInvoker(delegate() {
                                MAIN_FORM.main_form_object.sip_status_combo.SelectedIndex = 1;
                                //MAIN_FORM.main_form_object.onIncomingCallAction("answer_new_order", currentCallInfo);
                            }));


                            





                            this.Close();
                        }
                    }
                }
            }

            if (e.ColumnIndex == (DataGridView1.Columns["btn_cancel"].Index))
            {
                if (DataGridView1.Rows[e.RowIndex].Cells["CallID"].Value != DBNull.Value)
                {
                    String CallID = (String)DataGridView1.Rows[e.RowIndex].Cells["CallID"].Value;

                    foreach (SipCaller.incoming_call_info info in SipCaller.getCallsListCopy()) {
                        if (info.CallID == CallID)
                        {
                            info.caller.onIncomingCallCancel(info);
                            {

                                DataGridView1.Rows[e.RowIndex].Cells["CallID"].Value = null;
                                DataGridView1.Rows[e.RowIndex].Cells["info"].Value = null;
                                DataGridView1.Rows[e.RowIndex].Cells["from"].Value = null;
                                DataGridView1.Rows[e.RowIndex].Cells["line"].Value = null;
                                DataGridView1.Rows[e.RowIndex].Cells["descr"].Value = null;
                                DataGridView1.Rows[e.RowIndex].Cells["arr_tm"].Value = DBNull.Value; ;
                                DataGridView1.Rows[e.RowIndex].Cells["phone"].Value = null;
                                DataGridView1.Rows[e.RowIndex].Cells["delta_tm"].Value = null;
                            }
                            //MAIN_FORM.onIncomingCallAction("cancel", info);
                        }
                    }
                }

                
            }

        }

        private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView2_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
        {
            e.Value = "adfasdf";
        }

        private void incoming_call_Shown(object sender, EventArgs e)
        {
            if (MAIN_FORM.main_form_object.sip_status_combo.SelectedIndex == 1)
                this.Close();
        }
    }
}
