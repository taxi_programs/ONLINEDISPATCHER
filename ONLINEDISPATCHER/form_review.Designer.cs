﻿namespace ONLINEDISPATCHER
{
    partial class form_review
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.message = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.car_descr = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.driver_fio = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.carid = new System.Windows.Forms.ComboBox();
            this.status = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cln_fio = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.line = new System.Windows.Forms.Label();
            this.tm = new System.Windows.Forms.Label();
            this.op_fio = new System.Windows.Forms.Label();
            this.phone = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.btnMakeOrder = new System.Windows.Forms.Button();
            this.chk_close_not_saved = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // message
            // 
            this.message.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.message.Location = new System.Drawing.Point(3, 378);
            this.message.MaxLength = 500;
            this.message.Multiline = true;
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(524, 96);
            this.message.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 362);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Отзыв";
            // 
            // car_descr
            // 
            this.car_descr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.car_descr.Location = new System.Drawing.Point(23, 76);
            this.car_descr.Name = "car_descr";
            this.car_descr.Size = new System.Drawing.Size(480, 20);
            this.car_descr.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Машина (описание)";
            // 
            // driver_fio
            // 
            this.driver_fio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.driver_fio.Location = new System.Drawing.Point(23, 123);
            this.driver_fio.Name = "driver_fio";
            this.driver_fio.Size = new System.Drawing.Size(480, 20);
            this.driver_fio.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 107);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Водитель (описание)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Машина (позывной)";
            // 
            // carid
            // 
            this.carid.FormattingEnabled = true;
            this.carid.Location = new System.Drawing.Point(23, 37);
            this.carid.Name = "carid";
            this.carid.Size = new System.Drawing.Size(480, 21);
            this.carid.TabIndex = 10;
            this.carid.SelectedIndexChanged += new System.EventHandler(this.carid_SelectedIndexChanged);
            // 
            // status
            // 
            this.status.FormattingEnabled = true;
            this.status.Items.AddRange(new object[] {
            "Новое сообщение",
            "В работе",
            "Закрыто"});
            this.status.Location = new System.Drawing.Point(3, 499);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(225, 21);
            this.status.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 483);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Статус";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cln_fio);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.line);
            this.groupBox1.Controls.Add(this.tm);
            this.groupBox1.Controls.Add(this.op_fio);
            this.groupBox1.Controls.Add(this.phone);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(3, 68);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(524, 101);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Информация о звонящем";
            // 
            // cln_fio
            // 
            this.cln_fio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cln_fio.Location = new System.Drawing.Point(23, 66);
            this.cln_fio.Name = "cln_fio";
            this.cln_fio.Size = new System.Drawing.Size(287, 20);
            this.cln_fio.TabIndex = 24;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(20, 54);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(34, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "ФИО";
            // 
            // line
            // 
            this.line.AutoSize = true;
            this.line.Location = new System.Drawing.Point(328, 66);
            this.line.Name = "line";
            this.line.Size = new System.Drawing.Size(42, 13);
            this.line.TabIndex = 22;
            this.line.Text = "Линия:";
            // 
            // tm
            // 
            this.tm.AutoSize = true;
            this.tm.Location = new System.Drawing.Point(328, 43);
            this.tm.Name = "tm";
            this.tm.Size = new System.Drawing.Size(36, 13);
            this.tm.TabIndex = 21;
            this.tm.Text = "Дата:";
            // 
            // op_fio
            // 
            this.op_fio.AutoSize = true;
            this.op_fio.Location = new System.Drawing.Point(328, 23);
            this.op_fio.Name = "op_fio";
            this.op_fio.Size = new System.Drawing.Size(59, 13);
            this.op_fio.TabIndex = 20;
            this.op_fio.Text = "Оператор:";
            // 
            // phone
            // 
            this.phone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.phone.Location = new System.Drawing.Point(23, 32);
            this.phone.Name = "phone";
            this.phone.Size = new System.Drawing.Size(287, 20);
            this.phone.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Контактный телефон";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.car_descr);
            this.groupBox2.Controls.Add(this.driver_fio);
            this.groupBox2.Controls.Add(this.carid);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(3, 175);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(524, 185);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "О ком отзыв";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(431, 499);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 20;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(11, 9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(457, 26);
            this.label11.TabIndex = 22;
            this.label11.Text = "В этой форме заносим информацию, которую нам передают пассажиры (или водители).\r\n" +
                "пожелания, жалобы, просто информация";
            // 
            // btnMakeOrder
            // 
            this.btnMakeOrder.Location = new System.Drawing.Point(320, 38);
            this.btnMakeOrder.Name = "btnMakeOrder";
            this.btnMakeOrder.Size = new System.Drawing.Size(207, 28);
            this.btnMakeOrder.TabIndex = 23;
            this.btnMakeOrder.Text = "Создать заказ";
            this.btnMakeOrder.UseVisualStyleBackColor = true;
            this.btnMakeOrder.Click += new System.EventHandler(this.btnMakeOrder_Click);
            // 
            // chk_close_not_saved
            // 
            this.chk_close_not_saved.AutoSize = true;
            this.chk_close_not_saved.BackColor = System.Drawing.Color.Yellow;
            this.chk_close_not_saved.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chk_close_not_saved.Location = new System.Drawing.Point(474, 8);
            this.chk_close_not_saved.Name = "chk_close_not_saved";
            this.chk_close_not_saved.Size = new System.Drawing.Size(33, 17);
            this.chk_close_not_saved.TabIndex = 77;
            this.chk_close_not_saved.Text = "Х";
            this.chk_close_not_saved.UseVisualStyleBackColor = false;
            // 
            // form_review
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 534);
            this.Controls.Add(this.chk_close_not_saved);
            this.Controls.Add(this.btnMakeOrder);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.status);
            this.Controls.Add(this.message);
            this.Controls.Add(this.label2);
            this.Name = "form_review";
            this.Text = "Информация для руководства/Отзыв о работе";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.form_review_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox message;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox car_descr;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox driver_fio;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox carid;
        private System.Windows.Forms.ComboBox status;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox cln_fio;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label tm;
        private System.Windows.Forms.Label op_fio;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.Label line;
        private System.Windows.Forms.Button btnMakeOrder;
        public System.Windows.Forms.TextBox phone;
        private System.Windows.Forms.CheckBox chk_close_not_saved;
    }
}