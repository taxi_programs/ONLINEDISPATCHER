﻿namespace ONLINEDISPATCHER
{
    partial class settings
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnTestAudioOutDevice = new System.Windows.Forms.Button();
            this.checkAudioInDevice = new System.Windows.Forms.CheckBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.comboAudioInDevice = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboAudioOutDevice = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.osrm_server = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.web_proxy = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.dbServerConnectPath = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.sipPassword = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.sipLogin = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.sipLocalIP = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.sipPort = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.sipServerIP = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.font_example = new System.Windows.Forms.Label();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.btnColor1 = new System.Windows.Forms.Button();
            this.btnColor2 = new System.Windows.Forms.Button();
            this.btnColor3 = new System.Windows.Forms.Button();
            this.btnColor4 = new System.Windows.Forms.Button();
            this.chkDistrByNewProg = new System.Windows.Forms.CheckBox();
            this.chkНовоеОкноЗаказа = new System.Windows.Forms.CheckBox();
            this.chk_ОтладкаSIP = new System.Windows.Forms.CheckBox();
            this.chk_UseNewOSRMProtocol = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnTestAudioOutDevice);
            this.groupBox1.Controls.Add(this.checkAudioInDevice);
            this.groupBox1.Controls.Add(this.progressBar1);
            this.groupBox1.Controls.Add(this.comboAudioInDevice);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.comboAudioOutDevice);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(23, 15);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(561, 12);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Настройки аудио";
            this.groupBox1.Visible = false;
            // 
            // btnTestAudioOutDevice
            // 
            this.btnTestAudioOutDevice.Location = new System.Drawing.Point(429, 50);
            this.btnTestAudioOutDevice.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnTestAudioOutDevice.Name = "btnTestAudioOutDevice";
            this.btnTestAudioOutDevice.Size = new System.Drawing.Size(100, 28);
            this.btnTestAudioOutDevice.TabIndex = 8;
            this.btnTestAudioOutDevice.Text = "Тест";
            this.btnTestAudioOutDevice.UseVisualStyleBackColor = true;
            // 
            // checkAudioInDevice
            // 
            this.checkAudioInDevice.AutoSize = true;
            this.checkAudioInDevice.Location = new System.Drawing.Point(235, 144);
            this.checkAudioInDevice.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkAudioInDevice.Name = "checkAudioInDevice";
            this.checkAudioInDevice.Size = new System.Drawing.Size(293, 21);
            this.checkAudioInDevice.TabIndex = 7;
            this.checkAudioInDevice.Text = "Автоматическая регулировка громкости";
            this.checkAudioInDevice.UseVisualStyleBackColor = true;
            this.checkAudioInDevice.Visible = false;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(27, 144);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(185, 21);
            this.progressBar1.TabIndex = 6;
            this.progressBar1.Visible = false;
            // 
            // comboAudioInDevice
            // 
            this.comboAudioInDevice.FormattingEnabled = true;
            this.comboAudioInDevice.Location = new System.Drawing.Point(27, 111);
            this.comboAudioInDevice.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboAudioInDevice.Name = "comboAudioInDevice";
            this.comboAudioInDevice.Size = new System.Drawing.Size(377, 24);
            this.comboAudioInDevice.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 91);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Устройство ввода";
            // 
            // comboAudioOutDevice
            // 
            this.comboAudioOutDevice.FormattingEnabled = true;
            this.comboAudioOutDevice.Location = new System.Drawing.Point(27, 50);
            this.comboAudioOutDevice.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboAudioOutDevice.Name = "comboAudioOutDevice";
            this.comboAudioOutDevice.Size = new System.Drawing.Size(377, 24);
            this.comboAudioOutDevice.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 31);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Устройство вывода";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chk_UseNewOSRMProtocol);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.osrm_server);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.web_proxy);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.dbServerConnectPath);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.sipPassword);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.sipLogin);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.sipLocalIP);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.sipPort);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.sipServerIP);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(23, 187);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(561, 303);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Настройки подключения";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(47, 123);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(449, 17);
            this.label11.TabIndex = 24;
            this.label11.Text = "порт уникальный для каждого оператора из диапазона 5600-5700";
            this.label11.Visible = false;
            // 
            // osrm_server
            // 
            this.osrm_server.Location = new System.Drawing.Point(257, 47);
            this.osrm_server.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.osrm_server.Name = "osrm_server";
            this.osrm_server.Size = new System.Drawing.Size(184, 22);
            this.osrm_server.TabIndex = 23;
            this.osrm_server.Text = "taxi.dyndns.org:5000";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(253, 27);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(184, 17);
            this.label10.TabIndex = 22;
            this.label10.Text = "Сервер расчета маршрута";
            // 
            // web_proxy
            // 
            this.web_proxy.Location = new System.Drawing.Point(27, 47);
            this.web_proxy.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.web_proxy.Name = "web_proxy";
            this.web_proxy.Size = new System.Drawing.Size(184, 22);
            this.web_proxy.TabIndex = 21;
            this.web_proxy.Text = "192.168.100.10:8080";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(23, 27);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 17);
            this.label9.TabIndex = 20;
            this.label9.Text = "WEB proxy";
            // 
            // dbServerConnectPath
            // 
            this.dbServerConnectPath.Location = new System.Drawing.Point(235, 250);
            this.dbServerConnectPath.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dbServerConnectPath.Name = "dbServerConnectPath";
            this.dbServerConnectPath.Size = new System.Drawing.Size(184, 22);
            this.dbServerConnectPath.TabIndex = 19;
            this.dbServerConnectPath.Text = "192.168.100.1";
            this.dbServerConnectPath.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(231, 230);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(145, 17);
            this.label8.TabIndex = 18;
            this.label8.Text = "Сервер базы данных";
            this.label8.Visible = false;
            // 
            // sipPassword
            // 
            this.sipPassword.Location = new System.Drawing.Point(220, 164);
            this.sipPassword.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.sipPassword.Name = "sipPassword";
            this.sipPassword.Size = new System.Drawing.Size(184, 22);
            this.sipPassword.TabIndex = 17;
            this.sipPassword.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(216, 144);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 17);
            this.label7.TabIndex = 16;
            this.label7.Text = "Пароль";
            this.label7.Visible = false;
            // 
            // sipLogin
            // 
            this.sipLogin.Location = new System.Drawing.Point(27, 164);
            this.sipLogin.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.sipLogin.Name = "sipLogin";
            this.sipLogin.Size = new System.Drawing.Size(184, 22);
            this.sipLogin.TabIndex = 15;
            this.sipLogin.Text = "1200";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(23, 144);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 17);
            this.label6.TabIndex = 14;
            this.label6.Text = "SIP Логин";
            // 
            // sipLocalIP
            // 
            this.sipLocalIP.FormattingEnabled = true;
            this.sipLocalIP.Location = new System.Drawing.Point(257, 95);
            this.sipLocalIP.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.sipLocalIP.Name = "sipLocalIP";
            this.sipLocalIP.Size = new System.Drawing.Size(157, 24);
            this.sipLocalIP.TabIndex = 5;
            this.sipLocalIP.Text = "192.168.0.24";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(253, 75);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(140, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Сетевой интерфейс";
            // 
            // sipPort
            // 
            this.sipPort.Location = new System.Drawing.Point(431, 95);
            this.sipPort.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.sipPort.Name = "sipPort";
            this.sipPort.Size = new System.Drawing.Size(53, 22);
            this.sipPort.TabIndex = 3;
            this.sipPort.Text = "7667";
            this.sipPort.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(427, 75);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 17);
            this.label4.TabIndex = 2;
            this.label4.Text = "Порт";
            this.label4.Visible = false;
            // 
            // sipServerIP
            // 
            this.sipServerIP.Location = new System.Drawing.Point(27, 95);
            this.sipServerIP.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.sipServerIP.Name = "sipServerIP";
            this.sipServerIP.Size = new System.Drawing.Size(184, 22);
            this.sipServerIP.TabIndex = 1;
            this.sipServerIP.Text = "192.168.0.1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 75);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "SIP сервер";
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button2.Location = new System.Drawing.Point(23, 497);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 28);
            this.button2.TabIndex = 2;
            this.button2.Text = "OK";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button3.Location = new System.Drawing.Point(484, 497);
            this.button3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(100, 28);
            this.button3.TabIndex = 3;
            this.button3.Text = "Отмена";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 100);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 28);
            this.button1.TabIndex = 4;
            this.button1.Text = "Шрифт";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // font_example
            // 
            this.font_example.AutoSize = true;
            this.font_example.Location = new System.Drawing.Point(131, 106);
            this.font_example.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.font_example.Name = "font_example";
            this.font_example.Size = new System.Drawing.Size(383, 17);
            this.font_example.TabIndex = 5;
            this.font_example.Text = " Умереть со скуки - выражение чисто гиперболическое. ";
            // 
            // btnColor1
            // 
            this.btnColor1.Location = new System.Drawing.Point(16, 63);
            this.btnColor1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnColor1.Name = "btnColor1";
            this.btnColor1.Size = new System.Drawing.Size(100, 28);
            this.btnColor1.TabIndex = 6;
            this.btnColor1.Text = "Цвет 1";
            this.btnColor1.UseVisualStyleBackColor = false;
            this.btnColor1.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnColor2
            // 
            this.btnColor2.Location = new System.Drawing.Point(135, 63);
            this.btnColor2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnColor2.Name = "btnColor2";
            this.btnColor2.Size = new System.Drawing.Size(100, 28);
            this.btnColor2.TabIndex = 7;
            this.btnColor2.Text = "Цвет 2";
            this.btnColor2.UseVisualStyleBackColor = false;
            this.btnColor2.Click += new System.EventHandler(this.btnColor2_Click);
            // 
            // btnColor3
            // 
            this.btnColor3.Location = new System.Drawing.Point(257, 63);
            this.btnColor3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnColor3.Name = "btnColor3";
            this.btnColor3.Size = new System.Drawing.Size(100, 28);
            this.btnColor3.TabIndex = 8;
            this.btnColor3.Text = "Цвет 3";
            this.btnColor3.UseVisualStyleBackColor = false;
            this.btnColor3.Click += new System.EventHandler(this.btnColor3_Click);
            // 
            // btnColor4
            // 
            this.btnColor4.Location = new System.Drawing.Point(377, 63);
            this.btnColor4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnColor4.Name = "btnColor4";
            this.btnColor4.Size = new System.Drawing.Size(100, 28);
            this.btnColor4.TabIndex = 9;
            this.btnColor4.Text = "Цвет 4";
            this.btnColor4.UseVisualStyleBackColor = false;
            this.btnColor4.Click += new System.EventHandler(this.btnColor4_Click);
            // 
            // chkDistrByNewProg
            // 
            this.chkDistrByNewProg.AutoSize = true;
            this.chkDistrByNewProg.Location = new System.Drawing.Point(16, 135);
            this.chkDistrByNewProg.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkDistrByNewProg.Name = "chkDistrByNewProg";
            this.chkDistrByNewProg.Size = new System.Drawing.Size(223, 21);
            this.chkDistrByNewProg.TabIndex = 10;
            this.chkDistrByNewProg.Text = "Очередь по новой программе";
            this.chkDistrByNewProg.UseVisualStyleBackColor = true;
            this.chkDistrByNewProg.Visible = false;
            this.chkDistrByNewProg.CheckedChanged += new System.EventHandler(this.chkDistrByNewProg_CheckedChanged);
            // 
            // chkНовоеОкноЗаказа
            // 
            this.chkНовоеОкноЗаказа.AutoSize = true;
            this.chkНовоеОкноЗаказа.Location = new System.Drawing.Point(16, 159);
            this.chkНовоеОкноЗаказа.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkНовоеОкноЗаказа.Name = "chkНовоеОкноЗаказа";
            this.chkНовоеОкноЗаказа.Size = new System.Drawing.Size(152, 21);
            this.chkНовоеОкноЗаказа.TabIndex = 11;
            this.chkНовоеОкноЗаказа.Text = "Новое окно заказа";
            this.chkНовоеОкноЗаказа.UseVisualStyleBackColor = true;
            this.chkНовоеОкноЗаказа.CheckedChanged += new System.EventHandler(this.chkНовоеОкноЗаказа_CheckedChanged);
            // 
            // chk_ОтладкаSIP
            // 
            this.chk_ОтладкаSIP.AutoSize = true;
            this.chk_ОтладкаSIP.Location = new System.Drawing.Point(436, 135);
            this.chk_ОтладкаSIP.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chk_ОтладкаSIP.Name = "chk_ОтладкаSIP";
            this.chk_ОтладкаSIP.Size = new System.Drawing.Size(105, 21);
            this.chk_ОтладкаSIP.TabIndex = 12;
            this.chk_ОтладкаSIP.Text = "ОтладкаSIP";
            this.chk_ОтладкаSIP.UseVisualStyleBackColor = true;
            // 
            // chk_UseNewOSRMProtocol
            // 
            this.chk_UseNewOSRMProtocol.AutoSize = true;
            this.chk_UseNewOSRMProtocol.Location = new System.Drawing.Point(448, 47);
            this.chk_UseNewOSRMProtocol.Name = "chk_UseNewOSRMProtocol";
            this.chk_UseNewOSRMProtocol.Size = new System.Drawing.Size(136, 21);
            this.chk_UseNewOSRMProtocol.TabIndex = 25;
            this.chk_UseNewOSRMProtocol.Text = "Новый протокол";
            this.chk_UseNewOSRMProtocol.UseVisualStyleBackColor = true;
            // 
            // settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(609, 564);
            this.Controls.Add(this.chk_ОтладкаSIP);
            this.Controls.Add(this.chkНовоеОкноЗаказа);
            this.Controls.Add(this.chkDistrByNewProg);
            this.Controls.Add(this.btnColor4);
            this.Controls.Add(this.btnColor3);
            this.Controls.Add(this.btnColor2);
            this.Controls.Add(this.btnColor1);
            this.Controls.Add(this.font_example);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "settings";
            this.Text = "settings";
            this.Activated += new System.EventHandler(this.settings_Activated);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboAudioInDevice;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboAudioOutDevice;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button btnTestAudioOutDevice;
        private System.Windows.Forms.CheckBox checkAudioInDevice;
        private System.Windows.Forms.ComboBox sipLocalIP;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox sipPort;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox sipServerIP;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox sipPassword;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox sipLogin;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox dbServerConnectPath;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox web_proxy;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label font_example;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Button btnColor1;
        private System.Windows.Forms.Button btnColor2;
        private System.Windows.Forms.Button btnColor3;
        private System.Windows.Forms.Button btnColor4;
        private System.Windows.Forms.CheckBox chkDistrByNewProg;
        private System.Windows.Forms.CheckBox chkНовоеОкноЗаказа;
        private System.Windows.Forms.TextBox osrm_server;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox chk_ОтладкаSIP;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox chk_UseNewOSRMProtocol;
    }
}