﻿#undef TRACE

using System.Diagnostics;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using csmapcontrol;
using System.Net;
using System.IO;
using System.Data.SqlClient;
using System.Xml;
using System.Reflection;






//using LumiSoft.SIP.UA.Resources;
/*
 * //машины - белая, фиолетовая, блеклая
 * Биты (0-4) (число)
0*: 'свободен' белая
2*: 'вышел на 5 минут' 
4*: 'занят, взял пассажира по заказу'
5: 'освободился, довез пассажира'
6: 'освободился, пассажир отказался от поездки'
7*: 'занят, ожидаю клиента на месте встречи' фиолетовая
8: 'занят, заказ взял, приступаю к выполнению'

Бит 6 – Тревога   
Бит 7 – Позвоните желтый
Бит 8 – Установлен GPS - красный GPS
Бит 9 – GPS дает корректные данные синий GPS

 * 
 * 
 * 
using LumiSoft.Net;
using LumiSoft.Net.SDP;
using LumiSoft.Net.SIP;
using LumiSoft.Net.SIP.Debug;
using LumiSoft.Net.SIP.Stack;
using LumiSoft.Net.SIP.Message;
 */
using LumiSoft.Net.Media;
using LumiSoft.Net.Media.Codec;
using LumiSoft.Net.Media.Codec.Audio;
/*using LumiSoft.Net.RTP;
using LumiSoft.Net.RTP.Debug;
using LumiSoft.Net.STUN.Client;
using LumiSoft.Net.UPnP.NAT;
 */
using System.Text;
using LumiSoft.Net;
using System.Windows;
using ONLINEDISPATCHER.resources;
using LumiSoft.Net.SIP.Stack;
using LumiSoft.Net.SDP;
using System.Collections;








/* 2. прокладка маршрута - автоматический запрос к серверу, разбор XML+++
 * 1. сохранение тайлов на диске - переработка компонента +++
 * 3. загрузка маркеров из базы данных. обновление маркеров. по таймеру картинка маркера. текст маркера
 * 4. получение координат объекта из базы - 5000 наименований. 80 тыс кооординат. возврат мнгновенный.
 * 
 * 
9	256		      100 000 000  	на линии в районе
104	772		    1 100 000 100  	на заказе
425	768		    1 100 000 000  	на линии но не в районе
1	1280	   10 100 000 000  	не на линии

 * 
 * 
 * 
 */



namespace ONLINEDISPATCHER
{
    public partial class MAIN_FORM : Form
    {

        //public IAXClientClass iaxc;

        public static settings form_settings = null; 


        ArrayList colorArray = new ArrayList() ;

        private Boolean TICK = false;

        public static int SIP_STATUS = 0;

        //public static int cnt_skipped_calls = 0;
        public static Hashtable skipped_calls = new Hashtable();


        DataTable orders_table = null;

       



        public static MAIN_FORM main_form_object = null;
        public static DateTime last_call_action = DateTime.MinValue;
        public static bool distr_by_new_prog = false;



        public static order_big active_order = null;

        //public  static System.Collections.Generic.List<SipCaller.incoming_call_info> incoming_calls_list = new List<SipCaller.incoming_call_info>();

        System.Collections.Hashtable car_attributes = new System.Collections.Hashtable();
        System.Collections.Hashtable zones_orders_count = new System.Collections.Hashtable();
        System.Collections.Hashtable zones_orders_closed_30_count = new System.Collections.Hashtable();
        System.Collections.Hashtable zones_cars_closed_count = new System.Collections.Hashtable();

        public System.Collections.Hashtable first_cars = new System.Collections.Hashtable();
        public System.Collections.Hashtable car_zones = new System.Collections.Hashtable();


        DataTable services = DataBase.mssqlRead("SELECT * FROM SERVICES where ID>0");
        DataTable cars = DataBase.mssqlRead("SELECT CARS.ID, cast (CARS.ID as varchar(6))+' '+  CARS.COLOR +' '+  CARS.mark +' '+ cars.gosnum +' '+drivers.fio +' '+ drivers.phones CAR_DRIVER_DESC  FROM CARS, DRIVERS WHERE DRIVERS.ID = CARS.DRIVERID order by CARS.ID");
        DataTable organizations = DataBase.mssqlRead("SELECT * FROM ORGANIZATIONS WHERE AGREEMENTSTATUS=1 order by name");

        int show_order_type = 0;
        
        
        //AudioOut audioout = new AudioOut(AudioOut.Devices[0], 8000, 16, 1);


        //String DataFile = @"c:\projects\OSM_CLIENT\database\osm_client.db3";


        public static Font table_font = Control.DefaultFont;
        public static Font table_bold_font = Control.DefaultFont;

        public static Color color_1 = Color.LightCyan;
        public static Color color_2 = Color.LightCyan;
        public static Color color_3 = Color.LightCyan;
        public static Color color_4 = Color.LightCyan;

        public static int OperatorID = 0;
        public static List<int> AdminList = new List<int>();
        public static String OperatorFIO = "(SYSTEM)";
        
        String DataFile = @"database\osm_client.db3";

        private int scale = 32;

        private double last_click_lat = 0;
        private double last_click_lon = 0;
        //private double to_lat = 0;
        //private double to_lon = 0;


        csmapcontrol.MapPath calculated_path = new csmapcontrol.MapPath();

        private Boolean calcDone = true;

        private double path_distance = 0;





        Marker from_marker = new Marker(), to_marker = new Marker();

        private Dictionary<string, car> mCars = new Dictionary<string, car>();



        public static caller caller = null; 

        private caller m_pIncomingCallUI = null;
        private int current_call_line = -1;



        public class car
        {
            //779||7-ка  1991г.в. 779||57690738||39805210
            public car(String car_data)
            {
                String[] s_arr = car_data.Split('|');
                if (s_arr.Length == 4)
                {
                    id = s_arr[0];
                    name = s_arr[1];
                    Double.TryParse(s_arr[2], out lat);
                    Double.TryParse(s_arr[3], out lon);
                    lat = lat / 1000000;
                    lon = lon / 1000000;
                }
            }
            public double lat = 0;
            public double lon = 0;
            public String name = "";
            public String id = "";
            public String status = "";

            public Marker marker = new Marker();
        }


        //private Dictionary<String, String> zones = new Dictionary<string, string>();


        public static bool OperatorIsAdmin()
        {
            if (AdminList.Count == 0)
            {
                AdminList.Add(129);
                AdminList.Add(123);
                AdminList.Add(139);
                AdminList.Add(115);
                AdminList.Add(147);
                AdminList.Add(120);
            }


            foreach (int id in AdminList)
            {
                if (id == MAIN_FORM.OperatorID)
                    return true;

            }
            return false;
        }

        public MAIN_FORM()
        {
            MAIN_FORM.main_form_object = this;

            colorArray.Add(new SolidBrush(Color.LightGreen));
            colorArray.Add(new SolidBrush(Color.Pink));
            colorArray.Add(new SolidBrush(Color.Red));



            




            InitializeComponent();


            

            
        }






        private void scrscale_ValueChanged(object sender, EventArgs e)
        {


        }

        private void Form1_Load(object sender, EventArgs e)
        {

            //InitMainForm();
            password_input_box box = new password_input_box();
            box.ShowDialog(this);
            if (box.DialogResult == DialogResult.OK) {


                //----- Create IAXClient object!



                form_settings = new settings();

                gisModule.doLoadGeoData();
                taxi_service_tarif.doLoadTarifs();

                InitMainForm();
                
            }
        }

       

        private void InitMainForm() {

            //mapControl1.ViewChanged += MapViewChanged;
            //mapControl1.LocationClicked += MapLocationClicked;
            //mapControl1.TrackPointClicked += TrackPointClicked;


            //MAIN_FORM.caller.main_form = this;
            

            this.Text += " " + OperatorFIO;
            //this.Text += " " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            DateTime dt = DateTime.Parse("2000/01/01");
            dt = dt.AddDays(System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Build);

            dt = dt.AddSeconds(System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Revision*2);
            

            this.Text += ".  Сборка: " + dt.ToString();

            mapControl1.CachePath = "./tiles";
            mapControl1.Latitude = 57.6315;
            mapControl1.Longitude = 39.8684;
            //Uri newUri = 
            if (settings.WEB_PROXY.Length == 0)
            {
                MapControl.webProxy = null;
                httpModule.webProxy = null;
            }
            else
            {
                WebProxy myProxy = new WebProxy();
                myProxy.Address = new Uri("http://"+settings.WEB_PROXY);
                MapControl.webProxy = myProxy;
                httpModule.webProxy = myProxy;
            }



            //57.6477&tlon=39.8684
            //39.8671&tlat=57.6315
            scale = 26;
            mapControl1.LonScale = MapControl.DiscreteScales[scale];



           // service_name.SelectedIndex = 0;

            // Add a Track for demo purposes...
            //Track t=new Track("../SwinstyReservoir.gpx");
            //mapControl1.Tracks.Add(t);

            mapControl1.HighlightEntities = true;


            mapControl1.MouseWheel += new MouseEventHandler(wheel);
            mapControl1.onMarkerHighlited += new MapControl.MarkerHighlited(mapControl1_onMarkerHighlited);


            timer2.Enabled = true;





            #region set filter combos


            //    ServiceID.Items.Clear();
            filterCar.DataSource = cars;
            filterCar.DisplayMember = "CAR_DRIVER_DESC";
            filterCar.ValueMember = "ID";
            //filterCar.DropDownStyle = ComboBoxStyle.DropDown;
            filterCar.AutoCompleteSource = AutoCompleteSource.ListItems;
            //  ZoneID.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            //filterCar.Text = "";
            filterCar.SelectedIndex = -1;
            
            filterCar.AutoCompleteMode = AutoCompleteMode.SuggestAppend;



            filterOrg.DataSource = organizations;
            filterOrg.DisplayMember = "Name";
            filterOrg.ValueMember = "ID";

            filterOrg.AutoCompleteSource = AutoCompleteSource.ListItems;
            filterOrg.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            filterOrg.SelectedIndex = -1;




            //    ServiceID.Items.Clear();
            filterOrderService.DataSource = services;
            filterOrderService.DisplayMember = "Name";
            filterOrderService.ValueMember = "ID";
            filterOrderService.AutoCompleteSource = AutoCompleteSource.ListItems;
            filterOrderService.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            filterOrderService.SelectedIndex = -1;

            //DataTable serv2 = ;
            filterCarService.DataSource = services.Copy();
            filterCarService.DisplayMember = "Name";
            filterCarService.ValueMember = "ID";
            filterCarService.AutoCompleteSource = AutoCompleteSource.ListItems;
            filterCarService.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            filterCarService.SelectedIndex = -1;


            filterOrderService.DropDownHeight = filterOrderService.ItemHeight * ((DataTable)filterOrderService.DataSource).Rows.Count;


            #endregion



            полноеРедактированиеЗаказаToolStripMenuItem.Visible = false;
            if ((OperatorID == 129) || (OperatorID == 123))
                полноеРедактированиеЗаказаToolStripMenuItem.Visible = true;


            //loadData();


            //System.Windows.Input.Mouse.AddPreviewMouseDownHandler(null, PreviewMouseDownEllipse);

            // ПОДКЛЮЧЕНИЕ к SIP серверу
        }

        /*void PreviewMouseDownEllipse(object sender, RoutedEventArgs e)
        { Debug.WriteLine("PreviewMouseDownEllipse"); }
*/

        delegate void SetStreetsCallback(string[] streets);


        private void SetStreets(string[] streets)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
       /*     if (this.from_street.InvokeRequired)
            {
                SetStreetsCallback d = new SetStreetsCallback(SetStreets);
                this.Invoke(d, new object[] { streets });
            }
            else
            {
                this.from_street.Items.AddRange(streets);
                to_street.Items.AddRange(streets);
                over_street1.Items.AddRange(streets);
                over_street2.Items.AddRange(streets);
            }
        */ 
        }


/*
        private void threadLoadData()
        {
            Console.WriteLine("hello!");

            SQLiteConnection con = new SQLiteConnection();

            con.ConnectionString = @"Data Source=" + DataFile + ";New=False;Version=3"; // в таком виде всё работает!
            try
            {
                con.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



            System.Data.SQLite.SQLiteCommand cmd = con.CreateCommand();



            cmd.CommandText = "SELECT DISTINCT street FROM addresses";

            List<String> streets = new List<String>();

            SQLiteDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                streets.Add(dr.GetString(0));
                
            }
            dr.Close();

            cmd.CommandText = "SELECT DISTINCT name FROM addresses";

            dr = cmd.ExecuteReader();




            while (dr.Read())
            {
                streets.Add(dr.GetString(0));

            }
            dr.Close();
            Console.WriteLine("hello 2!");

            this.SetStreets(streets.ToArray());
            
        }
*/        
        /*
        private void loadData()
        {
            System.Threading.Thread t = new System.Threading.Thread(new System.Threading.ThreadStart(threadLoadData));
            t.Start();


        }
        */
        private void wheel(object sender, MouseEventArgs e)
        {
            scale -= (int)(e.Delta / 120);

            if (scale < 0)
                scale = 0;
            if (scale > MapControl.DiscreteScales.Length - 1)
                scale = MapControl.DiscreteScales.Length - 1;

            mapControl1.LonScale = MapControl.DiscreteScales[scale];


        }

        private void mapControl1_ViewChanged(double lat, double lon, double scale)
        {

            if (System.Diagnostics.Process.GetCurrentProcess().WorkingSet64 > 50000000)
                MapControl.EmptyHalfTileCache();



        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void mapControl1_LocationClicked(double lat, double lon)
        {
            last_click_lat = lat;
            last_click_lon = lon;
            if ((Control.ModifierKeys & Keys.Shift) != Keys.Shift)
                return;

            if (calcDone == false)
                return;
            calculated_path.addLastPoint(lat, lon, "");

            doCalculate();




        }

        private void doUpdateCars()
        {
            if (leftTab.SelectedTab != tabMap)
                return;


            //if (mapControl1.onMarkerHighlited == null)
                

            /*
            if (use_sql.Checked == false) {
                mapControl1.MarkersCars.Clear();
                return;
            }
            */
            
//            SqlCommand cmd = m_conn.CreateCommand();
            //cmd.CommandText = "select cast(id as varchar(10))+cast('|' as varchar(10))+cast(descr as varchar(32))+cast(' ' as varchar(10))+cast(gosnum as varchar(10))+cast('|' as varchar(10))+cast(lat as varchar(10))+cast('|' as varchar(10))+cast(lon as varchar(10)), (status & 4) from cars where (status & 1024 = 0)";
            //SqlDataReader dr =  cmd.ExecuteReader();

            //DataTable cars = DataBase.mssqlRead("select cast(id as varchar(10))+cast('|' as varchar(10))+cast(descr as varchar(32))+cast(' ' as varchar(10))+cast(gosnum as varchar(10))+cast('|' as varchar(10))+cast(lat as varchar(10))+cast('|' as varchar(10))+cast(lon as varchar(10))+'|'+cast(case when program_type is null then 0 else program_type end as varchar(10)), case when (mobilestatus & 15)=0 then 0 else 4 end, program_type, lat, lon, ID from cars where (status & 1024 = 0) or ((cars.mobileStatus&1024 <> 1024)  and network_channel_id > 0) and datediff(minute, dtLastUpdate, getdate())< 10");
            DataTable cars = DataBase.mssqlRead("select id, mark+' '+descr +' госномер '+ gosnum, mobilestatus & 15,lat, lon from cars where (status & 1024 = 0) or ((cars.mobileStatus&1024 <> 1024)  and network_channel_id > 0) and datediff(minute, dtLastUpdate, getdate())< 10");

            //mapControl1.MarkersCars.Clear();
            mapControl1.carsMarkers.Clear();

            ///что можно нарисовать по машине:
            ///. стрелку куда едет: если на заказ - точка заказ, если по заказу  - точку окончания заказа
            ///. путь за последние полчаса
            ///


            foreach(DataRow row in cars.Rows)
            {


                if (row[3] != DBNull.Value)
                {

                    //System.Console.WriteLine(s);
                    int ilat = (int)row[3];
                    int ilon = (int)row[4];
                    Marker car = new Marker();
                    car.lat = ilat / 1000000.0;
                    car.lon = ilon / 1000000.0;
                    car_data cardata = new car_data();
                    cardata.car_id = row[0].ToString();
                    cardata.car_desc = row[1].ToString();
                    cardata.car_state = (int)row[2];


                    car.Tag = cardata;
                    mapControl1.carsMarkers.Add(car);
                }
                //MapControl.MarkerCar c = new MapControl.MarkerCar(s, on_order);
                //cma .Marker car = new 

                //{
                    //if(mapControl1.MarkersCars.ContainsKey(c.id))
                  //  {
                      //  mapControl1.MarkersCars.Remove(c.id);
                    //}
                  //  mapControl1.MarkersCars.Add(c.id, c);
                //}
            }
            
          



//            dr.Close();
            mapControl1.Invalidate();
            



            /*            cmd.CommandText = "SELECT ID, DESCR  FROM ZONES ORDER BY ID";
            dr = cmd.ExecuteReader();
            while (dr.Read()){
                zones.Add(dr.GetString(0), dr.GetString(0));
            }


            zones
            */




        }

        void mapControl1_onMarkerHighlited(object sender, Marker marker) {
            //Процедура заполнения текущими данными маркера.
            //вызывается при наведении на маркер мышки
            Console.Out.WriteLine("onMarkerHighlited " + marker.ToString());
            if (marker.Tag == null)
                return;
            if ( marker.Tag.GetType() == typeof(car_data))
            {
                car_data cd = (car_data)marker.Tag;
                DataTable dt = DataBase.mssqlRead("select top 1 lat,lon,to_lat,to_lon, AddrFrom+' '+HouseFrom+'->'+AddrTo+' '+houseTo order_address from orders where dtarrive > getdate()-0.1 and carid = " + cd.car_id + " and dtEnd is null");
                if (dt.Rows.Count > 0)
                {
                    int lat = (int)dt.Rows[0]["lat"];
                    int lon = (int)dt.Rows[0]["lon"];
                    int to_lat = (int)dt.Rows[0]["to_lat"];
                    int to_lon = (int)dt.Rows[0]["to_lon"];
                    String order_address = (String)dt.Rows[0]["order_address"];


                    cd.to_lat = to_lat / 1000000.0;
                    cd.to_lon = to_lon / 1000000.0;
                    cd.order_lat = lat / 1000000.0;
                    cd.order_lon = lon / 1000000.0;
                    cd.order_address = order_address;

                }
            }






        }
        /*
        public void doShowPath(csmapcontrol.MapPath path) {
            mapControl1.Paths.Clear();
            mapControl1.Paths.Add(path);

            mapControl1.Invalidate();
        }
         */

        private void doCalculate()
        {


           // csmapcontrol.MapPath.PathPoint from = new csmapcontrol.MapPath.PathPoint(from_lat, from_lon, "");
           // csmapcontrol.MapPath.PathPoint to = new csmapcontrol.MapPath.PathPoint(to_lat, to_lon, "");

            //path.addInterPoint(from_lat, from_lon, "");
            //path.addInterPoint(to_lat, to_lon, "");
/*
            calculated_path.calculatePath(4000);
            mapControl1.Paths.Clear();
            mapControl1.Paths.Add(calculated_path);

            mapControl1.Invalidate();


            distance.Text = String.Format("{0:.00} км", calculated_path.path_distance);


            service_name_SelectedIndexChanged(null, null);


            calcDone = true;
 */ 
        }


        private void button1_Click(object sender, EventArgs e)
        {
            //http://wiki.openstreetmap.org/wiki/YOURS
            //http://www.yournavigation.org/api/1.0/gosmore.php?format=kml&flat=57.6929&flon=39.7779&tlat=57.5930&tlon=39.8653&v=motorcar&fast=1&layer=mapnik&instructions=1
            String c = "39.77779,57.69285 39.778449,57.692441 39.778303,57.692247 39.776215,57.691203 39.775907,57.691048 39.775683,57.690935 39.777974,57.689605 39.778278,57.689419 39.778529,57.689261 39.779056,57.68897 39.784246,57.686027 39.784726,57.685705 39.784562,57.685559 39.783526,57.684518 39.779385,57.680358 39.779303,57.680278 39.773797,57.674885 39.773765,57.674756 39.773001,57.67395 39.77217,57.673126 39.771777,57.672791 39.770774,57.671819 39.770532,57.671625 39.770289,57.671468 39.769653,57.671076 39.769392,57.670861 39.769211,57.670655 39.768948,57.67034 39.768521,57.669676 39.768285,57.669322 39.768011,57.668991 39.766994,57.667967 39.765207,57.666441 39.762254,57.664003 39.76129,57.663216 39.761082,57.663047 39.757926,57.66049 39.756622,57.659404 39.755885,57.658645 39.754847,57.657285 39.752181,57.652914 39.751022,57.65147 39.749007,57.649565 39.748738,57.64931 39.748482,57.649059 39.748284,57.648837 39.748117,57.648626 39.747948,57.648384 39.7477,57.647949 39.747575,57.647705 39.747517,57.647578 39.747442,57.64736 39.747413,57.647229 39.747397,57.647095 39.747371,57.646771 39.74736,57.646359 39.747613,57.641755 39.747621,57.64121 39.747613,57.640972 39.747592,57.640717 39.747554,57.640365 39.747445,57.639772 39.747391,57.639438 39.747302,57.639089 39.74717,57.638673 39.74656,57.636959 39.746314,57.636273 39.746199,57.635979 39.745934,57.635393 39.745881,57.6352 39.745861,57.635033 39.745869,57.63485 39.745913,57.634693 39.745967,57.63453 39.746047,57.634406 39.746162,57.634288 39.746327,57.634171 39.746557,57.634058 39.746786,57.633961 39.747639,57.633688 39.747987,57.633572 39.748286,57.633446 39.756072,57.629564 39.757638,57.628782 39.762681,57.626313 39.778328,57.617862 39.779144,57.617399 39.77999,57.616908 39.780629,57.61651 39.781122,57.616186 39.781411,57.615965 39.78214,57.615345 39.783277,57.614362 39.783323,57.614322 39.785574,57.612258 39.791164,57.607196 39.795714,57.603046 39.796134,57.602686 39.797017,57.601886 39.797399,57.601528 39.799413,57.599704 39.800026,57.599216 39.800468,57.598878 39.800977,57.598527 39.801383,57.598285 39.801765,57.5981 39.80218,57.597941 39.802641,57.597792 39.803135,57.597654 39.803801,57.597494 39.804929,57.597266 39.80712,57.596919 39.807277,57.596891 39.808186,57.596727 39.808311,57.596705 39.809666,57.596368 39.810718,57.595867 39.81149,57.59539 39.812573,57.594483 39.816291,57.591182 39.816513,57.590995 39.817855,57.589644 39.818562,57.588929 39.819424,57.588058 39.820603,57.586865 39.822114,57.585298 39.822822,57.584644 39.82398,57.583839 39.825588,57.583038 39.833652,57.57984 39.83503,57.579288 39.835735,57.579006 39.838442,57.577923 39.839669,57.577432 39.839923,57.577331 39.840025,57.57729 39.840178,57.577229 39.844045,57.575683 39.844371,57.575575 39.845418,57.576378 39.845683,57.576588 39.846396,57.577154 39.847986,57.578316 39.850554,57.580177 39.852036,57.581388 39.852082,57.581425 39.852422,57.5817 39.852987,57.582196 39.85407,57.583277 39.855106,57.584358 39.855744,57.585078 39.857382,57.58701 39.858517,57.588354 39.858606,57.58847 39.859132,57.589158 39.859601,57.58973 39.859857,57.590042 39.860435,57.590591 39.861251,57.591255 39.861434,57.591382 39.861618,57.59151 39.863246,57.59254 39.863795,57.592868 39.863956,57.592971 39.864919,57.593573 39.865547,57.593971 39.865593,57.593663 39.865448,57.593279 39.865632,57.593095";
            String[] c_arr = c.Split(' ');
            Track t = new Track();
            for (int i = 0; i < c_arr.Length; i++)
            {
                String[] c1 = c_arr[i].Split(',');
                c1[0] = c1[0].Replace('.', ',');
                c1[1] = c1[1].Replace('.', ',');
                TrackPoint tp = new TrackPoint();
                tp.Lon = Double.Parse(c1[0]);
                tp.Lat = Double.Parse(c1[1]);

                t.Points.Add(tp);
            }

            mapControl1.Tracks.Add(t);

        }

        private void diagnostic_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            doCalculate();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            doUpdateCars();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            TICK = !TICK;

            int current_calls = 0;
            foreach (String key in skipped_calls.Keys) {

                DateTime call_dt = (DateTime) skipped_calls[key];
                if (call_dt.Add(TimeSpan.FromSeconds(2)) > DateTime.Now) {
                    current_calls++;
                    //skipped_calls.Remove(key);
                    //break;
                }
            }
            

            if (MAIN_FORM.skipped_calls.Count == 0)
                SkippedCalls.Text = "";
            else
                SkippedCalls.Text = current_calls.ToString()+", "+ MAIN_FORM.skipped_calls.Count.ToString() + " всего";


            if (leftTab.SelectedTab != tabMap)
                return;

            doUpdateCars();

            order_big ord = MAIN_FORM.active_order;
            if (ord != null)
            {
                
                /* 1. задать карту маршрут если его там нет и если он есть в заказе
                 *  следующее действие - копирование маршрута из карты в заказ если точки разные - пометить их как измененные
                 */


                if (csmapcontrol.MapControl.ORDER_CALCULATED_PATH != ord.ORDER_PATH)
                    MapControl.ORDER_CALCULATED_PATH = ord.ORDER_PATH;


                if (ord.ORDER_PATH.need_calculate)
                {
                    ord.doCalculatePath();

                    ord.UpdateZonesByOnGeoPointChanges();
                    mapControl1.Invalidate();
                }

                mapControl1.Invalidate();
                return;


                if (csmapcontrol.MapControl.ORDER_CALCULATED_PATH.InterMarkers.Count == 0)
                    if (ord.ORDER_PATH.getLastPoint().lat!=0)
                        if (ord.ORDER_PATH.getFirstPoint().lat!=0)
                        {
                            //csmapcontrol.MapControl.ORDER_CALCULATED_PATH.Clear();


                            csmapcontrol.MapControl.ORDER_CALCULATED_PATH.setFirstPoint(ord.ORDER_PATH.getFirstPoint().lat, ord.ORDER_PATH.getFirstPoint().lon, "");
                            csmapcontrol.MapControl.ORDER_CALCULATED_PATH.setLastPoint(ord.ORDER_PATH.getLastPoint().lat, ord.ORDER_PATH.getLastPoint().lon, "");

                            //Добавить промежуточные точки если их нет.

                            for (int i = 1; i < ord.ORDER_PATH.InterMarkers.Count - 1; i++)
                            {
                                csmapcontrol.MapControl.ORDER_CALCULATED_PATH.setPoint(i, ord.ORDER_PATH.InterMarkers[i].lat, ord.ORDER_PATH.InterMarkers[i].lon, "");
                            }

                        }


                if (csmapcontrol.MapControl.ORDER_CALCULATED_PATH.InterMarkers.Count > 0)
                {
                    MapPath path = csmapcontrol.MapControl.ORDER_CALCULATED_PATH;

                    bool need_update_path = false;

                    for (int i = 0; i < path.InterMarkers.Count; i++)
                    {
                        if ((path.InterMarkers[i].lat != ord.ORDER_PATH.getInterPoint(i).lat) ||
                            (path.InterMarkers[i].lon != ord.ORDER_PATH.getInterPoint(i).lon))
                        {
                            //ord.geo_location_from.isMoved = 1;
                            ord.ORDER_PATH.setPoint(i, path.InterMarkers[i].lat, path.InterMarkers[i].lon, "");
                            ord.ORDER_PATH.InterMarkers[i].MarkerMoved = 1;
                            //ord.ORDER_PATH.InterMarkers[i].lat = path.InterMarkers[i].lat;
                            //ord.ORDER_PATH.InterMarkers[i].lon = path.InterMarkers[i].lon;
                            need_update_path = true;
                        }
                    }
                    

                    if (need_update_path)
                    {
                        ord.doCalculatePath();

                        ord.UpdateZonesByOnGeoPointChanges();
                        mapControl1.Invalidate();
                    }



                    
                }













                /*

                if (ord.geo_location_from != null)
                {


                    if (csmapcontrol.MapControl.ORDER_CALCULATED_PATH.InterMarkers.Count == 0)
                    if (ord.geo_location_to != null)
                    if (ord.geo_location_from != null)
                    {
                        csmapcontrol.MapControl.ORDER_CALCULATED_PATH.setFirstPoint(ord.geo_location_from.lat, ord.geo_location_from.lon, "");
                        csmapcontrol.MapControl.ORDER_CALCULATED_PATH.setLastPoint(ord.geo_location_to.lat, ord.geo_location_to.lon, "");
                    }

                    if (csmapcontrol.MapControl.ORDER_CALCULATED_PATH.InterMarkers.Count > 0)
                    {
                        MapPath path = csmapcontrol.MapControl.ORDER_CALCULATED_PATH;

                        bool need_update_path = false;
                        if (path.InterMarkers.Count > 0)
                        {
                            if ((path.InterMarkers[0].lat != ord.geo_location_from.lat) ||
                            (path.InterMarkers[0].lon != ord.geo_location_from.lon))
                            {
                                ord.geo_location_from.isMoved = 1;
                                ord.geo_location_from.lat = path.InterMarkers[0].lat;
                                ord.geo_location_from.lon = path.InterMarkers[0].lon;
                                need_update_path = true;

                            }
                            //последняя точка в пути - ее тоже сравнить в той что в заказе
                            if (path.InterMarkers.Count > 1)
                            {
                                
                                if ((path.InterMarkers[path.InterMarkers.Count-1].lat != ord.geo_location_to.lat) ||
                               (path.InterMarkers[path.InterMarkers.Count - 1].lon != ord.geo_location_to.lon))
                                {
                                    ord.geo_location_to.isMoved = 1;
                                    ord.geo_location_to.lat = path.InterMarkers[path.InterMarkers.Count - 1].lat;
                                    ord.geo_location_to.lon = path.InterMarkers[path.InterMarkers.Count - 1].lon;

                                    need_update_path = true;
                                    
                                }
                            }

                            if (need_update_path)
                            {
                                ord.doCalculatePath();

                                ord.UpdateZonesByOnGeoPointChanges();
                                mapControl1.Invalidate();
                            }
                             
                            
                            
                        }
                    }

                    
                }



                */







            }
            else
            {
                //mapControl1.Paths.Clear();
                MapControl.ORDER_CALCULATED_PATH.Clear();

            }



            doShowOrdersOnMap();
            if (chkMapShowOrders.GetItemChecked(0)==false)
                 mapControl1.Markers.Clear();
                
        }

        public void doClearMarkers() {

            mapControl1.Markers.Clear();
        }

        public void doClearAdressMarkers() {

            mapControl1.addressMarkers.Clear();
        }

        public void doShowMarker(int lat, int lon, String descr) {
            Marker m = new Marker();
            m.lat = lat / 1000000.0;
            m.lon = lon / 1000000.0;
            m.Tag = descr;
            mapControl1.Markers.Add(m);
        }

        public void doShowAddressMarker(int lat, int lon, String descr) {
            Marker m = new Marker();
            m.lat = lat / 100000.0;
            m.lon = lon / 100000.0;
            m.Tag = descr;

            mapControl1.addressMarkers.Add(m);
        }

        private void doShowOrdersOnMap()
        {
            
            
            {
                if (chkMapShowOrders.GetItemChecked(0) == true)
                {

                    int ms_start = DateTime.Now.Second * 1000 + DateTime.Now.Millisecond;
                    //System.Console.WriteLine(n.ToString() + ". Время обработки " + (DateTime.Now.Second * 1000 + DateTime.Now.Millisecond - ms_start).ToString() + "мсек");


                    mapControl1.Markers.Clear();
                    //показать свободные заказы

                    DataTable dt = ((DataTable) view_work_orders.DataSource).Copy();

                    foreach (DataRow row in dt.Rows)
                    {
                        if (row["CARID"].ToString().CompareTo("0")!=0)
                            continue;
                        String lat = (String)row["LAT"];
                        String lon = (String)row["LON"];
                        String ORDERID_string = (String)row["ID"];
                        int orderid = int.Parse(ORDERID_string);
                        String orderText = (String)row["SHORT_ADDRFROM"];
                        String to = (String)row["ADDRTO"];
                        
                        bool orderCoord = true;

                        String latlon = "0.0 0.0";
                        if (lat == "0")
                        {

                            //latlon = MAIN_FORM.doYandexSearchAddress("ЯРОСЛАВЛЬ+" + orderText, "");
                            orderCoord = false;
                        }
                        else 
                            latlon = ((double.Parse(lon)/1000000.0).ToString()+ " "+ ((double.Parse(lat)/1000000.0).ToString()));
                        
                        latlon = latlon.Replace('.', ',');
                        System.Console.WriteLine(latlon + " " + orderText);

                        if (latlon != "0.0 0.0")
                        {
                            
                            
                            Double lat1, lon1;
                            lon1 = Double.Parse(latlon.Split(' ')[0]);
                            lat1 = Double.Parse(latlon.Split(' ')[1]);

                            if (!orderCoord)
                            {
                                //записать координаты в заказ
                                int int_lat =(int) (lat1 *1000000);
                                int int_lon = (int)(lon1 * 1000000);

                                String[] fields = { "LAT", "LON"};
                                object[] values = { int_lat, int_lon };

                              //  DataBase.mssqlUpdate("ORDERS", fields, values, "ID", orderid);
                                    
                            }

                            doShowMarker((int)(lat1 * 1000000), (int)(lon1*1000000), orderText+"->"+to);

//                            Marker m = new Marker();
                            //m.lat = lat1;
                            //m.lon = lon1;
                            //m.Tag = orderText+"->"+to;
                            //mapControl1.Markers.Add(m);
                        }
                        
                    }

                    //
                    System.Console.WriteLine("Время вывода заказов на карту" + (DateTime.Now.Second * 1000 + DateTime.Now.Millisecond - ms_start).ToString() + "мсек");
                }
            }

        }


        /*        private void loadCSV()
                {


                    SQLiteConnection con = new SQLiteConnection();
                    con.ConnectionString = @"Data Source=" + DataFile + ";New=False;Version=3"; // в таком виде всё работает!
                    try
                    {
                        con.Open();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }



                    System.Data.SQLite.SQLiteCommand cmd = con.CreateCommand();
                    SQLiteTransaction trans = con.BeginTransaction();
                    int n = 0;
                    StreamReader sr = new StreamReader("c:/addresses.csv");

                    String s = "";
                    while (sr.Peek() >= 0)
                    {
                        String[] adr = sr.ReadLine().Split(';');
                        if (adr.Length == 6)
                        {
                            double lat, lon;
                            double.TryParse(adr[3], out lat);
                            double.TryParse(adr[4], out lon);
                            cmd.CommandText = "insert into addresses(street, building_num, lat, lon, name) values(@street, @house_number, @lat, @lon, @name)";
                            cmd.Parameters.AddWithValue("@street", adr[0]);
                            cmd.Parameters.AddWithValue("@house_number", adr[1]);
                            cmd.Parameters.AddWithValue("@lat", lat);
                            cmd.Parameters.AddWithValue("@lon", lon);
                            cmd.Parameters.AddWithValue("@name", adr[2]);

                            cmd.ExecuteNonQuery();
                        }
                        n++;

                    }

                    trans.Commit();
                    Console.WriteLine(n.ToString() + "nodes read");
                    //log.Text += nodes.Count.ToString() + " узлов прочитано";
                    Console.ReadLine();
                }
        */

        /*        private void btnLoadXML_Click(object sender, EventArgs e)
                {


                    SQLiteConnection con = new SQLiteConnection();
                    con.ConnectionString = @"Data Source=" + DataFile + ";New=False;Version=3"; // в таком виде всё работает!
                    try
                    {
                        con.Open();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }



                    System.Data.SQLite.SQLiteCommand cmd = con.CreateCommand();
                    SQLiteTransaction trans = con.BeginTransaction();
                    int n = 0;
                    XmlTextReader reader = new XmlTextReader("c:/ru-yar.xml");
                    String last_id = "";   //для вставки в связки и таги - последний путь по которому прошелся парсер
                    while (reader.Read())
                    {
                        n++;
                        switch (reader.NodeType)
                        {
                            case XmlNodeType.Element: // Узел является элементом.
                                if (reader.Name == "node")
                                {
                                    //Console.Write("<" + reader.Name);
                                    //Console.WriteLine("> id = " + reader.GetAttribute("id"));
                                    cmd.CommandText = "insert into primitives(id, name, uid, lat, lon) values('" +
                                        reader.GetAttribute("id") + "','node', '" + reader.GetAttribute("uid") + "','" +
                                    reader.GetAttribute("lat") + "','" + reader.GetAttribute("lon") + "')";
                                    cmd.ExecuteNonQuery();
                                    last_id = reader.GetAttribute("id");
                                    //               

                                }
                                if (reader.Name == "way")
                                {
                                    cmd.CommandText = "insert into primitives(id, name, uid) values('" +
                                        reader.GetAttribute("id") + "','way', '" + reader.GetAttribute("uid") + "')";
                                    cmd.ExecuteNonQuery();
                                    last_id = reader.GetAttribute("id");
                                    //загрузить список нодов
                                }
                                if (reader.Name == "relation")
                                {
                                    cmd.CommandText = "insert into primitives(id, name, uid) values('" +
                                        reader.GetAttribute("id") + "','relation', '" + reader.GetAttribute("uid") + "')";
                                    cmd.ExecuteNonQuery();
                                    last_id = reader.GetAttribute("id");
                                    //загрузить список нодов
                                }
                                if (reader.Name == "nd")
                                {
                                    cmd.CommandText = "insert into nd(primitive_id, ref) values('" +
                                        last_id + "','" + reader.GetAttribute("ref") + "')";
                                    cmd.ExecuteNonQuery();
                                }
                                if (reader.Name == "tag")
                                {
                                    cmd.CommandText = "insert into tags(primitive_id, name, value) values(@primitive_id, @name, @value)";
                                    cmd.Parameters.AddWithValue("@primitive_id", last_id);
                                    cmd.Parameters.AddWithValue("@name", reader.GetAttribute("k"));
                                    cmd.Parameters.AddWithValue("@value", reader.GetAttribute("v"));

                                    cmd.ExecuteNonQuery();
                                }


                                break;
                            case XmlNodeType.Text: // Вывести текст в каждом элементе.
                                //Console.WriteLine(reader.Value);
                                break;
                            case XmlNodeType.EndElement: // Вывести конец элемента.
                                //Console.Write("</" + reader.Name);
                                //Console.WriteLine(">");
                                break;
                        }


                    }

                    trans.Commit();
                    Console.WriteLine(n.ToString() + "nodes read");
                    //log.Text += nodes.Count.ToString() + " узлов прочитано";
                    Console.ReadLine();

                }
        */
        private void button4_Click(object sender, EventArgs e)
        {
            /*String [] c = coord_from.Text.Replace('.', ',').Split(' ');
            if (c.Length == 2)
            {
                Double.TryParse(c[1], out from_lat);
                Double.TryParse(c[0], out from_lon);
            }
           

            c = coord_to.Text.Replace('.', ',').Split(' ');
            if (c.Length == 2)
            {
                Double.TryParse(c[1], out to_lat);
                Double.TryParse(c[0], out to_lon);
            }
            */

            //if (calculated_path
            //calculated_path.InterPoints[

            doCalculate();

        }

        private void btnLoadCsv_Click(object sender, EventArgs e)
        {
         //   loadCSV();
        }

        private void path_distance_Click(object sender, EventArgs e)
        {

        }

        private void service_name_SelectedIndexChanged(object sender, EventArgs e)
        {
           

        }

       




        private void pointer1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawLine(Pens.Green, 0, 0, 10, 10);
        }

        
       
        

        private void очиститьМаршрутToolStripMenuItem_Click(object sender, EventArgs e)
        {
            calculated_path.InterMarkers.Clear();
            calculated_path.Points.Clear();
            calculated_path.path_distance = 0;
            mapControl1.Invalidate();
        ;

        }

        private void coord_from_TextChanged(object sender, EventArgs e)
        {
        /*    String[] arr = coord_from.Text.Replace('.', ',').Split(' ');
            if (arr.Length == 2)
            {
                double lat, lon;

                if ((Double.TryParse(arr[0], out lat)) && (
                        Double.TryParse(arr[1], out lon)))
                {
                    calculated_path.setPoint(0, lat, lon, "");
                }
            }
         */ 
        }




        private DataTable UpdateListOrders()
        {

            int ms_start = DateTime.Now.Second * 1000 + DateTime.Now.Millisecond;

            /*Заполнить список свободных заказов
             */

            ///ВЫвести предварительные заказы в работу
            ///
           /* try
            {
                DataBase.mssqlExecuteSQL("update orders set orders.state=0, dtArrive=getdate() where dtPredvar > getdate()-14 and orders.state = 5 and datediff(minute, getdate(), dtPredvar )<predvShowDelta");
            }
            catch(Exception ex){};
            */


            try
            {


                String sql1 = "  select  ID," +
                " (select descr  from zones where id = orders.zoneID) ZONE_NAME, " +
                " CARID " +
                " from orders with (NOLOCK) " +
                " where datediff(hour, dtArrive, getdate())<12 and dtArrive > getdate()-1 and orders.state <>3  and orders.state <>5";
                ////обновить таблицу количества зон и районов
                DataTable dt_1 = DataBase.mssqlRead(sql1);
                lock (zones_orders_count)
                {

                    zones_orders_count.Clear();
                    foreach (DataRow row in dt_1.Rows)
                    {
                        String zone_name = row["ZONE_NAME"].ToString();
                        if ((row["CARID"].ToString() == "") || row["CARID"].ToString().CompareTo("0") == 0)
                        {
                            if (!zones_orders_count.ContainsKey(zone_name))
                            {
                                zones_orders_count[zone_name] = 1;
                            }
                            else
                                zones_orders_count[zone_name] = (int)zones_orders_count[zone_name] + 1;
                        }
                    }

                }


                //заполнить список машин, идущих в район
                //заполнить список заказов, ушедших из района
                //zones_orders_closed_30_count = new System.Collections.Hashtable();
                //System.Collections.Hashtable zones_cars_closed_count = n
                sql1 = "  select  ID," +
                " (select descr  from zones where id = orders.RaionTo) ZONE_NAME, " +
                " CARID " +
                " from orders with (NOLOCK) " +
                " where dtArrive > getdate() - 12.0/24.0  and orders.state <>3  and orders.state <>5";
                ////обновить таблицу количества зон и районов
                dt_1 = DataBase.mssqlRead(sql1);
                lock (zones_cars_closed_count)
                {

                    zones_cars_closed_count.Clear();
                    foreach (DataRow row in dt_1.Rows)
                    {
                        String zone_name = row["ZONE_NAME"].ToString();
                        if (row["CARID"].ToString() != "")
                        {
                            if (!zones_cars_closed_count.ContainsKey(zone_name))
                            {
                                zones_cars_closed_count[zone_name] = 1;
                            }
                            else
                                zones_cars_closed_count[zone_name] = (int)zones_cars_closed_count[zone_name] + 1;
                        }
                    }

                }
            }
            catch (Exception ex) { };



            try
            {


                String sql1 = "  select  ID," +
                           " (select descr  from zones where id = orders.ZoneId) ZONE_NAME, " +
                           " CARID " +
                           " from orders with (NOLOCK) " +
                           " where dtEnd > getdate() - 1.0/(24.0*2.0) and dtArrive > getdate()-1 and orders.state =3  and orders.resultcode=1";
                ////обновить таблицу количества зон и районов
                DataTable dt_1 = DataBase.mssqlRead(sql1);
                lock (zones_orders_closed_30_count)
                {

                    zones_orders_closed_30_count.Clear();
                    foreach (DataRow row in dt_1.Rows)
                    {
                        String zone_name = row["ZONE_NAME"].ToString();
                        //if (row["CARID"].ToString() != "")
                        {
                            if (!zones_orders_closed_30_count.ContainsKey(zone_name))
                            {
                                zones_orders_closed_30_count[zone_name] = 1;
                            }
                            else
                                zones_orders_closed_30_count[zone_name] = (int)zones_orders_closed_30_count[zone_name] + 1;
                        }
                    }

                }




            }
            catch (Exception ex) { };


            String sql = "";
            List<SqlParameter> parameters = new List<SqlParameter>();
            try
            {




                


                

                sql += "  select taxometr_enabled, tax_uslugi,  " +
                    " dbo.format_date_delta(dtArrive, getdate()) dtArrive, " +
                    " case when predvar = 1 then dbo.format_date_delta(dtPredvar, getdate()) else null end dtPredvar, " +
                    " dbo.format_date_delta(dtBegin, getdate()) dtBegin, " +

                    " dtEnd,  orders.id, case when connectPhone <> '' then connectPhone+', ' else '' end +  phone phone, orders.carid," +
    " (select '['+descr+']  ' from zones where id = orders.zoneID)+ " +

    " addrFrom +' '+ houseFrom +'-'+flatFrom ADDRFROM, addrTo+' '+houseto addrTo, orders.state order_state, " +

    " case when carid > 0 then cast(cars1.id as varchar(10)) + ' ' +cars1.color +' '+cars1.mark + ' ' + cars1.gosnum else cast(distrib_current_carid as varchar(10)) + ' ('+cast(datediff(second, distrib_last_try_tm, getdate()) as varchar(10))+')' end car_name, " +



    " case when cars2.dtLastUpdate < getdate() - 1.0/24/12 then '' else ' '+ cast (cars2.speed as varchar(10))+' км/ч'  end  car_speed,  " +
    " case when cars2.dtLastUpdate < getdate() - 1.0/24/12 then 0 else cars2.lat end  car_lat,  " +
    " case when cars2.dtLastUpdate < getdate() - 1.0/24/12 then 0 else cars2.lon end  car_lon,  " +
    " cars1.state car_state, " +
    " case when cars1.network_channel_id>0 then cars1.mobileStatus else cars1.status end car_status, " +
    " cast(   case when (select top 1 status+cast(tries as varchar(10)) from call_out where order_id=orders.id and nivr=2) is null then '  ' else (select top 1 status+cast(tries as varchar(10)) from call_out where order_id=orders.id and nivr=2) end     as varchar(2))+' '+cast(orders.cnt_auto_callback as varchar(2)) dial_status, " +
    " (select services.name from services where services.id = orders.serviceid) service_name, " +
    " orders.clientname, " +
    " (select OPERATORS.FIO from OPERATORS WHERE OPERATORS.ID=ORDERS.OPID) OPERATOR, " +
    " (select descr  from zones where id = orders.zoneID) ZONE_NAME, " +
    " BnalMoney sum," +
    " case when predvar=1 then dtPredvar else dtArrive end ord_date," +
    " orders.descr, " +
    " orders.time2meet, " +
    " orders.VIP, orders.agreement," +
    " addrFrom +' '+ houseFrom SHORT_ADDRFROM, ORDERS.LAT, ORDERS.LON, ORDERS.TalonNumber, " +
    " Phone phone1, connectPhone phone2, " +
    " addrFrom addrfrom1, housefrom housefrom1, flatfrom flatfrom1 , orders.sub_status, " +
    " distrib_last_try_tm, distrib_status, cast(distrib_current_carid as varchar(10)) + ' ('+cast(datediff(second, distrib_last_try_tm, getdate()) as varchar(10))+')' as distrib_current_carid," +
    " orders.tax_money order_sum," +
    " (select name  from organization_detail where id = orders.OrganizationDetailId) ORGANIZATION_DETAIL_NAME ," +
    " (select interpoints from mobile_split_order_interpoints(orders.calc_path_points)) INTERPOINTS," +
    " orders.serviceid , orders.visible_distance "
    ;


                //this.Invoke((new MethodInvoker(delegate()
                //    {
                if (show_order_type == 3)
                { //архив с фильтрами

                    //01-07-2014sql += ", (select sum(sum) from bank where orderid = orders.id and type=2) bank_sum ";
                    sql += ",  (select doc_summ from balance_reg where doc_id = orders.id and doc_typ='ORDER' and FROM_DOC_TYP='DRIVER' ) bank_sum ";
                }
                else sql += ", 0 bank_sum";
                //    })));




                sql += " from orders with (NOLOCK) " +
                " left join cars cars1 with (NOLOCK) on cars1.id=orders.carid " +
                " left join cars cars2 with (NOLOCK) on cars2.id=orders.distrib_current_carid " +
                " where ";
                if (show_order_type == 0)
                {
                    sql += "datediff(day, dtArrive, getdate())<2  and orders.state <>3  and orders.state <>5";
                }
                if (show_order_type == 1)
                {
                    sql += " datediff(day, dtPredvar, getdate())<1000  and orders.state = 5";
                }
                if (show_order_type == 2)
                {
                    sql += " datediff(hour, dtPredvar, getdate())<12 and orders.state = 3";
                }

                if (show_order_type == 3)
                {
                    sql += " 1=1 ";
                }


            


                main_form_object.Invoke((new MethodInvoker(delegate()
        {

            if ((show_order_type == 3) ||  (show_order_type == 1))
            if (filterOrg.SelectedValue != null)
                try
                {
                    int orgid = (int)filterOrg.SelectedValue;
                    parameters.Add(new SqlParameter("orgid", orgid));
                    sql += " and orgid=@orgid";
                }
                catch (Exception ex) { }


            if ((show_order_type == 3))
            { //архив с фильтрами
                DateTime dt1, dt2;

                dt1 = filterDt1.Value.Date.Add(filterTm1.Value.TimeOfDay);
                dt2 = filterDt2.Value.Date.Add(filterTm2.Value.TimeOfDay);

                parameters.Add(new SqlParameter("dt1", dt1));
                parameters.Add(new SqlParameter("dt2", dt2));
                sql += " and dtEnd between @dt1 and @dt2 ";
                //if (filterPhone.Text !="")
                parameters.Add(new SqlParameter("phone", "%" + filterPhone.Text + "%"));
                parameters.Add(new SqlParameter("address", "%" + filterAddress.Text + "%"));

                if (filterCar.SelectedValue != null)
                {
                    try
                    {
                        int carid = (int)filterCar.SelectedValue;
                        parameters.Add(new SqlParameter("carid", carid));
                        sql += " and orders.carid=@carid";
                    }
                    catch (Exception ex) { }
                }
                
                if (chkBnal.Checked)
                {
                    sql += " and orgid > 0";
                }
                if (chkUnclosedBnal.Checked)
                {
                    sql += " and orgid > 0 and bnalmoney = 0";
                }

                if (bnal_opl_tm.Checked)
                {

                    parameters.Add(new SqlParameter("bnal_opl_tm", bnal_opl_tm.Value.Date));
                    sql += " and CAST(FLOOR( CAST( bnal_opl_tm AS FLOAT ) )AS DATETIME)  = @bnal_opl_tm";
                }



                if (filterOrderService.SelectedValue != null)
                    try
                    {
                        int serviceid = (int)filterOrderService.SelectedValue;
                        parameters.Add(new SqlParameter("serviceid", serviceid));
                        sql += " and orders.serviceid=@serviceid";
                    }
                    catch (Exception ex) { }
                if (filterCarService.SelectedValue != null)
                    try
                    {
                        int carserviceid = (int)filterCarService.SelectedValue;
                        parameters.Add(new SqlParameter("carserviceid", carserviceid));
                        sql += " and cars1.serviceid=@carserviceid";
                    }
                    catch (Exception ex) { }





                sql += " and ((orders.phone like @phone) or ( orders.connectphone like @phone))";
                sql += " and addrFrom +' '+ houseFrom +'-'+flatFrom +'-'+addrTo like @address";
                //Result_code
                String result_code_sql = " and orders.state = 3 and ( (1=0)";

                if (Успешные.Checked)
                    result_code_sql += " or (orders.resultcode=1) ";

                if (Отмененные.Checked)
                    result_code_sql += " or (orders.resultcode<>1) ";
                //sql += " and orders.resultcode<>1 ";

                result_code_sql += ") ";

                sql += result_code_sql;

                //else
                //sql += " and orders.state = 3 and resultcode=1 ";

            }


            
        })));


                if (chkMyOrders.Checked)
                {
                    sql += " and OpID = " + MAIN_FORM.OperatorID.ToString() + " ";
                }

                if (chkFreeOrders.Checked)
                {
                    sql += " and CARID =0 ";
                }


                if (fFilterAddrPhone.Text != "")
                {
                    sql += " and ((orders.phone like @AddrFone) or ( orders.connectphone like @AddrFone) or (addrFrom +' '+ houseFrom +'-'+flatFrom +'-'+addrTo like @AddrFone))";

                    parameters.Add(new SqlParameter("AddrFone", "%" + fFilterAddrPhone.Text + "%"));
                }

                if (FFilterPos.Text != "")
                {
                    sql += " and ((orders.carid = @fFilterPos))";

                    parameters.Add(new SqlParameter("fFilterPos", FFilterPos.Text));
                }




                //" and orders.state <>3"+
                //" and orders.state <>5"+
                sql += " order by carid ";

                if (show_order_type == 1)  
                    System.Console.WriteLine(sql);

            }
            catch (Exception ex) { };


            DataTable dt = null;
            try
            {
                dt = DataBase.mssqlRead(sql, parameters.ToArray());
            }
            catch (Exception ex) { };





            
            //UpdateViewDataOrders(dt, view_work_orders);




//            int ms_start = DateTime.Now.Second * 1000 + DateTime.Now.Millisecond;
            System.Console.WriteLine(DateTime.Now+": UpdateListOrders Query. Время обработки " + (DateTime.Now.Second * 1000 + DateTime.Now.Millisecond - ms_start).ToString() + "мсек");
            
            return dt;
        }

        private void UpdateListRaions(DataTable dt, DataTable dt_new)
        {

            if (dt_new == null)
                return;
            if (dt_new.Rows.Count < 1)
                return;

            int ms_start = DateTime.Now.Second * 1000 + DateTime.Now.Millisecond;





            //[7 522 515 504][8 813][9 518 507][11 530 525][14 527][16 508 519]

            //dataSet1.Tables["ord"].Merge(dt);


            String distr = "";// dt.Rows[0]["DATA"].ToString();
            String distr_new = dt_new.Rows[0]["DATA"].ToString();

            

            //String [] separators = { "[", "][", "]" };

            //String[] spl1 = distr.Split(']');



            if (view_raions.DataSource == null)
            {
                DataTable zones = DataBase.mssqlRead("SELECT ID, DESCR FROM ZONES ORDER BY ORD");
                foreach (DataRow row in zones.Rows)
                {
                    String id = row[0].ToString();
                    String id_str = "[" + id + " ";
                    int idx = distr.IndexOf(id_str);

                    
                    int id_str_len = id_str.Length;
                    if (idx != -1) 
                    {

                        String ss = distr.Substring(idx + id_str_len, distr.IndexOf(']', idx) - idx - id_str_len);
                        String[] split = ss.Split(' ');

                        while (zones.Columns.Count - 2 < split.Length)
                        {
                            String column_name = (zones.Columns.Count - 2).ToString();
                            zones.Columns.Add(column_name);

                        }
                        for (int i = 0; i < split.Length; i++)
                        {
                            row[i + 2] = split[i];
                        }

                    }
                }
                view_raions.DataSource = zones;


                view_raions.Columns["ID"].Visible = false;
                view_raions.Columns[1].Width = 80;
                //view_raions.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                for (int i = 2; i < view_raions.Columns.Count; i++)
                    view_raions.Columns[i].Width = 30;

            }
            else
            {
                DataTable zones = (DataTable)view_raions.DataSource;

                car_zones.Clear();

                distr = distr + distr_new;


                foreach (DataRow row in zones.Rows)
                {
                    String id = row[0].ToString();
                    String id_str = "[" + id + " ";
                    int idx = distr.IndexOf(id_str);
                    int idx_new = -1;
                    int id_str_len = id_str.Length;
                    if (idx >=0)
                        idx_new = distr.IndexOf(id_str, idx + id_str_len);

                    
                    if (idx == -1)
                    {
                        //очистить строку - нет района в распределении
                        for (int i = 2; i < zones.Columns.Count; i++)
                            row[i] = "";
                    }
                    else
                    {

                        String ss = distr.Substring(idx + id_str_len, distr.IndexOf(']', idx) - idx - id_str_len);
                        String ss2 = "";
                        if (idx_new!= -1)
                            ss2 = distr.Substring(idx_new + id_str_len, distr.IndexOf(']', idx_new) - idx_new - id_str_len);
                        ss = ss +" "+ ss2;
                        String[] split = ss.Split(' ');

                        
                        

                        while (zones.Columns.Count - 2 < split.Length)
                        {
                            String column_name = (zones.Columns.Count - 2).ToString();
                            zones.Columns.Add(column_name);
                            view_raions.Columns[view_raions.Columns.Count - 1].Width = 50;
                        }
                        for (int i = 0; i < split.Length; i++)
                        {
                            row[i + 2] = split[i];
                            car_zones[split[i]] = id;
                        }
                        for (int i = split.Length + 2; i < zones.Columns.Count; i++)
                        {
                            row[i] = "";
                        }

                    }
                }

            }

            view_raions.Columns[1].MinimumWidth = view_raions.Columns[1].GetPreferredWidth(DataGridViewAutoSizeColumnMode.AllCells, false);
            view_raions.InvalidateColumn(1);

            return;


        }

        private DataTable UpdateListCars()
        {

            //сложный список. сортировка по статус + ID
            //в принципе - это и есть ID по которому будет ориентироваться процедура обновления таблицы. 
            //код машины === CARID

            int ms_start = DateTime.Now.Second * 1000 + DateTime.Now.Millisecond;


            String sql = "SELECT * FROM  GET_CARS_QUEUE()  ";
            DataTable dt =null;
            try
            {
                dt =  DataBase.mssqlRead(sql);
                /*
                            DataTable dt = DataBase.mssqlRead(""+
                            "select  -(cars.status&15)*1000 + cars.ID ID2, -(cars.status&15)*1000 + cars.ID ID, " +
            
                            "cars.id CARID, cast (cars.id as varchar(10))+' '+color +' '+mark +' ' + gosnum car_desc, "+
                "fio, phones, "+
                "cars.status car_status, "+
                " cars.serviceid, cars.status2 "+
                "from cars  "+
                "left join drivers on drivers.id = cars.driverid "+
                "where   cars.State&1=1  "+
                "and cars.status&1024 <> 1024  "+
                "order by 1");
                */
                lock (this.car_attributes)
                {
                    first_cars.Clear();
                    foreach (DataRow row in dt.Rows)
                    {
                        car_attributes[(int)row["CARID"]] = (int)row["SERVICEID"];

                        if ((row["status2"].ToString()) == "1")
                            first_cars[(int)row["CARID"]] = 0;
                    }
                }


            }
            catch (Exception ex) { };

            return dt;
        }


        private int _int_if_null(object value){
            if (value == DBNull.Value)
                return (int)0;
            else
                return (int)value;
        }

        private String _str_if_null(object value)
        {
            if (value == DBNull.Value)
                return "";
            else
                return (String)value;
        }
        private void UpdateViewDataOrders(DataTable dt, DataGridView view_orders)
        {

            if (dt == null)
                return;

            if (!dt.Columns.Contains("ID"))
                return;
            //if (dt.Rows.Count < 1)
                //return;

            Stopwatch sWatch = new Stopwatch();
            sWatch.Start();
            

            orders_table = dt.Copy();

            if (view_work_orders.DataSource == null)
            {
                DataTable dt1 = new DataTable();
                dt1.Columns.Add("ID");
                dt1.Columns.Add("ord_date");
                
                dt1.Columns["ord_date"].DataType = System.Type.GetType("System.DateTime");
                dt1.Columns.Add("TM_STATUS_DESC");
                dt1.Columns.Add("dtArrive");
                dt1.Columns.Add("dtBegin");
                dt1.Columns.Add("dtPredvar");
                dt1.Columns.Add("dtEnd");

                dt1.Columns.Add("VIP");
                dt1.Columns.Add("agreement");




                

                dt1.Columns.Add("ADDRFROM");
                dt1.Columns.Add("ADDRTO");
                dt1.Columns.Add("CARID");
                dt1.Columns.Add("CAR_NAME");
                dt1.Columns.Add("PHONE");
                dt1.Columns.Add("PHONE1");
                dt1.Columns.Add("PHONE2");
                dt1.Columns.Add("KLN");
                dt1.Columns.Add("clientname");
                dt1.Columns.Add("descr");
                dt1.Columns.Add("OPERATOR");
                dt1.Columns.Add("DIAL_STATUS");
                dt1.Columns.Add("SERVICE_NAME");
                dt1.Columns.Add("ORDER_STATE");
                dt1.Columns.Add("CAR_STATE");
                dt1.Columns.Add("CAR_STATUS");
                dt1.Columns.Add("TM_STATUS_CHANGE");
                dt1.Columns.Add("sum");
                dt1.Columns.Add("TalonNumber");
                dt1.Columns.Add("time2meet");
                dt1.Columns.Add("LAT");
                dt1.Columns.Add("LON");
                dt1.Columns.Add("SHORT_ADDRFROM");
                dt1.Columns.Add("ADDRFROM1");
                dt1.Columns.Add("HOUSEFROM1");
                dt1.Columns.Add("FLATFROM1");

                dt1.Columns.Add("sub_status");
                dt1.Columns.Add("distrib_last_try_tm");
                dt1.Columns.Add("distrib_status");
                dt1.Columns.Add("distrib_current_carid");
                
                dt1.Columns.Add("order_sum");
                dt1.Columns.Add("bank_sum");
                
                dt1.Columns.Add("car_speed");
                dt1.Columns.Add("car_lat");
                dt1.Columns.Add("car_lon");
                dt1.Columns.Add("ORGANIZATION_DETAIL_NAME");
                dt1.Columns.Add("INTERPOINTS");
                dt1.Columns.Add("taxometr_enabled");
                dt1.Columns.Add("tax_uslugi");
                dt1.Columns.Add("serviceid");
                dt1.Columns.Add("visible_distance");
                

                

                

                



                

                


                dt1.Columns["CARID"].DataType = System.Type.GetType("System.Int32");
                dt1.Columns["tax_uslugi"].DataType = System.Type.GetType("System.Int32");




                //   dt1.Merge(dt, false, MissingSchemaAction.Ignore);

                view_work_orders.DataSource = dt1;

                //view_work_orders.Columns["dt_arrive"].HeaderText = "Поступление";
                //view_work_orders.Columns["ADDRFROM"].Width = 160;


                view_work_orders.Columns["ADDRFROM"].HeaderText = "Откуда";

                #region Load grid settings

                foreach (DataGridViewColumn column in view_work_orders.Columns) {

                    String sValue = DataBase.settings.getUserSettingValue("VIEW_WORK_ORDER_COLUMN_WIDTH_" + column.DataPropertyName);
                    int iValue = 40;
                    int.TryParse(sValue, out iValue);
                    if (iValue != 40) {
                        column.Width = iValue;
                    }


                }



                #endregion

//                view_work_orders.Columns["ADDRFROM"].Width = 160;
                
                view_work_orders.Columns["order_sum"].HeaderText = "Цена по таксометру";
                view_work_orders.Columns["order_sum"].Width = 40;
                view_work_orders.Columns["bank_sum"].HeaderText = "Списано с баланса";
                view_work_orders.Columns["bank_sum"].Width = 40;
                view_work_orders.Columns["ADDRTO"].HeaderText = "Куда";
                view_work_orders.Columns["CARID"].HeaderText = "Машина";
//                view_work_orders.Columns["CARID"].Width = 140;
                view_work_orders.Columns["PHONE"].HeaderText = "Телефон";
                view_work_orders.Columns["KLN"].HeaderText = "Клиент";
//                view_work_orders.Columns["KLN"].Width = 60;
                view_work_orders.Columns["KLN"].Visible = false;
                view_work_orders.Columns["KLN"].Visible = false;
                view_work_orders.Columns["OPERATOR"].HeaderText = "Оператор";
//                view_work_orders.Columns["OPERATOR"].Width = 60;
                view_work_orders.Columns["DIAL_STATUS"].HeaderText = "Дозвон";
//                view_work_orders.Columns["DIAL_STATUS"].Width = 40;
                view_work_orders.Columns["clientname"].HeaderText = "Пассажир";
//                view_work_orders.Columns["clientname"].Width = 60;
                view_work_orders.Columns["descr"].HeaderText = "Примечание";
//                view_work_orders.Columns["descr"].Width = 60;
                view_work_orders.Columns["sum"].HeaderText = "Сумма";
//                view_work_orders.Columns["sum"].Width = 60;
                view_work_orders.Columns["TalonNumber"].HeaderText = "Талон";
                view_work_orders.Columns["dtArrive"].HeaderText = "Время с принятия";
//                view_work_orders.Columns["dtArrive"].Width = 40;
                view_work_orders.Columns["dtArrive"].Visible = false;
                view_work_orders.Columns["dtBegin"].HeaderText = "Время";
//                view_work_orders.Columns["dtBegin"].Width = 40;
                view_work_orders.Columns["dtBegin"].Visible = false;
                view_work_orders.Columns["dtPredvar"].HeaderText = "Предв";
///                view_work_orders.Columns["dtPredvar"].Width = 40;
                view_work_orders.Columns["dtPredvar"].Visible = false;
                view_work_orders.Columns["ord_date"].HeaderText = "Дата-время";
//                view_work_orders.Columns["ord_date"].Width = 40;
                view_work_orders.Columns["ord_date"].Visible = false;


                
                

                view_work_orders.Columns["TM_STATUS_DESC"].HeaderText = "Время";
//                view_work_orders.Columns["TM_STATUS_DESC"].Width = 40;
                view_work_orders.Columns["SERVICE_NAME"].HeaderText = "Служба";
                view_work_orders.Columns["serviceid"].Visible = false;

                
//                view_work_orders.Columns["SERVICE_NAME"].Width = 40;



                if ((MAIN_FORM.OperatorID == 129) || (MAIN_FORM.OperatorID == 123)) {
                    view_work_orders.Columns["ID"].Visible = true;
                }
                else
                    view_work_orders.Columns["ID"].Visible = false;
                
                view_work_orders.Columns["TM_STATUS_CHANGE"].Visible = false;
                //view_work_orders.Columns["TM_STATUS_DESC"].Visible;
                view_work_orders.Columns["CAR_NAME"].Visible = false;
                view_work_orders.Columns["dtEnd"].Visible = false;
                view_work_orders.Columns["ORDER_STATE"].Visible = false;
                view_work_orders.Columns["CAR_STATE"].Visible = false;
                view_work_orders.Columns["CAR_STATUS"].Visible = false;
                view_work_orders.Columns["time2meet"].Visible = false;
                view_work_orders.Columns["VIP"].Visible = false;
                view_work_orders.Columns["agreement"].Visible = false;
                view_work_orders.Columns["PHONE1"].Visible = false;
                view_work_orders.Columns["PHONE2"].Visible = false;
                view_work_orders.Columns["ADDRFROM1"].Visible = false;
                view_work_orders.Columns["HOUSEFROM1"].Visible = false;
                view_work_orders.Columns["FLATFROM1"].Visible = false;
                view_work_orders.Columns["LAT"].Visible = false;
                view_work_orders.Columns["LON"].Visible = false;
                view_work_orders.Columns["SHORT_ADDRFROM"].Visible = false;

                view_work_orders.Columns["sub_status"].Visible = false;
                view_work_orders.Columns["distrib_last_try_tm"].Visible = false;
                view_work_orders.Columns["distrib_status"].Visible = false;
                view_work_orders.Columns["distrib_current_carid"].Visible = false;
                view_work_orders.Columns["ORGANIZATION_DETAIL_NAME"].Visible = true;
                view_work_orders.Columns["taxometr_enabled"].Visible = false;
                view_work_orders.Columns["tax_uslugi"].Visible = false;
                


                

                //view_work_orders.Columns["car_speed"].Visible = false;
                //view_work_orders.Columns["car_lat"].Visible = false;
                //view_work_orders.Columns["car_lon"].Visible = false;

                


                
                









            }



            /*загрузить данные в таблицу.
            * по строчно - обновлять данные в текущей отображаемой таблице 
            * 
            * 3 прохода.
            * 1. удалить строки которые пропали
            * 2. обновить строки которые есть
            * 3. добавить новые строки
            */


            sWatch.Stop();
//            Console.WriteLine("UpdateViewDataOrders 1: " + sWatch.ElapsedMilliseconds.ToString());
            sWatch.Start();

            int n = 0;

            DataTable dt_view = (DataTable)view_orders.DataSource;
            DataColumnCollection columns = dt_view.Columns;
            for (int i = dt_view.Rows.Count - 1; i >= 0; i--)
            {
                DataRow view_row = dt_view.Rows[i];
                ///int id = int.Parse(view_row["ID"].ToString());
                //DataRow[] select_result = dt.Select("ID = " + id.ToString());
                DataRow[] select_result = dt.Select("ID = " + view_row["ID"].ToString());
                if (select_result.Length == 0)  //строки уже нет
                {
                    view_row.Delete();
                }

                if (select_result.Length == 1)  //такая строка есть - обновить данные
                {


                    bool need_changes = false;
                    foreach (DataColumn col in dt_view.Columns)
                    {
                        if (dt.Columns.Contains(col.ColumnName))
                        {
                            if (view_row[col].ToString().CompareTo(select_result[0][col.ColumnName].ToString()) != 0)
                                need_changes = true;
                        }

                    }
                    if (need_changes)
                        foreach (DataColumn col in dt_view.Columns)
                        {
                            if (dt.Columns.Contains(col.ColumnName))
                            {
                                // view_row.BeginEdit();
                                n++;
                                if (view_row[col].ToString().CompareTo(select_result[0][col.ColumnName].ToString()) != 0)
                                    view_row[col] = select_result[0][col.ColumnName];

                                if (col.ColumnName.CompareTo("CAR_NAME") == 0)
                                {
                                    //view_row[col] = select_result[0]["CAR_NAME"];

                                    if (view_row[col]!= DBNull.Value)
                                    {
                                            String car_speed = select_result[0]["car_speed"].ToString();
                                            
                                            if (car_speed != "")
                                                view_row[col] += car_speed;
                                        

                                    }


                                    int status = _int_if_null(select_result[0]["CAR_STATUS"]);
                                    //if (select_result[0]["CAR_STATUS"]!= DBNull.Value)
                                        //status = (int)select_result[0]["CAR_STATUS"];
                                    int car_lat=0, car_lon=0, order_lat=0, order_lon=0;

                                    car_lat = _int_if_null(select_result[0]["car_lat"]);
                                    car_lon = _int_if_null(select_result[0]["car_lon"]);
                                    order_lat = _int_if_null(select_result[0]["lat"]);
                                    order_lon = _int_if_null(select_result[0]["lon"]);

                                    if ((status & 15) != 4)
                                    {

/*
                                        int car_lat = 0;
                                        int.TryParse(view_work_orders.Rows[e.RowIndex].Cells[car_lat_idx].Value.ToString(), out car_lat);
                                        int car_lon = 0;
                                        int.TryParse(view_work_orders.Rows[e.RowIndex].Cells[car_lon_idx].Value.ToString(), out car_lon);
                                        int order_lat = 0;
                                        int.TryParse(view_work_orders.Rows[e.RowIndex].Cells[order_lat_idx].Value.ToString(), out order_lat);
                                        int order_lon = 0;
                                        int.TryParse(view_work_orders.Rows[e.RowIndex].Cells[order_lon_idx].Value.ToString(), out order_lon);
 */

                                        if ((car_lat > 0) && (car_lon > 0) && (order_lat > 0) && (order_lon > 0))
                                        {



                                            double clat = car_lat / 1000000.0;
                                            double clon = car_lon / 1000000.0;
                                            double olat = order_lat / 1000000.0;
                                            double olon = order_lon / 1000000.0;


                                            double distance = gisModule.Distance(clat, clon, olat, olon);








                                            view_row[col] += " | " + String.Format("{0:.0}км", distance) + " | ";
                                        }

                                    }


                                    int time2meet = _int_if_null(select_result[0]["time2meet"]);

                                    String t2meet = "";
                                    if (time2meet == -1)
                                        t2meet = "(ко врем)";
                                    if (time2meet == 1)
                                        t2meet = "(5 мин)";
                                    if (time2meet == 2)
                                        t2meet = "(10 мин)";
                                    if (time2meet == 3)
                                        t2meet = "(15 мин)";

                                    if (time2meet == 4)
                                        t2meet = "(3 мин)";
                                    if (time2meet == 5)
                                        t2meet = "(6 мин)";
                                    if (time2meet == 6)
                                        t2meet = "(9 мин)";


                                    view_row[col] +=  t2meet;
                                    try
                                    {
                                        if ((int)select_result[0]["CARID"] != 0)
                                            if (t2meet == "")
                                                view_row[col] = "***НЕТ ПОДТВ*** " + view_row[col];
                                    }
                                    catch (Exception exx)
                                    {
                                    }
  








                                    

                                    
                                }

                                //  view_row.EndEdit();
                            }
                        }


                    select_result[0].Delete();
                    //select_result[0].AcceptChanges();
                }
            }
            dt.AcceptChanges();

            sWatch.Stop();
            //Console.WriteLine("UpdateViewDataOrders 2: " + sWatch.ElapsedMilliseconds.ToString() + " n changes = "+n.ToString());
            sWatch.Start();

            foreach (DataRow new_row in dt.Rows)
            {


                DataRow r = dt_view.NewRow();
                //r.BeginEdit();
                foreach (DataColumn col in dt_view.Columns)
                {
                    if (dt.Columns.Contains(col.ColumnName))
                    {
                        r[col] = new_row[col.ColumnName];
                    }
                }
                //r.EndEdit();
                dt_view.Rows.InsertAt(r, 0);

            }





            tabOrders.Text = "Заказы (" + dt_view.Rows.Count.ToString() + ")";


            if (dt_view.Rows.Count < 30)
                if (chkMyOrders.Checked == false)
                    chkMyOrders.Enabled = false;

            if (dt_view.Rows.Count >= 30)
                chkMyOrders.Enabled = true;

            view_orders.InvalidateColumn(view_work_orders.Columns["TM_STATUS_DESC"].Index);

            sWatch.Stop();
            //Console.WriteLine("UpdateViewDataOrders 3: " + sWatch.ElapsedMilliseconds.ToString());
            //sWatch.Start();


            //view_work_orders.Refresh();
            view_work_orders.InvalidateColumn(11);

            
        }

        private void UpdateViewDataCars(DataTable dt, DataGridView view_cars) {

            if (dt == null)
                return;
            if (dt.Rows.Count < 1)
                return;

            /*загрузить данные в таблицу.
            * по строчно - обновлять данные в текущей отображаемой таблице 
            * 
            * 3 прохода.
            * 1. удалить строки которые пропали
            * 2. обновить строки которые есть
            * 3. добавить новые строки
             * 
             * для сортированного списка машин - так грузить нельзя.
             * полное обновление данных в таблице и позиционирование-прокрутка. 
             * 
             * либо - поиск нужного места для вставки - тоже можно в принципе.... и будет красивее-быстрее.
             * 
             * 
            */

            if (view_cars.DataSource == null)
            {
                DataTable dt1 = new DataTable();
                dt1.Columns.Add("ID2");
                dt1.Columns.Add("ID");
                dt1.Columns.Add("CARID");
                dt1.Columns.Add("FIO");
                dt1.Columns.Add("PHONES");

                dt1.Columns.Add("CAR_STATUS_DESC");
                dt1.Columns.Add("CAR_DESC");
                dt1.Columns.Add("CAR_STATUS");
                dt1.Columns.Add("STATUS2");





                dt1.Columns["ID"].DataType = System.Type.GetType("System.Int32");

                //   dt1.Merge(dt, false, MissingSchemaAction.Ignore);
                view_cars.DataSource = dt1;
                view_cars.Columns["ID"].HeaderText = "Машина";
                view_cars.Columns["ID"].Width = 150;
                view_cars.Columns["ID"].SortMode = DataGridViewColumnSortMode.NotSortable;
                view_cars.Columns["CARID"].HeaderText = "Машина";
                view_cars.Columns["CARID"].Width = 150;
                view_cars.Columns["CARID"].SortMode = DataGridViewColumnSortMode.NotSortable;

                view_cars.Columns["CAR_DESC"].HeaderText = "Машина";
                view_cars.Columns["FIO"].HeaderText = "Водитель";
                view_cars.Columns["FIO"].SortMode = DataGridViewColumnSortMode.NotSortable;
                view_cars.Columns["PHONES"].HeaderText = "Телефон";
                view_cars.Columns["PHONES"].Width = 120;
                view_cars.Columns["PHONES"].SortMode = DataGridViewColumnSortMode.NotSortable;
                view_cars.Columns["CAR_STATUS_DESC"].HeaderText = "Статус";
                view_cars.Columns["CAR_STATUS_DESC"].SortMode = DataGridViewColumnSortMode.NotSortable;
                view_cars.Columns["CAR_DESC"].Visible = false;
                view_cars.Columns["STATUS2"].Visible = false;

                view_cars.Columns["ID2"].Visible = false;
                view_cars.Columns["CARID"].Visible = false;

                //view_cars.Columns["ADDRTO"].HeaderText = "Куда";
            }

            DataTable dt_view = (DataTable)view_cars.DataSource;
            DataColumnCollection columns = dt_view.Columns;
            for (int i = dt_view.Rows.Count - 1; i >= 0; i--) {
                DataRow view_row = dt_view.Rows[i];
                int id = int.Parse(view_row["ID"].ToString());
                DataRow[] select_result = dt.Select("ID = " + id.ToString());
                if (select_result.Length == 0)  //строки уже нет
                {
                    view_row.Delete();
                }

                if (select_result.Length == 1)  //такая строка есть - обновить данные
                {


                    bool need_changes = false;
                    foreach (DataColumn col in dt_view.Columns) {
                        if (dt.Columns.Contains(col.ColumnName)) {
                            if (view_row[col].ToString().CompareTo(select_result[0][col.ColumnName].ToString()) != 0)
                                need_changes = true;
                        }

                    }
                    if (need_changes)
                        foreach (DataColumn col in dt_view.Columns) {
                            if (dt.Columns.Contains(col.ColumnName)) {
                                // view_row.BeginEdit();
                                view_row[col] = select_result[0][col.ColumnName];
                                //  view_row.EndEdit();
                            }
                        }


                    select_result[0].Delete();
                    //select_result[0].AcceptChanges();
                }
            }
            dt.AcceptChanges();
            //осталось несколько строк - их надо вставить в порядке сортировки.
            //для каждой строки искать строку с меньшим кодом и вставлять сразу после нее...
            foreach (DataRow new_row in dt.Rows) {

                int new_row_id = (int)new_row["ID"];
                int pos = 0;
                for (pos = 0; pos < dt_view.Rows.Count; pos++) {
                    if (new_row_id < (int)dt_view.Rows[pos]["ID"])
                        break;
                }

                DataRow r = dt_view.NewRow();
                //r.BeginEdit();
                foreach (DataColumn col in dt_view.Columns) {
                    if (dt.Columns.Contains(col.ColumnName)) {
                        r[col] = new_row[col.ColumnName];
                    }
                }
                //r.EndEdit();
                
                dt_view.Rows.InsertAt(r, pos);


            }

            //UpdateViewDataCars(dt, view_cars);

            tabCars.Text = "Машины (" + view_cars.Rows.Count.ToString() + ")";
        }





        [Conditional("TRACE")]
        private void LogCommand()
        {
            //System.Console.WriteLine("MY TRACE");
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }


        private void dataGridView1_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (!e.RowIndex.Equals(-1) && !e.ColumnIndex.Equals(-1)) {
                if (e.Button.Equals(MouseButtons.Right)) {
                    view_work_orders.CurrentCell = view_work_orders[e.ColumnIndex, e.RowIndex];
                    view_work_orders.CurrentRow.Selected = true;
                }
            } else {
                //view_work_orders.CurrentCell = null;
                view_work_orders.ClearSelection();
            }

//            if (!e.RowIndex.Equals(-1) && !e.ColumnIndex.Equals(-1) && e.Button.Equals(MouseButtons.Right))
            {
                
            }

        }

        private void button13_Click(object sender, EventArgs e)
        {



        }

        private void button14_Click(object sender, EventArgs e)
        {
            //init
            // local ip
            //remote ip
            //local sip port
            // sip user id
            //sip user password

           


        }



        private void btnCallDial_Click(object sender, EventArgs e)
        {
            caller.doDial("1200");
        }

        private void callBtnDial_Click(object sender, EventArgs e)
        {
            if (caller == null)
                return;
            sip_status_combo.SelectedIndex = 1;

             long carid = 0;
             long.TryParse(callPhone.Text, out carid);

            if (carid < 5000)
            {
                    doDialCar((int)carid);
            }
            else
                caller.doDial(callPhone.Text);


        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (caller!=null)
                caller.SipClose();
        }

        private void callBtnHangup_Click(object sender, EventArgs e)
        {
            if (caller == null)
                return;

            caller.doHangup();
            callPhone.Text = "";
            //caller.doHangup();
            //doStartPostCallTimer();

            if (current_call_line != -1)
            {

            }
        }

      

       
        private void button2_Click_1(object sender, EventArgs e)
        {
            //caller.doShowSettings();
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            this.miOrderEdit_Click(null, null);
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            time_label.Text = System.DateTime.Now.ToLongTimeString();

            if (this.sip_status_combo.SelectedIndex == 0)
            {
                if (last_call_action != DateTime.MinValue)
                {
                    int delta = (int)(DateTime.Now - last_call_action).Duration().TotalSeconds;
                    label_pwo_timer.Text = delta.ToString();
                    //this.sip_status_combo.Items[0] = "На смене (" + delta.ToString()+ ")";
                    if (delta > 300) {
                        //SipCaller.m_pPlayer.Play(ResManager.GetStream("Windows XP - свертывание.wav"), 1);
                        this.sip_status_combo.SelectedIndex = 2;
                    }
                }
            }

            if (MouseButtons != MouseButtons.None)
                return;

            if (mnuViewWorkOrders.Visible)
                return;

            //проверить, нажата ли кнопка мыши или активно ли всполывающее окно

            //System.Console.WriteLine(System.Windows.Input.Mouse.LeftButton.ToString());

            //if (System.Windows.Input.Mouse.LeftButton == System.Windows.Input.MouseButtonState.Pressed)
            {

                //if (DataBase.mssqlGetConnection().State != ConnectionState.Open ) {
                //                    timer2.Enabled = false;
                //                    return;
                //                }
                view_work_orders.Font = MAIN_FORM.table_font;
                

                if (!backgroundWorker1.IsBusy)
                    this.backgroundWorker1.RunWorkerAsync();
                if (!backgroundWorker2.IsBusy)
                    this.backgroundWorker2.RunWorkerAsync();
                if (!backgroundWorker3.IsBusy)
                    this.backgroundWorker3.RunWorkerAsync();


                if (show_order_type != 0)
                {
                    timer2.Enabled = false;
                }

                /*
                if (statusbar.Text.StartsWith("Соединение установлено"))
                {
                    statusbar.BackColor = Color.Red;
                }
                else
                    statusbar.BackColor = Color.FromName("ButtonFace");
                 */ 
                    //statusbar.BackColor = Color.FromName("ActiveCaption");
                    


                //                UpdateListOrders();
                //                UpdateListRaions();
                //UpdateListCars();
            }
        }

        private void view_work_orders_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            //разцветка таблицы
            //состояние заказа //
            //откуда, телефон,клиент, оператор - белый, рыжий, красный
            //машины - белая, фиолетовая, блеклая
            //дозвон - белый, голубой, красный


            //ПОТОРОПИТЕ - sub_status=1


            //
            /*
             * 9	256		      100 000 000  	на линии в районе
104	772		    1 100 000 100  	на заказе
425	768		    1 100 000 000  	на линии но не в районе
1	1280	   10 100 000 000  	не на линии
            
             * 
 * Биты (0-4) (число)
0*: 'свободен' белая
2*: 'вышел на 5 минут' 
3 - посадка по городу.
4*: 'занят, взял пассажира по заказу'
5: 'освободился, довез пассажира'
6: 'освободился, пассажир отказался от поездки'
7*: 'занят, ожидаю клиента на месте встречи' фиолетовая
8: 'занят, заказ взял, приступаю к выполнению'

Бит 6 – Тревога   
Бит 7 – Позвоните желтый
Бит 8 – Установлен GPS - красный GPS
Бит 9 – GPS дает корректные данные синий GPS             
Бит 10 0 - на линии, 1 - отключен
             * 
             
             
             
             */




            int order_service_id = 0;
            int.TryParse(view_work_orders.Rows[e.RowIndex].Cells["serviceid"].Value.ToString(), out order_service_id);
            int id = int.Parse(view_work_orders.Rows[e.RowIndex].Cells[0].Value.ToString());
            String vip = view_work_orders.Rows[e.RowIndex].Cells["VIP"].Value.ToString();
            String agreement = view_work_orders.Rows[e.RowIndex].Cells["agreement"].Value.ToString();

            //int order_sub_status_column_idx = view_work_orders.Columns["sub_status"].Index;
            int order_sub_status = 0;
            if (view_work_orders.Rows[e.RowIndex].Cells["sub_status"].Value != DBNull.Value) {
                order_sub_status = int.Parse(view_work_orders.Rows[e.RowIndex].Cells["sub_status"].Value.ToString());
            }



            if (view_work_orders.Columns[e.ColumnIndex].Name == "PHONE") {
                if ((vip == "1") || (agreement == "1"))
                    e.CellStyle.Font = MAIN_FORM.table_bold_font;
                
            }

            if (view_work_orders.Columns[e.ColumnIndex].Name == "CARID")
            {
                e.Value = "";
                if (order_service_id == 3)
                {
                    e.Value = "Из интернета, проверьте заказ и смените службу";
                    e.CellStyle.BackColor = Color.LightSalmon;
                }
                /// Если водитель взял заказ но заказ провисел 10 минут - надо позвонить клиенту, спросить ждет ли 
                /// Если водитель взял заказ не поставил дозвон в течении 5 минут после взятия заказа - водитель не вызывает пассажира
                /// Если водитель взял заказ не поставил дозвон в течении 5 минут до предварительного заказа - водитель не вызывает пассажира


                e.Value += view_work_orders.Rows[e.RowIndex].Cells["CAR_NAME"].Value.ToString();

                

            }

            if ((view_work_orders.Columns[e.ColumnIndex].Name == "ADDRFROM")
                || (view_work_orders.Columns[e.ColumnIndex].Name == "PHONE")
                || (view_work_orders.Columns[e.ColumnIndex].Name == "ADDRTO")
                || (view_work_orders.Columns[e.ColumnIndex].Name == "OPERATOR")
                
                )
            {
                int state_column_idx = view_work_orders.Columns["ORDER_STATE"].Index;
                int state = int.Parse(view_work_orders.Rows[e.RowIndex].Cells[state_column_idx].Value.ToString());
                if (state == 2)
                {
                    e.CellStyle.BackColor = MAIN_FORM.color_1;
                }


            }

            if (view_work_orders.Columns[e.ColumnIndex].Name == "CAR_STATUS")
            {
                int car_status_column_idx = view_work_orders.Columns["CAR_STATUS"].Index;
                if (view_work_orders.Rows[e.RowIndex].Cells[car_status_column_idx].Value != DBNull.Value)
                {
                    int status = int.Parse(view_work_orders.Rows[e.RowIndex].Cells[car_status_column_idx].Value.ToString());
                    /*
                     * 
                     * 0 или 1  - фиолетовый - не вижу клиента
                     * 10 - вылетел с линии, обрыв связи
                     */

                    int low_status = status & 15;
                    
                    String bb = Convert.ToString(status, 2);
                    int bbLen = bb.Length;
                    bb = ("0000000000000000" + bb).Substring(bbLen);

                    e.CellStyle.BackColor = Color.LightGreen;
                    e.Value+=' '+ bb;
                    if (low_status == 0)
                    {
                        e.CellStyle.BackColor = MAIN_FORM.color_1;
                        
                    }
                    if (low_status == 4)
                    {
                        //e.CellStyle.BackColor = MAIN_FORM.color_2; 
                        e.CellStyle.BackColor = Color.White;

                    }
                    if (low_status == 7)
                    {
                        e.CellStyle.BackColor = Color.Coral;
                        
                    }

                    if (low_status == 8) {
                        e.CellStyle.BackColor = MAIN_FORM.color_3;

                    }

                    if (bb[15 - 6] == '1')
                        e.Value += "T";
                    if (bb[15 - 7] == '1')
                        e.Value += "C";
                    String g = "";
                    if (bb[15 - 8] == '1')
                        g = "g";
                    if (bb[15 - 9] == '1')
                        g = "G";


                    if (bb[15 - 6] == '1')
                    {
                        g = " !!  ТРЕВОГА !! ";
                        e.CellStyle.BackColor = Color.Red;
                    }

                    if (bb[15 - 7] == '1')
                    {
                        g = " >>ПОЗВОНИТЕ<< ";
                        e.CellStyle.BackColor = Color.Yellow;
                    }

                    if (order_sub_status == 1) {
                        e.CellStyle.BackColor = Color.Aquamarine;
                    }



                    e.Value += g;
                }

                
                /*
                 * * Биты (0-4) (число)
                0*: 'свободен' белая
                2*: 'вышел на 5 минут' 
                4*: 'занят, взял пассажира по заказу'
                5: 'освободился, довез пассажира'
                6: 'освободился, пассажир отказался от поездки'
                7*: 'занят, ожидаю клиента на месте встречи' фиолетовая
                8: 'занят, заказ взял, приступаю к выполнению'

                Бит 6 – Тревога   
                Бит 7 – Позвоните желтый
                Бит 8 – Установлен GPS - красный GPS
                Бит 9 – GPS дает корректные данные синий GPS    
                */

            }

            

                

            if (view_work_orders.Columns[e.ColumnIndex].Name == "CARID") {


                int car_status_column_idx1 = view_work_orders.Columns["CAR_STATUS"].Index;
                int car_distrib_column_idx1 = view_work_orders.Columns["distrib_current_carid"].Index;
                int time2meet_idx = view_work_orders.Columns["time2meet"].Index;
                int car_speed_idx = view_work_orders.Columns["car_speed"].Index;
                int car_lat_idx = view_work_orders.Columns["car_lat"].Index;
                int car_lon_idx = view_work_orders.Columns["car_lon"].Index;

                int order_lat_idx = view_work_orders.Columns["lat"].Index;
                int order_lon_idx = view_work_orders.Columns["lon"].Index;

                int taxometr_enabled_idx = view_work_orders.Columns["taxometr_enabled"].Index;

                int status = 0;

                if (view_work_orders.Rows[e.RowIndex].Cells[car_status_column_idx1].Value != DBNull.Value) {
                    status = int.Parse(view_work_orders.Rows[e.RowIndex].Cells[car_status_column_idx1].Value.ToString());
                }


                //111
                /*перенесено
                if (e.Value == DBNull.Value) {
                    if (view_work_orders.Rows[e.RowIndex].Cells[car_distrib_column_idx1].Value != DBNull.Value) {
                        e.Value = view_work_orders.Rows[e.RowIndex].Cells[car_distrib_column_idx1].Value;
                    }
                }

                String car_speed = view_work_orders.Rows[e.RowIndex].Cells[car_speed_idx].Value.ToString();
                e.Value += car_speed;
                ///-1111

                 */ 
                /* перенесено
                if ((status & 15) != 4) {


                    int car_lat = 0;
                    int.TryParse(view_work_orders.Rows[e.RowIndex].Cells[car_lat_idx].Value.ToString(), out car_lat);
                    int car_lon = 0;
                    int.TryParse(view_work_orders.Rows[e.RowIndex].Cells[car_lon_idx].Value.ToString(), out car_lon);
                    int order_lat = 0;
                    int.TryParse(view_work_orders.Rows[e.RowIndex].Cells[order_lat_idx].Value.ToString(), out order_lat);
                    int order_lon = 0;
                    int.TryParse(view_work_orders.Rows[e.RowIndex].Cells[order_lon_idx].Value.ToString(), out order_lon);

                    if ((car_lat > 0) && (car_lon > 0) && (order_lat > 0) && (order_lon > 0)) {



                        double clat = car_lat / 1000000.0;
                        double clon = car_lon / 1000000.0;
                        double olat = order_lat / 1000000.0;
                        double olon = order_lon / 1000000.0;


                        double distance = gisModule.Distance(clat, clon, olat, olon);



                        


                        

                        e.Value += " | " + String.Format("{0:.0}км", distance) + " | ";
                    }

                }
                */
                /* перенесено
                int time2meet = int.Parse(view_work_orders.Rows[e.RowIndex].Cells[time2meet_idx].Value.ToString());
                
                String t2meet = "";
                if (time2meet == 1)
                    t2meet = "(5 мин)";
                if (time2meet == 2)
                    t2meet = "(10 мин)";
                if (time2meet == 3)
                    t2meet = "(15 мин)";

                if (time2meet == 4)
                    t2meet = "(3 мин)";
                if (time2meet == 5)
                    t2meet = "(6 мин)";
                if (time2meet == 6)
                    t2meet = "(9 мин)";


                e.Value += t2meet;

                */

                if (view_work_orders.Rows[e.RowIndex].Cells[car_status_column_idx1].Value != DBNull.Value) 
                {


                    if (view_work_orders.Rows[e.RowIndex].Cells[taxometr_enabled_idx].Value.ToString().CompareTo("0")==0)
                        e.Value += "*";
                    else
                        e.Value += "_";

                   // int status = int.Parse(view_work_orders.Rows[e.RowIndex].Cells[car_status_column_idx1].Value.ToString());

/*                    int time2meet = 0;
                if (view_work_orders.Rows[e.RowIndex].Cells[time2meet_idx].Value != DBNull.Value) 
                    time2meet = int.Parse(view_work_orders.Rows[e.RowIndex].Cells[time2meet_idx].Value.ToString());

                    String t2meet = "";
                    if (time2meet == 1)
                        t2meet = "(5 мин)";
                    if (time2meet == 2)
                        t2meet = "(10 мин)";
                    if (time2meet == 3)
                        t2meet = "(15 мин)";
 */ 
                    /*
                     * 
                     * 0 или 1  - фиолетовый - не вижу клиента
                     * 10 - вылетел с линии, обрыв связи
                     */

                    int low_status = status & 15;

                    String bb = Convert.ToString(status, 2);
                    int bbLen = bb.Length;
                    bb = ("0000000000000000" + bb).Substring(bbLen);

                    e.CellStyle.BackColor = Color.LightGreen;

                    if (low_status == 0) {
                        e.CellStyle.BackColor = MAIN_FORM.color_1;

                    }
                    if (low_status == 4) {
                        e.CellStyle.BackColor = Color.White;// MAIN_FORM.color_1;

                    }
                    if (low_status == 7) {
                        e.CellStyle.BackColor = MAIN_FORM.color_2;

                    }
/*
                    e.Value += t2meet;
*/


//                    if (bb[15 - 6] == '1')
//                        e.Value += "T";
                    if (bb[15 - 7] == '1')
                        e.Value += "C";
                    String g = "";
                    if (bb[15 - 8] == '1')
                        g = "g";
                    if (bb[15 - 9] == '1')
                        g = "G";

                    if (bb[15 - 6] == '1')
                    {
                        g = " !!  ТРЕВОГА !! ";
                        e.CellStyle.BackColor = Color.Red;
                    }

                    if (bb[15 - 7] == '1')
                    {
                        g = " >>ПОЗВОНИТЕ<< ";
                        e.CellStyle.BackColor = Color.Yellow;
                    }

                    if (order_sub_status == 1) {
                        e.CellStyle.BackColor = Color.Magenta;
                    }

                    e.Value += g;
                }
            }

            if (view_work_orders.Columns[e.ColumnIndex].Name == "DIAL_STATUS")
            {
                int dial_status_column_idx = view_work_orders.Columns["DIAL_STATUS"].Index;
                if (view_work_orders.Rows[e.RowIndex].Cells[dial_status_column_idx].Value != DBNull.Value)
                {
                    String status = view_work_orders.Rows[e.RowIndex].Cells[dial_status_column_idx].Value.ToString();
                    if (status[0]=='f')
                        e.CellStyle.BackColor = Color.Red;
                    if (status[0] == 'd')
                        e.CellStyle.BackColor = Color.LightBlue;
                    String s = ""+status[1];  ///status[1] == кол-во дозвонов [status[3] == колво автодозвонов
                    int dial_status = -1;
                    int.TryParse(s, out dial_status);

                    if (  dial_status >2 )
                        e.CellStyle.BackColor = Color.LightBlue;
                    
                    e.Value = status.Substring(1);

                }


            }
            if (view_work_orders.Columns[e.ColumnIndex].Name == "TM_STATUS_DESC")
            {
            
                int dtArrive_column_idx = view_work_orders.Columns["dtArrive"].Index;
                int dtBegin_column_idx = view_work_orders.Columns["dtBegin"].Index;
                int dtPredvar_column_idx = view_work_orders.Columns["dtPredvar"].Index;
                int ord_status_column_idx = view_work_orders.Columns["ORDER_STATE"].Index;
                int car_status_column_idx = view_work_orders.Columns["CAR_STATUS"].Index;
                int car_column_idx = view_work_orders.Columns["CARID"].Index;

                int status = int.Parse("0" + view_work_orders.Rows[e.RowIndex].Cells[car_status_column_idx].Value.ToString());
                /*
                 * 
                 * 0 или 1  - фиолетовый - не вижу клиента
                 * 10 - вылетел с линии, обрыв связи
                 */

                int low_status = status & 15;


                int ord_state = int.Parse("0"+view_work_orders.Rows[e.RowIndex].Cells[ord_status_column_idx].Value.ToString());
                if (ord_state == 0) {   //заказ новый - вывести время с поступления
                    e.Value = view_work_orders.Rows[e.RowIndex].Cells[dtArrive_column_idx].Value;
                }

                if (view_work_orders.Rows[e.RowIndex].Cells[dtPredvar_column_idx].Value.ToString() != "") {   //заказ предварительный  -  время до посадки
                    //e.Value = "Пр." + view_work_orders.Rows[e.RowIndex].Cells[dtPredvar_column_idx].Value;
                    e.Value = view_work_orders.Rows[e.RowIndex].Cells[dtPredvar_column_idx].Value;
                    e.CellStyle.BackColor = Color.Yellow;
                } else {
                    if (ord_state == 0) {   //заказ новый - если поступил больше 5 минут назад - сделать оранж
                        String tm = (String)(view_work_orders.Rows[e.RowIndex].Cells[dtArrive_column_idx].Value);
                        int minute = int.Parse(tm.Split(':')[0]);
                        if (minute > 2)
                            e.CellStyle.BackColor = Color.PaleTurquoise;
                        if (minute > 5)
                            e.CellStyle.BackColor = Color.Tomato;
                        if (minute > 10)
                            e.CellStyle.BackColor = Color.Red;
                    }

                }

                //if ((view_work_orders.Rows[e.RowIndex].Cells[car_column_idx].Value.ToString() != "") || (view_work_orders.Rows[e.RowIndex].Cells[car_column_idx].Value.ToString().CompareTo("0")==0))
                if (view_work_orders.Rows[e.RowIndex].Cells[dtPredvar_column_idx].Value.ToString() == "") //только непредварительные заказы
                if  ((view_work_orders.Rows[e.RowIndex].Cells[car_column_idx].Value.ToString() != "") && (view_work_orders.Rows[e.RowIndex].Cells[car_column_idx].Value.ToString().CompareTo("0") != 0))
                {
                //if (low_status == 7) {   //заказ с машиной - выводить время по статусу машины
                    //e.Value = "*"+view_work_orders.Rows[e.RowIndex].Cells[dtBegin_column_idx].Value;
                    e.Value = view_work_orders.Rows[e.RowIndex].Cells[dtBegin_column_idx].Value;
                }

                


                //вывести строку времени - в зависимости от статуса новый (предварительный) - приглашается - отзвонился.

                /*

                if (view_work_orders.Rows[e.RowIndex].Cells[dial_status_column_idx].Value != DBNull.Value)
                {
                    String status = view_work_orders.Rows[e.RowIndex].Cells[dial_status_column_idx].Value.ToString();
                    if (status[0]=='f')
                        e.CellStyle.BackColor = Color.Red;
                    if (status[0] == 'd')
                        e.CellStyle.BackColor = Color.LightBlue;
                    
                    e.Value = status[1];

                }
                */


            }

            
        }

        private void button15_Click(object sender, EventArgs e)
        {
            
        }

        private void view_work_orders_SortCompare(object sender, DataGridViewSortCompareEventArgs e)
        {
           
        }

        private void view_cars_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            /*
             * 9	256		      100 000 000  	на линии в районе
104	772		    1 100 000 100  	на заказе
425	768		    1 100 000 000  	на линии но не в районе
1	1280	   10 100 000 000  	не на линии
            
             * 
 * Биты (0-4) (число)
0*: 'свободен' белая
2*: 'вышел на 5 минут' 
4*: 'занят, взял пассажира по заказу'
5: 'освободился, довез пассажира'
6: 'освободился, пассажир отказался от поездки'
7*: 'занят, ожидаю клиента на месте встречи' фиолетовая
8: 'занят, заказ взял, приступаю к выполнению'

Бит 6 – Тревога   
Бит 7 – Позвоните желтый
Бит 8 – Установлен GPS - красный GPS
Бит 9 – GPS дает корректные данные синий GPS             
Бит 10 0 - на линии, 1 - отключен
             * 
             
             * 
             *  dt1.Columns.Add("ID");
                dt1.Columns.Add("CAR_DESC");
                dt1.Columns.Add("FIO");
                dt1.Columns.Add("PHONES");
                dt1.Columns.Add("CAR_STATUS");
                dt1.Columns.Add("CAR_STATUS_DESC");
             
             
             */

            //для каждой ячейки - разбор статуса..... лишняя работа но по другому - потом оптимизировать

             int car_status_column_idx = view_cars.Columns["CAR_STATUS"].Index;
             int car_status2_column_idx = view_cars.Columns["STATUS2"].Index;

            int status = 0;
            if (view_cars.Rows[e.RowIndex].Cells[car_status_column_idx].Value != DBNull.Value)
            {
                status = int.Parse(view_cars.Rows[e.RowIndex].Cells[car_status_column_idx].Value.ToString());
                /*
                 * 
                 * 0 или 1  - фиолетовый - не вижу клиента
                 * 10 - вылетел с линии, обрыв связи
                 */
            }

            int status2 = 0;
            if (view_cars.Rows[e.RowIndex].Cells[car_status2_column_idx].Value != DBNull.Value)
            {
                status2 = int.Parse(view_cars.Rows[e.RowIndex].Cells[car_status2_column_idx].Value.ToString());
                /*
                 * 
                 * 0 или 1  - фиолетовый - не вижу клиента
                 * 10 - вылетел с линии, обрыв связи
                 */
            }

                int low_status = status & 15;
                
                String bb = Convert.ToString(status, 2);
                int bbLen = bb.Length;
                bb = ("0000000000000000" + bb).Substring(bbLen);
                //цвет по умолчанию
                e.CellStyle.BackColor = Color.LightGreen;


               
                if (low_status == 0)
                {
                    e.CellStyle.BackColor = Color.White;
                    
                }

                if (status2 == 1)
                {
                    e.CellStyle.ForeColor = Color.Red ;
                    
                }

                if (low_status == 3) {
                    e.CellStyle.BackColor = Color.Coral;

                }
                if (low_status == 4)
                {
                    e.CellStyle.BackColor = MAIN_FORM.color_1;

                }



            

                if (low_status == 7)
                {
                    e.CellStyle.BackColor = MAIN_FORM.color_2;
                    
                }
                if (low_status == 8) {
                    e.CellStyle.BackColor = Color.Cyan;

                }
                String g = "";
                if (bb[15 - 8] == '1')
                    g = "g";
                if (bb[15 - 9] == '1')
                    g = "G";
                e.Value += g;

                if (bb[15 - 6] == '1')
                {
                    g = " !!  ТРЕВОГА !! ";
                    e.CellStyle.BackColor = Color.Red;
                }

                if (bb[15 - 7] == '1')
                {
                    g = " >>ПОЗВОНИТЕ<< ";
                    e.CellStyle.BackColor = Color.Yellow;
                }

             
            



     //       int id = int.Parse(view_cars.Rows[e.RowIndex].Cells[0].Value.ToString());


            if (view_cars.Columns[e.ColumnIndex].Name == "ID")
            {
                e.Value = view_cars.Rows[e.RowIndex].Cells["CAR_DESC"].Value + g;
            }

            if (view_cars.Columns[e.ColumnIndex].Name == "CAR_STATUS_DESC")
            {
                if (low_status == 0)
                {
                    e.Value = "Свободен";

                }
                if (low_status == 3) {
                    e.Value = "Рука";
                }
                if (low_status == 4)
                {
                    e.Value = "Выполняет заказ";

                }
                if (low_status == 7)
                {
                    e.Value = "Вызывает клиента";

                }
                if (low_status == 8) {
                    e.Value = "Получил заказ";

                }
                
            }

           
           
        }

        private void mnuViewWorkOrders_Opening(object sender, CancelEventArgs e)
        {
            if (view_work_orders.SelectedRows.Count != 1) {
                e.Cancel = true;
            } else {


                String carid = (view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["CARID"].Value.ToString());
                mnuDialCar.Enabled = (carid != "");
                miEndOrder1.Enabled = (carid != "");
            }
            
            

        }

        private void miOrderEdit_Click(object sender, EventArgs e)
        {
            int ms_start = DateTime.Now.Second * 1000 + DateTime.Now.Millisecond;
            if (view_work_orders.SelectedRows.Count != 1)
                return;

            if (view_work_orders.SelectedRows[0].Index >= 0)
            {

                int id = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ID"].Value.ToString()));

                if (settings.UseNewOrderForm) {
                    order_big o = new order_big();
                    o.doLoad(id, "", "", "");
                    o.Show();
                } 
            }
        }

        private void miOrderSetCar_Click(object sender, EventArgs e)
        {
            

            
        }

        private void sip_status_combo_SelectedIndexChanged(object sender, EventArgs e)
        {
            MAIN_FORM.SIP_STATUS = sip_status_combo.SelectedIndex;
            skipped_calls.Clear();

            if (sip_status_combo.SelectedIndex == 0)
            {
                if (caller == null)
                {
                    caller = new caller();
                    caller.Show();
                    caller.Close();
                    caller.initSip(form_settings.SipLocalIpAddress, form_settings.SipPort, form_settings.SipServer, form_settings.SipUsername, form_settings.SipPassword);

                }
            }

            /*
            if (caller.isInitComplete == false) {
                caller.InitCaller();
                caller.registerCaller();
                caller.isInitComplete = true;
            }
             */

            //caller.main_sip_status = sip_status_combo.SelectedIndex;
            if (sip_status_combo.SelectedIndex == 3)
            {
                
            }
            if (sip_status_combo.SelectedIndex == 0)
            {
                last_call_action = DateTime.MinValue;
                label_pwo_timer.Text = "";
            }
        }

        private void miNewOrder_Click(object sender, EventArgs e)
        {

            if (settings.UseNewOrderForm) {
                order_big o = new order_big();
                o.doLoad(-1, "", "", "");
                o.Show();
            } 
        }

        private void post_order_timer_Tick(object sender, EventArgs e) {
            if (sip_status_combo.SelectedIndex == 1)
                sip_status_combo.SelectedIndex = 0;
            post_order_timer.Enabled = false;
        }

        public void doStartPostCallTimer() {
            //if (caller.hasActiveCall())
            if (caller != null)
            {
                doBlockIncomingCalls();
                this.post_order_timer.Enabled = true;
            }
        }

        public void doBlockIncomingCalls() {
            sip_status_combo.SelectedIndex = 1;
            
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e) {
            ////при открытии контекстного меню карты
            //1. определить над каким регионом оно нажато - это просто взять регион из объукта карты
            редактироватьРайонToolStripMenuItem.Visible = false;
            for (int i = 0; i < MapControl.ZoneRegion.list_zone_region.Count; i++)
            {
                if (MapControl.ZoneRegion.list_zone_region[i].hightLighted)
                {
                    //сделать активным пункт меню : изменить район
                    редактироватьРайонToolStripMenuItem.Visible = true;
                    редактироватьРайонToolStripMenuItem.Tag = MapControl.ZoneRegion.list_zone_region[i];
                }
            }
            
            //MapControl.ZoneRegion.

        }

        private void view_cars_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (!e.RowIndex.Equals(-1) && !e.ColumnIndex.Equals(-1) && e.Button.Equals(MouseButtons.Right)) {

                if (view_cars.SelectedRows.Count > 1)
                {
                    if (view_cars.SelectedRows.Contains(view_cars.Rows[e.RowIndex]))
                        return;
                }
                
                view_cars.CurrentCell = view_cars[e.ColumnIndex, e.RowIndex];
                view_cars.CurrentRow.Selected = true;
            }
        }

        private void mnuDialCar_Click(object sender, EventArgs e) {
            if (view_work_orders.SelectedRows.Count != 1)
                return;

            if (view_work_orders.SelectedRows[0].Index >= 0)
            {

                int carid = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["CARID"].Value.ToString()));
                doDialCar(carid);
                
            }
            
            
        }

        private void mnuDialClient_Click(object sender, EventArgs e) {
            if (view_work_orders.SelectedRows.Count != 1)
                return;

            if (view_work_orders.SelectedRows[0].Index >= 0) {

                String phone = (String)view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["PHONE"].Value;
                String[] p = phone.Split(',');
                if (p != null)
                    if (p[0] != "")
                        phone = p[0];
                doDialPhone(phone);

            }
        }

        public void doDialCar(int carid)
        {
            //this.doBlockIncomingCalls();


            DataTable result = DataBase.mssqlRead("SELECT DRIVERS.PHONES FROM CARS, DRIVERS WHERE CARS.ID=" + carid.ToString() + " and CARS.DRIVERID=DRIVERS.ID ");
            if (result.Rows.Count > 0) {
                String phone = (String)result.Rows[0]["PHONES"];
                doDialPhone(phone);
            }
            return;

        }
        public void doDialPhone(String phone) {
            doBlockIncomingCalls();
         //   rightTabControl.SelectedIndex = 1;
            callPhone.Text = phone;
            caller.doDial(callPhone.Text);
            return ;
        }

        private void позвонитьToolStripMenuItem_Click(object sender, EventArgs e) {
            if (((ContextMenuStrip)((ToolStripDropDownItem)sender).Owner).SourceControl.Name == "view_raions")
            {
                if (view_raions.SelectedCells.Count == 1)
                {
                    int carid = 0;

                    if (int.TryParse(view_raions.SelectedCells[0].Value.ToString(), out carid))
                    {
                        doDialCar(carid);
                    }
                }


            }


            if (((ContextMenuStrip)((ToolStripDropDownItem)sender).Owner).SourceControl.Name == "view_cars")
            {
                if (view_cars.SelectedRows.Count != 1)
                    return;

                if (view_cars.SelectedRows[0].Index >= 0)
                {

                    int carid = int.Parse((view_cars.Rows[view_cars.SelectedRows[0].Index].Cells["CARID"].Value.ToString()));
                    doDialCar(carid);

                }
            }
        }

        private void дублироватьToolStripMenuItem_Click(object sender, EventArgs e) {
            if (view_work_orders.SelectedRows.Count != 1)
                return;

            if (view_work_orders.SelectedRows[0].Index >= 0)
            {

                int id = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ID"].Value.ToString()));
                if (settings.UseNewOrderForm) {
                    order_big o = new order_big();
                    o.doLoad(id, "", "", "");
                    o.setAsNewOrder();
                    o.Show();
                } 
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            //здесь запустить обмен с базой данных
            ///примерно 70-100 мс
            

            DataTable dt = UpdateListOrders();
            e.Result = dt;

            




        }

        
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            
            
            DataTable dt = (DataTable) e.Result;
            if (dt!=null)
                UpdateViewDataOrders(dt, this.view_work_orders);

            
            //здесь обработать таблицы, загруженные из базы данных
        }

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            DataTable dt = UpdateListCars();
            e.Result = dt;
        }

        private void backgroundWorker2_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            DataTable dt = (DataTable)e.Result;
            if (dt!=null)
            UpdateViewDataCars(dt, this.view_cars);
        }

        private void backgroundWorker3_DoWork(object sender, DoWorkEventArgs e)
        {
            String sql = "SELECT ID, DATA FROM DISTRIB";
            if (distr_by_new_prog == true)
                sql = "SELECT ID, DATA FROM GET_CARS_DISTRIB()";

            String sql_new = "SELECT ID, DATA FROM GET_CARS_DISTRIB()";
            
            
            //DataTable dt = DataBase.mssqlRead(sql);
            DataTable dt_new = null;
            try
            {
                dt_new = DataBase.mssqlRead(sql_new);
            }
            catch (Exception ex) { }

            DataTable [] result= {null, dt_new};

            e.Result = result;
        }

        private void backgroundWorker3_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

            DataTable[] result = (DataTable[])e.Result;
            DataTable dt = result[0];
            DataTable dt_new = result[1];
            if (dt_new != null)
                UpdateListRaions(dt, dt_new);
            
        }

        private void view_raions_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

            
            //здесь можно выводить количество заказов в районе --- полоской например....

            if (e.ColumnIndex == 1) //колонка с названиями зон. Зона кол-во заказов, кол-во машин едущих в район (+3) (кол-во заказов за полчаса (-4)
            {
                String add_data = "";
                String prev_data = "";
                lock (zones_orders_count)
                {
                    if ( zones_orders_count.ContainsKey(e.Value.ToString()))
                    {
                        add_data = add_data + " " + zones_orders_count[e.Value.ToString()].ToString();
                        e.CellStyle.BackColor = Color.Yellow;
                    }
                }

                lock (this.zones_cars_closed_count){
                    if (zones_cars_closed_count.ContainsKey(e.Value.ToString()))
                    {
                        add_data = add_data + " +" + zones_cars_closed_count[e.Value.ToString()].ToString();
                        //e.CellStyle.BackColor = Color.Yellow;
                    }
                }

                lock (this.zones_orders_closed_30_count)
                {
                    if (zones_orders_closed_30_count.ContainsKey(e.Value.ToString()))
                    {
                        prev_data = prev_data + "-" + zones_orders_closed_30_count[e.Value.ToString()].ToString()+" ";
                        //e.CellStyle.BackColor = Color.Yellow;
                    }
                }




                e.Value =  e.Value  + add_data + prev_data;
                 
                    


                
            }

            if (e.ColumnIndex>1)
            {
                //получить имя зоны - 
                  

                    //view_raions.Columns["ID"].

                String zoneid = ((DataTable)view_raions.DataSource).Rows[e.RowIndex]["ID"].ToString();



                if (e.Value!=null)
                {
                    String svalue = e.Value.ToString();
                    if (svalue != "")
                    {
                        int value=0;
                        if (int.TryParse(svalue, out value)){
                        //int value = int.Parse(svalue);

                        if (car_attributes.Contains(value))
                        {
                            int serviceid = (int)car_attributes[value];
                            if (serviceid == 1)
                                e.CellStyle.BackColor = Color.LightPink;

                            if (serviceid == 10)
                                e.CellStyle.BackColor = Color.LightGreen;
                            if (serviceid == 16)
                                e.CellStyle.BackColor = Color.LightSteelBlue;
                            if (serviceid == 29)
                                e.CellStyle.BackColor = Color.LightSlateGray;


                        }
                        }
                        else//несколько машин - считаем что это машины 29 службы (44)
                            e.CellStyle.BackColor = Color.LightSlateGray;


                        if (this.first_cars.ContainsKey(value)) {
                            if (TICK)
                                e.CellStyle.BackColor = Color.MediumVioletRed;
                            //в каком районе?

                            this.first_cars[value] = zoneid;
                            //add_data = add_data + " +" + zones_cars_closed_count[e.Value.ToString()].ToString();
                            //e.CellStyle.BackColor = Color.Yellow;
                        }

                    }
                    
                }
                //view_raions.get
                
            }
            
            
        }

        private void cmd_filter_orders_SelectedIndexChanged(object sender, EventArgs e) {
            show_order_type = cmd_filter_orders.SelectedIndex;
            if (show_order_type == 0)
            {
                filterPanel.Visible = false;
                timer2.Enabled = true;

            }
            else
            {
                filterPanel.Visible = true;
            }
            view_work_orders.Columns["ord_date"].Visible = filterPanel.Visible;
        }

        private void button3_Click_1(object sender, EventArgs e)
        {

        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            form_settings.ShowDialog();
        }

        private void miEndOrder1_Click(object sender, EventArgs e)
        {
            //должен быть активен только если на заказе есть машина.

            
            if (view_work_orders.SelectedRows.Count != 1)
                return;

            if (view_work_orders.SelectedRows[0].Index >= 0)
            {
                //0 - красный как ошибка распределения
                //1 - тоже красный. пишет завершен успешно.


                int id = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ID"].Value.ToString()));
                String[] fields = { "ComID", "OpID", "dtArrive", "Value1", "Value2", "State" };
                object[] values = { 4, MAIN_FORM.OperatorID, DateTime.Now, id, 1, 0 };

                DataBase.mssqlInsert("COMMANDS", fields, values); 
                
            }
            
            
            
        }

        private void нетМашинToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (view_work_orders.SelectedRows.Count != 1)
                return;
            //5 - ничего не делает.
            //4 - закрывает красным без результата
            //3 - заказ снят, желтый
            //2 - клиент не заплатил


            if (view_work_orders.SelectedRows[0].Index >= 0)
            {

                int id = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ID"].Value.ToString()));
                String[] fields = { "ComID", "OpID", "dtArrive", "Value1", "Value2", "State" };
                object[] values = { 4, MAIN_FORM.OperatorID, DateTime.Now, id, 1, 0 };

                DataBase.mssqlInsert("COMMANDS", fields, values);

            }
        }

        private void снятToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (view_work_orders.SelectedRows.Count != 1)
                return;
            //5 - ничего не делает.
            //4 - закрывает красным без результата
            //3 - заказ снят, желтый
            //2 - клиент не заплатил


            if (view_work_orders.SelectedRows[0].Index >= 0)
            {

                int id = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ID"].Value.ToString()));
                String[] fields = { "ComID", "OpID", "dtArrive", "Value1", "Value2", "State" };
                object[] values = { 4, MAIN_FORM.OperatorID, DateTime.Now, id, 2, 0 };

                DataBase.mssqlInsert("COMMANDS", fields, values);

            }
        }

        private void закрытьИДублироватьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (view_work_orders.SelectedRows.Count != 1)
                return;

            return;
            //5 - ничего не делает.
            //4 - закрывает красным без результата
            //3 - заказ снят, желтый
            //2 - клиент не заплатил


            if (view_work_orders.SelectedRows[0].Index >= 0)
            {

                int id = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ID"].Value.ToString()));


                if (settings.UseNewOrderForm) {
                    order_big o = new order_big();
                    o.doLoad(id, "", "", "");
                    o.setAsNewOrder();
                    o.Show();
                } 


                String[] fields1 = { "DescrForClient"};
                object[] values1 = { "снят дублированием"};

                DataBase.mssqlUpdate("ORDERS", fields1, values1, "ID", id);

                String[] fields = { "ComID", "OpID", "dtArrive", "Value1", "Value2", "State" };
                object[] values = { 4, MAIN_FORM.OperatorID, DateTime.Now, id, 3, 0 };

                DataBase.mssqlInsert("COMMANDS", fields, values);


            }
        }

        private void view_work_orders_MouseDown(object sender, MouseEventArgs e) {
            view_work_orders.ClearSelection();
        }

        private void mnuCars_Opening(object sender, CancelEventArgs e)
        {
            if (((ContextMenuStrip)sender).SourceControl.Name == "view_raions")
            {
                    позвонитьToolStripMenuItem.Enabled = (view_raions.SelectedCells.Count == 1);

            //    e.Cancel = true;
            }

            if (((ContextMenuStrip)sender).SourceControl.Name == "view_cars")
            {
                позвонитьToolStripMenuItem.Enabled = (view_cars.SelectedRows.Count == 1);
                //    e.Cancel = true;
            }
            
          //  if (view_work_orders.SelectedRows.Count != 1) {
//                e.Cancel = true;
//            } else {
        }

        private void сообщениеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (((ContextMenuStrip)((ToolStripDropDownItem)sender).Owner).SourceControl.Name == "view_raions")
            {
                String text = "";
                foreach (DataGridViewTextBoxCell cell in view_raions.SelectedCells)
                {
                    int carid = 0;

                    if ( int.TryParse(cell.Value.ToString(), out carid))
                    {
                        text += " " + cell.Value+ " ";
                    }
                    
                    
                    
                    

                }


                send_mess_box smb = new send_mess_box();
                smb.CAR_IDS.Text = text;
                smb.Show();
            }


            if (((ContextMenuStrip)((ToolStripDropDownItem)sender).Owner).SourceControl.Name == "view_cars")
            {
                String text = "";

                if (view_cars.SelectedRows.Count <1)
                    return;

                foreach (DataGridViewRow sel_row in view_cars.SelectedRows)
                {
                    int carid = int.Parse((view_cars.Rows[sel_row.Index].Cells["CARID"].Value.ToString()));
                    

                    text += " " + carid.ToString() + " ";






                }


                send_mess_box smb = new send_mess_box();
                smb.CAR_IDS.Text = text;
                smb.Show();
            }


        }

        private void view_raions_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (!e.RowIndex.Equals(-1) && !e.ColumnIndex.Equals(-1) && e.Button.Equals(MouseButtons.Right))
            {

                if (view_raions.SelectedCells.Count > 1)
                {
                    if (view_raions.SelectedCells.Contains(view_raions[e.ColumnIndex, e.RowIndex]))
                        return;
                }

                view_raions.ClearSelection();
                view_raions.CurrentCell = view_raions[e.ColumnIndex, e.RowIndex];
                
            }
        }


        public void doShowIncomingCallList(bool update_only )
        {
            this.BeginInvoke(new MethodInvoker(delegate()
                        {
                            try
                            {
                                //MAIN_FORM.incoming_calls_list.Add(inc_call_info);

                                if (m_pIncomingCallUI == null)
                                    m_pIncomingCallUI = new caller();

                                m_pIncomingCallUI.UpdateCallList();
                                if (!update_only)
                                    m_pIncomingCallUI.Show();


                                //new System.Threading.Thread(show_order_form).Start();
                                // Show incoming call UI.
//                                m_pIncomingCallUI = new incoming_call(e.ServerTransaction, dialog, this, sdpOffer);

//                                m_pIncomingCallUI.Show();

//                                m_pIncomingCallUI = null;
                            }
                            catch(Exception ex){
                            }
                        }));

            
        }

/*        public void doCancellAllRingingCalls()
        {
            SipCaller.doCancellAllRingingCalls();
        }
*/
        public void onIncomingCallAction(String action, caller.call_info inc_call_info)
        {
            /*
            1. answer
            2. hangup (Cancel)
            3. cancel */
            //answer - new order, SipCaller - new call session
            if (action == "answer")
            {
                //inc_call_info.caller.onIncomingCallAnswer(inc_call_info);
            }

            if (action == "answer_new_order")
            {
                this.sip_status_combo.SelectedIndex = 1;

                if (inc_call_info.PhoneInfo[5] != "") {
                    //Это звонок от водителя...
                    driver_form f = new driver_form();
                    f.Text = inc_call_info.call_desc;
                    //f.Tag = inc_call_info.PhoneInfo[5];
                    
                    f.current_order_id = inc_call_info.PhoneInfo[0];
                    f.Show();
                    f.textBox1.Text = inc_call_info.PhoneInfo[5];
                } else {

                    if (settings.UseNewOrderForm) {
                        if (inc_call_info.line.CompareTo("420100") == 0) {
                            form_review form = new form_review();
                            form.line.Text = "420100";
                            form.phone.Text = inc_call_info.phone;
                            form.Show();
                        } else {


                            order_big o = new order_big();
                            o.Text = inc_call_info.call_desc;
                            if (inc_call_info.orderid > 0)
                                o.doLoad(inc_call_info.orderid, inc_call_info.phone, inc_call_info.line, "");
                            else
                                o.doLoad(0, inc_call_info.phone, inc_call_info.line, "");
                            o.Show();
                        }
                    } 
                }


                //inc_call_info.caller.onIncomingCallAnswer(inc_call_info);
            }

            if (action == "answer_order") {

                if (settings.UseNewOrderForm) {
                    if (inc_call_info.line.CompareTo("420100") == 0) {
                        form_review form = new form_review();
                        form.line.Text = "420100";
                        form.phone.Text = inc_call_info.phone;
                        form.Show();
                    } else {
                        order_big o = new order_big();
                        o.Text = inc_call_info.call_desc;
                        o.doLoad(inc_call_info.orderid, inc_call_info.phone, inc_call_info.line, "");
                        o.Show();

                        //inc_call_info.caller.onIncomingCallAnswer(inc_call_info);
                    }
                } 
            }

            if (action == "cancel")
            {
                //iaxc.Lines.SelectLine(lineNumber);

                //inc_call_info.caller.onIncomingCallCancel(inc_call_info);
            }

            if (action == "hangup")
            {

                //iaxc.Lines[lineNumber].DropCall();
                //inc_call_info.caller.doHangup();
            }
        }

        private void btnDisableAlarm_Click(object sender, EventArgs e)
        {
            DataBase.mssqlExecuteSQL("UPDATE CARS SET Status=Status^64 WHERE Status&64=64");
        }

        private void btnPromisedPayment_Click(object sender, EventArgs e)
        {
            select_car form = new select_car();
            form.btnDial.Visible = false;
            form.btnSelect.Visible = false;
            form.ShowDialog(this);
        }

        private void btnRefreshOrders_Click(object sender, EventArgs e)
        {
            timer2.Enabled = true;
        }

        private void filterPanel_VisibleChanged(object sender, EventArgs e)
        {
            if (filterPanel.Visible == true)
            {

               





            }

        }

        private void положитьСуммуПоЗаказуВодителюToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ////1. проверить, что по заказу ещще не клалась сумма
            ////2. внести сумму в BANK с пометкой, по какому заказу
            //int orderid = 0;
            //int carid = 0;
            //int summ = 0;
            //if (view_work_orders.SelectedRows.Count == 0)
            //    return;

            //if (view_work_orders.SelectedRows[0].Index < 0)
            //    return;
            //orderid = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ID"].Value.ToString()));
            //carid = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["CARID"].Value.ToString()));
            //summ = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["sum"].Value.ToString()));

            //if (orderid <= 0)
            //    return;
            //if (carid <= 0)
            //    return;
            //if (summ <= 0)
            //    return;
            
            

            

        }

        private void sip_status_combo_DrawItem(object sender, DrawItemEventArgs e)
        {
           Graphics g = e.Graphics ;
 Rectangle r = e.Bounds ;

 if ( e.Index >= 0 ) 
 {
// Rectangle rd = r ; 
 //rd.Width = rd.Left + 100 ; 

 //Rectangle rt = r ;
 //r.X = rd.Right ;

 SolidBrush b = (SolidBrush)colorArray[e.Index];
 g.FillRectangle(b, r);
 StringFormat sf = new StringFormat();
 sf.Alignment = StringAlignment.Near;

 //Console.WriteLine(e.State.ToString());
 e.Graphics.DrawRectangle(new Pen(new SolidBrush(Color.Black), 2 ), r );
 if ( e.State == ( DrawItemState.NoAccelerator | DrawItemState.NoFocusRect))
 {
// e.Graphics.FillRectangle(new SolidBrush(Color.White) , r);
 e.Graphics.DrawString( ((ComboBox)sender).Items[e.Index].ToString(), new Font("Ariel" , 8 , FontStyle.Bold ) , new SolidBrush(Color.Black), r ,sf);
 e.DrawFocusRectangle();
 }
 else
 {
 //e.Graphics.FillRectangle(new SolidBrush(Color.LightBlue) , r);
     e.Graphics.DrawString(((ComboBox)sender).Items[e.Index].ToString(), new Font("Ariel", 8, FontStyle.Bold), new SolidBrush(Color.Black), r, sf);
 e.DrawFocusRectangle();
 }
 }
 
        }

        private void btn1Day_Click(object sender, EventArgs e)
        {
            filterDt1.Value = DateTime.Today;
            filterDt2.Value = DateTime.Today.AddDays(1);
            filterTm1.Value = DateTime.Parse("00:00:00");
            filterTm2.Value = DateTime.Parse("00:00:00");

        }

        private void btn7Day_Click(object sender, EventArgs e)
        {
            filterDt1.Value = DateTime.Today.AddDays(-6);
            filterDt2.Value = DateTime.Today.AddDays(1);
            filterTm1.Value = DateTime.Parse("00:00:00");
            filterTm2.Value = DateTime.Parse("00:00:00");
        }

        private void btn1Month_Click(object sender, EventArgs e)
        {
            filterDt1.Value = DateTime.Today.AddDays(-30);
            filterDt2.Value = DateTime.Today.AddDays(1);
            filterTm1.Value = DateTime.Parse("00:00:00");
            filterTm2.Value = DateTime.Parse("00:00:00");
        }

        private void filterPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            DateTime dt1, dt2;
            dt2 = DateTime.Now;
            dt1 = dt2.AddHours(-2);
            filterDt1.Value = dt1.Date; ;
            filterDt2.Value = dt2.Date; ;
            filterTm1.Value = dt1;
            filterTm2.Value = dt2;
            
            timer2.Enabled = true;

        }


        private void снятьДозвонToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (((ContextMenuStrip)((ToolStripDropDownItem)sender).Owner).SourceControl.Name == "view_raions")
            {
                if (view_raions.SelectedCells.Count == 1)
                {
                    int carid = 0;

                    if (int.TryParse(view_raions.SelectedCells[0].Value.ToString(), out carid))
                    {
                        DataBase.mssqlExecuteSQL("UPDATE CARS SET Status=Status^128, mobileStatus=mobileStatus^128 WHERE ID=" + carid.ToString());
                    }
                }


            }


            if (((ContextMenuStrip)((ToolStripDropDownItem)sender).Owner).SourceControl.Name == "view_cars")
            {
                if (view_cars.SelectedRows.Count != 1)
                    return;

                if (view_cars.SelectedRows[0].Index >= 0)
                {

                    int carid = int.Parse((view_cars.Rows[view_cars.SelectedRows[0].Index].Cells["CARID"].Value.ToString()));
                    DataBase.mssqlExecuteSQL("UPDATE CARS SET Status=Status^128, mobileStatus=mobileStatus^128 WHERE ID=" + carid.ToString());

                }
            }
        }

        private void снятьпToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (((ContextMenuStrip)((ToolStripDropDownItem)sender).Owner).SourceControl.Name == "view_raions")
            {
                if (view_raions.SelectedCells.Count == 1)
                {
                    int carid = 0;

                    if (int.TryParse(view_raions.SelectedCells[0].Value.ToString(), out carid))
                    {
                        DataBase.mssqlExecuteSQL("UPDATE CARS SET Status2=0 WHERE ID=" + carid.ToString());
                    }
                }


            }


            if (((ContextMenuStrip)((ToolStripDropDownItem)sender).Owner).SourceControl.Name == "view_cars")
            {
                if (view_cars.SelectedRows.Count != 1)
                    return;

                if (view_cars.SelectedRows[0].Index >= 0)
                {

                    int carid = int.Parse((view_cars.Rows[view_cars.SelectedRows[0].Index].Cells["CARID"].Value.ToString()));
                    DataBase.mssqlExecuteSQL("UPDATE CARS SET Status2=0 WHERE ID=" + carid.ToString());

                }
            }
        }

        private void снятьТревогуToolStripMenuItem1_Click(object sender, EventArgs e)
        {

            if (((ContextMenuStrip)((ToolStripDropDownItem)sender).Owner).SourceControl.Name == "view_raions")
            {
                if (view_raions.SelectedCells.Count == 1)
                {
                    int carid = 0;

                    if (int.TryParse(view_raions.SelectedCells[0].Value.ToString(), out carid))
                    {
                        DataBase.mssqlExecuteSQL("UPDATE CARS SET Status=Status^64, mobileStatus=mobileStatus^64 WHERE ID=" + carid.ToString());
                    }
                }


            }


            if (((ContextMenuStrip)((ToolStripDropDownItem)sender).Owner).SourceControl.Name == "view_cars")
            {
                if (view_cars.SelectedRows.Count != 1)
                    return;

                if (view_cars.SelectedRows[0].Index >= 0)
                {

                    int carid = int.Parse((view_cars.Rows[view_cars.SelectedRows[0].Index].Cells["CARID"].Value.ToString()));
                    DataBase.mssqlExecuteSQL("UPDATE CARS SET Status=Status^64, mobileStatus=mobileStatus^64 WHERE ID=" + carid.ToString());

                }
            }

        }

        private void статусыToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void поставитьпослеОтменыToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (((ContextMenuStrip)((ToolStripDropDownItem)sender).Owner).SourceControl.Name == "view_raions")
            {
                if (view_raions.SelectedCells.Count == 1)
                {
                    int carid = 0;

                    if (int.TryParse(view_raions.SelectedCells[0].Value.ToString(), out carid))
                    {
                        DataBase.mssqlExecuteSQL("UPDATE CARS SET Status2=1 WHERE ID=" + carid.ToString());
                    }
                }


            }


            if (((ContextMenuStrip)((ToolStripDropDownItem)sender).Owner).SourceControl.Name == "view_cars")
            {
                if (view_cars.SelectedRows.Count != 1)
                    return;

                if (view_cars.SelectedRows[0].Index >= 0)
                {

                    int carid = int.Parse((view_cars.Rows[view_cars.SelectedRows[0].Index].Cells["CARID"].Value.ToString()));
                    DataBase.mssqlExecuteSQL("UPDATE CARS SET Status2=1 WHERE ID=" + carid.ToString());

                }
            }
        }

        public void doShowCarOrders(int carid) {
            cmd_filter_orders.SelectedIndex = 3;
            //filterCar.Text = carid.ToString();
            filterCar.SelectedValue = carid.ToString();
            button1_Click_1(null, null);

            //filterCar.Text += " ";
            //filterCar.

        }

        private void button2_Click_2(object sender, EventArgs e) {
            
            add_money form = new add_money();
            form.Show();
        }

        private void button3_Click_2(object sender, EventArgs e) {

            DataTable res = DataBase.mssqlRead("SELECT ID, Address, HouseNum from ADRESSES and lon is null");
            foreach (DataRow row in res.Rows) {
                String street = (string)row[1];
                String house = (string)row[2];
                int id = (int)row[0];



                String latlon = doYandexSearchAddress(street, house);
                //String latlon = doSearchAddress(street, house);


                if (latlon != null)
                if (latlon != "") {
                    String[] ll = latlon.Split(' ');

                    String[] fields = { "lon", "lat"};
                    object[] values = { ll[0], ll[1]};
                    DataBase.mssqlUpdate("ADRESSES", fields, values, "ID", id);
                    
                }
            }
        }
        public static String doYandexSearchAddress(String street, String building_number)
        {

            String url = "http://geocode-maps.yandex.ru/1.x/?format=json&geocode=";
            url +=street +"+"+building_number ;
            //url += "&ll=37.618920,55.756994&spn=3.552069,2.400552";


            int timeout = 4000;
            string result = "0.0 0.0";
            String response = MapControl.GetHtmlPageText(url, timeout);
            if (response == "") {
                //calcDone = true;

                System.Console.WriteLine("Пустой ответ - ошибка расчета");
                return "0.0";
            }

            if (response.Contains("Point\":{\"pos\":\"39.88771 57.62250\"}")) {
                //calcDone = true;

                System.Console.WriteLine("не найден адрес - центр города - ошибка расчета");
                return "0.0";
            }
            System.Console.Write(response);

            // "Point":{"pos":"39.879378 57.596803"}
            int s = response.IndexOf("\"Point\":{\"pos\":\"");
            int e = 0;
            if (s > 0) {
                s += "\"Point\":{\"pos\":\"".Length;
                e = response.IndexOf("\"}", s);
                if (e > s) {
                    result = response.Substring(s, e - s);
                }
            }

//                
            return result;
        }

        private String doSearchAddress(String street, String building_number) {
            return "";

/*
           


            SQLiteConnection con = new SQLiteConnection();
            con.ConnectionString = @"Data Source=" + DataFile + ";New=False;Version=3"; // в таком виде всё работает!
            try {
                con.Open();
            } catch (Exception ex) {
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            System.Data.SQLite.SQLiteCommand cmd = con.CreateCommand();
            String result = "";
            if (building_number == "") { //поиск места - без номера дома
                cmd.CommandText = "SELECT lon ||' '||lat from addresses where upper(name)=@name";
                cmd.Parameters.AddWithValue("@name", street);
                result = (String)cmd.ExecuteScalar();

            } else {   //поиск по улице и дому
                cmd.CommandText = "SELECT lon ||' '||lat from addresses where street=@street and building_num=@building_num";
                cmd.Parameters.AddWithValue("@street", street);
                cmd.Parameters.AddWithValue("@building_num", building_number);
                result = (String)cmd.ExecuteScalar();
            }

            return result;
*/

        }

        private void маршрутОтсюдаToolStripMenuItem_Click(object sender, EventArgs e) {

        }

        private void button3_Click_3(object sender, EventArgs e) {
            ListGpsAddress list = new ListGpsAddress();
            list.Show(this);
        }

        private void chkMapShowOrders_SelectedIndexChanged(object sender, EventArgs e) {

        }


        

        private void Load2GisAddresses() {

            GrymCore.IGrym grymApp = null;
            GrymCore.IBaseReference baseRef = null;
            GrymCore.IBaseViewThread baseViewTread = null;
            GrymCore.ICommandLine cmdLine = null;
            grymApp = new GrymCore.GrymClass();
            // Получаем описание файла данных для заданного города
            // из коллекции описаний.
            baseRef = grymApp.BaseCollection.FindBase("Ярославль");
            baseViewTread = grymApp.GetBaseView(baseRef, true, false);

            baseViewTread.Activate(3);

            GrymCore.ITable table = baseViewTread.Database.get_Table("grym_map_building");
            System.Console.WriteLine("count = " + table.RecordCount.ToString());

            GrymCore.IMapCoordinateTransformationGeo transformate = (GrymCore.IMapCoordinateTransformationGeo)baseViewTread.Frame.Map.CoordinateTransformation;

            System.IO.StreamWriter sw = new StreamWriter("c:\\1\\2gis-adresses", true);
            

            for (int i = 0; i < table.RecordCount; i++) {
                GrymCore.IDataRow row = table.GetRecord(i + 1);
                String name = row.get_Value("name").ToString();
                //GrymCore.IDataRow addr = (GrymCore.IDataRow)row.get_Value("addr_1");
                int addr_count = int.Parse(row.get_Value("addr_count").ToString());
                //if (name!= "")
                if (!name.Contains(("Рекламный щит")))
                    if (!name.Contains(("Перетяжка ")))
                        if (!name.Contains(("Брандмауэр")))
                            if (!name.Contains(("Пилларс")))
                                if (!name.Contains(("Арка")))
                                    if (!name.Contains(("Суперсайт")))
                                        if (!name.Contains(("Призматрон")))
                if (addr_count > 0)
                    try {

                        for (int addr_idx = 1; addr_idx < addr_count + 1; addr_idx++) {

                            System.Console.WriteLine(i);

                            String street_1 = row.get_Value("street_"+addr_idx.ToString()).ToString();
                            String number_1 = row.get_Value("number_"+addr_idx.ToString()).ToString();
                            String city = row.get_Value("city").ToString();

                            GrymCore.IFeature feature = (GrymCore.IFeature)row;


                            GrymCore.IMapPoint point = transformate.LocalToGeo(feature.CenterPoint);

                            //System.Console.WriteLine(city + " " + name + " " + street_1 + " " + number_1 + " " + point.X.ToString() + " " + point.Y.ToString());

                            sw.WriteLine(name + ";"+city+";"+street_1+";"+number_1+";"+point.Y+";"+point.X);

                            String[] fields = { "GIS_NAME", "CITY", "STREET", "NUMBER", "ZONEID", "STATE", "LAT", "LON" };

                            object[] values = { name, city, street_1, number_1, 0, 122016, point.Y, point.X };

                            DataBase.mssqlInsert("GIS_ADRESSES", fields, values);
                        }


                    } catch (Exception ex) {
                    }


            }

            sw.Flush();
            sw.Close();






        }

        private void Load2GisOrgs()
        {

            GrymCore.IGrym grymApp = null;
            GrymCore.IBaseReference baseRef = null;
            GrymCore.IBaseViewThread baseViewTread = null;
            GrymCore.ICommandLine cmdLine = null;
            grymApp = new GrymCore.GrymClass();
            // Получаем описание файла данных для заданного города
            // из коллекции описаний.
            baseRef = grymApp.BaseCollection.FindBase("Ярославль");
            baseViewTread = grymApp.GetBaseView(baseRef, true, false);

            baseViewTread.Activate(3);

            GrymCore.ITable table = baseViewTread.Database.get_Table("grym_org");
            System.Console.WriteLine("count = " + table.RecordCount.ToString());

            GrymCore.IMapCoordinateTransformationGeo transformate = (GrymCore.IMapCoordinateTransformationGeo)baseViewTread.Frame.Map.CoordinateTransformation;

            System.IO.StreamWriter sw = new StreamWriter("z:\\2gis-orgs", true);
            /*
             * две базы.
             * 1. база "дома" - даже с двумя адресами
             * 2. база "организации - несколько адресов для одной организации
             */

            for (int i = 0; i < table.RecordCount; i++)
            {
                GrymCore.IDataRow row = table.GetRecord(i + 1);
                String name = row.get_Value("name").ToString();
                //GrymCore.IDataRow addr = (GrymCore.IDataRow)row.get_Value("addr_1");
                //int addr_count = int.Parse(row.get_Value("addr_count").ToString());

                
                int fil_count = int.Parse(row.get_Value("fil_count").ToString());
                for (int fil_idx = 1; fil_idx < fil_count + 1; fil_idx++)
                {
                    GrymCore.IDataRow fil = ( GrymCore.IDataRow)row.get_Value("fil_" + fil_idx);
                    int addr_count = int.Parse(fil.get_Value("addr_count").ToString());
                    if (addr_count > 0)
                        try
                        {

                            for (int addr_idx = 1; addr_idx < addr_count + 1; addr_idx++)
                            {
                                GrymCore.IDataRow addr = (GrymCore.IDataRow)fil.get_Value("addr_" + addr_idx);

                                GrymCore.IDataRow row_street = ((GrymCore.IDataRow)addr.get_Value("street"));

                                String city = ((GrymCore.IDataRow)row_street.get_Value("city")).get_Value("name").ToString();
                                String street = row_street.get_Value("name").ToString();
                                String number = addr.get_Value("number").ToString();
                                // String city = row.get_Value("city").ToString();

                                GrymCore.IFeature feature = (GrymCore.IFeature)addr.get_Value("feature");


                                GrymCore.IMapPoint point = transformate.LocalToGeo(feature.CenterPoint);

                                //System.Console.WriteLine(city + " " + name + " " + street_1 + " " + number_1 + " " + point.X.ToString() + " " + point.Y.ToString());

                                sw.WriteLine(name + ";" + city + ";" + street + ";" + number + ";" + point.Y + ";" + point.X);

                                String[] fields = { "NAME", "CITY", "STREET", "NUMBER", "ZONEID", "STATE", "LAT", "LON" };

                                object[] values = { name, city, street, number, 0, 11, point.Y, point.X };

                                DataBase.mssqlInsert("GIS_ADRESSES", fields, values);
                            }


                        }
                        catch (Exception ex)
                        {
                            System.Console.WriteLine(ex.Message);
                        }
                }

            }

            sw.Flush();
            sw.Close();






        }

        private void mapControl1_MouseUp(object sender, MouseEventArgs e) {
           

            
        }

        private void mapControl1_MouseDown(object sender, MouseEventArgs e) {



        }

        private void btnRestartSip_Click(object sender, EventArgs e)
        {
            statusbar.Text = "";

            if (caller != null)
            {
                caller.doHangup();
                caller.ResetCaller();
                //caller.SipClose();
                //caller = null;
            }

           // sip_status_combo.SelectedIndex = -1; 
/*


            caller = new SipCaller();
            caller.main_form = this;

            
            caller.InitCaller();
            caller.registerCaller();
            caller.isInitComplete = true;
*/
        }

        private void view_work_orders_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e) {
            DataBase.settings.saveUserSettingValue("VIEW_WORK_ORDER_COLUMN_WIDTH_"+e.Column.DataPropertyName, e.Column.Width.ToString());
        }

        private void btnPhonesList_Click(object sender, EventArgs e)
        {
            dictionary form = new dictionary();
            form.makeInterface("PHONES");
            form.ShowDialog(this);
        }

        private void вЧерныйСписокToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (view_work_orders.SelectedRows.Count != 1)
                return;

            if (view_work_orders.SelectedRows[0].Index >= 0)
            {

                
                int id = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ID"].Value.ToString()));
                String phone = (view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["PHONE1"].Value.ToString());
                String connect_phone = (view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["PHONE2"].Value.ToString());
                String address = (view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ADDRFROM"].Value.ToString()); ;
                String addrfrom1 = (view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ADDRFROM1"].Value.ToString()); ;
                String housefrom1 = (view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["HOUSEFROM1"].Value.ToString()); ;
                String flatfrom1 = (view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["FLATFROM1"].Value.ToString()); ;
                String date = (view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ORD_DATE"].Value.ToString()); ;

                String car_name = (view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["CAR_NAME"].Value.ToString());

                String car_id = (view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["CARID"].Value.ToString());

                if (car_name != "")
                {
                    address += ". Водитель " + car_name;

                }
                
                
                int phone_id = DataBase.mssqlReadInt("SELECT ID FROM PHONES where Phone = '"+phone+"'");

                

                
                DictionaryElement elem = new DictionaryElement();
                //elem.doMakeInterface("PHONES");
                elem.doLoad("PHONES", phone_id);
                elem.doSetValue("STATE", 1);
                if (phone_id == 0) {
                    elem.doSetValue("PHONE", phone);
                    elem.doSetValue("PHONE2", connect_phone);
                    elem.doSetValue("ADDRESS", addrfrom1);
                    elem.doSetValue("HOUSENUM", housefrom1);
                    elem.doSetValue("FLATNUM", flatfrom1);
                }
                elem.doSetValue("CAR_ID", car_id);
                

                elem.doSetValue("DESCR", "ПО ЗАКАЗУ " + id.ToString() + " " + date + " " + address);

                
                elem.ShowDialog(this);
                
            }




        }

        private void view_raions_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {

//            if (this.dataGridView1.Columns["ContactName"].Index ==
//        e.ColumnIndex && e.RowIndex >= 0)

            String draw_text = "";
            if (e.Value != null)
                draw_text = e.Value.ToString();

            if (e.ColumnIndex == 1) //колонка с названиями зон. Зона кол-во заказов, кол-во машин едущих в район (+3) (кол-во заказов за полчаса (-4)
            {
                String add_data = "";
                String prev_data = "";
                lock (zones_orders_count)
                {
                    if (zones_orders_count.ContainsKey(e.Value.ToString()))
                    {
                        add_data = add_data + " " + zones_orders_count[e.Value.ToString()].ToString();
                        e.CellStyle.BackColor = Color.Yellow;
                    }
                }

                lock (this.zones_cars_closed_count)
                {
                    if (zones_cars_closed_count.ContainsKey(e.Value.ToString()))
                    {
                        //add_data = add_data + " +" + zones_cars_closed_count[e.Value.ToString()].ToString();
                        //e.CellStyle.BackColor = Color.Yellow;
                    }
                }
/*
                lock (this.zones_orders_closed_30_count)
                {
                    if (zones_orders_closed_30_count.ContainsKey(e.Value.ToString()))
                    {
                        prev_data = prev_data + " -" + zones_orders_closed_30_count[e.Value.ToString()].ToString() + " ";
                        //e.CellStyle.BackColor = Color.Yellow;
                    }
                }
*/



                draw_text = draw_text + add_data + prev_data;
            }


            String zone_name = ((DataTable)view_raions.DataSource).Rows[e.RowIndex]["DESCR"].ToString();

            int cars_closed_cnt = 0;

            lock (this.zones_cars_closed_count)
            {
                if (zones_cars_closed_count.ContainsKey(zone_name))
                {
                    cars_closed_cnt = (int)zones_cars_closed_count[zone_name];
                    //e.CellStyle.BackColor = Color.Yellow;
                }
            }






            if (e.ColumnIndex >=0)


            {
                Rectangle newRect = new Rectangle(e.CellBounds.X + 1,
                    e.CellBounds.Y + 1, e.CellBounds.Width - 4,
                    e.CellBounds.Height - 4);

                Color Fillcolor = e.CellStyle.BackColor;
                if ((e.State & DataGridViewElementStates.Selected) == DataGridViewElementStates.Selected)
                    Fillcolor = e.CellStyle.SelectionBackColor;
                

                using (
                    Brush gridBrush = new SolidBrush(Color.Black),
                    backColorBrush = new SolidBrush(Fillcolor))
                {

                    
                    using (Pen gridLinePen = new Pen(gridBrush, 2))
                    {
                        // Erase the cell.
                        e.Graphics.FillRectangle(backColorBrush, e.CellBounds);

                        // Draw the grid lines (only the right and bottom lines;
                        // DataGridView takes care of the others).
                        if (e.ColumnIndex > 1)
                        if (cars_closed_cnt >= e.ColumnIndex-1)
                        {

                            e.Graphics.DrawLine(gridLinePen, e.CellBounds.Left,
                                e.CellBounds.Bottom - 1, e.CellBounds.Right - 1,
                                e.CellBounds.Bottom - 1);
//                            e.Graphics.DrawLine(gridLinePen, e.CellBounds.Right - 1,
//                                e.CellBounds.Top, e.CellBounds.Right - 1,
//                                e.CellBounds.Bottom);
                        }

                        // Draw the inset highlight box.
                        //e.Graphics.DrawRectangle(Pens.Blue, newRect);

                        // Draw the text content of the cell, ignoring alignment.
                        if (e.Value != null)
                        {
                            String value = draw_text;
                            e.Graphics.DrawString(value, e.CellStyle.Font,
                                Brushes.Black, e.CellBounds.X + 2,
                                e.CellBounds.Y + 2, StringFormat.GenericDefault);
                        }
                        e.Handled = true;
                    }
                }
            }


        //    e.Handled = true;
        }

        private void view_raions_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {

        }

        private void view_raions_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            //e.
            //e.Graphics.DrawLine(Pens.Black, new Point(e.CellBounds.X, e.CellBounds.Y), new Point(e.CellBounds.X + e.CellBounds.Width, e.CellBounds.Y));
        }

        private void button7_Click(object sender, EventArgs e) {
            dictionary form = new dictionary();
            form.makeInterface("CARS");
            form.ShowDialog(this);

        }

        private void button8_Click(object sender, EventArgs e) {
            dictionary form = new dictionary();
            form.makeInterface("DRIVERS");
            form.ShowDialog(this);
        }

        private void btnSaveZones_Click(object sender, EventArgs e)
        {
            if ((MAIN_FORM.OperatorID != 129)) {
                MessageBox.Show("У вас нет доступа на данную операцию");
                return;
            }
            if (MapControl.ZoneRegion.list_zone_region.Count == 0)
                return;

            int i=0;
            DataBase.mssqlExecuteSQL("DELETE FROM ZONES_POLYGONS where zone_id <> -1");


            foreach (MapControl.ZoneRegion zr in MapControl.ZoneRegion.list_zone_region)
            {
                String POLYGONS_DATA = "";
                

                foreach (Marker m in zr.zoneMarkers)
                {
                    POLYGONS_DATA = POLYGONS_DATA + m.lat + "|" + m.lon + "&";
                    //DataBase.mssqlInsert("ZONE_POLYGONS", fields, values);
                }

                System.Console.WriteLine(zr.ID + " " + zr.zone_id + " " + zr.zone_name + " " + POLYGONS_DATA);

                String[] fields = { "ZONE_ID", "POLYGONS_DATA", "POLYGON_NUMBER" , "remote_zone"};
                object[] values = { zr.zone_id, POLYGONS_DATA, zr.ID , zr.remote_zone};
                DataBase.mssqlInsert("ZONES_POLYGONS", fields, values);

                i++;
            }
        }

        private void btnLoadZones_Click(object sender, EventArgs e)
        {
            DataTable dt = DataBase.mssqlRead("SELECT ZONES_POLYGONS.*, ZONES.DESCR FROM ZONES_POLYGONS LEFT JOIN ZONES ON ZONES.ID=ZONES_POLYGONS.ZONE_ID where zone_id <> -1 order by ZONE_ID, ID");
            MapControl.ZoneRegion.list_zone_region.Clear();
  //          int old_polygon_number = -1;

    //        Dictionary<String, List<Marker>> kvp = null;
      //      List<Marker> ml = null;

           
            foreach (DataRow row in dt.Rows)
            {
                int zone_id = (int)row["ZONE_ID"];
                int polygon_number = (int)row["POLYGON_NUMBER"];
                int remote_zone = (int)row["remote_zone"];

                
                String name = row["DESCR"].ToString();

                String POLYGONS_DATA = (String)row["POLYGONS_DATA"];

                String[] latlon = POLYGONS_DATA.Split('&');


                MapControl.ZoneRegion zr = null;

                gisModule.ZonePolygon zp = null;

                zr = MapControl.ZoneRegion.AddZoneRegion();
                zr.zone_id = zone_id;
                zr.zone_name = name;
                zr.remote_zone = remote_zone;

                zp = gisModule.ZonePolygon.AddZonePolygon();
                zp.zone_id = zone_id;
                zp.zone_name = name;
                zp.remote_zone = remote_zone;

                    
                    
                //}

                double lat = 0;
                double lon = 0;
                foreach (String ll in latlon)
                {

                    String[] ll2 = ll.Split('|');

                    if (ll2.Length == 2)
                    {

                        double.TryParse(ll2[0], out lat);
                        double.TryParse(ll2[1], out lon);

                        Marker m = new Marker();
                        m.lat = lat;
                        m.lon = lon;
                        m.Tag = name;
                        m.parent_id = zone_id;
                        zr.AddZoneRegionMarker(m);


                        gisModule.geoPoint p = new gisModule.geoPoint();
                        p.lat = lat;
                        p.lon = lon;
                        zp.AddZonePolygonPoint(p);
                    }

                }




            }

            this.mapControl1.Invalidate();


        }

        
       
        private void button11_Click(object sender, EventArgs e)
        {
            //1. нарисовать районы.
            //2. загрузить точки, входящие в район.
            //цикл по точкам на карте, записать в базу номер района для них

            //или даже не надо!!!!! при выборе адреса из базы - его можно проверить в какой олигон он попадает...

            //т.е. все эти функции - только для редактирования вида и размера зон. очень вручную и аккуратно....
            //таким образом - выбор района адреса подачи - не по числу, а функцией перебора районов-полигонов..... это недолго...

            /*
             * int getLocationRaion(lat, lon) -- вернет район по координатам
             * geoPoint getLocationByText(String city, String street, String housenumber, String name, bool over_yandex) - координаты. поиск по внутренней базе или по яндексу
             * double getDistanceByPoints( getPoints[]) // расстояние в километрах
             * 
             * 
            Для единообразия ввода - загрузить в таблицу районов новые адреса.
             1. добавить новые адреса из таблицы ГИС
             * * сохранить адреса-названия
             * старые адреса-не названия - пометить  как устаревшие
             * 
             * или по другому - программе брать адреса из таблицы ГИС

            //


            //1. сбросить привязку адрес - район.
            //2. загрузить все точки, определить их районы.
            //3. записать в базу район точки.
            */

            this.mapControl1.addressMarkers.Clear();
            this.mapControl1.Invalidate();
            DataTable dt = DataBase.mssqlRead("SELECT * FROM gis_adresses where zoneid=0");
            foreach (DataRow row in dt.Rows)
            {
                float lat = (float)(double)row["lat"];
                float lon = (float)(double)row["lon"];
                PointF point = new PointF(lat, lon);
                int id = (int)row["ID"];

                Marker m = new Marker();
                m.lat = lat;
                m.lon = lon;
                m.Tag = (String)row["street"];
                m.parent_id = id;
                m.intParam1 = 0;

                //MapControl.ZoneRegion region = MapControl.ZoneRegion.list_zone_region[0];
//                foreach (MapControl.ZoneRegion region in MapControl.ZoneRegion.list_zone_region)
                {
  //                  if (pnppoly(m, region))
                    {

                        //m.intParam1 = region.zone_id;

                        this.mapControl1.addressMarkers.Add(m);

                        this.mapControl1.Invalidate();

                    }
                   
                }

            }
/*
            int i = 0;
            DataBase.mssqlExecuteSQL("update gis_adresses set zoneid=0 where lat>0");


        
            foreach (Marker m in mapControl1.addressMarkers)
            {
                String[] fields = { "zoneid" };
                object[] values = { m.intParam1};

                DataBase.mssqlUpdate("gis_adresses", fields, values, "ID", m.parent_id);
            }
 */ 
            //i++;
       



        }

        private void yjdsqToolStripMenuItem_Click(object sender, EventArgs e)
        {
            order_big o = new order_big();
            o.doLoad(-1, "", "", "");
            
            o.Show();
        }

        private void редактироватьНоваяФормаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //int ms_start = DateTime.Now.Second * 1000 + DateTime.Now.Millisecond;
            if (view_work_orders.SelectedRows.Count != 1)
                return;

            if (view_work_orders.SelectedRows[0].Index >= 0)
            {

                int id = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ID"].Value.ToString()));
                order_big o = new order_big();
                o.doLoad(id, "", "", "");
              //  System.Console.WriteLine("EditOrder1 " + (DateTime.Now.Second * 1000 + DateTime.Now.Millisecond - ms_start).ToString() + "мсек");
                o.Show();
//                System.Console.WriteLine("EditOrder2 " + (DateTime.Now.Second * 1000 + DateTime.Now.Millisecond - ms_start).ToString() + "мсек");
                //System.Console.WriteLine("EditOrder2 " + (DateTime.Now.Second * 1000 + DateTime.Now.Millisecond - ms_start).ToString() + "мсек");
                //System.Console.WriteLine("EditOrder2 " + (DateTime.Now.Second * 1000 + DateTime.Now.Millisecond - ms_start).ToString() + "мсек");
            }
        }

        private void редактироватьРайонToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //открыть диалог редактирования района
            //код, район к которому привязан полигон, тип (город, пригород), 

            //сейчас они храняться в двух таблицах - зоны и районы
            //Задать номер района, для которого этот полигон.
            //если 0 - это пригород.

            if (редактироватьРайонToolStripMenuItem.Tag == null)
                return;
            
            edit_polygon_raion f = new edit_polygon_raion();
            //((MapControl.ZoneRegion)редактироватьРайонToolStripMenuItem.Tag).
            f.Tag = редактироватьРайонToolStripMenuItem.Tag;
            f.ShowDialog();
            

         
        }

        private void НовыйПолигон_Click(object sender, EventArgs e)
        {

        }

        private void удалитьТочкуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mapControl1.doDeleteHightLitedMarker();
        }

        private void button4_Click_1(object sender, EventArgs e) {
           // Load2GisAddresses();
            //Load2GisOrgs();
        }

        private void btnLoadGisModuleData_Click(object sender, EventArgs e)
        {
            gisModule.doLoadGeoData();
        }

        private void закрытьЧерныйСписокToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (view_work_orders.SelectedRows[0].Index >= 0)
            {


                int id = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ID"].Value.ToString()));
                String phone = (view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["PHONE1"].Value.ToString());
                String connect_phone = (view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["PHONE2"].Value.ToString());
                String address = (view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ADDRFROM"].Value.ToString()); ;
                String addrfrom1 = (view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ADDRFROM1"].Value.ToString()); ;
                String housefrom1 = (view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["HOUSEFROM1"].Value.ToString()); ;
                String flatfrom1 = (view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["FLATFROM1"].Value.ToString()); ;
                String date = (view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ORD_DATE"].Value.ToString()); ;

                String car_name = (view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["CAR_NAME"].Value.ToString());

                String car_id = (view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["CARID"].Value.ToString());


                DataTable dt = DataBase.mssqlRead("SELECT black_list_money, black_list_car_id from orders where id = " + id.ToString());

                string money = dt.Rows[0]["black_list_money"].ToString();
                string to_car_id = dt.Rows[0]["black_list_car_id"].ToString();
                if (to_car_id == "")
                    to_car_id = "0";

                if (car_id == "")
                    car_id = "0";



                close_black_list form = new close_black_list();
                
                if (connect_phone != "")
                    phone = connect_phone;
                form.black_list_phone.Text = phone;
                form.black_list_summ.Text = money;


                form.Show();

                form.comboCarsFrom.SelectedValue = car_id;
                form.comboCarsTo.SelectedValue = to_car_id;


            }



        }

        private void перевестиВУспешноЗавершеныеToolStripMenuItem_Click(object sender, EventArgs e) {
            //должен быть активен только если на заказе есть машина.


            if (view_work_orders.SelectedRows.Count != 1)
                return;

            if (view_work_orders.SelectedRows[0].Index >= 0) {
                

                int id = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ID"].Value.ToString()));

                DateTime ord_date = (DateTime)((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ord_date"].Value));
                String[] fields = { "state", "resultcode", "dtEnd"};
                object[] values = { "3", "1", ord_date.AddHours(1) };

                DataBase.mssqlUpdate("ORDERS", fields, values, "ID", id); 
                

            }
            
        }

        private void карточкаToolStripMenuItem_Click(object sender, EventArgs e)
        {

            int carid = 0;

            if (((ContextMenuStrip)((ToolStripDropDownItem)sender).Owner).SourceControl.Name == "view_raions")
            {
                if (view_raions.SelectedCells.Count == 1)
                {
                    int.TryParse(view_raions.SelectedCells[0].Value.ToString(), out carid);
                }
            }


            if (((ContextMenuStrip)((ToolStripDropDownItem)sender).Owner).SourceControl.Name == "view_cars")
            {
                if (view_cars.SelectedRows.Count != 1)
                    return;

                if (view_cars.SelectedRows[0].Index >= 0)
                {

                    int.TryParse((view_cars.Rows[view_cars.SelectedRows[0].Index].Cells["CARID"].Value.ToString()), out carid);
                    

                }
            }

            if (carid > 0){


            driver_form f = new driver_form();
            f.Text = "Карточка водителя "+carid.ToString();;
            f.Tag = carid.ToString();
            f.current_order_id = "0";
            f.Show();
            }
        }

        private void MAIN_FORM_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                driver_form f = new driver_form();
                f.Text = "Карточка водителя ";
                f.Tag = "";
                f.current_order_id = "0";
                f.Show();
            }

            if (e.KeyCode == Keys.F6)
            {
                ArendaCar f = new ArendaCar();
                f.Text = "Карточка водителя ";
                f.Tag = "";
                f.Show();
            }
        }


        private void btn_driver_card_Click(object sender, EventArgs e)
        {
            driver_form f = new driver_form();
            f.Text = "Карточка водителя ";
            f.Tag = "";
            f.current_order_id = "0";
            f.Show();
        }

        private void загрузитьМаршрутToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int carid = 0;

            if (((ContextMenuStrip)((ToolStripDropDownItem)sender).Owner).SourceControl.Name == "view_raions")
            {
                if (view_raions.SelectedCells.Count == 1)
                {
                    int.TryParse(view_raions.SelectedCells[0].Value.ToString(), out carid);
                }
            }


            if (((ContextMenuStrip)((ToolStripDropDownItem)sender).Owner).SourceControl.Name == "view_cars")
            {
                if (view_cars.SelectedRows.Count != 1)
                    return;

                if (view_cars.SelectedRows[0].Index >= 0)
                {

                    int.TryParse((view_cars.Rows[view_cars.SelectedRows[0].Index].Cells["CARID"].Value.ToString()), out carid);


                }
            }

            if (carid > 0)
            {
                Track t = new Track();
                DataTable dt = DataBase.mssqlRead("SELECT * FROM cars_gps_log where carid = " + carid.ToString() + " and gps_time > getdate() - 1 order by gps_time");
                foreach (DataRow row in dt.Rows)
                {
                    TrackPoint item = new TrackPoint();
                    item.Lat = ((int)row["lat"])/1000000.0;
                    item.Lon = ((int)row["lon"])/1000000.0;
                    item.speed = (double)row["speed"];
                    item.accuracy = (double)row["accuracy"];
                    item.gps_time = (DateTime)row["gps_time"];
                    item.Time = (DateTime)row["gps_time"];
                    

                    if ((item.Lat > 0) && (item.Lon > 0))
                        t.Points.Add(item);

                    

                }
                mapControl1.Tracks.Clear();
                mapControl1.Tracks.Add(t);
            }
        }

        private void завершитьСНазначениемВодителяToolStripMenuItem_Click(object sender, EventArgs e) {
            if (view_work_orders.SelectedRows.Count != 1)
                return;

            if (view_work_orders.SelectedRows[0].Index >= 0) {

                int order_id = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ID"].Value.ToString()));
                select_car form = new select_car();
                form.ShowDialog(this);
                if (form.DialogResult == DialogResult.OK) {

                    String[] fields = { "ComID", "OpID", "dtArrive", "Value1", "Value2", "State" };
                    object[] values = { 4, MAIN_FORM.OperatorID, DateTime.Now, order_id, 1, 0 };

                    DataBase.mssqlInsert("COMMANDS", fields, values); 




                    String[] fields2 = { "state", "resultcode", "CARID", "DRIVERID" };
                    object[] values2 = { "3", "1", form.selected_car_id, form.selected_driver_id };

                    DataBase.mssqlUpdate("ORDERS", fields2, values2, "ID", order_id); 




                    //DataBase.mssqlUpdate("ORDERS", "CARID", form.selected_car_id, "ID", order_id);

                }



            }

        }

    

        private void поставитьПервымПослеОтменыToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (((ContextMenuStrip)((ToolStripDropDownItem)sender).Owner).SourceControl.Name == "view_raions")
            {
                if (view_raions.SelectedCells.Count == 1)
                {
                    int carid = 0;

                    if (int.TryParse(view_raions.SelectedCells[0].Value.ToString(), out carid))
                    {
                        DataBase.mssqlExecuteSQL("UPDATE cars_distrib SET queue_priority=1 WHERE CARID=" + carid.ToString());
                    }
                }


            }


            if (((ContextMenuStrip)((ToolStripDropDownItem)sender).Owner).SourceControl.Name == "view_cars")
            {
                if (view_cars.SelectedRows.Count != 1)
                    return;

                if (view_cars.SelectedRows[0].Index >= 0)
                {

                    int carid = int.Parse((view_cars.Rows[view_cars.SelectedRows[0].Index].Cells["CARID"].Value.ToString()));
                    DataBase.mssqlExecuteSQL("UPDATE cars_distrib SET queue_priority=1 WHERE CARID=" + carid.ToString());

                }
            }
        }

        private void снятьпоторопитеToolStripMenuItem_Click(object sender, EventArgs e) {
            if (view_work_orders.SelectedRows.Count != 1)
                return;

            if (view_work_orders.SelectedRows[0].Index >= 0) {


                int id = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ID"].Value.ToString()));
                String[] fields = { "sub_status" };
                object[] values = { "0"};

                DataBase.mssqlUpdate("ORDERS", fields, values, "ID", id);


            }
        }

        private void перевестиВзаказВРаботеToolStripMenuItem_Click(object sender, EventArgs e) {
            


            if (view_work_orders.SelectedRows.Count != 1)
                return;

            if (view_work_orders.SelectedRows[0].Index >= 0) {


                DataBase.mssqlExecuteSQL("update orders set orders.state=0, dtArrive=getdate() where orders.id = " + (view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ID"].Value.ToString()));
/*
                int id = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ID"].Value.ToString()));
                String[] fields = { "state", "resultcode" };
                object[] values = { "0", "0" };

                DataBase.mssqlUpdate("ORDERS", fields, values, "ID", id);
 */ 


            }
        }

        private void снятьпоторопитеToolStripMenuItem1_Click(object sender, EventArgs e) {
            if (view_work_orders.SelectedRows.Count != 1)
                return;

            if (view_work_orders.SelectedRows[0].Index >= 0) {


                int id = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ID"].Value.ToString()));
                String[] fields = { "sub_status" };
                object[] values = { "0" };

                DataBase.mssqlUpdate("ORDERS", fields, values, "ID", id);


            }
        }

        private void поставитьпоторопитьToolStripMenuItem_Click(object sender, EventArgs e) {
            if (view_work_orders.SelectedRows.Count != 1)
                return;

            if (view_work_orders.SelectedRows[0].Index >= 0) {


                int id = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ID"].Value.ToString()));
                String[] fields = { "sub_status" };
                object[] values = { "1" };

                DataBase.mssqlUpdate("ORDERS", fields, values, "ID", id);


            }
        }

        private void снятьпоторопитеИОтправитьВодителюторопимToolStripMenuItem_Click(object sender, EventArgs e) {

            if (view_work_orders.SelectedRows.Count != 1)
                return;

            if (view_work_orders.SelectedRows[0].Index >= 0) {


                int id = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ID"].Value.ToString()));
                String[] fields = { "sub_status" };
                object[] values = { "0" };

                DataBase.mssqlUpdate("ORDERS", fields, values, "ID", id);


                int carid = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["CARID"].Value.ToString()));

                String[] fields1 = { "ComID", "OpID", "dtArrive", "Value1", "message", "State" };
                object[] values1 = { 5, MAIN_FORM.OperatorID, DateTime.Now, carid, "торопим", 0 };

                DataBase.mssqlInsert("COMMANDS", fields1, values1);



            }


            
        }

        private void buttoт_dic_organizations_Click(object sender, EventArgs e) {
            dictionary form = new dictionary();
            form.summ_value_index = 2;
            form.makeInterface("ORGANIZATIONS");
            form.ShowDialog(this);
        }

        private void button13_Click_1(object sender, EventArgs e)
        {

            dataSet1.Tables["report_params"].Clear();
            dataSet1.Tables["report_orders"].Clear();

            object[] values =  {filterOrg.Text, filterDt1.Text +" "+ filterTm1.Text, filterDt2.Text +" "+ filterTm2.Text};
            dataSet1.Tables["report_params"].LoadDataRow(values, true);

            

            DataTable dt = orders_table.Copy();

            foreach (DataRow row in dt.Select("", "dtEnd"))
            {
                dataSet1.Tables["report_orders"].ImportRow(row);
            }


            //dataSet1.Tables["report_orders"].Load(orders_table.CreateDataReader());

            //report1.RegisterData(orders_table, "orders_table");
            report1.Show();
        }

        private void загрузитьгород_Click(object sender, EventArgs e)
        {
            DataTable dt = DataBase.mssqlRead("SELECT ZONES_POLYGONS.* FROM ZONES_POLYGONS where zone_id = -1");
            MapControl.ZoneRegion.list_zone_region.Clear();
            //          int old_polygon_number = -1;

            //        Dictionary<String, List<Marker>> kvp = null;
            //      List<Marker> ml = null;


            foreach (DataRow row in dt.Rows)
            {
                int zone_id = (int)row["ZONE_ID"];
                int polygon_number = (int)row["POLYGON_NUMBER"];
                int remote_zone = (int)row["remote_zone"];


                String name = "Городская черта";

                String POLYGONS_DATA = (String)row["POLYGONS_DATA"];

                String[] latlon = POLYGONS_DATA.Split('&');


                MapControl.ZoneRegion zr = null;

                

                zr = MapControl.ZoneRegion.AddZoneRegion();
                zr.zone_id = zone_id;
                zr.zone_name = name;
                zr.remote_zone = remote_zone;

                



                //}

                double lat = 0;
                double lon = 0;
                foreach (String ll in latlon)
                {

                    String[] ll2 = ll.Split('|');

                    if (ll2.Length == 2)
                    {

                        double.TryParse(ll2[0], out lat);
                        double.TryParse(ll2[1], out lon);

                        Marker m = new Marker();
                        m.lat = lat;
                        m.lon = lon;
                        m.Tag = name;
                        m.parent_id = zone_id;
                        zr.AddZoneRegionMarker(m);


                        
                    }

                }




            }

            this.mapControl1.Invalidate();
        }

        private void сщхранитьгород_Click(object sender, EventArgs e)
        {
            if ((MAIN_FORM.OperatorID != 129) ) {
                MessageBox.Show("У вас нет доступа на данную операцию");
                return;
            }
            if (MapControl.ZoneRegion.list_zone_region.Count == 0)
                return;

            int i = 0;
            DataBase.mssqlExecuteSQL("DELETE FROM ZONES_POLYGONS WHERE ZONE_ID = -1");


            foreach (MapControl.ZoneRegion zr in MapControl.ZoneRegion.list_zone_region)
            {
                String POLYGONS_DATA = "";

                if (zr.zone_id == -1)
                {

                    foreach (Marker m in zr.zoneMarkers)
                    {
                        POLYGONS_DATA = POLYGONS_DATA + m.lat + "|" + m.lon + "&";
                        //DataBase.mssqlInsert("ZONE_POLYGONS", fields, values);
                    }

                    System.Console.WriteLine(zr.ID + " " + zr.zone_id + " " + zr.zone_name + " " + POLYGONS_DATA);

                    String[] fields = { "ZONE_ID", "POLYGONS_DATA", "POLYGON_NUMBER", "remote_zone" };
                    object[] values = { zr.zone_id, POLYGONS_DATA, zr.ID, zr.remote_zone };
                    DataBase.mssqlInsert("ZONES_POLYGONS", fields, values);

                    i++;
                }
            }
        }

        private void button13_Click_2(object sender, EventArgs e) {

            dataSet1.Tables["report_params"].Clear();
            dataSet1.Tables["report_orders"].Clear();

            object[] values = { filterOrg.Text, filterDt1.Text + " " + filterTm1.Text, filterDt2.Text + " " + filterTm2.Text };
            dataSet1.Tables["report_params"].LoadDataRow(values, true);



            DataTable dt = orders_table.Copy();

            //foreach (DataRow row in dt.Select("", "dtEnd")) {
            foreach (DataRow row in dt.Select("", "dtEnd"))
            {
                dataSet1.Tables["report_orders"].ImportRow(row);
            }


            //dataSet1.Tables["report_orders"].Load(orders_table.CreateDataReader());

            //report1.RegisterData(orders_table, "orders_table");
            report2.Show();
        }

        private void button14_Click_1(object sender, EventArgs e) {

            dataSet1.Tables["report_params"].Clear();
            dataSet1.Tables["report_orders"].Clear();

            object[] values = { filterOrg.Text, filterDt1.Text + " " + filterTm1.Text, filterDt2.Text + " " + filterTm2.Text };
            dataSet1.Tables["report_params"].LoadDataRow(values, true);



            DataTable dt = orders_table.Copy();

            foreach (DataRow row in dt.Select("", "dtEnd")) {
                dataSet1.Tables["report_orders"].ImportRow(row);
            }


            //dataSet1.Tables["report_orders"].Load(orders_table.CreateDataReader());

            //report1.RegisterData(orders_table, "orders_table");
            report3.Show();
        }

        private void miEndOrder3_Click(object sender, EventArgs e) {
            //должен быть активен только если на заказе есть машина.


            if (view_work_orders.SelectedRows.Count != 1)
                return;

            if (view_work_orders.SelectedRows[0].Index >= 0) {
                //0 - красный как ошибка распределения
                //1 - тоже красный. пишет завершен успешно.


                int id = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ID"].Value.ToString()));
                String[] fields = { "ComID", "OpID", "dtArrive", "Value1", "Value2", "State" };
                object[] values = { 4, MAIN_FORM.OperatorID, DateTime.Now, id, 3, 0 };

                DataBase.mssqlInsert("COMMANDS", fields, values);

            }
        }

        private void mnuEndOrder2_Click(object sender, EventArgs e) {
            //должен быть активен только если на заказе есть машина.


            if (view_work_orders.SelectedRows.Count != 1)
                return;

            if (view_work_orders.SelectedRows[0].Index >= 0) {
                //0 - красный как ошибка распределения
                //1 - тоже красный. пишет завершен успешно.


                int id = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ID"].Value.ToString()));
                String[] fields = { "ComID", "OpID", "dtArrive", "Value1", "Value2", "State" };
                object[] values = { 4, MAIN_FORM.OperatorID, DateTime.Now, id, 2, 0 };

                DataBase.mssqlInsert("COMMANDS", fields, values);

            }
        }

        private void завершитьЗаказToolStripMenuItem_Click(object sender, EventArgs e) {

        }

        private void btnReview_Click(object sender, EventArgs e) {
            dictionary form = new dictionary();
            form.makeInterface("REVIEW");
            form.ShowDialog(this);
        }

        private void btn_dic_gis_addresses_Click(object sender, EventArgs e)
        {
            dictionary form = new dictionary();
            form.makeInterface("GIS_ADRESSES");
            form.ShowDialog(this);
        }

        private void button15_Click_1(object sender, EventArgs e) {

            dataSet1.Tables["report_params"].Clear();
            dataSet1.Tables["report_orders"].Clear();

            object[] values = { filterOrg.Text, filterDt1.Text + " " + filterTm1.Text, filterDt2.Text + " " + filterTm2.Text };
            dataSet1.Tables["report_params"].LoadDataRow(values, true);



            DataTable dt = orders_table.Copy();

            foreach (DataRow row in dt.Select("", "ord_date")) {
                dataSet1.Tables["report_orders"].ImportRow(row);
            }


            //dataSet1.Tables["report_orders"].Load(orders_table.CreateDataReader());

            //report1.RegisterData(orders_table, "orders_table");
            report4.Show();
        }

        private void btnAktSverki_Click(object sender, EventArgs e) {
            bnal_akt_sverki form = new bnal_akt_sverki();
            form.Show();
        }

        private void view_raions_CellParsing(object sender, DataGridViewCellParsingEventArgs e)
        {

        }

        private void ВернутьЗаказВРаботу_Click(object sender, EventArgs e)
        {
            


             if (view_work_orders.SelectedRows.Count != 1)
                return;

             if (view_work_orders.SelectedRows[0].Index >= 0)
             {


                 int id = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ID"].Value.ToString()));

                 String sql = " exec MOBILE_RETURN_ORDER_TO_WORK " + id;
                 DataBase.mssqlExecuteSQL(sql);
             }

        }

        private void button16_Click(object sender, EventArgs e)
        {

            dataSet1.Tables["report_params"].Clear();
            dataSet1.Tables["report_orders"].Clear();

            object[] values = { filterOrg.Text, filterDt1.Text + " " + filterTm1.Text, filterDt2.Text + " " + filterTm2.Text };
            dataSet1.Tables["report_params"].LoadDataRow(values, true);



            DataTable dt = orders_table.Copy();

            //foreach (DataRow row in dt.Select("", "dtEnd")) {
            foreach (DataRow row in dt.Select("", "dtEnd"))
            {
                dataSet1.Tables["report_orders"].ImportRow(row);
            }


            //dataSet1.Tables["report_orders"].Load(orders_table.CreateDataReader());

            //report1.RegisterData(orders_table, "orders_table");
            report5.Show();
        }

        private void включитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (view_work_orders.SelectedRows.Count != 1)
                return;

            if (view_work_orders.SelectedRows[0].Index >= 0)
            {


                int id = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ID"].Value.ToString()));

                String sql = "update orders set taxometr_enabled=1 where id=" + id;
                DataBase.mssqlExecuteSQL(sql);
            }
        }

        private void выключитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (view_work_orders.SelectedRows.Count != 1)
                return;

            if (view_work_orders.SelectedRows[0].Index >= 0)
            {


                int id = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ID"].Value.ToString()));

                String sql = "update orders set taxometr_enabled=0 where id=" + id;
                DataBase.mssqlExecuteSQL(sql);
            }
        }



        private void check_call_state_Tick(object sender, EventArgs e)
        {
            if (MAIN_FORM.main_form_object.statusbar.Text.Length==0)
            {
                if (caller!=null){
                    if (caller.hasActiveCall()){
                        MAIN_FORM.main_form_object.statusbar.Text = "Активный звонок";

                    }
                    else
                        MAIN_FORM.main_form_object.statusbar.Text = "";
                }

                
            }
        }


        private void view_work_orders_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            

            if (e.RowIndex>=0)
            if (e.ColumnIndex == 11)
            {
                int uslugi = 0;
                //if (((DataTable)view_work_orders.DataSource).Rows[e.RowIndex]["tax_uslugi"] != DBNull.Value)
                    //uslugi = (int)((DataTable)view_work_orders.DataSource).Rows[e.RowIndex]["tax_uslugi"];
                try
                {
                    uslugi = (int)view_work_orders.Rows[e.RowIndex].Cells["tax_uslugi"].Value;
                }
                catch (Exception ex)
                {
                }
                
                
                Rectangle rc = e.CellBounds;
                /*
                 *                     int uslugi = (int)ord_data.Rows[0]["tax_uslugi"];
                    String sUslugi = "";
                    if ((uslugi & 1) == 1) sUslugi += "ОТД РАЙОН ";
                    if ((uslugi & 2) == 2) sUslugi += "БАГАЖ ";
                    if ((uslugi & 4) == 4) sUslugi += "ЖИВОТНОЕ ";
                    if ((uslugi & 8) == 8) sUslugi += "ГРУНТ ";
                 */

                e.PaintBackground(rc, true);
                e.PaintContent(rc);
                //e.Graphics.DrawEllipse(Pens.Blue, rc);

                //Image im = (Image) resources.ResManager..GetImage("heating.png");


                

                
                if ((uslugi & 1) == 1){
                    Rectangle rc_icon = e.CellBounds;
                    rc_icon.X = rc_icon.Right - 15;
                    rc_icon.Width = 13;
                    rc_icon.Y = rc_icon.Y + (rc_icon.Height - 13) / 2;
                    rc_icon.Height = 13;
                    e.Graphics.DrawImage(Properties.Resources.heating13, rc_icon);
                }
                if ((uslugi & 2) == 2)
                {
                    Rectangle rc_icon = e.CellBounds;
                    rc_icon.X = rc_icon.Right - 15-15;
                    rc_icon.Width = 13;
                    rc_icon.Y = rc_icon.Y + (rc_icon.Height - 13) / 2;
                    rc_icon.Height = 13;
                    e.Graphics.DrawImage(Properties.Resources.briefcase3213, rc_icon);
                }
                if ((uslugi & 4) == 4)
                {
                    Rectangle rc_icon = e.CellBounds;
                    rc_icon.X = rc_icon.Right - 15-30;
                    rc_icon.Width = 13;
                    rc_icon.Y = rc_icon.Y + (rc_icon.Height - 13) / 2;
                    rc_icon.Height = 13;
                    e.Graphics.DrawImage(Properties.Resources.dog4813, rc_icon);
                }
                if ((uslugi & 8) == 8)
                {
                    Rectangle rc_icon = e.CellBounds;
                    rc_icon.X = rc_icon.Right - 15-45;
                    rc_icon.Width = 13;
                    rc_icon.Y = rc_icon.Y + (rc_icon.Height - 13) / 2;
                    rc_icon.Height = 13;
                    e.Graphics.DrawImage(Properties.Resources.heating13, rc_icon);
                }
                //if ((uslugi & 2) == 2) sUslugi += "БАГАЖ ";
                //if ((uslugi & 4) == 4) sUslugi += "ЖИВОТНОЕ ";
                //if ((uslugi & 8) == 8) sUslugi += "ГРУНТ ";
                  

                

                

                //e.Graphics.DrawIcon(resources.ResManager.GetIcon("heating.ico"), 0, 0);
                e.Handled = true;
            }


                
        }

        private void назначитьВодителяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (cmd_filter_orders.SelectedIndex>0)
            return;

            if (view_work_orders.SelectedRows.Count != 1)
                return;

            if (view_work_orders.SelectedRows[0].Index >= 0)
            {

                String carid = view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["CARID"].Value.ToString();
                int order_id = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ID"].Value.ToString()));
                select_car form = new select_car();
                form.ShowDialog(this);
                if (form.DialogResult == DialogResult.OK)
                {

                    //exec  MOBILE_MANUAL_SEND_ORDER_TO_CAR @carid, @network_channel_id, @ID; 
                   // DataBase.mssqlUpdate("ORDERS", "CARID", form.selected_car_id, "ID", order_id);

                    String result = DataBase.mssqlReadString(" exec MOBILE_MANUAL_SET_CAR_TO_ORDER " + form.selected_car_id + "," + order_id + "; ");
                    if (!result.StartsWith("ОШИБКА"))
                        DataBase.mssqlExecuteSQL("exec  MOBILE_MANUAL_SEND_ORDER_TO_CAR " + form.selected_car_id + ", " + order_id + "; ");
                    
                    MessageBox.Show(result);


                    



                }



            }

        }

        private void снятьВодителяСЗаказаToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if ((cmd_filter_orders.SelectedIndex ==2) || (cmd_filter_orders.SelectedIndex ==3))
                return;

            if (view_work_orders.SelectedRows.Count != 1)
                return;

            if (view_work_orders.SelectedRows[0].Index >= 0)
            {

                int order_id = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ID"].Value.ToString()));
                int car_id = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["CARID"].Value.ToString()));

                 int id = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ID"].Value.ToString()));
                String[] fields = { "ComID", "OpID", "dtArrive", "Value1", "Value2", "State", "Value3" };
                object[] values = { 4, MAIN_FORM.OperatorID, DateTime.Now, id, 10, 0 , car_id};

                DataBase.mssqlInsert("COMMANDS", fields, values); 




            }

        }

        private void попиликатьУВодителяToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (view_work_orders.SelectedRows.Count != 1)
                return;

            if (view_work_orders.SelectedRows[0].Index >= 0)
            {

                int car_id = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["CARID"].Value.ToString()));
                if (car_id > 0)
                {
                    DataBase.mssqlExecuteSQL(" insert into mobile_commands (cmd, carid, network_channel_id)  select 'SERVER_PLAY_VNIMANIE', id, network_channel_id from cars where id = "+car_id+"");
                }




            }
        }

        private void предложитьЗаказВодителюОкошкомToolStripMenuItem_Click(object sender, EventArgs e)
        {
             if (view_work_orders.SelectedRows.Count != 1)
                return;

            if (view_work_orders.SelectedRows[0].Index >= 0)
            {

               //int car_id = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["CARID"].Value.ToString()));


                select_car form = new select_car();
                form.ShowDialog(this);
                if (form.DialogResult == DialogResult.OK)
                {

                    //exec  MOBILE_MANUAL_SEND_ORDER_TO_CAR @carid, @network_channel_id, @ID; 
                   // DataBase.mssqlUpdate("ORDERS", "CARID", form.selected_car_id, "ID", order_id);

                    //String result = DataBase.mssqlReadString(" exec MOBILE_MANUAL_SET_CAR_TO_ORDER " + form.selected_car_id;



                    int order_id = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ID"].Value.ToString()));
                    if (order_id > 0 && form.selected_car_id>0)
                    {
                        DataBase.mssqlExecuteSQL(" exec MOBILE_SEND_ORDER_OFFER_TO_CAR " + form.selected_car_id + ", " + order_id);
                    }
                }




            }
            
        }

        private void button10_Click(object sender, EventArgs e)
        {
            /*
             окно с текущими тарифами.
             в т.ч. вести историю изменений и кто менял.
             запись в базу в таблицу mobile_main - ввести новые поля 
             
             
             */

            DictionaryElement dic = new DictionaryElement();
            dic.Show();
            dic.doLoad("TARIF", 1);
        }

        private void полноеРедактированиеЗаказаToolStripMenuItem_Click(object sender, EventArgs e)
        {
           if ((MAIN_FORM.OperatorID == 123) || (MAIN_FORM.OperatorID == 129)){


               //int ms_start = DateTime.Now.Second * 1000 + DateTime.Now.Millisecond;
               if (view_work_orders.SelectedRows.Count != 1)
                   return;

               if (view_work_orders.SelectedRows[0].Index >= 0)
               {

                   int id = int.Parse((view_work_orders.Rows[view_work_orders.SelectedRows[0].Index].Cells["ID"].Value.ToString()));
                   order_big o = new order_big();
                   o.doLoad(id, "", "", "");

                   o.Show();
                   o.doOpenAllfields();
               }



                    
           }
        }

        private void btnArendaCars_Click(object sender, EventArgs e)
        {
            dictionary form = new dictionary();
            form.makeInterface("ARENDA_CARS");
            form.ShowDialog(this);
        }






  

    }
}

