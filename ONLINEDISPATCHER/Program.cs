﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ONLINEDISPATCHER
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
/*
            AppDomain.CurrentDomain.UnhandledException += delegate(object sender, UnhandledExceptionEventArgs e)
            {
                MessageBox.Show("Error: " + ((Exception)e.ExceptionObject).Message, "Error:", MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
            Application.ThreadException += delegate(object sender, System.Threading.ThreadExceptionEventArgs e)
            {
                MessageBox.Show("Error: " + e.Exception.Message, "Error:", MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
*/

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("de-DE");
            Application.Run(new MAIN_FORM());
            
        }
    }
}
