﻿namespace ONLINEDISPATCHER
{
    partial class send_mess_box
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.CAR_IDS = new System.Windows.Forms.TextBox();
            this.MESS = new System.Windows.Forms.TextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.пОДТВЕРДИТЕЗАКАЗToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сПАСИБОToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, -1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Позывные";
            // 
            // CAR_IDS
            // 
            this.CAR_IDS.Location = new System.Drawing.Point(12, 18);
            this.CAR_IDS.Multiline = true;
            this.CAR_IDS.Name = "CAR_IDS";
            this.CAR_IDS.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.CAR_IDS.Size = new System.Drawing.Size(508, 94);
            this.CAR_IDS.TabIndex = 1;
            // 
            // MESS
            // 
            this.MESS.ContextMenuStrip = this.contextMenuStrip1;
            this.MESS.Location = new System.Drawing.Point(12, 130);
            this.MESS.MaxLength = 200;
            this.MESS.Multiline = true;
            this.MESS.Name = "MESS";
            this.MESS.Size = new System.Drawing.Size(680, 104);
            this.MESS.TabIndex = 2;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4,
            this.toolStripMenuItem5,
            this.пОДТВЕРДИТЕЗАКАЗToolStripMenuItem,
            this.сПАСИБОToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(378, 180);
            this.contextMenuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenuStrip1_ItemClicked);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(377, 22);
            this.toolStripMenuItem1.Text = "ВАС НЕ ВИДЯТ";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(377, 22);
            this.toolStripMenuItem2.Text = "К ВАМ ВЫХОДЯТ";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(377, 22);
            this.toolStripMenuItem3.Text = "НЕ МОЖЕМ ДОЗВОНИТЬСЯ - НАБЕРИТЕ В ДОМОФОН";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(377, 22);
            this.toolStripMenuItem4.Text = "ПАССАЖИРЫ ПРОСЯТ ПОДОЖДАТЬ";
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(377, 22);
            this.toolStripMenuItem5.Text = "НУЖНА МАШИНА В ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 114);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Сообщение";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(617, 251);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Отправить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.CheckOnClick = true;
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Items.AddRange(new object[] {
            "Все подключенные",
            "Все зарегистрированные",
            "Все на заказах",
            "Все не подключенные",
            "Все свободные не зарегистрированные",
            "Все"});
            this.checkedListBox1.Location = new System.Drawing.Point(526, 18);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(160, 94);
            this.checkedListBox1.TabIndex = 5;
            this.checkedListBox1.Visible = false;
            this.checkedListBox1.SelectedIndexChanged += new System.EventHandler(this.checkedListBox1_SelectedIndexChanged);
            this.checkedListBox1.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBox1_ItemCheck);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(709, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // пОДТВЕРДИТЕЗАКАЗToolStripMenuItem
            // 
            this.пОДТВЕРДИТЕЗАКАЗToolStripMenuItem.Name = "пОДТВЕРДИТЕЗАКАЗToolStripMenuItem";
            this.пОДТВЕРДИТЕЗАКАЗToolStripMenuItem.Size = new System.Drawing.Size(377, 22);
            this.пОДТВЕРДИТЕЗАКАЗToolStripMenuItem.Text = "ПОДТВЕРДИТЕ  ЗАКАЗ";
            // 
            // сПАСИБОToolStripMenuItem
            // 
            this.сПАСИБОToolStripMenuItem.Name = "сПАСИБОToolStripMenuItem";
            this.сПАСИБОToolStripMenuItem.Size = new System.Drawing.Size(377, 22);
            this.сПАСИБОToolStripMenuItem.Text = "СПАСИБО";
            // 
            // send_mess_box
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(709, 279);
            this.Controls.Add(this.checkedListBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.MESS);
            this.Controls.Add(this.CAR_IDS);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "send_mess_box";
            this.Text = "отправка сообщения водителям";
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox MESS;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        public System.Windows.Forms.TextBox CAR_IDS;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem пОДТВЕРДИТЕЗАКАЗToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сПАСИБОToolStripMenuItem;
    }
}