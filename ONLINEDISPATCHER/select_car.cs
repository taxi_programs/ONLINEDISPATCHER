﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;

namespace ONLINEDISPATCHER
{
    public partial class select_car : Form
    {
        public select_car()
        {
            InitializeComponent();
        }

        public int selected_car_id = 0;
        public int selected_driver_id = 0;
        public int car_service_id = 0;

        private void select_car_Load(object sender, EventArgs e)
        {
            DataTable cars = DataBase.mssqlRead("SELECT CARS.ID, cast (CARS.ID as varchar(6))+' '+  CARS.COLOR +' '+  CARS.mark +' '+ cars.gosnum +' '+drivers.fio +' '+ drivers.phones CAR_DRIVER_DESC  FROM CARS, DRIVERS WHERE DRIVERS.ID = CARS.DRIVERID order by CARS.ID");



            //    ServiceID.Items.Clear();
            comboCars.DataSource = cars;
            comboCars.DisplayMember = "CAR_DRIVER_DESC";
            comboCars.ValueMember = "ID";
            comboCars.DropDownStyle = ComboBoxStyle.DropDown;
            comboCars.AutoCompleteSource = AutoCompleteSource.ListItems;
          //  ZoneID.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            //comboCars.SelectedIndex = -1;
            comboCars.AutoCompleteMode = AutoCompleteMode.SuggestAppend;

            comboCars.Text = selected_car_id.ToString();
        }

        private void comboCars_SelectedIndexChanged(object sender, EventArgs e)
        {
            //выбрать машину по позывному
            //выбрать баланс
            //ФИО
            //телефоны
            //
            
            
            if (comboCars.SelectedValue != null)
            {
                try
                {
                    //int s = (String)comboCars.SelectedValue;
                    //System.Console.WriteLine(s);
                    int carid = (int)comboCars.SelectedValue;
                    selected_car_id = carid;
                    DataTable carinfo = DataBase.mssqlRead("SELECT CARS.ID, cast (CARS.ID as varchar(6))+' '+  CARS.COLOR +' '+  CARS.mark +' '+ cars.gosnum CAR_DESC, drivers.fio +' '+ drivers.phones DRIVER_DESC, drivers.id driver_id, cars.serviceid car_service_id FROM CARS left join DRIVERS on DRIVERS.ID = CARS.DRIVERID WHERE CARS.ID = " + carid.ToString());
                    car_info.Text = carinfo.Rows[0]["CAR_DESC"] + " " + carinfo.Rows[0]["DRIVER_DESC"];
                    selected_driver_id = (int)carinfo.Rows[0]["driver_id"];
                    car_service_id = (int)carinfo.Rows[0]["car_service_id"];
                }
                catch (Exception ex)
                {

                }
            }
        }

        private void comboCars_DropDownClosed(object sender, EventArgs e)
        {
            comboCars_SelectedIndexChanged(null, null);
        }

        private void btnDial_Click(object sender, EventArgs e)
        {

        }

        private void btnPromisedPayment_Click(object sender, EventArgs e)
        {
             if (comboCars.SelectedValue == null)
                 return;
             int carid = (int)comboCars.SelectedValue;
             selected_car_id = carid;
                
            //проверить, нет ли непогашенного платежа
            //узнать службу водителя
            //внести 500 р в банк
             if (MessageBox.Show("Внести обещанный платеж?", "Обещанный платеж", MessageBoxButtons.YesNo) != DialogResult.Yes)
                 return;




            DataTable bank_info = DataBase.mssqlRead("SELECT dtArrive from bank where DriverID="+selected_driver_id.ToString()+" and promised_payment=1");
            if (bank_info.Rows.Count > 0)
            {
                MessageBox.Show("Ошибка. У водителя уже внесен обещанный платеж " + bank_info.Rows[0]["dtArrive"].ToString(), "Обещанный платеж", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }



            String[] fields = { "driverid", "ServiceID", "OpID", "dtArrive", "sum", "type", "promised_payment", "Descr" };
            object[] values = { selected_driver_id, car_service_id, MAIN_FORM.OperatorID, DateTime.Now, 200, 0, 1, "<ОБЕЩАННЫЙ ПЛАТЕЖ>" };

            DataBase.mssqlInsert("BANK", fields, values);

            MessageBox.Show("Внесен обещанный платеж 200 руб" , "Обещанный платеж", MessageBoxButtons.OK, MessageBoxIcon.None);
            
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            Close();
            /*
            driver_form f = new driver_form(null);
            f.Text = "ТЕКСТ Водитель такойто...";
            f.Tag = selected_car_id.ToString();
            f.current_order_id = "180";
            f.Show();
             */
        }
    }
}
