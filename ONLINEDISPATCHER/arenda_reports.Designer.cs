﻿namespace ONLINEDISPATCHER
{
    partial class arenda_reports
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arenda_reports));
            this.cmbDrivers = new SergeUtils.EasyCompletionComboBox();
            this.cmbArendaCars = new SergeUtils.EasyCompletionComboBox();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.dataSet1 = new System.Data.DataSet();
            this.report_data = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn11 = new System.Data.DataColumn();
            this.dataColumn12 = new System.Data.DataColumn();
            this.dataColumn13 = new System.Data.DataColumn();
            this.report_params = new System.Data.DataTable();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dataTable1 = new System.Data.DataTable();
            this.dataColumn14 = new System.Data.DataColumn();
            this.dataColumn15 = new System.Data.DataColumn();
            this.dataColumn16 = new System.Data.DataColumn();
            this.dataColumn17 = new System.Data.DataColumn();
            this.dataColumn18 = new System.Data.DataColumn();
            this.dataColumn19 = new System.Data.DataColumn();
            this.dataColumn21 = new System.Data.DataColumn();
            this.dataColumn22 = new System.Data.DataColumn();
            this.dataColumn23 = new System.Data.DataColumn();
            this.dataColumn20 = new System.Data.DataColumn();
            this.dataColumn24 = new System.Data.DataColumn();
            this.dataColumn25 = new System.Data.DataColumn();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label_info = new System.Windows.Forms.Label();
            this.dtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.report1 = new FastReport.Report();
            this.dataColumn26 = new System.Data.DataColumn();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.dataTable2 = new System.Data.DataTable();
            this.driverFIODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.payedcarsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.payersDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.payeddaysDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.arendaregsumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.arendaregtypenameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kassaarendadriverssumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kassaalldriverssumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nppDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataColumn27 = new System.Data.DataColumn();
            this.dataColumn28 = new System.Data.DataColumn();
            this.dataColumn29 = new System.Data.DataColumn();
            this.dataColumn30 = new System.Data.DataColumn();
            this.dataColumn31 = new System.Data.DataColumn();
            this.dataColumn32 = new System.Data.DataColumn();
            this.dataColumn33 = new System.Data.DataColumn();
            this.dataColumn34 = new System.Data.DataColumn();
            this.report2 = new FastReport.Report();
            this.dataColumn35 = new System.Data.DataColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.report_data)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.report_params)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.report1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.report2)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbDrivers
            // 
            this.cmbDrivers.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmbDrivers.FormattingEnabled = true;
            this.cmbDrivers.Location = new System.Drawing.Point(115, 106);
            this.cmbDrivers.Name = "cmbDrivers";
            this.cmbDrivers.Size = new System.Drawing.Size(491, 28);
            this.cmbDrivers.TabIndex = 75;
            // 
            // cmbArendaCars
            // 
            this.cmbArendaCars.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmbArendaCars.FormattingEnabled = true;
            this.cmbArendaCars.Location = new System.Drawing.Point(115, 140);
            this.cmbArendaCars.Name = "cmbArendaCars";
            this.cmbArendaCars.Size = new System.Drawing.Size(491, 24);
            this.cmbArendaCars.TabIndex = 74;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker2.Location = new System.Drawing.Point(144, 38);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(94, 20);
            this.dateTimePicker2.TabIndex = 73;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(26, 38);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(94, 20);
            this.dateTimePicker1.TabIndex = 72;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 140);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 70;
            this.label1.Text = "Арендная машина";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 71;
            this.label3.Text = "Водитель";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 76;
            this.label2.Text = "Период платежей";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(404, 194);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(202, 23);
            this.button1.TabIndex = 77;
            this.button1.Text = "Оплаты";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(13, 194);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(202, 23);
            this.button2.TabIndex = 78;
            this.button2.Text = "Оплаты (по водителям)";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(370, 428);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(202, 23);
            this.button3.TabIndex = 79;
            this.button3.Text = "Оплаты( по машинам)";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "NewDataSet";
            this.dataSet1.Tables.AddRange(new System.Data.DataTable[] {
            this.report_data,
            this.report_params,
            this.dataTable1,
            this.dataTable2});
            // 
            // report_data
            // 
            this.report_data.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn11,
            this.dataColumn12,
            this.dataColumn13});
            this.report_data.TableName = "report_data";
            // 
            // dataColumn1
            // 
            this.dataColumn1.Caption = "Водитель";
            this.dataColumn1.ColumnName = "driverFIO";
            // 
            // dataColumn2
            // 
            this.dataColumn2.Caption = "Оплаченые машины";
            this.dataColumn2.ColumnName = "payed_cars";
            // 
            // dataColumn3
            // 
            this.dataColumn3.Caption = "Оплатившие водители";
            this.dataColumn3.ColumnName = "payers";
            // 
            // dataColumn4
            // 
            this.dataColumn4.Caption = "Опаченые смены";
            this.dataColumn4.ColumnName = "payed_days";
            // 
            // dataColumn5
            // 
            this.dataColumn5.Caption = "Опаченая сумма";
            this.dataColumn5.ColumnName = "arenda_reg_sum";
            this.dataColumn5.DataType = typeof(int);
            // 
            // dataColumn6
            // 
            this.dataColumn6.Caption = "Виды оплат";
            this.dataColumn6.ColumnName = "arenda_reg_type_name";
            // 
            // dataColumn7
            // 
            this.dataColumn7.Caption = "дата";
            this.dataColumn7.ColumnName = "dt";
            this.dataColumn7.DataType = typeof(System.DateTime);
            // 
            // dataColumn11
            // 
            this.dataColumn11.Caption = "Нал арендники";
            this.dataColumn11.ColumnName = "kassa_arenda_drivers_sum";
            this.dataColumn11.DataType = typeof(int);
            // 
            // dataColumn12
            // 
            this.dataColumn12.Caption = "Нал все";
            this.dataColumn12.ColumnName = "kassa_all_drivers_sum";
            this.dataColumn12.DataType = typeof(int);
            // 
            // dataColumn13
            // 
            this.dataColumn13.Caption = "#";
            this.dataColumn13.ColumnName = "npp";
            this.dataColumn13.DataType = typeof(System.Data.SqlTypes.SqlInt32);
            // 
            // report_params
            // 
            this.report_params.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn8,
            this.dataColumn9,
            this.dataColumn10});
            this.report_params.TableName = "report_params";
            // 
            // dataColumn8
            // 
            this.dataColumn8.ColumnName = "title";
            // 
            // dataColumn9
            // 
            this.dataColumn9.ColumnName = "dt_from";
            this.dataColumn9.DataType = typeof(System.DateTime);
            // 
            // dataColumn10
            // 
            this.dataColumn10.ColumnName = "dt_to";
            this.dataColumn10.DataType = typeof(System.DateTime);
            // 
            // dataTable1
            // 
            this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn14,
            this.dataColumn15,
            this.dataColumn16,
            this.dataColumn17,
            this.dataColumn18,
            this.dataColumn19,
            this.dataColumn21,
            this.dataColumn22,
            this.dataColumn23,
            this.dataColumn20,
            this.dataColumn24,
            this.dataColumn25,
            this.dataColumn26});
            this.dataTable1.TableName = "report_data2";
            // 
            // dataColumn14
            // 
            this.dataColumn14.Caption = "Дата";
            this.dataColumn14.ColumnName = "dt";
            this.dataColumn14.DataType = typeof(System.DateTime);
            // 
            // dataColumn15
            // 
            this.dataColumn15.Caption = "Водитель";
            this.dataColumn15.ColumnName = "driver_fio";
            // 
            // dataColumn16
            // 
            this.dataColumn16.Caption = "Машина";
            this.dataColumn16.ColumnName = "car_name";
            // 
            // dataColumn17
            // 
            this.dataColumn17.Caption = "План";
            this.dataColumn17.ColumnName = "plan_sum";
            this.dataColumn17.DataType = typeof(int);
            // 
            // dataColumn18
            // 
            this.dataColumn18.Caption = "Тип оплаты";
            this.dataColumn18.ColumnName = "reg_type";
            // 
            // dataColumn19
            // 
            this.dataColumn19.Caption = "Оплата";
            this.dataColumn19.ColumnName = "reg_sum";
            this.dataColumn19.DataType = typeof(int);
            // 
            // dataColumn21
            // 
            this.dataColumn21.Caption = "Долг";
            this.dataColumn21.ColumnName = "dolg_sum";
            this.dataColumn21.DataType = typeof(int);
            // 
            // dataColumn22
            // 
            this.dataColumn22.Caption = "Описание";
            this.dataColumn22.ColumnName = "reg_descr";
            // 
            // dataColumn23
            // 
            this.dataColumn23.Caption = "Долг аренда";
            this.dataColumn23.ColumnName = "dolg1_on_date";
            this.dataColumn23.DataType = typeof(int);
            // 
            // dataColumn20
            // 
            this.dataColumn20.Caption = "Долг налоги";
            this.dataColumn20.ColumnName = "dolg3_on_date";
            this.dataColumn20.DataType = typeof(int);
            // 
            // dataColumn24
            // 
            this.dataColumn24.Caption = "Долг штрафы";
            this.dataColumn24.ColumnName = "dolg4_on_date";
            this.dataColumn24.DataType = typeof(int);
            // 
            // dataColumn25
            // 
            this.dataColumn25.Caption = "Долг ремонты";
            this.dataColumn25.ColumnName = "dolg5_on_date";
            this.dataColumn25.DataType = typeof(int);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.driverFIODataGridViewTextBoxColumn,
            this.payedcarsDataGridViewTextBoxColumn,
            this.payersDataGridViewTextBoxColumn,
            this.payeddaysDataGridViewTextBoxColumn,
            this.arendaregsumDataGridViewTextBoxColumn,
            this.arendaregtypenameDataGridViewTextBoxColumn,
            this.dtDataGridViewTextBoxColumn1,
            this.kassaarendadriverssumDataGridViewTextBoxColumn,
            this.kassaalldriverssumDataGridViewTextBoxColumn,
            this.nppDataGridViewTextBoxColumn});
            this.dataGridView1.DataMember = "report_data";
            this.dataGridView1.DataSource = this.dataSet1;
            this.dataGridView1.Location = new System.Drawing.Point(660, 10);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(816, 504);
            this.dataGridView1.TabIndex = 80;
            this.dataGridView1.Visible = false;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // label_info
            // 
            this.label_info.AutoSize = true;
            this.label_info.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_info.Location = new System.Drawing.Point(25, 346);
            this.label_info.Name = "label_info";
            this.label_info.Size = new System.Drawing.Size(65, 24);
            this.label_info.TabIndex = 81;
            this.label_info.Text = "+++++";
            // 
            // dtDataGridViewTextBoxColumn
            // 
            this.dtDataGridViewTextBoxColumn.DataPropertyName = "dt";
            this.dtDataGridViewTextBoxColumn.HeaderText = "Дата";
            this.dtDataGridViewTextBoxColumn.Name = "dtDataGridViewTextBoxColumn";
            this.dtDataGridViewTextBoxColumn.Width = 70;
            // 
            // report1
            // 
            this.report1.ReportResourceString = resources.GetString("report1.ReportResourceString");
            this.report1.RegisterData(this.dataSet1, "dataSet1");
            // 
            // dataColumn26
            // 
            this.dataColumn26.ColumnName = "car_gosnum";
            this.dataColumn26.DataType = typeof(int);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(13, 223);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(202, 23);
            this.button4.TabIndex = 82;
            this.button4.Text = "Касса подробно";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(13, 252);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(202, 23);
            this.button5.TabIndex = 83;
            this.button5.Text = "Касса по водителям";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // dataTable2
            // 
            this.dataTable2.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn27,
            this.dataColumn28,
            this.dataColumn29,
            this.dataColumn30,
            this.dataColumn31,
            this.dataColumn32,
            this.dataColumn33,
            this.dataColumn34,
            this.dataColumn35});
            this.dataTable2.TableName = "report_kassa_data";
            // 
            // driverFIODataGridViewTextBoxColumn
            // 
            this.driverFIODataGridViewTextBoxColumn.DataPropertyName = "driverFIO";
            this.driverFIODataGridViewTextBoxColumn.HeaderText = "driverFIO";
            this.driverFIODataGridViewTextBoxColumn.Name = "driverFIODataGridViewTextBoxColumn";
            // 
            // payedcarsDataGridViewTextBoxColumn
            // 
            this.payedcarsDataGridViewTextBoxColumn.DataPropertyName = "payed_cars";
            this.payedcarsDataGridViewTextBoxColumn.HeaderText = "payed_cars";
            this.payedcarsDataGridViewTextBoxColumn.Name = "payedcarsDataGridViewTextBoxColumn";
            // 
            // payersDataGridViewTextBoxColumn
            // 
            this.payersDataGridViewTextBoxColumn.DataPropertyName = "payers";
            this.payersDataGridViewTextBoxColumn.HeaderText = "payers";
            this.payersDataGridViewTextBoxColumn.Name = "payersDataGridViewTextBoxColumn";
            // 
            // payeddaysDataGridViewTextBoxColumn
            // 
            this.payeddaysDataGridViewTextBoxColumn.DataPropertyName = "payed_days";
            this.payeddaysDataGridViewTextBoxColumn.HeaderText = "payed_days";
            this.payeddaysDataGridViewTextBoxColumn.Name = "payeddaysDataGridViewTextBoxColumn";
            // 
            // arendaregsumDataGridViewTextBoxColumn
            // 
            this.arendaregsumDataGridViewTextBoxColumn.DataPropertyName = "arenda_reg_sum";
            this.arendaregsumDataGridViewTextBoxColumn.HeaderText = "arenda_reg_sum";
            this.arendaregsumDataGridViewTextBoxColumn.Name = "arendaregsumDataGridViewTextBoxColumn";
            // 
            // arendaregtypenameDataGridViewTextBoxColumn
            // 
            this.arendaregtypenameDataGridViewTextBoxColumn.DataPropertyName = "arenda_reg_type_name";
            this.arendaregtypenameDataGridViewTextBoxColumn.HeaderText = "arenda_reg_type_name";
            this.arendaregtypenameDataGridViewTextBoxColumn.Name = "arendaregtypenameDataGridViewTextBoxColumn";
            // 
            // dtDataGridViewTextBoxColumn1
            // 
            this.dtDataGridViewTextBoxColumn1.DataPropertyName = "dt";
            this.dtDataGridViewTextBoxColumn1.HeaderText = "dt";
            this.dtDataGridViewTextBoxColumn1.Name = "dtDataGridViewTextBoxColumn1";
            // 
            // kassaarendadriverssumDataGridViewTextBoxColumn
            // 
            this.kassaarendadriverssumDataGridViewTextBoxColumn.DataPropertyName = "kassa_arenda_drivers_sum";
            this.kassaarendadriverssumDataGridViewTextBoxColumn.HeaderText = "kassa_arenda_drivers_sum";
            this.kassaarendadriverssumDataGridViewTextBoxColumn.Name = "kassaarendadriverssumDataGridViewTextBoxColumn";
            // 
            // kassaalldriverssumDataGridViewTextBoxColumn
            // 
            this.kassaalldriverssumDataGridViewTextBoxColumn.DataPropertyName = "kassa_all_drivers_sum";
            this.kassaalldriverssumDataGridViewTextBoxColumn.HeaderText = "kassa_all_drivers_sum";
            this.kassaalldriverssumDataGridViewTextBoxColumn.Name = "kassaalldriverssumDataGridViewTextBoxColumn";
            // 
            // nppDataGridViewTextBoxColumn
            // 
            this.nppDataGridViewTextBoxColumn.DataPropertyName = "npp";
            this.nppDataGridViewTextBoxColumn.HeaderText = "npp";
            this.nppDataGridViewTextBoxColumn.Name = "nppDataGridViewTextBoxColumn";
            // 
            // dataColumn27
            // 
            this.dataColumn27.Caption = "ПОзывной";
            this.dataColumn27.ColumnName = "carid";
            this.dataColumn27.DataType = typeof(int);
            // 
            // dataColumn28
            // 
            this.dataColumn28.Caption = "Водитель";
            this.dataColumn28.ColumnName = "driver_fio";
            // 
            // dataColumn29
            // 
            this.dataColumn29.Caption = "Служба";
            this.dataColumn29.ColumnName = "service_name";
            // 
            // dataColumn30
            // 
            this.dataColumn30.Caption = "Сумма снятия";
            this.dataColumn30.ColumnName = "to_driver_summ";
            this.dataColumn30.DataType = typeof(int);
            // 
            // dataColumn31
            // 
            this.dataColumn31.Caption = "Сумма внесения";
            this.dataColumn31.ColumnName = "from_driver_summ";
            this.dataColumn31.DataType = typeof(int);
            // 
            // dataColumn32
            // 
            this.dataColumn32.Caption = "Дата";
            this.dataColumn32.ColumnName = "tm";
            this.dataColumn32.DataType = typeof(System.DateTime);
            // 
            // dataColumn33
            // 
            this.dataColumn33.Caption = "Тип оплаты";
            this.dataColumn33.ColumnName = "summ_type";
            // 
            // dataColumn34
            // 
            this.dataColumn34.Caption = "Описание";
            this.dataColumn34.ColumnName = "summ_descr";
            // 
            // report2
            // 
            this.report2.ReportResourceString = resources.GetString("report2.ReportResourceString");
            this.report2.RegisterData(this.dataSet1, "dataSet1");
            // 
            // dataColumn35
            // 
            this.dataColumn35.Caption = "Ушло в аренду";
            this.dataColumn35.ColumnName = "to_arenda_summ";
            this.dataColumn35.DataType = typeof(int);
            // 
            // arenda_reports
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(622, 282);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.label_info);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbDrivers);
            this.Controls.Add(this.cmbArendaCars);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Name = "arenda_reports";
            this.Text = "Денежные отчеты";
            this.Load += new System.EventHandler(this.arenda_reports_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.report_data)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.report_params)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.report1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.report2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private SergeUtils.EasyCompletionComboBox cmbDrivers;
        private SergeUtils.EasyCompletionComboBox cmbArendaCars;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Data.DataSet dataSet1;
        private System.Data.DataTable report_data;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private System.Data.DataColumn dataColumn7;
        private System.Data.DataTable report_params;
        private System.Data.DataColumn dataColumn8;
        private System.Data.DataColumn dataColumn9;
        private System.Data.DataColumn dataColumn10;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label_info;
        private System.Data.DataColumn dataColumn11;
        private System.Data.DataColumn dataColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtDataGridViewTextBoxColumn;
        private System.Data.DataColumn dataColumn13;
        private System.Data.DataTable dataTable1;
        private System.Data.DataColumn dataColumn14;
        private System.Data.DataColumn dataColumn15;
        private System.Data.DataColumn dataColumn16;
        private System.Data.DataColumn dataColumn17;
        private System.Data.DataColumn dataColumn18;
        private System.Data.DataColumn dataColumn19;
        private System.Data.DataColumn dataColumn21;
        private System.Data.DataColumn dataColumn22;
        private System.Data.DataColumn dataColumn23;
        private System.Data.DataColumn dataColumn20;
        private System.Data.DataColumn dataColumn24;
        private System.Data.DataColumn dataColumn25;
        private FastReport.Report report1;
        private System.Data.DataColumn dataColumn26;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Data.DataTable dataTable2;
        private System.Data.DataColumn dataColumn27;
        private System.Data.DataColumn dataColumn28;
        private System.Data.DataColumn dataColumn29;
        private System.Data.DataColumn dataColumn30;
        private System.Data.DataColumn dataColumn31;
        private System.Data.DataColumn dataColumn32;
        private System.Data.DataColumn dataColumn33;
        private System.Windows.Forms.DataGridViewTextBoxColumn driverFIODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn payedcarsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn payersDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn payeddaysDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn arendaregsumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn arendaregtypenameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn kassaarendadriverssumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kassaalldriverssumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nppDataGridViewTextBoxColumn;
        private System.Data.DataColumn dataColumn34;
        private FastReport.Report report2;
        private System.Data.DataColumn dataColumn35;

    }
}