﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LumiSoft.Net.Media;
using System.Net.NetworkInformation;
using System.Net;

namespace ONLINEDISPATCHER
{
    public partial class settings : Form
    {
        

        private String m_pAudioOutDevice = "";
        private String m_pAudioInDevice = "";
        private static Boolean m_useAGC= false;

        private static Boolean m_bUseNewOrderForm = false;
        public static Boolean m_bIsDebugSIP = false;
        private static string m_sWeb_proxy = "";
        private static string m_sOsrm_server = "router.project-osrm.org";

        public static Boolean UseNewOrderForm {
            get {
                return m_bUseNewOrderForm;
            }

        }
        public static string WEB_PROXY
        {
            get
            {
                return m_sWeb_proxy;
            }

        }

        public static string OSRM_SERVER
        {
            get
            {
                return m_sOsrm_server;
            }

        }

        public static Boolean useUGC{
            get
            {
                return m_useAGC;
            }

        }


        public int SipPort
        {
            get
            {
                int port = 5600 + MAIN_FORM.OperatorID;

                return port;
            }
        }

        public String SipServer
        {
            get
            {
                return this.sipServerIP.Text;
            }
        }

        public String SipUsername
        {
            get
            {
                return this.sipLogin.Text;
            }
        }

        public String SipPassword
        {
            get
            {
                return "Aa423535" + this.sipLogin.Text;
            }
        }

      

        public String SipLocalIpAddress
        {
            get
            {
                //1. проверить, что указанный адрес находится на данной машине
                //если нет - выдать ошибку и предложить выбрать другой адрес - запустить форму settings

                string sHostName = Dns.GetHostName();
                IPHostEntry ipE = Dns.GetHostByName(sHostName);
                IPAddress[] IpA = ipE.AddressList;
                bool found = false;
                for (int i = 0; i < IpA.Length; i++) {
                    if (IpA[i].ToString().Equals(this.sipLocalIP.Text))
                        found = true;
                    //Console.WriteLine("IP Address {0}: {1} ", i, IpA[i].ToString());
                }

                if (found)
                    return this.sipLocalIP.Text;
                else {
                    MessageBox.Show("Укажите правильный локальный адрес");
                    this.ShowDialog();
                }

                return this.sipLocalIP.Text;

                
            }
        }

        public AudioOutDevice AudioOutDevice
        {
            
            get 
            {
                uint WAVE_MAPPER = unchecked((uint)(-1));
                AudioOutDevice out_device = null;
                out_device = AudioOut.Devices[0];
                //m_pAudioInDevice = AudioIn.Devices[0];

                //out_device = new AudioOutDevice((int)WAVE_MAPPER, "Устройство windows по умолчанию", 1);
/*                
                foreach (AudioOutDevice device in AudioOut.Devices)
                {
                    if (comboAudioOutDevice.Text.CompareTo(device.Name)==0)
                    {
                        out_device = device;
                    }
                }
                if (out_device == null)
                    out_device = AudioOut.Devices[0];

//                 WAVE_MAPPER 
 */
                if (out_device == null)
                    throw new Exception("Нет устройств воспроизведения");
                m_pAudioOutDevice = out_device.Name;  
                return out_device;
            }
        }

        public AudioInDevice AudioInDevice
        {
            get
            {
                uint WAVE_MAPPER = unchecked((uint)(-1));
                AudioInDevice out_device = null;
                out_device = AudioIn.Devices[0];
                //out_device = new AudioInDevice((int)WAVE_MAPPER, "Устройство windows по умолчанию", 1);
/*                
                foreach (AudioInDevice device in AudioIn.Devices)
                {
                    if (comboAudioInDevice.Text.CompareTo(device.Name)==0)
                    {
                        out_device = device;
                    }
                }

                if (out_device == null)
                    out_device = AudioIn.Devices[0];
*/
                if (out_device == null)
                    throw new Exception("Нет устройств записи");
                m_pAudioInDevice = out_device.Name;
                return out_device;
            }
        }

        public settings()
        {
            InitializeComponent();
            //загрузить настройки из файла

            //m_pAudioOutDevice = AudioOut.Devices[0];
            //m_pAudioInDevice = AudioIn.Devices[0];

            try
            {

                if (DataBase.settings.getUserSettingValue("DebugSIP") != null)

                chk_ОтладкаSIP.Checked = bool.Parse(DataBase.settings.getUserSettingValue("DebugSIP"));
                sipServerIP.Text = DataBase.settings.getSettingValue("SIP_SERVER");
                sipPort.Text = DataBase.settings.getUserSettingValue("SIP_USER_PORT");
                sipLogin.Text = DataBase.settings.getSettingValue("SIP_USERNAME");
                sipPassword.Text = DataBase.settings.getSettingValue("SIP_PASSWORD");
                sipLocalIP.Text = DataBase.settings.getSettingValue("SIP_LOCAL_IP");

                String font_params = DataBase.settings.getUserSettingValue("TABLE_FONT");
                String val = DataBase.settings.getUserSettingValue("DISTR_BY_NEW_PROG");
                if (val != null)
                    chkDistrByNewProg.Checked = bool.Parse(val);


                sipLogin.Text = DataBase.mssqlReadString("select cclogin from operators where id = " + MAIN_FORM.OperatorID);

                MAIN_FORM.distr_by_new_prog = chkDistrByNewProg.Checked;

                web_proxy.Text = DataBase.settings.getSharedSettingValue("WEB_PROXY");
                osrm_server.Text = DataBase.settings.getSharedSettingValue("OSRM_SERVER");

                chk_UseNewOSRMProtocol.Checked = bool.Parse(DataBase.settings.getSharedSettingValue("OSRM_USE_NEW_PROTOCOL"));
                

                m_sWeb_proxy = web_proxy.Text;
                m_sOsrm_server = osrm_server.Text;
                csmapcontrol.MapPath.OSRM_SERVER = m_sOsrm_server;
                csmapcontrol.MapPath.USE_NEW_OSRM_PROTOCOL = chk_UseNewOSRMProtocol.Checked;

                m_bIsDebugSIP = chk_ОтладкаSIP.Checked;


                

                val = DataBase.settings.getSharedSettingValue("USE_NEW_ORDER_FORM");
                if (val != null)
                    chkНовоеОкноЗаказа.Checked = bool.Parse(val);

                m_bUseNewOrderForm = chkНовоеОкноЗаказа.Checked;

                



            

                if (font_params != null)
                if (font_params.Contains(":")) {

                    string[] parts = font_params.Split(':');

                    if (parts.Length != 3)
                        throw new ArgumentException("Not a valid font string", "font");

                    Font loadedFont = new Font(parts[0], float.Parse(parts[1]), (FontStyle)int.Parse(parts[2]));
                    Font boldloadedFont = new Font(parts[0], float.Parse(parts[1]), FontStyle.Bold);

                    fontDialog1.Font = loadedFont;

                    this.font_example.Font = loadedFont;
                    MAIN_FORM.table_font = loadedFont;
                    MAIN_FORM.table_bold_font = boldloadedFont;
                }


                String argb = DataBase.settings.getUserSettingValue("COLOR1");
                if (argb != ""){
                    int i_argb = 0;
                    int.TryParse(argb, out i_argb);
                    if (i_argb != 0){
                        btnColor1.BackColor = Color.FromArgb(i_argb);
                        MAIN_FORM.color_1 = btnColor1.BackColor;
                    }
                }

                argb = DataBase.settings.getUserSettingValue("COLOR2");
                if (argb != "") {
                    int i_argb = 0;
                    int.TryParse(argb, out i_argb);
                    if (i_argb != 0) {
                        btnColor2.BackColor = Color.FromArgb(i_argb);
                        MAIN_FORM.color_2 = btnColor2.BackColor;
                    }
                }

                argb = DataBase.settings.getUserSettingValue("COLOR3");
                if (argb != "") {
                    int i_argb = 0;
                    int.TryParse(argb, out i_argb);
                    if (i_argb != 0) {
                        btnColor3.BackColor = Color.FromArgb(i_argb);
                        MAIN_FORM.color_3 = btnColor3.BackColor;
                    }
                }

                argb = DataBase.settings.getUserSettingValue("COLOR4");
                if (argb != "") {
                    int i_argb = 0;
                    int.TryParse(argb, out i_argb);
                    if (i_argb != 0) {
                        btnColor4.BackColor = Color.FromArgb(i_argb);
                        MAIN_FORM.color_4 = btnColor4.BackColor;
                    }
            }


            }
            catch (Exception e)
            {
                System.Console.WriteLine(e.Message);
            }


            


            //if (System.IO.File.Exists("OSM_CLIENT.SET"))
            //{
            //    System.IO.StreamReader file = new System.IO.StreamReader("OSM_CLIENT.SET");
            //    String line = "";
            //    while ((line = file.ReadLine()) != null)
            //    {
            //        if (line.StartsWith("SIP_SERVER="))
            //        {
            //            this.sipServerIP.Text = line.Split('=')[1];
            //        }
            //        if (line.StartsWith("SIP_PORT="))
            //        {
            //            this.sipPort.Text = line.Split('=')[1];
            //        }
            //        if (line.StartsWith("SIP_USERNAME="))
            //        {
            //            this.sipLogin.Text = line.Split('=')[1];
            //        }
            //        if (line.StartsWith("SIP_PASSWORD="))
            //        {
            //            this.sipPassword.Text = line.Split('=')[1];
            //        }
            //        if (line.StartsWith("SIP_LOCAL_IP="))
            //        {
            //            this.sipLocalIP.Text = line.Split('=')[1];
            //        }

            //        if (line.StartsWith("AUDIO_IN_DEVICE="))
            //        {
            //            m_pAudioInDevice = line.Split('=')[1];
            //        }

            //        if (line.StartsWith("AUDIO_OUT_DEVICE="))
            //        {
            //            m_pAudioOutDevice = line.Split('=')[1];
            //        }

            //        if (line.StartsWith("AUDIO_USE_AGC="))
            //        {
            //            m_useAGC = Boolean.Parse(line.Split('=')[1]);
            //        }

            //        if (line.StartsWith("DATABASE_SERVER=")) {
            //            this.dbServerConnectPath.Text = line.Split('=')[1];
            //        }

                    


            //    }

            //    file.Close();
            //}


        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void settings_Activated(object sender, EventArgs e)
        {
            //заполнить поля выбора
            comboAudioOutDevice.Items.Clear();
            comboAudioInDevice.Items.Clear();
            foreach( AudioOutDevice device in AudioOut.Devices)
            {
                comboAudioOutDevice.Items.Add(device.Name);
            }

            foreach( AudioInDevice device in AudioIn.Devices)
            {
                comboAudioInDevice.Items.Add(device.Name);
            }
            //выбрать установленные ранее устройства или первые в списке

            //if (m_pAudioOutDevice == null)
            comboAudioOutDevice.Text = m_pAudioOutDevice;
            comboAudioInDevice.Text = m_pAudioInDevice;

            this.sipLocalIP.Items.Clear();
            for (int i=0; i< System.Net.Dns.GetHostAddresses("").Length; i++)
            {
                if (System.Net.Dns.GetHostAddresses("")[i].AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    this.sipLocalIP.Items.Add(System.Net.Dns.GetHostAddresses("")[i].ToString());
                }
            }

            //m_pAudioOutDevice = AudioOut.Devices[0];
            //m_pAudioInDevice = AudioIn.Devices[0];


          //  m_pPlayer = new WavePlayer(AudioOut.Devices[0]);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            m_pAudioOutDevice = comboAudioOutDevice.Text;
            m_pAudioInDevice = comboAudioInDevice.Text;

            m_useAGC = checkAudioInDevice.Checked;

            m_bIsDebugSIP = chk_ОтладкаSIP.Checked;

            String font_params = fontDialog1.Font.FontFamily.Name + ":" + fontDialog1.Font.Size + ":" + (int)fontDialog1.Font.Style;
            DataBase.settings.saveSettingValue("SIP_SERVER", sipServerIP.Text);
            //DataBase.settings.saveSettingValue("SIP_PORT", sipPort.Text);
           // DataBase.settings.saveSettingValue("SIP_USERNAME", sipLogin.Text);
           // DataBase.settings.saveSettingValue("SIP_PASSWORD", sipPassword.Text);
            DataBase.settings.saveSettingValue("SIP_LOCAL_IP", sipLocalIP.Text);

            DataBase.settings.saveUserSettingValue("DebugSIP", chk_ОтладкаSIP.Checked.ToString());
            
            DataBase.settings.saveUserSettingValue("SIP_USER_PORT", sipPort.Text);
            DataBase.settings.saveUserSettingValue("TABLE_FONT", font_params);
            DataBase.settings.saveUserSettingValue("DISTR_BY_NEW_PROG", chkDistrByNewProg.Checked.ToString());

            DataBase.settings.saveSharedSettingValue("USE_NEW_ORDER_FORM", chkНовоеОкноЗаказа.Checked.ToString());
            DataBase.settings.saveSharedSettingValue("WEB_PROXY", web_proxy.Text);
            DataBase.settings.saveSharedSettingValue("OSRM_SERVER", osrm_server.Text);

            DataBase.settings.saveSharedSettingValue("OSRM_SERVER", osrm_server.Text);
            DataBase.settings.saveSharedSettingValue("OSRM_USE_NEW_PROTOCOL", chk_UseNewOSRMProtocol.Checked.ToString());

            

            

            
            DataBase.mssqlUpdate("operators", "ccLogin", sipLogin.Text, "id", MAIN_FORM.OperatorID); 


            m_sWeb_proxy = web_proxy.Text;
            m_sOsrm_server = osrm_server.Text;

            csmapcontrol.MapPath.OSRM_SERVER = m_sOsrm_server;
            csmapcontrol.MapPath.USE_NEW_OSRM_PROTOCOL = chk_UseNewOSRMProtocol.Checked;

            m_bUseNewOrderForm = chkНовоеОкноЗаказа.Checked;


            if (settings.WEB_PROXY.Length == 0)
            {
                csmapcontrol.MapControl.webProxy = null;
                httpModule.webProxy = null;
            }
            else
            {
                WebProxy myProxy = new WebProxy();
                myProxy.Address = new Uri("http://"+settings.WEB_PROXY);
                csmapcontrol.MapControl.webProxy = myProxy;
                httpModule.webProxy = myProxy;
            }

            



            //System.IO.StreamWriter file = new System.IO.StreamWriter("OSM_CLIENT.SET");
            //file.WriteLine("SIP_SERVER="+sipServerIP.Text);
            //file.WriteLine("SIP_PORT="+sipPort.Text);
            //file.WriteLine("SIP_USERNAME=" + sipLogin.Text);
            //file.WriteLine("SIP_PASSWORD="+sipPassword.Text);
            //file.WriteLine("SIP_LOCAL_IP="+sipLocalIP.Text);

            //file.WriteLine("AUDIO_IN_DEVICE="+m_pAudioInDevice);
            //file.WriteLine("AUDIO_OUT_DEVICE=" + m_pAudioOutDevice);

            //file.WriteLine("AUDIO_USE_AGC=" + m_useAGC.ToString());
            //file.WriteLine("DATABASE_SERVER=" + this.dbServerConnectPath.Text);
            
            //file.Close();

        }

        private void button1_Click(object sender, EventArgs e) {
            fontDialog1.Font = MAIN_FORM.table_font;
            if (fontDialog1.ShowDialog() == DialogResult.OK) {
                font_example.Font = fontDialog1.Font;

                //MAIN_FORM.table_font = fontDialog1.Font;

                String fff = fontDialog1.Font.FontFamily.Name + ":" + fontDialog1.Font.Size + ":" + (int)fontDialog1.Font.Style;

                string[] parts = fff.Split(':');

                if (parts.Length != 3)
                    throw new ArgumentException("Not a valid font string", "font");

                Font loadedFont = new Font(parts[0], float.Parse(parts[1]), (FontStyle)int.Parse(parts[2]));
                Font boldloadedFont = new Font(parts[0], float.Parse(parts[1]), FontStyle.Bold);

                

                MAIN_FORM.table_font = loadedFont;
                MAIN_FORM.table_bold_font = boldloadedFont;
                
                //DataBase.settings.saveSettingValue("SIP_LOCAL_IP", font_example.ToString());    
                

                //Font f = new Font( new FontFamily(font_example.Font.FontFamily.Name), font_example.Font.
                
            }
        }

        private void button4_Click(object sender, EventArgs e) {
            colorDialog1.Color = btnColor1.BackColor;
            if (colorDialog1.ShowDialog() == DialogResult.OK) {
                btnColor1.BackColor = colorDialog1.Color;

                DataBase.settings.saveUserSettingValue("COLOR1", colorDialog1.Color.ToArgb().ToString());

                MAIN_FORM.color_1 = colorDialog1.Color;


            }
        }

        private void btnColor2_Click(object sender, EventArgs e) {
            colorDialog1.Color = btnColor1.BackColor;
            if (colorDialog1.ShowDialog() == DialogResult.OK) {
                btnColor2.BackColor = colorDialog1.Color;

                DataBase.settings.saveUserSettingValue("COLOR2", colorDialog1.Color.ToArgb().ToString());

                MAIN_FORM.color_2 = colorDialog1.Color;


            }
        }

        private void btnColor3_Click(object sender, EventArgs e) {
            colorDialog1.Color = btnColor1.BackColor;
            if (colorDialog1.ShowDialog() == DialogResult.OK) {
                btnColor3.BackColor = colorDialog1.Color;

                DataBase.settings.saveUserSettingValue("COLOR3", colorDialog1.Color.ToArgb().ToString());

                MAIN_FORM.color_3 = colorDialog1.Color;


            }
        }

        private void btnColor4_Click(object sender, EventArgs e) {
            colorDialog1.Color = btnColor1.BackColor;
            if (colorDialog1.ShowDialog() == DialogResult.OK) {
                btnColor4.BackColor = colorDialog1.Color;

                DataBase.settings.saveUserSettingValue("COLOR4", colorDialog1.Color.ToArgb().ToString());

                MAIN_FORM.color_4 = colorDialog1.Color;


            }
        }

        private void chkDistrByNewProg_CheckedChanged(object sender, EventArgs e)
        {
            MAIN_FORM.distr_by_new_prog = chkDistrByNewProg.Checked;
        }

        private void chkНовоеОкноЗаказа_CheckedChanged(object sender, EventArgs e) {
            m_bUseNewOrderForm = chkНовоеОкноЗаказа.Checked;
        }
    }
}
