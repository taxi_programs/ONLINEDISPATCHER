﻿namespace ONLINEDISPATCHER
{
    partial class dictionary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dictionary));
            this.btnRefresh = new System.Windows.Forms.Button();
            this.topNumber = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_oranization_subdetail = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnAkts = new System.Windows.Forms.Button();
            this.btnOrganizationPayments = new System.Windows.Forms.Button();
            this.btnOrganizationDetails = new System.Windows.Forms.Button();
            this.btnNewElement = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.dataSet1 = new System.Data.DataSet();
            this.driver_card = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.report1 = new FastReport.Report();
            this.report2 = new FastReport.Report();
            this.report3 = new FastReport.Report();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.driver_card)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.report1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.report2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.report3)).BeginInit();
            this.SuspendLayout();
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(1, 1);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 0;
            this.btnRefresh.Text = "Обновить";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // topNumber
            // 
            this.topNumber.Location = new System.Drawing.Point(82, 3);
            this.topNumber.Name = "topNumber";
            this.topNumber.Size = new System.Drawing.Size(44, 20);
            this.topNumber.TabIndex = 2;
            this.topNumber.Text = "5000";
            this.topNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridView1.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.HighlightText;
            this.dataGridView1.Location = new System.Drawing.Point(0, 58);
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView1.RowTemplate.Height = 18;
            this.dataGridView1.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(858, 366);
            this.dataGridView1.TabIndex = 54;
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btn_oranization_subdetail);
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.btnAkts);
            this.panel1.Controls.Add(this.btnOrganizationPayments);
            this.panel1.Controls.Add(this.btnOrganizationDetails);
            this.panel1.Controls.Add(this.btnNewElement);
            this.panel1.Controls.Add(this.btnEdit);
            this.panel1.Controls.Add(this.btnRefresh);
            this.panel1.Controls.Add(this.topNumber);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(858, 58);
            this.panel1.TabIndex = 55;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // btn_oranization_subdetail
            // 
            this.btn_oranization_subdetail.Location = new System.Drawing.Point(382, 30);
            this.btn_oranization_subdetail.Name = "btn_oranization_subdetail";
            this.btn_oranization_subdetail.Size = new System.Drawing.Size(151, 23);
            this.btn_oranization_subdetail.TabIndex = 9;
            this.btn_oranization_subdetail.Text = "Сотрудники отдела";
            this.btn_oranization_subdetail.UseVisualStyleBackColor = true;
            this.btn_oranization_subdetail.Visible = false;
            this.btn_oranization_subdetail.Click += new System.EventHandler(this.btn_oranization_subdetail_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(690, 3);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 8;
            this.btnDelete.Text = "Удалить";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Visible = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAkts
            // 
            this.btnAkts.Location = new System.Drawing.Point(609, 3);
            this.btnAkts.Name = "btnAkts";
            this.btnAkts.Size = new System.Drawing.Size(75, 23);
            this.btnAkts.TabIndex = 7;
            this.btnAkts.Text = "Акты";
            this.btnAkts.UseVisualStyleBackColor = true;
            this.btnAkts.Visible = false;
            this.btnAkts.Click += new System.EventHandler(this.btnAkts_Click);
            // 
            // btnOrganizationPayments
            // 
            this.btnOrganizationPayments.Location = new System.Drawing.Point(528, 3);
            this.btnOrganizationPayments.Name = "btnOrganizationPayments";
            this.btnOrganizationPayments.Size = new System.Drawing.Size(75, 23);
            this.btnOrganizationPayments.TabIndex = 6;
            this.btnOrganizationPayments.Text = "Платежки";
            this.btnOrganizationPayments.UseVisualStyleBackColor = true;
            this.btnOrganizationPayments.Visible = false;
            this.btnOrganizationPayments.Click += new System.EventHandler(this.btnOrganizationPayments_Click);
            // 
            // btnOrganizationDetails
            // 
            this.btnOrganizationDetails.Location = new System.Drawing.Point(424, 3);
            this.btnOrganizationDetails.Name = "btnOrganizationDetails";
            this.btnOrganizationDetails.Size = new System.Drawing.Size(98, 23);
            this.btnOrganizationDetails.TabIndex = 5;
            this.btnOrganizationDetails.Text = "Сотрудники";
            this.btnOrganizationDetails.UseVisualStyleBackColor = true;
            this.btnOrganizationDetails.Visible = false;
            this.btnOrganizationDetails.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnNewElement
            // 
            this.btnNewElement.Location = new System.Drawing.Point(167, 2);
            this.btnNewElement.Name = "btnNewElement";
            this.btnNewElement.Size = new System.Drawing.Size(100, 24);
            this.btnNewElement.TabIndex = 4;
            this.btnNewElement.Text = "Новый элемент";
            this.btnNewElement.UseVisualStyleBackColor = true;
            this.btnNewElement.Click += new System.EventHandler(this.btnNewElement_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(305, 3);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 23);
            this.btnEdit.TabIndex = 3;
            this.btnEdit.Text = "Редактировать";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 424);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(858, 22);
            this.statusStrip1.TabIndex = 56;
            this.statusStrip1.Text = "statusStrip1sf";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "NewDataSet";
            this.dataSet1.Tables.AddRange(new System.Data.DataTable[] {
            this.driver_card});
            // 
            // driver_card
            // 
            this.driver_card.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn8,
            this.dataColumn5,
            this.dataColumn6});
            this.driver_card.TableName = "driver_card";
            // 
            // dataColumn1
            // 
            this.dataColumn1.ColumnName = "fio";
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "carid";
            this.dataColumn2.DataType = typeof(int);
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "photo";
            this.dataColumn3.DataType = typeof(byte[]);
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "PHONES";
            // 
            // dataColumn8
            // 
            this.dataColumn8.ColumnName = "carname";
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "phone2";
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "phone3";
            // 
            // report1
            // 
            this.report1.ReportResourceString = resources.GetString("report1.ReportResourceString");
            this.report1.RegisterData(this.dataSet1, "dataSet1");
            // 
            // report2
            // 
            this.report2.ReportResourceString = resources.GetString("report2.ReportResourceString");
            this.report2.RegisterData(this.dataSet1, "dataSet1");
            // 
            // report3
            // 
            this.report3.ReportResourceString = resources.GetString("report3.ReportResourceString");
            this.report3.RegisterData(this.dataSet1, "dataSet1");
            // 
            // dictionary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(858, 446);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.panel1);
            this.Name = "dictionary";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "dictionary";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.driver_card)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.report1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.report2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.report3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.TextBox topNumber;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnNewElement;
        private System.Windows.Forms.Button btnOrganizationDetails;
        private System.Windows.Forms.Button btnOrganizationPayments;
        private System.Data.DataSet dataSet1;
        private System.Data.DataTable driver_card;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private FastReport.Report report1;
        private FastReport.Report report2;
        private System.Data.DataColumn dataColumn4;
        private FastReport.Report report3;
        private System.Data.DataColumn dataColumn8;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private System.Windows.Forms.Button btnAkts;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btn_oranization_subdetail;
    }
}