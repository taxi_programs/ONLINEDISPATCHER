//
// csmapcontrol - a C# map control - http://projects.ciarang.com/p/csmapcontrol
// Copyright (C) 2010, Ciaran Gultnieks
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>. 

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.IO;
using System.Resources;
using System.Reflection;




namespace csmapcontrol
{

	

	/// <summary>
	/// Map control
	/// </summary>
	public partial class MapControl : UserControl
	{

		/// <summary>
		/// A predetermined list of sensible (-ish) discrete scales that can be
		/// used, for example, to provide a linear zoom range for a scrollbar.
		/// </summary>
		public static double[] DiscreteScales={
			0.00005/256,
			0.0005/256,
			0.0008/256,			
			0.002/256,
			0.002/256,
			0.003/256,
			0.004/256,
			0.005/256,
			0.006/256,
			0.007/256,
			0.008/256,		
			0.009/256,
			0.01/256,
			0.015/256,
			0.02/256,
			0.025/256,
			0.03/256,
			0.035/256,
			0.04/256,
			0.045/256,
			0.05/256,
			0.055/256,
			0.06/256,
			0.065/256,
			0.07/256,
			0.075/256,
			0.08/256,
			0.085/256,
			0.09/256,
			0.095/256,
			0.1/256,
			0.2/256,
			0.3/256,
			0.4/256,
			0.5/256,
			0.5/256,
			0.6/256,
			0.7/256,
			0.8/256,
			0.9/256,
			1.0/256,
			2.0/256,
			3.0/256,
			4.0/256,
			5.0/256,
			6.0/256,
			7.0/256,
			8.0/256,
			9.0/256,
			10.0/256,
			15.0/256,
			20.0/256,
			30.0/256,
			40.0/256,
			50.0/256,
			60.0/256,
			70.0/256,
			80.0/256,
			90.0/256,
			100.0/256,
			110.0/256,
			130.0/256,
			150.0/256,
			170.0/256,
			190.0/256,
			210.0/256,
			240.0/256,
			1};
		
		public List<Marker> Markers
		{
			get { return mMarkers; }
		}
		private List<Marker> mMarkers=new List<Marker>();

        public List<Marker> addressMarkers {
            get { return maddressMarkers; }
        }
        private List<Marker> maddressMarkers = new List<Marker>();

        public List<Marker> carsMarkers {
            get { return mcarsMarkers; }
        }
        private List<Marker> mcarsMarkers = new List<Marker>();



        public static bool PointInZonePolygon(Marker Test, ZoneRegion polygon)
        {
            bool result = false;

            int i, j, nvert = polygon.zoneMarkers.Count;
            for (i = 0, j = nvert - 1; i < nvert; j = i++)
                if (((polygon.zoneMarkers[i].lon > Test.lon) != (polygon.zoneMarkers[j].lon > Test.lon)) &&
                   (Test.lat < (polygon.zoneMarkers[j].lat - polygon.zoneMarkers[i].lat) *
                   (Test.lon - polygon.zoneMarkers[i].lon) / (polygon.zoneMarkers[j].lon - polygon.zoneMarkers[i].lon) + polygon.zoneMarkers[i].lat))
                    result = !result;
            return result;
        }
        


        public class ZoneRegion {

            //object fields
            public int zone_id = 0; //присвоенный код.
            public String zone_name = "";
            public int ID = 0;  //автомарически присваемый код, номер по порядку в списке.
            public int remote_zone = 0;
            public List<Marker> zoneMarkers = new List<Marker>();
            public bool hightLighted = false;


            //public List<List<Marker>> zoneMarkers = new List<List<Marker>>();
            public void AddZoneRegionMarker(Marker m) {
                zoneMarkers.Add(m);
            }



            //static fields - for list of objects

            public static List<ZoneRegion> list_zone_region = new List<ZoneRegion>();

            public static void doDeleteZoneRegionMarker(Marker m)
            {
                for (int i = 0; i < list_zone_region.Count; i++)
                {
                    ZoneRegion zr = list_zone_region[i];
                    zr.zoneMarkers.Remove(m);
                }
            }

            public static ZoneRegion AddZoneRegion()
            {
                ZoneRegion r = new ZoneRegion();
                list_zone_region.Add(r);
                r.ID = list_zone_region.Count;
                
                return r;
            }

            

            
            
        }


      //  public Marker markerA = null;
      //  public Marker markerB = null;


        




        private Dictionary<string, Image> mMarkerImages = new Dictionary<string, Image>();

        
/*        
        public class MarkerCar
        {
            //779||7-ка  1991г.в. 779|57690738|39805210

            private String convertCoord(String coord) {
                try
                {
                    String grad = coord.Substring(0, 2);
                    String min = coord.Substring(2);

                    Double mm = Double.Parse(min);

                    mm = mm * 100 / 60;
                    coord = grad + mm.ToString().Substring(0, 6);
                }
                catch (Exception e)
                {
                }

                return coord ;
            }


            public MarkerCar(String car_data, String _status)
            {
                String[] s_arr = car_data.Split('|');
                if (s_arr.Length == 5)
                {
                    id = s_arr[0];
                    String program_type =  s_arr[4];
                    //String android_ids = "|1|299|390|45|266|";
                    name = s_arr[1];

                    if (program_type=="1") {
                        //s_arr[2] = convertCoord(s_arr[2]);
                        //s_arr[3] = convertCoord(s_arr[3]);
                    }

                    Double.TryParse(s_arr[2], out lat);
                    Double.TryParse(s_arr[3], out lon);
                    lat = lat / 1000000;
                    lon = lon / 1000000;
                    status = _status;
                }
            }
            public double lat = 0;
            public double lon = 0;
            public String name = "";
            public String id = "";
            public String status = "";

        }
        private Dictionary<string, MarkerCar> mMarkerCars = new Dictionary<string, MarkerCar>();
        public Dictionary<string, MarkerCar> MarkersCars
        {
            get { return mMarkerCars; }
        }


*/



		public List<Track> Tracks
		{
			get { return mTracks; }
		}
		private List<Track> mTracks=new List<Track>();

        public List<MapPath> Paths {
            get { return mPaths; }
        }
        private List<MapPath> mPaths = new List<MapPath>();


        public static MapPath ORDER_CALCULATED_PATH = new MapPath();
        
		
		/// <summary>
		/// Latitude at centre of view
		/// </summary>
		public double Latitude
		{
			get { return mLat; }
			set { mLat=value; Invalidate(); }
		}
		private double mLat=53.9753;

		/// <summary>
		/// Longitude at centre of view
		/// </summary>
		public double Longitude
		{
			get { return mLon; }
			set { mLon=value; Invalidate(); }
		}
		private double mLon=-1.7017;



/*add by programmer76*/

        public static  string GetHtmlPageText(string url, int timeout) {
            string result = String.Empty;





            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            if (MapControl.webProxy != null)
                request.Proxy = MapControl.webProxy;
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.52 Safari/537.17";
            request.Timeout = timeout;
            try {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse()) {
                    using (Stream dataStream = response.GetResponseStream()) {
                        using (StreamReader reader = new StreamReader(dataStream)) {
                            result = reader.ReadToEnd();
                        }
                    }
                }
            } catch (WebException e) {
                
                System.Console.WriteLine("Ошибка получения маршрута. " + e.Message);
            }

            return result;
        }        


        public String CachePath
        {
            get { return mTileManager.DiskCachePath; }
            set { mTileManager.DiskCachePath = value; }
        }

        public int CacheSize
        {
            get { return mTileManager.CacheSize; }
        }
        private static WebProxy mWebProxy = null;
        public static WebProxy webProxy
        {
            get {return  mWebProxy;}
            set{ mWebProxy = value;}
        }
        

		/// <summary>
		/// Scale - longitude degrees per pixel
		/// </summary>
		public double LonScale
		{
			get { return mScale; }
			set { mScale=value; Invalidate(); }
		}
		private double mScale=1.0/256;

		public bool HighlightEntities
		{
			get { return mHighlightEntities; }
			set { mHighlightEntities=value; Invalidate(); }
		}
		private bool mHighlightEntities=false;

		private EntityInfo mHighlightedEntity=null;

        public void doDeleteHightLitedMarker()
        {
            if (lastZoneMarker == null)
                return;

            ZoneRegion.doDeleteZoneRegionMarker(lastZoneMarker);

            ORDER_CALCULATED_PATH.RemoveInterPoint(lastZoneMarker);

            lastZoneMarker = null;
            Invalidate();



        }

		/// <summary>
		/// Tile cache
		/// </summary>
		private static TileManager mTileManager=new TileManager();

		private bool mUpdateView=false;

		/// <summary>
		/// Event raised when the view parameters are changed INTERNALLY.
		/// For example, by the user dragging the map. This event is not raised
		/// when the change is made by the client, e.g. by setting the relevant
		/// properties.
		/// </summary>
		public event ViewChangedEventHandler ViewChanged;
		public delegate void ViewChangedEventHandler(double lat,double lon,double scale);

        public delegate void MarkerHighlited(object sender, Marker marker);
        public event MarkerHighlited onMarkerHighlited;

        private void FireonMarkerHighlited(Marker m) {
            if (onMarkerHighlited == null) return;
            if (m == null) return;
            onMarkerHighlited(this, m);
        }



		private void RaiseViewChanged()
		{
			if(ViewChanged!=null)
			{
				ViewChanged(mLat,mLon,mScale);
			}
		}

		/// <summary>
		/// Event raised when there is diagnostics information available.
		/// </summary>
		public event DiagsInfoEventHandler DiagsInfo;
		public delegate void DiagsInfoEventHandler(string sMsg);
		private void Diags(string msg)
		{
			if(DiagsInfo!=null)
				DiagsInfo(msg);
		}

		/// <summary>
		/// Event raised when the user clicks at a location on the map.
		/// </summary>
		public event LocationClickedEventHandler LocationClicked;
		public delegate void LocationClickedEventHandler(double lat,double lon);

		/// <summary>
		/// Event raised when the user clicks a Marker.
		/// </summary>
		public event MarkerClickedEventHandler MarkerClicked;
		public delegate void MarkerClickedEventHandler(Marker m);

		/// <summary>
		/// Event raised when the user clicks a Track point.
		/// </summary>
		public event TrackPointClickedEventHandler TrackPointClicked;
		public delegate void TrackPointClickedEventHandler(Track track,DateTime time);


        System.Drawing.Font drawFont = new System.Drawing.Font("Arial", 8);
        System.Drawing.Font drawFontMedium = new System.Drawing.Font("Arial", 10);
        System.Drawing.Font drawFontLarge = new System.Drawing.Font("Arial", 20);
        System.Drawing.SolidBrush drawBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Black);
        System.Drawing.SolidBrush drawGreenBrush = new System.Drawing.SolidBrush(System.Drawing.Color.LightGreen);
        System.Drawing.SolidBrush drawRedBrush = new System.Drawing.SolidBrush(System.Drawing.Color.LightPink);
        System.Drawing.SolidBrush drawBlueBrush = new System.Drawing.SolidBrush(System.Drawing.Color.LightBlue);
        System.Drawing.SolidBrush drawGrayBrush = new System.Drawing.SolidBrush(System.Drawing.Color.LightGray);

        static Color halfRedbrushColor = Color.FromArgb(250 / 100 * 25, 255, 0, 0);
        static System.Drawing.SolidBrush halfRedbrush = new SolidBrush(halfRedbrushColor);
        static Color halfBluebrushColor = Color.FromArgb(250 / 100 * 25, 0, 0, 255);
        static System.Drawing.SolidBrush halfBluebrush = new SolidBrush(halfBluebrushColor);
        static Color halfGreenbrushColor = Color.FromArgb(250 / 100 * 25, 0, 255, 0);
        static System.Drawing.SolidBrush halfGreenbrush = new SolidBrush(halfGreenbrushColor);
        static Color halfMagentabrushColor = Color.FromArgb(250 / 100 * 25, 255, 0, 255);
        static System.Drawing.SolidBrush halfMagentabrush = new SolidBrush(halfMagentabrushColor);
        static Color halfYellowbrushColor = Color.FromArgb(250 / 100 * 25, 0, 255, 255);
        static System.Drawing.SolidBrush halfYellowbrush = new SolidBrush(halfYellowbrushColor);




        
        System.Drawing.StringFormat drawFormat = new System.Drawing.StringFormat();
        //drawFormat.FormatFlags = StringFormatFlags.DirectionVertical;
        
    



		/// <summary>
		/// Clear the tile cache. Since the tile manager is static, and shared
		/// between all instances of the control, it will remain in place for
		/// the lifetime of the application unless this is called.
		/// </summary>
		public static void ClearTileCache()
		{
			if(mTileManager!=null)
				mTileManager.EmptyCache();			
		}

        public static void EmptyHalfTileCache()
        {
            if (mTileManager != null)
                mTileManager.EmptyHalfCache();
        }

		public MapControl()
		{
			InitializeComponent();
			mTileManager.NewDataAvailable+=NewTileDataAvailable;



            //загрузить иконки для маркеров
            Image im = Image.FromStream(Assembly.GetExecutingAssembly().GetManifestResourceStream("MapControl.contact_blue.png"),true);
            this.mMarkerImages.Add("from", im);
            im = Image.FromStream(Assembly.GetExecutingAssembly().GetManifestResourceStream("MapControl.okay.png"),true);
            this.mMarkerImages.Add("to", im);
            im = Image.FromStream(Assembly.GetExecutingAssembly().GetManifestResourceStream("MapControl.gem_options.png"),true);
            this.mMarkerImages.Add("over", im);
            im = Image.FromStream(Assembly.GetExecutingAssembly().GetManifestResourceStream("MapControl.pin-blue.png"), true);
            this.mMarkerImages.Add("pin-blue", im);
            im = Image.FromStream(Assembly.GetExecutingAssembly().GetManifestResourceStream("MapControl.pin-green.png"), true);
            this.mMarkerImages.Add("pin-green", im);
            im = Image.FromStream(Assembly.GetExecutingAssembly().GetManifestResourceStream("MapControl.finish.png"), true);
            this.mMarkerImages.Add("finish", im);

            
		}

		/// <summary>
		/// Flag that the view needs updating. Calling this will cause a call
		/// to Invalidate() at the next timer tick. We need to go down this
		/// convoluted route rather than simply calling Invalidate() from
		/// the NewTileDataAvailable event handler, because that could cause
		/// it to be called when a paint is still in progress, and this means
		/// the Invalidate gets ignored.
		/// </summary>
		private void UpdateView()
		{
			mUpdateView=true;
		}

		private void NewTileDataAvailable()
		{
			if(!this.IsHandleCreated)
				return;
			if(InvokeRequired)
			{
				BeginInvoke(new MethodInvoker(delegate
					{ UpdateView(); }));
			}
			else
			{
				UpdateView();
			}
		}

		/// <summary>
		/// Get the Tile X/Y position for a given latitude and longitude, and
		/// at a given zoom level.
		/// </summary>
		/// <param name="lat">The latitude</param>
		/// <param name="lon">The longitude</param>
		/// <param name="zoom">The zoom level</param>
		/// <param name="x">The tile X</param>
		/// <param name="y">The tile Y</param>
		private void GetTileXYFromLatLon(double lat,double lon,int zoom,out int x,out int y)
		{
			x=(int)((lon+180.0)/360.0*Math.Pow(2.0,zoom));
			y=(int)((1.0-Math.Log(Math.Tan(lat*Math.PI/180.0)+
				1.0/Math.Cos(lat*Math.PI/180.0))/Math.PI)/2.0*Math.Pow(2.0,zoom));
		}

		/// <summary>
		/// Get the latitude and longitude for the tile at the given X/Y and
		/// zoom. The lat/lon returned is for the top left corner of the tile.
		/// </summary>
		/// <param name="x">The tile X</param>
		/// <param name="y">The tile Y</param>
		/// <param name="zoom">The zoom level</param>
		/// <param name="lat">The latitude</param>
		/// <param name="lon">The longitude</param>
		private void GetLatLonFromTileXY(int x,int y,int zoom,out double lat,out double lon)
		{
			lon=((x/Math.Pow(2.0,zoom)*360.0)-180.0);
			double n=Math.PI-((2.0*Math.PI*y)/Math.Pow(2.0,zoom));
			lat=(180.0/Math.PI*Math.Atan(Math.Sinh(n)));
		}

		/// <summary>
		/// Get the lat/lon bounds of the current display.
		/// </summary>
		/// <param name="lat1"></param>
		/// <param name="lon1"></param>
		/// <param name="lat2"></param>
		/// <param name="lon2"></param>
		public void GetDisplayBounds(out double lat1,out double lon1,out double lat2,out double lon2)
		{
			ClientPosToLatLon(0,0,out lat1,out lon1);
			ClientPosToLatLon(Width-1,Height-1,out lat2,out lon2);
		}

		/// <summary>
		/// Get the latitude and longitude of a given position in the client
		/// area.
		/// </summary>
		/// <param name="x">The X position</param>
		/// <param name="y">The Y position</param>
		/// <param name="lat">The latitude</param>
		/// <param name="lon">The longitude</param>
		public void ClientPosToLatLon(int x,int y,out double lat,out double lon)
		{

			CalcGeometry();

			if(mGI.ViewTileSizePixels==0)
			{
				// We can't calculate it!
				lat=lon=0;
				return;
			}

			lon=mLon+mScale*(x-Width/2);

			double offset=(double)(y-mGI.ViewYTileYPos)/mGI.ViewTileSizePixels;
			double ty=mGI.CentreTileNumY+offset;
			double n=Math.PI-((2.0*Math.PI*ty)/Math.Pow(2.0,mGI.ViewTileZoom));
			lat=(180.0/Math.PI*Math.Atan(Math.Sinh(n)));
		}

		/// <summary>
		/// Get the view position in the client area of the given latitude and
		/// longitude.
		/// </summary>
		/// <param name="lat">The latitude</param>
		/// <param name="lon">The longitude</param>
		/// <param name="x">The X position</param>
		/// <param name="y">The Y position</param>
		private void LatLonToClientPos(double lat,double lon,out int x,out int y)
		{
			CalcGeometry();
			x=Width/2+(int)((lon-mLon)/mScale);
			double ty=((1.0-Math.Log(Math.Tan(lat*Math.PI/180.0)+
				1.0/Math.Cos(lat*Math.PI/180.0))/Math.PI)/2.0*Math.Pow(2.0,mGI.ViewTileZoom));
			y=mGI.ViewYTileYPos+(int)((ty-mGI.CentreTileNumY)*mGI.ViewTileSizePixels);
		}

		/// <summary>
		///Used to store calculated view geometry information. 
		/// </summary>
		private class GeometryInfo
		{
			// These five fields record the parameters used to calculate
			// the information...
			public int Width=-1;
			public int Height;
			public double Lat;
			public double Lon;
			public double Scale;

			// The remaining fields are the calculated information...
			public int CentreTileNumX;	// Tile number (X) in centre
			public int CentreTileNumY;	// Tile number (Y) in centre
			public int ViewYTileYPos;
			public int ViewTileSizePixels;
			public int ViewTileZoom;
			public double CentreTileLonT;
			public double CentreTileLonB;

		}
		private GeometryInfo mGI=new GeometryInfo();

		private void CalcGeometry()
		{

			// We don't need to do anything if the information we already have
			// is valid for the current view...			
			if(Width==mGI.Width &&
				Height==mGI.Height &&
				mLat==mGI.Lat &&
				mLon==mGI.Lon &&
				mScale==mGI.Scale)
				return;

			mGI.Width=Width;
			mGI.Height=Height;
			mGI.Lat=mLat;
			mGI.Lon=mLon;
			mGI.Scale=mScale;

			// Figure out what tile zoom we want...
			// We want this many degrees longitude per tile...
			double lonpertilewanted=256*mScale;
			// Meaning we want this many tiles across in total.
			int tilesnumwanted=(int)(360.0/lonpertilewanted);
			// Which corresponds to this zoom level...
			int tilezoom=(int)(Math.Log(tilesnumwanted)/Math.Log(2));
			if(tilezoom>18)
				tilezoom=18;
			mGI.ViewTileZoom=tilezoom;

			// Get the tile x/y at the centre of the view...
			GetTileXYFromLatLon(mLat,mLon,tilezoom,out mGI.CentreTileNumX,out mGI.CentreTileNumY);

			// Get the lat lon at the top left corner of that tile...
			double lat,lon;
			GetLatLonFromTileXY(mGI.CentreTileNumX,mGI.CentreTileNumY,tilezoom,out lat,out lon);
			mGI.CentreTileLonT=lon;

			// Get the lat lon at the bottom left corner as well...
			double lat2,lon2;
			GetLatLonFromTileXY(mGI.CentreTileNumX+1,mGI.CentreTileNumY+1,tilezoom,out lat2,out lon2);
			mGI.CentreTileLonB=lon2;

			// Calculate longitude degrees per tile...
			double lonpertile=360.0/(Math.Pow(2,tilezoom));

			// Calculate longitude degrees per pixel on the tile...
			double lonpertilepixel=lonpertile/256;

			// Calculate the size (pixels) that tiles will be scaled to
			// when drawn...
			mGI.ViewTileSizePixels=(int)(256*lonpertilepixel/mScale);

			mGI.ViewYTileYPos=(int)(Height/2-((double)mGI.ViewTileSizePixels*((mLat-lat)/(lat2-lat))));

		}

		protected override void OnPaint(PaintEventArgs e)
		{
            
			base.OnPaint(e);

			if(!DesignMode)
			{
				try
				{

					CalcGeometry();

					// Find where to draw the centre tile...
					int drawx,drawy;
					drawx=(int)(Width/2-((double)mGI.ViewTileSizePixels*((mLon-mGI.CentreTileLonT)/(mGI.CentreTileLonB-mGI.CentreTileLonT))));
					drawy=mGI.ViewYTileYPos;

					int x=mGI.CentreTileNumX;
					int y=mGI.CentreTileNumY;

					// Move the position back to the top-leftmost tile we need to
					// draw...
					while(drawx>=0)
					{
						drawx-=mGI.ViewTileSizePixels;
						x--;
					}
					while(drawy>=0)
					{
						drawy-=mGI.ViewTileSizePixels;
						y--;
					}
	
					// Draw all the tiles...
					int curdrawx;
					int curx;
					while(drawy<Height)
					{
						curdrawx=drawx;
						curx=x;
						while(curdrawx<Width)
						{
							Image img=mTileManager.GetTile(curx,y,mGI.ViewTileZoom);
							if(img!=null)
								e.Graphics.DrawImage(img,curdrawx,drawy,
									mGI.ViewTileSizePixels+1,mGI.ViewTileSizePixels+1);
							curdrawx+=mGI.ViewTileSizePixels;
							curx++;
						}
						drawy+=mGI.ViewTileSizePixels;
						y++;
					}

					// Draw markers...
					foreach(Marker m in mMarkers)
					{
						int mx,my;
						LatLonToClientPos(m.lat,m.lon,out mx,out my);
						Pen pen=new Pen(Color.DeepPink, 3);
						//e.Graphics.DrawEllipse(pen,mx-3,my-3,7,7);
                        e.Graphics.DrawEllipse(pen, mx - 3, my - 3, 7, 7);
                        e.Graphics.DrawString(m.Tag.ToString(), drawFont, drawBrush, mx, my);
						pen.Dispose();
					}

                    foreach (Marker m in maddressMarkers)
					{
						int mx,my;
						LatLonToClientPos(m.lat,m.lon,out mx,out my);
						Pen pen=new Pen(Color.Red, 4);
						//e.Graphics.DrawEllipse(pen,mx-3,my-3,7,7);
                        e.Graphics.DrawEllipse(pen, mx - 3, my - 3, 7, 7);
                        e.Graphics.DrawString(m.Tag.ToString(), drawFont, drawBrush, mx, my);
						pen.Dispose();
					}

                    foreach (Marker m in mcarsMarkers) {
                        int mx, my;
                        LatLonToClientPos(m.lat, m.lon, out mx, out my);
                        Pen pen = new Pen(Color.Red, 4);
                        //e.Graphics.DrawEllipse(pen,mx-3,my-3,7,7);
//                        e.Graphics.DrawEllipse(pen, mx - 3, my - 3, 7, 7);
//                        e.Graphics.DrawString(m.Tag.ToString(), drawFont, drawBrush, mx, my);

                        car_data cd = (car_data)m.Tag;



                        SizeF tsize = e.Graphics.MeasureString(cd.car_id, drawFont);
                        if (cd.car_state == 7)
                            e.Graphics.FillRectangle(drawRedBrush, mx, my, tsize.Width, tsize.Height);
                        else
                        if (cd.car_state == 8)
                            e.Graphics.FillRectangle(drawBlueBrush, mx, my, tsize.Width, tsize.Height);
                        else
                        if (cd.car_state == 4)
                            e.Graphics.FillRectangle(drawGrayBrush, mx, my, tsize.Width, tsize.Height);
                        else
                            e.Graphics.FillRectangle(drawGreenBrush, mx, my, tsize.Width, tsize.Height);

                            
                        e.Graphics.DrawString(cd.car_id, drawFont, drawBrush, mx, my);


                        pen.Dispose();
                    }


                    
                    // Draw markers...
/*                    foreach (MarkerCar m in mMarkerCars.Values)
                    {
                        int mx, my;
                        LatLonToClientPos(m.lat, m.lon, out mx, out my);
                        //Pen pen = new Pen(Color.Red);
                        SizeF tsize = e.Graphics.MeasureString(m.id, drawFont);
                        if (m.status=="4")
                            e.Graphics.FillRectangle(drawRedBrush, mx, my, tsize.Width, tsize.Height);
                        else
                            e.Graphics.FillRectangle(drawGreenBrush, mx, my, tsize.Width, tsize.Height);
                        e.Graphics.DrawString(m.id, drawFont, drawBrush, mx, my);
                        //pen.Dispose();
                    }
 */ 

					// Draw tracks...
					foreach(Track t in mTracks)
					{
						int px=0,py=0;	// Initialised only to pacify the compiler
						int tx,ty;
						bool first=true;
                        Pen greenPen = new Pen(Color.Green, 2);
                        Pen bluePen=new Pen(Color.Blue, 2);
                        Pen redPen = new Pen(Color.Red, 3);
                        Pen yellowPen = new Pen(Color.Yellow, 3);
                        Pen blackPen = new Pen(Color.Black, 4);
                        Pen workPen = bluePen;
						foreach(TrackPoint p in t.Points)
						{
							LatLonToClientPos(p.Lat,p.Lon,out tx,out ty);
							if(!first)
							{
                                if (p.speed < 60) workPen = greenPen;
                                else
                                    if (p.speed < 80) workPen = bluePen;
                                    else
                                        if (p.speed < 100) workPen = yellowPen;
                                        else
                                            if (p.speed < 120) workPen = redPen;
                                            else
                                                if (p.speed > 120) workPen = blackPen;

                                e.Graphics.DrawLine(workPen, new Point(px, py),
								                    	new Point(tx,ty));
							}
							px=tx;
							py=ty;
							first=false;
						}
                        bluePen.Dispose();
                        greenPen.Dispose();
                        redPen.Dispose();
					}

                    // Draw Paths...
                    foreach (MapPath path in mPaths) {
                        int px = 0, py = 0;	// Initialised only to pacify the compiler
                        int tx, ty;
                        bool first = true;
                        Pen pen = new Pen(Color.Magenta, 3);
                        e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                        if (this.LonScale < MapControl.DiscreteScales[10])
                            pen.EndCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;
                        foreach (MapPath.PathPoint p in path.Points) {
                            LatLonToClientPos(p.Lat, p.Lon, out tx, out ty);
                            if (!first) {
                                e.Graphics.DrawLine(pen, new Point(px, py),
                                                        new Point(tx, ty));
                               

                            }
                            px = tx;
                            py = ty;
                            first = false;
                        }
                        pen.Dispose();

                        pen = new Pen(Color.Magenta, 4);

                        int i=1;
                        foreach (Marker m  in path.InterMarkers) {
                            LatLonToClientPos(m.lat, m.lon, out tx, out ty);
                            //e.Graphics.DrawEllipse(pen, tx, ty, 10, 10);
                            e.Graphics.DrawString(i.ToString(), drawFont, drawBrush, new PointF(tx, ty));

                            String[] s = Assembly.GetExecutingAssembly().GetManifestResourceNames();


                             
                            if (i==1)

                                if (this.mMarkerImages["from"] != null)
                                {
//                                    e.Graphics.DrawImage(this.mMarkerImages["from"], new PointF(tx, ty));
                                    e.Graphics.DrawImage(this.mMarkerImages["pin-blue"], new PointF(tx - 30, ty - 58));
                                }

                            if (i == path.InterMarkers.Count)
                                if (this.mMarkerImages["finish"] != null)
                                    e.Graphics.DrawImage(this.mMarkerImages["finish"], new PointF(tx, ty));

                            if ((i < path.InterMarkers.Count) && (i > 1))
                                if (this.mMarkerImages["over"] != null)
                                    e.Graphics.DrawImage(this.mMarkerImages["over"], new PointF(tx, ty));
                            

                            i++;
                        }
                        pen.Dispose();
                    }

                   //Draw order calc path

                    if (MapControl.ORDER_CALCULATED_PATH.InterMarkers.Count > 0) {
                        int px = 0, py = 0;	// Initialised only to pacify the compiler
                        int tx, ty;
                        bool first = true;
                        Pen pen = new Pen(Color.Magenta, 3);
                        e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                        if (this.LonScale < MapControl.DiscreteScales[10])
                            pen.EndCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;
                        foreach (MapPath.PathPoint p in MapControl.ORDER_CALCULATED_PATH.Points) {
                            LatLonToClientPos(p.Lat, p.Lon, out tx, out ty);
                            if (!first) {
                                e.Graphics.DrawLine(pen, new Point(px, py),
                                                        new Point(tx, ty));
                               

                            }
                            px = tx;
                            py = ty;
                            first = false;
                        }
                        pen.Dispose();

                        pen = new Pen(Color.Magenta, 4);

                        int i=1;
                        foreach (Marker m  in MapControl.ORDER_CALCULATED_PATH.InterMarkers) {
                            LatLonToClientPos(m.lat, m.lon, out tx, out ty);
                            
                            e.Graphics.DrawString(i.ToString(), drawFont, drawBrush, new PointF(tx, ty));

                            String[] s = Assembly.GetExecutingAssembly().GetManifestResourceNames();

                            if (m.lat == 0)
                            {
                                double Latitude = 57.6315;
                                double Longitude = 39.8684;

                                LatLonToClientPos(Latitude, Longitude, out tx, out ty);
                            }
                             
                            if (i==1)

                                if (this.mMarkerImages["from"] != null)
                                {
//                                    e.Graphics.DrawImage(this.mMarkerImages["from"], new PointF(tx, ty));
                                    e.Graphics.DrawImage(this.mMarkerImages["pin-blue"], new PointF(tx - 30, ty - 58));
                                    e.Graphics.DrawEllipse(pen, tx - 1, ty - 1, 2, 2);
                                }

                            if (i == MapControl.ORDER_CALCULATED_PATH.InterMarkers.Count)
                                if (this.mMarkerImages["finish"] != null)
                                {
                                    


                                    e.Graphics.DrawImage(this.mMarkerImages["finish"], new PointF(tx - 16, ty - 32));
                                    e.Graphics.DrawEllipse(pen, tx-1, ty-1, 2, 2);
                                }

                            if ((i < MapControl.ORDER_CALCULATED_PATH.InterMarkers.Count) && (i > 1))
                            {
                                if (m.lat != 0)
                                if (this.mMarkerImages["over"] != null)
                                    e.Graphics.DrawEllipse(pen, tx - 5, ty - 5, 10, 10);
                                //e.Graphics.DrawImage(this.mMarkerImages["over"], new PointF(tx, ty));
                            }
                            

                            i++;
                        }
                        pen.Dispose();
                    }

                      
                  //end Draw order calc path


                    //Draw zone polygins
                    
                    Pen penPolygons = new Pen(Color.Blue, 4);
                    
                    int n = 0;
                    foreach (ZoneRegion zr in ZoneRegion.list_zone_region) 
                    {
                        
                        if (zr.zoneMarkers.Count > 0) {
                            //n = 0;
                            List<PointF> points = new List<PointF>();

                            PointF center = new PointF();

                            foreach (Marker m in zr.zoneMarkers) {
                                
                                int tx, ty;
                                LatLonToClientPos(m.lat, m.lon, out tx, out ty);
                                points.Add(new PointF(tx, ty));
                                //if (n == 0)
                                e.Graphics.DrawEllipse(penPolygons, tx - 3, ty - 3, 7, 7);

                                e.Graphics.DrawString(m.parent_id.ToString(), drawFontMedium, drawBrush, new PointF(tx, ty));

                                center.X+= tx;
                                center.Y+= ty;

                                //n++;
                            }


                            if (zr.hightLighted == true)

                            if (zr.remote_zone==1)
                                e.Graphics.FillPolygon(MapControl.halfMagentabrush, points.ToArray());
                            else
                                e.Graphics.FillPolygon(MapControl.halfBluebrush, points.ToArray());



                            if (points.Count > 1)
                            {
                                e.Graphics.DrawPolygon(penPolygons, points.ToArray());


                                center.X /= points.Count;
                                center.Y /= points.Count;

                                e.Graphics.DrawString(zr.zone_id.ToString() + " "+zr.zone_name, drawFontLarge, drawBrush, center);
                                
                            }
                        }
                    }
                    





					// Highlight something if necessary...
					if(mHighlightedEntity!=null)
					{
						Pen pen;
						int mx,my;
						switch(mHighlightedEntity.Type)
						{
							case EntityType.marker:
								pen=new Pen(Color.Blue,3);
								LatLonToClientPos(mHighlightedEntity.Marker.lat,mHighlightedEntity.Marker.lon,out mx,out my);


                                if (mHighlightedEntity.Marker.lat == 0)
                                {
                                    double Latitude = 57.6315;
                                    double Longitude = 39.8684;

                                    LatLonToClientPos(Latitude, Longitude, out mx, out my);
                                }


								e.Graphics.DrawEllipse(pen,mx-6,my-6,13,13);
								pen.Dispose();
								break;
                            case EntityType.zonemarker:
                                pen = new Pen(Color.Green, 2);
                                LatLonToClientPos(mHighlightedEntity.Marker.lat, mHighlightedEntity.Marker.lon, out mx, out my);
                                e.Graphics.DrawEllipse(pen, mx - 6, my - 6, 13, 13);
                                pen.Dispose();
                                break;
							case EntityType.trackpoint:
								pen=new Pen(Color.Orange,2);
								foreach(TrackPoint tp in mHighlightedEntity.Track.Points)
								{
                                    if (tp.Time == mHighlightedEntity.Time)
									{
										LatLonToClientPos(tp.Lat,tp.Lon,out mx,out my);


                                        String s = tp.speed.ToString() + " " +tp.gps_time.ToString();
                                        SizeF tsize = e.Graphics.MeasureString(s, drawFont);
                                       // if (m.status == "4")
                                       //     e.Graphics.FillRectangle(drawRedBrush, mx, my, tsize.Width, tsize.Height);
                                       // else
                                        e.Graphics.FillRectangle(drawGreenBrush, mx, my - tsize.Height-5, tsize.Width, tsize.Height);
                                        e.Graphics.DrawString(s, drawFont, drawBrush, mx, my - tsize.Height-5);

										e.Graphics.DrawEllipse(pen,mx-4,my-4,9,9);
									}
								}
								pen.Dispose();
								break;

                            case EntityType.car:
                                
                                pen = new Pen(Color.Orange, 2);
                                Marker car = mHighlightedEntity.Marker;
                                car_data cd = (car_data)car.Tag;
                                LatLonToClientPos(car.lat, car.lon, out mx, out my);
                                String s2 = cd.car_desc;
                                s2 += "\r\n"+cd.order_address;
                                SizeF tsize2 = e.Graphics.MeasureString(s2, drawFont);    
                                e.Graphics.FillRectangle(drawGreenBrush, mx, my - tsize2.Height - 5, tsize2.Width, tsize2.Height);
                                e.Graphics.DrawString(s2, drawFont, drawBrush, mx, my - tsize2.Height - 5);

                                e.Graphics.DrawEllipse(pen, mx - 4, my - 4, 9, 9);
                                if ((cd.car_state == 8 ) || (cd.car_state == 7)) {
                                    if (cd.order_lat > 0) {
                                        pen = new Pen(Color.Gray, 3);
                                        pen.EndCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;
                                        int tx, ty;
                                        LatLonToClientPos(cd.order_lat, cd.order_lon, out tx, out ty);

                                        e.Graphics.DrawLine(pen, new Point(mx, my), new Point(tx, ty));
                                    }

                                } else {

                                    if (cd.to_lat > 0) {
                                        pen = new Pen(Color.Magenta, 3);
                                        pen.EndCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;
                                        int tx, ty;
                                        LatLonToClientPos(cd.to_lat, cd.to_lon, out tx, out ty);

                                        e.Graphics.DrawLine(pen, new Point(mx, my), new Point(tx, ty));
                                    }

                                }

                               
                                
                                pen.Dispose();
                                break;
  

						}
					}

				}
				catch(Exception ex)
				{
					Diags("Drawing Exception: "+ex.Message);
				}

			}
	
		}

		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing)
			{
				if(components!=null)
					components.Dispose();
				if(mTileManager!=null)
					mTileManager.NewDataAvailable+=NewTileDataAvailable;
			}
			base.Dispose(disposing);
		}


		void Timer1Tick(object sender, EventArgs e)
		{
            if (mUpdateView)
			{
                System.Console.WriteLine("Invalidate()");
				Invalidate();
				mUpdateView=false;
			}
		}

		/// <summary>
		/// Types of entity that can be found by GetEntityAtClientPos. 
		/// </summary>
		private enum EntityType
		{
			latlon,
			marker,
			trackpoint,
            zonemarker,
            car,
            address
		}

		private class EntityInfo
		{
			public EntityType Type;
			public double Lat;
			public double Lon;
			public Marker Marker;
            public MapPath.PathPoint PathPoint;
			public Track Track;
			public DateTime Time;

			public bool Equals(EntityInfo e2)
			{
				if(this.Type!=e2.Type)
					return false;
				switch(this.Type)
				{
					case EntityType.latlon:
						if(Lat!=e2.Lat || Lon!=e2.Lon)
							return false;
						break;
					case EntityType.marker:
						if(Marker!=e2.Marker)
							return false;
						break;
                    case EntityType.car:
                        if (Marker != e2.Marker)
                            return false;
                        break;
                    case EntityType.address:
                        if (Marker != e2.Marker)
                            return false;
                        break;
                    case EntityType.zonemarker:
                        if (Marker != e2.Marker)
                            return false;
                        break;
					case EntityType.trackpoint:
						if(Track!=e2.Track)
							return false;
						break;
					default:
						throw new ApplicationException("Unsupported entity type");
				}
				return true;
			}
		}

		/// <summary>
		/// Get the 'entity' at the given client position.
		/// </summary>
		/// <param name="x">The client X position.</param>
		/// <param name="y">The client Y position.</param>
		/// <returns>An EntityInfo instance describing the entity.</returns>
		private EntityInfo GetEntityAtClientPos(int x,int y)
		{
			EntityInfo ei=new EntityInfo();

			int tx,ty;
			foreach(Marker m in mMarkers)
			{
				LatLonToClientPos(m.lat,m.lon,out tx,out ty);
				if(Math.Abs(x-tx)<4 && Math.Abs(y-ty)<4)
				{
					ei.Marker=m;
					ei.Type=EntityType.marker;
					return ei;
				}
			}

            foreach (Marker m in mcarsMarkers) {
                LatLonToClientPos(m.lat, m.lon, out tx, out ty);
                if (Math.Abs(x - tx) < 4 && Math.Abs(y - ty) < 4) {
                    ei.Marker = m;
                    ei.Type = EntityType.car;
                    return ei;
                }
            }
            foreach (Marker m in maddressMarkers) {
                LatLonToClientPos(m.lat, m.lon, out tx, out ty);
                if (Math.Abs(x - tx) < 4 && Math.Abs(y - ty) < 4) {
                    ei.Marker = m;
                    ei.Type = EntityType.address;
                    return ei;
                }
            }
            

            foreach (MapPath p in this.Paths)
            {
                for (int i = 0; i < p.InterMarkers.Count; i++)
                {
                    Marker marker = p.InterMarkers[i];


                    LatLonToClientPos(marker.lat, marker.lon, out tx, out ty);

                    //if (    (x >  tx-12) && (x < tx + 12) && (y< ty+4) && (y > ty - 50) )
                    if (Math.Abs(x - tx) < 4 && Math.Abs(y - ty) < 4)
                    {
                        {
                            ei.Type = EntityType.marker;
                            ei.Marker = marker;
                            return ei;
                        }

                    }
                }
            }

            if (MapControl.ORDER_CALCULATED_PATH.InterMarkers.Count>0)
            {
                for (int i = 0; i < MapControl.ORDER_CALCULATED_PATH.InterMarkers.Count; i++)
                {
                    Marker marker = MapControl.ORDER_CALCULATED_PATH.InterMarkers[i];


                    LatLonToClientPos(marker.lat, marker.lon, out tx, out ty);

                    if (marker.lon == 0)
                    {
                        double Latitude = 57.6315;
                        double Longitude = 39.8684;
                        LatLonToClientPos(Latitude, Longitude, out tx, out ty);
                    }

                    if (    (x >  tx-12) && (x < tx + 12) && (y< ty+4) && (y > ty - 50) )
                    {
                        ei.Type = EntityType.marker;
                        ei.Marker = marker;
                        return ei;
                    }

                }
                
            }

            


            foreach (ZoneRegion zr in ZoneRegion.list_zone_region)
            {
                foreach (Marker m in zr.zoneMarkers) {
                    LatLonToClientPos(m.lat, m.lon, out tx, out ty);
                    if (Math.Abs(x - tx) < 4 && Math.Abs(y - ty) < 4) {
                        ei.Marker = m;
                        ei.Type = EntityType.zonemarker;
                        return ei;
                    }
                }
            }


			foreach(Track t in mTracks)
			{
				foreach(TrackPoint tp in t.Points)
				{
					LatLonToClientPos(tp.Lat,tp.Lon,out tx,out ty);
					if(Math.Abs(x-tx)<3 && Math.Abs(y-ty)<3)
					{
						ei.Track=t;
						ei.Time=tp.Time;
						ei.Type=EntityType.trackpoint;
						return ei;
					}
				}
			}

			ClientPosToLatLon(x,y,out ei.Lat,out ei.Lon);
			ei.Type=EntityType.latlon;
			return ei;

		}

		private bool mMouseMoving=false;
        private bool mzonemarkerMoving = false;
        private Marker zoneMarkerMoved = null;
        private Marker lastZoneMarker = null;
		private int mMouseMovingFromX;
		private int mMouseMovingFromY;
		private int mMouseMovingStartX;
		private int mMouseMovingStartY;

		void MapControlMouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {


            mMouseMovingFromX = e.X;
            mMouseMovingFromY = e.Y;
            mMouseMovingStartX = e.X;
            mMouseMovingStartY = e.Y;



            //если нажат шифт - выделить точку
            //если нажата средняя кнопка - начать новый полигон 
            //если нажат контрол - сдублировать выделенную точку и ее двигать
            if (e.Button == MouseButtons.Middle)
            {

                Marker m = new Marker();
                ClientPosToLatLon(e.X, e.Y, out m.lat, out m.lon);
                m.Tag = "";

                ZoneRegion zr = ZoneRegion.AddZoneRegion();
                zr.zoneMarkers.Add( m);
                //   if (Control.ModifierKeys != Keys.Shift)
                //       mMouseMoving = true;
            }
            //Если нажато внутри точки - отменить прокрутку карты и двигать точку



            ////    +++++ поиск линии

            if (e.Button == MouseButtons.Left) 
            if (Control.ModifierKeys == Keys.Shift)
            {

                Boolean found_line = false;
                int mx, my;
                double x, y, x0, x1, y0, y1;
                double d;

                x = e.X;
                y = e.Y;

                for (int i = 0; i < ZoneRegion.list_zone_region.Count ; i++)
                {
                    List<Marker> ml = ZoneRegion.list_zone_region[i].zoneMarkers;
                    if (ml.Count > 1)
                    {

                        for (int j = 0; j < ml.Count; j++)
                        {
                            Marker m = ml[j];
                            Marker m2 = null;
                            LatLonToClientPos(m.lat, m.lon, out mx, out my);
                            x0 = mx; y0 = my;

                            if (j == ml.Count - 1)
                            {
                                m2 = ml[0];

                            }
                            else m2 = ml[j + 1];

                            LatLonToClientPos(m2.lat, m2.lon, out mx, out my);
                            x1 = mx; y1 = my;

                            double r1, r2, r12;

                            r1 = dist(x, y, x0, y0);
                            r2 = dist(x, y, x1, y1);
                            r12 = dist(x0, y0, x1, y1);
                            if (r1 >= dist(r2, r12, 0, 0))
                            {
                                //  writeln ('Точка правее (x2,y2),r=',r2:6:2)
                            }

                            else if (r2 >= dist(r1, r12, 0, 0))
                            {
                            }
                            else
                            {

                                d = ((y0 - y1) * x + (x1 - x0) * y + (x0 * y1 - x1 * y0)) / (Math.Sqrt((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0)));



                                if (Math.Abs(d) < 4)
                                {
                                    System.Console.WriteLine("LINE distance " + d.ToString());

                                    m = new Marker();
                                    ClientPosToLatLon(e.X, e.Y, out m.lat, out m.lon);
                                    m.Tag = "";

                                    ml.Insert(j+1, m);
                                    i = ZoneRegion.list_zone_region.Count;
                                    found_line = true;

                                    this.Invalidate();
                                    break;
                                }
                               
                               // zoneMarkerMoved = m;

                                ///!!!! вставить новую точку между этими двумя.
                            }
                        }
                    }
                }

                if (!found_line)
                {
                    Marker m = new Marker();
                    ClientPosToLatLon(e.X, e.Y, out m.lat, out m.lon);
                    m.Tag = "";
                    if (ZoneRegion.list_zone_region.Count > 0)
                    {
                        ZoneRegion.list_zone_region[ZoneRegion.list_zone_region.Count - 1].AddZoneRegionMarker(m);

                        this.Invalidate();
                        
                    }

                }
            }

            /////     ---поиск линии


            




            EntityInfo ei = GetEntityAtClientPos(e.X, e.Y);
            if (ei.Type == EntityType.zonemarker)
            {

                mzonemarkerMoving = true;

                if (Control.ModifierKeys == Keys.Control)
                {
                    foreach (ZoneRegion zr in ZoneRegion.list_zone_region)
                    {

                        for (int i = 0; i < zr.zoneMarkers.Count; i++)
                        {
                            Marker m = zr.zoneMarkers[i];
                            if (m == ei.Marker)
                            {
                                m = new Marker();
                                ClientPosToLatLon(e.X, e.Y, out m.lat, out m.lon);
                                m.Tag = "";

                                zr.zoneMarkers.Insert(i, m);
                                zoneMarkerMoved = m;


                               
                                break;

                            }


                        }
                    }

                }

                else

                    zoneMarkerMoved = ei.Marker;


                lastZoneMarker = zoneMarkerMoved;

                return;
            }

            if (ei.Type == EntityType.marker) {

                mzonemarkerMoving = true;

                zoneMarkerMoved = ei.Marker;

                lastZoneMarker = zoneMarkerMoved;
                
                return;
            }



            //Поиск линии в маршруте - только после поиска существующих точек.

            if (e.Button == MouseButtons.Left)
            {

                Boolean found_line = false;
                int mx, my;
                double x, y, x0, x1, y0, y1;
                double d;

                x = e.X;
                y = e.Y;





                {


                    {

                        for (int j = 0; j < ORDER_CALCULATED_PATH.Points.Count - 1; j++)
                        {
                            MapPath.PathPoint p = ORDER_CALCULATED_PATH.Points[j];
                            MapPath.PathPoint p2 = null;
                            LatLonToClientPos(p.Lat, p.Lon, out mx, out my);
                            x0 = mx; y0 = my;

                            p2 = ORDER_CALCULATED_PATH.Points[j + 1];

                            LatLonToClientPos(p2.Lat, p2.Lon, out mx, out my);
                            x1 = mx; y1 = my;

                            double r1, r2, r12;

                            r1 = dist(x, y, x0, y0);
                            r2 = dist(x, y, x1, y1);
                            r12 = dist(x0, y0, x1, y1);
                            if (r1 >= dist(r2, r12, 0, 0))
                            {
                                //  writeln ('Точка правее (x2,y2),r=',r2:6:2)
                            }

                            else if (r2 >= dist(r1, r12, 0, 0))
                            {
                            }
                            else
                            {

                                d = ((y0 - y1) * x + (x1 - x0) * y + (x0 * y1 - x1 * y0)) / (Math.Sqrt((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0)));



                                if (Math.Abs(d) < 4)
                                {
                                    System.Console.WriteLine("LINE distance " + d.ToString());

                                    double lat, lon;


                                    //m = new Marker();
                                    ClientPosToLatLon(e.X, e.Y, out lat, out lon);

                                    ////Если пром точка 1 == 0 - установит ее. 
                                    //иначе вторую.

                                    if (ORDER_CALCULATED_PATH.getInterPoint(1).lat == 0)
                                    {
                                        ORDER_CALCULATED_PATH.setPoint(1, lat, lon, "");
                                        zoneMarkerMoved = ORDER_CALCULATED_PATH.getInterPoint(1);
                                    }
                                    else
                                    {
                                        ORDER_CALCULATED_PATH.setPoint(2, lat, lon, "");
                                        zoneMarkerMoved = ORDER_CALCULATED_PATH.getInterPoint(2);
                                    }
                                    ORDER_CALCULATED_PATH.calculatePath(200);
                                    //m.Tag = "";

                                    //ml.Insert(j + 1, m);
                                    //i = ZoneRegion.list_zone_region.Count;
                                    found_line = true;

                                    mzonemarkerMoving = true;
                                    lastZoneMarker = zoneMarkerMoved;

                                        return;


                                    this.Invalidate();
                                    break;
                                }

                                // zoneMarkerMoved = m;

                                ///!!!! вставить новую точку между этими двумя.
                            }
                        }
                    }
                }

                /*if (!found_line)
                {
                    Marker m = new Marker();
                    ClientPosToLatLon(e.X, e.Y, out m.lat, out m.lon);
                    m.Tag = "";
                    if (ZoneRegion.list_zone_region.Count > 0)
                    {
                        ZoneRegion.list_zone_region[ZoneRegion.list_zone_region.Count - 1].AddZoneRegionMarker(m);

                        this.Invalidate();

                    }

                }
                 */
            }


            //----Поиск линии в маршруте



/*
            foreach (MapPath path in this.Paths)
            {
                double pointX = (this.Latitude - path.InterPoints[0].Lat) / mScale;
                double pointY = (this.Longitude - path.InterPoints[0].Lon) / mScale;

                System.Console.WriteLine("pointX = " + pointX.ToString() + "PointY = " + pointY.ToString());



            }
*/
            if (!mzonemarkerMoving)

            if ((Control.ModifierKeys != Keys.Shift) && (Control.ModifierKeys != Keys.Control))
                mMouseMoving = true;

        }

		void MapControlMouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
            //если прокрутка карты - не рисовать полигон
            //если выделена точка - отпустить ее.
            //иначе

            //Если нажат шифт - поставить точку в последний начатый полигон.
            //Если нажата правая кнопка - дублировать нажатую точку, вставить ее сразу после найденной с этими координатами  и ее и двигать.
            //если над линией - поставить промежуточную точку - так не сделать.

            if (mzonemarkerMoving )
            {
                

                ClientPosToLatLon(e.X, e.Y, out zoneMarkerMoved.lat, out zoneMarkerMoved.lon);
                mzonemarkerMoving = false;
                zoneMarkerMoved = null;
                return;
            }

            

/*
            if (!mMouseMoving) {
                if (zoneMarkers.Count > 0)
                {
                    int cnt = zoneMarkers.Count - 1;
                    List<Marker> zoneMarkerList = zoneMarkers[cnt];
                    if (zoneMarkerList != null)
                    {


                      //  Marker m = new Marker();
                      //  ClientPosToLatLon(e.X, e.Y, out m.lat, out m.lon);
                      //  m.Tag = "";

                      //  zoneMarkerList.Add(m);
                        return;
                    }
                }
            }
           
*/            
            
            
            
            mMouseMoving=false;
			if(e.X==mMouseMovingStartX && e.Y==mMouseMovingStartY)
			{

				EntityInfo ei=GetEntityAtClientPos(e.X,e.Y);
				switch(ei.Type)
				{
					case EntityType.trackpoint:
						if(TrackPointClicked!=null)
							TrackPointClicked(ei.Track,ei.Time);
						break;
					case EntityType.marker:
						if(MarkerClicked!=null)
							MarkerClicked(ei.Marker);
						break;
					case EntityType.latlon:
						if(LocationClicked!=null)
							LocationClicked(ei.Lat,ei.Lon);
						break;
				}

			}
		}

		void MapControlMouseLeave(object sender, System.EventArgs e)
		{
			mMouseMoving=false;
			mHighlightedEntity=null;
			Invalidate();
		}

        private double  dist (double x1,double y1,double x2,double y2)// {Расстояние между точками}
        {
            return Math.Sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
        }

		void MapControlMouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
            if (mzonemarkerMoving) {
                ClientPosToLatLon(e.X, e.Y, out zoneMarkerMoved.lat, out zoneMarkerMoved.lon);

                ORDER_CALCULATED_PATH.need_calculate = true;
               // ORDER_CALCULATED_PATH.calculatePath(200);
               // Invalidate();
                



                //найти маркер, к которому можно приклеиться.... и дать текущему маркеру такие же координаты.
                //вернее - добавить текущий маркер в список другого района....
                //ЕСЛИ НАЖАТ ШИФТ.
                //если без шифта - модно разносить точки.!!!

                Marker baseMarker = null;

                foreach (ZoneRegion zr in ZoneRegion.list_zone_region)
                {


                    foreach (Marker m in zr.zoneMarkers) {
                        if (m != zoneMarkerMoved) {
                            
                            int tx, ty;
                            LatLonToClientPos(m.lat, m.lon, out tx, out ty);
                            if (Math.Abs(e.X - tx) < 4 && Math.Abs(e.Y - ty) < 4) 
                                {

                                    m.lat = zoneMarkerMoved.lat;
                                    m.lon = zoneMarkerMoved.lon;
                                /*baseMarker = m;
                                foreach (ZoneRegion zr2 in ZoneRegion.list_zone_region)
                                {

                                    for (int i = 0; i < zr2.zoneMarkers.Count; i++) {
                                        if (zr2.zoneMarkers[i] == zoneMarkerMoved)
                                        {
                                            zr2.zoneMarkers[i].lat = baseMarker.lat;
                                            zr2.zoneMarkers[i].lon = baseMarker.lon;
                                            
                                        }
                                    }
                                }
                                */


                                //zoneMarkerMoved = baseMarker;
                               // break;

                            }
                        }
                    }
                    //if (baseMarker != null)
                     //   break;
                }

                Invalidate();
               // mzonemarkerMoving = false;
                return;
            }


            


            ///выделить регион, над которым мышка
            ///


            Marker mark = new Marker();
            ClientPosToLatLon(e.X, e.Y, out mark.lat, out mark.lon);

            for (int i=0; i< ZoneRegion.list_zone_region.Count; i++)
            {
                if (PointInZonePolygon(mark, ZoneRegion.list_zone_region[i]))
                {

                    if (ZoneRegion.list_zone_region[i].hightLighted != true)
                    {
                        ZoneRegion.list_zone_region[i].hightLighted = true;
                        Invalidate();
                    }

                }
                else
                {
                    if (ZoneRegion.list_zone_region[i].hightLighted != false)
                    {
                        ZoneRegion.list_zone_region[i].hightLighted = false;
                        Invalidate();
                    }
                }
            }

			if(mMouseMoving)
			{
				int movedx=e.X-mMouseMovingFromX;
				int movedy=e.Y-mMouseMovingFromY;
				if(movedx!=0 || movedy!=0)
				{
					mLat+=mScale*movedy*0.543;
					mLon-=mScale*movedx;
                    
					Invalidate();
					RaiseViewChanged();
				}
				mMouseMovingFromX=e.X;
				mMouseMovingFromY=e.Y;
			}
			else
			{
				if(mHighlightEntities)
				{
                    //определить расстояние до прямой
                    //d = ((y0-y1)x + (x1-x0)y + (x0*y1 - x1y0)) / (sqrt(  (x1-x0)^2 + (y1-y0)^2 ));
                    
                   



					EntityInfo ei=GetEntityAtClientPos(e.X,e.Y);
					EntityInfo newh;
					if(ei.Type==EntityType.marker || ei.Type==EntityType.trackpoint || ei.Type==EntityType.zonemarker || ei.Type == EntityType.car || ei.Type== EntityType.address)
					{
						newh=ei;
                        FireonMarkerHighlited(ei.Marker);


					}
					else
					{
						newh=null;
					}
					if(mHighlightedEntity==null)
					{
						if(newh!=null)
						{
							mHighlightedEntity=newh;
                            
							Invalidate();
						}
					}
					else
					{
						if(newh==null)
						{
							mHighlightedEntity=null;
							Invalidate();
						}
						else
						{
							if(!newh.Equals(mHighlightedEntity))
							{
								mHighlightedEntity=newh;

                                
								Invalidate();
							}
						}
					}
				}
			}
		}

	}


    public class car_data {
        public double to_lat = 0;
        public double to_lon = 0;
        public double order_lat = 0;
        public double order_lon = 0;
        public Track track = new Track();
        public String car_desc = "";
        public String car_id = "";
        public String order_address = "";
        public int car_state = 0;
    }

    public class Marker
    {
        public double lat;
        public double lon;
        public int parent_id;
        public int intParam1;

        public int MarkerMoved = 0;

        /// <summary>
        /// The tag for this marker - used to identify it by whatever means the
        /// user deems suitable.
        /// </summary>
        public object Tag;

        public Marker()
        {
            this.lat = 0;
            this.lon = 0;
        }

        public Marker(double lat, double lon)
        {
            this.lat = lat;
            this.lon = lon;
        }
    }

    public class TileManager
    {

        public delegate void NewDataAvailableHandler();
        public event NewDataAvailableHandler NewDataAvailable;

        /// <summary>
        /// Dictionary of cached tiles, keyed on a string formed
        /// as "zoom,x,y".
        /// </summary>
        private Dictionary<string, Bitmap> mTiles;



        /// <summary>
        /// Tile currently being downloaded, or null if none.
        /// </summary>
        private string mDownloadTile = null;

        public TileManager()
        {
            mTiles = new Dictionary<string, Bitmap>();
        }
        /*add by programmer76*/
        public String DiskCachePath
        {
            get { return mDiskCachePath; }
            set { mDiskCachePath = value; }
        }



        private String mDiskCachePath = "";




        public void EmptyCache()
        {
            foreach (string key in mTiles.Keys)
            {
                if (mTiles[key] != null)
                    mTiles[key].Dispose();
            }
            mTiles.Clear();
        }

        public void EmptyHalfCache()
        {
            Dictionary<string, Bitmap> newmTiles = new Dictionary<string, Bitmap>(); ;
            int n = 0;
            foreach (string key in mTiles.Keys)
            {
                if (n == 0)
                {
                    if (mTiles[key] != null)
                    {
                        mTiles[key].Dispose();
                    }
                    n = 1;
                }
                else
                {
                    n = 0;
                    newmTiles.Add(key, mTiles[key]);
                }

            }
            mTiles.Clear();
            mTiles = newmTiles;

        }
        public int CacheSize
        {
            get { return mTiles.Count; }
        }

        public Image GetTile(int x, int y, int zoom)
        {
            string key = zoom.ToString() + ',' + x.ToString() + ',' + y.ToString();
            lock (mTiles)
            {
                
                if (mTiles.ContainsKey(key))
                {
                    // We already have the tile in our cache...
                    return mTiles[key];

                }
                /*add by programmer76 - check for file on disk before internet download*/
                Bitmap tile = null;
                if (this.mDiskCachePath != "")
                {
                    if (File.Exists(mDiskCachePath + '/' + key + ".png"))
                    {
                        Stream s = new System.IO.FileStream(mDiskCachePath + '/' + key + ".png", FileMode.Open);
                        tile = new Bitmap(s);
                        s.Flush();
                        s.Close();

                        lock (mTiles)
                        {
                            mTiles[key] = tile;
                        }
                        return tile;

                    }
                }



               
            }

            if (mDownloadTile == null)
            {
                // Start downloading the tile...
                mDownloadTile = key;
                Thread t = new Thread(Download);
                t.Start();
            }
            return null;
        }

        private void Download()
        {
            String downTile = mDownloadTile;
            try
            {
                Bitmap tile = null;

                if (tile == null)
                {

                    string result = String.Empty;





                    string[] vals = downTile.Split(',');
                    //http://tile.openstreetmap.org/15/20023/9933.png
                    //http://tile0.maps.2gis.com/tiles?x=40017&y=19851&z=16&v=3
                    string url = "http://tile.openstreetmap.org/" + vals[0] + "/" + vals[1] + "/" + vals[2] + ".png";
                    url = "http://tile0.maps.2gis.com/tiles?x="+vals[1]+"&y="+vals[2]+"&z="+vals[0]+"&v=3";
                    WebClient client = new WebClient();
                    if (MapControl.webProxy != null)
                        client.Proxy = MapControl.webProxy;
                    Stream stream = client.OpenRead(url);
                    tile = new Bitmap(stream);
                    stream.Flush();
                    stream.Close();

                    //add by programmer76. save tile to disk cache
                    if (mDiskCachePath != "")
                        if (tile != null)
                        {
                            tile.Save(mDiskCachePath + '/' + downTile + ".png");
                        }
                }
                lock (mTiles)
                {
                    mTiles[downTile] = tile;
                }

                // TODO: If we've reached the (as yet undefined) limit of how
                // many tiles we should be caching, we should remove the least
                // recently used one.

                if (NewDataAvailable != null)
                    NewDataAvailable();
            }
            catch (Exception)
            {
                lock (mTiles)
                {
                    mTiles[mDownloadTile] = null;
                }
                if (NewDataAvailable != null)
                    NewDataAvailable();
            }
            finally
            {
                mDownloadTile = null;
            }

        }

    }
}
