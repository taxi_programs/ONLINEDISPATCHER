﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ONLINEDISPATCHER
{
    public partial class ArendaCar : Form
    {

        int disposed_summ = 0;
        int dispose_type = 1;
        int arendacarid = 0;
        int carid = 0;
        int driverid = 0;
        int balance = 0;
        String driver_fio = "";
        String car_name = "";
        bool loading = false;
        bool use_car_from_driver = true;


        String[] sum_types = { "Машина", "Телефон", "Налоги", "Штрафы", "Ремонты" };

        private class sum
        {
            public int value;
            public String name;
            public int sum_type;

        }
        private class totalSUM
        {
            public String name;
            public int getTotalSum(){
                int result = 0;
                for(int i=0; i< sumDetail.Count; i++)
                    result += sumDetail[i].value;
                return result;
            }
            public List<sum> sumDetail = new List<sum>();

            



        }

        public ArendaCar()
        {
            InitializeComponent();
        }

        

        private void ArendaCar_Load(object sender, EventArgs e)
        {
            

            dateTimePicker1.Value = DateTime.Now.Date.AddDays(-7);
            dateTimePicker2.Value = dateTimePicker1.Value.AddDays(14);


            DataTable drivers_only = DataBase.mssqlRead("SELECT drivers.id ID, cast(cars.id as varchar(10))+' '+cars.color+ ' ' +cars.mark + ' ' + cars.gosnum +' '+drivers.fio +' '+ drivers.phones DRIVER_DESC  FROM CARS, DRIVERS WHERE DRIVERS.ID = CARS.DRIVERID and cars.arenda_car_id>0 order by 1");



          


           // DataTable cars_only = DataBase.mssqlRead("SELECT CARS.ID, cast (CARS.ID as varchar(6))+' '+  CARS.COLOR +' '+  CARS.mark +' '+ cars.gosnum  CAR_DESC  FROM CARS order by CARS.ID");
            DataTable arenda_cars = DataBase.mssqlRead("select -1 ID, '<Любая машина>' CAR_DESC union SELECT ID, ARENDA_CARS.COLOR +' '+  ARENDA_CARS.mark +' '+ ARENDA_cars.gosnum  CAR_DESC  FROM ARENDA_CARS order by ID");
            //DataTable drivers_only = DataBase.mssqlRead("SELECT DRIVERS.ID, drivers.fio +' '+ drivers.phones DRIVER_DESC  FROM DRIVERS order by DRIVERS.FIO");

            
            cmbArendaCars.DisplayMember = "CAR_DESC";
            cmbArendaCars.ValueMember = "ID";
            cmbArendaCars.DropDownStyle = ComboBoxStyle.DropDown;
            cmbArendaCars.AutoCompleteSource = AutoCompleteSource.ListItems;

            cmbArendaCars.MatchingMethod = SergeUtils.StringMatchingMethod.UseRegexs;
            cmbArendaCars.Text = "";
            cmbArendaCars.DataSource = arenda_cars;


            
            cmbDrivers.DisplayMember = "DRIVER_DESC";
            cmbDrivers.ValueMember = "ID";
            cmbDrivers.DropDownStyle = ComboBoxStyle.DropDown;
            cmbDrivers.AutoCompleteSource = AutoCompleteSource.ListItems;

            cmbDrivers.MatchingMethod = SergeUtils.StringMatchingMethod.UseRegexs;
            cmbDrivers.Text = "";
            cmbDrivers.DataSource = drivers_only;


        }

        private void ArendaCar_Shown(object sender, EventArgs e)
        {
//            load_driver_data();
        }
        //варианты загрузки : на основе позывного и на основе измененых водителя и машины
        private void load_driver_data()
        {

            System.Console.WriteLine("load_driver_data");
            loading = true;

            if (use_car_from_driver)
                arendacarid = 0;

            driver_fio = "";
            car_name = "";
            balance = 0;

            driverid = 0;
            if (cmbDrivers.SelectedValue != null)
                driverid = (int)cmbDrivers.SelectedValue;

   //         label_balance.Text = "";
/*            if (this.Tag.ToString().Length == 0)
                return;
 */ 
            //carid = (int)this.Tag; ;
            this.Text = "Карточка водителя " + cmbDrivers.Text;
            String sql = "select drivers.id driverid, cars.id carid, isnull(cars.arenda_car_id, 0) arenda_car_id, cars.serviceid, cars.color+ ' ' +cars.mark + ' ' + cars.gosnum + ' ' + cars.descr car_descr, drivers.fio , drivers.phones,  drivers.phone2, drivers.phone3, drivers.descr, balance.summ balance, cars.block_to, cars.enable_beznal from cars,drivers " +
                " left join  balance on balance.doc_typ='DRIVER' and balance.doc_id = drivers.id " +
                " where cars.id = drivers.carid and drivers.id = " + driverid;
            DataTable dt = DataBase.mssqlRead(sql);

            if (dt.Rows.Count > 0)
            {

                balance = (int)(decimal)dt.Rows[0]["balance"];
                if (use_car_from_driver)
                    arendacarid = (int)dt.Rows[0]["arenda_car_id"];
                carid = (int)dt.Rows[0]["carid"];


                cmbArendaCars.SelectedValue = arendacarid;

                label_balance.Text = "Кошелек: " + balance + " руб";

            }





            load_driver_grafic_for_arenda_car();

            
            //обновляет суммы на надписях, считает расчетную сумму по аренде и сколько можно списать
            //так же вызывается при dt1_RowChanged - 
            update_arenda_sums_captions();

            loading = false;

            //распределяет сумму по графику платежей

            dispose_summ_2();
            //раскрашивает таблицу-график
            //update_dtaviewgrid2_colors();



            label_debug.Text = "driverid=" + driverid + " arendacarid=" + arendacarid;
            


        }

        private void load_driver_grafic_for_arenda_car()
        {
            List<SqlParameter> parameters1 = new List<SqlParameter>();

            parameters1.Add(new SqlParameter("arendacarid", arendacarid));
            parameters1.Add(new SqlParameter("driverid", driverid));
            parameters1.Add(new SqlParameter("dt1", dateTimePicker1.Value));
            parameters1.Add(new SqlParameter("dt2", dateTimePicker2.Value));

            DataTable dt_grafic = DataBase.mssqlRead("select * from arenda_show_grafic1 ( @arendacarid, @driverid, @dt1, @dt2 , 1)  order by arenda_date", parameters1.ToArray());



            if (dataGridView2.DataSource == null)
            {

                DataTable dt2 = new DataTable();

                dt2.Columns.Add("ARENDA_DATE").DataType = System.Type.GetType("System.DateTime");
                dt2.Columns.Add("WORK_DAY").DataType = System.Type.GetType("System.Int32"); ;
                dt2.Columns.Add("OPL_SUMS_COMMENTS").DataType = System.Type.GetType("System.String"); ;
                

                


                dt2.Columns.Add("ARENDA_SUM").DataType = System.Type.GetType("System.Int32");
                dt2.Columns.Add("OPL_SUM").DataType = System.Type.GetType("System.Int32");
                dt2.Columns.Add("DOLG_SUM").DataType = System.Type.GetType("System.Int32");
                for (int i = 1; i < 6; i++)
                    dt2.Columns.Add("OPL_SUM" + i.ToString()).DataType = System.Type.GetType("System.Int32");
                

                dt2.Columns.Add("DISPOSED_SUM").DataType = System.Type.GetType("System.Int32");
                for (int i = 1; i < 6; i++)
                    dt2.Columns.Add("DISPOSED_SUM" + i.ToString()).DataType = System.Type.GetType("System.Int32");



                dt2.Columns["ARENDA_SUM"].AllowDBNull = false;
                dt2.Columns["DISPOSED_SUM"].AllowDBNull = false;
                dt2.Columns["DOLG_SUM"].AllowDBNull = false;
                


                dt2.Columns["OPL_SUM"].AllowDBNull = false;



                for (int i = 1; i < 6; i++)
                {
                    dt2.Columns["DISPOSED_SUM" + i.ToString()].AllowDBNull = false;
                    dt2.Columns["OPL_SUM" + i.ToString()].AllowDBNull = false;
                }


                // может быть несколько дат - смотреть отдельно                
                //dt2.Columns.Add("OPL_DATE").DataType = System.Type.GetType("System.DateTime");


                //dt2.Columns.Add("WORK_TYPE").DataType = System.Type.GetType("System.String");

                dt2.Columns.Add("COMMENT").DataType = System.Type.GetType("System.String");
                dt2.Columns.Add("COMMENT_DISPOSED").DataType = System.Type.GetType("System.String");
                dt2.Columns.Add("ID").DataType = System.Type.GetType("System.Int32");
                dt2.Columns.Add("MODIFIED").DataType = System.Type.GetType("System.Int32");
                dt2.Columns.Add("DAY_COLOR").DataType = System.Type.GetType("System.Int32");


                dt2.Columns.Add("arenda_car_name").DataType = System.Type.GetType("System.String");
                dt2.Columns.Add("driver_fio").DataType = System.Type.GetType("System.String");








                DataGridViewCheckBoxColumn work_day_column = new DataGridViewCheckBoxColumn(false);
                work_day_column.DataPropertyName = "WORK_DAY";
                work_day_column.TrueValue = 1;
                work_day_column.FalseValue = 0;

                work_day_column.Name = "WORK_DAY";
                work_day_column.Width = 70;
             


                dataGridView2.Columns.Add(work_day_column);


                dataGridView2.DataSource = dt2;


                dataGridView2.Columns["ARENDA_DATE"].HeaderText = "Дата смены";
                dataGridView2.Columns["WORK_DAY"].HeaderText = "Оплачиваемый";

                dataGridView2.Columns["ARENDA_SUM"].HeaderText = "План";
                dataGridView2.Columns["DOLG_SUM"].HeaderText = "Долг";


                dataGridView2.Columns["OPL_SUM"].HeaderText = "Оплачено";
                dataGridView2.Columns["OPL_SUM"].ToolTipText = "В этой колонке выводится сумма оплат, внесенных за эту смену\nБелым выделены ячейки в которых сумма оплаты за машину соответствует плану\n"
                + "Зеленым выделены ячейки в которых сумма ранее оплаченая больше чем план за машину(когда водитель кроме аренды платит долги)\n"
                + "Красным выделены ячейки в которых сумма оплаты меньше чем сумма плана (водитель недоплатил за смену)";

                dataGridView2.Columns["DOLG_SUM"].ToolTipText = "В этой колонке выводится сумма ОТРИЦАТЕЛЬНЫХ оплат, внесенных на эту дату \nЭти оплаты считаются ДОЛГОМ (например догл за ремонт) и их потом необходимо гасить\n";
                




                dataGridView2.Columns["DISPOSED_SUM"].HeaderText = "Оплата сейчас";


                dataGridView2.Columns["COMMENT"].HeaderText = "комментарий к смене";
                dataGridView2.Columns["COMMENT_DISPOSED"].HeaderText = "Комментарий к оплате";
                dataGridView2.Columns["ID"].Visible = false;
                dataGridView2.Columns["MODIFIED"].Visible = false;
                dataGridView2.Columns["DAY_COLOR"].Visible = false;
                dataGridView2.Columns["COMMENT_DISPOSED"].Visible = false;

                dataGridView2.Columns["OPL_SUMS_COMMENTS"].HeaderText = "предыдущие оплаты";
                dataGridView2.Columns["OPL_SUMS_COMMENTS"].Visible = false;

                dataGridView2.Columns["arenda_car_name"].HeaderText = "машина";
                dataGridView2.Columns["arenda_car_name"].Width = 150;
                dataGridView2.Columns["arenda_car_name"].ReadOnly = true;

                dataGridView2.Columns["driver_fio"].HeaderText = "Водитель";
                dataGridView2.Columns["driver_fio"].Width = 150;
                dataGridView2.Columns["driver_fio"].ReadOnly = true;


                

                dataGridView2.Columns["ARENDA_SUM"].ReadOnly = false;
                dataGridView2.Columns["OPL_SUM"].ReadOnly = true;
                dataGridView2.Columns["DOLG_SUM"].ReadOnly = true;


                dataGridView2.Columns["DISPOSED_SUM"].ReadOnly = true;


                for (int i = 1; i < 6; i++)
                {

                    
                    dataGridView2.Columns["DISPOSED_SUM" + i.ToString()].Visible = chk_opl_detail.Checked;
                    dataGridView2.Columns["OPL_SUM" + i.ToString()].Visible = chk_opl_detail.Checked;

                    dataGridView2.Columns["OPL_SUM" + i.ToString()].ReadOnly = true;

                    dataGridView2.Columns["OPL_SUM" + i.ToString()].HeaderText = sum_types[i - 1] + "Опл. ранее"  ;
                    dataGridView2.Columns["DISPOSED_SUM" + i.ToString()].HeaderText = sum_types[i - 1] + "Оплата ";
                    

                    dataGridView2.Columns["DISPOSED_SUM" + i.ToString()].Width = 60;
                    dataGridView2.Columns["OPL_SUM" + i.ToString()].Width = 60;
                    

                }
                dataGridView2.Columns["ARENDA_SUM"].Width = 60;
                dataGridView2.Columns["DISPOSED_SUM"].Width = 60;
                dataGridView2.Columns["OPL_SUM"].Width = 60;
                dataGridView2.Columns["DOLG_SUM"].Width = 60;

                



                



                foreach (DataGridViewColumn column in dataGridView2.Columns)
                {
                    column.SortMode = DataGridViewColumnSortMode.NotSortable;
                }
            }

            int current_day_row = 0;
            DataTable dt3 = (DataTable)dataGridView2.DataSource;
            dt3.Rows.Clear();
            DataRow itog_row = dt3.NewRow();

            itog_row["ARENDA_SUM"] = 0;
            itog_row["DOLG_SUM"] = 0;
            //row["OPL_DATE"] = dt_grafic.Rows[i]["OPL_DATE"];
            itog_row["OPL_SUM"] = 0;
            itog_row["OPL_SUM1"] = 0;
            itog_row["OPL_SUM2"] = 0;
            itog_row["OPL_SUM3"] = 0;
            itog_row["OPL_SUM4"] = 0;
            itog_row["OPL_SUM5"] = 0;
            itog_row["ID"] = -100000;
            itog_row["MODIFIED"] = 0;
            itog_row["DISPOSED_SUM"] = 0;
            itog_row["DISPOSED_SUM1"] = 0;
            itog_row["DISPOSED_SUM2"] = 0;
            itog_row["DISPOSED_SUM3"] = 0;
            itog_row["DISPOSED_SUM4"] = 0;
            itog_row["DISPOSED_SUM5"] = 0;
            
            itog_row["DAY_COLOR"] = 0; ;

            
            for (int i = 0; i < dt_grafic.Rows.Count; i++)
            {
                DataRow row = dt3.NewRow();
                row["ID"] = dt_grafic.Rows[i]["ID"];
                row["MODIFIED"] = 0;
                row["DISPOSED_SUM"] = 0;
                row["DISPOSED_SUM1"] = 0;
                row["DISPOSED_SUM2"] = 0;
                row["DISPOSED_SUM3"] = 0;
                row["DISPOSED_SUM4"] = 0;
                row["DISPOSED_SUM5"] = 0;


                row["OPL_SUMS_COMMENTS"] = dt_grafic.Rows[i]["OPL_SUMS_COMMENTS"];
                row["ARENDA_DATE"] = dt_grafic.Rows[i]["ARENDA_DATE"];
                row["ARENDA_SUM"] = dt_grafic.Rows[i]["ARENDA_SUM"];
                row["DOLG_SUM"] = dt_grafic.Rows[i]["DOLG_SUM"];
                //row["OPL_DATE"] = dt_grafic.Rows[i]["OPL_DATE"];
                row["OPL_SUM"] = dt_grafic.Rows[i]["OPL_SUM"];
                row["OPL_SUM1"] = dt_grafic.Rows[i]["OPL_SUM1"];
                row["OPL_SUM2"] = dt_grafic.Rows[i]["OPL_SUM2"];
                row["OPL_SUM3"] = dt_grafic.Rows[i]["OPL_SUM3"];
                row["OPL_SUM4"] = dt_grafic.Rows[i]["OPL_SUM4"];
                row["OPL_SUM5"] = dt_grafic.Rows[i]["OPL_SUM5"];
                //   row["WORK_TYPE"] = dt_grafic.Rows[i]["WORK_TYPE"];
                row["WORK_DAY"] = dt_grafic.Rows[i]["WORK_DAY"];
                row["COMMENT"] = dt_grafic.Rows[i]["COMMENT"];
                row["COMMENT_DISPOSED"] = dt_grafic.Rows[i]["COMMENT_DISPOSED"];
                row["DAY_COLOR"] = dt_grafic.Rows[i]["DAY_COLOR"];

                row["arenda_car_name"] = dt_grafic.Rows[i]["arenda_car_name"];
                row["driver_fio"] = dt_grafic.Rows[i]["driver_fio"];



                itog_row["ARENDA_SUM"] = (int)dt_grafic.Rows[i]["ARENDA_SUM"] + (int)itog_row["ARENDA_SUM"];
                itog_row["DOLG_SUM"] = (int)dt_grafic.Rows[i]["DOLG_SUM"] + (int)itog_row["DOLG_SUM"];
                //row["OPL_DATE"] = dt_grafic.Rows[i]["OPL_DATE"];
                itog_row["OPL_SUM"] = (int)dt_grafic.Rows[i]["OPL_SUM"] + (int)itog_row["OPL_SUM"];
                itog_row["OPL_SUM1"] = (int)dt_grafic.Rows[i]["OPL_SUM1"] + (int)itog_row["OPL_SUM1"];
                itog_row["OPL_SUM2"] = (int)dt_grafic.Rows[i]["OPL_SUM2"] + (int)itog_row["OPL_SUM2"];
                itog_row["OPL_SUM3"] = (int)dt_grafic.Rows[i]["OPL_SUM3"] + (int)itog_row["OPL_SUM3"];
                itog_row["OPL_SUM4"] = (int)dt_grafic.Rows[i]["OPL_SUM4"] + (int)itog_row["OPL_SUM4"];
                itog_row["OPL_SUM5"] = (int)dt_grafic.Rows[i]["OPL_SUM5"] + (int)itog_row["OPL_SUM5"];
                itog_row["ARENDA_DATE"] = row["ARENDA_DATE"];


                if ((DateTime)row["ARENDA_DATE"] == DateTime.Now.Date)
                {
                    current_day_row = i;

                    //dataGridView2.Rows[i].
                }

                dt3.Rows.Add(row);
            }

            try
            {
                dataGridView2.Rows[current_day_row].Selected = true;
            }
            catch (Exception ex) { }

            dt3.Rows.Add(itog_row);





            dataGridView2.DataError += new DataGridViewDataErrorEventHandler(dataGridView2_DataError);
        }

        void dt1_RowChanged(object sender, DataRowChangeEventArgs e)
        {
            if (loading)
                return;

            update_arenda_sums_captions();




            dispose_summ_2();
        }

       

        void dataGridView2_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }

        

        private void update_arenda_sums_captions()
        {

            System.Console.WriteLine("update_arenda_sums_captions");

           /* disposed_summ = 0;

            for (int i = 0; i < ((DataTable)dataGridView1.DataSource).Rows.Count; i++)
            {
                disposed_summ += (int)((DataTable)dataGridView1.DataSource).Rows[i]["disposed_sum"];
                if ((int)((DataTable)dataGridView1.DataSource).Rows[i]["required_for_smena"] == 1)
                {
                    
                    arenda_sum_default += (int)((DataTable)dataGridView1.DataSource).Rows[i]["arenda_sum_default"];
                }

            }
            * */

            disposed_summ = 0;
            for (int i = 0; i < dataGridView2.Rows.Count; i++)
            {
                disposed_summ += (int)dataGridView2["DISPOSED_SUM", i].Value;
            }
            label_disposed_sum.Text = "Оплата сейчас " + disposed_summ;
            //label_plan_arendy.Text = "План аренды: " + arenda_sum_default;


            label_balance_info.Text = "";
            if (balance > disposed_summ)
                label_balance_info.Text = "Излишек в кошельке: " + (balance - disposed_summ);
            if (balance < disposed_summ)
                label_balance_info.Text = "Недостаточно денег в кошельке: " + (balance - disposed_summ);


            button_save.Enabled = (disposed_summ <= balance);
            if (disposed_summ > balance)
            {
                label_disposed_sum.ForeColor = Color.Red;

            }
            else
                label_disposed_sum.ForeColor = Color.Black;
        }



        private void dataGridView2_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            

        }

        private void button3_Click(object sender, EventArgs e)
        {
            save_grafic(true);
        }

        private void save_grafic(bool save_opl )
        {
            if (!(arendacarid > 0))
            {
                MessageBox.Show("Не указана машина");
                return;
            }

            int spis_sum = 0;
            for (int i = 0; i < dataGridView2.Rows.Count-1; i++)
            {
                if ((int)dataGridView2["MODIFIED", i].Value == 1)
                {
                    List<SqlParameter> parameters = new List<SqlParameter>();



                    //сохранить график и отдельно платежи по нему
                    //26-12-2016 - график содержит только сумму плана по аренде авто
                    //план по долгам - убран

                    String[] fields = { "arendacarid", "driverid", "arenda_sum", "arenda_date", "comment", "work_day", "driver_fio", "car_name", "operator_id" };
                    object[] values = { arendacarid, driverid, dataGridView2["ARENDA_SUM", i].Value, dataGridView2["ARENDA_DATE", i].Value, dataGridView2["COMMENT", i].Value, dataGridView2["WORK_DAY", i].Value, cmbDrivers.Text, cmbArendaCars.Text, MAIN_FORM.OperatorID };


                    if ((int)dataGridView2["ID", i].Value > 0)
                    {
                        if (DataBase.mssqlUpdate("arenda_grafic", fields, values, "ID", (int)dataGridView2["ID", i].Value) == 1)
                        {
                            dataGridView2["MODIFIED", i].Value = 0;
                        }
                    }


                    if ((int)dataGridView2["ID", i].Value == 0)
                    {
                        int new_id = DataBase.mssqlInsertOneReturnID("arenda_grafic", fields, values);
                        if (new_id > 0)
                        {
                            dataGridView2["ID", i].Value = new_id;
                            dataGridView2["MODIFIED", i].Value = 0;
                        }
                    }
                    if (save_opl == true)
                    {
                        if ((int)dataGridView2["DISPOSED_SUM", i].Value > 0)
                        {

                            for(int j=1; j< 6; j++){
                                String[] fields1 = { "arenda_sum_type_id", "arenda_grafic_id", "arendacarid", "driverid", "sum", "comment", "reg_type" };
                                object[] values1 = { j, (int)dataGridView2["ID", i].Value, arendacarid, driverid, (int)dataGridView2["DISPOSED_SUM" + j, i].Value, dataGridView2["COMMENT_DISPOSED", i].Value, "opl"};
                                DataBase.mssqlInsertOneReturnID("arenda_reg", fields1, values1);
                                spis_sum += (int)dataGridView2["DISPOSED_SUM" + j, i].Value; 

                                

                                dataGridView2["DISPOSED_SUM"+j.ToString(), i].Value = 0;
                                

                                
                            }
                            dataGridView2["COMMENT_DISPOSED", i].Value = "";



                        }
                    }


                }
            }

            //изменение баланса водителя - списание аренды с баланса
            //учесть что если сумма больше чем количество дней в графике - сумму надо уменьшать чтобы не было переплаты

            //сохранение сегодняшних платежей которые внесены в поля.
            //
            if (save_opl == true)
            {
                int type = 2;


                String[] fields0 = { "driverid", "ServiceID", "OpID", "dtArrive", "sum", "type", "promised_payment", "Descr" };
                object[] values0 = { driverid, 0, MAIN_FORM.OperatorID, DateTime.Now, spis_sum, type, 0, "списание в аренду " };

                DataBase.mssqlInsert("BANK", fields0, values0);
            }


            //изменение арендных балансов водителя - списать с него аренду в момент сохранения графика за все дни до текущего
            //допустима коррекция - если снять галку "рабочая смена"




            load_driver_data();
        }

        
        private void dispose_summ_2()
        {
            //int dispose_type = 0;

            //1 - 1 день доплатить
            //2 - доплатить 2 дня
            //0 - все что есть на балансе со старта

            //процедура разнесения суммы по графику
            //определить начало - откуда вносить
            //далее по порядку - сумму аредны и сумму денег которые вносим
            //вызывать процедуру:
            //по кнопке, при изменении суммы для разнесения, при вмене типа раоты в графике (например поставлен выходной)
            loading = true;

            disposed_summ = 0;


            System.Console.WriteLine("dispose_summ_2");

            //int ost_opl_sum = disposed_summ;
            
            bool start_found = false;

            DataTable dt = (DataTable)dataGridView2.DataSource;

            int need_disposed_days = dispose_type ;
            int balance_ost = balance;

            for (int i = 0; i < dt.Rows.Count-1; i++)
            {
                //ищем начало для записи. это место где arenda_sum > 0 и opl_sum < arenda_sum или где 

                //УЧЕСТЬ!!!!!   тут в первую строку таблицы если она пустая поставится аренда. надо задавать начало работы

                ////начало графика - по проставленному типу работы
                //т.е. если нет типа работы - строка пропускается
                //а дальше по типам работы (сравнение по строке - код не вынимается)
                //работа, выходной, больничный, ... прогул и т.п.


                //int arenda_sum = (int)dt.Rows[i]["ARENDA_SUM1"] + (int)dt.Rows[i]["ARENDA_SUM2"] + (int)dt.Rows[i]["ARENDA_SUM3"] + (int)dt.Rows[i]["ARENDA_SUM4"] + (int)dt.Rows[i]["ARENDA_SUM51"];

                //обнуление суммы для оплаты - только для аренды
                if ((int)dt.Rows[i]["ARENDA_SUM"] == 0)
                {
                    dt.Rows[i]["DISPOSED_SUM"] = 0;
                    
                    //обнуление суммы для оплаты - только для аренды
                    //оплату по долгам - не обнулять - обнулять только при смене машины-водителя и при сохранении графика
                    //при этом при распределении суммы из кошелька сначала вычесть сумму которая пошла в долги
                    //и распределять в аренду то что останется
                    //for (int j=1; j< 6; j++)
                    //    dt.Rows[i]["DISPOSED_SUM"+j.ToString()] = 0;
                    dt.Rows[i]["DISPOSED_SUM1"] = 0;
                    dt.Rows[i]["MODIFIED"] = 1;
                }
                        

                
                {

                    
                    
                    int neopl = 0;
                    
                        //учесть что при внесении долгов они попадут сюда как отрицательная величина
                        int opl_sum = (int)dt.Rows[i]["OPL_SUM1"];
                        if (opl_sum < 0)
                            opl_sum = 0;

                        if ((int)dt.Rows[i]["ARENDA_SUM"] > opl_sum)
                            neopl = 1;
                    
                    if (((int)dt.Rows[i]["ARENDA_SUM"] > 0) && neopl >0)
                    {
                        //тут старт - не зватает оплаченых денег
                        start_found = true;
                    }
                    
                    if ((start_found))
                    {
                        if (((int)dt.Rows[i]["DISPOSED_SUM"]) > 0)
                        {
                            dt.Rows[i]["DISPOSED_SUM"] = 0;
                            dt.Rows[i]["DISPOSED_SUM1"] = 0;
                            
                            dt.Rows[i]["MODIFIED"] = 1;
                        }

                        
                        if (need_disposed_days > 0 || dispose_type == 0)
                        {
                        if ((int)dt.Rows[i]["ARENDA_SUM"] > 0){

                            need_disposed_days--;
                            //сколько было оплачено ранее, сколько осталось доплатить в этот день
                            int delta = 0;

                            //for (int j = 1; j < 6; j++)
                            {
                                
                                //учесть что при внесении долгов они попадут сюда как отрицательная величина
                                int opl_sum1 = (int)dt.Rows[i]["OPL_SUM1"];
                                if (opl_sum1 < 0)
                                    opl_sum1 = 0;

                                delta = (int)dt.Rows[i]["ARENDA_SUM"] - opl_sum1;
                                if (dispose_type == 0)
                                {//распределить остаток баланса сколько есть
                                    if (delta > balance_ost)
                                        delta = balance_ost;
                                    balance_ost -= delta;
                                }

                                if (delta <= 0)
                                    delta = 0;
                                dt.Rows[i]["DISPOSED_SUM1"] = delta;
                            }


                            int sum = 0;
                            for (int j = 1; j < 6; j++)
                            {
                                sum += (int)dt.Rows[i]["DISPOSED_SUM" + j.ToString()];
                            }

                            dt.Rows[i]["DISPOSED_SUM"] = sum;
                            disposed_summ += (int)dt.Rows[i]["DISPOSED_SUM"];
                            dt.Rows[i]["MODIFIED"] = 1;

                        }
                        }
                        
                        
                    }
                     
                }
                


            }

            loading = false;

            
            updateDayArendaDetail();
            update_arenda_sums_captions();
            update_dtaviewgrid2_colors();
           // dataGridView2.Refresh();
            dataGridView2.Invalidate();
        }

        private void dataGridView2_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {

           
            
        }

        private void update_dtaviewgrid2_colors()
        {
            System.Console.WriteLine("update_dtaviewgrid2_colors");
            try
            {

                for (int i = 0; i < dataGridView2.Rows.Count; i++)
                {
                    

                    


                    




                }
            }
            catch (Exception e)
            {
            }

            dataGridView2.Invalidate();
        }



        /*
          int row_spis_summ = 0;
                        for (int j = arenda_table.Rows.Count-1; j >=0; j++)
                        {
                            //часть распределяемой суммы по типу аренды - пропорциональная
                            //
                            int disposed_arenda_sum = (int)(0.5+(int)dataGridView2["DISPOSED_SUM", i].Value * (int)arenda_table.Rows[j]["disposed_sum"] / (float)disposed_summ);

                            
                            //на первый тип аренды списать все что осталос чтобы не было ошибок округления
                            if (j == 0)
                                disposed_arenda_sum = (int)dataGridView2["DISPOSED_SUM", i].Value - row_spis_summ;

                            spis_sum += disposed_arenda_sum;
                            row_spis_summ += disposed_arenda_sum;
                            String[] fields1 = { "arenda_sum_type_id", "arenda_grafic_id", "carid", "driverid", "sum", "comment" };
                            object[] values1 = { arenda_table.Rows[j]["id"], (int)dataGridView2["ID", i].Value, carid, driverid, disposed_arenda_sum, dataGridView2["COMMENT_DISPOSED", i].Value };



                            int new_id_sum = DataBase.mssqlInsert("arenda_reg", fields1, values1);
                            if (new_id_sum > 0) arenda_saved = true;
                        }
         */
        //type: 0 - все что есть, 1 - одна смена, 2 - две смены
        
        /*private void dispose_summ_balance(int type)
        {
            return;
            //процедура разнесения текущей суммы на балансе в аренду по типам аренды
            DataTable dt1 = (DataTable)dataGridView1.DataSource;
            int row_spis_summ = 0;


            int disposed_sum = balance;
            if (type == 1)
                disposed_sum = arenda_sum_default;
            if (type == 2)
                disposed_sum = 2*arenda_sum_default;
            if (type == -1)
                disposed_sum = arenda_sum_default;
            if (type == -2)
                disposed_sum = 2*arenda_sum_default;
            if (type == -3) //все что есть в аренду
                disposed_sum = balance;

            ///посчитать сумму обязательных арендных платежей
            int required_arendA_sum = 0;
            if (type<0) 
                for (int i = dt1.Rows.Count - 1; i >= 0; i--)
                {
                    if ((int)dt1.Rows[i]["required_for_smena"]==1)
                        required_arendA_sum += (int)dt1.Rows[i]["arenda_sum_default"];
                }

            for (int i = dt1.Rows.Count-1; i >=0; i--)
            {
                if ((type <0 ))//сумму только в аренду
                {
                    if (type == -1)
                        disposed_sum = required_arendA_sum;
                    if (type == -2)
                        disposed_sum = 2*required_arendA_sum;
                    if (type == -3)
                        disposed_sum = balance;

                    if ((int)dt1.Rows[i]["required_for_smena"]==1)
                        dt1.Rows[i]["disposed_sum"] = disposed_sum * (int)dt1.Rows[i]["arenda_sum_default"] / required_arendA_sum;
                    else
                        dt1.Rows[i]["disposed_sum"] = 0;
                    
                }
                else
                {

                int disposed_arenda_sum = (int)(0.5 + disposed_sum * (int)dt1.Rows[i]["arenda_sum_default"] / (float)arenda_sum_default);
                if (i == 0)
                    disposed_arenda_sum = disposed_sum - row_spis_summ;
                
                dt1.Rows[i]["disposed_sum"] = disposed_arenda_sum;

                row_spis_summ += disposed_arenda_sum;
                }
            }

            dispose_summ_2(0);
        }
         */ 

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            load_driver_data();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            timer2.Enabled = false;
           
        }
        
     

        private void button_add_money_to_balance_Click(object sender, EventArgs e)
        {

            
            add_money form = new add_money();
            //int carid = 0;
            //int.TryParse(this.Tag.ToString(), out carid);
            //if (this.Tag != null)
                //carid = (int)this.Tag;
            form.selected_car_id = carid; ;
            form.selected_driver_id = driverid;
            form.radioButton2.Enabled = false;
            form.textbox_summ.Focus();
            form.ShowDialog();


            timer1.Enabled = true;
        }

      

        private void dataGridView2_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dataGridView2.IsCurrentCellDirty)
            {
                dataGridView2.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void dataGridView2_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
                return;

            if (dataGridView2.Columns[e.ColumnIndex].Name == "WORK_DAY")
            {
                if ((int)dataGridView2[e.ColumnIndex, e.RowIndex].Value == 0)
                {
                    dataGridView2["arenda_sum", e.RowIndex].Value = 0;
                }

                else
                {
                    //   dataGridView2["arenda_sum", e.RowIndex].Value = arenda_sum_default;

                    for (int j = e.RowIndex - 1; j >= 0; j--)
                    {
                        if ((int)dataGridView2["WORK_DAY", j].Value == 1)
                        {
                            dataGridView2["ARENDA_SUM", e.RowIndex].Value = dataGridView2["ARENDA_SUM", j].Value;
                            break;
                        }
                    }



                }
                dispose_summ_2();
                updateDayArendaDetail();
            }
            try
            {
                if ((dataGridView2.Columns[e.ColumnIndex].Name == "DISPOSED_SUM1") || (dataGridView2.Columns[e.ColumnIndex].Name == "DISPOSED_SUM2") || (dataGridView2.Columns[e.ColumnIndex].Name == "DISPOSED_SUM3") || (dataGridView2.Columns[e.ColumnIndex].Name == "DISPOSED_SUM4") || (dataGridView2.Columns[e.ColumnIndex].Name == "DISPOSED_SUM5"))
                    dataGridView2["DISPOSED_SUM", e.RowIndex].Value = (int)dataGridView2["DISPOSED_SUM1", e.RowIndex].Value + (int)dataGridView2["DISPOSED_SUM2", e.RowIndex].Value + (int)dataGridView2["DISPOSED_SUM3", e.RowIndex].Value + (int)dataGridView2["DISPOSED_SUM4", e.RowIndex].Value + (int)dataGridView2["DISPOSED_SUM5", e.RowIndex].Value;
            }
            catch (Exception ex)
            {
            }
            
            

            dataGridView2["MODIFIED", e.RowIndex].Value = 1;



            System.Console.WriteLine("dataGridView2_CellValueChanged");
            dataGridView2.Invalidate();
        }

        private void btn_dispose_1_smena_Click(object sender, EventArgs e)
        {
            dispose_type = 1;
            dispose_summ_2();
        }

        private void btn_dispose_2_smena_Click(object sender, EventArgs e)
        {
            dispose_type = 2;
            dispose_summ_2();
        }

        private void btn_dispose_all_balance_Click(object sender, EventArgs e)
        {
            dispose_type = 0;
            dispose_summ_2();
        }

        private void btn_save_arenda_plan_Click(object sender, EventArgs e)
        {
          
        }

        private void dataGridView2_RowHeadersWidthChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView2_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {

        }

        private void btn_dispose_only_1_smena_Click(object sender, EventArgs e)
        {
          
        }

        private void btn_dispose_all_balance_into_arenda_Click(object sender, EventArgs e)
        {
           
        }

        private void chk_arenda_detail_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void chk_opl_detail_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 1; i < 6; i++)
            {
                if (i == 2)
                    continue;
                dataGridView2.Columns["OPL_SUM" + i.ToString()].Visible = chk_opl_detail.Checked;
            }
        }

        private void chk_disposed_detail_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 1; i < 6; i++)
            {
                if (i == 2)
                    continue;
                dataGridView2.Columns["DISPOSED_SUM" + i.ToString()].Visible = chk_disposed_detail.Checked;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            save_grafic(false);
        }

        private void dataGridView2_SelectionChanged(object sender, EventArgs e)
        {
            updateDayArendaDetail();
        }

        private void updateDayArendaDetail()
        {
            if (dataGridView3.IsCurrentCellInEditMode)
                return;
            DataGridViewRow currRow = dataGridView2.CurrentRow;

            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("arendacarid", arendacarid));
            parameters.Add(new SqlParameter("driverid", driverid));

            DataTable dt = DataBase.mssqlRead("select * from arenda_get_driver_sum_types_balance(@arendacarid, @driverid) order by sum_type", parameters.ToArray());

            if (currRow != null)
            {
                dataTable1.Rows.Clear();
                for (int i = 1; i < dt.Rows.Count + 1; i++)
                {
                    if (i == 2)
                        continue;

                    DataRow row = dataTable1.NewRow();

                    if (i != (int)dt.Rows[i-1]["sum_type"])
                    {
                        MessageBox.Show("Ошибка ! несоответствие типов платежей");
                        break;
                    }
                    row["sum_type"] = i;
                    row["sum_name"] = (String)dt.Rows[i-1]["name"];
                    if (i == 1)
                        row["plan_sum"] = currRow.Cells["ARENDA_SUM"].Value;
                    row["opl_sum"] = currRow.Cells["OPL_SUM" + i.ToString()].Value;
                    row["disposed_sum"] = currRow.Cells["DISPOSED_SUM" + i.ToString()].Value;
                    row["balance1"] = (int)dt.Rows[i-1]["balance"];
                    dataTable1.Rows.Add(row);
                    //row["comment"] = '';
                    //row["sum_type"] = i;

                }
            }


            




        }

        private void dataGridView3_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dataGridView3.IsCurrentCellDirty)
            {
                dataGridView3.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void dataGridView3_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }

        private void dataGridView3_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
                return;

            if (dataGridView3.Columns[e.ColumnIndex].Name.Contains("disposedsum"))
            {
                
                dataGridView2.CurrentRow.Cells["DISPOSED_SUM" + (e.RowIndex + 1).ToString()].Value = dataGridView3[e.ColumnIndex, e.RowIndex].Value;
            }
                
        }

        private void button4_Click(object sender, EventArgs e)
        {
            dateTimePicker1.Value = DateTime.Now.Date.AddDays(-DateTime.Now.Day+1);
            dateTimePicker2.Value = dateTimePicker1.Value.AddMonths(1).AddDays(-1); ;
            timer1.Enabled = false;
            timer1.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            dateTimePicker1.Value = DateTime.Now.Date.AddDays(-7);
            dateTimePicker2.Value = dateTimePicker1.Value.AddDays(14);
            timer1.Enabled = false;
            timer1.Enabled = true;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            timer1.Enabled = true;
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            timer1.Enabled = true;
        }

 

        private void cmbCars_SelectedValueChanged(object sender, EventArgs e)
        {
            this.use_car_from_driver = false;

            if (cmbArendaCars.SelectedValue != null)
            {
                arendacarid = (int)cmbArendaCars.SelectedValue;
            }
            else
                arendacarid = -1;

            load_driver_grafic_for_arenda_car();
            
        }

        private void cmbDrivers_SelectedValueChanged(object sender, EventArgs e)
        {
            this.use_car_from_driver = true;
            load_driver_data();
            load_driver_grafic_for_arenda_car();
        }

        private void dataGridView2_CellToolTipTextNeeded(object sender, DataGridViewCellToolTipTextNeededEventArgs e)
        {
            /*
            if (e.RowIndex > 0 && e.ColumnIndex == ((DataTable)dataGridView2.DataSource).Columns.IndexOf(.c))
                e.ToolTipText = ((DataTable)dataGridView2.DataSource).Rows[e.RowIndex]["opl_sums_comments"].ToString();
            else
                e.ToolTipText = "";
             */
        }

        private void btnReports_Click(object sender, EventArgs e)
        {
            arenda_reports form = new arenda_reports();
            form.Show();
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            contextMenuStrip1.Items.Clear();
            if (dataGridView2.CurrentRow!=null)
            {

                int grafic_id = (int)dataGridView2.CurrentRow.Cells["ID"].Value;
                //загрузить возможные оплаты по графику, сделать из ниж элементы меню
                //        String[] sum_types = { "Машина", "Телефон", "Налоги", "Штрафы", "Ремонты" };

                ToolStripItem mi = contextMenuStrip1.Items.Add("Добавить налог");
                mi.Tag = (int)3;
                mi.Click += new EventHandler(mi_Click_add_dolg);

                mi = contextMenuStrip1.Items.Add("Добавить штраф");
                mi.Tag = (int)4;
                mi.Click += new EventHandler(mi_Click_add_dolg);

                mi = contextMenuStrip1.Items.Add("Добавить ремонт");
                mi.Tag = (int)5;
                mi.Click += new EventHandler(mi_Click_add_dolg);


                ToolStripSeparator sep = new ToolStripSeparator();
                contextMenuStrip1.Items.Add(sep);

                DataTable dt = DataBase.mssqlRead("SELECT distinct arendA_reg.id, arendA_reg.date, arendA_reg.comment, arenda_sum_type.name, arenda_reg.sum from arendA_reg, arenda_sum_type where arenda_reg.arendA_sum_type_id=arenda_sum_type.id and arenda_grafic_id=" + grafic_id);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //MenuItem mi = new MenuItem("Оплата от "+((DateTime)dt.Rows[i]["date"]).ToString("dd-MM-yyyy hh:mm")+", "+dt.Rows[i]["comment"].ToString());
                    //mi.Click += new EventHandler(mi_Click);

                    ToolStripItem mi1 = contextMenuStrip1.Items.Add(((DateTime)dt.Rows[i]["date"]).ToString("dd-MM-yyyy HH:mm") + " " + dt.Rows[i]["sum"].ToString() +" р. "+ dt.Rows[i]["name"].ToString() + ", " + dt.Rows[i]["comment"].ToString());
                    mi1.Tag = (int)dt.Rows[i]["id"];
                    mi1.Click+=new EventHandler(mi_Click);
                }
                /*
                String[] fields1 = { "arenda_sum_type_id", "arenda_grafic_id", "arendacarid", "driverid", "sum", "comment", "reg_type" };
                object[] values1 = { 1, grafic_id, arendacarid, driverid, 0, "", "opl"};
                int newID = DataBase.mssqlInsertOneReturnID("arenda_reg", fields1, values1);
                ToolStripItem mi = contextMenuStrip1.Items.Add("Добавить долг по аренде");
                mi.Tag = (int)-1;
                mi.Click += new EventHandler(mi_Click);
                 */ 

            }
        }

        void mi_Click_add_dolg(object sender, EventArgs e)
        {
            ToolStripItem mi = (ToolStripItem)sender;
            int type = (int)mi.Tag;




            int grafic_id = (int)dataGridView2.CurrentRow.Cells["ID"].Value;

            //добавление долга: перед открытие карточки редактирования - создать в базе запись с нужным типом
            String[] fields = { "arenda_sum_type_id", "arenda_grafic_id", "arendacarid", "driverid", "sum", "comment", "reg_type" };
            Object[] values = { type, grafic_id, arendacarid, driverid, 0, "штраф за превышение", "opl" };

            int id = DataBase.mssqlInsertOneReturnID("ARENDA_REG", fields, values);

            DictionaryElement di = new DictionaryElement();
            di.parent = this;

            di.Show(this); ;
            di.doLoad("ARENDA_REG", id);
            /*
            ArendaPayEdit form = new ArendaPayEdit();
            form.arenda_reg_id = id;
            form.doLoad();
            form.Show();
             * 
             */ 

   
        }


        void mi_Click(object sender, EventArgs e)
        {
            ToolStripItem mi = (ToolStripItem)sender;
            int id = (int)mi.Tag;

            DictionaryElement di = new DictionaryElement();
            di.parent = this;

            di.Show(this); ;
            di.doLoad("ARENDA_REG", id);
            /*
            ArendaPayEdit form = new ArendaPayEdit();
            form.arenda_reg_id = id;
            form.doLoad();
            form.Show();
             * 
             */ 

   
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dataGridView2_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (!e.RowIndex.Equals(-1) && !e.ColumnIndex.Equals(-1))
            {
                if (e.Button.Equals(MouseButtons.Right))
                {
                    dataGridView2.CurrentCell = dataGridView2[e.ColumnIndex, e.RowIndex];
                    dataGridView2.CurrentRow.Selected = true;
                }
            }
            else
            {
                //view_work_orders.CurrentCell = null;
                dataGridView2.ClearSelection();
            }
        }

      

        private void dataGridView2_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            int intValue = 1;

            if (int.TryParse(e.Value.ToString(), out intValue)){
                if (intValue == 0)
                    e.Value = null;
            }

            if (dataGridView2.Columns[e.ColumnIndex].Name.StartsWith("DISPOSED_SUM")){
                e.CellStyle.BackColor = Color.LightCyan;
            }

            if (dataGridView2.Columns[e.ColumnIndex].Name.StartsWith("OPL_SUM"))
            {
                e.CellStyle.BackColor = Color.Silver;
            }

                
        }

        private void dataGridView3_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            int intValue = 1;

            if (int.TryParse(e.Value.ToString(), out intValue))
            {
                if (intValue == 0)
                    e.Value = null;
            }
            if (e.ColumnIndex == 6)
            {
                System.Console.WriteLine(dataGridView3[6, 0]);
                if (intValue < 0)
                    e.CellStyle.BackColor = Color.LightPink;
            }
        }

        public void btnReload_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            timer1.Enabled = true;
        }

        private void dataGridView2_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            Rectangle rc = e.CellBounds;
            /*
             *                     int uslugi = (int)ord_data.Rows[0]["tax_uslugi"];
                String sUslugi = "";
                if ((uslugi & 1) == 1) sUslugi += "ОТД РАЙОН ";
                if ((uslugi & 2) == 2) sUslugi += "БАГАЖ ";
                if ((uslugi & 4) == 4) sUslugi += "ЖИВОТНОЕ ";
                if ((uslugi & 8) == 8) sUslugi += "ГРУНТ ";
             */


            if (e.ColumnIndex != -1 && e.RowIndex!=-1)
            {
                if (dataGridView2.Columns[e.ColumnIndex].Name.StartsWith("DISPOSED_SUM"))
                {
                    e.CellStyle.BackColor = Color.LightCyan;
                }

                if (dataGridView2.Columns[e.ColumnIndex].Name.StartsWith("OPL_SUM"))
                {
                    e.CellStyle.BackColor = Color.WhiteSmoke;
                }

                if (dataGridView2.Columns[e.ColumnIndex].Name == "ARENDA_DATE")
                {
                    if ((int)dataGridView2["DAY_COLOR", e.RowIndex].Value == 1)
                        e.CellStyle.BackColor = dataGridView2.DefaultCellStyle.BackColor;
                    else
                        e.CellStyle.BackColor = Color.LightGray;
                }


                if ( (DateTime)dataGridView2["ARENDA_DATE", e.RowIndex].Value == DateTime.Now.Date){
                    if ((dataGridView2.Columns[e.ColumnIndex].Name == "ARENDA_DATE") ||
                        (dataGridView2.Columns[e.ColumnIndex].Name == "ARENDA_SUM") ||
                        (dataGridView2.Columns[e.ColumnIndex].Name == "WORK_DAY") )
                            e.CellStyle.BackColor = Color.LightCyan;
                }

                if (e.RowIndex == dataGridView2.RowCount - 1)
                {
                    e.CellStyle.BackColor = Color.LightGray;
                }


                


             

               

            }

            e.PaintBackground(rc, true);
            e.PaintContent(rc);
            if (e.ColumnIndex != -1 && e.RowIndex != -1)
            {
                if (dataGridView2.Columns[e.ColumnIndex].Name == "OPL_SUM")
                {
                    if ((int)dataGridView2["OPL_SUM", e.RowIndex].Value != (int)dataGridView2["ARENDA_SUM", e.RowIndex].Value)
                    {
                        Rectangle rc_icon = e.CellBounds;
                        rc_icon.X = rc_icon.Right - 15;
                        rc_icon.Width = 13;
                        rc_icon.Y = rc_icon.Y + (rc_icon.Height - 13) / 2;
                        rc_icon.Height = 13;



                        if ((int)dataGridView2["OPL_SUM", e.RowIndex].Value > (int)dataGridView2["ARENDA_SUM", e.RowIndex].Value)
                            e.Graphics.DrawImage(Properties.Resources.flag_green, rc_icon);
                        if ((int)dataGridView2["OPL_SUM", e.RowIndex].Value < (int)dataGridView2["ARENDA_SUM", e.RowIndex].Value)
                            e.Graphics.DrawImage(Properties.Resources.flag_red, rc_icon);
                    }
                    else
                        e.CellStyle.BackColor = dataGridView2.DefaultCellStyle.BackColor;
                }


                if (dataGridView2.Columns[e.ColumnIndex].Name == "OPL_SUM1")
                {
                    if ((int)dataGridView2["OPL_SUM1", e.RowIndex].Value != (int)dataGridView2["ARENDA_SUM", e.RowIndex].Value)
                    {
                        Rectangle rc_icon = e.CellBounds;
                        rc_icon.X = rc_icon.Right - 15;
                        rc_icon.Width = 13;
                        rc_icon.Y = rc_icon.Y + (rc_icon.Height - 13) / 2;
                        rc_icon.Height = 13;



                        if ((int)dataGridView2["OPL_SUM1", e.RowIndex].Value > (int)dataGridView2["ARENDA_SUM", e.RowIndex].Value)
                            e.Graphics.DrawImage(Properties.Resources.flag_green, rc_icon);
                        if ((int)dataGridView2["OPL_SUM1", e.RowIndex].Value < (int)dataGridView2["ARENDA_SUM", e.RowIndex].Value)
                            e.Graphics.DrawImage(Properties.Resources.flag_red, rc_icon);
                    }
                    else
                        e.CellStyle.BackColor = dataGridView2.DefaultCellStyle.BackColor;
                }


                if (dataGridView2.Columns[e.ColumnIndex].Name == "DISPOSED_SUM")
                {
                    Rectangle rc_icon = e.CellBounds;
                    rc_icon.X = rc_icon.Right - 15;
                    rc_icon.Width = 13;
                    rc_icon.Y = rc_icon.Y + (rc_icon.Height - 13) / 2;
                    rc_icon.Height = 13;


                    if ((int)dataGridView2["DISPOSED_SUM", e.RowIndex].Value > 0)
                        e.Graphics.DrawImage(Properties.Resources.flag_green, rc_icon);
                   //else
                    //    e.Graphics.DrawImage(Properties.Resources.flag_red, rc_icon);

                }
            }

            
            

            e.Handled = true;
        }

        private void dataGridView2_CellFormatting_1(object sender, DataGridViewCellFormattingEventArgs e)
        {
            int intValue = 1;

            if (int.TryParse(e.Value.ToString(), out intValue))
            {
                if (intValue == 0)
                    e.Value = null;
            }
        }

 




    }
}
