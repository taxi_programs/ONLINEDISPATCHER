﻿namespace ONLINEDISPATCHER
{
    partial class order
    {
        /// <summary>
        ///Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.Phone = new System.Windows.Forms.TextBox();
            this.ConnectPhone = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.AddrFrom = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.HouseFrom = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.FlatFrom = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.AddrFromName = new System.Windows.Forms.ComboBox();
            this.ServiceID = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.AddrToName = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.HouseTo2 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.ClientID = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.OrgID = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.CarID = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.ZoneID = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.RaionToID = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.Descr = new System.Windows.Forms.TextBox();
            this.order_prim_menu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnSave = new System.Windows.Forms.Button();
            this.btnEndCall = new System.Windows.Forms.Button();
            this.chkPredvar = new System.Windows.Forms.CheckBox();
            this.dtPredvar = new System.Windows.Forms.DateTimePicker();
            this.tmPredvar = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dt_order_info = new System.Windows.Forms.Label();
            this.btnPhoneInfo = new System.Windows.Forms.Button();
            this.lbPhoneInfo = new System.Windows.Forms.ListBox();
            this.FromPhone = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.ORGDETAIL_ID = new System.Windows.Forms.ComboBox();
            this.phone_info = new System.Windows.Forms.Label();
            this.AddrToForDriver = new System.Windows.Forms.ComboBox();
            this.chk_EndOrder = new System.Windows.Forms.CheckBox();
            this.comboEndState = new System.Windows.Forms.ComboBox();
            this.btnEndOrder = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.summ = new System.Windows.Forms.TextBox();
            this.btnSumtoDriver = new System.Windows.Forms.Button();
            this.CarIDView = new System.Windows.Forms.TextBox();
            this.btnCarDial = new System.Windows.Forms.Button();
            this.btnCarMessage = new System.Windows.Forms.Button();
            this.btnPhone1Dial = new System.Windows.Forms.Button();
            this.btnPhone2Dial = new System.Windows.Forms.Button();
            this.TalonNumber = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.расчетМаршрута = new System.Windows.Forms.Button();
            this.chkYaroslavl1 = new System.Windows.Forms.CheckBox();
            this.chkYaroslavl2 = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.AddrTo = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.DistanceMoney = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.predvShowDelta = new System.Windows.Forms.ComboBox();
            this.HouseTo = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(4, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Телефон";
            // 
            // Phone
            // 
            this.Phone.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Phone.Location = new System.Drawing.Point(4, 41);
            this.Phone.Name = "Phone";
            this.Phone.Size = new System.Drawing.Size(100, 23);
            this.Phone.TabIndex = 0;
            this.Phone.TabStop = false;
            // 
            // ConnectPhone
            // 
            this.ConnectPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ConnectPhone.Location = new System.Drawing.Point(158, 41);
            this.ConnectPhone.Name = "ConnectPhone";
            this.ConnectPhone.Size = new System.Drawing.Size(100, 23);
            this.ConnectPhone.TabIndex = 1;
            this.ConnectPhone.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(168, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Телефон для связи";
            // 
            // AddrFrom
            // 
            this.AddrFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddrFrom.Location = new System.Drawing.Point(4, 97);
            this.AddrFrom.Name = "AddrFrom";
            this.AddrFrom.Size = new System.Drawing.Size(161, 23);
            this.AddrFrom.TabIndex = 0;
            this.AddrFrom.TextChanged += new System.EventHandler(this.AddrFrom_TextChanged);
            this.AddrFrom.Click += new System.EventHandler(this.AddrFrom_Click);
            this.AddrFrom.Leave += new System.EventHandler(this.AddrFrom_Leave);
            this.AddrFrom.Enter += new System.EventHandler(this.AddrFrom_Enter);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(4, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Откуда";
            // 
            // HouseFrom
            // 
            this.HouseFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HouseFrom.Location = new System.Drawing.Point(171, 97);
            this.HouseFrom.Name = "HouseFrom";
            this.HouseFrom.Size = new System.Drawing.Size(39, 23);
            this.HouseFrom.TabIndex = 1;
            this.HouseFrom.TextChanged += new System.EventHandler(this.HouseFrom_TextChanged);
            this.HouseFrom.Click += new System.EventHandler(this.HouseFrom_Click);
            this.HouseFrom.Leave += new System.EventHandler(this.HouseFrom_Leave);
            this.HouseFrom.Enter += new System.EventHandler(this.HouseFrom_Enter);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(171, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Дом";
            // 
            // FlatFrom
            // 
            this.FlatFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FlatFrom.Location = new System.Drawing.Point(213, 97);
            this.FlatFrom.Name = "FlatFrom";
            this.FlatFrom.Size = new System.Drawing.Size(39, 23);
            this.FlatFrom.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(213, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(22, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Кв";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(390, 81);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(106, 15);
            this.label6.TabIndex = 10;
            this.label6.Text = "Название откуда";
            // 
            // AddrFromName
            // 
            this.AddrFromName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddrFromName.Location = new System.Drawing.Point(387, 96);
            this.AddrFromName.Name = "AddrFromName";
            this.AddrFromName.Size = new System.Drawing.Size(146, 24);
            this.AddrFromName.TabIndex = 4;
            this.AddrFromName.TabStop = false;
            this.AddrFromName.SelectedValueChanged += new System.EventHandler(this.AddrFromName_SelectedValueChanged);
            this.AddrFromName.Click += new System.EventHandler(this.AddrFromName_Click);
            // 
            // ServiceID
            // 
            this.ServiceID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ServiceID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ServiceID.FormattingEnabled = true;
            this.ServiceID.Location = new System.Drawing.Point(297, 40);
            this.ServiceID.Name = "ServiceID";
            this.ServiceID.Size = new System.Drawing.Size(121, 24);
            this.ServiceID.TabIndex = 2;
            this.ServiceID.TabStop = false;
            this.ServiceID.SelectedIndexChanged += new System.EventHandler(this.ServiceID_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(300, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 15);
            this.label7.TabIndex = 12;
            this.label7.Text = "Служба";
            // 
            // AddrToName
            // 
            this.AddrToName.Enabled = false;
            this.AddrToName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddrToName.FormattingEnabled = true;
            this.AddrToName.Location = new System.Drawing.Point(890, 363);
            this.AddrToName.Name = "AddrToName";
            this.AddrToName.Size = new System.Drawing.Size(146, 24);
            this.AddrToName.TabIndex = 8;
            this.AddrToName.TabStop = false;
            this.AddrToName.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(894, 345);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 15);
            this.label8.TabIndex = 18;
            this.label8.Text = "Название куда";
            this.label8.Visible = false;
            // 
            // HouseTo2
            // 
            this.HouseTo2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HouseTo2.Location = new System.Drawing.Point(829, 117);
            this.HouseTo2.Name = "HouseTo2";
            this.HouseTo2.Size = new System.Drawing.Size(39, 23);
            this.HouseTo2.TabIndex = 6;
            this.HouseTo2.TabStop = false;
            this.HouseTo2.Visible = false;
            this.HouseTo2.Leave += new System.EventHandler(this.HouseTo_Leave);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(829, 105);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Дом";
            this.label9.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(3, 122);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(104, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "Куда (водителю)";
            // 
            // ClientID
            // 
            this.ClientID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ClientID.Location = new System.Drawing.Point(357, 225);
            this.ClientID.Name = "ClientID";
            this.ClientID.Size = new System.Drawing.Size(178, 23);
            this.ClientID.TabIndex = 10;
            this.ClientID.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(381, 210);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 15);
            this.label11.TabIndex = 22;
            this.label11.Text = "Пассажир";
            // 
            // OrgID
            // 
            this.OrgID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.OrgID.FormattingEnabled = true;
            this.OrgID.Location = new System.Drawing.Point(10, 26);
            this.OrgID.Name = "OrgID";
            this.OrgID.Size = new System.Drawing.Size(160, 24);
            this.OrgID.TabIndex = 12;
            this.OrgID.TabStop = false;
            this.OrgID.SelectedIndexChanged += new System.EventHandler(this.OrgID_SelectedIndexChanged);
            this.OrgID.TextChanged += new System.EventHandler(this.OrgID_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(8, 11);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 15);
            this.label12.TabIndex = 20;
            this.label12.Text = "Организация";
            // 
            // CarID
            // 
            this.CarID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CarID.FormattingEnabled = true;
            this.CarID.Location = new System.Drawing.Point(8, 226);
            this.CarID.Name = "CarID";
            this.CarID.Size = new System.Drawing.Size(265, 24);
            this.CarID.TabIndex = 14;
            this.CarID.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(6, 210);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(79, 15);
            this.label13.TabIndex = 24;
            this.label13.Text = "Автомобиль";
            // 
            // ZoneID
            // 
            this.ZoneID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ZoneID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ZoneID.Location = new System.Drawing.Point(258, 96);
            this.ZoneID.Name = "ZoneID";
            this.ZoneID.Size = new System.Drawing.Size(121, 24);
            this.ZoneID.TabIndex = 3;
            this.ZoneID.SelectedIndexChanged += new System.EventHandler(this.ZoneID_SelectedIndexChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(258, 81);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(87, 13);
            this.label14.TabIndex = 26;
            this.label14.Text = "Район откуда";
            // 
            // RaionToID
            // 
            this.RaionToID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.RaionToID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RaionToID.FormattingEnabled = true;
            this.RaionToID.Location = new System.Drawing.Point(258, 151);
            this.RaionToID.Name = "RaionToID";
            this.RaionToID.Size = new System.Drawing.Size(121, 24);
            this.RaionToID.TabIndex = 5;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.Location = new System.Drawing.Point(258, 135);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(74, 13);
            this.label15.TabIndex = 28;
            this.label15.Text = "Район куда";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(3, 165);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(164, 13);
            this.label16.TabIndex = 30;
            this.label16.Text = "Примечание для водителя";
            // 
            // Descr
            // 
            this.Descr.ContextMenuStrip = this.order_prim_menu;
            this.Descr.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Descr.Location = new System.Drawing.Point(6, 181);
            this.Descr.Name = "Descr";
            this.Descr.Size = new System.Drawing.Size(529, 23);
            this.Descr.TabIndex = 6;
            // 
            // order_prim_menu
            // 
            this.order_prim_menu.Name = "order_prim_menu";
            this.order_prim_menu.Size = new System.Drawing.Size(153, 26);
            this.order_prim_menu.Opening += new System.ComponentModel.CancelEventHandler(this.order_prim_menu_Opening);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.Control;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSave.Location = new System.Drawing.Point(22, 365);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(246, 25);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "На распределение/Сохранить (F2)";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnEndCall
            // 
            this.btnEndCall.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnEndCall.ForeColor = System.Drawing.Color.Red;
            this.btnEndCall.Location = new System.Drawing.Point(364, 365);
            this.btnEndCall.Name = "btnEndCall";
            this.btnEndCall.Size = new System.Drawing.Size(171, 25);
            this.btnEndCall.TabIndex = 11;
            this.btnEndCall.TabStop = false;
            this.btnEndCall.Text = "Завершить разговор";
            this.btnEndCall.UseVisualStyleBackColor = true;
            this.btnEndCall.Visible = false;
            this.btnEndCall.Click += new System.EventHandler(this.btnEndCall_Click);
            // 
            // chkPredvar
            // 
            this.chkPredvar.AutoSize = true;
            this.chkPredvar.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chkPredvar.Location = new System.Drawing.Point(425, 2);
            this.chkPredvar.Name = "chkPredvar";
            this.chkPredvar.Size = new System.Drawing.Size(88, 14);
            this.chkPredvar.TabIndex = 15;
            this.chkPredvar.TabStop = false;
            this.chkPredvar.Text = "предварительный";
            this.chkPredvar.UseVisualStyleBackColor = true;
            this.chkPredvar.CheckStateChanged += new System.EventHandler(this.chkPredvar_CheckStateChanged);
            this.chkPredvar.CheckedChanged += new System.EventHandler(this.chkPredvar_CheckedChanged);
            // 
            // dtPredvar
            // 
            this.dtPredvar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dtPredvar.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtPredvar.Location = new System.Drawing.Point(425, 16);
            this.dtPredvar.Name = "dtPredvar";
            this.dtPredvar.Size = new System.Drawing.Size(82, 23);
            this.dtPredvar.TabIndex = 18;
            this.dtPredvar.TabStop = false;
            this.dtPredvar.Visible = false;
            // 
            // tmPredvar
            // 
            this.tmPredvar.CustomFormat = "HH:mm";
            this.tmPredvar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tmPredvar.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.tmPredvar.Location = new System.Drawing.Point(425, 43);
            this.tmPredvar.Name = "tmPredvar";
            this.tmPredvar.ShowUpDown = true;
            this.tmPredvar.Size = new System.Drawing.Size(59, 23);
            this.tmPredvar.TabIndex = 19;
            this.tmPredvar.TabStop = false;
            this.tmPredvar.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dt_order_info);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(7, 396);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(374, 97);
            this.groupBox1.TabIndex = 38;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Информация по заказу";
            // 
            // dt_order_info
            // 
            this.dt_order_info.Location = new System.Drawing.Point(6, 18);
            this.dt_order_info.Name = "dt_order_info";
            this.dt_order_info.Size = new System.Drawing.Size(362, 76);
            this.dt_order_info.TabIndex = 0;
            this.dt_order_info.Text = "Поступил:\r\nТолько что\r\n";
            // 
            // btnPhoneInfo
            // 
            this.btnPhoneInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnPhoneInfo.Location = new System.Drawing.Point(105, 39);
            this.btnPhoneInfo.Name = "btnPhoneInfo";
            this.btnPhoneInfo.Size = new System.Drawing.Size(15, 23);
            this.btnPhoneInfo.TabIndex = 1;
            this.btnPhoneInfo.TabStop = false;
            this.btnPhoneInfo.Text = "И";
            this.btnPhoneInfo.UseVisualStyleBackColor = true;
            this.btnPhoneInfo.Click += new System.EventHandler(this.btnPhoneInfo_Click);
            // 
            // lbPhoneInfo
            // 
            this.lbPhoneInfo.BackColor = System.Drawing.Color.LemonChiffon;
            this.lbPhoneInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbPhoneInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbPhoneInfo.FormattingEnabled = true;
            this.lbPhoneInfo.ItemHeight = 16;
            this.lbPhoneInfo.Location = new System.Drawing.Point(5, 64);
            this.lbPhoneInfo.Name = "lbPhoneInfo";
            this.lbPhoneInfo.Size = new System.Drawing.Size(414, 2);
            this.lbPhoneInfo.TabIndex = 40;
            this.lbPhoneInfo.Visible = false;
            this.lbPhoneInfo.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lbPhoneInfo_MouseDoubleClick);
            this.lbPhoneInfo.SelectedIndexChanged += new System.EventHandler(this.lbPhoneInfo_SelectedIndexChanged);
            this.lbPhoneInfo.Leave += new System.EventHandler(this.lbPhoneInfo_Leave);
            // 
            // FromPhone
            // 
            this.FromPhone.AutoSize = true;
            this.FromPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FromPhone.Location = new System.Drawing.Point(364, 28);
            this.FromPhone.Name = "FromPhone";
            this.FromPhone.Size = new System.Drawing.Size(0, 17);
            this.FromPhone.TabIndex = 41;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label23.Location = new System.Drawing.Point(11, 49);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(68, 15);
            this.label23.TabIndex = 42;
            this.label23.Text = "Сотрудник";
            // 
            // ORGDETAIL_ID
            // 
            this.ORGDETAIL_ID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ORGDETAIL_ID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.ORGDETAIL_ID.DropDownHeight = 350;
            this.ORGDETAIL_ID.DropDownWidth = 450;
            this.ORGDETAIL_ID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ORGDETAIL_ID.FormattingEnabled = true;
            this.ORGDETAIL_ID.IntegralHeight = false;
            this.ORGDETAIL_ID.Location = new System.Drawing.Point(10, 64);
            this.ORGDETAIL_ID.MaxDropDownItems = 15;
            this.ORGDETAIL_ID.Name = "ORGDETAIL_ID";
            this.ORGDETAIL_ID.Size = new System.Drawing.Size(338, 21);
            this.ORGDETAIL_ID.Sorted = true;
            this.ORGDETAIL_ID.TabIndex = 13;
            this.ORGDETAIL_ID.TabStop = false;
            this.ORGDETAIL_ID.TextChanged += new System.EventHandler(this.ORGDETAIL_ID_TextChanged);
            // 
            // phone_info
            // 
            this.phone_info.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.phone_info.Location = new System.Drawing.Point(7, -2);
            this.phone_info.Name = "phone_info";
            this.phone_info.Size = new System.Drawing.Size(412, 22);
            this.phone_info.TabIndex = 43;
            // 
            // AddrToForDriver
            // 
            this.AddrToForDriver.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddrToForDriver.FormattingEnabled = true;
            this.AddrToForDriver.Location = new System.Drawing.Point(4, 138);
            this.AddrToForDriver.Name = "AddrToForDriver";
            this.AddrToForDriver.Size = new System.Drawing.Size(161, 24);
            this.AddrToForDriver.TabIndex = 4;
            this.AddrToForDriver.SelectedIndexChanged += new System.EventHandler(this.AddrToForDriver_SelectedIndexChanged);
            this.AddrToForDriver.Leave += new System.EventHandler(this.AddrToForDriver_Leave);
            this.AddrToForDriver.TextChanged += new System.EventHandler(this.AddrToForDriver_TextChanged);
            // 
            // chk_EndOrder
            // 
            this.chk_EndOrder.AutoSize = true;
            this.chk_EndOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chk_EndOrder.Location = new System.Drawing.Point(392, 474);
            this.chk_EndOrder.Name = "chk_EndOrder";
            this.chk_EndOrder.Size = new System.Drawing.Size(127, 19);
            this.chk_EndOrder.TabIndex = 1;
            this.chk_EndOrder.TabStop = false;
            this.chk_EndOrder.Text = "Завершить заказ";
            this.chk_EndOrder.UseVisualStyleBackColor = true;
            this.chk_EndOrder.CheckedChanged += new System.EventHandler(this.chk_EndOrder_CheckedChanged);
            // 
            // comboEndState
            // 
            this.comboEndState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboEndState.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboEndState.FormattingEnabled = true;
            this.comboEndState.Items.AddRange(new object[] {
            "Успешно",
            "Снят",
            "Снят (Нет машин)",
            "Снят (абонент недоступен)"});
            this.comboEndState.Location = new System.Drawing.Point(392, 444);
            this.comboEndState.Name = "comboEndState";
            this.comboEndState.Size = new System.Drawing.Size(130, 24);
            this.comboEndState.TabIndex = 44;
            this.comboEndState.TabStop = false;
            this.comboEndState.Visible = false;
            // 
            // btnEndOrder
            // 
            this.btnEndOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnEndOrder.Location = new System.Drawing.Point(392, 414);
            this.btnEndOrder.Name = "btnEndOrder";
            this.btnEndOrder.Size = new System.Drawing.Size(129, 24);
            this.btnEndOrder.TabIndex = 45;
            this.btnEndOrder.TabStop = false;
            this.btnEndOrder.Text = "Завершить заказ";
            this.btnEndOrder.UseVisualStyleBackColor = true;
            this.btnEndOrder.Visible = false;
            this.btnEndOrder.Click += new System.EventHandler(this.btnEndOrder_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(383, 49);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(43, 15);
            this.label17.TabIndex = 46;
            this.label17.Text = "сумма";
            // 
            // summ
            // 
            this.summ.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.summ.Location = new System.Drawing.Point(386, 64);
            this.summ.Name = "summ";
            this.summ.Size = new System.Drawing.Size(72, 23);
            this.summ.TabIndex = 47;
            this.summ.TabStop = false;
            // 
            // btnSumtoDriver
            // 
            this.btnSumtoDriver.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSumtoDriver.Location = new System.Drawing.Point(463, 64);
            this.btnSumtoDriver.Name = "btnSumtoDriver";
            this.btnSumtoDriver.Size = new System.Drawing.Size(23, 23);
            this.btnSumtoDriver.TabIndex = 48;
            this.btnSumtoDriver.TabStop = false;
            this.btnSumtoDriver.Text = ">>";
            this.btnSumtoDriver.UseVisualStyleBackColor = true;
            this.btnSumtoDriver.Visible = false;
            this.btnSumtoDriver.Click += new System.EventHandler(this.btnSumtoDriver_Click);
            // 
            // CarIDView
            // 
            this.CarIDView.BackColor = System.Drawing.Color.LightBlue;
            this.CarIDView.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CarIDView.Location = new System.Drawing.Point(7, 226);
            this.CarIDView.Name = "CarIDView";
            this.CarIDView.ReadOnly = true;
            this.CarIDView.Size = new System.Drawing.Size(261, 23);
            this.CarIDView.TabIndex = 49;
            // 
            // btnCarDial
            // 
            this.btnCarDial.Location = new System.Drawing.Point(278, 226);
            this.btnCarDial.Name = "btnCarDial";
            this.btnCarDial.Size = new System.Drawing.Size(28, 23);
            this.btnCarDial.TabIndex = 50;
            this.btnCarDial.TabStop = false;
            this.btnCarDial.Text = "Т";
            this.btnCarDial.UseVisualStyleBackColor = true;
            this.btnCarDial.Click += new System.EventHandler(this.btnCarDial_Click);
            // 
            // btnCarMessage
            // 
            this.btnCarMessage.Location = new System.Drawing.Point(310, 226);
            this.btnCarMessage.Name = "btnCarMessage";
            this.btnCarMessage.Size = new System.Drawing.Size(31, 23);
            this.btnCarMessage.TabIndex = 51;
            this.btnCarMessage.TabStop = false;
            this.btnCarMessage.Text = "С";
            this.btnCarMessage.UseVisualStyleBackColor = true;
            this.btnCarMessage.Click += new System.EventHandler(this.btnCarMessage_Click);
            // 
            // btnPhone1Dial
            // 
            this.btnPhone1Dial.Location = new System.Drawing.Point(121, 40);
            this.btnPhone1Dial.Name = "btnPhone1Dial";
            this.btnPhone1Dial.Size = new System.Drawing.Size(28, 23);
            this.btnPhone1Dial.TabIndex = 52;
            this.btnPhone1Dial.TabStop = false;
            this.btnPhone1Dial.Text = "Т";
            this.btnPhone1Dial.UseVisualStyleBackColor = true;
            this.btnPhone1Dial.Click += new System.EventHandler(this.btnPhone1Dial_Click);
            // 
            // btnPhone2Dial
            // 
            this.btnPhone2Dial.Location = new System.Drawing.Point(260, 40);
            this.btnPhone2Dial.Name = "btnPhone2Dial";
            this.btnPhone2Dial.Size = new System.Drawing.Size(28, 23);
            this.btnPhone2Dial.TabIndex = 53;
            this.btnPhone2Dial.TabStop = false;
            this.btnPhone2Dial.Text = "Т";
            this.btnPhone2Dial.UseVisualStyleBackColor = true;
            this.btnPhone2Dial.Click += new System.EventHandler(this.btnPhone2Dial_Click);
            // 
            // TalonNumber
            // 
            this.TalonNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TalonNumber.Location = new System.Drawing.Point(385, 26);
            this.TalonNumber.MaxLength = 10;
            this.TalonNumber.Name = "TalonNumber";
            this.TalonNumber.Size = new System.Drawing.Size(101, 23);
            this.TalonNumber.TabIndex = 55;
            this.TalonNumber.TabStop = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label18.Location = new System.Drawing.Point(382, 11);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(42, 15);
            this.label18.TabIndex = 54;
            this.label18.Text = "Талон";
            // 
            // расчетМаршрута
            // 
            this.расчетМаршрута.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.расчетМаршрута.Location = new System.Drawing.Point(637, 361);
            this.расчетМаршрута.Name = "расчетМаршрута";
            this.расчетМаршрута.Size = new System.Drawing.Size(65, 24);
            this.расчетМаршрута.TabIndex = 56;
            this.расчетМаршрута.TabStop = false;
            this.расчетМаршрута.Text = "расчет Маршрута";
            this.расчетМаршрута.UseVisualStyleBackColor = true;
            this.расчетМаршрута.Visible = false;
            this.расчетМаршрута.Click += new System.EventHandler(this.расчетМаршрута_Click);
            // 
            // chkYaroslavl1
            // 
            this.chkYaroslavl1.AutoSize = true;
            this.chkYaroslavl1.Checked = true;
            this.chkYaroslavl1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkYaroslavl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chkYaroslavl1.Location = new System.Drawing.Point(101, 80);
            this.chkYaroslavl1.Name = "chkYaroslavl1";
            this.chkYaroslavl1.Size = new System.Drawing.Size(64, 14);
            this.chkYaroslavl1.TabIndex = 59;
            this.chkYaroslavl1.TabStop = false;
            this.chkYaroslavl1.Text = "Ярославль";
            this.chkYaroslavl1.UseVisualStyleBackColor = true;
            // 
            // chkYaroslavl2
            // 
            this.chkYaroslavl2.AutoSize = true;
            this.chkYaroslavl2.Checked = true;
            this.chkYaroslavl2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkYaroslavl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chkYaroslavl2.Location = new System.Drawing.Point(759, 104);
            this.chkYaroslavl2.Name = "chkYaroslavl2";
            this.chkYaroslavl2.Size = new System.Drawing.Size(64, 14);
            this.chkYaroslavl2.TabIndex = 60;
            this.chkYaroslavl2.TabStop = false;
            this.chkYaroslavl2.Text = "Ярославль";
            this.chkYaroslavl2.UseVisualStyleBackColor = true;
            this.chkYaroslavl2.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ORGDETAIL_ID);
            this.groupBox2.Controls.Add(this.OrgID);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.TalonNumber);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.summ);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.btnSumtoDriver);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox2.Location = new System.Drawing.Point(7, 255);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(528, 104);
            this.groupBox2.TabIndex = 61;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "БЕЗНАЛ";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // AddrTo
            // 
            this.AddrTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddrTo.FormattingEnabled = true;
            this.AddrTo.Location = new System.Drawing.Point(662, 116);
            this.AddrTo.Name = "AddrTo";
            this.AddrTo.Size = new System.Drawing.Size(161, 24);
            this.AddrTo.TabIndex = 62;
            this.AddrTo.TabStop = false;
            this.AddrTo.Visible = false;
            this.AddrTo.Leave += new System.EventHandler(this.AddrTo_Leave);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label19.Location = new System.Drawing.Point(662, 105);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(70, 13);
            this.label19.TabIndex = 63;
            this.label19.Text = "Куда (адрес)";
            this.label19.Visible = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Strikeout, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label20.Location = new System.Drawing.Point(3, 201);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(535, 13);
            this.label20.TabIndex = 64;
            this.label20.Text = "                                                                                 " +
                "                                                                                " +
                "               ";
            // 
            // DistanceMoney
            // 
            this.DistanceMoney.AutoSize = true;
            this.DistanceMoney.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DistanceMoney.Location = new System.Drawing.Point(714, 368);
            this.DistanceMoney.Name = "DistanceMoney";
            this.DistanceMoney.Size = new System.Drawing.Size(16, 17);
            this.DistanceMoney.TabIndex = 65;
            this.DistanceMoney.Text = "0";
            this.DistanceMoney.Visible = false;
            // 
            // label21
            // 
            this.label21.Location = new System.Drawing.Point(542, 182);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(219, 51);
            this.label21.TabIndex = 66;
            this.label21.Text = "расчет ведется в тестовом режиме. клиента об этом предупреждать.";
            this.label21.Visible = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(639, 347);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(117, 13);
            this.label22.TabIndex = 67;
            this.label22.Text = "Формула цены 80+10";
            this.label22.Visible = false;
            // 
            // predvShowDelta
            // 
            this.predvShowDelta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.predvShowDelta.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.predvShowDelta.FormattingEnabled = true;
            this.predvShowDelta.Items.AddRange(new object[] {
            "20",
            "30",
            "40"});
            this.predvShowDelta.Location = new System.Drawing.Point(490, 43);
            this.predvShowDelta.Name = "predvShowDelta";
            this.predvShowDelta.Size = new System.Drawing.Size(41, 24);
            this.predvShowDelta.TabIndex = 68;
            this.predvShowDelta.Visible = false;
            // 
            // HouseTo
            // 
            this.HouseTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HouseTo.Location = new System.Drawing.Point(637, 282);
            this.HouseTo.Name = "HouseTo";
            this.HouseTo.Size = new System.Drawing.Size(39, 23);
            this.HouseTo.TabIndex = 8;
            this.HouseTo.Visible = false;
            this.HouseTo.TextChanged += new System.EventHandler(this.HouseTo_TextChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label24.Location = new System.Drawing.Point(637, 266);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(33, 13);
            this.label24.TabIndex = 70;
            this.label24.Text = "Дом";
            this.label24.Visible = false;
            this.label24.Click += new System.EventHandler(this.label24_Click);
            // 
            // order
            // 
            this.ClientSize = new System.Drawing.Size(542, 497);
            this.Controls.Add(this.HouseTo);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.predvShowDelta);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.DistanceMoney);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.AddrTo);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.CarID);
            this.Controls.Add(this.расчетМаршрута);
            this.Controls.Add(this.btnPhone2Dial);
            this.Controls.Add(this.btnPhone1Dial);
            this.Controls.Add(this.btnCarMessage);
            this.Controls.Add(this.btnCarDial);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.btnEndOrder);
            this.Controls.Add(this.comboEndState);
            this.Controls.Add(this.chk_EndOrder);
            this.Controls.Add(this.AddrToForDriver);
            this.Controls.Add(this.phone_info);
            this.Controls.Add(this.FromPhone);
            this.Controls.Add(this.lbPhoneInfo);
            this.Controls.Add(this.btnPhoneInfo);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tmPredvar);
            this.Controls.Add(this.dtPredvar);
            this.Controls.Add(this.chkPredvar);
            this.Controls.Add(this.btnEndCall);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.Descr);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.RaionToID);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.ZoneID);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.ClientID);
            this.Controls.Add(this.AddrToName);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.HouseTo2);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.ServiceID);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.AddrFromName);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.FlatFrom);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.HouseFrom);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.AddrFrom);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ConnectPhone);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Phone);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CarIDView);
            this.Controls.Add(this.chkYaroslavl1);
            this.Controls.Add(this.chkYaroslavl2);
            this.Controls.Add(this.label20);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "order";
            this.ShowInTaskbar = false;
            this.Text = "Новый заказ";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.order_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.order_MouseDown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.order_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.order_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Phone;
        private System.Windows.Forms.TextBox ConnectPhone;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox AddrFrom;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox HouseFrom;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox FlatFrom;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox AddrFromName;
        private System.Windows.Forms.ComboBox ServiceID;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox AddrToName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox HouseTo2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox ClientID;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox OrgID;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox CarID;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox ZoneID;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox RaionToID;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox Descr;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnEndCall;
        private System.Windows.Forms.CheckBox chkPredvar;
        private System.Windows.Forms.DateTimePicker dtPredvar;
        private System.Windows.Forms.DateTimePicker tmPredvar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label dt_order_info;
        private System.Windows.Forms.Button btnPhoneInfo;
        private System.Windows.Forms.ListBox lbPhoneInfo;
        private System.Windows.Forms.Label FromPhone;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox ORGDETAIL_ID;
        private System.Windows.Forms.Label phone_info;
        private System.Windows.Forms.ComboBox AddrToForDriver;
        private System.Windows.Forms.CheckBox chk_EndOrder;
        private System.Windows.Forms.ComboBox comboEndState;
        private System.Windows.Forms.Button btnEndOrder;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox summ;
        private System.Windows.Forms.Button btnSumtoDriver;
        private System.Windows.Forms.TextBox CarIDView;
        private System.Windows.Forms.Button btnCarDial;
        private System.Windows.Forms.Button btnCarMessage;
        private System.Windows.Forms.Button btnPhone1Dial;
        private System.Windows.Forms.Button btnPhone2Dial;
        private System.Windows.Forms.TextBox TalonNumber;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button расчетМаршрута;
        private System.Windows.Forms.CheckBox chkYaroslavl1;
        private System.Windows.Forms.CheckBox chkYaroslavl2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ComboBox AddrTo;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label DistanceMoney;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox predvShowDelta;
        private System.Windows.Forms.TextBox HouseTo;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ContextMenuStrip order_prim_menu;
    }
}
