﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ONLINEDISPATCHER {
    public partial class bnal_akt_sverki : Form {
        public bnal_akt_sverki() {
            InitializeComponent();
        }

        private void bnal_akt_sverki_Load(object sender, EventArgs e) {
            this.OrgID.DataSource = DataBase.mssqlRead("SELECT * FROM ORGANIZATIONS WHERE AGREEMENTSTATUS=1 order by name");
            OrgID.DisplayMember = "Name";
            OrgID.ValueMember = "ID";

            OrgID.AutoCompleteSource = AutoCompleteSource.ListItems;
            OrgID.AutoCompleteMode = AutoCompleteMode.Suggest;
            OrgID.SelectedIndex = -1;


        }

        private void button1_Click(object sender, EventArgs e) {
            
            List<SqlParameter> parameters = new List<SqlParameter>();

            
            parameters.Add(new SqlParameter("dt1", dateTimePicker1.Value.Date));
            parameters.Add(new SqlParameter("dt2", dateTimePicker2.Value.Date));
            parameters.Add(new SqlParameter("orgid", (int)OrgID.SelectedValue));

            

            DataTable dt = DataBase.mssqlRead("SELECT * FROM GET_BNAL_AKT_SWERKI(@dt1, @dt2, @orgid)", parameters.ToArray());

            dataSet1.Tables[0].Clear();



            foreach (DataRow row in dt.Select("", "dt")) {
                dataSet1.Tables[0].ImportRow(row);
            }
            

            report1.Show();
            


        }
    }
}
