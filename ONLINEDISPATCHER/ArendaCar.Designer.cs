﻿namespace ONLINEDISPATCHER
{
    partial class ArendaCar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label_balance = new System.Windows.Forms.Label();
            this.button_add_money_to_balance = new System.Windows.Forms.Button();
            this.label_disposed_sum = new System.Windows.Forms.Label();
            this.button_save = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.gsdfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sdgsdfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label5 = new System.Windows.Forms.Label();
            this.label_balance_info = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.btn_dispose_1_smena = new System.Windows.Forms.Button();
            this.btn_dispose_2_smena = new System.Windows.Forms.Button();
            this.btn_dispose_all_balance = new System.Windows.Forms.Button();
            this.chk_opl_detail = new System.Windows.Forms.CheckBox();
            this.chk_disposed_detail = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.dataSet1 = new System.Data.DataSet();
            this.dataTable1 = new System.Data.DataTable();
            this.sum_type = new System.Data.DataColumn();
            this.sum_name = new System.Data.DataColumn();
            this.plan_sum = new System.Data.DataColumn();
            this.opl_sum = new System.Data.DataColumn();
            this.disposed_sum = new System.Data.DataColumn();
            this.comment = new System.Data.DataColumn();
            this.balance1 = new System.Data.DataColumn();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.sumtypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sumnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.plansumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.oplsumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.disposedsumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.commentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.cmbArendaCars = new SergeUtils.EasyCompletionComboBox();
            this.cmbDrivers = new SergeUtils.EasyCompletionComboBox();
            this.label_debug = new System.Windows.Forms.Label();
            this.btnReports = new System.Windows.Forms.Button();
            this.btnReload = new System.Windows.Forms.Button();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 36;
            this.label1.Text = "Арендная машина";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 37;
            this.label3.Text = "Водитель";
            // 
            // label_balance
            // 
            this.label_balance.AutoSize = true;
            this.label_balance.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_balance.Location = new System.Drawing.Point(487, 9);
            this.label_balance.Name = "label_balance";
            this.label_balance.Size = new System.Drawing.Size(180, 24);
            this.label_balance.TabIndex = 41;
            this.label_balance.Text = "Кошелек: 800 руб";
            // 
            // button_add_money_to_balance
            // 
            this.button_add_money_to_balance.BackColor = System.Drawing.SystemColors.Control;
            this.button_add_money_to_balance.Location = new System.Drawing.Point(693, 8);
            this.button_add_money_to_balance.Name = "button_add_money_to_balance";
            this.button_add_money_to_balance.Size = new System.Drawing.Size(157, 23);
            this.button_add_money_to_balance.TabIndex = 42;
            this.button_add_money_to_balance.Text = "Внести / снять наличные";
            this.button_add_money_to_balance.UseVisualStyleBackColor = false;
            this.button_add_money_to_balance.Click += new System.EventHandler(this.button_add_money_to_balance_Click);
            // 
            // label_disposed_sum
            // 
            this.label_disposed_sum.AutoSize = true;
            this.label_disposed_sum.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_disposed_sum.Location = new System.Drawing.Point(689, 61);
            this.label_disposed_sum.Name = "label_disposed_sum";
            this.label_disposed_sum.Size = new System.Drawing.Size(181, 20);
            this.label_disposed_sum.TabIndex = 45;
            this.label_disposed_sum.Text = "Оплата сейчас 1900";
            this.label_disposed_sum.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // button_save
            // 
            this.button_save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_save.Location = new System.Drawing.Point(312, 661);
            this.button_save.Name = "button_save";
            this.button_save.Size = new System.Drawing.Size(208, 23);
            this.button_save.TabIndex = 46;
            this.button_save.Text = "Сохранить график  и оплатить смены";
            this.button_save.UseVisualStyleBackColor = true;
            this.button_save.Click += new System.EventHandler(this.button3_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.toolStripTextBox1,
            this.toolStripComboBox1,
            this.gsdfToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(182, 84);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(178, 6);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 23);
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(121, 23);
            // 
            // gsdfToolStripMenuItem
            // 
            this.gsdfToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sdgsdfToolStripMenuItem});
            this.gsdfToolStripMenuItem.Name = "gsdfToolStripMenuItem";
            this.gsdfToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.gsdfToolStripMenuItem.Text = "gsdf";
            // 
            // sdgsdfToolStripMenuItem
            // 
            this.sdgsdfToolStripMenuItem.Name = "sdgsdfToolStripMenuItem";
            this.sdgsdfToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.sdgsdfToolStripMenuItem.Text = "sdgsdf";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(126, 13);
            this.label5.TabIndex = 49;
            this.label5.Text = "График работы и оплат";
            // 
            // label_balance_info
            // 
            this.label_balance_info.AutoSize = true;
            this.label_balance_info.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_balance_info.ForeColor = System.Drawing.Color.Red;
            this.label_balance_info.Location = new System.Drawing.Point(488, 39);
            this.label_balance_info.Name = "label_balance_info";
            this.label_balance_info.Size = new System.Drawing.Size(275, 17);
            this.label_balance_info.TabIndex = 50;
            this.label_balance_info.Text = "Недостаточно денег на в кошельке";
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Interval = 20;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // btn_dispose_1_smena
            // 
            this.btn_dispose_1_smena.Location = new System.Drawing.Point(490, 58);
            this.btn_dispose_1_smena.Name = "btn_dispose_1_smena";
            this.btn_dispose_1_smena.Size = new System.Drawing.Size(51, 23);
            this.btn_dispose_1_smena.TabIndex = 52;
            this.btn_dispose_1_smena.Text = "1 смена";
            this.btn_dispose_1_smena.UseVisualStyleBackColor = true;
            this.btn_dispose_1_smena.Click += new System.EventHandler(this.btn_dispose_1_smena_Click);
            // 
            // btn_dispose_2_smena
            // 
            this.btn_dispose_2_smena.Location = new System.Drawing.Point(548, 58);
            this.btn_dispose_2_smena.Name = "btn_dispose_2_smena";
            this.btn_dispose_2_smena.Size = new System.Drawing.Size(54, 23);
            this.btn_dispose_2_smena.TabIndex = 53;
            this.btn_dispose_2_smena.Text = "2 смены";
            this.btn_dispose_2_smena.UseVisualStyleBackColor = true;
            this.btn_dispose_2_smena.Click += new System.EventHandler(this.btn_dispose_2_smena_Click);
            // 
            // btn_dispose_all_balance
            // 
            this.btn_dispose_all_balance.Location = new System.Drawing.Point(608, 58);
            this.btn_dispose_all_balance.Name = "btn_dispose_all_balance";
            this.btn_dispose_all_balance.Size = new System.Drawing.Size(75, 23);
            this.btn_dispose_all_balance.TabIndex = 54;
            this.btn_dispose_all_balance.Text = "Все что есть";
            this.btn_dispose_all_balance.UseVisualStyleBackColor = true;
            this.btn_dispose_all_balance.Click += new System.EventHandler(this.btn_dispose_all_balance_Click);
            // 
            // chk_opl_detail
            // 
            this.chk_opl_detail.AutoSize = true;
            this.chk_opl_detail.Location = new System.Drawing.Point(6, 12);
            this.chk_opl_detail.Name = "chk_opl_detail";
            this.chk_opl_detail.Size = new System.Drawing.Size(81, 17);
            this.chk_opl_detail.TabIndex = 57;
            this.chk_opl_detail.Text = "Ранее опл.";
            this.chk_opl_detail.UseVisualStyleBackColor = true;
            this.chk_opl_detail.CheckedChanged += new System.EventHandler(this.chk_opl_detail_CheckedChanged);
            // 
            // chk_disposed_detail
            // 
            this.chk_disposed_detail.AutoSize = true;
            this.chk_disposed_detail.Location = new System.Drawing.Point(82, 12);
            this.chk_disposed_detail.Name = "chk_disposed_detail";
            this.chk_disposed_detail.Size = new System.Drawing.Size(87, 17);
            this.chk_disposed_detail.TabIndex = 58;
            this.chk_disposed_detail.Text = "Опл. сейчас";
            this.chk_disposed_detail.UseVisualStyleBackColor = true;
            this.chk_disposed_detail.CheckedChanged += new System.EventHandler(this.chk_disposed_detail_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.Location = new System.Drawing.Point(12, 661);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(125, 23);
            this.button1.TabIndex = 59;
            this.button1.Text = "Сохранить график";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "NewDataSet";
            this.dataSet1.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable1});
            // 
            // dataTable1
            // 
            this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.sum_type,
            this.sum_name,
            this.plan_sum,
            this.opl_sum,
            this.disposed_sum,
            this.comment,
            this.balance1});
            this.dataTable1.TableName = "Table1";
            // 
            // sum_type
            // 
            this.sum_type.ColumnName = "sum_type";
            this.sum_type.DataType = typeof(int);
            // 
            // sum_name
            // 
            this.sum_name.ColumnName = "sum_name";
            // 
            // plan_sum
            // 
            this.plan_sum.ColumnName = "plan_sum";
            this.plan_sum.DataType = typeof(int);
            // 
            // opl_sum
            // 
            this.opl_sum.ColumnName = "opl_sum";
            this.opl_sum.DataType = typeof(int);
            // 
            // disposed_sum
            // 
            this.disposed_sum.ColumnName = "disposed_sum";
            this.disposed_sum.DataType = typeof(int);
            // 
            // comment
            // 
            this.comment.ColumnName = "comment";
            // 
            // balance1
            // 
            this.balance1.ColumnName = "balance1";
            this.balance1.DataType = typeof(int);
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToAddRows = false;
            this.dataGridView3.AllowUserToDeleteRows = false;
            this.dataGridView3.AutoGenerateColumns = false;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.sumtypeDataGridViewTextBoxColumn,
            this.sumnameDataGridViewTextBoxColumn,
            this.plansumDataGridViewTextBoxColumn,
            this.oplsumDataGridViewTextBoxColumn,
            this.disposedsumDataGridViewTextBoxColumn,
            this.commentDataGridViewTextBoxColumn,
            this.dataGridViewTextBoxColumn1});
            this.dataGridView3.DataMember = "Table1";
            this.dataGridView3.DataSource = this.dataSet1;
            this.dataGridView3.Location = new System.Drawing.Point(858, -4);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.RowHeadersVisible = false;
            this.dataGridView3.RowTemplate.Height = 24;
            this.dataGridView3.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dataGridView3.Size = new System.Drawing.Size(211, 107);
            this.dataGridView3.TabIndex = 60;
            this.dataGridView3.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView3_CellValueChanged);
            this.dataGridView3.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView3_CellFormatting);
            this.dataGridView3.CurrentCellDirtyStateChanged += new System.EventHandler(this.dataGridView3_CurrentCellDirtyStateChanged);
            this.dataGridView3.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView3_DataError);
            // 
            // sumtypeDataGridViewTextBoxColumn
            // 
            this.sumtypeDataGridViewTextBoxColumn.DataPropertyName = "sum_type";
            this.sumtypeDataGridViewTextBoxColumn.HeaderText = "sum_type";
            this.sumtypeDataGridViewTextBoxColumn.Name = "sumtypeDataGridViewTextBoxColumn";
            this.sumtypeDataGridViewTextBoxColumn.ReadOnly = true;
            this.sumtypeDataGridViewTextBoxColumn.Visible = false;
            // 
            // sumnameDataGridViewTextBoxColumn
            // 
            this.sumnameDataGridViewTextBoxColumn.DataPropertyName = "sum_name";
            this.sumnameDataGridViewTextBoxColumn.HeaderText = "Тип";
            this.sumnameDataGridViewTextBoxColumn.Name = "sumnameDataGridViewTextBoxColumn";
            this.sumnameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // plansumDataGridViewTextBoxColumn
            // 
            this.plansumDataGridViewTextBoxColumn.DataPropertyName = "plan_sum";
            this.plansumDataGridViewTextBoxColumn.HeaderText = "План";
            this.plansumDataGridViewTextBoxColumn.Name = "plansumDataGridViewTextBoxColumn";
            this.plansumDataGridViewTextBoxColumn.ReadOnly = true;
            this.plansumDataGridViewTextBoxColumn.Visible = false;
            this.plansumDataGridViewTextBoxColumn.Width = 65;
            // 
            // oplsumDataGridViewTextBoxColumn
            // 
            this.oplsumDataGridViewTextBoxColumn.DataPropertyName = "opl_sum";
            this.oplsumDataGridViewTextBoxColumn.HeaderText = "Ранее оплачено";
            this.oplsumDataGridViewTextBoxColumn.Name = "oplsumDataGridViewTextBoxColumn";
            this.oplsumDataGridViewTextBoxColumn.ReadOnly = true;
            this.oplsumDataGridViewTextBoxColumn.Visible = false;
            this.oplsumDataGridViewTextBoxColumn.Width = 65;
            // 
            // disposedsumDataGridViewTextBoxColumn
            // 
            this.disposedsumDataGridViewTextBoxColumn.DataPropertyName = "disposed_sum";
            this.disposedsumDataGridViewTextBoxColumn.HeaderText = "Оплата сейчас";
            this.disposedsumDataGridViewTextBoxColumn.Name = "disposedsumDataGridViewTextBoxColumn";
            this.disposedsumDataGridViewTextBoxColumn.Visible = false;
            this.disposedsumDataGridViewTextBoxColumn.Width = 65;
            // 
            // commentDataGridViewTextBoxColumn
            // 
            this.commentDataGridViewTextBoxColumn.DataPropertyName = "comment";
            this.commentDataGridViewTextBoxColumn.HeaderText = "comment";
            this.commentDataGridViewTextBoxColumn.Name = "commentDataGridViewTextBoxColumn";
            this.commentDataGridViewTextBoxColumn.Visible = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "balance1";
            this.dataGridViewTextBoxColumn1.HeaderText = "Долг/предоплата";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 125;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chk_opl_detail);
            this.groupBox1.Controls.Add(this.chk_disposed_detail);
            this.groupBox1.Location = new System.Drawing.Point(439, 89);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(170, 30);
            this.groupBox1.TabIndex = 61;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Подробно";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(15, 98);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(94, 20);
            this.dateTimePicker1.TabIndex = 63;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker2.Location = new System.Drawing.Point(129, 98);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(94, 20);
            this.dateTimePicker2.TabIndex = 64;
            this.dateTimePicker2.ValueChanged += new System.EventHandler(this.dateTimePicker2_ValueChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(293, 98);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(67, 20);
            this.button2.TabIndex = 65;
            this.button2.Text = "+-7д";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(357, 126);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(67, 20);
            this.button3.TabIndex = 66;
            this.button3.Text = "с долгов";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(228, 98);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(62, 20);
            this.button4.TabIndex = 67;
            this.button4.Text = "1-31";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // cmbArendaCars
            // 
            this.cmbArendaCars.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmbArendaCars.FormattingEnabled = true;
            this.cmbArendaCars.Location = new System.Drawing.Point(117, 44);
            this.cmbArendaCars.Name = "cmbArendaCars";
            this.cmbArendaCars.Size = new System.Drawing.Size(361, 24);
            this.cmbArendaCars.TabIndex = 68;
            this.cmbArendaCars.SelectedValueChanged += new System.EventHandler(this.cmbCars_SelectedValueChanged);
            // 
            // cmbDrivers
            // 
            this.cmbDrivers.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmbDrivers.FormattingEnabled = true;
            this.cmbDrivers.Location = new System.Drawing.Point(117, 10);
            this.cmbDrivers.Name = "cmbDrivers";
            this.cmbDrivers.Size = new System.Drawing.Size(361, 28);
            this.cmbDrivers.TabIndex = 69;
            this.cmbDrivers.SelectedValueChanged += new System.EventHandler(this.cmbDrivers_SelectedValueChanged);
            // 
            // label_debug
            // 
            this.label_debug.AutoSize = true;
            this.label_debug.Location = new System.Drawing.Point(856, 106);
            this.label_debug.Name = "label_debug";
            this.label_debug.Size = new System.Drawing.Size(35, 13);
            this.label_debug.TabIndex = 71;
            this.label_debug.Text = "label2";
            // 
            // btnReports
            // 
            this.btnReports.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnReports.Location = new System.Drawing.Point(590, 661);
            this.btnReports.Name = "btnReports";
            this.btnReports.Size = new System.Drawing.Size(75, 23);
            this.btnReports.TabIndex = 72;
            this.btnReports.Text = "Отчеты";
            this.btnReports.UseVisualStyleBackColor = true;
            this.btnReports.Click += new System.EventHandler(this.btnReports_Click);
            // 
            // btnReload
            // 
            this.btnReload.Location = new System.Drawing.Point(366, 98);
            this.btnReload.Name = "btnReload";
            this.btnReload.Size = new System.Drawing.Size(67, 20);
            this.btnReload.TabIndex = 73;
            this.btnReload.Text = "Обновить";
            this.btnReload.UseVisualStyleBackColor = true;
            this.btnReload.Click += new System.EventHandler(this.btnReload_Click);
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToOrderColumns = true;
            this.dataGridView2.AllowUserToResizeRows = false;
            this.dataGridView2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.ContextMenuStrip = this.contextMenuStrip1;
            this.dataGridView2.Location = new System.Drawing.Point(12, 139);
            this.dataGridView2.MultiSelect = false;
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.RowTemplate.Height = 24;
            this.dataGridView2.Size = new System.Drawing.Size(1057, 516);
            this.dataGridView2.TabIndex = 47;
            this.dataGridView2.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellValueChanged);
            this.dataGridView2.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView2_CellMouseDown);
            this.dataGridView2.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView2_CellFormatting_1);
            this.dataGridView2.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellEndEdit);
            this.dataGridView2.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dataGridView2_CellPainting);
            this.dataGridView2.RowHeadersWidthChanged += new System.EventHandler(this.dataGridView2_RowHeadersWidthChanged);
            this.dataGridView2.CurrentCellDirtyStateChanged += new System.EventHandler(this.dataGridView2_CurrentCellDirtyStateChanged);
            this.dataGridView2.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView2_DataError);
            this.dataGridView2.ColumnWidthChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dataGridView2_ColumnWidthChanged);
            this.dataGridView2.SelectionChanged += new System.EventHandler(this.dataGridView2_SelectionChanged);
            this.dataGridView2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellContentClick);
            // 
            // ArendaCar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1072, 688);
            this.Controls.Add(this.btnReload);
            this.Controls.Add(this.btnReports);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.cmbDrivers);
            this.Controls.Add(this.cmbArendaCars);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label_debug);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView3);
            this.Controls.Add(this.btn_dispose_all_balance);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label_balance_info);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btn_dispose_2_smena);
            this.Controls.Add(this.btn_dispose_1_smena);
            this.Controls.Add(this.button_save);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_add_money_to_balance);
            this.Controls.Add(this.label_disposed_sum);
            this.Controls.Add(this.label_balance);
            this.Controls.Add(this.label3);
            this.Name = "ArendaCar";
            this.Text = "Ведение аренды (F6)";
            this.Load += new System.EventHandler(this.ArendaCar_Load);
            this.Shown += new System.EventHandler(this.ArendaCar_Shown);
            this.contextMenuStrip1.ResumeLayout(false);
            this.contextMenuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label_balance;
        private System.Windows.Forms.Button button_add_money_to_balance;
        private System.Windows.Forms.Label label_disposed_sum;
        private System.Windows.Forms.Button button_save;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label_balance_info;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Button btn_dispose_1_smena;
        private System.Windows.Forms.Button btn_dispose_2_smena;
        private System.Windows.Forms.Button btn_dispose_all_balance;
        private System.Windows.Forms.CheckBox chk_opl_detail;
        private System.Windows.Forms.CheckBox chk_disposed_detail;
        private System.Windows.Forms.Button button1;
        private System.Data.DataSet dataSet1;
        private System.Data.DataTable dataTable1;
        private System.Data.DataColumn sum_type;
        private System.Data.DataColumn sum_name;
        private System.Data.DataColumn plan_sum;
        private System.Data.DataColumn opl_sum;
        private System.Data.DataColumn disposed_sum;
        private System.Data.DataColumn comment;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Data.DataColumn balance1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private SergeUtils.EasyCompletionComboBox cmbArendaCars;
        private SergeUtils.EasyCompletionComboBox cmbDrivers;
        private System.Windows.Forms.Label label_debug;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Button btnReports;
        private System.Windows.Forms.Button btnReload;
        private System.Windows.Forms.DataGridViewTextBoxColumn sumtypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sumnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn plansumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn oplsumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn disposedsumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn commentDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
        private System.Windows.Forms.ToolStripMenuItem gsdfToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sdgsdfToolStripMenuItem;
    }
}