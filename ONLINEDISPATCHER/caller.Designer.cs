﻿namespace ONLINEDISPATCHER
{
    partial class caller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.button1 = new System.Windows.Forms.Button();
            this.m_pCall_HangUp = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.m_pStatusBar = new System.Windows.Forms.StatusStrip();
            this.status_label1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.text = new System.Windows.Forms.ToolStripStatusLabel();
            this.duration = new System.Windows.Forms.ToolStripStatusLabel();
            this.DataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.m_pToggleOnHold = new System.Windows.Forms.Button();
            this.m_pToolbar = new System.Windows.Forms.ToolStrip();
            this.m_pStatusBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(11, 61);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(70, 19);
            this.button1.TabIndex = 2;
            this.button1.Text = "Ответить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // m_pCall_HangUp
            // 
            this.m_pCall_HangUp.Location = new System.Drawing.Point(556, 71);
            this.m_pCall_HangUp.Margin = new System.Windows.Forms.Padding(2);
            this.m_pCall_HangUp.Name = "m_pCall_HangUp";
            this.m_pCall_HangUp.Size = new System.Drawing.Size(162, 19);
            this.m_pCall_HangUp.TabIndex = 3;
            this.m_pCall_HangUp.Text = "Завершить";
            this.m_pCall_HangUp.UseVisualStyleBackColor = true;
            this.m_pCall_HangUp.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(220, 71);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(167, 19);
            this.button3.TabIndex = 5;
            this.button3.Text = "Открыть заказ";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 300;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // m_pStatusBar
            // 
            this.m_pStatusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.status_label1,
            this.text,
            this.duration});
            this.m_pStatusBar.Location = new System.Drawing.Point(0, 215);
            this.m_pStatusBar.Name = "m_pStatusBar";
            this.m_pStatusBar.Size = new System.Drawing.Size(905, 22);
            this.m_pStatusBar.TabIndex = 7;
            this.m_pStatusBar.Text = "statusStrip1";
            this.m_pStatusBar.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.statusStrip1_ItemClicked);
            // 
            // status_label1
            // 
            this.status_label1.Name = "status_label1";
            this.status_label1.Size = new System.Drawing.Size(0, 17);
            // 
            // text
            // 
            this.text.Name = "text";
            this.text.Size = new System.Drawing.Size(109, 17);
            this.text.Text = "toolStripStatusLabel1";
            // 
            // duration
            // 
            this.duration.Name = "duration";
            this.duration.Size = new System.Drawing.Size(109, 17);
            this.duration.Text = "toolStripStatusLabel1";
            // 
            // DataGridView1
            // 
            this.DataGridView1.AllowUserToAddRows = false;
            this.DataGridView1.AllowUserToDeleteRows = false;
            this.DataGridView1.AllowUserToOrderColumns = true;
            this.DataGridView1.AllowUserToResizeRows = false;
            this.DataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView1.ColumnHeadersVisible = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(2);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridView1.DefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.DataGridView1.Location = new System.Drawing.Point(0, 0);
            this.DataGridView1.Margin = new System.Windows.Forms.Padding(15);
            this.DataGridView1.MultiSelect = false;
            this.DataGridView1.Name = "DataGridView1";
            this.DataGridView1.ReadOnly = true;
            this.DataGridView1.RowHeadersVisible = false;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridView1.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridView1.Size = new System.Drawing.Size(905, 215);
            this.DataGridView1.TabIndex = 8;
            this.DataGridView1.CellValueNeeded += new System.Windows.Forms.DataGridViewCellValueEventHandler(this.DataGridView1_CellValueNeeded);
            this.DataGridView1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.DataGridView1_CellFormatting);
            this.DataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellClick);
            this.DataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellContentClick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.m_pToggleOnHold);
            this.panel1.Controls.Add(this.m_pToolbar);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.m_pCall_HangUp);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 205);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(905, 10);
            this.panel1.TabIndex = 9;
            this.panel1.Visible = false;
            // 
            // m_pToggleOnHold
            // 
            this.m_pToggleOnHold.Location = new System.Drawing.Point(44, 112);
            this.m_pToggleOnHold.Name = "m_pToggleOnHold";
            this.m_pToggleOnHold.Size = new System.Drawing.Size(75, 23);
            this.m_pToggleOnHold.TabIndex = 7;
            this.m_pToggleOnHold.Text = "m_pToggleOnHold";
            this.m_pToggleOnHold.UseVisualStyleBackColor = true;
            // 
            // m_pToolbar
            // 
            this.m_pToolbar.Location = new System.Drawing.Point(0, 0);
            this.m_pToolbar.Name = "m_pToolbar";
            this.m_pToolbar.Size = new System.Drawing.Size(905, 25);
            this.m_pToolbar.TabIndex = 6;
            this.m_pToolbar.Text = "toolStrip1";
            // 
            // caller
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(905, 237);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.DataGridView1);
            this.Controls.Add(this.m_pStatusBar);
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "caller";
            this.Text = "Входящие звонки";
            this.Load += new System.EventHandler(this.incoming_call_Load);
            this.Shown += new System.EventHandler(this.incoming_call_Shown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.incoming_call_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.caller_KeyDown);
            this.m_pStatusBar.ResumeLayout(false);
            this.m_pStatusBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button m_pCall_HangUp;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.StatusStrip m_pStatusBar;
        private System.Windows.Forms.ToolStripStatusLabel status_label1;
        private System.Windows.Forms.DataGridView DataGridView1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStrip m_pToolbar;
        private System.Windows.Forms.Button m_pToggleOnHold;
        private System.Windows.Forms.ToolStripStatusLabel text;
        private System.Windows.Forms.ToolStripStatusLabel duration;
    }
}