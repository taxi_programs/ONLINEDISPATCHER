//
// csmapcontrol - a C# map control - http://projects.ciarang.com/p/csmapcontrol
// Copyright (C) 2010, Ciaran Gultnieks
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>. 

using System;
using System.Collections.Generic;
using System.Xml;
using System.Drawing;

namespace csmapcontrol
{

	public class TrackPoint : ICloneable
	{
		public double Lat;
		public double Lon;
		public double Elevation;
        public double speed;
        public double accuracy;
        public double pause_minutes;
        public double lost_link_minutes;
        public DateTime gps_time;

		public DateTime Time;

		public object Clone()
		{
			TrackPoint copy=new TrackPoint();
			copy.Lat=Lat;
			copy.Lon=Lon;
			copy.Elevation=Elevation;
			copy.Time=Time;
			return copy;
		}

	}

	public class Track
	{

		public List<TrackPoint> Points;
		public string Name;

		/// <summary>
		/// Create a new empty Track.
		/// </summary>
		public Track()
		{
			Points=new List<TrackPoint>();
		}

		/// <summary>
		/// Create a new Track by loading from a GPX file.
		/// </summary>
		/// <param name="filespec">The filespec of a GPX file.</param>
		public Track(string gpxfile)
		{
			Points=new List<TrackPoint>();
			XmlDocument x=new XmlDocument();
			x.Load(gpxfile);
			if(x.DocumentElement.Name!="gpx")
				throw new ApplicationException("Document element should be 'gpx'");
			string ns="http://www.topografix.com/GPX/1/0";
			string ver=x.DocumentElement.GetAttribute("version");
			if(ver!=null && ver=="1.1")
				ns="http://www.topografix.com/GPX/1/1";
			XmlNamespaceManager nsmgr=new XmlNamespaceManager(x.NameTable);
			nsmgr.AddNamespace("gpx", ns);
			Name=x.GetElementsByTagName("name")[0].InnerText;
			foreach(XmlNode point in x.SelectNodes("//gpx:trkpt",nsmgr))
			{
				TrackPoint p=new TrackPoint();
				p.Lat=double.Parse(point.SelectSingleNode("@lat").Value);
				p.Lon=double.Parse(point.SelectSingleNode("@lon").Value);
				p.Elevation=double.Parse(point.SelectSingleNode("gpx:ele",nsmgr).InnerText);
				p.Time=DateTime.Parse(point.SelectSingleNode("gpx:time",nsmgr).InnerText).ToUniversalTime();
				Points.Add(p);
			}
		}

		/// <summary>
		/// The start time of the track. Returns DateTime.MinValue if the
		/// track has no points.
		/// </summary>
		public DateTime StartTime
		{
			get
			{
				if(Points.Count==0)
					return DateTime.MinValue;
				return Points[0].Time;
			}
		}

		/// <summary>
		/// The end time of the track. Returns DateTime.MinValue if the
		/// track has no points.
		/// </summary>
		public DateTime EndTime
		{
			get
			{
				if(Points.Count==0)
					return DateTime.MinValue;
				return Points[Points.Count-1].Time;
			}
		}

		/// <summary>
		/// Split this Track at the specified time.
		/// </summary>
		/// <param name="splittime">The time to split at.</param>
		/// <returns>An array containing two new Track objects, the first being everything
		/// from this Track up to and including splittime, and the second being the
		/// remainder.</returns>
		public Track[] Split(DateTime splittime)
		{
			Track[] retval=new Track[1];
			retval[0]=new Track();
			retval[1]=new Track();
			retval[0].Name=Name+" Part 1";
			retval[1].Name=Name+" Part 2";
			foreach(TrackPoint p in Points)
				if(p.Time<=splittime)
					retval[0].Points.Add((TrackPoint)p.Clone());
				else
					retval[1].Points.Add((TrackPoint)p.Clone());
			return retval;
		}

		/// <summary>
		/// Remove the trackpoint at the given time.
		/// </summary>
		/// <param name="removetime">The time at which the point will be removed. Any
		/// points at this exact time will be removed.</param>
		public void RemovePoint(DateTime removetime)
		{
			List<TrackPoint> toremove=new List<TrackPoint>();
			foreach(TrackPoint p in Points)
				if(p.Time==removetime)
					toremove.Add(p);
			foreach(TrackPoint p in toremove)
				Points.Remove(p);
		}

		public class Bounds
		{
			public double MinLat;
			public double MinLon;
			public double MaxLat;
			public double MaxLon;
		}

		/// <summary>
		/// Calculate the bounds of this track.
		/// </summary>
		/// <returns>The bounds of the track, or null if the track contains no
		/// data at all.</returns>
		public Bounds CalculateBounds()
		{
			Bounds bounds=null;
			foreach(TrackPoint p in Points)
			{
				if(bounds==null)
				{
					bounds=new Bounds();
					bounds.MinLat=bounds.MaxLat=p.Lat;
					bounds.MinLon=bounds.MaxLon=p.Lon;
				}
				else
				{
					if(p.Lat<bounds.MinLat)
						bounds.MinLat=p.Lat;
					if(p.Lat>bounds.MaxLat)
						bounds.MaxLat=p.Lat;
					if(p.Lon<bounds.MinLon)
						bounds.MinLon=p.Lon;
					if(p.Lon>bounds.MaxLon)
						bounds.MaxLon=p.Lon;
				}
			}
			return bounds;
		}

	}
}
