﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ONLINEDISPATCHER {
    public partial class EditGpsAddress : Form {
        
        private static DataTable zones = null;

        private int _id = 0;
        
        public EditGpsAddress() {
            InitializeComponent();

            if (zones == null)
                zones = DataBase.mssqlRead("SELECT * FROM ZONES order by descr");


            zone.DataSource = zones;
            zone.DisplayMember = "Descr";
            zone.ValueMember = "ID";

            zone.AutoCompleteSource = AutoCompleteSource.ListItems;
            zone.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            zone.SelectedIndex = -1;

            zone.DropDownHeight = zone.ItemHeight * ((DataTable)zone.DataSource).Rows.Count;

        }

        private void EditGpsAddress_Load(object sender, EventArgs e) {

        }

        public void doLoad(int id) {
            _id = id;
            DataTable dt = DataBase.mssqlRead("SELECT * from ADRESSES where ID = " + id.ToString());
            street.Text = (String)dt.Rows[0]["address"];
            housenumber.Text = (String)dt.Rows[0]["housenum"];
            name.Text = dt.Rows[0]["name"].ToString();
            lat.Text = (dt.Rows[0]["lat"]).ToString();
            lon.Text = (dt.Rows[0]["lon"]).ToString();


            if ((dt.Rows[0]["state"]).ToString() == "0")
                state.Checked = true;

            if (dt.Rows[0]["zoneid"].ToString() != "")
                zone.SelectedValue = dt.Rows[0]["zoneid"].ToString();
        }

        private void button1_Click(object sender, EventArgs e) {
            String latlon = MAIN_FORM.doYandexSearchAddress("Ярославль "+street.Text, housenumber.Text);
            //String latlon = doSearchAddress(street, house);


            if (latlon != null)
                if (latlon != "") {
                    String[] ll = latlon.Split(' ');

                    String[] fields = { "lon", "lat" };
                    object[] values = { ll[0], ll[1] };

                    lat.Text = ll[1];
                    lon.Text = ll[0];

                    //DataBase.mssqlUpdate("ADRESSES", fields, values, "ID", id);

                }
        }

        private void button2_Click(object sender, EventArgs e) {

            MAIN_FORM.main_form_object.doClearAdressMarkers();
            double dlat = double.Parse(lat.Text.Replace('.',','));
            double dlon = double.Parse(lon.Text.Replace('.', ','));
            MAIN_FORM.main_form_object.doShowAddressMarker((int)(dlat * 100000), (int)(dlon * 100000), street.Text + ' ' + housenumber.Text);
        }

        private void button3_Click(object sender, EventArgs e) {

            double dlat = double.Parse(lat.Text.Replace('.', ','));
            double dlon = double.Parse(lon.Text.Replace('.', ','));



            String[] fields = { "NAME", "ADDRESS", "HOUSENUM", "ZONEID", "LAT", "LON", "STATE" };
            int istate = 0;
            if (!state.Checked)
                istate = 1;
            object[] values = { name.Text, street.Text, housenumber.Text, zone.SelectedValue, dlat, dlon, istate };

            DataBase.mssqlUpdate("ADRESSES", fields, values, "ID", _id);
            this.Close();
        }
    }
}
