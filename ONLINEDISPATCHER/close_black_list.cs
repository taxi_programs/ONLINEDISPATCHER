﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ONLINEDISPATCHER
{
    public partial class close_black_list : Form
    {
        public close_black_list()
        {
            InitializeComponent();
        }

        private void close_black_list_Load(object sender, EventArgs e)
        {
            DataTable cars = DataBase.mssqlRead("SELECT CARS.ID, cast (CARS.ID as varchar(6))+' '+  CARS.COLOR +' '+  CARS.mark +' '+ cars.gosnum +' '+drivers.fio CAR_DRIVER_DESC  FROM CARS, DRIVERS WHERE DRIVERS.ID = CARS.DRIVERID order by CARS.ID");
            


            //    ServiceID.Items.Clear();
            comboCarsFrom.DataSource = cars;
            comboCarsFrom.DisplayMember = "CAR_DRIVER_DESC";
            comboCarsFrom.ValueMember = "ID";
            comboCarsFrom.DropDownStyle = ComboBoxStyle.DropDown;
            comboCarsFrom.AutoCompleteSource = AutoCompleteSource.ListItems;
            //  ZoneID.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            //comboCars.SelectedIndex = -1;
            comboCarsFrom.AutoCompleteMode = AutoCompleteMode.SuggestAppend;


            comboCarsTo.DataSource = cars.Copy(); ;
            comboCarsTo.DisplayMember = "CAR_DRIVER_DESC";
            comboCarsTo.ValueMember = "ID";
            comboCarsTo.DropDownStyle = ComboBoxStyle.DropDown;
            comboCarsTo.AutoCompleteSource = AutoCompleteSource.ListItems;
            //  ZoneID.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            //comboCars.SelectedIndex = -1;
            comboCarsTo.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String car_from = comboCarsFrom.SelectedValue.ToString();
            String car_to = comboCarsTo.SelectedValue.ToString();
            String s_summ = black_list_summ.Text;
            String phone = black_list_phone.Text;

            if ((car_from=="") || (car_to == "") || (s_summ== "") || (phone ==""))
            {
                MessageBox.Show("Не все указано");
                return;
            }

            DataTable dt = DataBase.mssqlRead("SELECT ID FROM DRIVERS WHERE CARID="+car_from);
            int driver_from_id = (int)dt.Rows[0]["ID"];
            dt = DataBase.mssqlRead("SELECT ID FROM DRIVERS WHERE CARID=" + car_to);
            int driver_to_id = (int)dt.Rows[0]["ID"];
            if (driver_from_id == 0)
            {
                MessageBox.Show("Не выбран водитель 'от кого'");
                return;
            }

            if (driver_to_id == 0)
            {
                MessageBox.Show("Не выбран водитель 'кому'");
                return;
            }

            dt = DataBase.mssqlRead("SELECT ID  FROM PHONES where PHONE='" + phone + "' and deleted = 0");
            if (dt.Rows.Count > 0)
            {
                int phone_id = (int)dt.Rows[0]["ID"];
                int summ = 0;
                int.TryParse(s_summ, out summ);

                if (summ > 0)
                {

                    String[] fields1 = { "driverid", "ServiceID", "OpID", "dtArrive", "sum", "type", "promised_payment", "Descr" };
                    object[] values1 = { driver_from_id, 1, MAIN_FORM.OperatorID, DateTime.Now, summ, 0, 1, "<ОБЕЩАННЫЙ ПЛАТЕЖ>" };

                    DataBase.mssqlInsert("BANK", fields1, values1);

                    
                    
                    String[] fields = { "driverid", "ServiceID", "OpID", "dtArrive", "sum", "type", "promised_payment", "Descr" };
                    object[] values = { driver_from_id, 1, MAIN_FORM.OperatorID, DateTime.Now.AddSeconds(1), summ, 2, 0, "<СНЯТИЕ ДЛЯ ПЕРЕДАЧИ ДРУГОМУ ВОДИТЕЛЮ>" + car_to };

                    DataBase.mssqlInsert("BANK", fields, values);

                    


                    String[] fields2 = { "driverid", "ServiceID", "OpID", "dtArrive", "sum", "type", "promised_payment", "Descr" };
                    object[] values2 = { driver_to_id, 1, MAIN_FORM.OperatorID, DateTime.Now.AddSeconds(2), summ, 0, 0, "<ПЕРЕДАЧА ОТ ВОДИТЕЛЯ>" + car_from };

                    DataBase.mssqlInsert("BANK", fields2, values2);


                    DictionaryElement elem = new DictionaryElement();
                    //elem.doMakeInterface("PHONES");
                    elem.doLoad("PHONES", phone_id);

                    this.Close();
                    elem.ShowDialog();

                    

                    


                    
                }

            }



            /*
             * 1. Снять 150 руб с 174 для 195
            2. Положить обещанный платеж 150 руб 174
            3. Положить 150 руб для 195 от 174
            4. Списать долг с телефона 335604
            5. Отредактировать примечание у телефона 335604
             * 
             * 
             *  
                String[] fields = { "driverid", "ServiceID", "OpID", "dtArrive", "sum", "type", "promised_payment", "Descr" };
                object[] values = { selected_driver_id, car_service_id, MAIN_FORM.OperatorID, DateTime.Now, 500, 0, 1, "<ОБЕЩАННЫЙ ПЛАТЕЖ>" };

                DataBase.mssqlInsert("BANK", fields, values);
             * 
             * 
             * 
             * 
             * 
             * 
             * 
            */




        }

        private void black_list_summ_TextChanged(object sender, EventArgs e) {
            String text = "1. Снять " + black_list_summ.Text + " руб с " + comboCarsFrom.Text + " для " + comboCarsTo.Text + " \n" +
"2. Положить обещанный платеж " + black_list_summ.Text + " руб " + comboCarsFrom.Text + " \n" +
"3. Положить " + black_list_summ.Text + " руб для " + comboCarsTo.Text + " от " + comboCarsFrom.Text + " \n" +
"4. Списать долг " + black_list_summ.Text + " с телефона " + black_list_phone.Text + " \n" +
"5. Отредактировать примечание у телефона " + black_list_phone.Text;

            label1.Text = text;
        }

        private void black_list_phone_TextChanged(object sender, EventArgs e) {
            String text = "1. Снять " + black_list_summ.Text + " руб с " + comboCarsFrom.Text + " для " + comboCarsTo.Text + " \n" +
"2. Положить обещанный платеж " + black_list_summ.Text + " руб " + comboCarsFrom.Text + " \n" +
"3. Положить " + black_list_summ.Text + " руб для " + comboCarsTo.Text + " от " + comboCarsFrom.Text + " \n" +
"4. Списать долг " + black_list_summ.Text + " с телефона " + black_list_phone.Text + " \n" +
"5. Отредактировать примечание у телефона " + black_list_phone.Text;
            label1.Text = text;
        }

        private void comboCarsFrom_SelectedIndexChanged(object sender, EventArgs e) {
            String text = "1. Снять " + black_list_summ.Text + " руб с " + comboCarsFrom.Text + " для " + comboCarsTo.Text + " \n" +
"2. Положить обещанный платеж " + black_list_summ.Text + " руб " + comboCarsFrom.Text + " \n" +
"3. Положить " + black_list_summ.Text + " руб для " + comboCarsTo.Text + " от " + comboCarsFrom.Text + " \n" +
"4. Списать долг " + black_list_summ.Text + " с телефона " + black_list_phone.Text + " \n" +
"5. Отредактировать примечание у телефона " + black_list_phone.Text;
            label1.Text = text;
        }

        private void comboCarsTo_SelectedIndexChanged(object sender, EventArgs e) {
            String text = "1. Снять " + black_list_summ.Text + " руб с " + comboCarsFrom.Text + " для " + comboCarsTo .Text+ " \n" +
"2. Положить обещанный платеж " + black_list_summ.Text + " руб " + comboCarsFrom.Text + " \n" +
"3. Положить " + black_list_summ.Text + " руб для " + comboCarsTo.Text + " от " + comboCarsFrom.Text + " \n" +
"4. Списать долг " + black_list_summ.Text + " с телефона " + black_list_phone.Text + " \n" +
"5. Отредактировать примечание у телефона " + black_list_phone.Text;
            label1.Text = text;
        }

        private void button2_Click(object sender, EventArgs e) {

            DataTable dt = DataBase.mssqlRead("SELECT ID  FROM PHONES where PHONE='" + black_list_phone.Text + "' and deleted = 0");
            if (dt.Rows.Count > 0) {
                int phone_id = (int)dt.Rows[0]["ID"];
                DictionaryElement elem = new DictionaryElement();
                //elem.doMakeInterface("PHONES");
                elem.doLoad("PHONES", phone_id);

                this.Close();
                elem.ShowDialog();
            }

            
            
        }
    }
}
