﻿namespace ONLINEDISPATCHER
{
    partial class close_black_list
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.black_list_summ = new System.Windows.Forms.TextBox();
            this.black_list_phone = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comboCarsFrom = new System.Windows.Forms.ComboBox();
            this.comboCarsTo = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(271, 65);
            this.label1.TabIndex = 0;
            this.label1.Text = "1. Снять 150 руб с 174 для 195\r\n2. Положить обещанный платеж 150 руб 174\r\n3. Поло" +
                "жить 150 руб для 195 от 174\r\n4. Списать долг с телефона 335604\r\n5. Отредактирова" +
                "ть примечание у телефона 335604";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Сумма:";
            // 
            // black_list_summ
            // 
            this.black_list_summ.Location = new System.Drawing.Point(107, 84);
            this.black_list_summ.Name = "black_list_summ";
            this.black_list_summ.Size = new System.Drawing.Size(100, 20);
            this.black_list_summ.TabIndex = 2;
            this.black_list_summ.TextChanged += new System.EventHandler(this.black_list_summ_TextChanged);
            // 
            // black_list_phone
            // 
            this.black_list_phone.Location = new System.Drawing.Point(107, 110);
            this.black_list_phone.Name = "black_list_phone";
            this.black_list_phone.Size = new System.Drawing.Size(100, 20);
            this.black_list_phone.TabIndex = 4;
            this.black_list_phone.TextChanged += new System.EventHandler(this.black_list_phone_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Телефон Ч.С.:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 143);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "От позывного:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 169);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Для позывного:";
            // 
            // comboCarsFrom
            // 
            this.comboCarsFrom.DropDownHeight = 120;
            this.comboCarsFrom.FormattingEnabled = true;
            this.comboCarsFrom.IntegralHeight = false;
            this.comboCarsFrom.Location = new System.Drawing.Point(107, 135);
            this.comboCarsFrom.Margin = new System.Windows.Forms.Padding(2);
            this.comboCarsFrom.Name = "comboCarsFrom";
            this.comboCarsFrom.Size = new System.Drawing.Size(357, 21);
            this.comboCarsFrom.TabIndex = 8;
            this.comboCarsFrom.SelectedIndexChanged += new System.EventHandler(this.comboCarsFrom_SelectedIndexChanged);
            // 
            // comboCarsTo
            // 
            this.comboCarsTo.DropDownHeight = 120;
            this.comboCarsTo.FormattingEnabled = true;
            this.comboCarsTo.IntegralHeight = false;
            this.comboCarsTo.Location = new System.Drawing.Point(107, 161);
            this.comboCarsTo.Margin = new System.Windows.Forms.Padding(2);
            this.comboCarsTo.Name = "comboCarsTo";
            this.comboCarsTo.Size = new System.Drawing.Size(357, 21);
            this.comboCarsTo.TabIndex = 9;
            this.comboCarsTo.SelectedIndexChanged += new System.EventHandler(this.comboCarsTo_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(389, 206);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "ОК";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(222, 107);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(165, 23);
            this.button2.TabIndex = 11;
            this.button2.Text = "открыть карточку телефона";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // close_black_list
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(482, 241);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.comboCarsTo);
            this.Controls.Add(this.comboCarsFrom);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.black_list_phone);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.black_list_summ);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "close_black_list";
            this.Text = "Закрытие черного списка";
            this.Load += new System.EventHandler(this.close_black_list_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.TextBox black_list_summ;
        public System.Windows.Forms.TextBox black_list_phone;
        public System.Windows.Forms.ComboBox comboCarsFrom;
        public System.Windows.Forms.ComboBox comboCarsTo;
        private System.Windows.Forms.Button button2;
    }
}