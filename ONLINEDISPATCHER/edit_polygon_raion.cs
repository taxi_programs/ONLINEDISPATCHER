﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using csmapcontrol;

namespace ONLINEDISPATCHER
{
    public partial class edit_polygon_raion : Form
    {
        public edit_polygon_raion()
        {
            InitializeComponent();
        }

        private void edit_polygon_raion_Load(object sender, EventArgs e)
        {
            DataTable zones = DataBase.mssqlRead("SELECT * FROM ZONES order by descr");
            object [] dop_zone = {-1, "ГОРОДСКАЯ ЧЕРТА",   null, null, 0, 0, null};
            zones.Rows.Add(dop_zone);

            ZoneID.DataSource = zones;
            ZoneID.DisplayMember = "Descr";
            ZoneID.ValueMember = "ID";

            ZoneID.AutoCompleteSource = AutoCompleteSource.ListItems;
            ZoneID.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            ZoneID.SelectedIndex = -1;

            ZoneID.DropDownHeight = ZoneID.ItemHeight * ((DataTable)ZoneID.DataSource).Rows.Count;
            
        }

        private void edit_polygon_raion_Shown(object sender, EventArgs e)
        {
            

            MapControl.ZoneRegion  zr = (MapControl.ZoneRegion) this.Tag;

            ZoneID.SelectedValue = zr.zone_id;

            if (zr.remote_zone == 0)
            {
                checkBox1.Checked = true;
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            MapControl.ZoneRegion zr = (MapControl.ZoneRegion)this.Tag;

            zr.zone_id = (int)ZoneID.SelectedValue;
            zr.zone_name = ZoneID.Text;
            Close();

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            MapControl.ZoneRegion zr = (MapControl.ZoneRegion)this.Tag;
            if (checkBox1.Checked)
                zr.remote_zone = 1;
            else
                zr.remote_zone = 0;

            
        }
    }
}
