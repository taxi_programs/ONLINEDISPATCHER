﻿namespace OSM_CLIENT
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button11 = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.connectPhone = new System.Windows.Forms.TextBox();
            this.phone = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.predv_tm = new System.Windows.Forms.DateTimePicker();
            this.predv_dt = new System.Windows.Forms.DateTimePicker();
            this.predv = new System.Windows.Forms.CheckBox();
            this.prim = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.zone_from = new System.Windows.Forms.ComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.over_building_number2 = new System.Windows.Forms.ComboBox();
            this.coord_over_2 = new System.Windows.Forms.TextBox();
            this.over_street2 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.over_building_number1 = new System.Windows.Forms.ComboBox();
            this.coord_over_1 = new System.Windows.Forms.TextBox();
            this.over_street1 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.to_building_number = new System.Windows.Forms.ComboBox();
            this.from_building_number = new System.Windows.Forms.ComboBox();
            this.button4 = new System.Windows.Forms.Button();
            this.coord_to = new System.Windows.Forms.TextBox();
            this.coord_from = new System.Windows.Forms.TextBox();
            this.use_sql = new System.Windows.Forms.CheckBox();
            this.label17 = new System.Windows.Forms.Label();
            this.path_cost = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.distance = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.service_name = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.to_street = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.from_street = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.listBox3 = new System.Windows.Forms.ListBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.listBox4 = new System.Windows.Forms.ListBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.маршрутОтсюдаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.маршрутСюдаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.промежуточнаяТочка1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.промежуточнаяТочка2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.очиститьМаршрутToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.очиститьТочку1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.очиститьПромежуточнуюТочку2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button12 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tabControl4 = new System.Windows.Forms.TabControl();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tabControl5 = new System.Windows.Forms.TabControl();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.button6 = new System.Windows.Forms.Button();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker4 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.button5 = new System.Windows.Forms.Button();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.tabControl3 = new System.Windows.Forms.TabControl();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button15 = new System.Windows.Forms.Button();
            this.statusbar = new System.Windows.Forms.TextBox();
            this.btnCallDial = new System.Windows.Forms.Button();
            this.btnCallHangup = new System.Windows.Forms.Button();
            this.btnCallHold = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.listBox5 = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.callBtnHold = new System.Windows.Forms.Button();
            this.callBtnHangup = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.callBtnDial = new System.Windows.Forms.Button();
            this.callPhone = new System.Windows.Forms.TextBox();
            this.dataSet1 = new System.Data.DataSet();
            this.dataTable1 = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.mapControl1 = new csmapcontrol.MapControl();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.button2 = new System.Windows.Forms.Button();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabControl4.SuspendLayout();
            this.tabPage9.SuspendLayout();
            this.tabPage10.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabControl5.SuspendLayout();
            this.tabPage11.SuspendLayout();
            this.tabControl3.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.splitContainer1.Panel1.Controls.Add(this.tabControl1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.splitContainer1.Panel2.Controls.Add(this.tabControl3);
            this.splitContainer1.Size = new System.Drawing.Size(936, 713);
            this.splitContainer1.SplitterDistance = 285;
            this.splitContainer1.TabIndex = 5;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(285, 713);
            this.tabControl1.TabIndex = 7;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tabControl2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(0);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(277, 687);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Заказы";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Controls.Add(this.tabPage5);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(0, 0);
            this.tabControl2.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.Padding = new System.Drawing.Point(0, 0);
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(277, 687);
            this.tabControl2.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dataGridView1);
            this.tabPage3.Controls.Add(this.panel2);
            this.tabPage3.Controls.Add(this.listBox2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(0);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(269, 661);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Активные";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SunkenHorizontal;
            this.dataGridView1.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.HighlightText;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(269, 661);
            this.dataGridView1.TabIndex = 52;
            this.dataGridView1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dataGridView1_MouseClick);
            this.dataGridView1.DoubleClick += new System.EventHandler(this.dataGridView1_DoubleClick);
            this.dataGridView1.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseDown);
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button11);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.connectPhone);
            this.panel2.Controls.Add(this.phone);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.predv_tm);
            this.panel2.Controls.Add(this.predv_dt);
            this.panel2.Controls.Add(this.predv);
            this.panel2.Controls.Add(this.prim);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.zone_from);
            this.panel2.Controls.Add(this.btnSave);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.over_building_number2);
            this.panel2.Controls.Add(this.coord_over_2);
            this.panel2.Controls.Add(this.over_street2);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.over_building_number1);
            this.panel2.Controls.Add(this.coord_over_1);
            this.panel2.Controls.Add(this.over_street1);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.to_building_number);
            this.panel2.Controls.Add(this.from_building_number);
            this.panel2.Controls.Add(this.button4);
            this.panel2.Controls.Add(this.coord_to);
            this.panel2.Controls.Add(this.coord_from);
            this.panel2.Controls.Add(this.use_sql);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.path_cost);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.distance);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.service_name);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.to_street);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.from_street);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(269, 24);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(280, 650);
            this.panel2.TabIndex = 50;
            this.panel2.Visible = false;
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(111, 56);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(118, 39);
            this.button11.TabIndex = 53;
            this.button11.Text = "button11";
            this.button11.UseVisualStyleBackColor = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(136, 387);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(45, 13);
            this.label23.TabIndex = 103;
            this.label23.Text = "Служба";
            // 
            // connectPhone
            // 
            this.connectPhone.Location = new System.Drawing.Point(137, 454);
            this.connectPhone.Name = "connectPhone";
            this.connectPhone.Size = new System.Drawing.Size(121, 20);
            this.connectPhone.TabIndex = 101;
            // 
            // phone
            // 
            this.phone.Location = new System.Drawing.Point(4, 454);
            this.phone.Name = "phone";
            this.phone.Size = new System.Drawing.Size(121, 20);
            this.phone.TabIndex = 100;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(136, 438);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(106, 13);
            this.label22.TabIndex = 99;
            this.label22.Text = "Телефон для связи";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(4, 438);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(52, 13);
            this.label21.TabIndex = 98;
            this.label21.Text = "Телефон";
            // 
            // predv_tm
            // 
            this.predv_tm.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.predv_tm.Location = new System.Drawing.Point(135, 530);
            this.predv_tm.Name = "predv_tm";
            this.predv_tm.Size = new System.Drawing.Size(121, 20);
            this.predv_tm.TabIndex = 97;
            // 
            // predv_dt
            // 
            this.predv_dt.Location = new System.Drawing.Point(135, 504);
            this.predv_dt.Name = "predv_dt";
            this.predv_dt.Size = new System.Drawing.Size(121, 20);
            this.predv_dt.TabIndex = 96;
            // 
            // predv
            // 
            this.predv.AutoSize = true;
            this.predv.Location = new System.Drawing.Point(137, 488);
            this.predv.Name = "predv";
            this.predv.Size = new System.Drawing.Size(119, 17);
            this.predv.TabIndex = 95;
            this.predv.Text = "Предварительный";
            this.predv.UseVisualStyleBackColor = true;
            // 
            // prim
            // 
            this.prim.Location = new System.Drawing.Point(3, 553);
            this.prim.Multiline = true;
            this.prim.Name = "prim";
            this.prim.Size = new System.Drawing.Size(255, 46);
            this.prim.TabIndex = 94;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(1, 537);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(70, 13);
            this.label20.TabIndex = 93;
            this.label20.Text = "Примечание";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(4, 488);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(43, 13);
            this.label18.TabIndex = 90;
            this.label18.Text = "Откуда";
            // 
            // zone_from
            // 
            this.zone_from.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.zone_from.FormattingEnabled = true;
            this.zone_from.Location = new System.Drawing.Point(4, 504);
            this.zone_from.Name = "zone_from";
            this.zone_from.Size = new System.Drawing.Size(121, 21);
            this.zone_from.TabIndex = 89;
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.Location = new System.Drawing.Point(108, 613);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(150, 23);
            this.btnSave.TabIndex = 88;
            this.btnSave.Text = "Оформить заказ";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click_1);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(202, 27);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(69, 13);
            this.label15.TabIndex = 87;
            this.label15.Text = "Координаты";
            // 
            // over_building_number2
            // 
            this.over_building_number2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.over_building_number2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.over_building_number2.FormattingEnabled = true;
            this.over_building_number2.Location = new System.Drawing.Point(129, 216);
            this.over_building_number2.Name = "over_building_number2";
            this.over_building_number2.Size = new System.Drawing.Size(59, 21);
            this.over_building_number2.TabIndex = 62;
            this.over_building_number2.TabStop = false;
            // 
            // coord_over_2
            // 
            this.coord_over_2.Location = new System.Drawing.Point(194, 215);
            this.coord_over_2.Name = "coord_over_2";
            this.coord_over_2.Size = new System.Drawing.Size(100, 20);
            this.coord_over_2.TabIndex = 86;
            this.coord_over_2.TabStop = false;
            // 
            // over_street2
            // 
            this.over_street2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.over_street2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.over_street2.FormattingEnabled = true;
            this.over_street2.Location = new System.Drawing.Point(4, 215);
            this.over_street2.Name = "over_street2";
            this.over_street2.Size = new System.Drawing.Size(119, 21);
            this.over_street2.TabIndex = 61;
            this.over_street2.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(127, 199);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 13);
            this.label7.TabIndex = 84;
            this.label7.Text = "Дом";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(1, 199);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 13);
            this.label8.TabIndex = 83;
            this.label8.Text = "Улица/место";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(1, 186);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 13);
            this.label9.TabIndex = 82;
            this.label9.Text = "Через";
            // 
            // over_building_number1
            // 
            this.over_building_number1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.over_building_number1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.over_building_number1.FormattingEnabled = true;
            this.over_building_number1.Location = new System.Drawing.Point(129, 156);
            this.over_building_number1.Name = "over_building_number1";
            this.over_building_number1.Size = new System.Drawing.Size(59, 21);
            this.over_building_number1.TabIndex = 60;
            this.over_building_number1.TabStop = false;
            // 
            // coord_over_1
            // 
            this.coord_over_1.Location = new System.Drawing.Point(194, 155);
            this.coord_over_1.Name = "coord_over_1";
            this.coord_over_1.Size = new System.Drawing.Size(100, 20);
            this.coord_over_1.TabIndex = 81;
            this.coord_over_1.TabStop = false;
            // 
            // over_street1
            // 
            this.over_street1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.over_street1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.over_street1.FormattingEnabled = true;
            this.over_street1.Location = new System.Drawing.Point(4, 155);
            this.over_street1.Name = "over_street1";
            this.over_street1.Size = new System.Drawing.Size(119, 21);
            this.over_street1.TabIndex = 59;
            this.over_street1.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(127, 139);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 79;
            this.label4.Text = "Дом";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1, 139);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 13);
            this.label5.TabIndex = 78;
            this.label5.Text = "Улица/место";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(1, 126);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 77;
            this.label6.Text = "Через";
            // 
            // to_building_number
            // 
            this.to_building_number.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.to_building_number.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.to_building_number.FormattingEnabled = true;
            this.to_building_number.Location = new System.Drawing.Point(129, 102);
            this.to_building_number.Name = "to_building_number";
            this.to_building_number.Size = new System.Drawing.Size(59, 21);
            this.to_building_number.TabIndex = 56;
            // 
            // from_building_number
            // 
            this.from_building_number.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.from_building_number.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.from_building_number.FormattingEnabled = true;
            this.from_building_number.Location = new System.Drawing.Point(129, 41);
            this.from_building_number.Name = "from_building_number";
            this.from_building_number.Size = new System.Drawing.Size(59, 21);
            this.from_building_number.TabIndex = 53;
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button4.Location = new System.Drawing.Point(4, 282);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(104, 34);
            this.button4.TabIndex = 58;
            this.button4.Text = "построить маршрут";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // coord_to
            // 
            this.coord_to.Location = new System.Drawing.Point(194, 101);
            this.coord_to.Name = "coord_to";
            this.coord_to.Size = new System.Drawing.Size(100, 20);
            this.coord_to.TabIndex = 76;
            this.coord_to.TabStop = false;
            // 
            // coord_from
            // 
            this.coord_from.Location = new System.Drawing.Point(194, 42);
            this.coord_from.Name = "coord_from";
            this.coord_from.Size = new System.Drawing.Size(100, 20);
            this.coord_from.TabIndex = 75;
            this.coord_from.TabStop = false;
            // 
            // use_sql
            // 
            this.use_sql.AutoSize = true;
            this.use_sql.Location = new System.Drawing.Point(4, 367);
            this.use_sql.Name = "use_sql";
            this.use_sql.Size = new System.Drawing.Size(169, 17);
            this.use_sql.TabIndex = 74;
            this.use_sql.TabStop = false;
            this.use_sql.Text = "Соединение с базой данных";
            this.use_sql.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(3, 319);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(291, 45);
            this.label17.TabIndex = 73;
            this.label17.Text = "Стоимость - ориентировочная. \r\nОна не учитывает удаленные районы, багаж, ожидание" +
                ".";
            // 
            // path_cost
            // 
            this.path_cost.AutoSize = true;
            this.path_cost.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.path_cost.Location = new System.Drawing.Point(224, 290);
            this.path_cost.Name = "path_cost";
            this.path_cost.Size = new System.Drawing.Size(45, 20);
            this.path_cost.TabIndex = 72;
            this.path_cost.Text = "0 км";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(148, 290);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 13);
            this.label16.TabIndex = 71;
            this.label16.Text = "Стоимость:";
            // 
            // distance
            // 
            this.distance.AutoSize = true;
            this.distance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.distance.Location = new System.Drawing.Point(224, 262);
            this.distance.Name = "distance";
            this.distance.Size = new System.Drawing.Size(45, 20);
            this.distance.TabIndex = 70;
            this.distance.Text = "0 км";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(4, 238);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(45, 13);
            this.label14.TabIndex = 69;
            this.label14.Text = "Служба";
            // 
            // service_name
            // 
            this.service_name.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.service_name.FormattingEnabled = true;
            this.service_name.Items.AddRange(new object[] {
            "Онлайн (80 + 8 * км)",
            "Студент (70 + 7 * км)"});
            this.service_name.Location = new System.Drawing.Point(139, 403);
            this.service_name.Name = "service_name";
            this.service_name.Size = new System.Drawing.Size(119, 21);
            this.service_name.TabIndex = 57;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(148, 262);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 13);
            this.label13.TabIndex = 68;
            this.label13.Text = "Расстояние:";
            // 
            // to_street
            // 
            this.to_street.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.to_street.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.to_street.FormattingEnabled = true;
            this.to_street.Location = new System.Drawing.Point(4, 101);
            this.to_street.Name = "to_street";
            this.to_street.Size = new System.Drawing.Size(119, 21);
            this.to_street.TabIndex = 55;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(127, 85);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(30, 13);
            this.label10.TabIndex = 65;
            this.label10.Text = "Дом";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(1, 85);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(75, 13);
            this.label11.TabIndex = 64;
            this.label11.Text = "Улица/место";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(1, 72);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 13);
            this.label12.TabIndex = 63;
            this.label12.Text = "Куда";
            // 
            // from_street
            // 
            this.from_street.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.from_street.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.from_street.FormattingEnabled = true;
            this.from_street.Location = new System.Drawing.Point(4, 42);
            this.from_street.Name = "from_street";
            this.from_street.Size = new System.Drawing.Size(119, 21);
            this.from_street.TabIndex = 51;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(127, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 54;
            this.label3.Text = "Дом";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 52;
            this.label2.Text = "Улица/место";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(1, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 50;
            this.label1.Text = "Откуда";
            // 
            // listBox2
            // 
            this.listBox2.ColumnWidth = 150;
            this.listBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listBox2.FormattingEnabled = true;
            this.listBox2.ItemHeight = 16;
            this.listBox2.Items.AddRange(new object[] {
            "фыа фыажф;asdfasdf;asdfasdf;asdf"});
            this.listBox2.Location = new System.Drawing.Point(206, 256);
            this.listBox2.Margin = new System.Windows.Forms.Padding(0);
            this.listBox2.MultiColumn = true;
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(410, 404);
            this.listBox2.TabIndex = 51;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.listBox3);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(0);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(269, 661);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "Предварительные";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // listBox3
            // 
            this.listBox3.ColumnWidth = 150;
            this.listBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listBox3.FormattingEnabled = true;
            this.listBox3.ItemHeight = 16;
            this.listBox3.Items.AddRange(new object[] {
            "фыа фыажф;asdfasdf;asdfasdf;asdf"});
            this.listBox3.Location = new System.Drawing.Point(0, 0);
            this.listBox3.Margin = new System.Windows.Forms.Padding(0);
            this.listBox3.MultiColumn = true;
            this.listBox3.Name = "listBox3";
            this.listBox3.Size = new System.Drawing.Size(269, 660);
            this.listBox3.TabIndex = 52;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.listBox4);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Margin = new System.Windows.Forms.Padding(0);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(269, 661);
            this.tabPage5.TabIndex = 2;
            this.tabPage5.Text = "За 12 часов";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // listBox4
            // 
            this.listBox4.ColumnWidth = 150;
            this.listBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listBox4.FormattingEnabled = true;
            this.listBox4.ItemHeight = 16;
            this.listBox4.Items.AddRange(new object[] {
            "фыа фыажф;asdfasdf;asdfasdf;asdf"});
            this.listBox4.Location = new System.Drawing.Point(0, 0);
            this.listBox4.Margin = new System.Windows.Forms.Padding(0);
            this.listBox4.MultiColumn = true;
            this.listBox4.Name = "listBox4";
            this.listBox4.Size = new System.Drawing.Size(269, 660);
            this.listBox4.TabIndex = 52;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.mapControl1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(0);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(277, 687);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Карта";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.маршрутОтсюдаToolStripMenuItem,
            this.маршрутСюдаToolStripMenuItem,
            this.промежуточнаяТочка1ToolStripMenuItem,
            this.промежуточнаяТочка2ToolStripMenuItem,
            this.очиститьМаршрутToolStripMenuItem,
            this.очиститьТочку1ToolStripMenuItem,
            this.очиститьПромежуточнуюТочку2ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(263, 158);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // маршрутОтсюдаToolStripMenuItem
            // 
            this.маршрутОтсюдаToolStripMenuItem.Name = "маршрутОтсюдаToolStripMenuItem";
            this.маршрутОтсюдаToolStripMenuItem.Size = new System.Drawing.Size(262, 22);
            this.маршрутОтсюдаToolStripMenuItem.Text = "Маршрут отсюда";
            this.маршрутОтсюдаToolStripMenuItem.Click += new System.EventHandler(this.маршрутОтсюдаToolStripMenuItem_Click);
            // 
            // маршрутСюдаToolStripMenuItem
            // 
            this.маршрутСюдаToolStripMenuItem.Name = "маршрутСюдаToolStripMenuItem";
            this.маршрутСюдаToolStripMenuItem.Size = new System.Drawing.Size(262, 22);
            this.маршрутСюдаToolStripMenuItem.Text = "Маршрут сюда";
            this.маршрутСюдаToolStripMenuItem.Click += new System.EventHandler(this.маршрутСюдаToolStripMenuItem_Click);
            // 
            // промежуточнаяТочка1ToolStripMenuItem
            // 
            this.промежуточнаяТочка1ToolStripMenuItem.Name = "промежуточнаяТочка1ToolStripMenuItem";
            this.промежуточнаяТочка1ToolStripMenuItem.Size = new System.Drawing.Size(262, 22);
            this.промежуточнаяТочка1ToolStripMenuItem.Text = "Промежуточная точка 1";
            this.промежуточнаяТочка1ToolStripMenuItem.Click += new System.EventHandler(this.промежуточнаяТочка1ToolStripMenuItem_Click);
            // 
            // промежуточнаяТочка2ToolStripMenuItem
            // 
            this.промежуточнаяТочка2ToolStripMenuItem.Name = "промежуточнаяТочка2ToolStripMenuItem";
            this.промежуточнаяТочка2ToolStripMenuItem.Size = new System.Drawing.Size(262, 22);
            this.промежуточнаяТочка2ToolStripMenuItem.Text = "Промежуточная точка 2";
            this.промежуточнаяТочка2ToolStripMenuItem.Click += new System.EventHandler(this.промежуточнаяТочка2ToolStripMenuItem_Click);
            // 
            // очиститьМаршрутToolStripMenuItem
            // 
            this.очиститьМаршрутToolStripMenuItem.Name = "очиститьМаршрутToolStripMenuItem";
            this.очиститьМаршрутToolStripMenuItem.Size = new System.Drawing.Size(262, 22);
            this.очиститьМаршрутToolStripMenuItem.Text = "Очистить маршрут";
            this.очиститьМаршрутToolStripMenuItem.Click += new System.EventHandler(this.очиститьМаршрутToolStripMenuItem_Click);
            // 
            // очиститьТочку1ToolStripMenuItem
            // 
            this.очиститьТочку1ToolStripMenuItem.Name = "очиститьТочку1ToolStripMenuItem";
            this.очиститьТочку1ToolStripMenuItem.Size = new System.Drawing.Size(262, 22);
            this.очиститьТочку1ToolStripMenuItem.Text = "Очистить промежуточную точку 1";
            this.очиститьТочку1ToolStripMenuItem.Click += new System.EventHandler(this.очиститьТочку1ToolStripMenuItem_Click);
            // 
            // очиститьПромежуточнуюТочку2ToolStripMenuItem
            // 
            this.очиститьПромежуточнуюТочку2ToolStripMenuItem.Name = "очиститьПромежуточнуюТочку2ToolStripMenuItem";
            this.очиститьПромежуточнуюТочку2ToolStripMenuItem.Size = new System.Drawing.Size(262, 22);
            this.очиститьПромежуточнуюТочку2ToolStripMenuItem.Text = "Очистить промежуточную точку 2";
            this.очиститьПромежуточнуюТочку2ToolStripMenuItem.Click += new System.EventHandler(this.очиститьПромежуточнуюТочку2ToolStripMenuItem_Click);
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.groupBox5);
            this.tabPage8.Controls.Add(this.groupBox4);
            this.tabPage8.Controls.Add(this.groupBox3);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Margin = new System.Windows.Forms.Padding(0);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(277, 687);
            this.tabPage8.TabIndex = 2;
            this.tabPage8.Text = "Управление";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.button12);
            this.groupBox5.Location = new System.Drawing.Point(10, 263);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(614, 56);
            this.groupBox5.TabIndex = 3;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Утилиты";
            this.groupBox5.Enter += new System.EventHandler(this.groupBox5_Enter);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(8, 19);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(75, 23);
            this.button12.TabIndex = 6;
            this.button12.Text = "SMS рассылка";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tabControl4);
            this.groupBox4.Location = new System.Drawing.Point(3, 158);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(621, 93);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Справочники";
            // 
            // tabControl4
            // 
            this.tabControl4.Controls.Add(this.tabPage9);
            this.tabControl4.Controls.Add(this.tabPage10);
            this.tabControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl4.Location = new System.Drawing.Point(3, 16);
            this.tabControl4.Name = "tabControl4";
            this.tabControl4.SelectedIndex = 0;
            this.tabControl4.Size = new System.Drawing.Size(615, 74);
            this.tabControl4.TabIndex = 1;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.button8);
            this.tabPage9.Controls.Add(this.button7);
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(607, 48);
            this.tabPage9.TabIndex = 0;
            this.tabPage9.Text = "Основные";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(89, 6);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 1;
            this.button8.Text = "Водители";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(8, 6);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 0;
            this.button7.Text = "Машины";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.button10);
            this.tabPage10.Controls.Add(this.button9);
            this.tabPage10.Location = new System.Drawing.Point(4, 22);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(607, 48);
            this.tabPage10.TabIndex = 1;
            this.tabPage10.Text = "Административные";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(89, 6);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 23);
            this.button10.TabIndex = 5;
            this.button10.Text = "Тарифы";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(8, 6);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 4;
            this.button9.Text = "Операторы";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tabControl5);
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(621, 120);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Отчеты";
            // 
            // tabControl5
            // 
            this.tabControl5.Controls.Add(this.tabPage11);
            this.tabControl5.Controls.Add(this.tabPage12);
            this.tabControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl5.Location = new System.Drawing.Point(3, 16);
            this.tabControl5.Name = "tabControl5";
            this.tabControl5.SelectedIndex = 0;
            this.tabControl5.Size = new System.Drawing.Size(615, 101);
            this.tabControl5.TabIndex = 0;
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this.button6);
            this.tabPage11.Controls.Add(this.dateTimePicker3);
            this.tabPage11.Controls.Add(this.dateTimePicker4);
            this.tabPage11.Controls.Add(this.dateTimePicker2);
            this.tabPage11.Controls.Add(this.dateTimePicker1);
            this.tabPage11.Controls.Add(this.button5);
            this.tabPage11.Location = new System.Drawing.Point(4, 22);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage11.Size = new System.Drawing.Size(607, 75);
            this.tabPage11.TabIndex = 0;
            this.tabPage11.Text = "tabPage11";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(155, 44);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(152, 23);
            this.button6.TabIndex = 5;
            this.button6.Text = "Графики";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePicker3.Location = new System.Drawing.Point(416, 7);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.Size = new System.Drawing.Size(71, 20);
            this.dateTimePicker3.TabIndex = 4;
            this.dateTimePicker3.ValueChanged += new System.EventHandler(this.dateTimePicker3_ValueChanged);
            // 
            // dateTimePicker4
            // 
            this.dateTimePicker4.Location = new System.Drawing.Point(280, 7);
            this.dateTimePicker4.Name = "dateTimePicker4";
            this.dateTimePicker4.Size = new System.Drawing.Size(130, 20);
            this.dateTimePicker4.TabIndex = 3;
            this.dateTimePicker4.ValueChanged += new System.EventHandler(this.dateTimePicker4_ValueChanged);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePicker2.Location = new System.Drawing.Point(144, 7);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(71, 20);
            this.dateTimePicker2.TabIndex = 2;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(8, 7);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(130, 20);
            this.dateTimePicker1.TabIndex = 1;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(6, 44);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(132, 23);
            this.button5.TabIndex = 0;
            this.button5.Text = "Список заказов";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // tabPage12
            // 
            this.tabPage12.Location = new System.Drawing.Point(4, 22);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage12.Size = new System.Drawing.Size(607, 75);
            this.tabPage12.TabIndex = 1;
            this.tabPage12.Text = "tabPage12";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // tabControl3
            // 
            this.tabControl3.Controls.Add(this.tabPage6);
            this.tabControl3.Controls.Add(this.tabPage7);
            this.tabControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl3.Location = new System.Drawing.Point(0, 0);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(647, 713);
            this.tabControl3.TabIndex = 7;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.listBox1);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(639, 687);
            this.tabPage6.TabIndex = 0;
            this.tabPage6.Text = "Машины";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // listBox1
            // 
            this.listBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Items.AddRange(new object[] {
            "13",
            "17",
            "25",
            "33"});
            this.listBox1.Location = new System.Drawing.Point(3, 3);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(633, 680);
            this.listBox1.TabIndex = 0;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.groupBox2);
            this.tabPage7.Controls.Add(this.groupBox1);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(639, 687);
            this.tabPage7.TabIndex = 1;
            this.tabPage7.Text = "Звонки";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.numericUpDown1);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.pictureBox1);
            this.groupBox2.Controls.Add(this.button15);
            this.groupBox2.Controls.Add(this.statusbar);
            this.groupBox2.Controls.Add(this.btnCallDial);
            this.groupBox2.Controls.Add(this.btnCallHangup);
            this.groupBox2.Controls.Add(this.btnCallHold);
            this.groupBox2.Controls.Add(this.button14);
            this.groupBox2.Controls.Add(this.listBox5);
            this.groupBox2.Location = new System.Drawing.Point(3, 102);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(526, 539);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "История звонков";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(19, 333);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(473, 112);
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(179, 136);
            this.button15.Margin = new System.Windows.Forms.Padding(2);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(56, 19);
            this.button15.TabIndex = 7;
            this.button15.Text = "button15";
            this.button15.UseVisualStyleBackColor = true;
            // 
            // statusbar
            // 
            this.statusbar.Location = new System.Drawing.Point(8, 181);
            this.statusbar.Margin = new System.Windows.Forms.Padding(2);
            this.statusbar.Name = "statusbar";
            this.statusbar.Size = new System.Drawing.Size(237, 20);
            this.statusbar.TabIndex = 6;
            // 
            // btnCallDial
            // 
            this.btnCallDial.Location = new System.Drawing.Point(186, 81);
            this.btnCallDial.Margin = new System.Windows.Forms.Padding(2);
            this.btnCallDial.Name = "btnCallDial";
            this.btnCallDial.Size = new System.Drawing.Size(56, 19);
            this.btnCallDial.TabIndex = 5;
            this.btnCallDial.Text = "btnCallDial";
            this.btnCallDial.UseVisualStyleBackColor = true;
            this.btnCallDial.Click += new System.EventHandler(this.btnCallDial_Click);
            // 
            // btnCallHangup
            // 
            this.btnCallHangup.Location = new System.Drawing.Point(186, 44);
            this.btnCallHangup.Margin = new System.Windows.Forms.Padding(2);
            this.btnCallHangup.Name = "btnCallHangup";
            this.btnCallHangup.Size = new System.Drawing.Size(56, 19);
            this.btnCallHangup.TabIndex = 4;
            this.btnCallHangup.Text = "Завершить";
            this.btnCallHangup.UseVisualStyleBackColor = true;
            // 
            // btnCallHold
            // 
            this.btnCallHold.Location = new System.Drawing.Point(186, 13);
            this.btnCallHold.Margin = new System.Windows.Forms.Padding(2);
            this.btnCallHold.Name = "btnCallHold";
            this.btnCallHold.Size = new System.Drawing.Size(56, 19);
            this.btnCallHold.TabIndex = 3;
            this.btnCallHold.Text = "Hold";
            this.btnCallHold.UseVisualStyleBackColor = true;
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(10, 260);
            this.button14.Margin = new System.Windows.Forms.Padding(2);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(56, 19);
            this.button14.TabIndex = 2;
            this.button14.Text = "button14";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // listBox5
            // 
            this.listBox5.FormattingEnabled = true;
            this.listBox5.Location = new System.Drawing.Point(10, 37);
            this.listBox5.Margin = new System.Windows.Forms.Padding(2);
            this.listBox5.Name = "listBox5";
            this.listBox5.Size = new System.Drawing.Size(118, 69);
            this.listBox5.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.callBtnHold);
            this.groupBox1.Controls.Add(this.callBtnHangup);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.callBtnDial);
            this.groupBox1.Controls.Add(this.callPhone);
            this.groupBox1.Location = new System.Drawing.Point(3, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(252, 90);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Телефон";
            // 
            // callBtnHold
            // 
            this.callBtnHold.Location = new System.Drawing.Point(168, 51);
            this.callBtnHold.Name = "callBtnHold";
            this.callBtnHold.Size = new System.Drawing.Size(75, 20);
            this.callBtnHold.TabIndex = 4;
            this.callBtnHold.Text = "Перевести";
            this.callBtnHold.UseVisualStyleBackColor = true;
            // 
            // callBtnHangup
            // 
            this.callBtnHangup.Location = new System.Drawing.Point(87, 51);
            this.callBtnHangup.Name = "callBtnHangup";
            this.callBtnHangup.Size = new System.Drawing.Size(75, 20);
            this.callBtnHangup.TabIndex = 3;
            this.callBtnHangup.Text = "Завершить";
            this.callBtnHangup.UseVisualStyleBackColor = true;
            this.callBtnHangup.Click += new System.EventHandler(this.callBtnHangup_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 74);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(85, 13);
            this.label19.TabIndex = 2;
            this.label19.Text = "статус звонка: ";
            // 
            // callBtnDial
            // 
            this.callBtnDial.Location = new System.Drawing.Point(6, 51);
            this.callBtnDial.Name = "callBtnDial";
            this.callBtnDial.Size = new System.Drawing.Size(75, 20);
            this.callBtnDial.TabIndex = 1;
            this.callBtnDial.Text = "Набрать";
            this.callBtnDial.UseVisualStyleBackColor = true;
            this.callBtnDial.Click += new System.EventHandler(this.callBtnDial_Click);
            // 
            // callPhone
            // 
            this.callPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.callPhone.Location = new System.Drawing.Point(6, 19);
            this.callPhone.Name = "callPhone";
            this.callPhone.Size = new System.Drawing.Size(237, 26);
            this.callPhone.TabIndex = 0;
            this.callPhone.Text = "120";
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "NewDataSet";
            this.dataSet1.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable1});
            // 
            // dataTable1
            // 
            this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2});
            this.dataTable1.TableName = "ord";
            // 
            // dataColumn1
            // 
            this.dataColumn1.Caption = "фывпфып";
            this.dataColumn1.ColumnName = "Column1";
            // 
            // dataColumn2
            // 
            this.dataColumn2.Caption = "фыв33";
            this.dataColumn2.ColumnName = "Column2";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(268, 230);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // mapControl1
            // 
            this.mapControl1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.mapControl1.CachePath = "";
            this.mapControl1.ContextMenuStrip = this.contextMenuStrip1;
            this.mapControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mapControl1.HighlightEntities = false;
            this.mapControl1.Latitude = 53.9753;
            this.mapControl1.Location = new System.Drawing.Point(0, 0);
            this.mapControl1.Longitude = -1.7017;
            this.mapControl1.LonScale = 0.00390625;
            this.mapControl1.Name = "mapControl1";
            this.mapControl1.Size = new System.Drawing.Size(277, 687);
            this.mapControl1.TabIndex = 2;
            this.mapControl1.TabStop = false;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(359, 209);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            -2147483648});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown1.TabIndex = 11;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(359, 46);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 12;
            this.button2.Text = "настройки SIP";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(936, 713);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.tabPage8.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.tabControl4.ResumeLayout(false);
            this.tabPage9.ResumeLayout(false);
            this.tabPage10.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.tabControl5.ResumeLayout(false);
            this.tabPage11.ResumeLayout(false);
            this.tabControl3.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem маршрутОтсюдаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem маршрутСюдаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem промежуточнаяТочка1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem промежуточнаяТочка2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem очиститьМаршрутToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem очиститьТочку1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem очиститьПромежуточнуюТочку2ToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox connectPhone;
        private System.Windows.Forms.TextBox phone;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DateTimePicker predv_tm;
        private System.Windows.Forms.DateTimePicker predv_dt;
        private System.Windows.Forms.CheckBox predv;
        private System.Windows.Forms.TextBox prim;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox zone_from;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox over_building_number2;
        private System.Windows.Forms.TextBox coord_over_2;
        private System.Windows.Forms.ComboBox over_street2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox over_building_number1;
        private System.Windows.Forms.TextBox coord_over_1;
        private System.Windows.Forms.ComboBox over_street1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox to_building_number;
        private System.Windows.Forms.ComboBox from_building_number;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox coord_to;
        private System.Windows.Forms.TextBox coord_from;
        private System.Windows.Forms.CheckBox use_sql;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label path_cost;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label distance;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox service_name;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox to_street;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox from_street;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage1;
        private csmapcontrol.MapControl mapControl1;
        private System.Windows.Forms.TabControl tabControl3;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button callBtnHold;
        private System.Windows.Forms.Button callBtnHangup;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button callBtnDial;
        private System.Windows.Forms.TextBox callPhone;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TabControl tabControl4;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TabControl tabControl5;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.Windows.Forms.DateTimePicker dateTimePicker4;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.ListBox listBox3;
        private System.Windows.Forms.ListBox listBox4;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Data.DataSet dataSet1;
        private System.Data.DataTable dataTable1;
        private System.Windows.Forms.Button button11;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Windows.Forms.ListBox listBox5;
        private System.Windows.Forms.Button button14;
        public System.Windows.Forms.Button btnCallHold;
        public System.Windows.Forms.Button btnCallDial;
        public System.Windows.Forms.Button btnCallHangup;
        public System.Windows.Forms.TextBox statusbar;
        public System.Windows.Forms.Button button15;
        public System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Button button2;
    }
}

