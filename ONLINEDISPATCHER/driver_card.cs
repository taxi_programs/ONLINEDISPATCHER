﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using csmapcontrol;
using System.Data.SqlClient;

namespace ONLINEDISPATCHER
{
    public partial class driver_form : Form
    {
        public string current_order_id = "";
        //SipCaller m_pSipCaller = null;

        int driver_id = 0;
        int car_service_id = 0;

        private static Point lastLocation = new Point(100, 100);
        

        public driver_form()
        {
            InitializeComponent();
//            m_pSipCaller = caller;
            btnEndCall.Visible = true;

            if (driver_form.lastLocation != null)
            {
                this.StartPosition = FormStartPosition.Manual;
                this.Location = driver_form.lastLocation;
            }
        }

        private void button2_Click(object sender, EventArgs e) {
            /*select_car form = new select_car();
            form.btnDial.Visible = false;
            form.btnSelect.Visible = false;
            
            form.Show();
            form.comboCars.SelectedValue = this.Tag;
            */
            if (this.Tag.ToString().Length == 0)
                return;
            int carid = int.Parse(this.Tag.ToString());


            DataTable driver_info = DataBase.mssqlRead("SELECT cars.serviceid from drivers, cars where drivers.carid = cars.id and drivers.ID=" + driver_id.ToString());
            if (driver_info.Rows.Count == 0){
                MessageBox.Show("Водитель или машина не найдены", "Ошибка", MessageBoxButtons.OK);
                return;
            }

            if (driver_info.Rows[0]["SERVICEID"].ToString().CompareTo("10") == 0) {
                //служба онлайн2 - запрет обещанного платежа
                MessageBox.Show("Водитель второго приоритета. Обещанный платеж не разрешен", "Ошибка", MessageBoxButtons.OK);
                return;
            }

            //проверить, нет ли непогашенного платежа
            //узнать службу водителя
            //внести 500 р в банк
            if (MessageBox.Show("Внести обещанный платеж?", "Обещанный платеж", MessageBoxButtons.YesNo) != DialogResult.Yes)
                return;




            DataTable bank_info = DataBase.mssqlRead("SELECT dtArrive from bank where DriverID=" + driver_id.ToString() + " and promised_payment=1");
            if (bank_info.Rows.Count > 0)
            {
                MessageBox.Show("Ошибка. У водителя уже внесен обещанный платеж " + bank_info.Rows[0]["dtArrive"].ToString(), "Обещанный платеж", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }



            String[] fields = { "driverid", "ServiceID", "OpID", "dtArrive", "sum", "type", "promised_payment", "Descr" };
            object[] values = { driver_id, car_service_id, MAIN_FORM.OperatorID, DateTime.Now, 50, 0, 1, "<ОБЕЩАННЫЙ ПЛАТЕЖ>" };

            DataBase.mssqlInsert("BANK", fields, values);

            MessageBox.Show("Внесен обещанный платеж 50 руб", "Обещанный платеж", MessageBoxButtons.OK, MessageBoxIcon.None);
            







        }

        private void button3_Click(object sender, EventArgs e) {
            if (current_order_id != "")
            {
                if (settings.UseNewOrderForm) {
                    order_big o = new order_big();
                    o.doLoad(int.Parse(current_order_id), "", "", "");
                    o.Show();
                } 
            }
        }

        private void btnEndCall_Click(object sender, EventArgs e) {
            //if (m_pSipCaller != null) {
              //  m_pSipCaller.doHangup();
            if (MAIN_FORM.caller == null)
                return;

                btnEndCall.Visible = false;
                //MAIN_FORM.main_form_object.doStartPostCallTimer();

                //caller.doHangup();
                MAIN_FORM.caller.doHangup();


                //to do сделать задержку на прием нового звонка пока открыто окно

                //m_pSipCaller.doBlockIncomingCalls();
                
            //}
        }

        private void button1_Click(object sender, EventArgs e) {
            MAIN_FORM.main_form_object.doShowCarOrders((int.Parse(this.Tag.ToString())));
        }

        private void driver_form_Shown(object sender, EventArgs e)
        {
            //загрузить инфо по водителю

            //this.textBox1.SelectAll();
            this.textBox1.Focus();



            



            
            if (this.Tag.ToString().Length==0)
                return;
            this.Text = "Карточка водителя " + this.Tag.ToString();
            String sql = "select drivers.id, cars.serviceid, cars.color+ ' ' +cars.mark + ' ' + cars.gosnum + ' ' + cars.descr car_descr, drivers.fio , drivers.phones,  drivers.phone2, drivers.phone3, drivers.descr, balance.summ balance, cars.block_to, cars.enable_beznal from cars,drivers "+
                " left join  balance on balance.doc_typ='DRIVER' and balance.doc_id = drivers.id "+
                " where cars.id = drivers.carid and cars.id = " + this.Tag; ;
            DataTable dt = DataBase.mssqlRead(sql);

            if (dt.Rows.Count > 0)
            {
                text_car_name.Text = dt.Rows[0]["car_descr"].ToString();
                text_driver_fio.Text = dt.Rows[0]["fio"].ToString();
                text_comment.Text = dt.Rows[0]["descr"].ToString();

                btn_phones.Text = dt.Rows[0]["phones"].ToString();
                btn_phone2.Text = dt.Rows[0]["phone2"].ToString();
                btn_phone3.Text = dt.Rows[0]["phone3"].ToString();
                btn_balance.Text = dt.Rows[0]["balance"].ToString() + ". Обещаный платеж";
                this.block_descr.Text = "";
                if (dt.Rows[0]["block_to"].ToString().Length > 0)
                    if (((DateTime)dt.Rows[0]["block_to"]) > DateTime.Now)
                        this.block_descr.Text = "Блокировка до " + dt.Rows[0]["block_to"].ToString();

                driver_id = (int)dt.Rows[0]["id"];
                car_service_id = (int)dt.Rows[0]["serviceid"];

                label_beznal.Visible = ((int)dt.Rows[0]["enable_beznal"]==0);

            }
            else
            {
                text_car_name.Text = "";
                text_driver_fio.Text = "";
                text_comment.Text = "";

                btn_phones.Text = "";
                btn_phone2.Text = "";
                btn_phone3.Text = "";
                btn_balance.Text = "";
                this.block_descr.Text = "";
                this.block_descr.Text = "";

                driver_id = 0;
                car_service_id = 0;
                label_beznal.Visible = false;
            }

        }

        private void btn_new_order_Click(object sender, EventArgs e)
        {
           if (settings.UseNewOrderForm)
            {
                order_big o = new order_big();
                o.doLoad(0, "", "", "");
                o.Phone.Text = btn_phones.Text;
                o.Show();
            }
            
            
        }

        private void btn_phones_Click(object sender, EventArgs e)
        {
            //if (m_pSipCaller != null) 
                //m_pSipCaller.doHangup();


            //this.m_pSipCaller = 
                MAIN_FORM.main_form_object.doDialPhone(this.btn_phones.Text);
            this.btnEndCall.Visible = true;
        }

        private void btn_phone2_Click(object sender, EventArgs e)
        {
            //if (m_pSipCaller != null) 
                //m_pSipCaller.doHangup();

            //this.m_pSipCaller = 
                MAIN_FORM.main_form_object.doDialPhone(this.btn_phone2.Text);
            this.btnEndCall.Visible = true;
        }

        private void btn_phone3_Click(object sender, EventArgs e)
        {
            //if (m_pSipCaller != null) 
                //m_pSipCaller.doHangup();

            //this.m_pSipCaller = 
                MAIN_FORM.main_form_object.doDialPhone(this.btn_phone3.Text);
            this.btnEndCall.Visible = true;
        }

        private void driver_form_FormClosing(object sender, FormClosingEventArgs e)
        {
            driver_form.lastLocation = this.Location;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            this.Tag = textBox1.Text;

            driver_form_Shown(null, null);

        }

        private void driver_form_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void driver_form_Load(object sender, EventArgs e)
        {
            
            dateTimePicker3.Value = DateTime.Now.AddDays(-1);
            dateTimePicker1.Value = dateTimePicker3.Value.Date;
            
            dateTimePicker4.Value = DateTime.Now;
            dateTimePicker2.Value = dateTimePicker4.Value.Date;

            this.textBox1.SelectAll();
            this.textBox1.Focus();




            DataTable cars = DataBase.mssqlRead("SELECT CARS.ID, cast (CARS.ID as varchar(6))+' '+  CARS.COLOR +' '+  CARS.mark +' '+ cars.gosnum +' '+drivers.fio +' '+ drivers.phones CAR_DRIVER_DESC  FROM CARS, DRIVERS WHERE DRIVERS.ID = CARS.DRIVERID order by CARS.ID");



            easyCompletionComboBox1.DataSource = cars;
            easyCompletionComboBox1.DisplayMember = "CAR_DRIVER_DESC";
            easyCompletionComboBox1.ValueMember = "ID";
            easyCompletionComboBox1.DropDownStyle = ComboBoxStyle.DropDown;
            easyCompletionComboBox1.AutoCompleteSource = AutoCompleteSource.ListItems;
            //  ZoneID.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            //comboCars.SelectedIndex = -1;
            //easyCompletionComboBox1.AutoCompleteMode = easyCompletionComboBox.
            easyCompletionComboBox1.MatchingMethod = SergeUtils.StringMatchingMethod.UseRegexs;
            easyCompletionComboBox1.Text = "";
            //easyCompletionComboBox1.DropDownHeight = 400;



        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            dateTimePicker3.Value = DateTime.Now.AddHours(-2.0);
            dateTimePicker1.Value = dateTimePicker3.Value.Date;

            dateTimePicker4.Value = DateTime.Now;
            dateTimePicker2.Value = dateTimePicker4.Value.Date;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            dateTimePicker3.Value = DateTime.Now.AddHours(-4.0);
            dateTimePicker1.Value = dateTimePicker3.Value.Date;

            dateTimePicker4.Value = DateTime.Now;
            dateTimePicker2.Value = dateTimePicker4.Value.Date;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            dateTimePicker3.Value = DateTime.Now.AddDays(-1.0);
            dateTimePicker1.Value = dateTimePicker3.Value.Date;

            dateTimePicker4.Value = DateTime.Now;
            dateTimePicker2.Value = dateTimePicker4.Value.Date;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            dateTimePicker3.Value = DateTime.Now.AddDays(-2.0);
            dateTimePicker1.Value = dateTimePicker3.Value.Date;

            dateTimePicker4.Value = DateTime.Now.AddDays(-1.0);
            dateTimePicker2.Value = dateTimePicker4.Value.Date;
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            int carid = 0;
            int.TryParse(this.Tag.ToString(), out carid);
            if (carid > 0)
            {
                Track t = new Track();

                List<SqlParameter> parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("dt1", dateTimePicker1.Value.Date + dateTimePicker3.Value.TimeOfDay));
                parameters.Add(new SqlParameter("dt2", dateTimePicker2.Value.Date + dateTimePicker4.Value.TimeOfDay));

                //String sql = "SELECT * FROM cars_gps_log where carid = " + carid.ToString() + " and gps_time between @dt1 and @dt2 order by gps_time";
                //DataTable dt = DataBase.mssqlRead(sql, parameters.ToArray());

                //if (dt.Rows.Count == 0)
                //{
                String sql = "SELECT cast (lat as integer) lat, cast (lon as integer) lon, speed, cast(0 as float) accuracy, dtReceived as gps_time FROM gps_log where carid = " + carid.ToString() + " and dtReceived between @dt1 and @dt2 order by dtReceived";
                DataTable dt = DataBase.mssqlRead(sql, parameters.ToArray());

                //}

                foreach (DataRow row in dt.Rows)
                {
                    TrackPoint item = new TrackPoint();
                    item.Lat = ((int)row["lat"]) / 1000000.0;
                    item.Lon = ((int)row["lon"]) / 1000000.0;
                    item.speed = (double)row["speed"];
                    item.accuracy = (double)row["accuracy"];
                    item.gps_time = (DateTime)row["gps_time"];
                    item.Time = (DateTime)row["gps_time"];


                    if ((item.Lat > 0) && (item.Lon > 0))
                        t.Points.Add(item);
                }
                MAIN_FORM.main_form_object.mapControl1.Tracks.Clear();
                MAIN_FORM.main_form_object.mapControl1.Tracks.Add(t);
            }
        }

        private void btn_open_car_Click(object sender, EventArgs e) {


            int id = int.Parse(this.Tag.ToString());
            DictionaryElement de = new DictionaryElement();
            //de.doMakeInterface(dic_type);

            de.Show();
            de.doLoad("CARS", id);
        }

        private void btn_open_driver_Click(object sender, EventArgs e) {
            DataTable dt = DataBase.mssqlRead("SELECT ID  FROM DRIVERS where CARID='" + this.Tag.ToString() + "' and state=0");
            if (dt.Rows.Count > 0) {
                int driver_id = (int)dt.Rows[0]["ID"];


                //int id = int.Parse(this.Tag.ToString());
                DictionaryElement de = new DictionaryElement();
                //de.doMakeInterface(dic_type);

                de.Show();
                de.doLoad("DRIVERS", driver_id);
            }
        }

        private void btn_money_Click(object sender, EventArgs e) {

            int id = 0;
            int.TryParse(this.Tag.ToString(), out id);
            add_money form = new add_money();
            form.selected_car_id = id;
            form.Show();
        }

        private void Бэйдж_Click(object sender, EventArgs e)
        {

            DataTable dt = DataBase.mssqlRead("SELECT fio, carid, photo  FROM DRIVERS where CARID='" + this.Tag.ToString() + "' and state=0");
            if (dt.Rows.Count <= 0)
                return;


            dataSet1.Tables["driver_card"].Clear();
            String fio = (String)dt.Rows[0]["fio"];
            fio = fio.Replace(' ', '\n');


            //object[] values = { (String)dt.Rows[0]["fio"], (int)dt.Rows[0]["carid"], (byte [])dt.Rows[0]["photo"] };
            object[] values = { fio , (int)dt.Rows[0]["carid"], dt.Rows[0]["photo"] };
            dataSet1.Tables["driver_card"].LoadDataRow(values, true);


            report1.Show();
        }

        private void btnFromQueue_Click(object sender, EventArgs e) {
            if (MessageBox.Show("Снять водителя с очереди?", "Снятие с очереди", MessageBoxButtons.YesNo) != DialogResult.Yes)
                return;
            DataBase.mssqlExecuteSQL("delete from cars_distrib where carid=" + this.Tag);
            DataBase.mssqlExecuteSQL("delete from mobile_car_visible_count where carid=" + this.Tag);
            DataBase.mssqlExecuteSQL("insert into mobile_car_visible_count(carid, zoneid, orders_cnt, cars_cnt, changed, tm) select "+this.Tag+", zones.id, 0, 0, 1, getdate() from zones");
            DataBase.mssqlExecuteSQL("update cars set networkStatus = 0, mobileStatus = (mobileStatus&65520) where id=" + this.Tag);
            //DataBase.mssqlExecuteSQL("update cars set networkStatus = 0, mobileStatus = (mobileStatus&65520) where id=" + this.Tag);
            
    
    
            DataBase.mssqlExecuteSQL("insert into mobile_commands(cmd, carid,   network_channel_id, intParam1,  strParam1, strParam2) select 'ZONE_CARS_CHANGED', "+this.Tag+", cars.network_channel_id, 0, 'НЕТ РЕГИСТРАЦИИ(Оператор)', '' from cars where cars.id = "+this.Tag);            

            
            

        }

        private void btnBlock30_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Заблокировать на полчаса?", "Блокировка", MessageBoxButtons.YesNo) != DialogResult.Yes)
                return;
            DataBase.mssqlExecuteSQL("update cars set block_to = getdate() + 30.0/24/60 where id = "+this.Tag);

            String[] fields = { "ComID", "OpID", "dtArrive", "Value1", "message", "State" };
            object[] values = { 5, MAIN_FORM.OperatorID, DateTime.Now, this.Tag, "Блокировка на 30 минут", 0 };

            DataBase.mssqlInsert("COMMANDS", fields, values);

            driver_form_Shown(null, null);
        }

        private void block60_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Заблокировать на час?", "Блокировка", MessageBoxButtons.YesNo) != DialogResult.Yes)
                return;
            DataBase.mssqlExecuteSQL("update cars set block_to = getdate() + 60.0/24/60 where id = " + this.Tag);
            String[] fields = { "ComID", "OpID", "dtArrive", "Value1", "message", "State" };
            object[] values = { 5, MAIN_FORM.OperatorID, DateTime.Now, this.Tag, "Блокировка на 60 минут", 0 };

            DataBase.mssqlInsert("COMMANDS", fields, values);
            driver_form_Shown(null, null);
        }

        private void blockCancel_Click(object sender, EventArgs e)
        {
            DataBase.mssqlExecuteSQL("update cars set block_to = getdate()-1 where id = "+this.Tag);
            driver_form_Shown(null, null);
        }

        private void block15_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Заблокировать на 15 мин?", "Блокировка", MessageBoxButtons.YesNo) != DialogResult.Yes)
                return;
            DataBase.mssqlExecuteSQL("update cars set block_to = getdate() + 15.0/24/60 where id = " + this.Tag);
            String[] fields = { "ComID", "OpID", "dtArrive", "Value1", "message", "State" };
            object[] values = { 5, MAIN_FORM.OperatorID, DateTime.Now, this.Tag, "Блокировка на 15 минут", 0 };

            DataBase.mssqlInsert("COMMANDS", fields, values);
            driver_form_Shown(null, null);
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DataTable dt = DataBase.mssqlRead("SELECT ID  FROM DRIVERS where CARID='" + this.Tag.ToString() + "' and state=0");
            if (dt.Rows.Count > 0)
            {
                int driver_id = (int)dt.Rows[0]["ID"];


                //int id = int.Parse(this.Tag.ToString());
                dictionary dic = new dictionary();
                dic.summ_value_index = 3;
                //de.doMakeInterface(dic_type);
                dic.Show();
                dic.Tag = driver_id.ToString();
                dic.makeInterface("DRIVER_PAYMENTS");
            }
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DataTable dt = DataBase.mssqlRead("SELECT ID  FROM DRIVERS where CARID='" + this.Tag.ToString() + "' and state=0");
            if (dt.Rows.Count > 0)
            {
                int driver_id = (int)dt.Rows[0]["ID"];


                //int id = int.Parse(this.Tag.ToString());
                dictionary dic = new dictionary();
                
                //de.doMakeInterface(dic_type);
                dic.Show();
                dic.Tag = driver_id.ToString();
                dic.makeInterface("DRIVER_BALANCE_HISTORY");
            }
        }

        private void easyCompletionComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void easyCompletionComboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            String sID = easyCompletionComboBox1.SelectedValue.ToString();
            textBox1.Text = sID;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            String dt1 = dateTimePicker1.Value.Date.ToString("yyyy-MM-dd")+ ' ' + dateTimePicker3.Text;
            String dt2 = dateTimePicker2.Value.Date.ToString("yyyy-MM-dd") + ' ' + dateTimePicker4.Text;

            String sql = "select  'from_server' 'Направление', tm 'Время', cmd 'Команда',  intParam1 'Данные1', intparam2, strParam1 'Данные2', strparam2 from mobile_commands  where carid=" + textBox1.Text + " and tm between '" + dt1 + "' and '" + dt2 + "'" +
            " union  " +
            " select 'from_driver', tm, doc,  intparam1, intparam2, strparam1, strparam2 from history where dop2=" + textBox1.Text + " and tm between '" + dt1 + "' and '" + dt2 + "'" +
            " order by tm desc";

            DataTable readTbl = DataBase.mssqlRead(sql);
            String result="";

            for(int i=0; i< readTbl.Rows.Count; i++){
                result += readTbl.Rows[i][0].ToString() + '\t' + readTbl.Rows[i][1].ToString() + '\t' + readTbl.Rows[i][2].ToString() + '\t' + readTbl.Rows[i][3].ToString() + '\t' + readTbl.Rows[i][4].ToString() + "\r\n"
                    + readTbl.Rows[i][5].ToString() + "\r\n" + readTbl.Rows[i][6].ToString() + "\r\n";
            
            }

            text_show_form form = new text_show_form();
            form.textBox1.Text = result;
            form.Show();
        }
        



    }
}
