﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace ONLINEDISPATCHER
{
    public class gisModule
    {
          //int getLocationRaion(lat, lon) -- вернет район по координатам
          //geoPoint getLocationByText(String city, String street, String housenumber, String name, bool over_yandex) - координаты. поиск по внутренней базе или по яндексу
          //double getDistanceByPoints( getPoints[]) // расстояние в километрах



        private static System.Data.DataTable ADRESSES = null;

        private static System.Data.DataTable adresses_names = null;

        public class geoPoint
        {
            public geoPoint()
            {
            }

            public geoPoint(double _lat, double _lon)
            {
                lat = _lat;
                lon = _lon;
            }
            public double lat=0;
            public double lon=0;

            public int isMoved = 0;
        }


        public static double Distance(geoPoint pos1, geoPoint pos2) {
            double R =  6371;
            double dLat = toRadian(pos2.lat - pos1.lat);
            double dLon = toRadian(pos2.lon - pos1.lon);
            double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
                Math.Cos(toRadian(pos1.lat)) * Math.Cos(toRadian(pos2.lat)) *
                Math.Sin(dLon / 2) * Math.Sin(dLon / 2);
            double c = 2 * Math.Asin(Math.Min(1, Math.Sqrt(a)));
            double d = R * c;
            return d;
        }

        public static double Distance(double lat1, double lon1, double lat2, double lon2) {
            double R = 6371;
            double dLat = toRadian(lat2 - lat1);
            double dLon = toRadian(lon2 - lon1);
            double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
                Math.Cos(toRadian(lat1)) * Math.Cos(toRadian(lat2)) *
                Math.Sin(dLon / 2) * Math.Sin(dLon / 2);
            double c = 2 * Math.Asin(Math.Min(1, Math.Sqrt(a)));
            double d = R * c;
            return d;
        }

        private static double toRadian(double val) {
            return (Math.PI / 180) * val;
        }  

        public class ZonePolygon
        {

            //object fields
            public int zone_id = 0; //присвоенный код.
            public String zone_name = "";
            public int ID = 0;  //автомарически присваемый код, номер по порядку в списке.
            public int remote_zone = 0;
            public List<geoPoint> zonePoints = new List<geoPoint>();


            public void AddZonePolygonPoint(geoPoint point)
            {
                zonePoints.Add(point);
            }



            //static fields - for list of objects

            public static List<ZonePolygon> list_zone_polygons = new List<ZonePolygon>();
            public static ZonePolygon CITY_zone_polygon = new ZonePolygon();

            public static ZonePolygon AddZonePolygon()
            {
                ZonePolygon p = new ZonePolygon();
                list_zone_polygons.Add(p);
                p.ID = list_zone_polygons.Count;

                return p;
            }

        }

        public static void doLoadGeoData()
        {
            //загрузить регионы
            //загрузить таблицу адресов

            ZonePolygon.list_zone_polygons.Clear();
            ZonePolygon.CITY_zone_polygon = new ZonePolygon();

            DataTable dt = DataBase.mssqlRead("SELECT ZONES_POLYGONS.*, ZONES.DESCR FROM ZONES_POLYGONS LEFT JOIN ZONES ON ZONES.ID=ZONES_POLYGONS.ZONE_ID where zones_polygons.zone_id > 0 order by ZONE_ID, ID");
            int old_polygon_number = -1;
            foreach (DataRow row in dt.Rows)
            {
                int zone_id = (int)row["ZONE_ID"];
                int polygon_number = (int)row["POLYGON_NUMBER"];
                int remote_zone = (int)row["remote_zone"];


                String name = row["DESCR"].ToString();

                String POLYGONS_DATA = (String)row["POLYGONS_DATA"];

                String[] latlon = POLYGONS_DATA.Split('&');

                gisModule.ZonePolygon zp = null;
                zp = gisModule.ZonePolygon.AddZonePolygon();
                zp.zone_id = zone_id;
                zp.zone_name = name;
                zp.remote_zone = remote_zone;

                double lat = 0;
                double lon = 0;
                foreach (String ll in latlon)
                {

                    String[] ll2 = ll.Split('|');

                    if (ll2.Length == 2)
                    {
                        double.TryParse(ll2[0], out lat);
                        double.TryParse(ll2[1], out lon);

                        gisModule.geoPoint p = new gisModule.geoPoint();
                        p.lat = lat;
                        p.lon = lon;
                        zp.AddZonePolygonPoint(p);
                    }

                }

            }




            dt = DataBase.mssqlRead("SELECT ZONES_POLYGONS.*, ZONES.DESCR FROM ZONES_POLYGONS LEFT JOIN ZONES ON ZONES.ID=ZONES_POLYGONS.ZONE_ID where zones_polygons.zone_id = -1");
            
            foreach (DataRow row in dt.Rows) {
                int zone_id = (int)row["ZONE_ID"];
                int polygon_number = (int)row["POLYGON_NUMBER"];
                int remote_zone = (int)row["remote_zone"];


                String name = row["DESCR"].ToString();

                String POLYGONS_DATA = (String)row["POLYGONS_DATA"];

                String[] latlon = POLYGONS_DATA.Split('&');

                


                ZonePolygon.CITY_zone_polygon.zone_id = zone_id;
                ZonePolygon.CITY_zone_polygon.zone_name = name;
                ZonePolygon.CITY_zone_polygon.remote_zone = remote_zone;

                double lat = 0;
                double lon = 0;
                foreach (String ll in latlon) {

                    String[] ll2 = ll.Split('|');

                    if (ll2.Length == 2) {
                        double.TryParse(ll2[0], out lat);
                        double.TryParse(ll2[1], out lon);

                        gisModule.geoPoint p = new gisModule.geoPoint();
                        p.lat = lat;
                        p.lon = lon;
                        ZonePolygon.CITY_zone_polygon.AddZonePolygonPoint(p);
                    }

                }

            }






            //ADRESSES = DataBase.mssqlRead("SELECT * FROM ADRESSES");
            ADRESSES = DataBase.mssqlRead("SELECT * FROM GIS_ADRESSES where state < 9");


            adresses_names = DataBase.mssqlRead("select distinct name from adresses " +
            "union "+
            "select distinct name from gis_adresses order by name");

        }

        public static System.Data.DataTable get_adresses_names(){
            if (adresses_names == null)
                doLoadGeoData();
            return adresses_names;
        }

        public static String[] getStreets(String city)
        {
            if (ADRESSES == null)
                doLoadGeoData();

            DataRow[] rows;
            
            /*if (city != "")
                rows = ADRESSES.Select("CITY = " + city, "STREET");
            else
                rows = ADRESSES.Select("CITY = 'Ярославль'", "STREET");
             */
            rows = ADRESSES.Select("","STREET");

            List<String> string_list = new List<string>();
            for (int i = 0; i < rows.Length; i++)
            {
                if (rows[i]["STREET"] != DBNull.Value)
                    string_list.Add((String)rows[i]["STREET"]);
            }

            return string_list.ToArray();
        }

        public static String[] getStreetNumbers(String city, String street)
        {
            if (ADRESSES == null)
                doLoadGeoData();

            DataRow[] rows;
            /*
            if (city != "")
                rows = ADRESSES.Select("CITY = '" + city+"' and STREET='"+street+"'", "number");
            else
                rows = ADRESSES.Select("CITY = 'Ярославль' and STREET='"+street+"'", "number");
            */
            rows = ADRESSES.Select("STREET='" + street + "'", "number");

            List<String> string_list = new List<string>();
            for (int i = 0; i < rows.Length; i++)
            {
                string_list.Add((String)rows[i]["NUMBER"]);
            }

            return string_list.ToArray();
        }


        public static geoPoint getLocationByText(String city, String street, String housenumber, String name, bool over_yandex)// - координаты. поиск по внутренней базе или по яндексу
        {
            if (ADRESSES == null)
                doLoadGeoData();

            geoPoint result = new geoPoint(0, 0);
            if (over_yandex == false)
            {
                DataRow [] rows;
                /*
                if (city != "")
                    rows = ADRESSES.Select("CITY = '"+city+"' and STREET='" + street + "' and NUMBER='" + housenumber + "'");
                else
                    rows = ADRESSES.Select("CITY = 'Ярославль' and STREET='" + street + "' and NUMBER='"+housenumber+"'");
                 */
                rows = ADRESSES.Select("STREET='" + street + "' and NUMBER='" + housenumber + "'");

                if (rows.Length > 0)
                {
                    result.lat = (double)rows[0]["lat"];
                    result.lon = (double)rows[0]["lon"];
                }
            }
            else{
                result = doYandexSearchAddress(city + "+" + street + "+" + housenumber);
            }




            return result;

        }


        private static geoPoint doYandexSearchAddress(String search_adress)
        {

            String url = "http://geocode-maps.yandex.ru/1.x/?format=json&geocode=";
            url += search_adress;
            //url += "&ll=37.618920,55.756994&spn=3.552069,2.400552";


            int timeout = 4000;
            string result = "0.0 0.0";
            String response = httpModule.GetHtmlPageText(url, timeout);
            if (response == "")
            {
                //calcDone = true;

                System.Console.WriteLine("Пустой ответ - ошибка расчета");
                return new geoPoint(0, 0);
            }

            if (response.Contains("Point\":{\"pos\":\"39.88771 57.62250\"}"))
            {
                //calcDone = true;

                System.Console.WriteLine("не найден адрес - центр города - ошибка расчета");
                return new geoPoint(0,0);
            }
            System.Console.Write(response);

            // "Point":{"pos":"39.879378 57.596803"}
            int s = response.IndexOf("\"Point\":{\"pos\":\"");
            int e = 0;
            if (s > 0)
            {
                s += "\"Point\":{\"pos\":\"".Length;
                e = response.IndexOf("\"}", s);
                if (e > s)
                {
                    String lonlat = response.Substring(s, e - s);

                    lonlat = lonlat.Replace('.', ',');
                    System.Console.WriteLine(lonlat + " " + search_adress);

                    Double lat1, lon1;
                    lon1 = Double.Parse(lonlat.Split(' ')[0]);
                    lat1 = Double.Parse(lonlat.Split(' ')[1]);

                    return new geoPoint(lat1, lon1);
                }
            }

            //                
            return new geoPoint(0, 0);
        }


        public static bool PointInZonePolygon(geoPoint Test, ZonePolygon polygon)
        {
            bool result = false;
            if (Test == null)
                return false;

            int i, j, nvert = polygon.zonePoints.Count;
            for (i = 0, j = nvert - 1; i < nvert; j = i++)
                if (((polygon.zonePoints[i].lon > Test.lon) != (polygon.zonePoints[j].lon > Test.lon)) &&
                   (Test.lat < (polygon.zonePoints[j].lat - polygon.zonePoints[i].lat) *
                   (Test.lon - polygon.zonePoints[i].lon) / (polygon.zonePoints[j].lon - polygon.zonePoints[i].lon) + polygon.zonePoints[i].lat))
                    result = !result;
            return result;
        }



        public static int getPointRaion(geoPoint point) // вернет район по координатам
        {
            if (point == null)
                return 0;
            for (int i = 0; i < ZonePolygon.list_zone_polygons.Count; i++)
            {
                ZonePolygon p = ZonePolygon.list_zone_polygons[i];
                if (PointInZonePolygon(point, p))
                {
                    if (p.remote_zone == 1)
                        return -p.zone_id;
                    else
                        return p.zone_id;
                }
            }
            return 0;
        }

        public static void showPointIn2GIS(geoPoint point, String name)
        {
            try
            {

                GrymCore.IGrym grymApp = null;
                GrymCore.IBaseReference baseRef = null;
                GrymCore.IBaseViewThread baseViewTread = null;
                GrymCore.ICommandLine cmdLine = null;
                grymApp = new GrymCore.GrymClass();

                //GrymCore.IGrymObjectFactory factory = GrymCore.IGrymObjectFactory
                // Получаем описание файла данных для заданного города
                // из коллекции описаний.
                baseRef = grymApp.BaseCollection.FindBase("Ярославль");
                baseViewTread = grymApp.GetBaseView(baseRef, true, false);
                GrymCore.IMapPoint geoPoint = baseViewTread.Factory.CreateMapPoint(point.lon, point.lat);

                baseViewTread.Activate(3);
                GrymCore.IMapCoordinateTransformationGeo transformate = (GrymCore.IMapCoordinateTransformationGeo)baseViewTread.Frame.Map.CoordinateTransformation;
                //grymApp.

                GrymCore.IMapPoint point2 = transformate.GeoToLocal(geoPoint);
                baseViewTread.Frame.Map.ShowPos(point2, 10, true, false);



                GrymCore.IDevPoint pVector = baseViewTread.Factory.CreateDevPoint(50, 50);
                GrymCore.IDevSize pSize = baseViewTread.Factory.CreateDevSize(100, 100);
                GrymCore.IMapGraphics pGraphics = baseViewTread.Frame.Map as GrymCore.IMapGraphics;

                //Удалить все выноски
                
                //pGraphics.RemoveGraphic(pGraphics.get_GraphicByTag(name));

                GrymCore.ICallout pCallout = (GrymCore.ICallout)pGraphics.get_GraphicByTag(name);
                if (pCallout == null)
                {
                    pCallout = pGraphics.CreateCallout(point2, true, pVector, pSize);
                    pGraphics.AddGraphic(pCallout);
                }
                
                pCallout.AutoClose = true;
                pCallout.AddStandardButton(GrymCore.CalloutStandardButtonType.CalloutStandardButtonTypeClose);
                pCallout.Title = name;
                pCallout.Tag = name;
                
                
                

            }
            catch (Exception e)
            {

            }
        }

        public static void showAddressIn2GIS(String street, String house)
        {

            gisModule.geoPoint p = gisModule.getLocationByText("", street, house, "", false);
            try
            {

                GrymCore.IGrym grymApp = null;
                GrymCore.IBaseReference baseRef = null;
                GrymCore.IBaseViewThread baseViewTread = null;
                GrymCore.ICommandLine cmdLine = null;
                grymApp = new GrymCore.GrymClass();

                //GrymCore.IGrymObjectFactory factory = GrymCore.IGrymObjectFactory
                // Получаем описание файла данных для заданного города
                // из коллекции описаний.
                baseRef = grymApp.BaseCollection.FindBase("Ярославль");
                baseViewTread = grymApp.GetBaseView(baseRef, true, false);
                GrymCore.IMapPoint geoPoint = baseViewTread.Factory.CreateMapPoint(p.lon, p.lat);

                baseViewTread.Activate(3);
                GrymCore.IMapCoordinateTransformationGeo transformate = (GrymCore.IMapCoordinateTransformationGeo)baseViewTread.Frame.Map.CoordinateTransformation;
                //grymApp.

                GrymCore.IMapPoint point = transformate.GeoToLocal(geoPoint);
                baseViewTread.Frame.Map.ShowPos(point, 10, true, false);
            }
            catch (Exception e)
            {
                
            }
        }


    }
}
